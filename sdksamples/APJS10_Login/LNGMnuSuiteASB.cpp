//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/basicmenu/BscMnuSuiteASB.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

//===========================
//  INCLUDES
//===========================

#include "VCPlugInHeaders.h"								// For MSVC

// Interface headers
#include "ILoginMenuSuite.h"							// Superclass declaration

// API includes:
#include "CmdUtils.h"
#include "SelectionASBTemplates.tpp"

/** 
	Abstract selection boss (ASB) IBscMnuSuite implementation.  The purpose of this integrator suite is
	to determine how to forward the client request on to the CSB suite(s).  Note that the client does not
	interact with the CSB (which has all the real implementation of the suite) directly, the client interacts 
	with the ASB only.  Also note that the Suite API shouldn't contain model data that is specific to a 
	selection format (layout uidLists, text model/range, etc) so that the client code can be completely 
	decoupled from the underlying CSB.

	@ingroup basicmenu
	@author Lee Huang
*/
class LNGMnuSuiteASB : public CPMUnknown<ILoginMenuSuite>
{
public:
	/**
		Constructor.
		@param iBoss interface ptr from boss object on which this interface is aggregated.
	*/
	LNGMnuSuiteASB(IPMUnknown *iBoss);
	
	/** Destructor. */
	virtual ~LNGMnuSuiteASB(void);

	/**
		See IBscMnuSuite::CanApplyBscMnu
	*/
	virtual bool16			CanApplyLoginMenu(void);
	virtual bool16			CanApplyLoginMenuForDoc(void );
};

/* CREATE_PMINTERFACE
 	Binds the C++ implementation class onto its ImplementationID making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(LNGMnuSuiteASB, kLNGMnuSuiteASBImpl)

/* LNGMnuSuiteASB Constructor
*/
LNGMnuSuiteASB::LNGMnuSuiteASB(IPMUnknown* iBoss) :
	CPMUnknown<ILoginMenuSuite>(iBoss)
{
}

/* LNGMnuSuiteASB Destructor
*/
LNGMnuSuiteASB::~LNGMnuSuiteASB(void)
{
}

#pragma mark-
/* CanApplyBscMnuData
*/
bool16 LNGMnuSuiteASB::CanApplyLoginMenu(void)
{
	return (AnyCSBSupports (make_functor(&ILoginMenuSuite::CanApplyLoginMenu), this));
}


bool16 LNGMnuSuiteASB::CanApplyLoginMenuForDoc(void)
{
	return (AnyCSBSupports (make_functor(&ILoginMenuSuite::CanApplyLoginMenuForDoc), this));
}

// End, LNGMnuSuiteASB.cpp.

