/*
//
//	File: listBoxHelper.cpp
//
//	Date: 6-Mar-2001
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/
#include "VCPlugInHeaders.h"
// Interface includes

#include "listBoxHelperFuncs.h"

#include "IListControlDataOf.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "IWidgetParent.h"
#include "IListBoxAttributes.h"
#include "IControlView.h"
#include "IApplication.h"
#include "IListBoxController.h"

// implem includes
#include "PersistUtils.h" // GetDatabase
//#include "PalettePanelUtils.h"
#include "CreateObject.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"

#include "SDKListBoxHelper.h"
#include "CAlert.h"

//#include "IAppFramework.h"
#include "LoginHelper.h"
#include "dataStore.h"
#include "LNGID.h"

#define CA(X) CAlert::InformationAlert(X)


listBoxHelper::listBoxHelper(IPMUnknown * owner, int32 pluginId) : fOwner(owner), fOwnerPluginID(pluginId)
{

}

listBoxHelper::~listBoxHelper()
{
	fOwner=nil;
	fOwnerPluginID=0;
}

IControlView * listBoxHelper ::FindCurrentListBox()
{
	if(!verifyState())
		return nil;
	
	IControlView * listBoxControlView = nil;
	do {
		WidgetID listBoxID =kLaunchpadListBoxWidgetID;

		InterfacePtr<IPanelControlData> iPanelControlData(fOwner,UseDefaultIID());
		ASSERT_MSG(iPanelControlData != nil, "listBoxHelper ::FindCurrentListBox() iPanelControlData nil");
		if(iPanelControlData == nil) {
			CAlert::InformationAlert("iPanelControlData NULL");
			break;
		}
		listBoxControlView = iPanelControlData->FindWidget(listBoxID);
		ASSERT_MSG(listBoxControlView != nil, "listBoxHelper ::FindCurrentListBox() no listbox");
		if(listBoxControlView == nil) {
			CAlert::InformationAlert("listBoxControlView NULL");
			break;
		}
	
	} while(0);

	return listBoxControlView;
}
 
void listBoxHelper::AddElements(int atIndex)
{
	//CA("listBoxHelper::AddElements");

	if(!verifyState())
		return;
	do
	{
		LoginInfoValue currentServerInfo;
		VectorLoginInfoPtr result=NULL;
		VectorLoginInfoValue::iterator it;
		LoginHelper LoginHelperObj(fOwner);
		
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) {
			CAlert::InformationAlert(" FindCurrentListBox nil ");
			break;
		}
		result=	LoginHelperObj.getAvailableServers();
		if(result== NULL){
			//CAlert::InformationAlert(" Currently there are no environments set. Please create a new environment before starting to use. ");
			//CAlert::InformationAlert(" Please Create New Environment and Server name/URL Properties. "); //AvailableServers File not found...
			break;
		}
		if(result->size()==0){
			//CAlert::InformationAlert("Available Servers File show zero entries.");
			break;
		}		
		for(it = result->begin(); it != result->end(); it++)
		{
			currentServerInfo = *it;	
			InterfacePtr<IListBoxAttributes> listAttr(listBox, UseDefaultIID());
			if(listAttr == nil) {
				CAlert::InformationAlert("AddElements listAttr nil");
				break;	
			}

			RsrcID widgetRsrcID = listAttr->GetItemWidgetRsrcID();
			if (widgetRsrcID == 0){
				CAlert::InformationAlert("AddElements widgetRsrcID nil");
				return;
			}
			
			RsrcSpec elementResSpec(LocaleSetting::GetLocale(), fOwnerPluginID, kViewRsrcType, widgetRsrcID);
			// Create an instance of the list element type
			InterfacePtr<IControlView> newElView( (IControlView*) ::CreateObject(::GetDataBase(listBox), elementResSpec, IID_ICONTROLVIEW));			
			if(newElView == nil){
				CAlert::InformationAlert("AddElements newElView nil");
				break;
			}
			this->addListElementWidget(listBox,newElView,&currentServerInfo,atIndex);
		}
		delete result;
		SelectPrevSelectedElement(listBox);
	}
	while (false);
}

void listBoxHelper::AddElement(IControlView * listBox, LoginInfoValue& currentServerInfo)
{	
	do
	{	
		LoginHelper LoginHelperObj(fOwner);	
			
		// Create an instance of a list element
		InterfacePtr<IListBoxAttributes> listAttr(listBox, UseDefaultIID());
		if(listAttr == nil) {
			CAlert::InformationAlert("listAttr nil");
			break;	
		}
		RsrcID widgetRsrcID = listAttr->GetItemWidgetRsrcID();
		if (widgetRsrcID == 0){
			CAlert::InformationAlert("widgetRsrcID nil");
			return;
		}
		RsrcSpec elementResSpec(LocaleSetting::GetLocale(), fOwnerPluginID, kViewRsrcType, widgetRsrcID);
		// Create an instance of the list element type
		InterfacePtr<IControlView> newElView( (IControlView*) ::CreateObject(::GetDataBase(listBox), elementResSpec, IID_ICONTROLVIEW));			
		if(newElView == nil){
			CAlert::InformationAlert("newElView nil");
			break;
		}	
		this->addListElementWidget(listBox,newElView,&currentServerInfo,-2);	
		
	}
	while (false);
}

void listBoxHelper::RemoveElementAt(int indexRemove)
{
	if(!verifyState())
		return;
	
	do		// false loop
	{
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) {
			break;
		}
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "listBoxHelper::RemoveElementAt() Found listbox but not control data?");
		if(listControlData==nil) {
			CAlert::InformationAlert("listControlData nil");
			break;
		}
		int32 entries=listControlData->Length();
		if(indexRemove < 0 || indexRemove >=entries ){
			// Don't remove outside of list data bounds
			break;
		}
		listControlData->Remove(indexRemove);
		removeCellWidget(listBox, indexRemove);	
		
		InterfacePtr<IListBoxController> listController(listBox, IID_ILISTBOXCONTROLLER);
		ASSERT_MSG(listControlData != nil, "listBoxHelper::RemoveElementAt() Found listbox but not controller?");
		if(listControlData==nil) {
			CAlert::InformationAlert("listController nil");
			break;
		}
		if(entries-1 == indexRemove) // last
			listController->Select(indexRemove-1);
		else listController->Select(indexRemove);
	}
	while (false);	// false loop
}

void listBoxHelper::RemoveLastElement()
{
	if(!verifyState())
		return;	
	do
	{
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) {
			CAlert::InformationAlert("FindCurrentListBox nil");
			break;
		}
		
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());		
		if(listControlData==nil){
			CAlert::InformationAlert("listControlData nil");
			break;
		}
		int lastIndex = listControlData->Length()-1;
		if(lastIndex > 0) {		
			listControlData->Remove(lastIndex);
			removeCellWidget(listBox, lastIndex);
		}
		InterfacePtr<IListBoxController> listController(listBox,IID_ILISTBOXCONTROLLER);
		ASSERT_MSG(listControlData != nil, "listBoxHelper::RemoveLastElement() Found listbox but not controller?");
		if(listController==nil) {
			CAlert::InformationAlert("listController nil");
			break;
		}		
		listController->Select(lastIndex-1);
	}
	while (false);
}

void listBoxHelper::SelectLastElement(IControlView* listBox)
{
	do
	{		
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());		
		if(listControlData==nil){
			CAlert::InformationAlert("listControlData nil");
			break;
		}
		int lastRow = listControlData->Length()-1;
		if(lastRow == -1) {		
			break;
		}
		InterfacePtr<IListBoxController> listController(listBox,IID_ILISTBOXCONTROLLER);		
		if(listController==nil) {
			CAlert::InformationAlert("listController nil");
			break;
		}		
		listController->Select(lastRow);
	}
	while (false);
}


void listBoxHelper::SelectPrevSelectedElement(IControlView* listBox)
{
	do
	{		
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());		
		if(listControlData==nil){
			CAlert::InformationAlert("listControlData nil");
			break;
		}
		int lastRow = listControlData->Length()-1;
		if(lastRow == -1) {		
			break;
		}
		InterfacePtr<IListBoxController> listController(listBox,IID_ILISTBOXCONTROLLER);		
		if(listController==nil){
			CAlert::InformationAlert("listController nil");
			break;
		}
		int curRow=EvnironmentData::getSelectedserver();
		if(lastRow < curRow)
			curRow=lastRow;			
		listController->Select(curRow);
	}
	while (false);
}

int listBoxHelper::GetElementCount() 
{
	int retval=0;	
	do {
	
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil) {
			break;
		}
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());		
		if(listControlData==nil) {
			break;
		}
		retval = listControlData->Length();
	} while(0);
	
	return retval;
}

void listBoxHelper::removeCellWidget(IControlView * listBox, int removeIndex) {
	
	//CAlert::InformationAlert("removeCellWidget");
	do {

		if(listBox==nil) break;
		// recall that when the element is added, it is added as a child of the cell-panel
		// widget. Therefore, navigate to the cell panel and remove the child at the specified
		// index. Simultaneously, remove the corresponding element from the list controldata.
		// +
		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		ASSERT_MSG(panelData != nil, "listBoxHelper::removeCellWidget()  Cannot get panelData");
		if(panelData == nil) {
			break;
		}
		
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "listBoxHelper::removeCellWidget() cannot find cellControlView");
		if(cellControlView == nil) {
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil,"listBoxHelper::removeCellWidget() cellPanelData nil"); 
		if(cellPanelData == nil) {
			break;
		}

		if(removeIndex < 0 || removeIndex >= cellPanelData->Length()) {
			break;
		}
		cellPanelData->RemoveWidget(removeIndex);
		// -

	} while(0);

}

// adds element to the list box
void listBoxHelper::addListElementWidget(IControlView* listbox,InterfacePtr<IControlView> & elView, LoginInfoValue *serverValue, int atIndex)
{	
	// get data
	do
	{		
		// Find the child widgets
		InterfacePtr<IPanelControlData> newElPanelData(elView, UseDefaultIID());
		if (newElPanelData == nil) {
			break;
		}
		// Locate the child that displays the 'name' value
		IControlView* nameTextView = newElPanelData->FindWidget(kLaunchpadListBoxEnvTextWidgetID);
		if ( (nameTextView == nil)  ) {
			break;
		}
		IControlView* nameTextView2 = newElPanelData->FindWidget(kLaunchpadListBoxSrvTextWidgetID);
		if ( (nameTextView2 == nil) ) {
			break;
		}

		// Set the  name in the static text widget of this element
		InterfacePtr<ITextControlData> newEltext (nameTextView,UseDefaultIID());
		if (newEltext == nil) {
			CAlert::InformationAlert("newEltext 1 nil");
			break;
		}	
		InterfacePtr<ITextControlData> newEltext2 (nameTextView2,UseDefaultIID());
		if (newEltext2 == nil) 
		{
			CAlert::InformationAlert("newEltext 2 nil");
			break;
		}				
		PMString ASD(serverValue->getEnvName());
		//CAlert::InformationAlert(ASD);
		newEltext->SetString(ASD, kTrue, kTrue);
		ASD.Clear();
		
		/*if(serverValue->getdbConnectionMode() == 1)
		{	ASD.Append(serverValue->getServerName());
			if(ASD.IsEmpty())
			break;
			newEltext2->SetString(serverValue->getServerName(), kTrue, kTrue);
		}
		else if(serverValue->getdbConnectionMode() == 0)*/
		{	ASD.Append(serverValue->getCmurl());
			if(ASD.IsEmpty())
			break;
			newEltext2->SetString(ASD, kTrue, kTrue);
		}

		// Find the Cell Panel widget and it's panel control data interface
		InterfacePtr<IPanelControlData> panelData(listbox,UseDefaultIID());		
		if(panelData == nil){
			CAlert::InformationAlert("IPanelControlData nil");
			break;
		}

		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);		
		if(cellControlView == nil){
			CAlert::InformationAlert("FindWidget nil");
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());		
		if(cellPanelData == nil){
			CAlert::InformationAlert("cellControlView IPanelControlData nil");
			break;
		}				
		cellPanelData->AddWidget(elView);				

		InterfacePtr< IListControlDataOf<IControlView*> > listData(listbox, UseDefaultIID());		
		if(listData == nil) { 
			CAlert::InformationAlert("listData nil");
			break;
		}
		listData->Add(elView);	
		SelectLastElement(listbox);
	}while(0);
}
			
void listBoxHelper::EmptyCurrentListBox()
{	
	do {
		IControlView* listBoxControlView = this->FindCurrentListBox();
		if(listBoxControlView == nil) {
			break;
		}
		InterfacePtr<IListControlData> listData (listBoxControlView, UseDefaultIID());
		if(listData == nil) {
			break;
		}
		InterfacePtr<IPanelControlData> iPanelControlData(listBoxControlView, UseDefaultIID());
		if(iPanelControlData == nil) {
			break;
		}
		IControlView* panelControlView = iPanelControlData->FindWidget(kCellPanelWidgetID);
		if(panelControlView == nil) {
			break;
		}
		InterfacePtr<IPanelControlData> panelData(panelControlView, UseDefaultIID());
		if(panelData == nil) {
			break;
		}
		listData->Clear(kFalse, kFalse);
		panelData->ReleaseAll();
		listBoxControlView->Invalidate();
	} while(0);
}

void listBoxHelper::getselectedEnvironmentName(int *selectedServer,PMString& envName)
{
	*selectedServer=-1;
	do
	{
		IControlView * listBox = this->FindCurrentListBox();
		if(listBox == nil){
			CAlert::InformationAlert("FindCurrentListBox nil");
			break;
		}


		InterfacePtr<IListBoxController> listCntl(listBox,IID_ILISTBOXCONTROLLER);		
		if(listCntl == nil) {
			CAlert::InformationAlert("listCntl nil");
			break;
		}		
		
		K2Vector<int32> multipleSelection ;
		listCntl->GetSelected( multipleSelection ) ;
		const int kSelectionLength =  multipleSelection.Length() ;
		int indexSelected;
		if (kSelectionLength > 0 )
			indexSelected = multipleSelection[0];

		//PMString str("Clicked at : ");
		//str.AppendNumber(indexSelected);
		//CAlert::InformationAlert(str);
	
		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		if (panelData == nil) {
			CAlert::InformationAlert("listBox IPanelControlData nil");
			break;
		}
		
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);		
		if(cellControlView == nil){
			CAlert::InformationAlert("FindWidget nil");
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData(cellControlView, UseDefaultIID());		
		if(cellPanelData == nil){
			CAlert::InformationAlert("cellControlView IPanelControlData nil");
			break;
		}	
		
		IControlView* nameTextView = cellPanelData->GetWidget(indexSelected);
		if ( (nameTextView == nil)  ) {
			//CAlert::InformationAlert("FindWidget nameTextView nil");
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelDataChild(nameTextView, UseDefaultIID());		
		if(cellPanelDataChild == nil){
			CAlert::InformationAlert("cellControlViewChild IPanelControlData nil");
			break;
		}

		IControlView* nameTextViewChild = cellPanelDataChild->GetWidget(1);
		if ( (nameTextView == nil)  ) {
			CAlert::InformationAlert("FindWidgetChild nameTextViewChild nil");
			break;
		}


		InterfacePtr<ITextControlData> newEltext(nameTextViewChild,UseDefaultIID());
		if (newEltext == nil) {
			CAlert::InformationAlert("newEltext 1 nil");
			break;
		}
		envName=newEltext->GetString();	
		*selectedServer=indexSelected;
		
	}while(0);
}

void listBoxHelper::modifyListElementData(IPanelControlData* iPanelControlData)
{	
	do
	{		
		IControlView * listBox = iPanelControlData->FindWidget(kLaunchpadListBoxWidgetID);		
		if(listBox == nil) {
			CAlert::InformationAlert("FindWidget fail");
			break;
		}	

		InterfacePtr<IListBoxController> listCntl(listBox,IID_ILISTBOXCONTROLLER);	
		if(listCntl == nil) {
			CAlert::InformationAlert("listCntl nil");
			break;
		}		
		int indexSelected=listCntl->GetSelected();
		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		if (panelData == nil) {
			CAlert::InformationAlert("listBox IPanelControlData nil");
			break;
		}
		
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);		
		if(cellControlView == nil){
			CAlert::InformationAlert("FindWidget nil");
			break;
		}
		InterfacePtr<IPanelControlData> cellPanelData(cellControlView, UseDefaultIID());		
		if(cellPanelData == nil){
			CAlert::InformationAlert("cellControlView IPanelControlData nil");
			break;
		}	
		
		IControlView* nameTextView = cellPanelData->GetWidget(indexSelected);
		if ( (nameTextView == nil)  ) {
			CAlert::InformationAlert("FindWidget nameTextView nil");
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelDataChild(nameTextView, UseDefaultIID());		
		if(cellPanelDataChild == nil){
			CAlert::InformationAlert("cellControlViewChild IPanelControlData nil");
			break;
		}
		IControlView* nameTextViewChild = cellPanelDataChild->GetWidget(1);
		if ( (nameTextView == nil)  ) {
			CAlert::InformationAlert("FindWidgetChild nameTextView nil");
			break;
		}

		InterfacePtr<ITextControlData> newEltext(nameTextViewChild,UseDefaultIID());
		if (newEltext == nil) {
			CAlert::InformationAlert("newEltext 1 nil");
			break;
		}
		newEltext->SetString(EvnironmentData::getServerProperties().getEnvName());
		IControlView* nameTextViewChild2 = cellPanelDataChild->GetWidget(2);
		if ( (nameTextViewChild2 == nil)  ) {
			CAlert::InformationAlert("nameTextViewChild2 nil");
			break;
		}
		InterfacePtr<ITextControlData> newEltext2(nameTextViewChild2,UseDefaultIID());
		if (newEltext2 == nil) {
			CAlert::InformationAlert("newEltext 2  nil");
			break;
		}	


		//if(EvnironmentData::getServerProperties().getdbConnectionMode() == 0)
		{
			
			//CA("EvnironmentData::getServerProperties().getCmurl()	:	"+EvnironmentData::getServerProperties().getCmurl());
			newEltext2->SetString(EvnironmentData::getServerProperties().getCmurl());		
		}
		/*else if(EvnironmentData::getServerProperties().getdbConnectionMode() == 1)
			{
			
				newEltext2->SetString(EvnironmentData::getServerProperties().getServerName());		
		}*/

	}while(0);
}