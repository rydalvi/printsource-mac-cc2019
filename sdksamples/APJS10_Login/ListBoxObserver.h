/*
//	File:	LNGListBoxObserver.cpp
//
//	Date:	5-Mar-2001
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
*/
#include "VCPlugInHeaders.h"

// Implementation includes
#include "WidgetID.h"

// Interface includes
#include "ISubject.h"
#include "IControlView.h"
#include "IListControlData.h"
#include "IListBoxController.h"
// Implem includes
#include "CAlert.h"
#include "CObserver.h"
#include "LNGID.h"
#include "listBoxHelperFuncs.h"
#include "dataStore.h"

/**

	Implements IObserver. The intent of this class is to provide handling for the AutoAttach (sent when shown), AutoDetach (hidden)
	and Update (when listbox hit by end-user, for instance) message
	
	 The class is derived from CObserver, and overrides the
	AutoAttach(), AutoDetach(), and Update() methods.
	This class implements the IObserver interface using the CObserver helper class,
	and is listening along the IID_ILISTCONTROLDATA protocol for changes in the list-data model.

	@author Ian Paterson
	@version API Level 1.5
*/
class LNGListBoxObserver : public CObserver
{
public:
	
	/**
		Constructor for WLBListBoxObserver class.
		@param interface ptr from boss object on which this interface is aggregated.
	*/
	LNGListBoxObserver(IPMUnknown *boss);

	/**
		Destructor for LNGListBoxObserver class - 
		performs cleanup 
	*/	
	~LNGListBoxObserver();

	/**
		AutoAttach is only called for registered observers
		of widgets.  This method is called by the application
		core when the widget is shown.
	
	*/	
	virtual void AutoAttach();

	/**
		AutoDetach is only called for registered observers
		of widgets. Called when widget hidden.
	*/	
	virtual void AutoDetach();

	/**
		Update is called for all registered observers, and is
		the method through which changes are broadcast. 

		This class is interested in changes along IID_ILISTCONTROLDATA protocol with classID of
		kListSelectionChangedByUserMessage. This message is sent when a user clicks on an element
		in the list-box.

	
		@param theChange this is specified by the agent of change; it can be the class ID of the agent,
		or it may be some specialised message ID.
		@param theSubject this provides a reference to the object which has changed; in this case, the button
		widget boss object that is being observed.
		@param protocol the protocol along which the change occurred.
		@param changedBy this can be used to provide additional information about the change or a reference
		to the boss object that caused the change.
	*/	
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
	void updateListBox(bool16 isAttaching);
private:
	/**
		Helper method to change state of list-box, dependent on whether
		it is being shown or hidden.
		@param isAttaching specifies whether attaching (shown) or detaching (hidden)
	*/
	

	
};