#include "VCPlugInHeaders.h"
#include "IMessageServer.h"
#include "CAlert.h"

////temp alert message test
//void CAMessage(const PMString & fileName,const PMString & functionName,const PMString & message,int32 line)
//{
//	InterfacePtr<IMessageServer > ptrIMsgServer((static_cast<IMessageServer*> (CreateObject(kMessageServerBoss,IID_IMESSAGESERVER))));
//	if(ptrIMsgServer==nil)
//	{
//		CAlert::ErrorAlert("Interface for IClientOptions not found.");
//		return;
//	}
//	//if(0)//functionName == "TableUtility::AddTableAndCellElements"/*functionName == "TableUtility::TagTable"*//*functionName == "TableUtility::TagTableCellText"*/)
//	{
//		//CAlert::InformationAlert(pluginName + "\n" + fileName + "\n" + functionName + "\n" + message);
//		//CAlert::InformationAlert(functionName + "\n" + message);
//		
//		ptrIMsgServer->Message("AP45_CatalogIndex",fileName,functionName,message,line);
//	}
//
//}
#include "VCPlugInHeaders.h"
//#include "IMessageServer.h"
#include "CAlert.h"

//temp alert message test
void CAMessage(const PMString & fileName,const PMString & functionName,const PMString & message,int32 line)
{
	//InterfacePtr<IMessageServer > ptrIMsgServer((static_cast<IMessageServer*> (CreateObject(kMessageServerBoss,IID_IMESSAGESERVER))));
	//if(ptrIMsgServer==nil)
	//{
		//CAlert::ErrorAlert("Interface for IClientOptions not found.");
		//return;
	//}
	//if(0)//functionName == "TableUtility::AddTableAndCellElements"/*functionName == "TableUtility::TagTable"*//*functionName == "TableUtility::TagTableCellText"*/)
	//{
		//CAlert::InformationAlert(pluginName + "\n" + fileName + "\n" + functionName + "\n" + message);
		//CAlert::InformationAlert(functionName + "\n" + message);
		
		//ptrIMsgServer->Message("AP45_Login",fileName,functionName,message,line);
	//The pluginName and fileName can be used to filter the messages.
	PMString pluginNameString("PluginName : ");
	pluginNameString.Append("AP45_Login.pln");

	PMString fileNameString("FileName : ");
	fileNameString.Append(fileName);

	PMString functionNameString("FunctionName : ");
	functionNameString.Append(functionName);

	PMString lineNumber("Line Number : ");
	lineNumber.AppendNumber(line);
	lineNumber.Append("\n***********************");

	PMString messageString("Message : ");
	messageString.Append(message);

	CAlert::InformationAlert(pluginNameString + "\n" + fileNameString +"\n" + functionNameString + "\n" + lineNumber +"\n" + messageString);
	//}

}