#include "VCPlugInHeaders.h"
#include "PlugIn.h"
#include "LNGMediator.h"
#include "CAlert.h"

class LNGPluginLoader : public PlugIn
{
private :
	LNGMediator gMedObj;
public:
	LNGMediator getMediator(void);
	bool16 Load(ISession*);
};

bool16 LNGPluginLoader::Load(ISession* gSession)
{
	PlugIn::Load(gSession);
	CAlert::InformationAlert("Coming ONLY ONCE");
	LNGMediator md;
	gMedObj=md;
	return kTrue;
}

LNGMediator LNGPluginLoader::getMediator(void)
{
	return gMedObj;
}
