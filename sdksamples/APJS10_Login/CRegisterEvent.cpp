#include "VCPlugInHeaders.h"
#include "IRegisterEvent.h"
#include "LNGMediator.h"
#include "LNGPlugInEntrypoint.h"

class CRegisterEvent : public CPMUnknown<IRegisterLoginEvent>
{
	public:
		CRegisterEvent(IPMUnknown* );
		~CRegisterEvent();
		void registerLoginEvent(int32);
};

CREATE_PMINTERFACE(CRegisterEvent,kLNGLoginEventsHandlerImpl)

CRegisterEvent::CRegisterEvent(IPMUnknown* boss):
CPMUnknown<IRegisterLoginEvent>(boss)
{}

CRegisterEvent::~CRegisterEvent()
{}

void CRegisterEvent::registerLoginEvent(int32 evt) 
{ 
	LNGMediator* md=getMediators(); 
	md->addToObservers(evt); 
}
