#include "VCPlugInHeaders.h"
#include "ListBoxObserver.h"

#define CA(X) CAlert::InformationAlert(X)

CREATE_PMINTERFACE(LNGListBoxObserver, kLNGLaunchpadListBoxObserImpl)


LNGListBoxObserver::LNGListBoxObserver(IPMUnknown* boss)
: CObserver(boss)
{
	
}

LNGListBoxObserver::~LNGListBoxObserver()
{
	
}


void LNGListBoxObserver::AutoAttach()
{
	//CAlert::InformationAlert("List AutoAttach");

	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
	}

	updateListBox(kTrue);
	listBoxHelper listHelper(this, kLNGPluginID);
	IControlView * listBox = listHelper.FindCurrentListBox();
	if(listBox == nil) {
		CAlert::InformationAlert("LNGListBoxObserver::Update IControlView nil");
		//break;
	}
	InterfacePtr<IListBoxController> listCntl(listBox,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface			
	if(listCntl == nil) {
		CAlert::InformationAlert("IListBoxController nil");

		//break;
	}
	listCntl->Select(0);
}


void LNGListBoxObserver::AutoDetach()
{
	//CAlert::InformationAlert("List AutoDetach");

	updateListBox(kFalse);
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this,IID_IUNKNOWN);
	}
}

// actions in listbox handle here
void LNGListBoxObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	//CAlert::InformationAlert("LNGListBoxObserver::Update");
	if ((protocol == IID_ILISTCONTROLDATA) && (theChange == kListSelectionChangedByUserMessage) ) {
		do {			
			listBoxHelper listHelper(this, kLNGPluginID);
			IControlView * listBox = listHelper.FindCurrentListBox();
			if(listBox == nil) {
				CAlert::InformationAlert("LNGListBoxObserver::Update IControlView nil");
				break;
			}
			InterfacePtr<IListBoxController> listCntl(listBox,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface			
			if(listCntl == nil) {
				CAlert::InformationAlert("IListBoxController nil");
				break;
			}
			PMString envName("");
			int selection=-1;
			listHelper.getselectedEnvironmentName(&selection,envName);
			//CAlert::InformationAlert(envName);
			EvnironmentData::setSelectedserver(selection);	
			EvnironmentData::setSelectedServerEnvName(envName);
		} while(0);
	}
}

// here adding entries to the listbox
void LNGListBoxObserver::updateListBox(bool16 isAttaching)
{
	//CAlert::InformationAlert("updateListBox");
	listBoxHelper listHelper(this, kLNGPluginID);
	listHelper.EmptyCurrentListBox();
	if(isAttaching){
		do{			
			listHelper.AddElements(); // adds entries to list.
			IControlView * listBox = listHelper.FindCurrentListBox();
			if(listBox == nil) {
				CAlert::InformationAlert("FindCurrentListBox nil");
				break;
			}
		//	listBox->Invalidate();
		} while(0);
	}
}

