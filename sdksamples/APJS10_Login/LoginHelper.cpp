
#include "VCPlugInHeaders.h"
//#include "IAppFramework.h"
#include "LoginHelper.h"
#include "ILoginHelper.h"
#include "ICoreFilename.h"
#include "SDKUtilities.h"
#include "ICoreFilenameUtils.h"
#include "CAlert.h"
#include "dataStore.h"
#include "LNGID.h"
#include "LNGActionComponent.h"

#include <fstream>
#include <string>
#define CA(x) CAlert::InformationAlert(x)



CREATE_PMINTERFACE(LoginHelper , kLNGLoginHelperImpl)

LoginHelper::LoginHelper(IPMUnknown* boss) : CPMUnknown<ILoginHelper>(boss)
{}

LoginHelper::~LoginHelper()
{}

bool8 LoginHelper::getAppFolder(PMString &appFolder)
{	
	bool8 status = FALSE;
	do
	{
		InterfacePtr<ICoreFilename> iExecutableCoreFilename((ICoreFilename*)::CreateObject(kCoreFilenameBoss, IID_ICOREFILENAME));
		if (iExecutableCoreFilename == nil)
		{
			ASSERT_FAIL("CHLinguisticUtils::GetApplicationFolder() iExecutableCoreFilename invalid" );
			break;
		}

		InterfacePtr<ICoreFilenameUtils> iCoreFilenameUtils((ICoreFilenameUtils*)::CreateObject(kCoreFilenameUtilsBoss, IID_ICOREFILENAMEUTILS));
		if (iCoreFilenameUtils == nil)
		{
			ASSERT_FAIL("CHLinguisticUtils::GetApplicationFolder() iCoreFilenameUtils invalid" );
			break;
		}

		iCoreFilenameUtils->GetExecutableName(iExecutableCoreFilename);
		
		const PMString* executableFilename = iExecutableCoreFilename->GetFullName();
		if (executableFilename == nil){
			break;
		}
		if (executableFilename->NumUTF16TextChars () <= 0){		
			break;
		}
		appFolder = *executableFilename;
		SDKUtilities::RemoveLastElement(appFolder);

		PMString pluginFolderStr("");
#ifdef MACINTOSH
		pluginFolderStr+=":";
#else
		pluginFolderStr+="\\";
#endif
		pluginFolderStr+="Plug-ins";
#ifdef MACINTOSH
		pluginFolderStr+=":";
#else
		pluginFolderStr+="\\";
#endif
		pluginFolderStr+="PRINTsource";
		appFolder+=pluginFolderStr;
		status = TRUE;
#ifdef MACINTOSH
		//SDKUtilities::getMacPath(appFolder);
        while(appFolder.Contains(":"))
		{
			SDKUtilities::Replace(appFolder,":","/");
		}
#endif


	} while (false); // only do once
	return status;
}

bool8 LoginHelper::isAvailableServersFileExists(PMString &fPath)
{
	FILE *fp=NULL;
    PMString tempPath = fPath;
    
#ifdef MACINTOSH
	SDKUtilities::convertToMacPath(tempPath); //SDKUtilities::getMacPath(fPath);
#endif
	fp=std::fopen(fPath.GetPlatformString().c_str(), "r");
	if(!fp)
		return FALSE;
	std::fclose(fp);
	return TRUE;
}

bool8 LoginHelper::isEnvironmentKeyWordExistsInFile(PMString &fPath)
{
	FILE *fp=NULL;
	char str[512];	
	bool8 isKeyExist=kFalse;

#ifdef MACINTOSH
	SDKUtilities::convertToMacPath(fPath);    //SDKUtilities::getMacPath(fPath);
#endif

	fp=std::fopen(fPath.GetPlatformString().c_str(), "r");
	if(!fp)
		return FALSE;

	while(std::fgets(str, 255, fp))
	{
		//PMString lineText(str);
		WideString lineText(kBlankStringTextKey);
		lineText = (WideString) str;

		//PMString env(kenvironmentsTextkey);		
		WideString env("environments=");
		//CA(env);

		//CharCounter flag=lineText.IndexOfString(env);
		CharCounter flag=lineText.IndexOf(env);
		if(flag!=-1)
		{
			isKeyExist=kTrue;
			break;
		}
	}
	std::fclose(fp);
	if(isKeyExist==kFalse)
	{
		if(createAvailableServersFile(fPath)==FALSE)
			CAlert::InformationAlert("Failed to create the available servers file");
	}
	return TRUE;
}

bool8 LoginHelper::createAvailableServersFile(PMString &fPath)
{
	FILE *fp=NULL;
#ifdef MACINTOSH
	//SDKUtilities::getMacPath(fPath);
    while(fPath.Contains(":"))
	{
		SDKUtilities::Replace(fPath,":","/");
	}
#endif
	fp=std::fopen(fPath.GetPlatformString().c_str(), "w+");
	if(!fp)
		return FALSE;
	char *env="environments=";
	std::fprintf(fp, "%s", env);
	std::fclose(fp);
	return TRUE;
}

void LoginHelper::appendFileName(PMString &folderName)
{
#ifdef MACINTOSH
	folderName+=":";
#else
	folderName+="\\";
#endif
	folderName+="AvailableServers.properties";
}

VectorLoginInfoPtr LoginHelper::getAvailableServers(void)
{	
	//CA("Inside getAvailableServers");
	VectorLoginInfoPtr result=NULL;
	PMString appFolder(kBlankStringTextKey);
	
	//do{
	//	if(getAppFolder(appFolder)==FALSE){
	//		CAlert::InformationAlert("Fail to get application folder path");
	//		break;
	//	}
	//	EvnironmentData::setAvailableServerFilePath(appFolder);
	//	//CAlert::InformationAlert(appFolder);
	//	appendFileName(appFolder);
	//	//CAlert::InformationAlert(appFolder);
	//	if(isAvailableServersFileExists(appFolder)==FALSE){
	//		//CAlert::InformationAlert("File not exists...");
	//		if(createAvailableServersFile(appFolder)==FALSE){
	//			//CAlert::InformationAlert("Fail to create File...");
	//			break;
	//		}			
	//	}		
	//	else{ // file exist, checking for environment key word in the file
	//		isEnvironmentKeyWordExistsInFile(appFolder);
	//	}		

	//}while(0);

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil){
			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			return result;
		}
	
	do{
		//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		
//		PMString fPath=EvnironmentData::getAvailableServerFilePath();
//		PMString fName=EvnironmentData::getAvailableServerFileName();
//#ifdef MACINTOSH
//		SDKUtilities::getUnixPath(fPath);
//#endif
//		const char* CharFilePath = (fPath.GetPlatformString().c_str()); //CS4
//		const char* CharFileName = (fName.GetPlatformString().c_str()); //Cs4
		
		double ClientID = ptrIAppFramework->getClientID();

		result=ptrIAppFramework->getLoginInfoProperties(/*CharFilePath, CharFileName, ClientID*/);
		if(result==NULL){
			//ptrIAppFramework->UTILITYMngr_dispErrorMessage();
			break;
		
		}		
		//VectorServerInfoValue::iterator it;
		//for(it = result->begin(); it != result->end(); it++)
		//{
		//	LoginInfoValue currentServerInfo=(*it);		
		//	// Create an instance of a list element
		//	//CA("Inside Add Elements");
			/*PMString ASD("EnvName inside LoginHelper::getAvailableServers : ");
			ASD.Append((currentServerInfo.getEnvName()));
			ASD.Append(" ServerURL: ");
			ASD.Append(currentServerInfo.getCmurl());
			CA(ASD);*/
		//}
	}while(0);	
	return result;
}

bool16 LoginHelper::getCurrentServerInfo(PMString& envName,LoginInfoValue& selectedServerValue)
{
	LoginInfoValue currentServerInfo;
	VectorLoginInfoPtr result=NULL;
	bool16 bFlag=-1;
	envName = EvnironmentData::getSelectedServerEnvName();
	//CA(envName);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil){
			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			return FALSE;
		}
	do{
//CA("inside LoginHelper::getCurrentServerInfo");
		//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		PMString fPath=EvnironmentData::getAvailableServerFilePath();
////CA("fPath = " + fPath);
//		PMString fName=EvnironmentData::getAvailableServerFileName();
////CA("fName = " + fName);
//#ifdef MACINTOSH
//		SDKUtilities::getUnixPath(fPath);
//#endif
//		const char* CharFilePath =(fPath.GetPlatformString().c_str()); //Cs4 
//		const char* CharFileName = (fName.GetPlatformString().c_str()); //CS4

		double ClientID = ptrIAppFramework->getClientID();
		result=ptrIAppFramework->getLoginInfoProperties(/*CharFilePath,CharFileName, ClientID*/);
		if(result==NULL)
		{
			//ptrIAppFramework->UTILITYMngr_dispErrorMessage();
			break;
		}			
		if(result->size()==0){
			CAlert::InformationAlert("getCurrentServerInfo::Available Servers File show zero entries.");
			break;
		}
		VectorLoginInfoValue::iterator it;

		for(it = result->begin(); it != result->end(); it++)
		{
			currentServerInfo=*it;
//CA("inside for");
			if(currentServerInfo.getEnvName().Compare(kTrue , envName) == 0)
			{
//CA("inside LoginHelper before setting of selectedServerValue");
				EvnironmentData::setSelectedServerProperties(currentServerInfo);//added on 17 feb 2006
				bFlag=0;
				selectedServerValue.setEnvName(currentServerInfo.getEnvName());				
				selectedServerValue.setSchema(currentServerInfo.getSchema());
				selectedServerValue.setDbPassword(currentServerInfo.getDbPassword());
				selectedServerValue.setCmurl(currentServerInfo.getCmurl());
				selectedServerValue.setServerName(currentServerInfo.getServerName());
				////added by Tushar on 08_03_07				
				selectedServerValue.setVendorName(currentServerInfo.getVendorName());
				selectedServerValue.setVersion(currentServerInfo.getVersion());
				selectedServerValue.setServerPort(currentServerInfo.getServerPort());
				selectedServerValue.setDbUserName(currentServerInfo.getDbUserName());
				selectedServerValue.setDbPassword(currentServerInfo.getDbPassword());
				//selectedServerValue.setdbConnectionMode(0/*currentServerInfo.getdbConnectionMode()*/);
//PMString temp = currentServerInfo.getProject();
//CA("temp = " + temp);				

				PMString clientno("");
				clientno.AppendNumber(PMReal(ClientID));
				ClientInfoValue currentClientInfoValue = currentServerInfo.getClientInfoByClientNo(clientno);

				if(currentClientInfoValue.getClientno() != "")
				{
					ClientInfoValue selectedClientInfoValue;
					selectedClientInfoValue.setProject(currentClientInfoValue.getProject());
					selectedClientInfoValue.setLanguage(currentClientInfoValue.getLanguage());
					selectedClientInfoValue.setAssetserverpath(currentClientInfoValue.getAssetserverpath());
					selectedClientInfoValue.setisAddTableHeaders(currentClientInfoValue.getisAddTableHeaders());
					selectedClientInfoValue.setisItemTablesasTabbedText(currentClientInfoValue.getisItemTablesasTabbedText());
					selectedClientInfoValue.setDocumentPath(currentClientInfoValue.getDocumentPath());

					selectedClientInfoValue.setAssetStatus(currentClientInfoValue.getAssetStatus());
					selectedClientInfoValue.setMissingFlag(currentClientInfoValue.getMissingFlag());
					selectedClientInfoValue.setLogLevel(currentClientInfoValue.getLogLevel());
					selectedClientInfoValue.setSectionID(currentClientInfoValue.getSectionID());
					selectedClientInfoValue.setIsONESource(currentClientInfoValue.getIsONESource());
						
					selectedClientInfoValue.setDisplayPartnerImages(currentClientInfoValue.getDisplayPartnerImages());	
					selectedClientInfoValue.setDisplayPickListImages(currentClientInfoValue.getDisplayPickListImages());
					selectedClientInfoValue.setShowObjectCountFlag(currentClientInfoValue.getShowObjectCountFlag());
					selectedClientInfoValue.setAssetStatus(currentClientInfoValue.getAssetStatus());
					selectedClientInfoValue.setLogLevel(currentClientInfoValue.getLogLevel());
				
					selectedServerValue.setClientInfoValue(currentClientInfoValue);
				}
					//selectedServerValue.setdbAppenderForPassword(currentServerInfo.getdbAppenderForPassword());				
							
				EvnironmentData::SelctedConnMode = 0;				

				break;
			}
		}
		if(result)
			delete result;
	}while(0);
	return bFlag;
}

bool16 LoginHelper::addServerInfo(LoginInfoValue& selectedServerValue)
{	
	bool16 result=-1;
	int16 flag=FALSE;
	double clientID = -1;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil){
			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			return FALSE;
		}
	do{
		//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		PMString fPath=EvnironmentData::getAvailableServerFilePath();
		PMString fName=EvnironmentData::getAvailableServerFileName();
		

#ifdef MACINTOSH
		//SDKUtilities::getUnixPath(fPath);
#endif
		
		//result=ptrIAppFramework->LOGINMngr_updateOrCreateNewServerProperties(selectedServerValue,flag,fPath.GrabCString(),fName.GrabCString());//CS3
		result=ptrIAppFramework->LOGINMngr_updateOrCreateNewServerProperties(selectedServerValue,flag,clientID);//Cs4
		if(result==-1){
			//ptrIAppFramework->UTILITYMngr_dispErrorMessage();
			
			break;
		}		
		
	}while(0);
	
	return result;
}

bool16 LoginHelper::deleteServer(PMString& envName)
{	
	bool16 result=-1;
	
		//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil){
			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			return FALSE;
		}
	do{	
//		PMString fPath=EvnironmentData::getAvailableServerFilePath();
//		PMString fName=EvnironmentData::getAvailableServerFileName();
//#ifdef MACINTOSH
//		SDKUtilities::getUnixPath(fPath);
//#endif
//		//result=ptrIAppFramework->LOGINMngr_deleteUser(envName.GrabCString(),fPath.GrabCString(),fName.GrabCString());//Cs3		

		double ClientID = ptrIAppFramework->getClientID();

		result=ptrIAppFramework->LOGINMngr_deleteUser(const_cast<char*>(envName.GetPlatformString().c_str()),
													 /* const_cast<char*>(fPath.GetPlatformString().c_str()),
													  const_cast<char*>(fName.GetPlatformString().c_str()),*/
													  ClientID ); //CS4
		if(result==-1){
			//ptrIAppFramework->UTILITYMngr_dispErrorMessage();
			break;
		}		
	
	}while(0);
		
	return result;
}

bool16 LoginHelper::editServerInfo(LoginInfoValue& selectedServerValue,double clientID)
{
	//CA("LoginHelper::editServerInfo");
	bool16 result=-1;
	int16 flag=TRUE;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil){
			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			return FALSE;
		}
	do{
		//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
				
		PMString fPath=EvnironmentData::getAvailableServerFilePath();
		PMString fName=EvnironmentData::getAvailableServerFileName();
#ifdef MACINTOSH
		//SDKUtilities::getUnixPath(fPath);
#endif

		//result=ptrIAppFramework->LOGINMngr_updateOrCreateNewServerProperties(selectedServerValue,flag,fPath.GrabCString(),fName.GrabCString());//Cs3
		result=ptrIAppFramework->LOGINMngr_updateOrCreateNewServerProperties( selectedServerValue,flag,
																			  //const_cast<char*>(fPath.GrabCString()/*GetPlatformString().c_str()*/),
																			  //const_cast<char*>(fName.GrabCString()/*GetPlatformString().c_str()*/),
																			  clientID
																			 ); //Cs4
		if(result==-1){
			//ptrIAppFramework->UTILITYMngr_dispErrorMessage();
			break;
		}		
	}while(0);
	
	return result;
}

//VectorVendorInfoPtr LoginHelper::getVendors(void)
//{	
//	VectorVendorInfoPtr result=NULL;
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == nil){
//			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
//			return FALSE;
//		}
//	do{
//		result=ptrIAppFramework->LOGINMngr_getVendorVersions();
//		if(result==NULL){
//			//CA("Result is NUll");
//			ptrIAppFramework->UTILITYMngr_dispErrorMessage();
//			break;
//		}		
//	}while(0);
//	
//	return result;
//}

bool16 LoginHelper::loginUser(PMString &username,PMString &password,PMString &clientName,VectorClientModelPtr vectorClientModelPtr, PMString & errorMsg)
{
	bool bResult=false;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil){
			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			return FALSE;
		}
	do{		
		//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		LoginInfoValue serverInfoValue = EvnironmentData ::getServerProperties ();
		ptrIAppFramework->LOGINMngr_setServerInfoValue(serverInfoValue);

		if(clientName.NumUTF16TextChars() == 0)
		{
			bResult=ptrIAppFramework->callLoginFirstStep(serverInfoValue.getCmurl(), username,password,vectorClientModelPtr, errorMsg);
		}
		else
		{
			PMString sessionId("");
			bResult=ptrIAppFramework->callLogin(serverInfoValue.getCmurl(), username,password,clientName,sessionId);			

		}
		
		
		if(bResult==true){
			//CAlert::InformationAlert("Successfully connected to " + "Env Name ");
//22-feb
            PMString asd = serverInfoValue.getEnvName();
			this->getCurrentServerInfo(asd, serverInfoValue );


			if(clientName.NumUTF16TextChars() > 0)
			{
				ClientInfoValue clientInfoObj = serverInfoValue.getClientInfoByClientNo(clientName);

				if(clientInfoObj.getClientno() == "")
				{
					ClientInfoValue clientInfo;
					clientInfo.setClientno(clientName);
					serverInfoValue.setClientInfoValue(clientInfo);
					double clinetId = clientName.GetAsDouble();
					editServerInfo(serverInfoValue, clinetId);
				}
				ptrIAppFramework ->setLoggerFileFlag(clientInfoObj.getLogLevel ());
				ptrIAppFramework ->setAssetServerOption (clientInfoObj.getAssetStatus ());
				ptrIAppFramework ->setMissingFlag(clientInfoObj.getMissingFlag());

				
			}
//22-feb		
			EvnironmentData::setServerStatus(-1);
			break;
		}
		if(bResult!=true){
			//ptrIAppFramework->UTILITYMngr_dispErrorMessage();			
			EvnironmentData::setServerStatus(-1);
			break;
		}
	}while(0);
	
	return (bool16)bResult;
}

bool16 LoginHelper::isValidServer(PMString &envName)
{
	bool16 bResult=-1;
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil){
			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			return FALSE;
		}
	do{	 
		LoginInfoValue currentServerInfo;
		if(getCurrentServerInfo(envName,currentServerInfo)==-1)
		{ 
			//CAlert::InformationAlert("2.2.");
			break;
		}

		//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
				
		ptrIAppFramework->LOGINMngr_setServerInfoValue(currentServerInfo);//added by Tushar on 09_03_07

		//if(EvnironmentData::SelctedConnMode == 0)
		//	bResult=(bool16)ptrIAppFramework->LOGINMngr_isValidServerURL(currentServerInfo.getCmurl()/*,currentServerInfo.getServerPort()*/);
		//else if(EvnironmentData::SelctedConnMode == 1)
		//{
		//	bResult=(bool16)ptrIAppFramework->LOGINMngr_isValidServer(currentServerInfo.getServerName(),currentServerInfo.getServerPort());
		//}
		//if(bResult==TRUE) 
		//	break;
		// 
		//if(bResult!=TRUE){
		//	break;
		//}
		
	}while(0);

	return bResult;
}

bool16 LoginHelper::setCurrentServer(PMString &envName)
{
	bool16 bResult=-1;
	do{	
		LoginInfoValue currentServerInfo;
		if(getCurrentServerInfo(envName,currentServerInfo)==-1)
			return bResult;

		//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil){
			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			break;
		}

		PMString fPath=EvnironmentData::getAvailableServerFilePath();
		PMString fName=EvnironmentData::getAvailableServerFileName();
		//CA("Before LOGINMngr_setDBServer");
		//bResult=(bool16)ptrIAppFramework->LOGINMngr_setDBServer(currentServerInfo,fPath.GrabCString(),fName.GrabCString());//Cs3
		//bResult=(bool16)ptrIAppFramework->LOGINMngr_setDBServer(currentServerInfo,const_cast<char*>(fPath.GetPlatformString().c_str()),const_cast<char*>(fName.GetPlatformString().c_str()));//Cs4
		//if(bResult==TRUE){
		//	//CAlert::InformationAlert("Connected to Server...");
		//	bResult=TRUE;
		//	break;
		//}
		//if(bResult!=TRUE){
		//	//ptrIAppFramework->UTILITYMngr_dispErrorMessage();			
		//	break;
		//}
	}while(0);
	return bResult;
}

bool16 LoginHelper :: getCurrentServerInfo(LoginInfoValue& serverInfoVal)
{
	serverInfoVal = EvnironmentData::getServerProperties();
	
	/*if(serverInfoVal.getdbConnectionMode() == -1)
		return kFalse ;
	else*/
		return kTrue ;
}

void LoginHelper :: setEditedServerInfo(LoginInfoValue serverInfoVal)
{
	EvnironmentData::setSelectedServerProperties(serverInfoVal);
}

void LoginHelper::callfireResetPlugins(){
	LNGActionComponent lngActionComponentObj(this);
	lngActionComponentObj.fireResetPlugins();
}

ClientInfoValue LoginHelper::getCurrentClientInfoValue(void)
{
	ClientInfoValue clientInfoObj;
	LoginInfoValue serverInfoVal = EvnironmentData::getServerProperties();
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil){
		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return clientInfoObj;
	}

	double clientID = ptrIAppFramework->getClientID();
	PMString ClientNo("");
	ClientNo.AppendNumber(PMReal(clientID));
	clientInfoObj =  serverInfoVal.getClientInfoByClientNo(ClientNo);

	return clientInfoObj;
}