#include "VCPlugInHeaders.h"
#include "ISession.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
#include "IActionStateList.h"
#include "SDKUtilities.h"
#include "CActionComponent.h"
#include "CAlert.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "LNGID.h"
#include "dataStore.h"
//#include "IAppFramework.h"
#include "WBID.h"
#include "IWBDialogCloser.h"
#include "TPLID.h"
#include "ITemplatesDialogCloser.h"
#include "PspID.h"
#include "IPGSDialogCloser.h"
#include "RsrcSpec.h"
#include "LaunchpadActionComponent.h"
#include "LNGMediator.h"
#include "ILoginEvent.h"
#include "LNGPlugInEntrypoint.h"
#include "ISprayer.h"
#include "ApplicationFrameHeader.h"


#define CA(X) CAlert::InformationAlert(X)

CREATE_PMINTERFACE(LNGActionComponent, kLNGLaunchpadDlgActionCompImpl)

IDialog* LNGActionComponent::curDlgPtr=0;

LNGActionComponent::LNGActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{
}

LNGActionComponent::~LNGActionComponent()
{
}
IDialog* LNGActionComponent::getDlgPtr(void)
{
	return curDlgPtr;
}

void LNGActionComponent::DoAction(ActionID actionID)
{
	switch (actionID.Get())
	{
		case kLNGAboutActionID:
			{	 

			this->DoAbout();
			break;
		}	

		case kLNGDialogActionID:
			{	 
//CA("11");
			//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			AppFramework* ptrIAppFramework = new AppFramework();
			if(ptrIAppFramework == nil){
				CAlert::InformationAlert("Pointer to IAppFramework is nil.");
				break;
			} 
//CA("12");			
			bool16 status = (bool16)ptrIAppFramework->LOGINMngr_getLoginStatus(Indesign);
//CA("13");
			if(status != kTrue)
			{	  
				this->DoDialog(1); // opening the Login Dialog			
			}
			else
				showLogoutPrintDialog();
//CA("14");
			if(ptrIAppFramework)
			delete ptrIAppFramework;
			break;

			
		}			
		default:
		{
			break;
		}
	}
}

//void LNGActionComponent::UpdateActionStates(IActionStateList* actionState)
//{
//	
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil){
//		CAlert::InformationAlert("Pointer to IAppFramework is nil.");		
//		return;
//	}
//	
//	bool16 status = ptrIAppFramework->LOGINMngr_getLoginStatus();
//	actionState->SetNthActionState(0,1);
//	if(status == kTrue)
//	{
//		actionState->SetNthActionName(0,"&Logout");		
//	}
//	else
//		actionState->SetNthActionName(0,"&Login");
//}

void LNGActionComponent::UpdateActionStates(IActiveContext* ac, IActionStateList* listToUpdate, GSysPoint mousePoint, IPMUnknown* widget)
{
	if (ac == nil)
	{
		ASSERT(ac);
		return;
	}

	for(int32 iter = 0; iter < listToUpdate->Length(); iter++) 
	{
		ActionID actionID = listToUpdate->GetNthAction(iter);
		if (actionID.Get() == kLNGDialogActionID) 
		{
			//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			AppFramework* ptrIAppFramework = new AppFramework();
			if(ptrIAppFramework == nil)
				return;
		//	CA("Checking inside Updateaction States");
			bool16 result=(bool16)ptrIAppFramework->LOGINMngr_getLoginStatus(Indesign);
		
				listToUpdate->SetNthActionState(iter, kEnabledAction);
			
			
				if(result == kTrue)
				{	//CA("Login is true");
					listToUpdate->SetNthActionName(iter,"&Logout");		
				}
				else
				{	//CA("Login is false");
					listToUpdate->SetNthActionName(iter,"&Login");
				}
			if(ptrIAppFramework)
				delete ptrIAppFramework;
		}
	}
}

void LNGActionComponent::DoAbout()
{
	SDKUtilities::InvokePlugInAboutBox(kLNGAboutBoxStringKey);
}

void LNGActionComponent::DoDialog(int dlgID)
{//CA("Inside DO DIalog");
	int id=kLaunchpadDialogResourceID;
	do
	{
		InterfacePtr<IApplication> application(gSession->QueryApplication());
		if (application == nil)
		{
			ASSERT_FAIL("LNGActionComponent::DoAction: application invalid");
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		if (dialogMgr == nil)
		{ 
			ASSERT_FAIL("LNGActionComponent::DoAction: dialogMgr invalid"); 
			break;
		}
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		//CAlert::InformationAlert("1");
		if(dlgID==2)		 
		id=kLoginDialogRsrcID; 
		 
		//deleted byalok
		 else if(dlgID==3)
			id=kProp2DialogRsrcID; 
		////added by alok
		 else if (dlgID==4) 			  
			 id =kProp2DialogRsrcID; 
		//CA("dlgID=4");}
		//////////////////
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kLNGPluginID,				// Our Plug-in ID from LNGID.h. 
			kViewRsrcType,				// This is the kViewRsrcType.
			id,							// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
	 
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		if (dialog == nil)
		{ 
			ASSERT_FAIL("LNGActionComponent::DoAction: can't create dialog"); 
			break;
		}	

		curDlgPtr = dialog;

	//	CA("before dialog->open");
		dialog->Open();		
	} while(false);			
}

/* call this function on Logout to close all dialogs and palettes */
void LNGActionComponent::showLogoutPrintDialog(void)
{	
	bool8 WBdlgOpenFlag=kFalse,TmpldlgOpenFlag=kFalse,PGSdlgOpenFlag=kFalse;
	do
	{	
//		InterfacePtr<ITemplatesDialogCloser> TmpldlgCloser((ITemplatesDialogCloser*) ::CreateObject(kTemplatesDialogCloserBoss,IID_ITEMPLATEDIALOGCLOSER));
//		if(TmpldlgCloser == nil){
			//CAlert::InformationAlert("Fail to get ITemplatesDialogCloser interface....");
			//break;
//		}
	//	TmpldlgOpenFlag = TmpldlgCloser->IsDialogOpen();

//		InterfacePtr<IPGSDialogCloser> PGSdlgCloser((IPGSDialogCloser*) ::CreateObject(kPGSDialogCloserBoss,IID_IPGSDIALOGCLOSER));
//		if(PGSdlgCloser == nil){
			//CAlert::InformationAlert("Fail to get IPGSDialogCloser interface....");
			//break;
//		}
		//PGSdlgOpenFlag = PGSdlgCloser->IsDialogOpen();
//		InterfacePtr<IWBDialogCloser> WBdlgCloser((IWBDialogCloser*) ::CreateObject(kWBDialogCloserBoss,IID_IWBDIALOGCLOSER));
//		if(WBdlgCloser == nil){
			//CAlert::InformationAlert("Fail to get IWBDialogCloser interface....");
			//break;
//		}
		//WBdlgOpenFlag = WBdlgCloser->IsDialogOpen();
//		InterfacePtr<ISprayer> iPageSprayer((ISprayer*)::CreateObject(kSPRDialogBoss,IID_ISPRAYER));	
//		if(iPageSprayer == nil)	{ //CA("Sprayer not Detected");
		//break;
//		}
		//if(TmpldlgOpenFlag || PGSdlgOpenFlag || WBdlgOpenFlag)		
//		{
			PMString message("You will be logged out of PRINTsource. This will close all the active PRINTsource dialogs/palettes. Do you still want to continue?   ");
			PMString ok_btn("OK"); //1
			PMString cancel_btn("Cancel");//2
			int32 isLogout= CAlert::ModalAlert(message,ok_btn,cancel_btn,"",2,CAlert::eQuestionIcon);
			if(isLogout==1)
			{
				//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
				AppFramework* ptrIAppFramework = new AppFramework();
				if(ptrIAppFramework == nil)
				{
					CAlert::InformationAlert("Pointer to IAppFramework is nil.");
					break;
				}
				ptrIAppFramework->LOGINMngr_logoutCurrentUser(Indesign);
					fireUserLoggedOut();

//					if(TmpldlgCloser)
//					TmpldlgCloser->closeTemplDialog();
//				if(PGSdlgCloser)
//					PGSdlgCloser->closePGSDialog();
				
//				if(WBdlgCloser)
//				WBdlgCloser->closeWBDialog();


//				if(iPageSprayer)
//				iPageSprayer->CloseSprayerDialog();

				if(ptrIAppFramework)
				delete ptrIAppFramework;
			}
//	}
	}while(kFalse);
}

void LNGActionComponent::fireUserLoggedOut(void)
{	
	vector<int32 > observers=getMediators()->getObservers();
	vector<int32 >::iterator it;
	
	for(it=observers.begin();it!=observers.end();it++)
	{	
		int32 i = *it;
		InterfacePtr<ILoginEvent> evt
			((ILoginEvent*) ::CreateObject(i,IID_ILOGINEVENT));
		if(!evt)
		{
			//CA("Invalid logEvtHndler");
			continue;
		}		
		evt->userLoggedOut();
		
	}
}

