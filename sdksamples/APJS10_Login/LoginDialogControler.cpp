#include "VCPlugInHeaders.h"

//#include "LoginDialogController.h"
#include "CDialogController.h"
#include "SystemUtils.h"
//#include "IAppFramework.h"
#include "CAlert.h"
#include "LNGID.h"
#include "dataStore.h"
#include "LNGMediator.h"
#include "LNGPlugInEntrypoint.h"
#include "ILoginEvent.h"
//8-march
#include "IPanelControlData.h"
#include "IControlView.h"
//#include "listBoxHelperFuncs.h"
#include "LoginHelper.h"
#include "ICounterControlData.h"
#include "ITextControlData.h"
#include "dataStore.h"
#include "time.h"
#include "ProgressBar.h"

#include "IStringListControlData.h"
#include "IDropDownListController.h"

#include "LNGTreeDataCache.h"
#include "ITreeViewMgr.h"
#include "LNGTreeModel.h"
#include "AcquireModalCursor.h"
#include "IClientOptions.h"

//8-march
//12-march
extern bool16 isValidServerStatus;
PMString logTxt;//(kBlankStringTextKey);
//WideString logTxt; //((WideString)kBlankStringTextKey);
bool16 bResult;
PMString UserNameStr;
PMString PasswordStr;

VectorClientModelPtr vectorClientModelPtr = NULL;

extern int32 displayEnvList ;
extern PMString selectedClientName;
extern double selectedClientId;

//12-march
class LNG2DialogController : public CDialogController
{
	public:
		LNG2DialogController(IPMUnknown* boss) : CDialogController(boss) {}
		virtual ~LNG2DialogController() {}
		virtual void InitializeDialogFields(IActiveContext*); 

		virtual WidgetID ValidateDialogFields(IActiveContext*);
		virtual void ApplyDialogFields(IActiveContext*, const WidgetID&);

		void fireUserLoggedIn();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(LNG2DialogController, kLNGLoginDlgControllerImpl)

/* ApplyFields
*/
void LNG2DialogController::InitializeDialogFields(IActiveContext* myContext) 
{	
	//CA("Inside  LNG2DialogController::InitializeDialogFields");
	CDialogController::InitializeDialogFields(myContext);	
	PMString blankString("");
	blankString.SetTranslatable(kFalse);
	SetTextControlData(kLoginUserNameTextEditWidgetID, blankString);
	SetTextControlData(kLoginPasswordTextEditWidgetID, blankString);
	// Put code to initialize widget values here.

	LoginHelper lngHelperObj(this);
	LoginInfoValue currentServerInfo;

//version7 login chage starts here
	
	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil)
		return ;

	EvnironmentData::setLoginPanel(panelControlData);

	IControlView* iLoginUserNameStaticTextWidgetControlView = panelControlData->FindWidget(kLoginUserNameStaticTextWidgetID);
	if (iLoginUserNameStaticTextWidgetControlView == nil)
		return ;

	iLoginUserNameStaticTextWidgetControlView->Enable();
	iLoginUserNameStaticTextWidgetControlView->Invalidate();


	IControlView* iLoginUserNameTextEditWidgetControlView = panelControlData->FindWidget(kLoginUserNameTextEditWidgetID);
	if (iLoginUserNameTextEditWidgetControlView == nil)
		return ;

	iLoginUserNameTextEditWidgetControlView->Enable();
	iLoginUserNameTextEditWidgetControlView->Invalidate();



	IControlView* iLoginPasswordStaticTextWidgetControlView = panelControlData->FindWidget(kLoginPasswordStaticTextWidgetID);
	if (iLoginPasswordStaticTextWidgetControlView == nil)
		return ;

	iLoginPasswordStaticTextWidgetControlView->Enable();
	iLoginPasswordStaticTextWidgetControlView->Invalidate();

	IControlView* iLoginPasswordTextEditWidgetControlView = panelControlData->FindWidget(kLoginPasswordTextEditWidgetID);
	if (iLoginPasswordTextEditWidgetControlView == nil)
		return ;

	iLoginPasswordTextEditWidgetControlView->Enable();
	iLoginPasswordTextEditWidgetControlView->Invalidate();



	IControlView* iHostDropDownWidgetControlView = panelControlData->FindWidget(kHostDropDownWidgetID);
	if (iHostDropDownWidgetControlView == nil)
		return ;

	iHostDropDownWidgetControlView->Enable();
	iHostDropDownWidgetControlView->Invalidate();

	IControlView* iLoginSelectaHostStaticTextControlView = panelControlData->FindWidget(kLoginSelectaHostStaticTextWidgetID);
	if (iLoginSelectaHostStaticTextControlView == nil)
		return ;

	iLoginSelectaHostStaticTextControlView->Enable();
	iLoginSelectaHostStaticTextControlView->Invalidate();

	IControlView* iLoginButtonWidgetControlView = panelControlData->FindWidget(kLoginButtonWidgetID);
	if (iLoginButtonWidgetControlView == nil)
		return ;

	iLoginButtonWidgetControlView->Enable();
	iLoginButtonWidgetControlView->Invalidate();

	IControlView* iSecondCancelButtonWidgetControlView = panelControlData->FindWidget(kSecondCancelButtonWidgetID);
	if (iSecondCancelButtonWidgetControlView == nil)
		return ;

	iSecondCancelButtonWidgetControlView->Enable();
	iSecondCancelButtonWidgetControlView->Invalidate();

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil){
		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return;
	}

	InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
	if(dialogController == nil)
	{
		CA("dialogController == nil");
		return;
	}

	InterfacePtr<IStringListControlData> HostDropListData(
	dialogController->QueryListControlDataInterface(kHostDropDownWidgetID));
	if(HostDropListData == nil)
	{
		//CA("PubDropListData nil");
		return;
	}
	HostDropListData->Clear(kFalse,kFalse);	

	InterfacePtr<IDropDownListController> HostDropListController(HostDropListData,UseDefaultIID());
	if (HostDropListController == nil)
	{
		//CA("PubDropListController nil");
		return;
	}

	LoginHelper LoginHelperObj(this);

	VectorLoginInfoPtr result=NULL;
	result=	LoginHelperObj.getAvailableServers();
	if(result== NULL){
		iLoginButtonWidgetControlView->Disable();
		//CAlert::InformationAlert(" Please Create New Environment and Server name/URL Property. "); //AvailableServers File not found...
		return;
	}
	if(result->size()==0){
		//CAlert::InformationAlert("Available Servers File show zero entries.");
        //CAlert::InformationAlert(" Please Create New Environment and Server name/URL Property. ");
		return;
	}	

	int32 dropDownListRowIndex = 0;
	int32 selectDropDownListRow = -1;
	VectorLoginInfoValue::iterator it;
	for(it = result->begin(); it != result->end(); it++)
	{
		currentServerInfo = *it;
		HostDropListData->AddString((currentServerInfo.getCmurl()).SetTranslatable(kFalse), dropDownListRowIndex, kFalse, kFalse);

		if(currentServerInfo.getServerPort() == 1)//if(dropDownListRowIndex == 0)
		{
			selectDropDownListRow = dropDownListRowIndex;
			EvnironmentData::setSelectedserver(dropDownListRowIndex);
            PMString asd = currentServerInfo.getEnvName();
			EvnironmentData::setSelectedServerEnvName(asd);
			EvnironmentData::SelctedConnMode = 0; //currentServerInfo.getdbConnectionMode();
			EvnironmentData::setSelectedServerProperties(currentServerInfo);
		}
		dropDownListRowIndex++;
		
	}
	if(selectDropDownListRow == -1)
	{
		it = result->begin();
		currentServerInfo = *it;

		EvnironmentData::setSelectedserver(0);
        PMString asd = currentServerInfo.getEnvName();
		EvnironmentData::setSelectedServerEnvName(asd);
		EvnironmentData::SelctedConnMode = 0; //currentServerInfo.getdbConnectionMode();
		EvnironmentData::setSelectedServerProperties(currentServerInfo);

		selectDropDownListRow = 0;

	}

	HostDropListController->Select(selectDropDownListRow);
	delete result;
	
	PMString envName;
	envName = EvnironmentData::getSelectedServerEnvName();
	//CA("envName = " + envName);
	/*bool16 bStatus */isValidServerStatus = kTrue; //lngHelperObj.isValidServer(envName);

	//if(EvnironmentData::SelctedConnMode == 1)
	//{	//CA("Server Connection Selected");
	//	//ptrIAppFramework->setSelectedConnMode(1); // for Server	

	//	bool16 bStatus = lngHelperObj.setCurrentServer(envName);			
	//	if(bStatus  != TRUE){
	//		CAlert::InformationAlert("Failed to set Current Server... ");
	//		//PMString str("Please select an environment to connect to.");
	//		//changeStatusText(str,TRUE);
	//		return;
	//	}  
	//}
	//if(EvnironmentData::SelctedConnMode == 0)
	//{
	//	ptrIAppFramework->setSelectedConnMode(0); // for Http	
	//	//CA("Http Connection Selected");
	//}

	PMString userName = EvnironmentData::getServerProperties().getUsername();
	IControlView * userNameView = panelControlData->FindWidget(kLoginUserNameTextEditWidgetID);
	if(userNameView == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RegisterPubToONESource::RPTOSDialogObserver::bookInfo::userNameView == nil");
		return ;
	}
	InterfacePtr<ITextControlData> iTextControlData1(userNameView,UseDefaultIID());
	if(iTextControlData1 == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RegisterPubToONESource::RPTOSDialogObserver::bookInfo::iTextControlData1 == nil");
		return ;
	}		
	//CA("userName : "+ userName);
	userName.SetTranslatable(kFalse);
    userName.ParseForEmbeddedCharacters();
	iTextControlData1->SetString(userName);

	PMString password = EvnironmentData::getServerProperties().getPassword();/*getdbAppenderForPassword();*/
	IControlView * passwordView = panelControlData->FindWidget(kLoginPasswordTextEditWidgetID);
	if(passwordView == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RegisterPubToONESource::RPTOSDialogObserver::bookInfo::passwordView == nil");
		return ;
	}
	InterfacePtr<ITextControlData> iTextControlData2(passwordView,UseDefaultIID());
	if(iTextControlData2 == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RegisterPubToONESource::RPTOSDialogObserver::bookInfo::iTextControlData2 == nil");
		return ;
	}		
	//CA("password : "+ password);
	password.SetTranslatable(kFalse);
    password.ParseForEmbeddedCharacters();
	iTextControlData2->SetString(password);
//version7 login chage ends here
	

	//8-march
	IControlView* TreePanelCtrlView = panelControlData->FindWidget(kLNGTreeViewWidgetID);
	if (TreePanelCtrlView == nil)
	{
		//CA("TreePanelCtrlView is nil");
		return;
	};

	TreePanelCtrlView->HideView();

	IControlView* iMultilineTextExpander1ControlView = panelControlData->FindWidget(kLNGMultilineTextExpander1WidgetID);
	if (iMultilineTextExpander1ControlView == nil)
		return;

	iMultilineTextExpander1ControlView->HideView();

	IControlView* iMultilineTextExpander2ControlView = panelControlData->FindWidget(kLNGMultilineTextExpander2WidgetID);
	if (iMultilineTextExpander2ControlView == nil)
		return;

	iMultilineTextExpander2ControlView->HideView();

	IControlView* iScrollBarWidgetControlView = panelControlData->FindWidget(kScrollBarWidgetID);
	if (iScrollBarWidgetControlView == nil)
		return;

	iScrollBarWidgetControlView->ShowView();

	IControlView* iContinueButtonWidgetControlView = panelControlData->FindWidget(kContinueButtonWidgetID);
	if (iContinueButtonWidgetControlView == nil)
		return;
	iContinueButtonWidgetControlView->Disable();
	iContinueButtonWidgetControlView->HideView();

	IControlView* iCancelButtonWidgetControlView = panelControlData->FindWidget(kCancelButton1WidgetID);
	if (iCancelButtonWidgetControlView == nil)
		return;

	iCancelButtonWidgetControlView->HideView();

	IControlView* iOptionsButtonWidgetControlView = panelControlData->FindWidget(kOptionsButtonWidgetID);
	if (iOptionsButtonWidgetControlView == nil)
		return;

	iOptionsButtonWidgetControlView->ShowView();

	

	IControlView* iMultilineTextExpanderControlView = panelControlData->FindWidget(kLNGMultilineTextExpanderWidgetID);
	if (iMultilineTextExpanderControlView == nil)
		return;

	iMultilineTextExpanderControlView->ShowView();

	InterfacePtr<ITextControlData> iTextControlData (iMultilineTextExpanderControlView,UseDefaultIID());
	if (iTextControlData == nil) 
		return;

	IControlView* iScrollBarControlView = panelControlData->FindWidget(kScrollBarWidgetID);
	if (iScrollBarControlView == nil)
		return;

	InterfacePtr<ICounterControlData> counterData(iScrollBarControlView, UseDefaultIID());
	if(counterData==nil) 	
		return;
//12-march

	

	IControlView * iOkButtonCntrlView = panelControlData->FindWidget(kLoginButtonWidgetID/*kOKButtonWidgetID*/);
	if(iOkButtonCntrlView == nil)
	{
		CA("iOkButtonCntrlView == nil");
		return;
	}

	iOkButtonCntrlView->Enable();

	IControlView * iOkButtonCntrlView1 = panelControlData->FindWidget(kOKButtonWidgetID);
	if(iOkButtonCntrlView1 == nil)
	{
		CA("iOkButtonCntrlView == nil");
		return;
	}

	iOkButtonCntrlView1->Disable();
//12-march

	
	//listBoxHelper listHelper(this, kLNGPluginID);

	
	
	//isValidServerStatus = lngHelperObj.isValidServer(envName);

	bool16 bStatus = lngHelperObj.getCurrentServerInfo(envName,currentServerInfo);
	logTxt.Clear();
	PMString blank("  ");
	blank.SetTranslatable(kFalse);
	logTxt.Append("\n");
	logTxt.Append(blank); //(blank);
	//WideString attempting(kAttemptingConnectionStaticTextKey);
	logTxt.Append("<-- Attempting Connection to -->  \n"); //(attempting);// 
	//14-march
	logTxt.Append((WideString)ptrIAppFramework ->GetTimeStamp ());
	logTxt.Append(blank); //((WideString)kBlankStringTextKey); //
	//14-march
	WideString envNamee ("Enviroment Name : ");//(kEnviornmentNameTextKey);
	 envNamee.Append ((WideString)currentServerInfo.getEnvName ());

	logTxt.Append ((WideString)envNamee);
	logTxt.Append("\n");
	//logTxt.Append (blank);// (blank); //

	//logTxt.Append ((WideString)kModeTextKey); //("Mode : ");
	int32 mode = 0; //currentServerInfo .getdbConnectionMode ();
	switch(mode)
	{
	case 0: ;//logTxt.Append ((WideString)kHTTPTextKey); break;
		case 1:; //logTxt.Append ((WideString)kJDBCTextKey);  break;
	}
	//logTxt.Append(blank);// ("  ");

	//if(currentServerInfo.getdbConnectionMode() == 0)  // if Properties are for HTTP mode
	{
		logTxt.Append ("Server URL : "); //((WideString)kServerUrlTextKey);//
		logTxt.Append ((WideString)currentServerInfo.getCmurl ());
		logTxt.Append (blank); // ((WideString)kBlankStringTextKey);//
		logTxt.Append("\n");

		if(currentServerInfo.getVendorName () == kUseProxyStaticKey)//"Use_Proxy")
		{
			logTxt.Append((WideString)"Proxy settings :");
			logTxt.Append(blank);
			
			logTxt.Append((WideString)"Protocol : ");
			logTxt.Append((WideString)currentServerInfo.getVersion());
			logTxt.Append(blank);

			logTxt.Append((WideString)"Host server : ");
			logTxt .Append((WideString)currentServerInfo.getServerName());
			logTxt.Append(blank);

			logTxt.Append((WideString)"Port : ");
			logTxt.AppendNumber(currentServerInfo.getServerPort()); //got a error
			logTxt.Append(blank);

			logTxt.Append((WideString)"User Name : ");
			logTxt.Append((WideString)currentServerInfo.getDbUserName());
			logTxt.Append(blank);
			logTxt.Append("\n");

		}
	}

	//logTxt.Append ("Asset Server : ");
	//if(currentServerInfo.getAssetStatus())
	///	logTxt.Append("Asset Server same as Image Working Directory.");
	//else
	//	logTxt.Append("Asset Server is not same as Image Working Directory.");
	
	//13 sep added by nitin

	//logTxt.Append ("Images : ");
	//if(currentServerInfo.getAssetStatus())
	//{
	//	logTxt.Append("Map To Repository : ");
	//	logTxt.Append(currentServerInfo.getImagePath());
	//}
//	else
//	{
		//logTxt.Append("Download Images To : ");
		//logTxt.Append(currentServerInfo.getImagePath());
		
//	}
//	logTxt.Append("  ");
//	logTxt.Append ("Missing Flag : ");
//	if(currentServerInfo.getMissingFlag())
//		logTxt.Append((WideString)"True");
//	else
//		logTxt.Append((WideString)"False");
	
	//upto here
//	logTxt.Append("  ");
//	logTxt.Append ("Debug Level : ");
//	int32 debugLevel = currentServerInfo.getLogLevel();
//	switch(debugLevel)
//	{
//		case 0:;//logTxt.Append ((WideString)"Error"); break;
//		case 1:; //logTxt.Append ((WideString)"Fatal"); break;
//		case 2:; //logTxt.Append ((WideString)"Debug"); break;
//		case 3:; //logTxt.Append ((WideString)"Info");  break;
//	}

//	logTxt.Append ("    ");
	logTxt.SetTranslatable(kFalse);
	ptrIAppFramework->LogDebug(logTxt);
//12-march
	if(isValidServerStatus)
	{
	}
	else
	{
		iOkButtonCntrlView->Disable();
		iOkButtonCntrlView1->Disable();
		logTxt.Append(ptrIAppFramework ->GetTimeStamp ());
		logTxt.Append (blank);
		PMString loginErrorString ; //= ptrIAppFramework->getLoginErrorString();
		if(loginErrorString.NumUTF16TextChars() == 0)
		{
			loginErrorString = "Some unknwon error occured. Please contact your System Administrator!" ;
		}
		ptrIAppFramework->LogError(loginErrorString);
		logTxt.Append (loginErrorString );
		logTxt.Append (blank);
		logTxt.Append("\n");
	}

//12-march
	int32 minValue = counterData->GetMinimum();	
	counterData->SetValue(minValue);
	logTxt.SetTranslatable(kFalse);
    logTxt.ParseForEmbeddedCharacters();
	iTextControlData->SetString(logTxt);
	int32 maxValue = counterData->GetMaximum();	
	counterData->SetValue(maxValue);

	int32 minVaalue = counterData->GetMinimum();	
	counterData->SetValue(minVaalue);

	iMultilineTextExpanderControlView->Invalidate();
	iScrollBarControlView->Invalidate();
//8-march


}

/* ValidateFields
*/
WidgetID LNG2DialogController::ValidateDialogFields(IActiveContext* myContext)
{ 
	//CA("LNG2DialogController::ValidateDialogFields");
	WidgetID result; // = CDialogController::ValidateDialogFields(myContext);
	// Put code to validate widget values here.
	PMString username(kBlankStringTextKey);
	PMString password(kBlankStringTextKey);
	username=GetTextControlData(kLoginUserNameTextEditWidgetID);
	password=GetTextControlData(kLoginPasswordTextEditWidgetID);

	//CA("username = " + username + ", password = " + password);
	if(username=="")
	{//CA("username is nil");
		return kLoginUserNameTextEditWidgetID;
	}
	else if(password=="")
	{//CA("password is nil");
		return kLoginPasswordTextEditWidgetID;
	}

//13-march
	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil)
		return result;

	IControlView* iMultilineTextExpanderControlView = panelControlData->FindWidget(kLNGMultilineTextExpanderWidgetID);
	if (iMultilineTextExpanderControlView == nil)
		return result;

	InterfacePtr<ITextControlData> iTextControlData (iMultilineTextExpanderControlView,UseDefaultIID());
	if (iTextControlData == nil) 
		return result;

	IControlView* iScrollBarControlView = panelControlData->FindWidget(kScrollBarWidgetID);
	if (iScrollBarControlView == nil)
		return result;

	InterfacePtr<ICounterControlData> counterData(iScrollBarControlView, UseDefaultIID());
	if(counterData==nil) 	
		return  result;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil){
		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return result;
	}
//13-march	
	do{	
		LoginHelper LngHelpObj(this);
		EvnironmentData::setServerStatus(-1);
	
		UserNameStr = "";
		PasswordStr = "";
		PMString clientName("");
		PMString errorMsg("");
				
		vectorClientModelPtr = new VectorClientModel;
		bool16 bResult1=LngHelpObj.loginUser(username,password,clientName,vectorClientModelPtr, errorMsg);
		int32 size =0;
		
		if(vectorClientModelPtr != nil)
		{
			size = static_cast<int32>(vectorClientModelPtr->size());
			UserNameStr = username;
			PasswordStr = password;

			if(size > 1)
			{	
				

				IControlView* iLoginUserNameStaticTextWidgetControlView = panelControlData->FindWidget(kLoginUserNameStaticTextWidgetID);
				if (iLoginUserNameStaticTextWidgetControlView == nil)
					return result;

				iLoginUserNameStaticTextWidgetControlView->Disable();
				iLoginUserNameStaticTextWidgetControlView->Invalidate();


				IControlView* iLoginUserNameTextEditWidgetControlView = panelControlData->FindWidget(kLoginUserNameTextEditWidgetID);
				if (iLoginUserNameTextEditWidgetControlView == nil)
					return result;

				iLoginUserNameTextEditWidgetControlView->Disable();
				iLoginUserNameTextEditWidgetControlView->Invalidate();

				IControlView* iLoginPasswordStaticTextWidgetControlView = panelControlData->FindWidget(kLoginPasswordStaticTextWidgetID);
				if (iLoginPasswordStaticTextWidgetControlView == nil)
					return result;

				iLoginPasswordStaticTextWidgetControlView->Disable();
				iLoginPasswordStaticTextWidgetControlView->Invalidate();

				IControlView* iLoginPasswordTextEditWidgetControlView = panelControlData->FindWidget(kLoginPasswordTextEditWidgetID);
				if (iLoginPasswordTextEditWidgetControlView == nil)
					return result;

				iLoginPasswordTextEditWidgetControlView->Disable();
				iLoginPasswordTextEditWidgetControlView->Invalidate();

				IControlView* iHostDropDownWidgetControlView = panelControlData->FindWidget(kHostDropDownWidgetID);
				if (iHostDropDownWidgetControlView == nil)
					return result;

				iHostDropDownWidgetControlView->Disable();
				iHostDropDownWidgetControlView->Invalidate();

				IControlView* iLoginSelectaHostStaticTextControlView = panelControlData->FindWidget(kLoginSelectaHostStaticTextWidgetID);
				if (iLoginSelectaHostStaticTextControlView == nil)
					return result;

				iLoginSelectaHostStaticTextControlView->Disable();
				iLoginSelectaHostStaticTextControlView->Invalidate();

				IControlView* iLoginButtonWidgetControlView = panelControlData->FindWidget(kLoginButtonWidgetID);
				if (iLoginButtonWidgetControlView == nil)
					return result;

				iLoginButtonWidgetControlView->Disable();
				iLoginButtonWidgetControlView->Invalidate();

				IControlView* iSecondCancelButtonWidgetControlView = panelControlData->FindWidget(kSecondCancelButtonWidgetID);
				if (iSecondCancelButtonWidgetControlView == nil)
					return result;

				iSecondCancelButtonWidgetControlView->Disable();
				iSecondCancelButtonWidgetControlView->Invalidate();

				IControlView* iMultilineTextExpanderControlView = panelControlData->FindWidget(kLNGMultilineTextExpanderWidgetID);
				if (iMultilineTextExpanderControlView == nil)
					return result;

				iMultilineTextExpanderControlView->HideView();
				iMultilineTextExpanderControlView->Invalidate();

				IControlView* iScrollBarWidgetControlView = panelControlData->FindWidget(kScrollBarWidgetID);
				if (iScrollBarWidgetControlView == nil)
					return result;

				iScrollBarWidgetControlView->HideView();
				iScrollBarWidgetControlView->Invalidate();

				IControlView* iOptionsButtonWidgetControlView = panelControlData->FindWidget(kOptionsButtonWidgetID);
				if (iOptionsButtonWidgetControlView == nil)
					return result;

				iOptionsButtonWidgetControlView->HideView();
				iOptionsButtonWidgetControlView->Invalidate();

				IControlView* iMultilineTextExpander1ControlView = panelControlData->FindWidget(kLNGMultilineTextExpander1WidgetID);
				if (iMultilineTextExpander1ControlView == nil)
					return result;

				iMultilineTextExpander1ControlView->ShowView();	
				iMultilineTextExpander1ControlView->Invalidate();

				IControlView* iMultilineTextExpander2ControlView = panelControlData->FindWidget(kLNGMultilineTextExpander2WidgetID);
				if (iMultilineTextExpander2ControlView == nil)
					return result;

				iMultilineTextExpander2ControlView->ShowView();	
				iMultilineTextExpander2ControlView->Invalidate();

				IControlView* iContinueButtonWidgetControlView = panelControlData->FindWidget(kContinueButtonWidgetID);
				if (iContinueButtonWidgetControlView == nil)
					return result;

				iContinueButtonWidgetControlView->ShowView();
				iContinueButtonWidgetControlView->Enable();
				iContinueButtonWidgetControlView->Invalidate();

				IControlView* iCancelButtonWidgetControlView = panelControlData->FindWidget(kCancelButton1WidgetID);
				if (iCancelButtonWidgetControlView == nil)
					return result;

				iCancelButtonWidgetControlView->ShowView();
				iCancelButtonWidgetControlView->Invalidate();

				IControlView * iOkButtonCntrlView1 = panelControlData->FindWidget(kOKButtonWidgetID);
				if(iOkButtonCntrlView1 == nil)
				{
					CA("iOkButtonCntrlView == nil");
					return result;
				}

				iOkButtonCntrlView1->Enable();


				IControlView* TreePanelCtrlView = panelControlData->FindWidget(kLNGTreeViewWidgetID);
				if (TreePanelCtrlView == nil)
				{
					CA("TreePanelCtrlView is nil");
					break;
				};

				TreePanelCtrlView->ShowView();
			
				InterfacePtr<ITreeViewMgr> treeViewMgr(TreePanelCtrlView, UseDefaultIID());
				if(!treeViewMgr)
				{
					CA("treeViewMgr is nil");
					return result;
				}

				LNGTreeDataCache dc;
				dc.clearMap();
				
				LNGTreeModel treeModel;
				displayEnvList = 0;
				PMString pfName("100 Root");
				treeModel.setRoot(-1, pfName, 1);
				treeViewMgr->ClearTree(kTrue);
				treeModel.GetRootUID();
				
				treeViewMgr->ChangeRoot();				
				TreePanelCtrlView->Invalidate();

				return kContinueButtonWidgetID;
			}
			else if(size == 1)
			{
				
				AcquireWaitCursor awc;	
				awc.Animate();				

				//CA("selectedClientName = " + selectedClientName);
				PMString clientNo("");
				selectedClientId = vectorClientModelPtr->at(0).getClient_id();
				clientNo.AppendNumber(PMReal(vectorClientModelPtr->at(0).getClient_id()));
				selectedClientName = clientNo;
				
			
				bResult=LngHelpObj.loginUser(UserNameStr,PasswordStr,selectedClientName,vectorClientModelPtr, errorMsg);
			
				if(bResult==TRUE)
				{
					ptrIAppFramework->setClientID(selectedClientId);
					PMString envName = EvnironmentData::getSelectedServerEnvName();
					logTxt.Append ("    ");			
					logTxt.Append (ptrIAppFramework ->GetTimeStamp ());
					logTxt.Append (" ");
					logTxt.Append ("User ");
					logTxt.Append (UserNameStr );
					logTxt .Append (" connected successfully to " + envName + " !");
					logTxt .Append ("  ");
					//logTxt.Append ("    ");
					ptrIAppFramework->LogDebug( "User " + UserNameStr + " connected successfully to " + envName + " !");
							
					logTxt.Append (ptrIAppFramework ->GetTimeStamp ());
					logTxt.Append (" ");
					logTxt.Append ("Loading client caches ... ");
					logTxt.Append ("        ");

					ptrIAppFramework->setSelectedEnvName(envName);
					ptrIAppFramework->setClientID(selectedClientId);

					PMString ASD("clientId: ");
					ASD.AppendNumber(PMReal(selectedClientId));
					ptrIAppFramework->LogDebug(ASD);
					//CA("Successfully connected to " + envName + " !");								
					InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
					if (panelControlData == nil)
						return result;

					IControlView* iMultilineTextExpanderControlView = panelControlData->FindWidget(kLNGMultilineTextExpanderWidgetID);
					if (iMultilineTextExpanderControlView == nil)
						return result;

					InterfacePtr<ITextControlData> iTextControlData (iMultilineTextExpanderControlView,UseDefaultIID());
					if (iTextControlData == nil) 
						return result;

					IControlView* iScrollBarControlView = panelControlData->FindWidget(kScrollBarWidgetID);
					if (iScrollBarControlView == nil)
						return result;

					InterfacePtr<ICounterControlData> counterData(iScrollBarControlView, UseDefaultIID());
					if(counterData==nil) 	
						return  result;

					int32 minValue = counterData->GetMinimum();	
					counterData->SetValue(minValue);
					logTxt.SetTranslatable(kFalse);
                    logTxt.ParseForEmbeddedCharacters();
					iTextControlData->SetString(logTxt);
					int32 maxValue = counterData->GetMaximum();	
					counterData->SetValue(maxValue);

					//13-march
					/*RangeProgressBar progressBar("PRINTsource Login", 0, 1000, kFalse, kFalse);
					progressBar.SetTaskText("Connecting to " + envName );
					for(int32 i=0; i<1000; i++)
					{	
						progressBar.SetTaskText("Connecting to " + envName + "...");
						progressBar.SetPosition(i);
					}
					progressBar.Abort();	*/

					if(bResult)
					{							
						//ptrIAppFramework->loadAllCaches();

						counterData->SetValue(maxValue);
						//Sleep(1000);
					
						logTxt.Clear();				
					}
					
				
					//EvnironmentData::setServerStatus(1);
					//if(EvnironmentData::getServerStatus()==1)
					//{
					//	//CA("EvnironmentData::getServerStatus()==1");	
					//	InterfacePtr<IClientOptions> cop((IClientOptions*) ::CreateObject(kClientOptionsReaderBoss,IID_ICLIENTOPTIONS));
					//	if(cop != nil && bResult)
					//	{
					//		//CAlert::InformationAlert("Got the Options Interface");
					//		PMString name("");
					//		PMString LocaleName("");
					//		//CAlert::InformationAlert("-- calling get dufault locale and publication name --");
					//		cop->setdoRefreshFlag(kTrue);
					//		int32 localeID = cop->getDefaultLocale(LocaleName);
					//		if(localeID != -1){
					//			int32 pubID = cop->getDefPublication(name);
					//		}
					//		PMString imagePath = cop->getImageDownloadPath();
					//		//CAlert::InformationAlert(" --publication name-- "+name);
					//		//CAlert::InformationAlert(" --locale name-- "+LocaleName);
					//	}
					//	else{
					//		if(cop == nil)
					//			CAlert::InformationAlert(" Options plugin not available !!");
					//	}
					//}	
					//fireUserLoggedIn();			
				
					//iMultilineTextExpanderControlView->Invalidate();
					//iScrollBarControlView->Invalidate();

					int32 isEditServer=0;
					if(isEditServer == envName.Compare(TRUE,EvnironmentData::getSelectedServerEnvName()))
					{	
						LoginInfoValue serverProperties  = EvnironmentData::getServerProperties();
						serverProperties.setUsername(UserNameStr);
						//serverProperties.setdbAppenderForPassword(PasswordStr);
						serverProperties.setPassword(PasswordStr);
						//serverProperties.setServerPort(1);
						//int32 clientID = -1;
						if(LngHelpObj.editServerInfo(serverProperties,selectedClientId) == TRUE){
							EvnironmentData::setSelectedServerProperties(serverProperties);						
						}
					}				
					LoginInfoValue currentServerInfo;
				
					VectorLoginInfoPtr result1=NULL;
					result1=	LngHelpObj.getAvailableServers();
					if(result1== NULL){
						CAlert::InformationAlert(" Please Create New Environment and Server name/URL Properties. "); //AvailableServers File not found...
						return result;
					}
					if(result1->size()==0){
						//CAlert::InformationAlert("Available Servers File show zero entries.");
						return result;
					}	

					VectorLoginInfoValue::iterator it;
					for(it = result1->begin(); it != result1->end(); it++)
					{
						currentServerInfo = *it;						
						if(envName != currentServerInfo.getEnvName())
						{
							//CA("inside for = " + currentServerInfo.getEnvName());
							//currentServerInfo.setServerPort(0);
							//int32 clientID = -1;
							//if(LngHelpObj.editServerInfo(currentServerInfo,clientID) == TRUE){
							//	//CAlert::InformationAlert("edit existing server");
							//}						
						}														
					}
					delete result1;
					/*EvnironmentData::setServerStatus(1);
					return kDefaultWidgetId;	*/		
				}

			}
			//return kLoginUserNameTextEditWidgetID;
		}

		if(bResult==TRUE)
		{
			PMString envName = EvnironmentData::getSelectedServerEnvName();
			logTxt.Append ("    ");			
			logTxt.Append (ptrIAppFramework ->GetTimeStamp ());
			logTxt.Append (" ");
			logTxt.Append ("User ");
			logTxt.Append (username );
			logTxt .Append (" connected successfully to " + envName + " !");
			logTxt .Append ("  ");
			logTxt.Append ("    ");

			ptrIAppFramework->LogDebug( "User " + username + " connected successfully to " + envName + " !");
						
			//logTxt.Append (ptrIAppFramework ->GetTimeStamp ());
			//logTxt.Append (" ");
			//logTxt.Append ("Loading client caches ... ");
			//logTxt.Append ("        ");
			


			//CA("Successfully connected to " + envName + " !");
			
			int32 minValue = counterData->GetMinimum();	
			counterData->SetValue(minValue);
			logTxt.SetTranslatable(kFalse);
            logTxt.ParseForEmbeddedCharacters();
			iTextControlData->SetString(logTxt);
			int32 maxValue = counterData->GetMaximum();	
			counterData->SetValue(maxValue);
//13-march
			/*RangeProgressBar progressBar("PRINTsource Login", 0, 1000, kFalse, kFalse);
			progressBar.SetTaskText("Connecting to " + envName );
			for(int32 i=0; i<1000; i++)
			{	
				progressBar.SetTaskText("Connecting to " + envName + "...");
				progressBar.SetPosition(i);
			}
			progressBar.Abort();	*/
			
			EvnironmentData::setServerStatus(1);			
			//fireUserLoggedIn();			
			
			iMultilineTextExpanderControlView->Invalidate();
			iScrollBarControlView->Invalidate();

			//Version7 login change starts here
			int32 isEditServer=0;
			if(isEditServer == envName.Compare(TRUE,EvnironmentData::getSelectedServerEnvName()))
			{	
				//CA("setServerPort(1) " + password);
				LoginInfoValue serverProperties  = EvnironmentData::getServerProperties();
				/*serverProperties.setSchema(username);
				serverProperties.setdbAppenderForPassword(password);*/
				serverProperties.setUsername(username);
				serverProperties.setPassword(password);
				//serverProperties.setServerPort(1);
				double clientID = -1;
				if(LngHelpObj.editServerInfo(serverProperties,clientID) == TRUE){
					EvnironmentData::setSelectedServerProperties(serverProperties);
					//CAlert::InformationAlert("edit existing server");
				}

			}


			
			LoginInfoValue currentServerInfo;

			VectorLoginInfoPtr result1=NULL;
			result1=	LngHelpObj.getAvailableServers();
			if(result1== NULL){
				CAlert::InformationAlert(" Please Create New Environment and Server name/URL Properties. "); //AvailableServers File not found...
				return kCancelButton_WidgetID;
			}
			if(result1->size()==0){
				//CAlert::InformationAlert("Available Servers File show zero entries.");
				return kCancelButton_WidgetID;
			}	

			VectorLoginInfoValue::iterator it;
			for(it = result1->begin(); it != result1->end(); it++)
			{
				currentServerInfo = *it;	
				
				if(envName != currentServerInfo.getEnvName())
				{
					//CA("inside for = " + currentServerInfo.getEnvName());
					//currentServerInfo.setServerPort(0);
					//int32 clientID = -1;
					//if(LngHelpObj.editServerInfo(currentServerInfo,clientID) == TRUE){
						//CAlert::InformationAlert("edit existing server");
					//}
					
				}
				
			
				
			}
			delete result1;
			result1=NULL;
			//Version7 login change ends here
			return kDefaultWidgetId;
			
		}
		if(bResult1!=TRUE)
		{
			//SetTextControlData(kLoginUserNameTextEditWidgetID, "");
			//SetTextControlData(kLoginPasswordTextEditWidgetID, "");
			EvnironmentData::setServerStatus(-1);
			PMString blankString("  ");
			blankString.SetTranslatable(kFalse);
			//logTxt.Append("\n");
			//logTxt.Append (blankString);
			logTxt.Append (ptrIAppFramework ->GetTimeStamp ());
			logTxt.Append (" ");
			PMString loginErrorString = errorMsg; // = ptrIAppFramework->getLoginErrorString ();
			if(loginErrorString.NumUTF16TextChars() == 0)
			{
				loginErrorString = "Incorrect Login. Invalid ID or password. Please try again!" ;
			}
			loginErrorString.SetTranslatable(kFalse);
			//logTxt.Append("\n");
			logTxt.Append(loginErrorString);
			logTxt.Append("\n");
			ptrIAppFramework->LogError(loginErrorString);
			logTxt .Append (blankString);

			int32 minValue = counterData->GetMinimum();	
			counterData->SetValue(minValue);
			logTxt.SetTranslatable(kFalse);
            logTxt.ParseForEmbeddedCharacters();
			iTextControlData->SetString(logTxt);
			int32 maxValue = counterData->GetMaximum();	
			counterData->SetValue(maxValue);

		 	return kLoginUserNameTextEditWidgetID;			
		}


		iMultilineTextExpanderControlView->Invalidate();
		iScrollBarControlView->Invalidate();

		if(size == 1)
				return kDefaultWidgetId;
			else if(size > 1)
				return kContinueButtonWidgetID;			
			

	}while(0);
	return result;
}

/* ApplyFields
*/
void LNG2DialogController::ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId) 
{
	//CA("Inside ApplyDialogFields ");
	logTxt.Clear ();//13-march
	// Replace with code that gathers widget values and applies them.
}

void LNG2DialogController::fireUserLoggedIn()
{
	vector<int32 > observers=getMediators()->getObservers();
	vector<int32 >::iterator it;

	for(it=observers.begin();it!=observers.end();it++)
	{
		int32 i = *it;

		InterfacePtr<ILoginEvent> evt
		((ILoginEvent*) ::CreateObject(i,IID_ILOGINEVENT));
		if(!evt)
		{
			//CA("Invalid logEvtHndler in LNG2DialogController");
			continue;
		}
		//CA("Executing user login event");
		evt->userLoggedIn();
	}
}
