//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __CTIID_h__
#define __CTIID_h__

#include "SDKDef.h"

// Company:
#define kCTICompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kCTICompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kCTIPluginName	"Create Index"			// Name of this plug-in.
#define kCTIPrefixNumber	0xDB1101 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kCTIVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kCTIAuthor		"Apsiva Inc."					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kCTIPrefixNumber above to modify the prefix.)
#define kCTIPrefix		RezLong(kCTIPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kCTIStringPrefix	SDK_DEF_STRINGIZE(kCTIPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kCTIMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kCTIMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kCTIPluginID, kCTIPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kCTIActionComponentBoss, kCTIPrefix + 0)
DECLARE_PMID(kClassIDSpace, kCTIDialogBoss, kCTIPrefix + 2)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 3)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 4)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 5)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 6)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 7)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 8)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 9)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 10)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 11)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 12)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 13)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 14)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kCTIBoss, kCTIPrefix + 25)


// InterfaceIDs:
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 0)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 1)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTIINTERFACE, kCTIPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kCTIActionComponentImpl, kCTIPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kCTIDialogControllerImpl, kCTIPrefix + 1 )
DECLARE_PMID(kImplementationIDSpace, kCTIDialogObserverImpl, kCTIPrefix + 2 )
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 3)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 4)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 5)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 6)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 7)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 8)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 9)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 10)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 11)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 12)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kCTIImpl, kCTIPrefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kCTIAboutActionID, kCTIPrefix + 0)

DECLARE_PMID(kActionIDSpace, kCTIDialogActionID, kCTIPrefix + 4)
//DECLARE_PMID(kActionIDSpace, kCTIActionID, kCTIPrefix + 5)
//DECLARE_PMID(kActionIDSpace, kCTIActionID, kCTIPrefix + 6)
//DECLARE_PMID(kActionIDSpace, kCTIActionID, kCTIPrefix + 7)
//DECLARE_PMID(kActionIDSpace, kCTIActionID, kCTIPrefix + 8)
//DECLARE_PMID(kActionIDSpace, kCTIActionID, kCTIPrefix + 9)
//DECLARE_PMID(kActionIDSpace, kCTIActionID, kCTIPrefix + 10)
//DECLARE_PMID(kActionIDSpace, kCTIActionID, kCTIPrefix + 11)
//DECLARE_PMID(kActionIDSpace, kCTIActionID, kCTIPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kCTIActionID, kCTIPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kCTIActionID, kCTIPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kCTIActionID, kCTIPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kCTIActionID, kCTIPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kCTIActionID, kCTIPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kCTIActionID, kCTIPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kCTIActionID, kCTIPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kCTIActionID, kCTIPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kCTIActionID, kCTIPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kCTIActionID, kCTIPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kCTIActionID, kCTIPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kCTIActionID, kCTIPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kCTIActionID, kCTIPrefix + 25)


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kCTIDialogWidgetID, kCTIPrefix + 1)
DECLARE_PMID(kWidgetIDSpace, kCTIIndexEntryComboBoxWidgetID, kCTIPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kCTISelectBoxWidgetID, kCTIPrefix + 3)
DECLARE_PMID(kWidgetIDSpace, kCTIOldGroupPanelWidgetID, kCTIPrefix + 4)
DECLARE_PMID(kWidgetIDSpace, kCTINewGroupPanelWidgetID, kCTIPrefix + 5)
DECLARE_PMID(kWidgetIDSpace, kCTISelectBox1WidgetID, kCTIPrefix + 6)
DECLARE_PMID(kWidgetIDSpace, kCTIIndexEntryComboBox1WidgetID, kCTIPrefix + 7)
//DECLARE_PMID(kWidgetIDSpace, kCTIStartButtonWidgetID, kCTIPrefix + 8)
//DECLARE_PMID(kWidgetIDSpace, kCTINewCancelButtonWidgetID, kCTIPrefix + 9)
//DECLARE_PMID(kWidgetIDSpace, kZerothNextButtonWidgetID, kCTIPrefix + 10)
DECLARE_PMID(kWidgetIDSpace, kCTISelectBox2WidgetID, kCTIPrefix + 11)
//DECLARE_PMID(kWidgetIDSpace, kCTIBackButtonWidgetID, kCTIPrefix + 12)
//DECLARE_PMID(kWidgetIDSpace, kCTIIndexEntryComboBox1WidgetID, kCTIPrefix + 13)
DECLARE_PMID(kWidgetIDSpace, kCTIIndexEntryComboBox2WidgetID, kCTIPrefix + 14)
/////////////////////  New Create Index 9 Jan chetan
DECLARE_PMID(kWidgetIDSpace, kItemNumberRadioButtonWidgetID, kCTIPrefix + 15)
DECLARE_PMID(kWidgetIDSpace, kCategoryNameRadioButtonWidgetID, kCTIPrefix + 16)
DECLARE_PMID(kWidgetIDSpace, kBrandNameRadioButtonWidgetID, kCTIPrefix + 17)
DECLARE_PMID(kWidgetIDSpace, kManufacturerNameRadioButtonWidgetID, kCTIPrefix + 18)
DECLARE_PMID(kWidgetIDSpace, kSupplierNameRadioButtonWidgetID, kCTIPrefix + 19)
DECLARE_PMID(kWidgetIDSpace, kCustomRadioButtonWidgetID, kCTIPrefix + 20)
DECLARE_PMID(kWidgetIDSpace, kCTILevel1DDBoxWidgetID, kCTIPrefix + 21)
DECLARE_PMID(kWidgetIDSpace, kCTILevel2DDBoxWidgetID, kCTIPrefix + 22)
DECLARE_PMID(kWidgetIDSpace, kCTILevel3DDBoxWidgetID, kCTIPrefix + 23)
DECLARE_PMID(kWidgetIDSpace, kCTILevel4DDBoxWidgetID, kCTIPrefix + 24)
DECLARE_PMID(kWidgetIDSpace, kCTIClusterPanelWidgetID, kCTIPrefix + 25)
DECLARE_PMID(kWidgetIDSpace, kCTISelectlanguageDropDownWidgetID, kCTIPrefix + 27)
DECLARE_PMID(kWidgetIDSpace, kCatalogIdexCancelIconWidgetID, kCTIPrefix + 28)
DECLARE_PMID(kWidgetIDSpace, kCatalogIdexNextIconWidgetID, kCTIPrefix + 29)
DECLARE_PMID(kWidgetIDSpace, kCTIBackIconWidgetID, kCTIPrefix + 30)
DECLARE_PMID(kWidgetIDSpace, kCTIStartIconWidgetID, kCTIPrefix + 31)
DECLARE_PMID(kWidgetIDSpace, kCTICancel1IconWidgetID, kCTIPrefix + 32)
DECLARE_PMID(kWidgetIDSpace, kLevel1DDBoxSIWidgetID, kCTIPrefix + 33)
DECLARE_PMID(kWidgetIDSpace, kLevel2DDBoxSIWidgetID, kCTIPrefix + 34)
DECLARE_PMID(kWidgetIDSpace, kLevel3DDBoxSIWidgetID, kCTIPrefix + 35)
DECLARE_PMID(kWidgetIDSpace, kLevel1DDBoxTIWidgetID, kCTIPrefix + 36)
DECLARE_PMID(kWidgetIDSpace, kLevel2DDBoxTIWidgetID, kCTIPrefix + 37)
DECLARE_PMID(kWidgetIDSpace, kLevel3DDBoxTIWidgetID, kCTIPrefix + 38)


/////////////////////  New Create Index 9 Jan chetan 


// "About Plug-ins" sub-menu:
#define kCTIAboutMenuKey			kCTIStringPrefix "kCTIAboutMenuKey"
#define kCTIAboutMenuPath		kSDKDefStandardAboutMenuPath kCTICompanyKey

// "Plug-ins" sub-menu:
#define kCTIPluginsMenuKey 		kCTIStringPrefix "kCTIPluginsMenuKey"
#define kCTIPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kCTICompanyKey kSDKDefDelimitMenuPath kCTIPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kCTIAboutBoxStringKey	kCTIStringPrefix "kCTIAboutBoxStringKey"
#define kCTITargetMenuPath kCTIPluginsMenuPath

// Menu item positions:

#define kCTIDialogTitleKey kCTIStringPrefix "kCTIDialogTitleKey"
// "Plug-ins" sub-menu item key for dialog:
#define kCTIDialogMenuItemKey kCTIStringPrefix "kCTIDialogMenuItemKey"
#define kCTICreateIndexStringKey kCTIStringPrefix "kCTICreateIndexStringKey"
#define kCTISelectLanguageStringkey kCTIStringPrefix  "kCTISelectLanguageStringkey"
#define kCTIChhose3indexkey kCTIStringPrefix  "kCTIChhose3indexkey"
#define kCTIPrimaryIndexkey kCTIStringPrefix   "kCTIPrimaryIndexkey"
#define kCTILevel1key kCTIStringPrefix  "kCTILevel1key"
#define kCTILevel2key kCTIStringPrefix  "kCTILevel2key"
#define kCTILevel3key kCTIStringPrefix  "kCTILevel3key"
#define kCTISecondaryIndex kCTIStringPrefix  "kCTISecondaryIndex"
#define kCTITertiaryIndex kCTIStringPrefix  "kCTITertiaryIndex"



// "Plug-ins" sub-menu item position for dialog:
#define kCTIDialogMenuItemPosition	12.0


// Initial data format version numbers
#define kCTIFirstMajorFormatNumber  RezLong(1)
#define kCTIFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kCTICurrentMajorFormatNumber kCTIFirstMajorFormatNumber
#define kCTICurrentMinorFormatNumber kCTIFirstMinorFormatNumber

//PNG ID
#define kPNGCatalogIdexCancelIconRsrcID              1000
#define kPNGCatalogIdexCancelIconRollRsrcID         1000

#define kPNGCatalogIdexNextIconRsrcID                1001 
#define kPNGCatalogIdexNextIconRollRsrcID            1001

#define kPNGCTIBackIconRsrcID                        1002
#define kPNGCTIBackIconRollRsrcID                    1002

#define kPNGCTICancel1IconRsrcID                     1003
#define kPNGCTICancel1IconRollRsrcID                 1003

#define kPNGCTIStartIconRsrcID                       1004
#define kPNGCTIStartIconRollRsrcID                   1004


#endif // __CTIID_h__
