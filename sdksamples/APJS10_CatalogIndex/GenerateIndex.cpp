
// SnpGenIndex.cpp
// Generate an index
// 

// PCH include
#include "VCPluginHeaders.h"

// Framework includes
//#include "SnpRunnable.h"
//#include "SnipRunLog.h"

// Interface includes
#include "IActiveTopicListContext.h"
#include "IBoolData.h"
#include "ICommand.h"
#include "IDataLinkReference.h"
#include "IDocument.h"
#include "IIndexCmdData.h"
#include "IIndexOptions.h"
#include "IPersistUIDRefData.h"
#include "IStoryList.h"
//#include "ITextFrame.h"
#include "ITextModel.h"
#include "IUIDData.h"
#include "CAlert.h"

// General includes
#include "IndexingID.h"
//#include "LayoutUtils.h" //Cs3 Depricated
#include "ILayoutUtils.h" //Cs4
#include "PreferenceUtils.h"
#include "GenerateIndex.h"


#define ASSERT_BREAK(cond, mesg)		\
	if(!(cond))							\
	{									\
		ASSERT_MSG((cond), (mesg));		\
		break;							\
	}

// Ctor & Dtor
SnpGenIndex::SnpGenIndex() /*: SnpRunnable("GenIndex")*/
{
	/*this->SetDescription("Generates Index");
	this->SetPreconditions("none");
	this->SetCategories("sdk_snippet");*/
}

SnpGenIndex::~SnpGenIndex()
{
}


// Run only if there's a front document
//bool16 SnpGenIndex::CanRun(ISnpRunnableContext *runnableContext)
//{
//	bool16 result = kFalse;
//
//	if (runnableContext->GetActiveContext()->GetContextDocument() != nil)
//		result = kTrue;
//
//	return result;
//}


//ErrorCode SnpGenIndex::Run(ISnpRunnableContext *runnableContext)
//{
//	ErrorCode error = kFailure;
//
//	do
//	{
//		InterfacePtr<IDocument> piDoc(runnableContext->GetActiveContext()->GetContextDocument(), UseDefaultIID());
//
//		// Search the document if there is already an Index and find the UID of its text story
//		UIDRef indexStoryUIDRef = this->GetIndexTextStory(piDoc);
//
//		// Generate the index and update it if it already placed in the document.
//		// If the index is not already present in the doc, then the cursor would be loaded with 
//		// the index text after the following function has been executed. Clicking on the document
//		// would place it. To do it programmatically, we have to create a new text frame and place
//		// the contents of the load gun into it.
//		if((error = this->GenerateIndex(piDoc, indexStoryUIDRef)) != kSuccess)
//			break;
//
//		error = kSuccess;
//	} while(false);
//
//	return error;
//}


// Find whether the document already contains an index
UIDRef SnpGenIndex::GetIndexTextStory(IDocument *piDoc)
{
	UIDRef indexUIDRef(nil, kInvalidUID);

	do
	{
		// Go thru all the stories in the doc checking if there's an Index story
		InterfacePtr<IStoryList> piStoryList(piDoc, UseDefaultIID());
		ASSERT_BREAK(piStoryList != nil, "SnpGenIndex::GetIndexTextStory() - No stories present or could not get the story list pointer");

		int32 numStories = piStoryList->GetUserAccessibleStoryCount();
		int32 i=0;
		for(; i<numStories; i++)
		{
			InterfacePtr<ITextModel> piTextModel(piStoryList->GetNthUserAccessibleStoryUID(i), IID_ITEXTMODEL);
			ASSERT_BREAK(piTextModel != nil, "Could not get the TextModel interface pointer");

			InterfacePtr<IDataLinkReference> piDataLink(piTextModel, IID_IDATALINKREFERENCE/*IID_IGENERICDATALINKREFS*/); //Cs4
			if (piDataLink)
			{
				UID dataLinkUID = piDataLink->GetUID();
				// Whether it is a generated Index story
				InterfacePtr<IPersistUIDRefData> pIndexInstanceUIDData(::GetDataBase(piStoryList), dataLinkUID, IID_IIDXINSTANCEPERSISTUIDDATA);
				if((pIndexInstanceUIDData) && ((pIndexInstanceUIDData->GetUID()) != kInvalidUID))
				{
					//SNIPLOG("This is an Index Text Story, UID: %d",::GetUID(piTextModel));
					indexUIDRef = ::GetUIDRef(piTextModel);
					return indexUIDRef;
				}

			}
		}

	} while(false);

	return indexUIDRef;
}


// Generate an index through a command
ErrorCode SnpGenIndex::GenerateIndex(IDocument *piDoc, UIDRef indexStoryUIDRef)
{
	ErrorCode error = kFailure;

	do 
	{
		// Create a command to generate the index
		InterfacePtr<ICommand> piGenIndexCmd(CmdUtils::CreateCommand(kGenerateIndexCmdBoss));
		ASSERT_BREAK(piGenIndexCmd != nil, "SnpGenIndex::GenerateIndex() - Could not create the generate index command");


		// Command Data 1: The list of topics to be put into the index
		// NOTE: This Active topic list is very important for the generate index command.
		// If this is not available then no index will be generated.
		InterfacePtr<IActiveTopicListContext> piActiveTopicListContext(GetExecutionContextSession(), UseDefaultIID());//Cs4
		ASSERT_BREAK(piActiveTopicListContext != nil, "SnpGenIndex::GenerateIndex() - Could not get the IActiveTopicListContext pointer!");
		//UIDRef	activeTopicList = piActiveTopicListContext->GetActiveTopicList();
		UIDRef	activeTopicList = piActiveTopicListContext->GetActiveTopicListFromActiveDocument();
		piGenIndexCmd->SetItemList(UIDList(activeTopicList));


		// Command Data 2: Set the index option preferences
		// NOTE: Here we are just getting the global preferences for Index and using them to create our index.
		// If more specific tuning of the index is required, please refer to the documentation on IIndexOptions
		// for a comprehensive list of all options.
		InterfacePtr<IIndexOptions> piIndexOptions((IIndexOptions*)::QueryPreferences(IID_IINDEXOPTIONS, /*kGetFrontmostPrefs*/piDoc));
		ASSERT_BREAK(piIndexOptions != nil, "SnpGenIndex::GenerateIndex() - Could not get the IIndexOptions pointer!");
		piIndexOptions->SetScope(IIndexOptions::kDocumentScope);
		

		// Copy the index options into the index options for the command
		InterfacePtr<IIndexOptions> iIndexOptions(piGenIndexCmd, UseDefaultIID());
		ASSERT_BREAK(iIndexOptions != nil, "SnpGenIndex::GenerateIndex() - Could not get the IIndexOptions pointer!");
		iIndexOptions->CopyDataFrom(piIndexOptions);

		
		// Command Data 3: The document that is the target of the command
		InterfacePtr<IIndexCmdData> piIndexCmdData(piGenIndexCmd, UseDefaultIID());
		ASSERT_BREAK(piIndexCmdData != nil, "SnpGenIndex::GenerateIndex() - Could not get the IIndexCmdData pointer!");
		piIndexCmdData->SetTargetItemRef(piDoc->GetDocWorkSpace());

		
		// Command Data 4: Whether we want to update the previously generated (already existing) index story
		InterfacePtr<IBoolData> piUpdateSelectedIndexStory(piGenIndexCmd, IID_IUPDATESELECTEDINDEXSTORY);
		ASSERT_BREAK(piUpdateSelectedIndexStory != nil, "SnpGenIndex::GenerateIndex() - Could not get the IBoolData pointer!");


		// Command Data 5: The UID of the selected index story
		// NOTE: The Index will be placed into this story if and only if the story is the generated index.
		// If you specify an arbitrary UID, the index would be created but not placed anywhere.
		if(indexStoryUIDRef != nil)
		{
			piUpdateSelectedIndexStory->Set(kTrue);
			InterfacePtr<IUIDData> piUIDData(piGenIndexCmd, IID_IUIDDATA);
			ASSERT_BREAK(piUIDData != nil, "SnpGenIndex::GenerateIndex() - Could not get the IUIDData pointer!");
			piUIDData->Set(indexStoryUIDRef);
		}
		else // Don't update the index in place
			piUpdateSelectedIndexStory->Set(kFalse);

		
		error = CmdUtils::ProcessCommand(piGenIndexCmd);

	} while(false);

	return error;
}



// An instance of the snippet
SnpGenIndex instanceSnpGenIndex;

// End
