#include "VCPlugInHeaders.h"
//#include "IMessageServer.h"
#include "CAlert.h"


#include "VCPlugInHeaders.h"
#include "CAlert.h"

void CAMessage(const PMString & fileName,const PMString & functionName,const PMString & message,int32 line)
{
	
	PMString pluginNameString("PluginName : ");
	pluginNameString.Append("AP45_CatalogIndex.pln");

	PMString fileNameString("FileName : ");
	fileNameString.Append(fileName);

	PMString functionNameString("FunctionName : ");
	functionNameString.Append(functionName);

	PMString lineNumber("Line Number : ");
	lineNumber.AppendNumber(line);
	lineNumber.Append("\n***********************");

	PMString messageString("Message : ");
	messageString.Append(message);

	CAlert::InformationAlert(pluginNameString + "\n" + fileNameString +"\n" + functionNameString + "\n" + lineNumber +"\n" + messageString);

}