//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// General includes:
#include "MenuDef.fh"
#include "ActionDef.fh"
#include "ActionDefs.h"
#include "AdobeMenuPositions.h"
#include "LocaleIndex.h"
#include "PMLocaleIds.h"
#include "StringTable.fh"
#include "ObjectModelTypes.fh"
#include "ShuksanID.h"
#include "ActionID.h"
#include "CommandID.h"
#include "WorkspaceID.h"
#include "WidgetID.h"
#include "BuildNumber.h"
#include "PlugInModel_UIAttributes.h"

#include "InterfaceColorDefines.h"
#include "IControlViewDefs.h"
#include "SysControlIDs.h"
#include "Widgets.fh"	// for PalettePanelWidget or DialogBoss

#include "EveInfo.fh"	// Required when using EVE for dialog layout/widget placement

// Project includes:
#include "CTIID.h"
#include "GenericID.h"
#include "ShuksanID.h"
#include "TextID.h"


#ifdef __ODFRC__

/*  
 * Plugin version definition.
 */
resource PluginVersion (kSDKDefPluginVersionResourceID)
{
	kTargetVersion,
	kCTIPluginID,
	kSDKDefPlugInMajorVersionNumber, kSDKDefPlugInMinorVersionNumber,
	kSDKDefHostMajorVersionNumber, kSDKDefHostMinorVersionNumber,
	kCTICurrentMajorFormatNumber, kCTICurrentMinorFormatNumber,
	{ kInDesignProduct, kInCopyProduct },
	{ kWildFS },
	kUIPlugIn,
	kCTIVersion
};

/*  
 * The ExtraPluginInfo resource adds extra information to the Missing Plug-in dialog
 * that is popped when a document containing this plug-in's data is opened when
 * this plug-in is not present. These strings are not translatable strings
 * since they must be available when the plug-in isn't around. They get stored
 * in any document that this plug-in contributes data to.
 */
resource ExtraPluginInfo(1)
{
	kCTICompanyValue,			// Company name
	kCTIMissingPluginURLValue,	// URL 
	kCTIMissingPluginAlertValue,	// Missing plug-in alert text
};

/* 
 * Boss class definitions.
 */
resource ClassDescriptionTable(kSDKDefClassDescriptionTableResourceID)
{{{

	/*
	 * This boss class supports two interfaces:
	 * IActionComponent and IPMPersist.
     *
	 * 
	 * @ingroup apjs9_catalogindex
	 */
	Class
	{
		kCTIActionComponentBoss,
		kInvalidClass,
		{
			// Handle the actions from the menu.
			IID_IACTIONCOMPONENT, kCTIActionComponentImpl,
			// Persist the state of the menu across application instantiation. Implementation provided by the API.
			IID_IPMPERSIST, kPMPersistImpl
		}
	},

	/*
	 * This boss class implements the dialog for this plug-in. All
	 * dialogs must implement IDialogController. Specialisation of
	 * IObserver is only necessary if you want to handle widget
	 * changes dynamically rather than just gathering their values
	 * and applying in the IDialogController implementation.
	 * In this implementation IObserver is specialised so that the
	 * plug-in's about box is popped when the info button widget
	 * is clicked.
     *
	 * 
	 * @ingroup apjs9_catalogindex
	 */
	Class
	{
		kCTIDialogBoss,
		kDialogBoss,
		{
			// Provides management and control over the dialog.
			IID_IDIALOGCONTROLLER, kCTIDialogControllerImpl,
			
			// Allows dynamic processing of dialog changes.
			IID_IOBSERVER, kCTIDialogObserverImpl,
		}
	},

}}};

/*  
 * Implementation definition.
 */
resource FactoryList (kSDKDefFactoryListResourceID)
{
	kImplementationIDSpace,
	{
		#include "CTIFactoryList.h"
	}
};

/*  
 * Menu definition.
 */
resource MenuDef (kSDKDefMenuResourceID)
{
	{
		// The About Plug-ins sub-menu item for this plug-in.
	//	kCTIAboutActionID,			// ActionID (kInvalidActionID for positional entries)
	//	kCTIAboutMenuPath,			// Menu Path.
	//	kSDKDefAlphabeticPosition,			// Menu Position.
	//	kSDKDefIsNotDynamicMenuFlag,		// kSDKDefIsNotDynamicMenuFlag or kSDKDefIsDynamicMenuFlag


	// The Plug-ins menu sub-menu items for this plug-in.
		kCTIDialogActionID,
		"BookPanelPopup", // ,//kCTIPluginsMenuPath,
		1303,//1306, //1315, //kCTIDialogMenuItemPosition,
		kSDKDefIsNotDynamicMenuFlag,
		

	}
};

/* 
 * Action definition.
 */
resource ActionDef (kSDKDefActionResourceID)
{
	{
/*		kCTIActionComponentBoss, 		// ClassID of boss class that implements the ActionID.
		kCTIAboutActionID,	// ActionID.
		kCTIAboutMenuKey,	// Sub-menu string.
		kOtherActionArea,				// Area name (see ActionDefs.h).
		kNormalAction,					// Type of action (see ActionDefs.h).
		kDisableIfLowMem,				// Enabling type (see ActionDefs.h).
		kInvalidInterfaceID,			// Selection InterfaceID this action cares about or kInvalidInterfaceID.
		kSDKDefInvisibleInKBSCEditorFlag, // kSDKDefVisibleInKBSCEditorFlag or kSDKDefInvisibleInKBSCEditorFlag.
*/

		kCTIActionComponentBoss,
		kCTIDialogActionID,		
		kCTICreateIndexStringKey, //"Create Index",		
		kOtherActionArea,			
		kNormalAction,				
		kCustomEnabling,	
		kInvalidInterfaceID,		
		kSDKDefVisibleInKBSCEditorFlag,


	}
};


/*  
 * Locale Indicies.
 * The LocaleIndex should have indicies that point at your
 * localizations for each language system that you are localized for.
 */

/*  
 * String LocaleIndex.
 */
resource LocaleIndex ( kSDKDefStringsResourceID)
{
	kStringTableRsrcType,
	{
		kWildFS, k_enUS, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_enGB, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_deDE, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_frFR, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_esES, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_ptBR, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_svSE, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_daDK, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_nlNL, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_itIT, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_nbNO, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_fiFI, kSDKDefStringsResourceID + index_enUS
		kInDesignJapaneseFS, k_jaJP, kSDKDefStringsResourceID + index_jaJP
	}
};

resource LocaleIndex (kSDKDefStringsNoTransResourceID)
{
	kStringTableRsrcType,
	{
		kWildFS, k_Wild, kSDKDefStringsNoTransResourceID + index_enUS
	}
};

// Strings not being localized
resource StringTable (kSDKDefStringsNoTransResourceID + index_enUS)
{
	k_enUS,									// Locale Id
	kEuropeanMacToWinEncodingConverter,		// Character encoding converter
	{
	}
};

/*  
 * Dialog LocaleIndex.
 */
resource LocaleIndex (kSDKDefDialogResourceID)
{
   kViewRsrcType,
	{
		kWildFS, k_Wild, kSDKDefDialogResourceID + index_enUS
	}
};

/*  
 * Type definition.
 */
type CTIDialogWidget(kViewRsrcType) : DialogBoss(ClassID = kCTIDialogBoss)
{
	//WidgetEveInfo;
};

/*  Dialog definition.
	This view is not localised: therefore, it can reside here.
	However, if you wish to localise it, it is recommended to locate it in one of
	the localised framework resource files (i.e. CTI_enUS.fr etc.) and
	update your Dialog LocaleIndex accordingly.
*/
//**************************************************************************
resource PNGA(kPNGCatalogIdexCancelIconRsrcID)  "Cancel1_hover80x22.png"
resource PNGR(kPNGCatalogIdexCancelIconRollRsrcID)  "Cancel1_80x22.png"
resource PNGA(kPNGCatalogIdexNextIconRsrcID)  "Next1_hover80x22.png"
resource PNGR(kPNGCatalogIdexNextIconRollRsrcID)  "Next_80x22.png"
resource PNGA(kPNGCTIBackIconRsrcID)  "Back1_Hover80x22.png"
resource PNGR(kPNGCTIBackIconRollRsrcID)  "Back_80x22.png"
resource PNGA(kPNGCTICancel1IconRsrcID)  "Cancel1_hover80x22.png" 
resource PNGR(kPNGCTICancel1IconRollRsrcID) "Cancel1_80x22.png"
resource PNGA(kPNGCTIStartIconRsrcID)  "Start_hover80x22.png"
resource PNGR(kPNGCTIStartIconRollRsrcID)  "Start_80x22.png"



//***************************************************************************
resource CTIDialogWidget (kSDKDefDialogResourceID + index_enUS)
{
	__FILE__, __LINE__,
	kCTIDialogWidgetID,	// WidgetID
	kPMRsrcID_None,		// RsrcID
	kBindNone,			// Binding
	0, 0, 740,370,		// Frame (l,t,r,b)	   //0, 0, 650,370,            
	kTrue, kTrue,		// Visible, Enabled
	kCTIDialogTitleKey,	// Dialog name
	{
	//<FREDDYWIDGETDEFLISTUS>
	

		GroupPanelWidget
		(
			// CControlView properties
			//kInvalidWidgetID, // widget ID
			kCTIOldGroupPanelWidgetID 
			kPMRsrcID_None, // PMRsrc ID
			kBindNone, // frame binding			
			Frame(2,2,739,369), // //  left, top, right, bottom2,2,649,349  Frame(2,2,739,349),	
			kTrue, // visible
			kTrue, // enabled
			// GroupPanelAttributes properties
			0,//kTblAttCellAttributesPanelTitleWidgetID, // header widget ID
			{
			
				InfoStaticTextWidget
				(
					kInvalidWidgetID, 
					kPMRsrcID_None,							// WidgetId, RsrcId
					kBindLeft | kBindRight,					// Frame binding
					//Frame(50,5,150,25)						// Frame//og
					Frame(250,100,350,125)						
					
					kTrue, kTrue, kAlignLeft, 
					kDontEllipsize,kTrue,					// Visible, Enabled
					kCTISelectLanguageStringkey,			//"Select Language :",					// Text
					0,
					kDialogWindowSystemScriptFontId,	//0, 
					kDialogWindowSystemScriptFontId,	//1,
				),
				DropDownListWidget
				(
					// CControlView properties
					kCTISelectlanguageDropDownWidgetID, // widget ID
					kSysDropDownPMRsrcId, // PMRsrc ID
					kBindRight,//kBindNone, // frame binding
					//Frame(155,5,300,25)  //  left, top, right, bottom//og
					Frame(355,100,500,125)  //  left, top, right, bottom
					kTrue, // visible
					kTrue, // enabled
					// DropDownListControlData properties
					{{	
						
					}}
				),
					
				DefaultButtonWidget
				(
					kOKButtonWidgetID,		// WidgetID
					kSysButtonPMRsrcId,		// RsrcID
					kBindNone,				// Binding
					0,0,1,1,		// Frame (l,t,r,b)							
					kTrue, kTrue,			// Visible, Enabled
					kSDKDefOKButtonApplicationKey, //"OK",	// Button text
				),
				
				CancelButtonWidget
				(
					kCancelButton_WidgetID,	// WidgetID
					kSysButtonPMRsrcId,		// RsrcID
					kBindNone,				// Binding					
					0,0,1,1,
					kTrue, kTrue,			// Visible, Enabled
					kSDKDefCancelButtonApplicationKey,	// Button name
					kTrue,					// Change to Reset on option-click.
				),

				RollOverIconButtonWidget
				(
					kCatalogIdexCancelIconWidgetID, 
					//kNextIconID, 
					kPNGCatalogIdexCancelIconRsrcID
					kCTIPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(290,330,370,352)					// Frame(240,330,320,352)
					kTrue, kTrue,
					kADBEIconSuiteButtonType,
				),
				
				RollOverIconButtonWidget
				(
					kCatalogIdexNextIconWidgetID, 
					//kNextIconID, 
					kPNGCatalogIdexNextIconRsrcID
					kCTIPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(390,330,470,352)							// Frame(340,330,420,352)
					kTrue, kTrue,
					kADBEIconSuiteButtonType,
				),
				
			
			}
		),	

/////////////////////  New Create Index 10 Jan chetan
		GroupPanelWidget
		(
			// CControlView properties
										// kInvalidWidgetID, // widget ID
			kCTINewGroupPanelWidgetID	// added on 16 Nov by Tushar
			kPMRsrcID_None,				// PMRsrc ID
			kBindNone,					// frame binding
			Frame(2,2,739,349),			// left, top, right, bottom//Frame(2,2,649,349)
			kTrue, // visible
			kTrue, // enabled
			// GroupPanelAttributes properties
			0,//kTblAttCellAttributesPanelTitleWidgetID, // header widget ID
			{
			
				StaticTextWidget
				(
					kInvalidWidgetID,			// widget ID
					kSysStaticTextPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(35,20,180,40),		//  left, top, right, bottom	
					kTrue,						// visible
					kTrue,						// enabled
												// StaticTextAttributes properties
					kAlignLeft,					// Alignment
					kDontEllipsize,kTrue,		// Ellipsize style
												// CTextControlData properties
					kCTIChhose3indexkey, //"  Choose up to 3 Indexes  ",				// control label //"  Level : ",
												// AssociatedWidgetAttributes properties
					kInvalidWidgetID,			// associated widget ID
				),
				GroupPanelWidget
				(
					// CControlView properties
					kInvalidWidgetID,	// widget ID			
					kPMRsrcID_None,		// PMRsrc ID
					kBindNone,			// frame binding
					Frame(32,45,705,294)//  left, top, right, bottom
					kTrue,				// visible
					kTrue,				// enabled
										// GroupPanelAttributes properties
					0,					//kTblAttCellAttributesPanelTitleWidgetID, // header widget ID
					{
						//StaticTextWidget
						//(
						//	kInvalidWidgetID,			// widget ID
						//	kSysStaticTextPMRsrcId,		// PMRsrc ID
						//	kBindNone,					// frame binding
						//	Frame(35,-10,180,10),		//  left, top, right, bottom	//Frame(35,-10,90,10)
						//	kTrue,						// visible
						//	kTrue,						// enabled
						//								// StaticTextAttributes properties
						//	kAlignLeft,					// Alignment
						//	kDontEllipsize,kTrue,		// Ellipsize style
						//								// CTextControlData properties
						//	"  Choose up to 3 Indexes  ",				// control label //"  Level : ",
						//								// AssociatedWidgetAttributes properties
						//	kInvalidWidgetID,			// associated widget ID
						//),			
					}
				)	
				
				StaticTextWidget
				(
					kInvalidWidgetID,			// widget ID
					kSysStaticTextPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(60,80,170,100),		//  left, top, right, bottom	//Frame(35,-10,90,10)
					kTrue,						// visible
					kTrue,						// enabled
												// StaticTextAttributes properties
					kAlignLeft,					// Alignment
					kDontEllipsize,kTrue,		// Ellipsize style
												// CTextControlData properties
					kCTIPrimaryIndexkey, //"  Primary Index: ",		// control label 
												// AssociatedWidgetAttributes properties
					kInvalidWidgetID,			// associated widget ID
				),	
						
				
				StaticTextWidget
				(
					kInvalidWidgetID,			// widget ID
					kSysStaticTextPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(50,120,110,140),		// left, top, right, bottom //Frame(95,70,165,90)
					kTrue,						// visible
					kTrue,						// enabled
												// StaticTextAttributes properties
					kAlignLeft,					// Alignment
					kDontEllipsize,kTrue,		// Ellipsize style
												// CTextControlData properties
					kCTILevel1key, //"Level 1: ",				// control label
												// AssociatedWidgetAttributes properties
					kInvalidWidgetID,			// associated widget ID
				),
								
				DropDownListWidget
				(
					kCTILevel1DDBoxWidgetID,	// widget ID
					kSysDropDownPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(120,120,250,140),		//Frame(175,70,315,90),
					kTrue,						// visible
					kTrue,						// enabled
												// DropDownListControlData properties
					{{
						
					}}
				),
				
				StaticTextWidget
				(
					kInvalidWidgetID,			// widget ID
					kSysStaticTextPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(50,165,110,195)		// left, top, right, bottom
					kTrue,						// visible
					kTrue,						// enabled
												// StaticTextAttributes properties
					kAlignLeft,					// Alignment
					kDontEllipsize,kTrue,		// Ellipsize style
												// CTextControlData properties
					kCTILevel2key, //"Level 2: ",				// control label
												// AssociatedWidgetAttributes properties
					kInvalidWidgetID,			// associated widget ID
				),
								
				DropDownListWidget
				(
					kCTILevel2DDBoxWidgetID,	// widget ID
					kSysDropDownPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(120,170,250,190)		// Frame(150.0,20.0,290.0,40.0) //  left, top, right, bottom
					kTrue,						// visible
					kTrue,						// enabled
												// DropDownListControlData properties
					{{
						
					}}
				),
				
				StaticTextWidget
				(
					kInvalidWidgetID,			// widget ID
					kSysStaticTextPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(50,210,110,250)		// left, top, right, bottom
					kTrue,						// visible
					kTrue,						// enabled
												// StaticTextAttributes properties
					kAlignLeft,					// Alignment
					kDontEllipsize,kTrue,		// Ellipsize style
												// CTextControlData properties
					kCTILevel3key, //"Level 3: ",				// control label
												// AssociatedWidgetAttributes properties
					kInvalidWidgetID,			// associated widget ID
				),
								
				DropDownListWidget
				(
					kCTILevel3DDBoxWidgetID,	// widget ID
					kSysDropDownPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(120,220,250,240)		// Frame(150.0,20.0,290.0,40.0) //  left, top, right, bottom
					kTrue,						// visible
					kTrue,						// enabled
												// DropDownListControlData properties
					{{
						
					}}
				),
				
				SeparatorWidget
				(
					kInvalidWidgetID, 
					kPMRsrcID_None,				// WidgetId, RsrcId
					kBindLeft | kBindRight,     // Frame binding
					Frame(260,45,262,294)
					kTrue,kTrue,				// Visible, Enabled
				),
				
				StaticTextWidget
				(
					kInvalidWidgetID,			// widget ID
					kSysStaticTextPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(275,80,385,100),		//  left, top, right, bottom	//Frame(35,-10,90,10)
					kTrue,						// visible
					kTrue,						// enabled
												// StaticTextAttributes properties
					kAlignLeft,					// Alignment
					kDontEllipsize,kTrue,		// Ellipsize style
												// CTextControlData properties
					kCTISecondaryIndex, //"  Secondary Index: ",		// control label 
												// AssociatedWidgetAttributes properties
					kInvalidWidgetID,			// associated widget ID
				),	
						
				
				StaticTextWidget
				(
					kInvalidWidgetID,			// widget ID
					kSysStaticTextPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(270,120,330,140),		// left, top, right, bottom //Frame(95,70,165,90)
					kTrue,						// visible
					kTrue,						// enabled
												// StaticTextAttributes properties
					kAlignLeft,					// Alignment
					kDontEllipsize,kTrue,		// Ellipsize style
												// CTextControlData properties
					kCTILevel1key, //"Level 1: ",				// control label
												// AssociatedWidgetAttributes properties
					kInvalidWidgetID,			// associated widget ID
				),
								
				DropDownListWidget
				(
					kLevel1DDBoxSIWidgetID,		// widget ID
					kSysDropDownPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(340,120,470,140),		//Frame(175,70,315,90),
					kTrue,						// visible
					kTrue,						// enabled
												// DropDownListControlData properties
					{{
						
					}}
				),
				
				StaticTextWidget
				(
					kInvalidWidgetID,			// widget ID
					kSysStaticTextPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(270,165,330,195)		// left, top, right, bottom
					kTrue,						// visible
					kTrue,						// enabled
												// StaticTextAttributes properties
					kAlignLeft,					// Alignment
					kDontEllipsize,kTrue,		// Ellipsize style
												// CTextControlData properties
					kCTILevel2key, //"Level 2: ",				// control label
												// AssociatedWidgetAttributes properties
					kInvalidWidgetID,			// associated widget ID
				),
								
				DropDownListWidget
				(
					kLevel2DDBoxSIWidgetID,		// widget ID
					kSysDropDownPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(340,170,470,190)		// Frame(150.0,20.0,290.0,40.0) //  left, top, right, bottom
					kTrue,						// visible
					kTrue,						// enabled
												// DropDownListControlData properties
					{{
						
					}}
				),
				
				StaticTextWidget
				(
					kInvalidWidgetID,			// widget ID
					kSysStaticTextPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(270,210,330,250)		// left, top, right, bottom
					kTrue,						// visible
					kTrue,						// enabled
												// StaticTextAttributes properties
					kAlignLeft,					// Alignment
					kDontEllipsize,kTrue,		// Ellipsize style
												// CTextControlData properties
					kCTILevel3key, //"Level 3: ",				// control label
												// AssociatedWidgetAttributes properties
					kInvalidWidgetID,			// associated widget ID
				),
								
				DropDownListWidget
				(
					kLevel3DDBoxSIWidgetID,		// widget ID
					kSysDropDownPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(340,220,470,240)		// Frame(150.0,20.0,290.0,40.0) //  left, top, right, bottom
					kTrue,						// visible
					kTrue,						// enabled
												// DropDownListControlData properties
					{{
						
					}}
				),
				
				SeparatorWidget
				(
					kInvalidWidgetID, 
					kPMRsrcID_None,				// WidgetId, RsrcId
					kBindLeft | kBindRight,     // Frame binding
					Frame(480,45,482,294)
					kTrue,kTrue,				// Visible, Enabled
				),
				
				StaticTextWidget
				(
					kInvalidWidgetID,			// widget ID
					kSysStaticTextPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(492,80,617,100),		//  left, top, right, bottom	//Frame(35,-10,90,10)
					kTrue,						// visible
					kTrue,						// enabled
												// StaticTextAttributes properties
					kAlignLeft,					// Alignment
					kDontEllipsize,kTrue,		// Ellipsize style
												// CTextControlData properties
					kCTITertiaryIndex, //"  Tertiary Index: ",		// control label 
												// AssociatedWidgetAttributes properties
					kInvalidWidgetID,			// associated widget ID
				),	
						
				
				StaticTextWidget
				(
					kInvalidWidgetID,			// widget ID
					kSysStaticTextPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(487,120,547,140),		// left, top, right, bottom //Frame(95,70,165,90)
					kTrue,						// visible
					kTrue,						// enabled
												// StaticTextAttributes properties
					kAlignLeft,					// Alignment
					kDontEllipsize,kTrue,		// Ellipsize style
												// CTextControlData properties
					kCTILevel1key, //"Level 1: ",				// control label
												// AssociatedWidgetAttributes properties
					kInvalidWidgetID,			// associated widget ID
				),
								
				DropDownListWidget
				(
					kLevel1DDBoxTIWidgetID,	// widget ID
					kSysDropDownPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(557,120,687,140),		//Frame(175,70,315,90),
					kTrue,						// visible
					kTrue,						// enabled
												// DropDownListControlData properties
					{{
						
					}}
				),
				
				StaticTextWidget
				(
					kInvalidWidgetID,			// widget ID
					kSysStaticTextPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(487,165,547,195)		// left, top, right, bottom
					kTrue,						// visible
					kTrue,						// enabled
												// StaticTextAttributes properties
					kAlignLeft,					// Alignment
					kDontEllipsize,kTrue,		// Ellipsize style
												// CTextControlData properties
					kCTILevel2key, //"Level 2: ",				// control label
												// AssociatedWidgetAttributes properties
					kInvalidWidgetID,			// associated widget ID
				),
								
				DropDownListWidget
				(
					kLevel2DDBoxTIWidgetID,		// widget ID
					kSysDropDownPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(557,170,687,190)		// Frame(150.0,20.0,290.0,40.0) //  left, top, right, bottom
					kTrue,						// visible
					kTrue,						// enabled
												// DropDownListControlData properties
					{{
						
					}}
				),
				
				StaticTextWidget
				(
					kInvalidWidgetID,			// widget ID
					kSysStaticTextPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(487,210,547,250)		// left, top, right, bottom
					kTrue,						// visible
					kTrue,						// enabled
												// StaticTextAttributes properties
					kAlignLeft,					// Alignment
					kDontEllipsize,kTrue,		// Ellipsize style
												// CTextControlData properties
					kCTILevel3key, //"Level 3: ",				// control label
												// AssociatedWidgetAttributes properties
					kInvalidWidgetID,			// associated widget ID
				),
								
				DropDownListWidget
				(
					kLevel3DDBoxTIWidgetID,		// widget ID
					kSysDropDownPMRsrcId,		// PMRsrc ID
					kBindNone,					// frame binding
					Frame(557,220,687,240)		// Frame(150.0,20.0,290.0,40.0) //  left, top, right, bottom
					kTrue,						// visible
					kTrue,						// enabled
												// DropDownListControlData properties
					{{
						
					}}
				),
					

				RollOverIconButtonWidget
				(
					kCTIBackIconWidgetID, 
					//kNextIconID, 
					kPNGCTIBackIconRsrcID
					kCTIPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(230,315,310,337)							// Frame(170,315,250,337)
					kTrue, kTrue,
					kADBEIconSuiteButtonType,
				),
				
				RollOverIconButtonWidget
				(
					kCTICancel1IconWidgetID, 
					//kNextIconID, 
					kPNGCTICancel1IconRsrcID
					kCTIPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(330,315,410,337)							// Frame(270,315,350,337)
					kTrue, kTrue,
					kADBEIconSuiteButtonType,
				),
				
				RollOverIconButtonWidget
				(
					kCTIStartIconWidgetID, 
					//kNextIconID, 
					kPNGCTIStartIconRsrcID
					kCTIPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(430,315,510,337)							// Frame(370,315,450,337)
					kTrue, kTrue,
					kADBEIconSuiteButtonType,
				),
		
			}
		),

	//</FREDDYWIDGETDEFLISTUS>

	},

	//kEVEArrangeChildrenInRow | kEVESmallMargin,
};

#endif // __ODFRC__

#include "CTI_enUS.fr"
#include "CTI_jaJP.fr"
