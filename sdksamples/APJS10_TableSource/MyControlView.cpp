#include "VCPlugInHeaders.h"

#include "ErasablePanelView.h"
//#include "ImageReorgniser.h"
//#include "GlobalFunctions.h"

// Plug-in includes

#include "TSID.h"
#include "CAlert.h"

#define CA(X) CAlert::InformationAlert(X)

/** Overrides the ConstrainDimensions to control the maximum and minimum width 
	and height of panel when it is resized.

	@ingroup snippetrunner
	@author Seoras Ashby
*/
class  MyRunControlView : public ErasablePanelView
{
	public:
		/** 
			Constructor.
		*/
		MyRunControlView(IPMUnknown* boss) 
			: ErasablePanelView(boss) 
		{
			//fowner = boss ;
			//CA(__FUNCTION__);
		}

		/** 
			Destructor.
		*/
		virtual ~MyRunControlView() {}

		/** Allows the panel size to be constrained.  
			@param dimensions OUT specifies the maximum and minimum width and height of the panel
				when it is resized.
		*/
		virtual PMPoint	ConstrainDimensions(const PMPoint& dimensions) const;

		/**	Clear the SnippetRunner framework log when resizing. 
			The multi line widget log gives some odd behaviour if we don't. 
			Means you lose the log contents when you resize.
			@param newDimensions
			@param invalidate
		*/
		virtual  void Resize(const PMPoint& newDimensions, bool16 invalidate);

	private:

		static const int kMinimumPanelWidth;
		static const int kMaximumPanelWidth;
		static const int kMinimumPanelHeight;
		static const int kMaximumPanelHeight;
		//IPMUnknown *fowner ; 
};

// define the max/min width and height for the panel
const int	MyRunControlView::kMinimumPanelWidth	=	207;
const int 	MyRunControlView::kMaximumPanelWidth	=	1400;
const int 	MyRunControlView::kMinimumPanelHeight	=	291;
const int 	MyRunControlView::kMaximumPanelHeight	=	1500;


/* Make the implementation available to the application.
*/
CREATE_PERSIST_PMINTERFACE(MyRunControlView , kTSSnipRunControlViewImpl )


/* Allows the panel size to be constrained.  
*/
PMPoint MyRunControlView::ConstrainDimensions(const PMPoint& dimensions) const
{
	PMPoint constrainedDim = dimensions;

	// Width can vary if not above maximum or below minimum
	if(constrainedDim.X() > kMaximumPanelWidth)
	{
		constrainedDim.X(kMaximumPanelWidth);
	}
	else if(constrainedDim.X() < kMinimumPanelWidth)
	{
		constrainedDim.X(kMinimumPanelWidth);
	}

	// Height can vary if not above maximum or below minimum
	if(constrainedDim.Y() > kMaximumPanelHeight)
	{
		constrainedDim.Y(kMaximumPanelHeight);
	}
	else if(constrainedDim.Y() < kMinimumPanelHeight)
	{
		constrainedDim.Y(kMinimumPanelHeight);
	}

	return constrainedDim;
}

/*
*/
void MyRunControlView::Resize(const PMPoint& newDimensions, bool16 invalidate)
{
	/*PMString  ASD("ProductFinder::Resize newDimensions.X : ");
	ASD.AppendNumber(newDimensions.X());
	ASD.Append("  newDimensions.Y : ");
	ASD.AppendNumber(newDimensions.Y());
	CA(ASD);*/

	PMPoint constrainedDim = newDimensions;
	if(constrainedDim.X() < kMinimumPanelWidth)
	{
		constrainedDim.X(kMinimumPanelWidth);
	}

	ErasablePanelView::Resize(constrainedDim, invalidate);
}



