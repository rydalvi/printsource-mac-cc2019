#include "VCPluginHeaders.h"
#include "string.h"
#include "TSTreeData.h"
#include "CAlert.h"
#include "TSMediatorClass.h"


//#include "IMessageServer.h"
inline PMString  numToPMString(int32 num)
	{
		PMString numStr;
		numStr.AppendNumber(num);
		return numStr;
	}

#define CA(X) CAlert::InformationAlert \
	( \
	PMString("BoxReader.cpp") + PMString("\n") + \
    PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + X)
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}


//vector<TSTreeInfo> TSTreeData::PFTreeData;
//vector<TSTreeInfo> TSTreeData::PGTreeData;
vector<TSTreeInfo> TSTreeData::PRTreeData;
vector<TSTreeInfo> TSTreeData::ItemTreeData;
vector<TSTreeInfo> TSTreeData::ProjectTreeData;
//vector<TSTreeInfo> TSTreeData::CatagoryTreeData;

typedef vector<TSTreeInfo> TSTreeInfoVect;

void TSTreeData::setData
(int16 TreeType, int16 index, double id, double typeId, double LanguageID,
 PMString name, bool16 state, PMString code, bool16 flag,int32 tableFlag) 
{
	TSTreeInfo listinfo;
	listinfo.id = id;
	listinfo.index = index;
	listinfo.typeId = typeId;
	listinfo.isSelected = state;
	listinfo.name = name;
	listinfo.LanguageID= LanguageID;
	listinfo.isDragged = kFalse;
	listinfo.code = code;
	listinfo.isImageFlag=flag;
	listinfo.tableFlag=tableFlag;

	//added on 11July...serial blast day
	listinfo.TreeType = TreeType;

	switch(TreeType)
	{
		//case 1:
		//	PFTreeData.push_back(listinfo);
		//	break;
		//case 2:
		//	PGTreeData.push_back(listinfo);
		//	break;
		case 3:
			PRTreeData.push_back(listinfo);
			break;
		case 4:
			ItemTreeData.push_back(listinfo);
			break;
		/*case 5:
			if(TSMediatorClass::IsOneSourceMode)
			{
				CatagoryTreeData.push_back(listinfo);
			}
			else
			{
				ProjectTreeData.push_back(listinfo);
			}			
			break;*/
		/*case 6:
			if(TSMediatorClass::IsOneSourceMode)
				CatagoryTreeData.push_back(listinfo);
			else
				ProjectTreeData.push_back(listinfo);
			break;*/
	}
}

TSTreeInfo TSTreeData::getData(int16 TreeType, int16 index)
{
	TSTreeInfo listinfo;
	switch(TreeType)
	{
		//case 1:
		//	listinfo = this->PFTreeData[index];
		//	break;
		//case 2:
		//	listinfo = this->PGTreeData[index];
		//	break;
		case 3:
			listinfo = this->PRTreeData[index];
			break;
		case 4:
			listinfo = this->ItemTreeData[index];
			break;
		/*case 5:
			if(TSMediatorClass::IsOneSourceMode)
			{
				listinfo = this->CatagoryTreeData[index];
			}
			else
			{
				listinfo = this->ProjectTreeData[index];
			}
			break;*/
	}
	return listinfo;
}

int32 TSTreeData::returnListVectorSize(int16 TreeType)
{//CA(__FUNCTION__);
	switch(TreeType)
	{
		//case 1:
		//	return PFTreeData.size();
		//	break;
		//case 2:
		//	return PGTreeData.size();
		//	break;
		case 3:
			return static_cast<int32>(PRTreeData.size());
			break;
		case 4:
			return static_cast<int32>(ItemTreeData.size());
	//	case 5:
	//		if(TSMediatorClass::IsOneSourceMode)
	//		{
	//			return CatagoryTreeData.size();
	//		}
	//		else
	//			return ProjectTreeData.size();
	//		break;
	}
	return 0;
}
	
void TSTreeData::clearVector(int16 TreeType)
{
	//CA(__FUNCTION__);
	switch(TreeType)
	{
		//case 1:
		//	/*PFTreeData.erase
		//		(PFTreeData.begin(), PFTreeData.end());*/
		//	PFTreeData.clear();
		//	break;
		//case 2:
		//	/*PGTreeData.erase
		//		(PGTreeData.begin(), PGTreeData.end());*/
		//	PGTreeData.clear();
		//	break;
		case 3:
			/*PRTreeData.erase
				(PRTreeData.begin(), PRTreeData.end());*/
			PRTreeData.clear();
			break;
		case 4:
			/*ItemTreeData.erase
				(ItemTreeData.begin(), ItemTreeData.end());*/
			ItemTreeData.clear();
			break;
	//	case 5:
	//		/*ProjectTreeData.erase
	//			(ProjectTreeData.begin(), ProjectTreeData.end());*/
	//		if(TSMediatorClass::IsOneSourceMode)
	//		{
	//			CatagoryTreeData.clear();
	//		}
	//		else
	//			ProjectTreeData.clear();
	//		break;
	}
}

void TSTreeData::setIsBoxDragged(int16 TreeType, int16 index, bool16 flag)
{//CA(__FUNCTION__);
	TSTreeInfo listinfo;
	TSTreeInfoVect::iterator myIterator;

	switch(TreeType)
	{
		//case 1:
		//	for (myIterator = PFTreeData.begin(); myIterator != PFTreeData.end(); myIterator++)
		//	{
		//		if((*myIterator).index == index)
		//		{
		//			(*myIterator).isDragged = flag;
		//		}
		//	}
		//	break;
		//case 2:
		//	for (myIterator = PGTreeData.begin(); myIterator != PGTreeData.end(); myIterator++)
		//	{
		//		if((*myIterator).index == index)
		//		{
		//			(*myIterator).isDragged = flag;
		//		}
		//	}
		//	break;
		case 3:
			for (myIterator = PRTreeData.begin(); myIterator != PRTreeData.end(); myIterator++)
			{
				if((*myIterator).index == index)
				{
					(*myIterator).isDragged = flag;
				}
			}
			break;
		case 4:
			for (myIterator = ItemTreeData.begin(); myIterator != ItemTreeData.end(); myIterator++)
			{
				if((*myIterator).index == index)
				{
					(*myIterator).isDragged = flag;
				}
			}
			break;
	//	case 5:
	//		if(TSMediatorClass::IsOneSourceMode)
	//		{
	//			for (myIterator = CatagoryTreeData.begin(); myIterator != CatagoryTreeData.end(); myIterator++)
	//			{
	//				if((*myIterator).index == index)
	//				{
	//					(*myIterator).isDragged = flag;
	//				}
	//			}
	//		}
	//		else
	//		{
	//			for (myIterator = ProjectTreeData.begin(); myIterator != ProjectTreeData.end(); myIterator++)
	//			{
	//				if((*myIterator).index == index)
	//				{
	//					(*myIterator).isDragged = flag;
	//				}
	//			}
	//		}
	//		break;
	}
}

void TSTreeData::changeData(int32 treeType , int32 index , TSTreeInfo treeInfo)
{
	switch(treeType)
	{
		case 3:
			PRTreeData[index] = treeInfo;
			break;
		case 4:
			ItemTreeData[index] = treeInfo; 
			break;
	}
}