//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifdef __ODFRC__

// English string table is defined here

resource StringTable (kSDKDefStringsResourceID + index_enUS)
{
        k_enUS,									// Locale Id
        kEuropeanWinToMacEncodingConverter,		// Character encoding converter (irp) I made this WinToMac as we have a bias to generate on Win...
        {
        	 // ----- Menu strings
                kTSCompanyKey,					kTSCompanyValue,
                kTSAboutMenuKey,				kTSPluginName "[US]...",
                kTSPluginsMenuKey,				kTSPluginName "[US]",
	
                kSDKDefAboutThisPlugInMenuKey,			kSDKDefAboutThisPlugInMenuValue_enUS,

                // ----- Command strings

                // ----- Window strings

                // ----- Panel/dialog strings
                kTSPanelTitleKey,				kTSPluginName ,
                kTSStaticTextKey,				kTSPluginName "[US]",


		// ----- Misc strings
                kTSAboutBoxStringKey,			kTSPluginName " [US], version " kTSVersion " by " kTSAuthor "\n\n" kSDKDefCopyrightStandardValue "\n\n" kSDKDefPartnersStandardValue_enUS,
				kTSListTableNameKey,			"List/Table Name : ",
				kTSStencilNaleKey,				"Stencil Name : ",
				kTSUpdateDateKey,				"Update Date : ",
				kTSReloadKey,					"Reload",
				kTSSprayKey,					"Spray",
				kTSPreviewKey,					"Preview",
				kTSEditStencilkey,				"Edit Stencil",
		
        }

};

#endif // __ODFRC__
