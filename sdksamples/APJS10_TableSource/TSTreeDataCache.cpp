#include "VCPlugInHeaders.h"
#include "TSTreeDataCache.h"
#include "CAlert.h"
#include "vector"

map<int32, TSDataNode>* TSTreeDataCache::dataCache=NULL;

TSTreeDataCache::TSTreeDataCache()
{
	if(dataCache)
		return;
	dataCache=new map<int32, TSDataNode>;
}

bool16 TSTreeDataCache::getAllIdForLevel(int level, int32& numIds, vector<int32>& idList)
{
	int32 flag=0;
	TSDataNode pNode;
	if(!dataCache)
	{
		dataCache=new map<int32, TSDataNode>;
		return kFalse;
	}
	map<int32, TSDataNode>::iterator mapIterator;

	for(mapIterator=dataCache->begin(); mapIterator!=dataCache->end(); mapIterator++)
	{
		pNode=(*mapIterator).second;
		/*if(pNode.getLevel()==level)
		{
			flag++;
			idList.push_back(pNode.getPubId());
		}*/
	}
	numIds=flag;
	return kTrue;
}

bool16 TSTreeDataCache::isExist(int32 id, TSDataNode& pNode)
{
	if(!dataCache)
	{
		dataCache = new map<int32, TSDataNode>;
		return kFalse;
	}

	map<int32, TSDataNode>::iterator mapIterator;

	mapIterator=dataCache->find(id);
	if(mapIterator==dataCache->end())
		return kFalse;

	pNode=(*mapIterator).second;
	pNode.setHitCount(pNode.getHitCount()+1);//Increase hit count by 1
	return kTrue;
}

bool16 TSTreeDataCache::add(TSDataNode& pNodeToAdd)
{
	if(!dataCache)
		dataCache=new map<int32, TSDataNode>;
	map<int32, TSDataNode>::iterator mapIterator;
	TSDataNode firstNode, anyNode;
	int32 removalId=-1;

	if((dataCache->size()+1)<dataCache->max_size())//Cache has some space left
	{
		mapIterator=dataCache->find(pNodeToAdd.getPubId());
		if(mapIterator==dataCache->end())//Not found. Insert it!!
		{
			dataCache->insert(map<int32, TSDataNode>::value_type(pNodeToAdd.getPubId(), pNodeToAdd));
			return kTrue;
		}
		//Node exists...Increase the hit count
		firstNode=(*mapIterator).second;
		(*mapIterator).second.setHitCount(firstNode.getHitCount()+1);
		return kTrue;
	}

	CAlert::ErrorAlert("System is low on resources. Please close some applications and proceed.");

	//We do not have any space left...Remove the element which is MOST accessed

	mapIterator=dataCache->begin();
	firstNode=(*mapIterator).second;

	removalId=firstNode.getPubId();
	
	for(mapIterator=dataCache->begin(); mapIterator!=dataCache->end(); mapIterator++)
	{
		anyNode=(*mapIterator).second;

		if(anyNode.getHitCount()>MAX_HITS_BEFORE_STALE)//Save some iterations
		{
			removalId=anyNode.getPubId();			
			break;
		}
		if(anyNode.getHitCount()>firstNode.getHitCount())
			removalId=anyNode.getPubId();			
	}
	dataCache->erase(removalId);
	dataCache->insert(map<int32, TSDataNode>::value_type(pNodeToAdd.getPubId(), pNodeToAdd));
	return kTrue;
}

bool16 TSTreeDataCache::clearMap(void)
{
	if(!dataCache)
		return kFalse;
	dataCache->erase(dataCache->begin(), dataCache->end());
	delete dataCache;
	dataCache=NULL;
	return kTrue;
}

bool16 TSTreeDataCache::isExist(int32 parentId, int32 sequence, TSDataNode& pNode)
{
	int flag=0;
	if(!dataCache)
	{
		dataCache=new map<int32, TSDataNode>;
		return kFalse;
	}
	map<int32, TSDataNode>::iterator mapIterator;

	for(mapIterator=dataCache->begin(); mapIterator!=dataCache->end(); mapIterator++)
	{
		pNode=(*mapIterator).second;
		if(pNode.getParentId()==parentId)
		{
			if(pNode.getSequence()==sequence)
			{
				pNode=(*mapIterator).second;
				(*mapIterator).second.setHitCount(pNode.getHitCount()+1);
				flag=1;
				break;
			}
		}
	}

	if(!flag)
		return kFalse;
	return kTrue;
}