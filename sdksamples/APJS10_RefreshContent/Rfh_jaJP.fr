//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifdef __ODFRC__


// Japanese string table is defined here

resource StringTable (kSDKDefStringsResourceID + index_jaJP)
{
        k_jaJP,	// Locale Id
        0,		// Character encoding converter

        {
        	// ----- Menu strings
                kRfhCompanyKey,					kRfhCompanyValue,
                kRfhAboutMenuKey,					kRfhPluginName "[JP]...",
                kRfhPluginsMenuKey,				kRfhPluginName "[JP]",

                kSDKDefAboutThisPlugInMenuKey,			kSDKDefAboutThisPlugInMenuValue_jaJP,

                // ----- Command strings

                // ----- Window strings

                // ----- Panel/dialog strings
                kRfhPanelTitleKey,				kRfhPluginName "[JP]",
                kRfhStaticTextKey,				kRfhPluginName "[JP]",

               // ----- Error strings

                // ----- Misc strings
                kRfhAboutBoxStringKey,			kRfhPluginName " [JP], version " kRfhVersion " by " kRfhAuthor "\n\n" kSDKDefCopyrightStandardValue "\n\n" kSDKDefPartnersStandardValue_jaJP,
                
                kRfhFrameRefreshStringKey, "Frame Refresh",
                kRfhUniqueAttrStringKey, "Unique Attributes",
                kRfhUpdateDocWithDeletesStringKey, "Update Doc with Deletes",
                kRfhCatlogRefreshStringKey, "Catalog Refresh",
                kRfhFrameRefresh2StringKey, " Frame Refresh " ,
                kRfhSelectStringKey, "Select:",
                kRfhSelect2StringKey, "--Select--",
                kRfhSelectedFramesStringKey, "Selected Frame(s)",
                kRfhRefreshStringKey, "Refresh  :",
                kRfhLinksRefreshStringKey, "Content",
                kRfhFullRefreshStringKey, "Structure",
                kRfhBlankStringKey, "",	
                kRfhCatalogFullRefreshStringKey, "Catalog Full Refresh",
                kRfhSelectFieldsStringKey, "Please select the fields you would like to refresh :",	
                kRfhSelectAllStringKey, "Select all",
                kRfhPreferenceStringKey, "Preference",
                kRfhUpdatedocumentwithdeletesStringKey, "Update document with deletes",
                kRfhRefreshTableStringKey, "Refresh Table :",
                kRfhByCellStringKey, "By cell",
                kRfhFullTableStringKey, "Full table",
                kRfhBlankStringKey, "",	
                kRfhBackupStringKey, "At this time, please backup all your publishing documents before proceeding. Changes made through this process are NOT  reversible.", 	
                kRfhDocumentDirectoryStringKey, "The document directory is as below :",
                kRfhSelectModeStringKey, "Select The Mode :",
                kRfhInThisModeStringKey, "In this mode,the document will be processed without user  intervention.",
                kRfhInteractiveModeStringKey, "Interactive  Mode � Beta!",
                kRfhOneFrameStringKey, "In this mode,the document will be processed one frame at a  time.",
                kRfhNextStringKey, "Next >",
                kRfhSelectContentToRefreshStringKey, "Select the content to refresh : ",
                kRfhRefreshDocumentStringKey, " Refresh Document: ",
              // kRfhUpdateProductsStringKey, "Update Products, Items from ONEsource.",
                kRfhUpdateProductsStringKey, "Refresh item and group copy.",
               // kRfhUpdateItemStringKey, "Update Item and Product Images. If you select this option,",
                kRfhUpdateItemStringKey, "Refresh item and group images.",
               // kRfhImageRelinkStringKey, " all Item and Product images will be re-linked.",
                kRfhImageRelinkStringKey, " ",
                //kRfhPleaseindicateStringKey, "Please indicate if you want to delete Items and Products from ",
                kRfhPleaseindicateStringKey, "Remove frames of items and groups,",                
                //kRfhPublishingStringKey, "the publishing documents if they are deleted from the publication ",
                kRfhPublishingStringKey, " which have been deleted from the database.",
               // kRfhPublishingStringKey, " ",
               // kRfhInOneSourceStringKey, "in ONEsource.",
                kRfhInOneSourceStringKey, "",
               // kRfhRefreshTableStringKey, "Refresh Table",
                kRfhRefreshTableStringKey, "Refresh lists and tables.",
                kRfhPleaseSelectStringKey, "Please select the fields you would like to refresh: ",
                kRfhSummaryOfUpdatesandDeletesStringKey, " Summary of Updates and Deletes",
                kRfhItemsFiledUpdatedStringKey, "Item fields updated: ",
                kRfhZeroStringKey, "0",	
                kRfhItemImagesReLinkedStringKey, "Item images re-linked:",
                kRfhItemGroupFieldUpdatedStringKey, "Item group fields updated: ",
                kRfhItemGroupImagesReLinkedStringKey, "Item group images re-linked: ",
                kRfhItemFramesDeletedStringKey, "Item frames deleted: ",
                kRfhItemImagesDeletedStringKey, "Item images deleted: ",
                kRfhItemGroupImagesDeletedStringKey, "Item group images deleted: ",
                kRfhProcessStringKey, "Process : ",
                kRfhProcessDeletesStringKey, "Process Deletes",
                kRfhProcessUpdatesStringKey, "Process Updates",
                kRfhProcessNewsStringKey, "Process New(s)",
                kRfhAnalyzeStringKey, "Analyze",
                kRfhOKStringKey, " &Ok ",
                kRfhCancelStringKey, "&Cancel",
                kRfhSelectRefreshModeStringKey, "Select the refresh mode :  ",
                kRfhSpaceStringKey, "  ",
                kRfhRefreshCatalogStringKey, "Refresh Catalog",
                kRfhRefreshingDocumentStringKey, "Refreshing Document : ",
                kRfhItemGroupFramesDeletedStringKey, "Item group frames deleted: ",
                
                kRfhTwowaystorefreshStringKey, "There are two ways to refresh a catalog.Please select the",
                kRfhselectmethodStringKey, "method you want based on the description provided for each ",
                kRfhMethodStringKey, "method.",
                kRfhSelectrefreshmodeStringKey, "Select refresh mode:",
                kRfhContentonlyStringKey, " Content Only",
                kRfhUseofcontentonlyoption1StringKey, "In this refresh mode, content on the page will be refreshed ",
                kRfhUseofcontentonlyoption12StringKey, "if it is linked to the database. This mode ignores event  ",
                kRfhUseofContentOnlyoption123StringKey, "relationships.  For example, item sequence within a list  ",
                kRfhUseofContentonlyoption1234StringKey, "will be ignored.  ",
                kRfhContentandStructureStringKey, " Content and Structure",
                kRfhUseofStructureoptionStringKey, "In this refresh mode, content on the page along with ",
                kRfhUseOfstructure12StringKey, "event level information will be refreshed. For example, item ",
                kRfhUseofStructureoption123StringKey, "sequence within a list will also be refreshed. ",
                kRfhCurrentPageStringKey, "Current Page",
                kRfhPageRangeStringKey, "Page  Range",
                
                  SilentModeKey, "Silent Mode",
                  ContentandStructureKey, "Content and Structure",
                  RefreshDocumentKey, "Refresh Document: ",
                  UpdateItemAndItemGKey, "Update Items and Item Groups from the Apsiva.",
                  UpdateItemAndIGImagesKey, "Update Item and Item Group Images. If you select this option,",
                  ImagesRelinkedKey, " all Item and Item Group images will be re-linked.",

        }

};

#endif // __ODFRC__
