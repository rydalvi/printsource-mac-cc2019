
#include "VCPlugInHeaders.h"
#include "TableStyleUtils.h"
#include "IScrapItem.h"
#include "ICommand.h"
#include "ISelectionManager.h"
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
//#include "ITextFrame.h"
#include "ITextModel.h"
#include "ITableModelList.h"
#include "ITableModel.h"
#include "ITableAttrAccessor.h"
#include "ITableAttrModifier.h"
#include "ITableTextContent.h"
#include "ITableSelectionSuite.h"
#include "IConcreteSelection.h"
#include "ITableTextSelection.h"
#include "ITextSelectionSuite.h"
#include "ITextTarget.h"
#include "IParagraphStyleSuite.h"
#include "ITableAttrUID.h"
#include "ITableTextContent.h"
#include "AttributeBossList.h"
#include "ITableSuite.h"
#include "IPMUnknown.h"
#include "ILayoutUIUtils.h"
//#include "LPTLibraryService.h"
#include "TextEditorID.h"
#include "IActiveContext.h"
#include "MediatorClass.h"
//#include "ILibrarySuite.h"
//#include "ILibraryAssetCollection.h"
//#include "ILibraryAssetIterator.h"
//#include "ILibraryAsset.h"
//#include "ILibraryAssetMetaData.h"

#include "IFrameUtils.h"
#include "IDocument.h"
//#include "ILPTContext.h"

#include "ISpreadLayer.h"
#include "IDocumentLayer.h"
#include "ISpread.h"
#include "ISpreadList.h"
#include "ILayerUtils.h"

#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "TagStruct.h"
#include "ITagReader.h"
#include "vector"
#include "ITextAttributeSuite.h"
#include "ISwatchUtils.h"
#include "ISwatchList.h"
#include "IGraphicFrameData.h"
//#include "ILibraryService.h"
//#include "ILibraryCmdUtils.h"
#include "IMultiColumnTextFrame.h"
#include "ITextFrameColumn.h"
#include "IHierarchy.h"
#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("TableStyleUtils.cpp")		
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

//#define CA(X) CAlert::InformationAlert(X)

////////////////////Test Temping//////////////////////////////////////////////////////////
#include "SDKLayoutHelper.h"

/////Global Pointers
extern ITagReader* itagReader;
///////////////

class StyleUIDs
{
public:
	UID ColourUID;
	UID TestStyleUID;
	int32 rowno;
	int32 colno;
	AttributeBossList ab;
	bool16 CanApplyTextStyle;
StyleUIDs()
{
	ColourUID= kInvalidUID;
	TestStyleUID = kInvalidUID;
	rowno = -1;
	colno = -1;
	CanApplyTextStyle = kFalse;
	//AttributeBossList ab=nil;
}

};
typedef vector<StyleUIDs> TableColStyleVector;

class rowStyle
{
public:
	int32 RowNo;
	TableColStyleVector stylevectorObj;

	rowStyle()
	{
		RowNo = -1;
		stylevectorObj.clear();
	}
};
typedef vector<rowStyle> TableStyleVector;

TableStyleVector SourceTableStyleVector;
int32 SourceTableRows= -1;
int32 SourceTablecols= -1;
AttributeBossList *attrBossList=NULL;
UID styleUID;

TableStyleUtils::TableStyleUtils()
{
}
TableStyleUtils::~TableStyleUtils()
{
}
int TableStyleUtils::deleteThisBox(UIDRef boxUIDRef)
{
	
	InterfacePtr<IScrapItem> scrap(boxUIDRef, UseDefaultIID());
	if(scrap==nil)
		return 0;
	InterfacePtr<ICommand> command(scrap->GetDeleteCmd());
	if(!command)
		return 0;
	command->SetItemList(UIDList(boxUIDRef));
	if(CmdUtils::ProcessCommand(command)!=kSuccess)
		return 0;
	return 1;
}
void TableStyleUtils::SetTableModel(bool16 isSourceTable, int32 TableIndex)
{
	//CA("In Set Table Model");
	
	InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
	if(!iSelectionManager)
	{	
			//CA("Selection Manager is Null ");
			return;
	}
		
	InterfacePtr<ITextMiscellanySuite> txtMisSuite(iSelectionManager,UseDefaultIID()); 
	if(!txtMisSuite)
	{
		//CA("Suite Pointer is null");
		return;
	}
	UIDList	selectUIDList;
	txtMisSuite->GetUidList(selectUIDList);
	if(selectUIDList.Length()<=0)
	{
		//CA("No selected item");

	}
	else
	{
		for(int32 j=0; j<selectUIDList.Length();j++)
		{
			//TagReader tReader;
			TagStruct tagInfo;
			InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
			if(!itagReader){
			return ;
			}
			TagList tList=itagReader->getTagsFromBox(selectUIDList.GetRef(j));
			if(tList.size()==0){
				tList = itagReader->getFrameTags(selectUIDList.GetRef(j));
			}
			if(tList.size()==0)
			{
				selectUIDList.Remove(j);
				j--;
				continue;
			}
			tagInfo=tList[0];
			if(tagInfo.isProcessed || tagInfo.parentId!=-1 || tagInfo.numValidFields < NUM_TAGS_FIELDS)
			{
				selectUIDList.Remove(j);
				j--;
			}

			if(tagInfo.isTablePresent== kTrue)
			{
				//CA("Table Present ");								
				/*InterfacePtr<IPMUnknown> unknown(tagInfo.curBoxUIDRef, IID_IUNKNOWN);
				UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
				UID textFrameUID = kInvalidUID;
				InterfacePtr<IGraphicFrameData> graphicFrameDataOne(tagInfo.curBoxUIDRef, UseDefaultIID());
				if (graphicFrameDataOne) 
				{
					textFrameUID = graphicFrameDataOne->GetTextContentUID();
				}
				if (textFrameUID == kInvalidUID)
				{
					//CA("TextFrame UID is Invalid");
					return;
				}
/*				////////	CS3 Change
				InterfacePtr<ITextFrame> textFrame(tagInfo.curBoxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
				if (textFrame == nil)
				{
					//CA("Text Frame is Null");
					return;
				}
*/				
/////////////////	Added by Amit
				
				InterfacePtr<IHierarchy> graphicFrameHierarchy(tagInfo.curBoxUIDRef, UseDefaultIID());
				if (graphicFrameHierarchy == nil) 
				{
					ASSERT(graphicFrameHierarchy);
					return;
				}
						
				InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
				if (!multiColumnItemHierarchy) {
					ASSERT(multiColumnItemHierarchy);
					return;
				}

				InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
				if (!multiColumnItemTextFrame) {
					ASSERT(multiColumnItemTextFrame);
					//CA("Its Not MultiColumn");
					return;
				}
				InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
				if (!frameItemHierarchy) {
					return;
				}

				InterfacePtr<ITextFrameColumn>textFrame(frameItemHierarchy, UseDefaultIID());
				if (!textFrame) {
					//CA("!!!ITextFrameColumn");
					return;
				}


/////////////////	End
				InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
				if (textModel == nil)
				{
					//CA("Text Mode is Null");
					return;
				}

				InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
				if(tableList==nil)
				{
					//CA("Table Model List is Null");
					return;
				}

				int32	tableIndex = tableList->GetModelCount() - 1;//2
				
				if(tableIndex<0) //This check is very important...  this ascertains if the table is in box or not.
				{
					//CA("No Table in Frame");
					return;
				}

				InterfacePtr<ITableModel> tblModel(tableList->QueryNthModel(TableIndex));	//tableIndex
				if(!tblModel)
				{
					//CA("No Table Model Found");
					return;
				}

				//tblModel->AddRef();

				if(isSourceTable)
				{	
					//CA("13");	
					this->sourceTblModel=tblModel;
//CA("14");	
					InterfacePtr<ITableAttrAccessor>tblAcsr(tblModel,UseDefaultIID());
					if(!tblAcsr)
					{
						//CA("ITableAttrAccessor for source is null");
						return;
					}
	//CA("15");					
					//styleUID=tblAcsr->GetTableStyle();
				
				}
				else
				{
					//CA("16");	
					this->targetTblModel=tblModel;
					//CA("17");	
				}

			}

			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
		}
	}
}

//=====================================Premal========================================

bool16 TableStyleUtils::GetCellTextStyleUID(InterfacePtr<ITableModel>tblModel,int32 row,int32 col,UID &textStyleUID,AttributeBossList* &ab)
{
	
	//CA("In Get Cell Text Style");
	InterfacePtr<ISelectionManager> iSelectionManager (Utils<ISelectionUtils> ()->GetActiveSelection ());
	if(!iSelectionManager)
	{
		//CA("Source Selection Manager is null");
		return kFalse;
	}
	//CA("Got ISelectionManager");
	if(!tblModel)
	{
		//CA("Source Table Model in Function is null");
		return kFalse;
	}
	//RowRange rowRange;
	//rowRange=tblModel->GetTotalRows();
	//ColRange colRange;
	//colRange=tblModel->GetTotalCols();
	//int32 totRows=rowRange.count;
	//int32 totCols=colRange.count;
	//PMString m("REturning : ");
	//m.AppendNumber(totRows);
	//m.AppendNumber(totCols);
	//m.AppendNumber(row);
	//m.AppendNumber(col);
	//CA(m);
	//if(row>=totRows)
	//{
	//	CA("Returning");
	//	return kFalse;
	//}
	//if(col>=totCols)
	//{
	//	CA("REturning col more");
	//	return kFalse;
	//}
	////CA("Got Table Model");
	//	
	
	InterfacePtr<ITableTextContent>tblTextContent(tblModel,UseDefaultIID());
	if(!tblTextContent)
	{
		//CA("Source ITableTextContent in function is null");
		return kFalse;
	}
	//CA("Got TableTextContent Model");
	InterfacePtr<ITextModel>tblTextModel(tblTextContent->QueryTextModel(),UseDefaultIID());
	if(!tblTextModel)
	{
		//CA("Source Text Model in fucntion is null");
		return kFalse;
	}
	//CA("Got Text Model");
	//InterfacePtr<IFrameList>frmList(tblTextModel->QueryFrameList());//,UseDefaultIId());
	//if(!frmList)
	//{
	//	CA("Frame List in Text Style functin is null");
	//	return;
	//}
	//CA("Got Frame List");
	
	//tblTextModel->AddRef(); //added by premal	
	InterfacePtr<ITableSelectionSuite>tblSelSuite(static_cast<ITableSelectionSuite *>(Utils<ISelectionUtils>()->QueryActiveTableSelectionSuite()));//,UseDefaultIID());
	if(!tblSelSuite)
	{
		//CA("Source Table Selection suite is null");
		return kFalse;
	}
	//CA("Got Table Selection Suite");
	GridArea ga(row,col,row,col);
	//tblSelSuite->DeselectAll();
	//tblSelSuite->Select(tblModel,ga,tblSelSuite->kAddTo,kTrue);
	//CA("Table cell is selected");
		
	InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
	if(!pTextSel)
	{
		//CA("Source IConcrete Selection is null");
		return kFalse;
	}
	//CA("Got Concrete Selection");
	InterfacePtr<ITableTextSelection>tblTxtSel(pTextSel,UseDefaultIID());
	if(!tblTxtSel)
	{
		//CA(" Source Table Text Selection is null");
		return kFalse;
	}
	//CA("Got TableText selection");

	GridAddress gadr(row,col);
	tblTxtSel->SelectTextInCell(tblModel,gadr);
	
	int32 fc,lc;
	fc=tblTxtSel->GetIndexOfFirstCharInCell();
	lc=tblTxtSel->GetIndexOfLastCharInCell();
	//CA("Text in cell is selected");
	PMString lChar("First and last char index of cell: ");
	lChar.AppendNumber(fc);
	lChar.AppendNumber(lc);
	//CA(lChar);
		
	InterfacePtr<ITextSelectionSuite>txtSelSuite(Utils<ISelectionUtils>()->QueryTextSelectionSuite(iSelectionManager)/*,UseDefaultIID()*/);
	if(!txtSelSuite)
	{
		//CA("Source Text Selectin suite is null");
		return kFalse;
	}
	//CA("Got Text Selection Suite");					
	InterfacePtr<ITextTarget> pTextTarget(pTextSel, UseDefaultIID()); 

	if(!pTextTarget)
	{
		//CA("Source Text Target is null");
		return kFalse;;
	}
	//CA("Got Text Target");
	if((lc-fc) == 0)
		return kFalse;

	RangeData rData(fc,lc);
	txtSelSuite->SetTextSelection(::GetUIDRef(tblTextModel),rData,Selection::kScrollIntoView,nil);
	pTextTarget->SetTextFocus(::GetUIDRef(tblTextModel),rData);
	//CA("We Got Text Focus");
	InterfacePtr<IParagraphStyleSuite> paraStyle(txtSelSuite,UseDefaultIID());
	if(!paraStyle)
	{
		//CA("Source Paragraph Style Suite is null");
		return kFalse;
	}
	/*CA("Getting ParaStyle");
	textStyleUID=paraStyle->CreateStyle();
	if(textStyleUID==kInvalidUID)
	{
		CA("Text style UID is null");
		return kFalse;
	}
	CA("Got Style UID");*/
	//abList=paraStyle->NewStyleAttributeList();
	
	ab=paraStyle->NewStyleAttributeList();
	if(!ab)
	{
		//CA("Attribute Boss List is null in GetSTyle");
	}
	return kTrue;
}
//====================================================================================
bool16 TableStyleUtils::GetCellColourUID(InterfacePtr<ITableModel>tblModel,int row,int col,UID &colorUID)
{
	//CA("In Get Cell Color");
	if(!tblModel)
	{
		//CA("Table Model in Function is null");
		return kFalse;
	}

	/*RowRange rowRange;
	rowRange=tblModel->GetTotalRows();
	ColRange colRange;
	colRange=tblModel->GetTotalCols();
	int32 totRows=rowRange.count;
	int32 totCols=colRange.count;

	if(totRows<row || totCols<col)
	{
		return kFalse;
	}*/

	//CA("Got Table Model");

	InterfacePtr<ITableSelectionSuite>tblSelSuite(static_cast<ITableSelectionSuite *>(Utils<ISelectionUtils>()->QueryActiveTableSelectionSuite()));//,UseDefaultIID());
	if(!tblSelSuite)
	{
		//CA("Source Color Table Selection suite is null");
		return kFalse;
	}
	//CA("Got Table Selection Suite");
	GridArea ga(row,col,row,col);
	//tblSelSuite->Select(tblModel,ga,tblSelSuite->kAddTo,kTrue);
	//CA("Table cell is selected");
	////////////testing code for getting attribute value/////////////////////////////
	
	InterfacePtr<ITableSuite> tableSuite(static_cast<ITableSuite* >( Utils<ISelectionUtils>()->QuerySuite(ITableSuite::kDefaultIID,nil))); 
	if(!tableSuite) 
	{
		//CA("Source Color Tables suite is null for target table");
		return kFalse;
	}

	InterfacePtr<const IPMUnknown> attr(tableSuite->QueryCellAttribute(kCellAttrFillColorBoss));
	if(!attr)
	{
		//CA("Source COlor IPMUnknown is null while getting cell attributes");
		return kFalse;
	}
	InterfacePtr<const ITableAttrUID> clrUID(attr, UseDefaultIID());
	if(!clrUID)
	{
		//CA("Source Color ITableAttrUID from source is null");
		return kFalse;
	}
	colorUID=clrUID->Get();

	/*PMString msg("IN Get color uid: ");
	msg.AppendNumber(colorUID.Get());
	CA(msg);*/
	return kTrue;
}
//===================================================================================
bool16 TableStyleUtils::SetCellColourUID(InterfacePtr<ITableModel>tblModel,int row,int col,UID colorUID)
{	
	if(!tblModel)
	{
		//CA("Target Table Model is null");
		return kFalse;
	}
		
	/*InterfacePtr<ITableTextContent>tblTextContent(tblModel,UseDefaultIID());
	if(!tblTextContent)
	{
		CA("Target ITableTextContent in function is null");
		return kFalse;
	}*/
	//CA("Got TableTextContent Model");
	/*InterfacePtr<ITextModel>tblTextModel(tblTextContent->QueryTextModel(),UseDefaultIID());

	if(!tblTextModel)
	{
		//CA("Target Text Model in fucntion is null");
		return kFalse;
	}	*/

	InterfacePtr<ITableSelectionSuite>tblSelSuite(static_cast<ITableSelectionSuite *>(Utils<ISelectionUtils>()->QueryActiveTableSelectionSuite()));//,UseDefaultIID());
	if(!tblSelSuite)
	{
		//CA("Target Table Selection suite is null");
		return kFalse;
	}
	
	//CA("Got Table Selection Suite");
	GridArea ga(row,col,row,col);
	//tblSelSuite->DeselectAll();
	
	//tblSelSuite->Select(tblModel,ga,tblSelSuite->kAddTo,kTrue);
	//CA("Table cell is selected");
	////////////testing code for getting attribute value/////////////////////////////
	
	InterfacePtr<ITableSuite> tableSuite(static_cast<ITableSuite* >( Utils<ISelectionUtils>()->QuerySuite(ITableSuite::kDefaultIID,nil))); 
	if(!tableSuite) 
	{
		//CA("Source Color Tables suite is null for target table");
		return kFalse;
	}

	if(tableSuite->CanApplyCellOverrides())
	{
		//CA("We can Apply Cell Overriedes");
	}
	InterfacePtr<ITableAttrUID> colorAttr(::CreateObject2< ITableAttrUID>(kCellAttrFillColorBoss));

	InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QueryActiveSwatchList());		
	if(iSwatchList==nil)
	{
		//CA("Cannot get swatch list?");
		return kFalse;
	}
	
	UID PaperColorUID = iSwatchList->GetPaperSwatchUID(); //GetNoneSwatchUID();

	colorAttr->Set(PaperColorUID);	
	AttributeBossList attrs1;
	attrs1.ApplyAttribute(colorAttr);
	tableSuite->ApplyCellOverrides(&attrs1);

	colorAttr->Set(colorUID);	
	AttributeBossList attrs;
	attrs.ApplyAttribute(colorAttr);
	tableSuite->ApplyCellOverrides(&attrs);
	//CA("End of color apply");
	return kTrue;
}

//===================================================================================
bool16 TableStyleUtils::SetCellTextStyleUID(InterfacePtr<ITableModel>tblModel,int row,int col,UID textStyleUID,AttributeBossList *ab)
{

	//CA("In apply style uid");
	InterfacePtr<ISelectionManager> iSelectionManager(Utils<ISelectionUtils> ()->GetActiveSelection ());
	if(!iSelectionManager)
	{
		//CA("Target Selection Manager is null");
		return kFalse;
	}
	if(!tblModel)
	{
		//CA("Target Table Model is null");
		return kFalse;
	}
		
	InterfacePtr<ITableTextContent>tblTextContent(tblModel,UseDefaultIID());
	if(!tblTextContent)
	{
		//CA("Target ITableTextContent in function is null");
		return kFalse;
	}
	//CA("Got TableTextContent Model");
	InterfacePtr<ITextModel>tblTextModel(tblTextContent->QueryTextModel()/*,UseDefaultIID()*/);

	if(!tblTextModel)
	{
		//CA("Target Text Model in fucntion is null");
		return kFalse;
	}	
	InterfacePtr<ITableSelectionSuite>tblSelSuite(static_cast<ITableSelectionSuite *>(Utils<ISelectionUtils>()->QueryActiveTableSelectionSuite()));//,UseDefaultIID());
	if(!tblSelSuite)
	{
		//CA("Target Table Selection suite is null");
		return kFalse;
	}
//	//CA("We got table selectoin suite");
	GridArea ga(row,col,row,col);
	//tblSelSuite->DeselectAll();
//	//CA("Deselected All");
	//tblSelSuite->Select(tblModel,ga,tblSelSuite->kAddTo,kTrue);
//	CA("Before concrete selection");
	InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
	if(!pTextSel)
	{
		//CA("Target IConcrete Selection is null");
		return kFalse;
	}
	//CA("Got concrete selection suite");
	InterfacePtr<ITableTextSelection>tblTxtSel(pTextSel,UseDefaultIID());
	if(!tblTxtSel)
	{
		//CA("Target Table Text Selection is null");
		return kFalse;
	}
	//CA("Got table text selection suite");
	GridAddress gadr1(row,col);
	tblTxtSel->SelectTextInCell(tblModel,gadr1);
	
	int32 fc=tblTxtSel->GetIndexOfFirstCharInCell();
	int32 lc=tblTxtSel->GetIndexOfLastCharInCell();
	
	//fc= fc+1;
	//lc= lc-1;
	InterfacePtr<ITextSelectionSuite>txtSelSuite(Utils<ISelectionUtils>()->QueryTextSelectionSuite(iSelectionManager)/*,UseDefaultIID()*/);
	if(!txtSelSuite)
	{
		//CA("Source Text Selectin suite is null");
		return kFalse;
	}
	//CA("got ITextSelection suite");
	RangeData rData1(fc,lc);
	
	txtSelSuite->SetTextSelection(::GetUIDRef(tblTextModel),rData1,Selection::kScrollIntoView,nil);
	
	InterfacePtr<ITextTarget> pTextTarget(pTextSel, UseDefaultIID()); 

	if(!pTextTarget)
	{
		//CA("Target Text Target is null");
		return kFalse;;
	}
	//CA("Got text target");
	pTextTarget->SetTextFocus(::GetUIDRef(tblTextModel),rData1);
	
	//if(!abList)
	if(!ab)
	{
		//CA("Attribute Boss List in ApplyStye is null");
		return kFalse;
	}


	if(ab != NULL)
	{//	 CA("25.5");
			
		InterfacePtr<ITextAttributeSuite>textAttributeSuite
		((ITextAttributeSuite*)Utils<ISelectionUtils>()->QuerySuite(ITextAttributeSuite::kDefaultIID));
		if (textAttributeSuite != nil && textAttributeSuite->CanApplyAttributes())
		{	
			//CA("Apply Style with textAttributeSuite");
			textAttributeSuite->ApplyAttributes (ab,kParaAttrStrandBoss);
		}
// CS3 Change		tblTextModel->ApplyOverrides(kFalse, fc, lc-fc, ab,/*kParaAttrStrandBoss*/ kCharAttrStrandBoss);
		tblTextModel->ApplyOverrides(fc, lc-fc, ab,/*kParaAttrStrandBoss*/ kCharAttrStrandBoss);
		//tblTextModel->ApplyStyle(kFalse, fc,lc-fc, textStyleUID,kParaAttrStrandBoss /*kCharAttrStrandBoss*/,kFalse);
	}
	//tblTextModel->ApplyStyle(kFalse, fc,lc-fc, textStyleUID,/*kParaAttrStrandBoss*/ kCharAttrStrandBoss,kFalse);
	//CA("End of function Style Applied");
	return kTrue;
}
//===================================================================================
bool16  TableStyleUtils::ApplyTableStyle()
{
	
	if(!targetTblModel)
	{
		//CA("Apply Style Table Model is null");
		return kFalse;
	}
	
	RowRange rowRange;
	rowRange=targetTblModel->GetTotalRows();
	ColRange colRange;
	colRange=targetTblModel->GetTotalCols();
	int32 totRows=rowRange.count;
	int32 totCols=colRange.count;	
		
	/*PMString ASD(" totRows : ");
	ASD.AppendNumber(totRows);
	ASD.Append(" totCols : ");
	ASD.AppendNumber(totCols);
	CA(ASD);*/
	//UID	colorUID;
	//UID textStyleUID;
	
	for(int32 row=0;row<totRows;row++)
	{		
		if(row>=SourceTableRows)
			break;
		for(int32 col=0;col<totCols;col++)
		{
			AttributeBossList *ab=nil;
			UID colorUID;
			UID textStyleUID;

			RowRange rrange(row,0);
			ColRange crange(col,0);
			GridArea GAD(rrange, crange);
			//CA("Got Color Style 1");
			if(!targetTblModel->ContainsCompleteCells(GAD))
			{	//CA("Cell not Found ");
				continue;
			}

			if(col>=SourceTablecols)
			{	
				colorUID = SourceTableStyleVector[row].stylevectorObj[SourceTablecols-1].ColourUID;
				textStyleUID = SourceTableStyleVector[row].stylevectorObj[SourceTablecols-1].TestStyleUID; 
				ab = &SourceTableStyleVector[row].stylevectorObj[SourceTablecols-1].ab;
			}
			else
			{
				colorUID = SourceTableStyleVector[row].stylevectorObj[col].ColourUID;
				textStyleUID = SourceTableStyleVector[row].stylevectorObj[col].TestStyleUID; 
				ab = &SourceTableStyleVector[row].stylevectorObj[col].ab;
			}

			
			/*PMString msg("in ApplyStyle Rows and Cols: ");
			msg.AppendNumber(row);
			msg.AppendNumber(col);
			CA(msg);*/
			
			//AttributeBossList *ab=nil;
			//UID colorUID;
			//bool16 success=;
			//CA("Before checking success");
			/*if(success==kTrue)
			{*/

			//GetCellTextStyleUID(sourceTblModel ,row, col,textStyleUID,ab);	
			//GetCellColourUID(sourceTblModel,row, col,colorUID);			
			SetCellTextStyleUID(targetTblModel,row,col,textStyleUID,ab);		
					
			//if(colorUID == kInvalidUID)
			//{
			//	//CA("colorUID == kInvalidUID");
			//	break;
			//}

			//if(col<SourceTablecols)
			//{	//CA("Apply Colour UID");
			//	SetCellColourUID(targetTblModel,row,col,colorUID);
			//}
			
		}//////////end of for loope of cols
		//CA("End of Inner loop");
	}////////end of for loop of rows
	//CA("Ending of ApplyTableStyle");
	return kTrue;
}



//===================================================================================

bool16  TableStyleUtils::GetTableStyle()
{
	//CA("19");
	if(!sourceTblModel)
	{
		//CA("Apply Style Table Model is null");
		return kFalse;
	}
	//CA("20");
	SourceTableStyleVector.clear();
	
	//CA("21");
	RowRange srowRange;
	srowRange=sourceTblModel->GetTotalRows();
	int32 stotRows=srowRange.count;
	ColRange scolRange;
	scolRange=sourceTblModel->GetTotalCols();
	int32 stotCols=scolRange.count;

	SourceTableRows = stotRows;
	SourceTablecols = stotCols;
	//CA("22");

	for(int32 row=0;row<stotRows;row++)
	{	
		rowStyle rowstyleObj;
		rowstyleObj.RowNo= row;
		rowstyleObj.stylevectorObj.clear();
		for(int32 col=0;col<stotCols;col++)
		{
			StyleUIDs styleObj;			
			/*PMString msg("Rows and Cols: ");
			msg.AppendNumber(row);
			msg.AppendNumber(col);
			CA(msg);*/
			styleObj.rowno = row;
			styleObj.colno = col;	
			UID textStyleUID;
			AttributeBossList *ab=nil;		

			//CA("Got Color Style");

			UID colorUID;	
			RowRange rrange(row,0);
			ColRange crange(col,0);
			GridArea GAD(rrange, crange);
			//CA("Got Color Style 1");
			if(!sourceTblModel->ContainsCompleteCells(GAD))
			{	//CA("Cell not Found ");
				styleObj.TestStyleUID =  kInvalidUID;
				styleObj.CanApplyTextStyle = kFalse;
				styleObj.ColourUID = kInvalidUID;
				rowstyleObj.stylevectorObj.push_back(styleObj);
				continue;
			}
			//CA("Got Color Style 2");
			GetCellTextStyleUID(sourceTblModel ,row, col,textStyleUID,ab);
		
			if(ab == NULL)
			{
				//CA("ab = NULL");
				styleObj.TestStyleUID =  textStyleUID;
				styleObj.CanApplyTextStyle = kFalse;
			}
			else
			{
				styleObj.CanApplyTextStyle = kTrue;
				styleObj.TestStyleUID =  textStyleUID;
				styleObj.ab = *ab;
			}

			//CA("Got textStyleUID");
			GetCellColourUID(sourceTblModel,row, col,colorUID);
			styleObj.ColourUID = colorUID;			
			rowstyleObj.stylevectorObj.push_back(styleObj);
						
			if(ab)
			{
				//CA("Deleting ab");
				delete ab;
			}
			
		}//////////end of for loope of cols
		
		SourceTableStyleVector.push_back(rowstyleObj);

	}////////end of for loop of rows
	//CA("Ending of ApplyTableStyle");
	return kTrue;
}
//================================================

void TableStyleUtils::getOverallTableStyle()
{
	InterfacePtr<ITableSelectionSuite>tblSelSuite(static_cast<ITableSelectionSuite *>(Utils<ISelectionUtils>()->QueryActiveTableSelectionSuite()));//,UseDefaultIID());
	if(!tblSelSuite)
	{
		//CA(" Source Table Selection suite is null");
		return ;
	}
	//CA("Got Table Selection Suite");
	
	tblSelSuite->DeselectAll();
	tblSelSuite->Select(sourceTblModel,sourceTblModel->GetTotalArea(),tblSelSuite->kAddTo,kTrue);
	InterfacePtr<ITableAttrAccessor>tblAttrAcsr(sourceTblModel,UseDefaultIID());
	if(!tblAttrAcsr)
	{
		 //CA("Table Attribute Accessor is null");
		 return;
	}
	//styleUID=tblAttrAcsr->GetTableStyle();
	attrBossList=const_cast<AttributeBossList*>(tblAttrAcsr->GetTableAttributesResolved());

}
void TableStyleUtils::setTableStyle()
{
	//CA(__FUNCTION__);
	InterfacePtr<ITableSelectionSuite>tblSelSuite(static_cast<ITableSelectionSuite *>(Utils<ISelectionUtils>()->QueryActiveTableSelectionSuite()));//,UseDefaultIID());
	if(!tblSelSuite)
	{
		//CA(" Source Table Selection suite is null");
		return ;
	}
	//tblSelSuite->AddRef();
	
	
	//tblSelSuite->DeselectAll();
	//tblSelSuite->Select(sourceTblModel,sourceTblModel->GetTotalArea(),tblSelSuite->kAddTo,kTrue);
	//InterfacePtr<ITableAttrAccessor>tblAttrAcsr(sourceTblModel,UseDefaultIID());
	//if(!tblAttrAcsr)
	//{
	//	 CA("Table Attribute Accessor is null");
	//	 return;
	//}
	////styleUID=tblAttrAcsr->GetTableStyle();
	//const AttributeBossList *attrBossList=tblAttrAcsr->GetTableAttributesResolved();
	//tblSelSuite->DeselectAll();
	tblSelSuite->Select(targetTblModel,targetTblModel->GetTotalArea(),tblSelSuite->kAddTo,kTrue);
	
	InterfacePtr<ITableAttrModifier>tblAttrMdfr(targetTblModel,UseDefaultIID());
	if(!tblAttrMdfr)
	{
		//CA("Table Attribute Modifier is null");
		return;
	}
	//tblAttrMdfr->SetTableStyle(styleUID);
	tblAttrMdfr->ApplyTableOverrides(attrBossList);	
	

}

void TableStyleUtils::ApplyStyleToSelectedFrame(PMString styleName,UIDRef &tempDoc)
{
	InterfacePtr<IDocument>idoc(Utils<ILayoutUIUtils>()->GetFrontDocument(),UseDefaultIID());
	if(!idoc)
	{
		//CA("No Document Open");
		return ;
	}
	InterfacePtr<ISelectionManager> iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
	if(!iSelectionManager)
	{ 
		//CA("Selection Manager is Null ");
		return ;
	}

	InterfacePtr<ITextMiscellanySuite> txtMisSuite(iSelectionManager,UseDefaultIID()); 
	if(!txtMisSuite)
	{
		//CA("No Frame selected to apply style");
		return ;
	}
	IActiveContext* myContext1 = /*gSession*/GetExecutionContextSession()->GetActiveContext();  //Cs4
	//myContext1->AddRef();//////////added by Premal
	//InterfacePtr<ILibrarySuite> lobj_libsuite(myContext1->GetContextSelection(), UseDefaultIID()); 
	////CA("5");
	//
	//lobj_libsuite->AddRef();   ///added by Premal
	////DoDialog();
	////CA("Dialog Opened");
	//InterfacePtr<ILibraryAssetCollection> lobj_assetCollection(LPTLibraryService::ilibrary1, IID_ILIBRARYASSETCOLLECTION ); 
	//ILibraryAssetIterator* lobj_assetIterator = lobj_assetCollection->CreateIteratorWithFilter( nil ); 
	//ILibraryAsset* lobj_libraryAsset = lobj_assetIterator->First(); 
	//
	//CA("Before While");
	//int32 index=0;
	//while(lobj_libraryAsset != nil)//added By premal
	//{ 

	//	//PMString lstr_id,ab("Style"),astr_itemname("Style"); 
	//	//lstr_id.AppendNumber(i); //changed by premal
	//	
	//	InterfacePtr<ILibraryAssetMetaData> lobj_assetMetaData(lobj_libraryAsset, IID_ILIBRARYASSETMETADATA ); 
	//	lobj_assetMetaData->AddRef();
	//
	//	if (lobj_assetMetaData->GetName()==styleName) 
	//	{ 
	//		PMString assetIndex("Asset index is: ");
	//		assetIndex.AppendNumber(index);
	//		//CA(assetIndex);
	//		AssetIDList Assetliste; 
	//		LibraryAssetID testid = lobj_libraryAsset->GetID(); 
	//		Assetliste.push_back(testid); 
	//		lobj_libsuite->AddRef(); 
	//		UIDList uidlist1;
	//		//CA("Placing");
	//		if(lobj_libsuite->CanAddPageToLibrary(LPTLibraryService::ilibrary1, myContext1->GetContextView()))
	//		{
	//			//CA("True");
	//			this->SetTableModel(kFalse);
	//			//CA("7");
	//			//lobj_libsuite->DoPlaceLibraryItems(Assetliste,LPTLibraryService::ilibrary1,uidlist1,myContext1->GetContextView()); 
	//			
	//			//////Added by PRemal/////////////////////////////////////////////////
	//			InterfacePtr<IDocument>tDoc(tempDoc,UseDefaultIID());
	//			if(!tDoc)
	//			{
	//				CA("Temp Doc is null");
	//				return;
	//			}
	//			//myContext1->ChangeContextDocument(tDoc);
	//			lobj_libsuite->DoPlaceLibraryItems(Assetliste,LPTLibraryService::ilibrary1,uidlist1,myContext1->GetContextView()); 
	//							
	//			///////////////////////////////////////////////////////////////////////
	//			
	//			//lobj_libsuite->DoPlaceLibraryItems(Assetliste,LPTLibraryService::ilibrary1,uidlist1,ctrlView); 
	//							
	//			//CA("8");
	//			this->SetTableModel(kTrue);
	//			//CA("9");
	//			if(!sourceTblModel)
	//			{
	//				CA("Fail to get source table model");
	//				break;
	//			}
	//			setTableStyle();
	//			//CA("11");
	//			this->ApplyTableStyle();
	//			//CA("12");
	//			deleteThisBox(uidlist1.GetRef(0));
	//			
	//		}
	//		
	//			//CA("False");
	//	} 
	//	delete lobj_libraryAsset;
	//	index++;
	//	lobj_libraryAsset = lobj_assetIterator->Next();//added by premal
	//}
	//CA("End of while");
}


//////////////////////////Temp Testing/////////////////////////////////////////////////
void TableStyleUtils::PlaceToHiddenDoc(UIDRef &openedDoc)
{
	SDKLayoutHelper sdkLHelper;
	UIDRef docUid=sdkLHelper.CreateDocument();
	openedDoc=docUid;
	InterfacePtr<IDocument>doc(docUid,UseDefaultIID());
	if(!doc)
	{
		//CA("Doc is null");
		return;
	}
	InterfacePtr<ISpreadList>spreadList(docUid,UseDefaultIID());
	if(!spreadList)
		CA("Spread List is null");
	int32 spreadCount=spreadList->GetSpreadCount();
	PMString spCount("Spread Count: ");
	spCount.AppendNumber(spreadCount);
	//CA(spCount);
	InterfacePtr<ISpread>spread(docUid.GetDataBase(),spreadList->GetNthSpreadUID(spreadCount-1),UseDefaultIID());
	if(!spread)
		CA("Spread is null");
	//IDocumentLayer *docLayer=Utils<ILayerUtils>()->QueryDocumentActiveLayer (doc);
	InterfacePtr<IDocumentLayer>docLayer(Utils<ILayerUtils>()->QueryDocumentActiveLayer (doc));
	//InterfacePtr<IDocumentLayer>docLayer((const IPMUnknown*)Utils<ILayerUtils>()->QueryDocumentActiveLayer (doc),UseDefaultIID());
	if(!docLayer)
	{
		//CA("Doc Layer is null");
		return;
	}
	InterfacePtr<ISpreadLayer>spreadLayer(spread->QueryLayer(docLayer)/*,UseDefaultIID()*/);
	if(!spreadLayer)
		CA("SpreadLayer is null");
	
	UIDRef spread1=::GetUIDRef(spreadLayer);
	//UIDRef spread2=::GetUIDRef(spread);
	//UIDRef layer=::GetUIDRef(docLayer);
	UIDRef parent(docUid.GetDataBase(),spread1.GetUID());
	if(parent.GetUID()==kInvalidUID)
		CA("Parent UID is invaid");
	PMPoint leftTop(25,25);
	PMPoint rightBottom(150,150);
	PMRect txtFrame(leftTop,rightBottom);
	
	UIDRef tf=sdkLHelper.CreateTextFrame(parent,txtFrame);
	if(tf.GetUID()==kInvalidUID)
		CA("Invalid UID of Text Frame");


	//UIDRef tf1(spread1.GetDataBase(),textFrameUID);
	//InterfacePtr<IPMUnknown> unknown(tf, IID_IUNKNOWN);
	//if(!unknown)
	//{
	//	//CA("Unknown is null");
	//	return;
	//}
	//UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
	UID textFrameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(tf, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	}
	if (textFrameUID == kInvalidUID)
	{
		//CA("TextFrame UID is Invalid");
		return;
	}
/*		////Commented By Amit
	InterfacePtr<ITextFrame>textFrame(tf.GetDataBase(),textFrameUID,UseDefaultIID());
	if(!textFrame)
	{
		//CA("Text Frame is null");
	}
*/	//doc->Save();
	/*InterfacePtr<IDocument>d(docUid,UseDefaultIID());
	if(!d)
		CA("Doc Creation is failed");
	UIDRef *uRef=new UIDRef;*/
}