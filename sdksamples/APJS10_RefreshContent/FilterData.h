#ifndef __FILTERDATA_H__
#define __FILTERDATA _H__

#include"vector"

using namespace std;

class FilterData
{
public:
	
	bool16 isSelected;
	int32 milestone_id;
	

	FilterData()
	{
		isSelected=kTrue;
		milestone_id=-1;
		
	}
};

typedef vector<FilterData> FilterDataList;

#endif