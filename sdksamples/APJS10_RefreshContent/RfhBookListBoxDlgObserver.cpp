#include "VCPlugInHeaders.h"
#include "WidgetID.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IListControlData.h"
#include "IListBoxController.h"
#include "CObserver.h"
#include "RfhID.h"
#include "SystemUtils.h"
//#include "RefreshData.h"
#include "ListBoxHelper.h"
//#include "MediatorClass.h"
#include "IListControlData.h"
#include "IPanelControlData.h"
//Added on 30May by Yogesh
#include "ITriStateControlData.h"
#include "CDialogController.h"
//ended on 30May

#include "CAlert.h"
//#include "IMessageServer.h"

#define CA(x) CAlert::InformationAlert(x)
//--------------
extern K2Vector<PMString> bookContentNames;		
extern K2Vector<bool16>  bookIsSelected;
extern IControlView* midBookListSelectAllControlView;


class RfhBookListBoxDlgObserver : public CObserver
{
public:
	RfhBookListBoxDlgObserver(IPMUnknown *boss);
	~RfhBookListBoxDlgObserver();
	virtual void AutoAttach();
	virtual void AutoDetach();
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
};

CREATE_PMINTERFACE(RfhBookListBoxDlgObserver, kRfhBookListBoxObserverImpl)

RfhBookListBoxDlgObserver::RfhBookListBoxDlgObserver(IPMUnknown* boss)
: CObserver(boss)
{
	
}

RfhBookListBoxDlgObserver::~RfhBookListBoxDlgObserver()
{
}

void RfhBookListBoxDlgObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
	}
}

void RfhBookListBoxDlgObserver::AutoDetach()
{
 	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this, IID_ILISTCONTROLDATA);
	}
}

void RfhBookListBoxDlgObserver::Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy)
{
	InterfacePtr<IPanelControlData> pPanelData(this, UseDefaultIID());
	if(protocol==IID_ILISTCONTROLDATA && theChange==kListSelectionChangedByUserMessage)
	{	
		//CA("ListBoxObserver::Update");
		SDKListBoxHelper listHelper(this, kRfhPluginID);
		IControlView *listBoxControlView  = pPanelData->FindWidget(kRfhBookListBoxWidgetID);

		InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);
		int32 currentlySelectedIndex = listCntl->GetClickItem (); 
		
		if(bookContentNames.size()==0){
			CA("bookContentNames.size()==0kCPDFCheckIconWidgetID");			
			return;
		}
		
		listHelper.CheckUncheckRow(listBoxControlView,currentlySelectedIndex,!bookIsSelected[currentlySelectedIndex]);
		bookIsSelected[currentlySelectedIndex] = !bookIsSelected[currentlySelectedIndex];

		K2Vector<bool16>::iterator itrIsSelected;		
		itrIsSelected = bookIsSelected.begin();

		for(itrIsSelected = bookIsSelected.begin();itrIsSelected != bookIsSelected.end();  itrIsSelected++ )
		{
			if(*itrIsSelected == kFalse)
			{					
				InterfacePtr<ITriStateControlData>selectAllrisetControlData(midBookListSelectAllControlView,UseDefaultIID());
				if(selectAllrisetControlData==nil)
				{
					CA("selectAllCheckbxControlView==nil");
					break;
				}
				selectAllrisetControlData->Deselect(kFalse,kFalse);					
			}				
		}
		bool16 checkedSelectAll = kTrue;
		for(itrIsSelected = bookIsSelected.begin();itrIsSelected != bookIsSelected.end();  itrIsSelected++ )
		{
			if( *itrIsSelected == kFalse)
			{
				checkedSelectAll = kFalse;
				break;						
			}				
		}
		if(checkedSelectAll == kTrue)
		{
			InterfacePtr<ITriStateControlData>selectAllrisetControlData(midBookListSelectAllControlView,UseDefaultIID());
			if(selectAllrisetControlData==nil)
			{
				CA("selectAllCheckbxControlView==nil");
				return;
			}
			selectAllrisetControlData->Select(kFalse,kFalse);
		}

				
	}

}
 
