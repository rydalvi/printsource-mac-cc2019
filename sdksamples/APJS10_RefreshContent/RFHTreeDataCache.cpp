#include "VCPlugInHeaders.h"
#include "RFHTreeDataCache.h"
#include "CAlert.h"
#include <set>
#include "IntNodeID.h"
//#include "vector"

map<double, RfhDataNode>* RFHTreeDataCache::dataCache=NULL;
extern set<NodeID> UniqueNodeIds;

RFHTreeDataCache::RFHTreeDataCache()
{
	if(dataCache)
		return;
	dataCache=new map<double, RfhDataNode>;
}

bool16 RFHTreeDataCache::getAllIdForLevel(int level, int32& numIds, vector<double>& idList)
{
	int32 flag=0;
	RfhDataNode pNode;
	if(!dataCache)
	{
		dataCache=new map<double, RfhDataNode>;
		return kFalse;
	}
	map<double, RfhDataNode>::iterator mapIterator;

	for(mapIterator=dataCache->begin(); mapIterator!=dataCache->end(); mapIterator++)
	{
		pNode=(*mapIterator).second;
		/*if(pNode.getLevel()==level)
		{
			flag++;
			idList.push_back(pNode.getPubId());
		}*/
	}
	numIds=flag;
	return kTrue;
}

bool16 RFHTreeDataCache::isExist(double id, RfhDataNode& pNode)
{
	if(!dataCache)
	{
		dataCache=new map<double, RfhDataNode>;
		return kFalse;
	}

	map<double, RfhDataNode>::iterator mapIterator;

	mapIterator=dataCache->find(id);
	if(mapIterator==dataCache->end())
		return kFalse;

	pNode=(*mapIterator).second;
	pNode.setHitCount(pNode.getHitCount()+1);//Increase hit count by 1
	return kTrue;
}

bool16 RFHTreeDataCache::add(RfhDataNode& pNodeToAdd)
{
	if(!dataCache)
		dataCache=new map<double, RfhDataNode>;
	map<double, RfhDataNode>::iterator mapIterator;
	RfhDataNode firstNode, anyNode;
	double removalId=-1;

	if((dataCache->size()+1)<dataCache->max_size())//Cache has some space left
	{
		mapIterator=dataCache->find(pNodeToAdd.getFieldId());
		if(mapIterator==dataCache->end())//Not found. Insert it!!
		{
			dataCache->insert(map<double, RfhDataNode>::value_type(pNodeToAdd.getFieldId(), pNodeToAdd));
			return kTrue;
		}
		//Node exists...Increase the hit count
		firstNode=(*mapIterator).second;
		(*mapIterator).second.setHitCount(firstNode.getHitCount()+1);
		return kTrue;
	}

	CAlert::ErrorAlert("System is low on resources. Please close some applications and proceed.");

	//We do not have any space left...Remove the element which is MOST accessed

	mapIterator=dataCache->begin();
	firstNode=(*mapIterator).second;

	removalId=firstNode.getFieldId();
	
	for(mapIterator=dataCache->begin(); mapIterator!=dataCache->end(); mapIterator++)
	{
		anyNode=(*mapIterator).second;

		if(anyNode.getHitCount()>MAX_HITS_BEFORE_STALE)//Save some iterations
		{
			removalId=anyNode.getFieldId();			
			break;
		}
		if(anyNode.getHitCount()>firstNode.getHitCount())
			removalId=anyNode.getFieldId();			
	}
	dataCache->erase(removalId);
	dataCache->insert(map<double, RfhDataNode>::value_type(pNodeToAdd.getFieldId(), pNodeToAdd));
	return kTrue;
}

bool16 RFHTreeDataCache::clearMap(void)
{
	if(!dataCache)
		return kFalse;
	dataCache->erase(dataCache->begin(), dataCache->end());
	delete dataCache;
	dataCache=NULL;
	UniqueNodeIds.clear();
	
	return kTrue;
}

bool16 RFHTreeDataCache::isExist(double parentId, int32 sequence, RfhDataNode& pNode)
{
	int flag=0;
	if(!dataCache)
	{
		dataCache=new map<double, RfhDataNode>;
		return kFalse;
	}
	map<double, RfhDataNode>::iterator mapIterator;

	for(mapIterator=dataCache->begin(); mapIterator!=dataCache->end(); mapIterator++)
	{
		pNode=(*mapIterator).second;
		if(pNode.getParentId()==parentId)
		{
			if(pNode.getSequence()==sequence)
			{
				pNode=(*mapIterator).second;
				(*mapIterator).second.setHitCount(pNode.getHitCount()+1);
				flag=1;
				break;
			}
		}
	}

	if(!flag)
		return kFalse;
	return kTrue;
}