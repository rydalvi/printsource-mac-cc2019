//========================================================================================
//  
//  $File: //depot/indesign_4.0/gm/source/sdksamples/basicdialog/RfhDlgDialogController.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2005/03/08 13:31:35 $
//  
//  $Revision: #1 $
//  
//  $Change: 323509 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:

// General includes:
#include "CDialogController.h"
#include "SystemUtils.h"
#include "IBookManager.h"
#include "IBookContentMgr.h"
#include "IBook.h"
#include "IDataLink.h"
#include "ISession.h"
#include "IBookUtils.h"
#include "RefreshData.h"
#include "FileUtils.h"
//#include "BoxReader.h"
#include "SDKLayoutHelper.h"
#include "TagReader.h"
#include "IStrokeAttributeSuite.h"
#include "ISelectionUtils.h"
#include "ILayoutSelectionSuite.h"
#include "IPanelControlData.h"
#include "MediatorClass.h"
#include "ListBoxHelper.h"
#include "Refresh.h"
#include "DocWchUtils.h"
#include "CAlert.h"
#include "ITagReader.h"
#include "IAppFramework.h"
#include "RfhDlgDialogController.h"
#include "SDKUtilities.h"
#include "IDFile.h"
#include "ISpreadList.h"
#include "ISpread.h"
// Project includes:
#include "RfhID.h"
//#include "MedCustomTableScreenValue.h"
#include "CAlert.h"
//#include "IMessageServer.h"
#include "IBookContent.h"
#include "ILayoutUIUtils.h"

#include "IHierarchy.h"
#include "IPageItemTypeUtils.h"

#include "ITreeViewMgr.h"
#include "RFHTreeModel.h"
#include "RFHTreeDataCache.h"
#include "IntNodeID.h"


#define FILENAME			PMString("RfhDialogController.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}


vector<IDFile> BookFileList;
PMString BookName("Rahul_Dalvi270580.indb");

extern RefreshDataList rBookDataList;
RefreshDataList UniqueBookDataList;
extern RefreshDataList OriginalrBookDataList;
extern int GroupFlag;
extern bool16 ISRfreshBookDlgOpen;
bool16 refreshTableByAttribute=kTrue;    //--------
/// Global Pointers
extern IAppFramework* ptrIAppFramework;
///////////
bool16 updateImage = kTrue;
bool16 updateProdItem = kTrue;
bool16 IsHiliteFlagSelected = kFalse;
extern bool16 IsDeleteFlagSelected;
bool16 updateTable = kTrue;

bool16 upadateItem = kTrue;//by amarjit
bool16 isupdateitemgroupSelected = kTrue;//by amarjit

vector<int32> pagesPerDocumetList;

IControlView* midBookListSelectAllControlView = nil;  //--------Take state of SelectAll Check Box(MidBookList) 
extern int32 TreeDisplayOption;

//class BookContentDocInfo
//{
//public:
//	PMString DocumentName;
//	IDFile DocFile;
//	int32 index;
//	UID DocUID;
//	//IDocument* documentPtr;
//};
//
//typedef vector<BookContentDocInfo> BookContentDocInfoVector;
/** Implements IDialogController based on the partial implementation CDialogController; 
	its methods allow for the initialization, validation, and application of dialog widget values.
  
	The methods take an additional parameter for 3.0, of type IActiveContext.
	See the design document for an explanation of the rationale for this
	new parameter and the renaming of the methods that CDialogController supports.
	
	
	@ingroup basicdialog
	
*/
//class RfhDlgDialogController : public CDialogController
//{
//	public:
//		/**
//			Constructor.
//			@param boss interface ptr from boss object on which this interface is aggregated.
//		*/
//		RfhDlgDialogController(IPMUnknown* boss) : CDialogController(boss) {}
//
//		/**
//			Initializes each widget in the dialog with its default value.
//			Called when the dialog is opened.
//			@param dlgContext
//		*/
//		 void InitializeDialogFields( IActiveContext* dlgContext);
//
//		/**
//			Validate the values in the widgets. 
//			By default, the widget with ID kOKButtonWidgetID causes 
//			this method to be called. When all widgets are valid, 
//			ApplyFields will be called.		
//			@param myContext
//			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.
//
//		*/
//		 WidgetID ValidateDialogFields( IActiveContext* myContext);
//
//		/**
//			Retrieve the values from the widgets and act on them.
//			@param myContext
//			@param widgetId identifies the widget on which to act.
//		*/
//		void ApplyDialogFields( IActiveContext* myContext, const WidgetID& widgetId);
//		void StartBookRefresh();
//		BookContentDocInfoVector* GetBookContentDocInfo(IBookContentMgr* bookContentMgr);
//		bool16 reSortTheListForBookUniqueAttr(void);
//		bool16 fillDataInListBox(int GroupFlag);
//
//
//};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(RfhDlgDialogController, kRfhDialogControllerImpl)


/* ApplyFields
*/
void RfhDlgDialogController::InitializeDialogFields( IActiveContext* dlgContext) 
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;

	ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields");
	//CA(__FUNCTION__);
	// Put code to initialize widget values here.
	CDialogController::InitializeDialogFields(dlgContext);
	// Put code to initialize widget values here.

	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::panelControlData == nil");
		return;
	}

	ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields: panelControlData");

	Mediator::catalogRefrshPanelControlData =panelControlData;//ADDEd By sachin Sharma

	IControlView* BookListControlView = panelControlData->FindWidget(/*kRfhDlgLstboxWidgetID*/kRfhCRBookFieldsTreeViewWidgetID);
	if(!BookListControlView)
	{		
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::BookListControlView == nil");
		return;
	}
	Mediator::BooklistControlView = BookListControlView;
	
	
	IControlView* RfhDlgRefreshButtonView = panelControlData->FindWidget(kRfhDlgRefreshButtonWidgetID);
	if(!RfhDlgRefreshButtonView)
	{		
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::RfhDlgRefreshButtonView == nil");
		return;
	}
	Mediator::RfhDlgRefreshButtonView = RfhDlgRefreshButtonView;

	IControlView* selectAllCheckBoxView = panelControlData->FindWidget(kRfhSelectAllCheckBoxWidgetID);
	if(!selectAllCheckBoxView)
	{		
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::selectAllCheckBoxView == nil");
		return;
	}
	Mediator::selectAllCheckBoxView = selectAllCheckBoxView;
	this->SetTriStateControlData(kRfhSelectAllCheckBoxWidgetID,kTrue);
	/*IsHiliteFlagSelected = kTrue;*/
//A//	IsDeleteFlagSelected = kTrue;

	rBookDataList.clear();
	UniqueBookDataList.clear();
	OriginalrBookDataList.clear();
	//this->StartBookRefresh();
//5-june chetan
////////////////////////////
	IControlView * interactiveoptionGroupPanelControlData = panelControlData->FindWidget(kInteractiveoptionGroupPanelWidgetID);
	if(interactiveoptionGroupPanelControlData == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::interactiveoptionGroupPanelControlData == nil");
		return;
	}
	interactiveoptionGroupPanelControlData->HideView();
	ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields: interactiveoptionGroupPanelControlData->HideView();");

	PMString inddfilepath;

	InterfacePtr<IBookManager> bookManager(/*gSession*/GetExecutionContextSession(), UseDefaultIID()); //Cs4
	if (bookManager == nil) 
	{ 
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::bookManager == nil");
		return; 
	}

	IBook * activeBook = bookManager->GetCurrentActiveBook();
	if(activeBook == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::activeBook == nil");
		return;			
	}
		
	IDFile file = activeBook->GetBookFileSpec();

	SDKUtilities sdkutils;
	inddfilepath =FileUtils::SysFileToPMString(file);  //MAC SPECIFIC 

	IControlView * bookPathCtrlView = panelControlData->FindWidget(kBookPathWidgetID);
	if(bookPathCtrlView == nil)
	{
		//CA("bookPathCtrlView == nil");
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::bookPathCtrlView == nil");
		return;
	}
	InterfacePtr<ITextControlData> bookPathTxtCtrlData (bookPathCtrlView,UseDefaultIID());
	if(bookPathTxtCtrlData == nil)
	{
		//CA("(bookPathTxtCtrlData == nil)");
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::bookPathTxtCtrlData == nil");
		return;
	}
	inddfilepath.SetTranslatable(kFalse);
    inddfilepath.ParseForEmbeddedCharacters();
	bookPathTxtCtrlData->SetString(inddfilepath);

////////////////////////////

	IControlView * secondGroupPanelControlData = panelControlData->FindWidget(kRfhSecondGroupPanelWidgetID);
	if(secondGroupPanelControlData == nil)
	{
		//CA("secondGroupPanelControlData == nil");
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::secondGroupPanelControlData == nil");
		return;
	}
	secondGroupPanelControlData->HideView();

	IControlView * originalGroupPanelControlData = panelControlData->FindWidget(kRfhoriginalGroupPanelWidgetID);
	if(originalGroupPanelControlData == nil)
	{
		//CA("secondGroupPanelControlData == nil");
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::secondGroupPanelControlData == nil");
		return;
	}
	originalGroupPanelControlData->HideView();

	IControlView * newoptionGroupPanelControlData = panelControlData->FindWidget(kRfhNewoptionGroupPanelWidgetID);
	if(newoptionGroupPanelControlData == nil)
	{
		//CA("secondGroupPanelControlData == nil");
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::secondGroupPanelControlData == nil");
		return;
	}
	newoptionGroupPanelControlData->HideView();


	ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields: secondGroupPanelControlData->HideView()");
	//Show the firstGroupPanel
	IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kRfhFirstGroupPanelWidgetID);
	if(firstGroupPanelControlData == nil)
	{
		//CA("firstGroupPanelControlData == nil");
		return;
	}
	firstGroupPanelControlData->HideView();

	ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields: firstGroupPanelControlData->HideView()");
	//Show the ZerothGroupPanel
	IControlView * zerothGroupPanelControlData = panelControlData->FindWidget(kRfhZerothGroupPanelWidgetID);
	if(zerothGroupPanelControlData == nil)
	{
		//CA("zerothGroupPanelControlData == nil");
		return;
	}
	zerothGroupPanelControlData->ShowView();
	ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields: zerothGroupPanelControlData->ShowView()");

	this->SetTriStateControlData(kRfhUpdateCheckBoxWidgetID,kTrue);
	
	if(IsDeleteFlagSelected)
		this->SetTriStateControlData(kRfhDeleteCheckBoxWidgetID,kTrue);
	else
		this->SetTriStateControlData(kRfhDeleteCheckBoxWidgetID,kFalse);

	this->SetTriStateControlData(kRfhUpdateImagesCheckBoxWidgetID,kTrue);
	//this->SetTriStateControlData(kRfhRefreshTabByAttributeCheckBoxWidgetID,refreshTableByAttribute);  // Chetan
	this->SetTriStateControlData(kRfhRefreshTabByAttributeCheckBoxWidgetID,kTrue);

	//By Amarjit
	this->SetTriStateControlData(kRfhnewUpdateCheckBoxWidgetID,kTrue);

	this->SetTriStateControlData(kRfhnewUpdateImagesCheckBoxWidgetID,kTrue);
	ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields: 1");
//By amarjit patil
	IControlView* newrefreshmode_clusterPanelview = panelControlData->FindWidget(knewRefreshModeOptionWidgetID);
	if(newrefreshmode_clusterPanelview == nil)
	{
		//CA("clusterPanelview == nil");
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::alwayEnable_clusterPanelview == nil");
		return;
	}				
	newrefreshmode_clusterPanelview->Enable();
	this->SetTriStateControlData(kcontentstructureWidgetID, kTrue);
    refreshTableByAttribute = kFalse;
//----------------------------------------------------------------------------------------------------------------
	IControlView* alwayEnable_clusterPanelview = panelControlData->FindWidget(kClusterRadiowidgetid);
	if(alwayEnable_clusterPanelview == nil)
	{
		//CA("clusterPanelview == nil");
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::alwayEnable_clusterPanelview == nil");
		return;
	}				
	alwayEnable_clusterPanelview->Enable();
	
	this->SetTriStateControlData(kNew_Refresh_ByCell_RadioButtonWidgetID, kTrue);	
	refreshTableByAttribute = kTrue;
		
	IControlView * thirdGroupPanelControlData = panelControlData->FindWidget(kRfhThirdGroupPanelWidgetID);
	if(thirdGroupPanelControlData == nil)
	{
		//CA("thirdGroupPanelControlData == nil");
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::thirdGroupPanelControlData == nil");
		return;
	}
	thirdGroupPanelControlData->HideView();
	ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields: thirdGroupPanelControlData->HideView()");


	//--------BookList Group Panel ADD BY LALIT--------
	IControlView * midBookListGroupPanelControlData = panelControlData->FindWidget(kRfhMidBookListGroupPanelWidgetID);
	if(midBookListGroupPanelControlData == nil)
	{
		//CA("midBookListGroupPanelControlData == nil");
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::midBookListGroupPanelControlData == nil");
		return;
	}
	IControlView * midBookListSelectAllCheckBoxControlView = panelControlData->FindWidget(kRfhBookListSelectAllCheckBoxWidgetID);
	if(midBookListSelectAllCheckBoxControlView == nil)
	{
		//CA("midBookListSelectAllCheckBoxControlView == nil");
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields::midBookListSelectAllCheckBoxControlView == nil");
		return;
	}
	midBookListSelectAllControlView =  midBookListSelectAllCheckBoxControlView;
	this->SetTriStateControlData(kRfhBookListSelectAllCheckBoxWidgetID,kTrue/*kFalse*/);
	midBookListGroupPanelControlData->HideView();

	this->SetTriStateControlData(kReportWithRefreshWidgetID,kTrue);
	//5-june

	ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhDlgDialogController::InitializeDialogFields: 2");
	
	IControlView * firstInteractiveGroupPanelControlView = panelControlData->FindWidget(kFirstInteractiveGroupPanelWidgetID);
	if(firstInteractiveGroupPanelControlView == nil)
	{
		//CA("firstInteractiveGroupPanelControlView == nil");
		return;
	}	
	firstInteractiveGroupPanelControlView->HideView();
	ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhDlgDialogController::firstInteractiveGroupPanelControlView->HideView(); 11");

	IControlView * interactiveListNilGroupPanelControlData = panelControlData->FindWidget(kInteractiveListNilGroupPanelWidgetID);
	if(interactiveListNilGroupPanelControlData == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::Update::interactiveListNilGroupPanelControlData == nil");
		return;
	}
	interactiveListNilGroupPanelControlData->HideView();
	ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhDlgDialogController::interactiveListNilGroupPanelControlData->HideView(); 11");

   
	IControlView * newaddedInteractiveGroupPanelControlView = panelControlData->FindWidget(kRfhnewGroupPanelWidgetID);
	if(newaddedInteractiveGroupPanelControlView == nil)
	{
		//CA("firstInteractiveGroupPanelControlView == nil");
		return;
	}	
	newaddedInteractiveGroupPanelControlView->HideView();

	IControlView * lastaddedInteractiveGroupPanelControlView = panelControlData->FindWidget(kRfhlastGroupPanelWidgetID);
	if(lastaddedInteractiveGroupPanelControlView == nil)
	{
		//CA("firstInteractiveGroupPanelControlView == nil");
		return;
	}	
	lastaddedInteractiveGroupPanelControlView->HideView();
}

/* ValidateFields
*/
WidgetID RfhDlgDialogController::ValidateDialogFields( IActiveContext* myContext) 
{
	WidgetID result = kNoInvalidWidgets;
	// Put code to validate widget values here.

	return result;
}

/* ApplyFields
*/
void RfhDlgDialogController::ApplyDialogFields( IActiveContext* myContext, const WidgetID& widgetId) 
{
	// Replace with code that gathers widget values and applies them.
//	SystemBeep();  
}


BookContentDocInfoVector* RfhDlgDialogController::GetBookContentDocInfo(IBookContentMgr* bookContentMgr)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		 //CA("ptrIAppFramework == nil");			
		return NULL;
	}
	pagesPerDocumetList.clear();
	//ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhDlgDialogController::GetBookContentDocInfo");
	bool16 flag=kFalse;
	BookContentDocInfoVector* BookContentDocInfovectorPtr = new BookContentDocInfoVector;
	BookContentDocInfovectorPtr->clear();
	do {
		
		if (bookContentMgr == nil) 
		{
			ASSERT(bookContentMgr);
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::GetBookContentDocInfo::bookContentMgr == nil");
			break;
		}

		// get the book's database (same for IBook)
		IDataBase* bookDB = ::GetDataBase(bookContentMgr);
		if (bookDB == nil) 
		{
			ASSERT_FAIL("bookDB is nil - wrong database?"); 
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::GetBookContentDocInfo::bookDB == nil");
			break;
		}
//ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhDlgDialogController::GetBookContentDocInfo  2");
		int32 contentCount = bookContentMgr->GetContentCount();
		for (int32 i = 0 ; i < contentCount ; i++) 
		{
			BookContentDocInfo contentDocInfo;
			UID contentUID = bookContentMgr->GetNthContent(i);
			if (contentUID == kInvalidUID) 
			{
				// somehow, we got a bad UID
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::GetBookContentDocInfo::contentUID == kInvalidUID continue");
				continue; // just goto the next one
			}
			// get the datalink that points to the book content  Depricated cs3
			//InterfacePtr<IDataLink> bookLink(bookDB, contentUID, UseDefaultIID());
			//if (bookLink == nil) 
			//{
			//	ASSERT_FAIL(FORMAT_ARGS("IDataLink for book #%d is missing", i));
			//	break; // out of for loop
			//}

			// get the book name and add it to the list
			//PMString* baseName = bookLink->GetBaseName();
			
			//******Added for Cs4
			InterfacePtr<IBookContent> bookContent(bookDB, contentUID, UseDefaultIID());
			if (bookContent == nil) 
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::GetBookContentDocInfo::bookContent == nil");
				ASSERT_FAIL(FORMAT_ARGS("IBookContent for book #%d is missing", i));
				//CA("bookContent == nil");
				break; // out of for loop
			}
			PMString baseName = bookContent->GetShortName();
			//*****

			//ASSERT(baseName && baseName/*->*/.IsNull() == kFalse);
			
			IDFile CurrFile;
			bool16 IsMissingPluginFlag = kFalse;

			IDocument* CurrDoc = Utils<IBookUtils>()->FindDocFromContentUID
				(
					bookDB,
					contentUID,
					CurrFile,
					IsMissingPluginFlag
				);

			bool16 Flag1 = kFalse;
			Flag1 =  FileUtils::DoesFileExist(CurrFile);
			if(Flag1 == kFalse)
			{
				//CA("File dOES nOT Exists");
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::GetBookContentDocInfo::File does not exists");
				continue;
			}
			else
			{	
				contentDocInfo.DocFile = CurrFile;	
				contentDocInfo.DocumentName = (/***/baseName);
				contentDocInfo.DocUID = contentUID;
				contentDocInfo.index = i;
				//contentDocInfo.documentPtr = CurrDoc;				
				BookContentDocInfovectorPtr->push_back(contentDocInfo);
				flag=kTrue;
			}			
			if(Mediator::isSilentModeSelected == kFalse){
				CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 

				SDKLayoutHelper sdklhelp;
				UIDRef CurrDocRef = sdklhelp.OpenDocument(CurrFile);
				if(CurrDocRef == UIDRef ::gNull)
				{
					CA("CurrDocRef is invalid");
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::GetBookContentDocInfo::CurrDocRef == UIDRef ::gNull");
					continue;
				}
				ErrorCode err = sdklhelp.OpenLayoutWindow(CurrDocRef);
				if(err == kFailure)
				{
					//CA("Error occured while opening the layoutwindow of the template");
					continue;
				}
				IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();
				if(fntDoc==nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::fillFrameRecordVector::fntDoc==nil");	
					continue;
				}
				InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());//Cs4
				if (layout == nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::fillFrameRecordVector::layout == nil");		
					continue;
				}
				IDataBase* database = ::GetDataBase(fntDoc);
				if(database==nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::fillFrameRecordVector::database==nil");			
					continue;
				}
				InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
				if (iSpreadList==nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::fillFrameRecordVector::iSpreadList==nil");				
					continue;
				}
				int numPages = 0;
				for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
				{
					UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));

					InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
					if(!spread)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogObserver::fillFrameRecordVector::!spread");						
						continue;
					}
					numPages = numPages + spread->GetNumPages();					
				}
				
				pagesPerDocumetList.push_back(numPages);
				sdklhelp.SaveDocumentAs(CurrDocRef,CurrFile);
				sdklhelp.CloseDocument(CurrDocRef, kTrue);				
			}			
		}
		if(Mediator::isSilentModeSelected == kFalse)
			CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 

		ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhDlgDialogController::GetBookContentDocInfo 3");

	} while (false);
//ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhDlgDialogController::GetBookContentDocInfo 4");
	if(flag)	
		return BookContentDocInfovectorPtr;
	else
	{
		delete BookContentDocInfovectorPtr;
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::GetBookContentDocInfo::return NULL");
		return NULL;
	}
}


void RfhDlgDialogController::StartBookRefresh ()
{
	//CA(__FUNCTION__);
	bool16 ListNameIsPresent = kFalse;
	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::StartBookRefresh::panelControlData == nil");
		return;
	}
	if(Mediator::isSilentModeSelected == kTrue){
		IControlView* RfhDlgUpdateImagesChkBxView = panelControlData->FindWidget(kRfhUpdateImagesCheckBoxWidgetID);
		if(!RfhDlgUpdateImagesChkBxView)
		{		
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::StartBookRefresh::RfhDlgUpdateImagesChkBxView == nil");
			return;
		}
		InterfacePtr<ITriStateControlData>updateImagesCheckBoxTriState(RfhDlgUpdateImagesChkBxView,UseDefaultIID());
		if(updateImagesCheckBoxTriState == nil)
		{
			//CA("updateImagesCheckBoxTriState == nil");
			return;
		}
		updateImage = updateImagesCheckBoxTriState->IsSelected();

		//By amarjit

		IControlView* RfhUpdateImagesCheckBoxView = panelControlData->FindWidget(kRfhUpdateImagesCheckBoxWidgetID);
		if(!RfhUpdateImagesCheckBoxView)
		{		
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::StartBookRefresh::RfhDlgUpdateImagesChkBxView == nil");
			return;
		}
		InterfacePtr<ITriStateControlData>updateitemgroupCheckBoxTriState(RfhUpdateImagesCheckBoxView,UseDefaultIID());
		if(updateitemgroupCheckBoxTriState == nil)
		{
			//CA("updateImagesCheckBoxTriState == nil");
			return;
		}
		isupdateitemgroupSelected = updateitemgroupCheckBoxTriState->IsSelected();



		IControlView* RfhDlgUpdateProdItemChkBxView = panelControlData->FindWidget(kRfhUpdateCheckBoxWidgetID);
		if(!RfhDlgUpdateProdItemChkBxView)
		{		
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::StartBookRefresh::RfhDlgUpdateProdItemChkBxView == nil");
			return;
		}
		InterfacePtr<ITriStateControlData>updateProdItemCheckBoxTriState(RfhDlgUpdateProdItemChkBxView,UseDefaultIID());
		if(updateProdItemCheckBoxTriState == nil)
		{
			//CA("updateProdItemCheckBoxTriState == nil");
			return;
		}
		updateProdItem = updateProdItemCheckBoxTriState->IsSelected();

		//By amarjit
		IControlView* RfhnewUpdateCheckBoxItemView = panelControlData->FindWidget(kRfhnewUpdateCheckBoxWidgetID);
		if(!RfhnewUpdateCheckBoxItemView)
		{		
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::StartBookRefresh::RfhDlgUpdateProdItemChkBxView == nil");
			return;
		}
		InterfacePtr<ITriStateControlData>updateItemCheckBoxTriState(RfhnewUpdateCheckBoxItemView,UseDefaultIID());
		if(updateItemCheckBoxTriState == nil)
		{
			//CA("updateProdItemCheckBoxTriState == nil");
			return;
		}
		upadateItem = updateItemCheckBoxTriState->IsSelected();
		IControlView* RfhDlgRefreshTableChkBxView = panelControlData->FindWidget(kRfhRefreshTabByAttributeCheckBoxWidgetID);
		if(!RfhDlgRefreshTableChkBxView)
		{		
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhDlgDialogController::StartBookRefresh::RfhDlgRefreshTableChkBxView == nil");
			return;
		}
		InterfacePtr<ITriStateControlData>updateTableCheckBoxTriState(RfhDlgRefreshTableChkBxView,UseDefaultIID());
		if(updateTableCheckBoxTriState == nil)
		{
			//CA("updateProdItemCheckBoxTriState == nil");
			return;
		}
		updateTable = updateTableCheckBoxTriState->IsSelected();
	}
	BookFileList.clear();
	InterfacePtr<IBookManager> bookManager(/*gSession*/GetExecutionContextSession(), UseDefaultIID()); //Cs4
	if (bookManager == nil) 
	{ 
		//CA("There is not book manager!");
		return; 
	}

	if (bookManager->GetBookCount() <= 0)
	{
		//CA("There is no book open. You must first open a book before running this snippet.");
		return;
	}

	int32 bookCount = bookManager->GetBookCount();

	PMString  ActiveBookName;
	bool16 result = Utils<IBookUtils>()->GetActiveBookName(ActiveBookName);
	//CA(ActiveBookName);
		
	for (int32 i = 0 ; i < bookCount ; i++) 
	{
		IBook* book = bookManager->GetNthBook(i);
		if (book == nil) 
		{	//go onto the next book...
			continue;
		}				
		PMString BookTitalName = book->GetBookTitleName();
		if(BookTitalName == ActiveBookName)
		{	
			
			InterfacePtr<IBookContentMgr> bookContentMgr(book, UseDefaultIID());
			if (bookContentMgr == nil) 
			{
				CA("This book doesn't have a book content manager!  Something is wrong.");
				return;
			}
			//BookContentDocInfoVector* BookContentinfovector = NULL;	
			BookContentDocInfoVector* BookContentinfovector = this->GetBookContentDocInfo(bookContentMgr);
			if(BookContentinfovector== nil)
				return;

			if(BookContentinfovector->size()<=0)
			{
				//CA(" BookContentinfovector->size()<=0 ");
				return;
			}

			BookContentDocInfoVector::iterator it1;
			for(it1 = BookContentinfovector->begin(); it1 != BookContentinfovector->end(); it1++)
			{				
				IDFile fileName= it1->DocFile ;
				BookFileList.push_back(fileName);				
			}

			if(BookContentinfovector)//--------
			{
				BookContentinfovector->clear();
				delete BookContentinfovector;
			}
		}
	}	
	
	UID pageUID=kInvalidUID;
	int k=0;
	rBookDataList.clear();
	OriginalrBookDataList.clear();
	UniqueBookDataList.clear();

	int pgno=0;
	int cursel=0;
	int seltext=0;
	int FlagParentID = 0;
	Refresh refresh;
	
	ISRfreshBookDlgOpen = kTrue;
	DocWchUtils::StopDocResponderMode();


	//for(int32 p=0; p< BookFileList.size(); p++)
	//{
	//	
	//	SDKLayoutHelper sdklhelp;
	//	UIDRef CurrDocRef = sdklhelp.OpenDocument(BookFileList[p]);
	//	ErrorCode err = sdklhelp.OpenLayoutWindow(CurrDocRef);
	//	refresh.selectUIDList.Clear();
	//	do
	//	{
	//		if(!refresh.getDocumentSelectedBoxIds())
	//		{
	//			return ;
	//		}
	//		for(int32 i=0; i<refresh.selectUIDList.Length(); i++)
	//		{
	//			PageData pData;
	//			bReader.getBoxInformation(refresh.selectUIDList.GetRef(i), pData);
	//			pData.sysFile = BookFileList[p];
	//			pData.DocumentUIDRef = CurrDocRef;
	//			
	//			
	//			refresh.appendToGlobalList(pData);
	//			// Added By Awasthi
	//			UIDRef boxID=refresh.selectUIDList.GetRef(i);				
	//			TagList tList;
	//			//TagReader tReader;
	//			InterfacePtr<ITagReader> itagReader
	//			((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	//			if(!itagReader){ 
	//				return ;
	//			}
	//			tList=itagReader->getTagsFromBox_ForRefresh(boxID);
	//			if(tList.size()==0)
	//			{
	//				tList=itagReader->getFrameTags(boxID);
	//				if(tList.size()==0)
	//				{
	//					//FlagParentID = 1;
	//					continue;
	//				}
	//			}
	//			for(int j=0; j<tList.size(); j++)
	//			{				
	//				if(tList[j].parentId==-1)
	//					continue;
	//				k=1;
	//			}
	//			if(k==0)
	//				FlagParentID = 1;				
	//		}
	//	}while(kFalse);
	//	sdklhelp.SaveDocumentAs(CurrDocRef,BookFileList[p]);
	//	sdklhelp.CloseDocument(CurrDocRef, kFalse);

	//	
	//}

	double lng_Id = ptrIAppFramework->getLocaleId();
	if(Mediator::isSilentModeSelected == kTrue){
		bool16 LanguageIdFound = kFalse;

		InterfacePtr<ITagReader> itagReader ((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader){
			ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::!itagReader");
			return;
		}

		for(int32 p=0; p< BookFileList.size(); p++)
		{		
			SDKLayoutHelper sdklhelp;
			UIDRef CurrDocRef = sdklhelp.OpenDocument(BookFileList[p]);
			ErrorCode err = sdklhelp.OpenLayoutWindow(CurrDocRef);
		
			UIDList tempUIDList;		
			do
		   {
				
				IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();//Cs4
				if(fntDoc==nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::fntDoc==nil");	
					break;
				}
				InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());//Cs4
				if (layout == nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::layout == nil");		
					break ;
				}
				IDataBase* database = ::GetDataBase(fntDoc);
				if(database==nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::database==nil");			
					break ;
				}
				InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
				if (iSpreadList==nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::iSpreadList==nil");				
					break ;
				}
				bool16 collisionFlag=kFalse;
				UIDList allPageItems(database);

				for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
				{
					UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));

					InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
					if(!spread)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::!spread");						
						break ;
					}
					int numPages=spread->GetNumPages();

					for(int i=0; i<numPages; i++)
					{
						UIDList tempList(database);
						spread->GetItemsOnPage(i, &tempList, kFalse);
						allPageItems.Append(tempList);
					}
				}
				tempUIDList= allPageItems;
				for( int p=0; p<tempUIDList.Length(); p++)
				{	
					InterfacePtr<IHierarchy> iHier(tempUIDList.GetRef(p), UseDefaultIID());
					if(!iHier)
						continue;
					UID kidUID;
					int32 numKids=iHier->GetChildCount();
					bool16 isGroupFrame = kFalse ;
					isGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(tempUIDList.GetRef(p));

					for(int j=0;j<numKids;j++)
					{
						//CA("1");
						kidUID=iHier->GetChildUID(j);
						UIDRef boxRef(tempUIDList.GetDataBase(), kidUID);
						IIDXMLElement* ptr;
						TagList tList,tList_checkForHybrid;
						
						if(isGroupFrame == kTrue) 
						{
							tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(boxRef, &ptr);
							if(tList_checkForHybrid.size() == 0)
							{
								//CA("tList_checkForHybrid.size() == 0");
								continue;
							}
							if(refreshTableByAttribute /*&& tList_checkForHybrid[0].tableType != 3*/)
								tList = itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxRef, &ptr);
							else
								tList = itagReader->getTagsFromBox_ForRefresh(boxRef, &ptr);
						}
						else 
						{
							tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(tempUIDList.GetRef(p), &ptr);
						
							if(tList_checkForHybrid.size() == 0)
							{
								continue;
							}

							if(refreshTableByAttribute /*&& tList_checkForHybrid[0].tableType != 3*/)
								tList = itagReader->getTagsFromBox_ForRefresh_ByAttribute(tempUIDList.GetRef(p), &ptr);
							else
								tList = itagReader->getTagsFromBox_ForRefresh(tempUIDList.GetRef(p), &ptr);
						
						}
						
						for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
						{
							
			 				PMString strID = tList[tagIndex].tagPtr->GetAttributeValue(WideString("ID"));
							double ID = strID.GetAsDouble();

							PMString strID11 = tList[tagIndex].tagPtr->GetAttributeValue(WideString("index"));
							int32 Index = strID11.GetAsNumber();

							PMString str1("Index : ");
							str1.AppendNumber(Index);
							//CA(str1);

							if(ID==-121 && Index==3)
							{
								//CA("TRUE");
								ListNameIsPresent=kTrue;
							}

							PMString str = tList[tagIndex].tagPtr->GetAttributeValue(WideString("LanguageID"));
							if(str == "-1")
								continue;
							else
							{
								lng_Id = str.GetAsDouble();
								LanguageIdFound = kTrue;
								break;
							}
						}					
						
						for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
						{
							tList[tagIndex].tagPtr->Release();
						}
						for(int32 tagIndex = 0 ; tagIndex < tList_checkForHybrid.size() ; tagIndex++)
						{
							tList_checkForHybrid[tagIndex].tagPtr->Release();
						}

						if(LanguageIdFound)
							break;
					}
					if(LanguageIdFound)
						break;
				}
			}while(0);
			
			sdklhelp.SaveDocumentAs(CurrDocRef,BookFileList[p]);
			sdklhelp.CloseDocument(CurrDocRef, kFalse);	

			if(LanguageIdFound)
				break;
		}
	}
	do{
		if(updateProdItem)
		{
			//CA("updateProdItem");
			VectorElementInfoPtr eleValObj = ptrIAppFramework->StructureCache_getItemGroupCopyAttributesByLanguageId( lng_Id/*1*/); // Going for default language
			if(eleValObj==nil){
				ptrIAppFramework->LogError("AP7_RefreshContent::RfhDlgDialogController::StartBookRefresh::eleValObj==nil");
				break;
			}
			//CA("eleValObj== not nil ");
			VectorElementInfoValue::iterator it;				
			
			
			for(it=eleValObj->begin();it!=eleValObj->end();it++)
			{	//CA("6oooo");		
				RefreshData rData;
				rData.StartIndex= 0;
				rData.EndIndex= 0;			
				rData.elementID=it->getElementId();
				rData.isObject=kFalse;
				rData.isSelected=kFalse;
				rData.name=it->getDisplayName();
				rData.objectID=-1;
				rData.publicationID=-1;
				rData.isTableFlag=kFalse;
				rData.LanguageID = it->getLanguageId();
				//rData.TypeID = it->getElement_type_id();		
				rData.isImageFlag=kFalse;
				rData.whichTab = 3;
				rBookDataList.push_back(rData);
				OriginalrBookDataList.push_back(rData);			
			}
			if(eleValObj)
				delete eleValObj;
		}
	}while(0);

	do
	{
		if(updateTable)//if(updateProdItem )
		{
			VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
			if(typeValObj==nil)
			{	
				ptrIAppFramework->LogError("AP7_RefreshContent::RfhDlgDialogController::StartBookRefresh::StructureCache_getListTableTypes's typeValObj==nil");
				break;
			}
			VectorTypeInfoValue::iterator it1;

			for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
			{	//CA("8.1");	
				RefreshData rData;
				rData.StartIndex= 0;
				rData.EndIndex= 0;			
				rData.elementID=-1;
				rData.isObject=kFalse;
				rData.isSelected=kFalse;
				rData.name=it1->getName();
				rData.objectID=-1;
				rData.publicationID=-1;
				rData.isTableFlag=kTrue;
				rData.LanguageID = 1; // hardcoded Not getting it correctly
				rData.TypeID = it1->getTypeId();		
				rData.isImageFlag=kFalse;
				rData.whichTab = 3;
				rBookDataList.push_back(rData);
				OriginalrBookDataList.push_back(rData);			
								
			}
			if(typeValObj)
				delete typeValObj;

			{	//CA("8.1");	
				RefreshData rData;
				rData.StartIndex= 0;
				rData.EndIndex= 0;			
				rData.elementID=-1;
				rData.isObject=kFalse;
				rData.isSelected=kFalse;
				rData.name="Product CustomTabbedText";
				rData.objectID=-1;
				rData.publicationID=-1;
				rData.isTableFlag=kTrue;
				rData.LanguageID = 1; // hardcoded Not getting it correctly
				rData.TypeID = -5;		
				rData.isImageFlag=kFalse;
				rData.whichTab = 3;
				rBookDataList.push_back(rData);
				OriginalrBookDataList.push_back(rData);		
								
			}
			// By amarjit patil 
			{
			
					RefreshData rData;
					rData.StartIndex = 0;
					rData.EndIndex = 0;			
					rData.elementID = -121;
					rData.isObject=kFalse;
					rData.isSelected=kFalse;
					rData.name = "List_Table_Name";
					rData.publicationID = -1;
					rData.isTableFlag=kTrue;
					rData.LanguageID = 1;
					rData.isImageFlag=kFalse; 
					rData.whichTab = 3;
					rBookDataList.push_back(rData);
					OriginalrBookDataList.push_back(rData);
			}
		}
	}while(0);

	do
	{
		if(updateImage )
		{
			VectorTypeInfoPtr typeValObj1=
				ptrIAppFramework->StructureCache_getItemGroupImageAttributes();
			if(typeValObj1==nil)
			{
				ptrIAppFramework->LogError("AP7_RefreshContent::RfhDlgDialogController::StartBookRefresh::StructureCache_getItemGroupImageAttributes's typeValObj==nil");	
				break;
			}

			VectorTypeInfoValue::iterator it2;

			for(it2=typeValObj1->begin();it2!=typeValObj1->end();it2++)
			{	// CA("9");	
				RefreshData rData;
				rData.StartIndex= 0;
				rData.EndIndex= 0;			
				rData.elementID=-1;
				rData.isObject=kFalse;
				rData.isSelected=kFalse;
				rData.name=it2->getName();
				rData.objectID=-1;
				rData.publicationID=-1;
				rData.isTableFlag=kFalse;
				rData.LanguageID = 1; // hardcoded Not getting it correctly
				rData.TypeID = it2->getTypeId();		
				rData.isImageFlag=kTrue;
				rData.whichTab = 3;
				rBookDataList.push_back(rData);
				OriginalrBookDataList.push_back(rData);				
			}
			if(typeValObj1)
				delete typeValObj1;
		}
	}while(0);

	do  // For Custom Tables
	{
		if(updateProdItem )
		{
			//CA("11");
			RefreshData rData;
			rData.StartIndex= 0;
			rData.EndIndex= 0;			
			rData.elementID=-1;
			rData.isObject=kFalse;
			rData.isSelected=kFalse;
			rData.name="Custom Item Table";
			rData.objectID=-1;
			rData.publicationID=-1;
			rData.isTableFlag=kTrue;
			rData.LanguageID = 1; // hardcoded Not getting it correctly
			rData.TypeID = -1;//-3;
			rData.tableType = 2;
			rData.isImageFlag=kFalse;
			rData.whichTab = 3;
			rBookDataList.push_back(rData);
			OriginalrBookDataList.push_back(rData);	
			//CA("12");
		}
	}while(0);
	//// Entire Doc
	
	//1/11 For Med Custom Table  By Dattatray 
	do{
		if(updateProdItem )
		{
			RefreshData rData;
			rData.elementID=-1;
			rData.name = "Custom Table";
			rData.whichTab=3;
			rData.LanguageID=1;
			rData.isTableFlag=kTrue;
			rData.TypeID = -1;
			rData.isImageFlag = kFalse;
			rBookDataList.push_back(rData);
			OriginalrBookDataList.push_back(rData);	
		}
	}while(0);
	//1/11 upto here By Dattatray..........

////	Added By Amit
	do
	{
		if(updateProdItem )
		{
			VectorClassInfoPtr vectClassInfoValuePtr = ptrIAppFramework->ClassificationTree_getRoot(lng_Id/*1*/); // 1 default for English
			if(vectClassInfoValuePtr==nil)
			{		
				ptrIAppFramework->LogError("AP7_RefreshContent::RfhDlgDialogController::StartBookRefresh::ClassificationTree_getRoot's vectClassInfoValuePtr==nil");	
				break;
			}
			VectorClassInfoValue::iterator it;
			it=vectClassInfoValuePtr->begin();
			double CurrentClassID = it->getClass_id();
			VectorTypeInfoPtr typeValObj = NULL; //ptrIAppFramework->AttributeCache_getHybridTableTypesForClassAndParents(CurrentClassID);	
			if(typeValObj!=nil)
			{
				VectorTypeInfoValue::iterator it1;
				PMString Name("");
				for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
				{
		
					RefreshData rData;
					rData.elementID= -1;//-115;
					rData.tableType = 3;
					Name = it1->getName();
					Name.Append("_Hybrid_Project");
					rData.name = Name;
					rData.whichTab=5;
					rData.LanguageID=1;
					rData.isTableFlag=kTrue;
					rData.TypeID = it1->getTypeId();
					rData.isImageFlag = kFalse;
					rBookDataList.push_back(rData);
					OriginalrBookDataList.push_back(rData);	
				}
				for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
				{
		
					RefreshData rData;
					rData.elementID=-1;//-115;
					rData.tableType = 3;
					Name = it1->getName();
					Name.Append("_Hybrid_Product");
					rData.name = Name;
					rData.whichTab=3;
					rData.LanguageID=1;
					rData.isTableFlag=kTrue;
					rData.TypeID = it1->getTypeId();
					rData.isImageFlag = kFalse;
					rBookDataList.push_back(rData);
					OriginalrBookDataList.push_back(rData);	
				}
				for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
				{
		
					RefreshData rData;
					rData.elementID= -1; //-115;
					rData.tableType = 3;
					Name = it1->getName();
					Name.Append("_Hybrid_Item");
					rData.name = Name;
					rData.whichTab=4;
					rData.LanguageID=1;
					rData.isTableFlag=kTrue;
					rData.TypeID = it1->getTypeId();
					rData.isImageFlag = kFalse;
					rBookDataList.push_back(rData);
					OriginalrBookDataList.push_back(rData);	
				}
			}

			if(vectClassInfoValuePtr)
				delete vectClassInfoValuePtr;

			if(typeValObj)
				delete typeValObj;
		}
	}while(0);
////		End
	//---------27/10 To Add Item Copy Attributes By Dattatray
		double CurrentClassID=-1;
		do{
			if(updateProdItem )
			{
				VectorAttributeInfoPtr AttrInfoVectPtr = NULL;
				if(CurrentClassID == -1)
				{
					//CA("CurrentClassID == -1");
					VectorClassInfoPtr vectClassInfoValuePtr = ptrIAppFramework->ClassificationTree_getRoot(lng_Id/*1*/); // 1 default for English
					if(vectClassInfoValuePtr==nil)
					{		
						ptrIAppFramework->LogError("AP7_RefreshContent::RfhDlgDialogController::StartBookRefresh::ClassificationTree_getRoot's vectClassInfoValuePtr==nil");	
						break;
					}
					VectorClassInfoValue::iterator it;
					it=vectClassInfoValuePtr->begin();
					CurrentClassID = it->getClass_id();	

					if(vectClassInfoValuePtr)
						delete vectClassInfoValuePtr;
				}

				//AttrInfoVectPtr = ptrIAppFramework->AttributeCache_getAllItemAttributes();//AttributeCache_getItemAttributesForClassAndParents(CurrentClassID, 1);
				AttrInfoVectPtr = ptrIAppFramework->StructureCache_getItemAttributesForClassAndParents(CurrentClassID, 1);
				if(AttrInfoVectPtr== NULL){
					ptrIAppFramework->LogError("AP7_RefreshContent::RfhDlgDialogController::StartBookRefresh::AttributeCache_getAllItemAttributes's AttrInfoVectPtr==nil");	
					break;
				}
				VectorAttributeInfoValue::iterator it4;

				for(it4=AttrInfoVectPtr->begin();it4!=AttrInfoVectPtr->end();it4++)
				{
					RefreshData rData;
					rData.StartIndex= 0;
					rData.EndIndex= 0;			
					rData.elementID=it4->getAttributeId();
					rData.isObject=kFalse;
					rData.isSelected=kFalse;
					rData.name=it4->getDisplayName();
					rData.objectID=-1;
					rData.publicationID=-1;
					rData.isTableFlag=kFalse;
					rData.LanguageID = it4->getLanguageId();
					rData.TypeID = it4->getTypeId();		
					rData.isImageFlag=kFalse;
					rData.whichTab = 4;
					rBookDataList.push_back(rData);
					OriginalrBookDataList.push_back(rData);
					
					
				}
				if(AttrInfoVectPtr)
					delete AttrInfoVectPtr;
				{///For $off , %off
					
					RefreshData rData;
					rData.StartIndex = 0;
					rData.EndIndex = 0;			
					rData.elementID = -703;
					rData.isObject = kFalse;
					rData.isSelected = kFalse;
					rData.name = "$ off";
					rData.objectID = -1;
					rData.publicationID = -1;
					rData.isTableFlag = kFalse;
					rData.LanguageID = 1;
					rData.TypeID = -1;		
					rData.isImageFlag = kFalse;
					rData.whichTab = 4;
					rBookDataList.push_back(rData);
					OriginalrBookDataList.push_back(rData);
				
					RefreshData rData1;
					rData1.StartIndex = 0;
					rData1.EndIndex = 0;			
					rData1.elementID = -704;
					rData1.isObject = kFalse;
					rData1.isSelected = kFalse;
					rData1.name = "% off";
					rData1.objectID = -1;
					rData1.publicationID = -1;
					rData1.isTableFlag = kFalse;
					rData1.LanguageID = 1;
					rData1.TypeID = -1;		
					rData1.isImageFlag = kFalse;
					rData1.whichTab = 4;
					rBookDataList.push_back(rData1);
					OriginalrBookDataList.push_back(rData1);					
				}


				{///For Make Model Year
					
					RefreshData rData;
					rData.StartIndex = 0;
					rData.EndIndex = 0;			
					rData.elementID = -401;
					rData.isObject = kFalse;
					rData.isSelected = kFalse;
					rData.name = "Make";
					rData.objectID = -1;
					rData.publicationID = -1;
					rData.isTableFlag = kFalse;
					rData.LanguageID = 1;
					rData.TypeID = -1;		
					rData.isImageFlag = kFalse;
					rData.whichTab = 4;
					rBookDataList.push_back(rData);
					OriginalrBookDataList.push_back(rData);
				
					RefreshData rData1;
					rData1.StartIndex = 0;
					rData1.EndIndex = 0;			
					rData1.elementID = -402;
					rData1.isObject = kFalse;
					rData1.isSelected = kFalse;
					rData1.name = "Model";
					rData1.objectID = -1;
					rData1.publicationID = -1;
					rData1.isTableFlag = kFalse;
					rData1.LanguageID = 1;
					rData1.TypeID = -1;		
					rData1.isImageFlag = kFalse;
					rData1.whichTab = 4;
					rBookDataList.push_back(rData1);
					OriginalrBookDataList.push_back(rData1);

					RefreshData rData2;
					rData2.StartIndex = 0;
					rData2.EndIndex = 0;			
					rData2.elementID = -403;
					rData2.isObject = kFalse;
					rData2.isSelected = kFalse;
					rData2.name = "Year";
					rData2.objectID = -1;
					rData2.publicationID = -1;
					rData2.isTableFlag = kFalse;
					rData2.LanguageID = 1;
					rData2.TypeID = -1;		
					rData2.isImageFlag = kFalse;
					rData2.whichTab = 4;
					rBookDataList.push_back(rData2);
					OriginalrBookDataList.push_back(rData2);
				}

			}
		}while(0);

	//-----------upto here For Item Copy Attributes By Dattatray
	
	//-----------To Add Item Table Types By Dattatray on 28/10
		
	do
	 {
		 if(updateTable)//if(updateProdItem)
		 {
			VectorTypeInfoPtr typeValObj=NULL;	
			typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
			if(typeValObj == NULL)
			{
				ptrIAppFramework->LogError("AP7_RefreshContent::RfhDlgDialogController::StartBookRefresh::StructureCache_getListTableTypes's typeValObj==nil");			
				break;
			}

			VectorTypeInfoValue::iterator it6;
			for(it6=typeValObj->begin();it6!=typeValObj->end();it6++)
			{
				RefreshData rData;
				rData.StartIndex= 0;
				rData.EndIndex= 0;			
				rData.elementID=-1;//Added on 1/11/06
				rData.isObject=kFalse;
				rData.isSelected=kFalse;
				rData.name=it6->getName();
				rData.objectID=-1;
				rData.publicationID=-1;
				rData.isTableFlag=kTrue;
				rData.LanguageID = 1;
				rData.TypeID = it6->getTypeId();		
				rData.isImageFlag=kFalse;
				rData.whichTab = 4;
				rBookDataList.push_back(rData);
				OriginalrBookDataList.push_back(rData);
			}
			if(typeValObj)
				delete typeValObj;

			{
				RefreshData rData;
				rData.StartIndex= 0;
				rData.EndIndex= 0;			
				rData.elementID=-101;//Added on 1/11/06
				rData.isObject=kFalse;
				rData.isSelected=kFalse;
				rData.name="Item CustomTabbedText";
				rData.objectID=-1;
				rData.publicationID=-1;
				rData.isTableFlag=kTrue;
				rData.LanguageID = 1;
				rData.TypeID = -5;		
				rData.isImageFlag=kFalse;
				rData.whichTab = 4;
				rBookDataList.push_back(rData);
				OriginalrBookDataList.push_back(rData);
				

			}

			{//For MMY Table
				RefreshData rData;
				rData.StartIndex = 0;
				rData.EndIndex = 0;			
				rData.elementID = -102;//Added on 1/11/06
				rData.isObject = kFalse;
				rData.isSelected = kFalse;
				rData.name = "MMY Table";
				rData.objectID = -1;
				rData.publicationID = -1;
				rData.isTableFlag = kTrue;
				rData.LanguageID = 1;
				rData.TypeID = -1;
				rData.isImageFlag = kFalse;
				rData.whichTab = 4;
				rData.tableType = 2;
				
				
				rBookDataList.push_back(rData);
				OriginalrBookDataList.push_back(rData);				
			}

		 }
	 }while(0);
	//-------------Upto here On 28/10-----

	//-----------To Add Item Image Attributes  on 27/10 By Dattatray

		double CurClsID =-1;
		do{
			if(updateImage)
			{
				VectorTypeInfoPtr AttrInfoVectPtr = NULL;
				if(CurClsID == -1)
				{
					VectorClassInfoPtr vectClassInfoValuePtr = ptrIAppFramework->ClassificationTree_getRoot(lng_Id/*1*/); // 1 default for English
					if(vectClassInfoValuePtr==nil)
					{		
						ptrIAppFramework->LogError("AP7_RefreshContent::RfhDlgDialogController::StartBookRefresh::ClassificationTree_getRoot's vectClassInfoValuePtr==nil");			
						break;
					}
					VectorClassInfoValue::iterator it;
					it=vectClassInfoValuePtr->begin();
					CurClsID = it->getClass_id();	
					if(vectClassInfoValuePtr)
						delete vectClassInfoValuePtr;

				}

				AttrInfoVectPtr = ptrIAppFramework->StructureCache_getItemImagesForClassAndParents(CurClsID);
				if(AttrInfoVectPtr== NULL)
				{
					ptrIAppFramework->LogError("AP7_RefreshContent::RfhDlgDialogController::StartBookRefresh::AttributeCache_getItemImagesForClassAndParents's AttrInfoVectPtr==nil");			
					break;
				}
				VectorTypeInfoValue::iterator it5;

				for(it5=AttrInfoVectPtr->begin();it5!=AttrInfoVectPtr->end();it5++)
				{
					
					RefreshData rData;
					rData.StartIndex= 0;
					rData.EndIndex= 0;			
					rData.elementID=-1;//Added on 1/11/06
					rData.isObject=kFalse;
					rData.isSelected=kFalse;
					rData.name=it5->getName();
					rData.objectID=-1;
					rData.publicationID=-1;
					rData.isTableFlag=kFalse;
					rData.LanguageID = 1;
					rData.TypeID = it5->getTypeId();		
					rData.isImageFlag=kTrue;
					rData.whichTab = 4;
					rBookDataList.push_back(rData);
					OriginalrBookDataList.push_back(rData);
					
					
				}
				if(AttrInfoVectPtr)
					delete AttrInfoVectPtr;
			}
		}while(0);

	//------------Up to here By Dattatray on 27/10

	Mediator::selectedRadioButton = 4;
	GroupFlag= 1; //Unique Attributes in Doc
	UniqueBookDataList.clear();
	UniqueBookDataList =OriginalrBookDataList; 
	//reSortTheListForBookUniqueAttr();
	if(Mediator::isSilentModeSelected == kTrue)
		fillDataInListBox(1);
	
	ISRfreshBookDlgOpen = kFalse;
//	DocWchUtils::StartDocResponderMode();
}

bool16 RfhDlgDialogController::fillDataInListBox(int GroupFlag)
{
	if(!Mediator::BooklistControlView)
		return kFalse;
	
	//SDKListBoxHelper listBox(this, kRfhPluginID);
	//listBox.EmptyCurrentListBox(Mediator::BooklistControlView);
	//Mediator::RfhDlgLocateButtonView->Disable();
	
	
	
	if(UniqueBookDataList.size()==0)
	{
		//CA("RfhSelectionObserver::fillDataInListBox->rDataLsit.size==0");
	}
	
	if(UniqueBookDataList.size()>0)
		Mediator::RfhDlgRefreshButtonView->Enable();
	else
		Mediator::RfhDlgRefreshButtonView->Disable();
	
	for(int i=0; i<UniqueBookDataList.size(); i++)
	{	
		PMString objectName;
		
		if(UniqueBookDataList[i].isObject)
		{
			//CA("RfhSelectionObserver::fillDataInListBox 1.1");
			objectName= UniqueBookDataList[i].name;
			//CA(objectName);
		}
		else
		{
			//CA("RfhSelectionObserver::fillDataInListBox 1.2");
			objectName="        " + UniqueBookDataList[i].name;			
			//CA(objectName);
		}			
		
		//CA(objectName);
		//listBox.AddElement(Mediator::BooklistControlView, objectName, kRfhDlgTextWidgetID, i, kTrue);
		//listBox.CheckUncheckRow(Mediator::BooklistControlView, i, kTrue);
		UniqueBookDataList[i].isSelected=kTrue;
		UniqueBookDataList[i].isProcessed=kFalse;	


		TreeDisplayOption = 3; // Book Refresh Field Tree.

		InterfacePtr<ITreeViewMgr> treeViewMgr(Mediator::BooklistControlView, UseDefaultIID());
		if(!treeViewMgr)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");					
			return kFalse;
		}
		RFHTreeDataCache dc;
		dc.clearMap();

		RFHTreeModel pModel;
		PMString pfName("Root");
		pModel.setRoot(-1, pfName, 1);
		treeViewMgr->ClearTree(kTrue);
		pModel.GetRootUID();
		treeViewMgr->ChangeRoot();


	}
	for(int i=0; i<UniqueBookDataList.size(); i++)
	{
		for(int j=0; j<rBookDataList.size(); j++)
		{
			if(rBookDataList[j].isProcessed == kTrue)
				continue;
			if(UniqueBookDataList[i].elementID == rBookDataList[j].elementID && UniqueBookDataList[i].TypeID ==  rBookDataList[j].TypeID && UniqueBookDataList[i].whichTab == rBookDataList[j].whichTab)
			{	
				rBookDataList[j].isSelected = UniqueBookDataList[i].isSelected;
				rBookDataList[j].isProcessed = kTrue;					
			}
		}
	}

	return kTrue;
}






bool16 RfhDlgDialogController::reSortTheListForBookUniqueAttr(void)//We want to group by element
{
	//CA("RfhSelectionObserver::reSortTheListForUniqueAttr");			
	rBookDataList = OriginalrBookDataList;
	//CA("RR");
	RefreshDataList theDataList=rBookDataList;
	rBookDataList.clear();
	UniqueBookDataList.clear();
	RefreshData nodeToAdd;
	RefreshDataList tempDataList;

	for(int j=0; j<theDataList.size(); j++)
	{	
		if(theDataList[j].isObject)
				continue;
		
		RefreshData nodeToAdd;
		if(UniqueBookDataList.size()==0)
		{	
			nodeToAdd =theDataList[j]; 
			UniqueBookDataList.push_back(nodeToAdd);
			rBookDataList.push_back(nodeToAdd);			
		}
		else
		{			
			bool16 CheckFlag = kFalse;
			for(int p=0; p<UniqueBookDataList.size(); p++)
			{	
				if(UniqueBookDataList[p].elementID == theDataList[j].elementID && UniqueBookDataList[p].whichTab == theDataList[j].whichTab && UniqueBookDataList[p].TypeID == theDataList[j].TypeID )
				{	
					nodeToAdd = theDataList[j];
					rBookDataList.push_back(nodeToAdd);	
					CheckFlag = kTrue;
					break;
                }
			}
			if(!CheckFlag)
			{	
				nodeToAdd = theDataList[j];
				UniqueBookDataList.push_back(nodeToAdd);
				rBookDataList.push_back(nodeToAdd);
			}
		}
	}


	tempDataList.clear();
	for(int32 j=0; j<UniqueBookDataList.size(); j++)
	{
		if(UniqueBookDataList[j].isTableFlag == kFalse && UniqueBookDataList[j].isImageFlag == kFalse && UniqueBookDataList[j].elementID == -2)
			tempDataList.push_back(UniqueBookDataList[j]);
	}
	for(int32 j=0; j<UniqueBookDataList.size(); j++)
	{
		if(UniqueBookDataList[j].isTableFlag == kFalse && UniqueBookDataList[j].isImageFlag == kFalse && UniqueBookDataList[j].elementID == -1 && UniqueBookDataList[j].elementID != -2)
			tempDataList.push_back(UniqueBookDataList[j]);
	}

	for(int32 j=0; j<UniqueBookDataList.size(); j++)
	{
		if(UniqueBookDataList[j].isTableFlag == kFalse && UniqueBookDataList[j].isImageFlag == kFalse && UniqueBookDataList[j].elementID != -1 && UniqueBookDataList[j].elementID != -2)
			tempDataList.push_back(UniqueBookDataList[j]);
	}

	for(int32 j=0; j<UniqueBookDataList.size(); j++)
	{
		if(UniqueBookDataList[j].isTableFlag == kTrue )
			tempDataList.push_back(UniqueBookDataList[j]);
	}
	for(int32 j=0; j<UniqueBookDataList.size(); j++)
	{
		if(UniqueBookDataList[j].isTableFlag == kFalse && UniqueBookDataList[j].isImageFlag == kTrue )
			tempDataList.push_back(UniqueBookDataList[j]);
	}
		
	UniqueBookDataList = tempDataList;

	
	return kTrue;
}
// End, RfhDlgDialogController.cpp.



