
#ifndef __BOOKREPORTDATA_H__
#define __BOOKREPORTDATA_H__

#include "VCPluginHeaders.h"
#include "vector"
#include "ILayoutControlData.h"
#include "PMRect.h"
#include "PMReal.h"
#include "IItemLockData.h"
#include "ITableUtils.h"
#include "ITableModel.h"
#include <ITableCommands.h>
#include "IBookContentMgr.h"
#include "Tagstruct.h"


using namespace std;

class PageHeader
{
public:
	PMString pageorSectionName;
	PMString bookName;
	short int reportType;  //**** 0 - Update; 1- New , 2 - Deletes;
};

class ReportRowData
{
public:
	PMString fieldName;	
	PMString documentValue;
	PMString oneSourceValue;
	bool16 isImage; 
	double itemID;	

};

typedef struct newItemDetails
{
	double itemNo;
	double objectType;
	double parentID;

}NEWITEMDETAIL;

class BookReportData
{			
private :
	static PMString currentbookName;		
public :
	ReportRowData oneRawData;
	static int32 currntRowIndex ;
	bool16 addEmptyRow;
	//static bool16 isReportWithRefreshFlag ;
	static short int reportType ;             //0 - Update; 1- New , 2 - Deletes;
	//static bool16 isNewPageAdded ;
	static PMString currentSection;
	bool16 isNewSection ;
	int32 newsectionRowIndex;
	static double langaugeID;
	static int32 currentTableIndex;
	
		
	static vector<BookReportData> vecReportRows;

	static vector<vector<BookReportData> > entireReport;
	static vector<TagList> deletedItemTagList;

	static vector<double> newItemidList;
	static vector<TagList> documentTagList;
	static vector<vector<double> > entireNewItemList;
	static vector<vector<TagList> > entireBookTagList;
	static vector<double> ielementID;
	

	BookReportData(){
		this->addEmptyRow = kFalse;
		this->isNewSection =kFalse;
	}

	UIDRef CreateTextFrame(ILayoutControlData *layoutControlData, const PMRect &boundsInPageCoords);
//	void insertTableInTextFrame();

	static bool16 selectFrame(const UIDRef& frameUIDRef);
	UIDRef CreateTable(const UIDRef& storyRef, 
						  const TextIndex at,
						  const int32 numRows = 3/*2*/, 
						  const int32 numCols = /*3*/3,
						  const PMReal rowHeight = 20.0,
						  const PMReal colWidth = 70,    //183.6
						  const CellType cellType = kTextContentType,
						  PMString headerSectionOrPageText="",
						  PMString headerBookNameText="",
						  short int reporttype =0                   //0 - Update; 1- New , 2 - Deletes;
						  );

	static PMString getCurrentBookName();
	static void setCurrentBookName(const PMString other);

	static bool16 addRowInTable(const UIDRef& tableUIDRef,int32 numRows,int32 numCols,int32 height,int32 width);
	static UIDRef CreatePictureBoxInCurrentCell(int32 rowindex,int32 colIndex,const PMString fullImagePath);

	static ErrorCode InsertInline(const UIDRef& storyUIDRef, const TextIndex& whereTextIndex,const PMString &fullImagePath)	;
	static ErrorCode CreateFrame(IDataBase* database, UIDRef& newFrameUIDRef) ;

	static ErrorCode ChangeToInline(const UIDRef& storyUIDRef, const TextIndex& whereTextIndex, const UIDRef& frameUIDRef);
	static bool16 ImportFileInFrame(const UIDRef& imageBox, const PMString& fromPath);

	static void MoveCursorAtPosition(const UIDRef& tableUIDRef,int32 rowIndex ,int32 colIndex);
	static ErrorCode addReportToBook(K2Vector<IDFile> contentFileList);

	static void addHeaderDataOnNewPage(UIDRef& tableUIDRef /*,vector<PMString>& data*/,InterfacePtr<ITableCommands>& tblcommand/*,int32 rowstartIndex*/);
 
	static bool16 isRefreshReportExistInBook(PMString& bookName);
	static K2Vector<PMString> GetBookContentNames(IBookContentMgr* bookContentMgr);
	static void fitImageInBox(const UIDRef& boxUIDRef, bool16 isInterFaceCall);

	static PMString getcurrentSectionName();
	static void setcurrentSectionName(PMString sacName);

	static void removeDashFromString(PMString& str);
	static void SetCellStroke(const Tables::ESelectionSides sides, const PMReal strokeWeight,const UIDRef& tableUIDRef,const ITableModel *tableModel);
	static ErrorCode ChangeFontSizeOfSelectedTextInFrame(int32 fontSize);

	static void selectTableBodyColumns(ITableModel *,int32 startcolumn=0,int32 colsToSelect=1);
	static ErrorCode setFontForSelectedTable(const UIDRef& tableUIDRef,ITableModel* tblModel,ITextModel *txtModel,PMString fontName="");
//	static void convertPaltBuffToUnicodeBuffer(InterfacePtr<ITextModel> iModel, int32 startIndex, int32 finishIndex, PMString& story);

	static void fillVecNewReportData();

	static void fillDocumentTagList(UIDRef& boxID, PMString& imagePath, bool16 isInterFaceCall = kFalse);

	static bool16 isFileExist(const PMString imagePath,const PMString fileName);
	
	static void changeTableStrokeWeight(const PMReal& strokeWeight);
	static void selectTableBodyRows(ITableModel *tableModel,int32 startrows,int32 rowsToSelect);

	static void deSelectAllPAgeItems();

	static void deSelectFrame(const UIDRef& frameuidref);
};

#endif