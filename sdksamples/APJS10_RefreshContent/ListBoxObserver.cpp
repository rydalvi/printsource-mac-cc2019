#include "VCPlugInHeaders.h"
#include "WidgetID.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IListControlData.h"
#include "IListBoxController.h"
#include "CAlert.h"
#include "CObserver.h"
#include "RfhID.h"
#include "SystemUtils.h"
#include "RefreshData.h"
#include "ListBoxHelper.h"
#include "MediatorClass.h"
#include "IListControlData.h"
#include "IAppFramework.h"
#include "ITriStateControlData.h"

#define CA(z) CAlert::InformationAlert(z)

extern RefreshDataList rDataList;
extern RefreshDataList UniqueDataList;
extern int GroupFlag;
bool16 CheckBoxFlage=kTrue;
bool16 SelectBoxFlage=kFalse;
class ListBoxObserver : public CObserver
{
public:
	ListBoxObserver(IPMUnknown *boss);
	~ListBoxObserver();
	virtual void AutoAttach();
	virtual void AutoDetach();
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
};

CREATE_PMINTERFACE(ListBoxObserver, kRfhPanelListBoxObserverImpl)

ListBoxObserver::ListBoxObserver(IPMUnknown* boss)
: CObserver(boss)
{
	
}

ListBoxObserver::~ListBoxObserver()
{
}

void ListBoxObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
	}
}


void ListBoxObserver::AutoDetach()
{
 	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this, IID_ILISTCONTROLDATA);
	}
}

void ListBoxObserver::Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy)
{	
		if(protocol==IID_ILISTCONTROLDATA && theChange==kListSelectionChangedByUserMessage)
		{	
		//edit by nitin
				InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
				if(ptrIAppFramework == nil)
				{
					//CA("ptrIAppFramework == nil");
					return;
				}
				IControlView * iControlView=
					Mediator::iPanelCntrlDataPtr->FindWidget(kRfhSelectionBoxtWidgetID);
				if(iControlView==nil) 
				{
					//CA("iControlView==nil");
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::fillDataInListBox::iControlView is nil");		
					return;
				}
				InterfacePtr<ITriStateControlData> itristatecontroldata(iControlView, UseDefaultIID());
				if(itristatecontroldata==nil)
				{
					//CA("itristatecontroldata==nil"s);
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::fillDataInListBox::itristatecontroldata is nil");				
					return;
				}

		//upto here.....			
			SDKListBoxHelper sList(this, kRfhPluginID);
			InterfacePtr<IListBoxController> listCntl(Mediator::listControlView,IID_ILISTBOXCONTROLLER);
			if(listCntl == nil) 
				return;

			K2Vector<int32> curSelection ;
			listCntl->GetSelected(curSelection ) ;
			const int kSelectionLength =  curSelection.Length();
			if(kSelectionLength<=0)
				return;

			if(GroupFlag != 1)
			{
				rDataList[curSelection[0]].isSelected=(rDataList[curSelection[0]].isSelected)? kFalse: kTrue;
				sList.CheckUncheckRow(Mediator::listControlView, curSelection[0], rDataList[curSelection[0]].isSelected);
			//	listCntl->DeselectAll();

				bool16 isGroupByObj=Mediator::isGroupByObj;
				///////////////////////////[ GROUPED BY OBJECT ]//////////////////////////////

				if(/*isGroupByObj*/GroupFlag== 2)//If its grouped by object
				{
					if(rDataList[curSelection[0]].isObject && curSelection[0]<rDataList.size())
					{					 
						for(int j=curSelection[0]+1; j<rDataList.size(); j++)
						{
							if(rDataList[j].isObject)
							{
								break;
							}
							sList.CheckUncheckRow(Mediator::listControlView, j, rDataList[curSelection[0]].isSelected);				
							rDataList[j].isSelected=rDataList[curSelection[0]].isSelected;
						}
					}
					else if(!rDataList[curSelection[0]].isObject && !rDataList[curSelection[0]].isSelected)
					{
						for(int j=curSelection[0]; j>=0; j--)
						{												
							if(rDataList[j].isObject)
							{						
								sList.CheckUncheckRow(Mediator::listControlView, j, kFalse);
								rDataList[j].isSelected=kFalse;							
								break;
							}						
						}
					}
					else if(!rDataList[curSelection[0]].isObject && rDataList[curSelection[0]].isSelected)
					{
						for(int i=curSelection[0]; i<rDataList.size(); i++)
						{							
							if(rDataList[i].isObject)
								break;
							if(!rDataList[i].isSelected)
								return;							
						}
						int32 k=0;
						for(k=curSelection[0]; k>=0; k--)
						{
							if(rDataList[k].isObject)
								break;				
							if(!rDataList[k].isSelected)
								return;
						}
						rDataList[k].isSelected=kTrue;					
						sList.CheckUncheckRow(Mediator::listControlView, k, kTrue);
					}
		///////////////////////////////////////////////////////////////////////
					for(int i=0;i<rDataList.size();i++)
					{					
						if(rDataList[i].isSelected )
							CheckBoxFlage=kTrue;						
						else
						{						
							CheckBoxFlage=kFalse;
							if(itristatecontroldata->IsSelected())
							{							
								SelectBoxFlage=kTrue;//true
								itristatecontroldata->Deselect();							
							}
							//CA("after deselect");						
							break;
						}
					}
					if(CheckBoxFlage==kTrue)
					{					
						SelectBoxFlage=kTrue;
						itristatecontroldata->Select();
						//CA("after select");
					}
		////////////////////////////////////////////////////////////////////
				}				
				///////////////////////////[ GROUPED BY ELEMENT ]//////////////////////////////
				else if(GroupFlag== 3)//Its grouped by element
				{
					if((!rDataList[curSelection[0]].isObject) && curSelection[0]<rDataList.size())
					{	
						for(int j=curSelection[0]+1; j<rDataList.size(); j++)
						{
							if(!rDataList[j].isObject)
								break;
							sList.CheckUncheckRow(Mediator::listControlView, j, rDataList[curSelection[0]].isSelected);				
							rDataList[j].isSelected=rDataList[curSelection[0]].isSelected;
						}
					}
					else if(rDataList[curSelection[0]].isObject && !rDataList[curSelection[0]].isSelected)
					{	
						for(int j=curSelection[0]; j>=0; j--)
						{	
							if(!rDataList[j].isObject)
							{
								sList.CheckUncheckRow(Mediator::listControlView, j, kFalse);
								rDataList[j].isSelected=kFalse;
								break;
							}						
						}
					}
					else if(rDataList[curSelection[0]].isObject && rDataList[curSelection[0]].isSelected)
					{
						for(int i=curSelection[0]; i<rDataList.size(); i++)
						{
							if(!rDataList[i].isObject)
								break;
							if(!rDataList[i].isSelected)
								return;
						}
						int32 k=0;
						for(k=curSelection[0]; k>=0; k--)
						{
							if(!rDataList[k].isObject)
								break;			
							if(!rDataList[k].isSelected)
								return;
						}
						rDataList[k].isSelected=kTrue;					
						sList.CheckUncheckRow(Mediator::listControlView, k, kTrue);
					}

		/////////////////////////////////////////////////////////////////////////////////////////////			        
					for(int i=0;i<rDataList.size();i++)
					{
						if(rDataList[i].isSelected )
							CheckBoxFlage=kTrue;						
						else
						{						
							CheckBoxFlage=kFalse;
							if(itristatecontroldata->IsSelected())
							{							
								SelectBoxFlage=kTrue;
								itristatecontroldata->Deselect();							
							}						
							//CA("after deselect");						
							break;
						}
					}
					if(CheckBoxFlage==kTrue)
					{					
						SelectBoxFlage=kTrue;
						itristatecontroldata->Select();
						//CA("after select");					
					}
		/////////////////////////////////////////////////////////////
				}			
			}
			else if(GroupFlag == 1)
			{		
				UniqueDataList[curSelection[0]].isSelected=(UniqueDataList[curSelection[0]].isSelected)? kFalse: kTrue;		
				sList.CheckUncheckRow(Mediator::listControlView, curSelection[0], UniqueDataList[curSelection[0]].isSelected);		
				for(int j=0; j<rDataList.size(); j++)
				{				
					if(UniqueDataList[curSelection[0]].elementID == rDataList[j].elementID /*&& UniqueDataList[curSelection[0]].TypeID ==  rDataList[j].TypeID*/ && UniqueDataList[curSelection[0]].whichTab == rDataList[j].whichTab)
					{	
						if((UniqueDataList[curSelection[0]].whichTab != 4 || UniqueDataList[curSelection[0]].isImageFlag == kTrue || UniqueDataList[curSelection[0]].isTableFlag == kTrue) && !(UniqueDataList[curSelection[0]].elementID == - 121 && UniqueDataList[curSelection[0]].isTableFlag == kFalse))
						{						
							if( UniqueDataList[curSelection[0]].TypeID !=  rDataList[j].TypeID)
								continue;							
						}
						if(UniqueDataList[curSelection[0]].whichTab == 4 && UniqueDataList[curSelection[0]].elementID == -101 ) // for Item Table in Tabbed text
						{						
							if( UniqueDataList[curSelection[0]].TypeID !=  rDataList[j].TypeID)
								continue;							
						}					
						rDataList[j].isSelected = UniqueDataList[curSelection[0]].isSelected;				
					}
				}

		/////////////////////////////////////////////////////////////////////////
					//for(int i=0;i<rDataList.size();i++)
					for(int i=0;i<UniqueDataList.size();i++)				
					{						
						if( rDataList[i].isSelected )
							CheckBoxFlage=kTrue;						
						else
						{						
							CheckBoxFlage=kFalse;
							if(itristatecontroldata->IsSelected())
							{	
								SelectBoxFlage=kTrue;//true
								itristatecontroldata->Deselect();				
							}
							//CA("after deselect");						
							break;
						}
					}
					if(CheckBoxFlage==kTrue)
					{
						SelectBoxFlage=kTrue;
						itristatecontroldata->Select();
						//CA("after select");					
					}
		//////////////////////////////////////////////////////////
			}
		}
	
}
 
