#include "VCPlugInHeaders.h"
#include "WidgetID.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IListControlData.h"
#include "IListBoxController.h"
#include "CAlert.h"
#include "CObserver.h"
#include "RfhID.h"
#include "SystemUtils.h"
#include "RefreshData.h"
#include "ListBoxHelper.h"
#include "MediatorClass.h"
#include "IListControlData.h"

//Added on 30May by Yogesh
#include "ITriStateControlData.h"
//ended on 30May

#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("RfhDlgListBoxObserver.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
//Added By Dattatray on 30/10 
bool16 IsEventInListBox = kFalse;
extern RefreshDataList rBookDataList;
extern RefreshDataList UniqueBookDataList;
extern int GroupFlag;
class RfhDlgListBoxObserver : public CObserver
{
public:
	RfhDlgListBoxObserver(IPMUnknown *boss);
	~RfhDlgListBoxObserver();
	virtual void AutoAttach();
	virtual void AutoDetach();
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
};

CREATE_PMINTERFACE(RfhDlgListBoxObserver, kRfhDlgListBoxObserverImpl)

RfhDlgListBoxObserver::RfhDlgListBoxObserver(IPMUnknown* boss)
: CObserver(boss)
{
	
}

RfhDlgListBoxObserver::~RfhDlgListBoxObserver()
{
}

void RfhDlgListBoxObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
	}
}

void RfhDlgListBoxObserver::AutoDetach()
{
 	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this, IID_ILISTCONTROLDATA);
	}
}

void RfhDlgListBoxObserver::Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy)
{
	
	IsEventInListBox = kTrue;//Added By dattatray on 31/10 to deselect all selected checkboxes
	if(protocol==IID_ILISTCONTROLDATA && theChange==kListSelectionChangedByUserMessage)
	{	//CA("ListBoxObserver::Update");
		
		SDKListBoxHelper sList(this, kRfhPluginID);
		InterfacePtr<IListBoxController> listCntl(Mediator::BooklistControlView,IID_ILISTBOXCONTROLLER);
		if(listCntl == nil) 
			return;

		K2Vector<int32> curSelection ;
		listCntl->GetSelected(curSelection ) ;
		const int kSelectionLength =  curSelection.size();
		if(kSelectionLength<=0)
			return;

	
        if(GroupFlag == 1)
		{
			UniqueBookDataList[curSelection[0]].isSelected=(UniqueBookDataList[curSelection[0]].isSelected)? kFalse: kTrue;
			sList.CheckUncheckRow(Mediator::BooklistControlView, curSelection[0], UniqueBookDataList[curSelection[0]].isSelected);
			
			//Added on 30May by Yogesh
			InterfacePtr<ITriStateControlData>selectAllCheckBoxTriState(Mediator::selectAllCheckBoxView,UseDefaultIID());
			if(selectAllCheckBoxTriState == nil)
			{
				//CA("selectAllCheckBoxTriState == nil");
				return;
			}
			selectAllCheckBoxTriState->Deselect();
			//ended on 30May 


			for(int j=0; j<rBookDataList.size(); j++)
			{				
				if(UniqueBookDataList[curSelection[0]].elementID == rBookDataList[j].elementID && UniqueBookDataList[curSelection[0]].TypeID ==  rBookDataList[j].TypeID && UniqueBookDataList[curSelection[0]].whichTab == rBookDataList[j].whichTab)
				{
					rBookDataList[j].isSelected = UniqueBookDataList[curSelection[0]].isSelected;					
				}
			}
		}

		
		//To Check selectAllCheckBox if all the checkboxes in ListBox are selected or anyone of them is unchecked then uncheck the selectAllCheckBox by Dattatray on 28/10
		bool16 flag = kTrue;

		for(int i=0; i<rBookDataList.size(); i++)
		{
			if(rBookDataList[i].isSelected);
			else
			{
				//CA("Indide kFalse");
				flag =kFalse;
				break;
			}
			

		}	
		if(flag)
		{
			//CA("Indide kTrue");		
			InterfacePtr<ITriStateControlData>selectAllSectionCheckBoxTriState(Mediator ::selectAllCheckBoxView,UseDefaultIID());
			if(selectAllSectionCheckBoxTriState == nil)
			{
				//CA("selectAllSectionCheckBoxTriState == nil");
				return;//break;
			}
			selectAllSectionCheckBoxTriState->Select(kTrue);
			IsEventInListBox = kFalse; //This is used to handle Deselect All the CheckBoxes(icon) when unchecked selectAllCheckBox ......
		}
	
		//up to here by dattatray on 28/10
		
}	
}
 
