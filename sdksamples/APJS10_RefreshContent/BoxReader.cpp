#include "VCPluginHeaders.h"
#include "BoxReader.h"
#include "PageData.h"
//#include "TagReader.h"
#include "IAppFrameWork.h"
#include "ISpecialChar.h"
#include "CAlert.h"
#include "ITagReader.h"

//start 21Jun addition.
#include "XMLContentIterator.h"
//#include "ITextFrame.h"
#include "IFrameUtils.h"
#include "IXMLUtils.h"
#include "ITextModel.h"
#include "IXMLReferenceData.h"
#include "ITextStoryThread.h"
#include "SlugStructure.h"
#include "ITemplatesDialogCloser.h"
#include "TextIterator.h"
#include "CTUnicodeTranslator.h"

/// Global Pointers
extern ITagReader* itagReader;
extern IAppFramework* ptrIAppFramework;
extern ISpecialChar* iConverter;
///////////
extern bool16 refreshTableByAttribute;

//end 21Jun
/*

  This is how the hierarchy goes
								
								   BOXID
									/ \
								   /   \
							ObjectID   ObjectID
								/\		 \
							   /  \		Elem1
							 Elem1 Elem2
				   
For each box in the page, there will be a corresponding object of PageData.
Each box will contain the ObjectIDs of the sprayed data. (We have to consider the 
different publication IDs too. For example object id 11001 with pubID 100 is not
the same as object id 11001 for pubID 102
*/

#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("BoxReader.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
//#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
PMString  numToPMString(int32 num);

#define CA(X) CAlert::InformationAlert \
	( \
	PMString("BoxReader.cpp") + PMString("\n") + \
    PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + X)

#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}



bool16 BoxReader::getBoxInformation(const UIDRef& boxUIDRef, PageData& pData)
{

	InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader){ 
		ptrIAppFramework->LogDebug("AP7_RefreshContent::BoxReader::getBoxInformation::!itagReader");
		return kFalse;
	}
	
	TagList tList,tList_Checkforhybrid, hybridTagList;
	bool16 taggedFrameFlag=kFalse;
    //pData.boxID=boxUIDRef.GetUID();
	pData.BoxUIDRef=boxUIDRef;	

	tList_Checkforhybrid = tList=itagReader->getTagsFromBox_ForRefresh(boxUIDRef);
	if(tList_Checkforhybrid.size() == 0)
	{
		//CA("tList_Checkforhybrid.size() == 0	+	return kFalse");
		return kFalse;
	}

    //CA("before calling getTagsFromBox_ForRefresh()");
	if(refreshTableByAttribute  && tList_Checkforhybrid[0].tableType != 3)  // Allowing Content Refresh for Hybrid tables only
    {
        tList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxUIDRef);
    
        //if(tList_Checkforhybrid[0].tableType == 3)
        //{
        //    hybridTagList = itagReader->getTagsFromBox_ForRefresh(boxUIDRef);
        //    for(int32 l=0 ; l < hybridTagList.size(); l++){
        //        tList.push_back(hybridTagList[l]);
        //    }
            //for(int32 l=0 ; l < hybridTagList.size(); l++){
            //    hybridTagList[l].tagPtr->Release();
            //}
        //}
        
    }
	else
		tList=itagReader->getTagsFromBox_ForRefresh(boxUIDRef);
    
	//DeleteBlock test 20Jun
	/*for(int32 tagIndex=0;tagIndex<tList.size();tagIndex++)
	{
		CA(tList[tagIndex].tagPtr->GetTagString());
	}*/
//end test 20Jun
//CA("after calling getTagsFromBox_ForRefresh()");
	
	if(!tList.size())//The box contains no tags..But it can be the tagged frame!!!!!
	{
		tList=itagReader->getFrameTags(boxUIDRef);
		if(tList.size()<=0)
		{
			//CA("return kFalse;");
			//It really is not our box
			// added by avinash
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
			for(int32 tagIndex = 0 ; tagIndex < tList_Checkforhybrid.size() ; tagIndex++)
			{
				tList_Checkforhybrid[tagIndex].tagPtr->Release();
			}
			//till here
			return kFalse;
		}
		taggedFrameFlag=kTrue;
	}

	int32 index;

	for(int i=0; i<tList.size(); i++)
	{
		TagStruct tInfo=tList[i];		
		if(taggedFrameFlag)
		{ 
			//CA("taggedFrameFlag");
			pData.boxID=boxUIDRef.GetUID();
			this->AppendObjectInfo(tInfo, pData, kTrue);
			continue;
		}
		if(this->objectIDExists(pData, tInfo, &index))//Search for this Object ID in the pData. If it exists add as an Element or add as a new Object
		{	
			//CA("going for AppendElementInfo");
			pData.boxID=boxUIDRef.GetUID();
			this->AppendElementInfo(tInfo, pData, index);
		}
		else
		{	
			pData.boxID=boxUIDRef.GetUID();
			this->AppendObjectInfo(tInfo, pData);
		}
	}

	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}
	for(int32 tagIndex = 0 ; tagIndex < tList_Checkforhybrid.size() ; tagIndex++)
	{
		tList_Checkforhybrid[tagIndex].tagPtr->Release();
	}

	return kTrue;
}

bool16 BoxReader::elementIDExists(const ElementInfoList& eList, double elementID, PMString& elementName)
{
	
	//CA(__FUNCTION__);
	for(int i=0; i<eList.size(); i++)
	{
		if(eList[i].elementID==elementID)		
		{
			if(elementName.NumUTF16TextChars())
				elementName=eList[i].elementName;
			return kTrue;
		}
	}
	return kFalse;
}
//added on 21Jun for Item_tabbed_text
bool16 BoxReader::itemIDelementIDExists(const ElementInfoList& eList, double elementID,double itemID, PMString& elementName)
{
	
	//CA(__FUNCTION__);
	//CA_NUM("eList.size() :",eList.size());
	for(int i=0; i<eList.size(); i++)
	{
		//CA_NUM("elementID : ",elementID);
		//CA(eList[i].elementName);
		if(eList[i].elementID==elementID )//&& eList[i].typeID == itemID)		
		{
			
			if(elementName.NumUTF16TextChars())
				elementName=eList[i].elementName;
			return kTrue;
		}
	}
	return kFalse;
}
//end added on 21Jun for Item_tabbed_text

bool16 BoxReader::AppendObjectInfo(const TagStruct& tInfo, PageData& pData, bool16 isTaggedFrame)
{	
	//CA(__FUNCTION__);

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFalse;

	CObjectValue oVal;
	if(tInfo.parentId == -1 /*|| tInfo.whichTab == 5*/)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::BoxReader::AppendObjectInfo::tInfo.parentId == -1 || tInfo.whichTab == 5");
		return kFalse;
	}

	
	PMString tableFlagValue = tInfo.tagPtr->GetAttributeValue(WideString("tableFlag")); //Cs4
	if(tableFlagValue == "-15")
	{
		//ptrIAppFramework->LogDebug("AP7_RefreshContent::BoxReader::AppendObjectInfo::tableFlagValue == -15");
		return kFalse;	
	}

	PMString objName("");
    if(tInfo.whichTab == 4 && tInfo.dataType == 6)
    {
        PMString id("");
        id.AppendNumber(PMReal(tInfo.parentId));
        objName = id;
    }
	else if(
			tInfo.whichTab == 4 
			&&				
			(   
				tInfo.imgFlag == 1 ||
				//ItemTable or ItemTableInTabbedTextForm
				tInfo.isTablePresent == 1 || 
				(tInfo.tagPtr->GetAttributeValue(WideString("tableFlag")) == WideString("-11")) || //Cs4
				//This is for sectionLevelItem's attributes inside text frame.
				tInfo.tagPtr->GetAttributeValue(WideString("tableFlag"))== WideString("-1001") ||  //Cs4
				tInfo.typeId == -5 /*for custom item copy attributes*/ ||
				tInfo.tableType == 3 
				/*|| tInfo.tagPtr->GetAttributeValue("colno")== "-101" || tInfo.tagPtr->GetAttributeValue("tableFlag")== "0"*/
				|| tInfo.tagPtr->GetAttributeValue(WideString("tableFlag")) == WideString("-12")
			)
		)
	{
		//CA("inside first if");
		if(refreshTableByAttribute && !tInfo.imgFlag && tInfo.tableType != 3)
		{
			if(tInfo.childTag == 1)
			{
				PMString id("");
				id.AppendNumber(PMReal(tInfo.childId));
				objName = id;
			}
			else
			{
				PMString id("");
				id.AppendNumber(PMReal(tInfo.parentId));
				objName = id;
			}
			
		}
		else
		{
			PMString id("");
			id.AppendNumber(PMReal(tInfo.parentId));
			objName = id;
		}
	}
	else if(tInfo.tagPtr->GetTagString() == WideString("ATTRID_ATCD")) //Cs4
	{
		if(refreshTableByAttribute)
		{

			PMString id("");
			id.AppendNumber(PMReal(tInfo.childId));
			objName = id;
		}
	}
	//ProductItem or ProductCopyAttributes
	else if(tInfo.tableType == 2/*tInfo.typeId == -3*/)
	{
		//CA("tInfo.tableType == 2");
		if(tInfo.whichTab == 3)
		{
			PMString id("");
			id.AppendNumber(PMReal(tInfo.parentId));
			objName = id;
		}
		else if(tInfo.whichTab == 4)
		{
			//CA("tInfo.whichTab == 4");
			if(tInfo.childTag == 1){
				PMString id("");
				id.AppendNumber(PMReal(tInfo.childId));
				objName = id;
			}
			else{
				PMString id("");
				id.AppendNumber(PMReal(tInfo.parentId));
				objName = id;
			}
		}
		//CA("objName	:	"+objName);
	}
	else if(tInfo.whichTab == 4 || tInfo.whichTab == 3)
	{	
		//CA("tInfo.whichTab == 4 || tInfo.whichTab == 3");
		if(tInfo.whichTab == 4 ){
			PMString tagString = tInfo.tagPtr->GetTagString();

			PMString strIndex = tInfo.tagPtr->GetAttributeValue(WideString("index"));
			//CA("strIndex = "+ strIndex);
			if(strIndex=="4")
			{
				PMString childTagStr = tInfo.tagPtr->GetAttributeValue(WideString("childTag"));
				if(childTagStr == "1")//isItemPerFrame case
				{
					PMString id("");
					id.AppendNumber(PMReal(tInfo.childId));
					objName = id;
				}else
				{
					PMString id("");
					id.AppendNumber(PMReal(tInfo.parentId));
					objName = id;
				}
			}
			else
			{
				if(refreshTableByAttribute)
				{
					PMString id("");
					id.AppendNumber(PMReal(tInfo.childId));
					objName = id;

				}
				else
				{
					PMString id("");
					id.AppendNumber(PMReal(tInfo.parentId));
					objName = id;
				}
			}
		}
		else{

			PMString id("");
			id.AppendNumber(PMReal(tInfo.parentId));
			objName = id;
			//CA("objName	:	"+objName);
		}
	}
    
	if(tInfo.whichTab == 5 && tInfo.tableType == 3)
	{
		PMString id("");
		id.AppendNumber(PMReal(tInfo.sectionID));
		objName = id;
	}
	if(tInfo.whichTab == 5 )
	{

		CPubModel CPubObjectModel = ptrIAppFramework->getpubModelByPubID(tInfo.sectionID,tInfo.languageID);
		objName=CPubObjectModel.getName();
	}
	if(objName.NumUTF16TextChars()==0 )
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::BoxReader::AppendObjectInfo::objName.NumUTF16TextChars()==0");
		return kFalse;
	}

	ObjectData oData;
	if(tInfo.isTablePresent)
	{
		if(refreshTableByAttribute && tInfo.tableType != 3 )
		{
			//CA("refreshTableByAttribute && tInfo.tableType != 3 ");
			if(tInfo.dataType == 4)
				oData.objectID=tInfo.parentId;	
			else
				oData.objectID= tInfo.childId;//tInfo.typeId;	


		}
		else
			oData.objectID=tInfo.parentId;	
	}
	else if(tInfo.whichTab == 5)
		oData.objectID =tInfo.sectionID;
	else
		oData.objectID=tInfo.parentId;	

	if(isTaggedFrame && tInfo.whichTab==4)
		oData.objectName="Item Data";
	
	if(tInfo.whichTab == 5)
		oData.objectTypeID =tInfo.typeId;
	//else
	//	oData.objectTypeID=oVal.getObject_type_id();

	oData.publicationID=tInfo.sectionID;
	oData.whichTab = tInfo.whichTab;

	if(tInfo.isTablePresent/*tableFlag*/) // Added for table////added by yogesh
		oData.isTableFlag=kTrue;
	else
		oData.isTableFlag=kFalse;

	
	if(tInfo.imgFlag) // Added for Image
		oData.isImageFlag=kTrue;
	else
		oData.isImageFlag=kFalse;
		
	if(tInfo.imgFlag == 0 || tInfo.whichTab == 5) //*******
	{
		//CA("FROM MY COUNDITION");	
		oData.objectName = objName;
		//CA("After FROM MY COUNDITION"+oData.objectName);
	}
	
	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	if(iConverter)
	{
		PMString temp=iConverter->translateString(oData.objectName);
		oData.objectName=temp;
	}
//CA("oData.objectName	:	"+oData.objectName);
	pData.objectDataList.push_back(oData);

//	for(int32 i = 0;i<pData.objectDataList.size();i++)
//		//CA(pData.objectDataList[i].objectName);
//CA("before AppendElementInfo");
	this->AppendElementInfo(tInfo, pData,(int32) pData.objectDataList.size()-1, isTaggedFrame);
	return kTrue;
}

bool16 getAttributeName(double elementID, PMString& name);
bool16 getAttributeName(double elementID, PMString& name)
{
	//CA(__FUNCTION__);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFalse;

	VectorAttributeInfoPtr objPtr;

	objPtr=nil;//temp_to_test ptrIAppFramework->ATTRIBMngr_getCoreAttributes();

	if(!objPtr)
		return kFalse;

	VectorAttributeInfoValue::iterator it;

	for(it=objPtr->begin(); it!=objPtr->end(); it++)
	{
		if(it->getAttributeId()==elementID)
		{
			name=it->getDisplayName();
			if(name.NumUTF16TextChars()>0)
			{
				//delete objPtr;
				return kTrue;
			}
			else
			{
				//delete objPtr;
				return kFalse;
			}
		}
	}

	delete objPtr;
	objPtr=nil;//temp_to_test ptrIAppFramework->ATTRIBMngr_getParametricAttributes();

	if(!objPtr)
		return kFalse;

	for(it=objPtr->begin(); it!=objPtr->end(); it++)
	{
		if(it->getAttributeId()==elementID)
		{
			name=it->getDisplayName();
			if(name.NumUTF16TextChars())
			{
				//delete objPtr;
				return kTrue;
			}
			else
			{
				//delete objPtr;
				return kFalse;
			}
		}
	}
	//delete objPtr;
	return kFalse;
}


bool16 BoxReader::AppendElementInfo(const TagStruct& tStruct, PageData& pData, int32 index, bool16 isTaggedFrame)
{
	//CA(" BoxReader::AppendElementInfo");
	//CA(tStruct.tagPtr->GetTagString());
	TagInfo tInfo;
	tInfo.elementID=tStruct.elementId;
	tInfo.typeID=tStruct.typeId;
	tInfo.isTaggedFrame=isTaggedFrame;
	//Note that tStruct.isTablePresent = 1 only when the tableFalg = 1
	//in all other cases it is 0.i.e when tableFlag =-11 and tableFlag=-12
	//the tStruct.isTablePresent = 0.
	tInfo.tableFlag = tStruct.isTablePresent/*tableFlag*/;//added by yogesh
	tInfo.imageFlag = tStruct.imgFlag;
	tInfo.whichTab = tStruct.whichTab;
	tInfo.languageid = tStruct.languageID;

	tInfo.tableType = tStruct.tableType;
	tInfo.dataType = tStruct.dataType;

	tInfo.catLevel = tStruct.catLevel;
	tInfo.parentId = tStruct.parentId;
	tInfo.childTag = tStruct.childTag;

	tInfo.parentTypeId = tStruct.parentTypeID;

	tInfo.isAutoResize = tStruct.isAutoResize;
	tInfo.isSprayItemPerFrame = tStruct.isSprayItemPerFrame;
    tInfo.groupKey = tStruct.groupKey;
    
    tInfo.tStruct = tStruct;
	
	
	ObjectData oData=pData.objectDataList[index];
	ElementInfoList eList=oData.elementList;

	//Unfortunately the tagStruct data type doesn't store the original
	//attribute values. eg.The attribute tableFlag's actual value is not stored.
	//so there is no way other than taking the IIDXMLElement ptr and get the 
	//attribute value of tableFlag.As tableFlag =-12 for item_copy_attributes.
	//And tableFlag = -11 for Item tag in case of tabbedText.
	PMString tableFlagValue = tStruct.tagPtr->GetAttributeValue(WideString("tableFlag"));//Cs4

	PMString eName;
	//handled for item_copy_attributes
    //CA(tableFlagValue);
	if(tableFlagValue == "-12" || tableFlagValue == "-1001")
	{ 
		//CA("(tableFlagValue == -12 || tableFlagValue == -1001)");
		int32 inttableFlagValue = tableFlagValue.GetAsNumber();
		tInfo.tableFlag = inttableFlagValue;
		if(this->itemIDelementIDExists(eList, tInfo.elementID,tInfo.typeID,eName))//An element can be sprayed multiple times
		{
			//CA("element already present	1");			
			return kFalse;
		}
		
	}	
	else
	{//Note the condition of ItemTableInTabbedText is not hanlded separately.
	//The "ItemXX" tag is treated as product_element only.
		//CA("else 2");
		if(tInfo.whichTab == 5 && tInfo.elementID < 0)
		{
			PMString str("");
			str.AppendNumber(PMReal(tInfo.elementID));
			int32 tempNum;
			if(tInfo.catLevel < 0)
				tempNum = -(tInfo.catLevel);
			else
				tempNum = tInfo.catLevel;
			str.AppendNumber(PMReal(tempNum));
			//CA(str);
			double newElementId = str.GetAsDouble();

			tInfo.elementID = newElementId;

			if(this->elementIDExists(eList, newElementId, eName))//An element can be sprayed multiple times
			{
				//CA("element already present 3");			
				return kFalse;
			}
		}
		else
		{
			if(this->elementIDExists(eList, tInfo.elementID, eName))//An element can be sprayed multiple times
			{
				//CA("element already present 3");			
				return kFalse;
			}
		}
	}
	tInfo.StartIndex =tStruct.startIndex;
	tInfo.EndIndex= tStruct.endIndex;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFalse;
	//added on 20Jun by Yogesh to handle the new added functionality of ItemCopy attribute
	//spraying in the text frame along with the product copy attributes.
	//tableFlag == -12 only for the itemcopy attributes which are sprayed in the text 
	//frame.
	bool16 isProductItemCopyAttribute = kFalse;
	bool16 isItemItemCopyAttribute = kFalse;

	bool16 isItemTableInTabbedText = kFalse;
	bool16 isCustomTabbedText = kFalse;
	bool16 isPivotList = kFalse;

	bool16 isHybridTablePresent = kFalse;

	if(tInfo.whichTab == 5 )	//----------
	{
		//CA("Pay attention");
		InterfacePtr<ITemplatesDialogCloser> ptrTempDialogCloser((static_cast<ITemplatesDialogCloser*> (CreateObject(kTemplatesDialogCloserBoss ,ITemplatesDialogCloser::kDefaultIID))));
		if(ptrTempDialogCloser == nil)
		{
			//CA("ptrTempDialogCloser == nil");
			return kFalse;
		}
		if(ptrIAppFramework->get_isONEsourceMode())
		{
			////CA("ptrIAppFramework->get_isONEsourceMode()");
			//InterfacePtr<ITemplatesDialogCloser> ptrTempDialogCloser((static_cast<ITemplatesDialogCloser*> (CreateObject(kTemplatesDialogCloserBoss ,ITemplatesDialogCloser::kDefaultIID))));
			//if(ptrTempDialogCloser == nil)
			//{
			//	//CA("ptrTempDialogCloser == nil");
			//	return kFalse;
			//}
			//PMString eventName("");
			//if(tInfo.catLevel > 0)
			//	eventName=ptrTempDialogCloser->getCatagoryName(tInfo.elementID , tInfo.typeID , tInfo.catLevel);
			//else
			//	eventName=ptrTempDialogCloser->getCatagoryName(tInfo.elementID , tInfo.typeID );

			//eName=eventName;
			//tInfo.elementName = eName;
			//pData.objectDataList[index].elementList.push_back(tInfo);

		}
		else
		{
			//CA("!get_isONEsourceMode()");
			PMString elementName("");
			if(tInfo.elementID < 0)
			{
				elementName = tStruct.tagPtr->GetTagString();		
			}
			else
			{
				elementName = tStruct.tagPtr->GetTagString();
			}
			//CA("eventName	:	"+eventName);
			eName=elementName;
			tInfo.elementName = eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
		}
		return kTrue;
	}

	if(tInfo.tableType == 3)
	{	
		//CA("tInfo.tableType == 3");
		PMString tagString1 = tStruct.tagPtr->GetTagString();
		PMString underScore = "_";
		bool16 searchUnderScore = tagString1.Contains(underScore);
		if(searchUnderScore)
		{
			int32 indexOfUnderScore = tagString1.IndexOfString(underScore);
			tagString1.Remove(indexOfUnderScore);
		//	CA(tagString1);
			PMString space = " ";
			tagString1.Insert(space,indexOfUnderScore);
		}
		//CA(tagString1);
		bool16 result = kFalse;
		int32 childCount = tStruct.tagPtr->GetChildCount();
		if(childCount == 0)
		{
			//CA("childCount == 0");
			eName = tagString1;	
		}
		else
		{
			//CA("else");

			XMLReference tableXMLRef =  tStruct.tagPtr->GetNthChild(0);
			//IIDXMLElement *PSHybridTableTagPtr = tableXMLRef.Instantiate();
			InterfacePtr<IIDXMLElement>PSHybridTableTagPtr(tableXMLRef.Instantiate());
			PMString tagString = PSHybridTableTagPtr->GetTagString();	
			
			if(tagString == "PSHybridTable")
			{
				eName = tagString1;//"Hybrid Table";  
			}
			else
			{
				if(tStruct.childTag == 1/*tStruct.colno == -101 || tStruct.colno == -555*/)
				{
					eName = tagString1;//"Hybrid Table_TabbedText";
					eName.Append("_TabbedText");
				}
			}
		}
		//CA(eName);									
		if(eName.NumUTF16TextChars()==0)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BoxReader::AppendElementInfo::isCustomTabbedText:eName.NumUTF16TextChars()==0");					
			return result;
		}
		tInfo.elementName = eName;
		pData.objectDataList[index].elementList.push_back(tInfo);
		result = kTrue;
		
		return result;
		//}
	}

    if(tInfo.whichTab == 4 && tInfo.dataType == 6)
    {
        bool16 result = kFalse;
        PMString eName("");
        
        eName = ptrIAppFramework->StructureCache_getItemAttributeGroupNameByAttributeGroupId(tInfo.elementID);
        eName = tInfo.groupKey + " (Attribute Group)";
        tInfo.elementName=eName;
        pData.objectDataList[index].elementList.push_back(tInfo);
        result = kTrue;
        return result;
    }
	if(tInfo.imageFlag == 0 && (tInfo.tableFlag == 0 || tInfo.tableFlag == -12 || tInfo.tableFlag == -1001 ) )
	{
		//CA("tInfo.imageFlag == 0 && (tInfo.tableFlag == 0 || tInfo.tableFlag == -12 || tInfo.tableFlag == -1001)");
		VectorElementInfoPtr eleValObj = nil;
		bool16 isAllStandardTables = kFalse;
		switch(tStruct.whichTab)
		{
		case 1:
			//eleValObj = ptrIAppFramework->ElementCache_getCopyAttributesByIndex(1, tInfo.languageid);
			break;
		case 2: 
			//eleValObj = ptrIAppFramework->ElementCache_getCopyAttributesByIndex(2, tInfo.languageid);
			break;
		case 3:
			{//For ItemTable in tabbed text format.
				if(tableFlagValue == "-11")
				{
					//CA("tableFlagValue == -11");
					isItemTableInTabbedText = kTrue;
					//This code is for refresh
					////CA("tableFlagValue == -11 found");
					////IIDXMLElement * tagPtr = tStruct.tagPtr;
					////if(tagPtr == nil)
					////{
					////	CA("tagPtr == nil");
					////	break;
					////}
					////if(tStruct.curBoxUIDRef == UIDRef::gNull)
					////{
					////	CA("tStruct.curBoxUIDRef is invalid");

					////}
					//end
					//alternate method to get the UIDRef of textmodel
					//alternate 1
					/*XMLContentReference textFrameXMLCntnRef = tagPtr->GetContentReference();
					IXMLReferenceData *textFrameXMLRefData = textFrameXMLCntnRef.Instantiate();
					//textFrameXMLRefData is coming as nil.
					//This alternate is failing.
					if(textFrameXMLRefData == nil)
					{
						CA("textFrameXMLRefData == nil");
						break;
					}
					InterfacePtr<ITextModel>textModel(textFrameXMLRefData,UseDefaultIID());
					if(textModel == nil)
					{
						CA("textModel == nil");
						break;
					}
					*/
					//alternate 2
					//This code is for refresh
					//////InterfacePtr<ITextStoryThread> textStoryThread(tagPtr->QueryContentTextStoryThread(),UseDefaultIID());
					//////if(textStoryThread == nil)
					//////{
					//////	CA("textStoryThread == nil");
					//////	break;
					//////}
					//////InterfacePtr<ITextModel>textModel(textStoryThread->QueryTextModel(),UseDefaultIID());
					//////if(textModel == nil)
					//////{
					//////	CA("textModel == nil");
					//////	break;
					//////}
					//end
					//end alternate method to get the UIDRef of textmodel
					/*InterfacePtr<IPMUnknown> unknown(tStruct.curBoxUIDRef, IID_IUNKNOWN);
					if(unknown == nil)
					{
						CA("unknown == nil");
						break;
					}
					UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
					if (textFrameUID == kInvalidUID)
					{
						CA("textFrameUID == kInvalidUID");
						break;
					}
					
					InterfacePtr<ITextFrame> textFrame(tStruct.curBoxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
					if (textFrame == nil)
					{
						CA("textFrame == nil");
						break;
					}
					
					InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
					if (textModel == nil)
					{
						CA("textModel == nil");
						break;
					}
					*/
					
					//This code is for refresh
					////UIDRef txtModelUIDRef = ::GetUIDRef(textModel);

					////int32 startReplaceFrom = -1;
					////int32 endReplace = -1;
					////Utils<IXMLUtils>()->GetElementIndices(tagPtr,&startReplaceFrom,&endReplace);
					////startReplaceFrom = startReplaceFrom +1;
					////endReplace = endReplace -1;

					////PMString textToInsert("");
					////WideString insertText(textToInsert);
					////textModel->Replace(kTrue,startReplaceFrom,endReplace-startReplaceFrom + 1 ,&insertText);
					////
					////sprayItemTableInTabbedTextForm(tStruct,textModel);
					//end

					//XMLReference tagRef = tagPtr->GetXMLReference();
					//Utils<IXMLElementCommands>()->DeleteElement(tagRef,kTrue);
					/*
					int32 noOfChildElements = tagPtr->GetChildCount () ;
					if(noOfChildElements <= 0)
						break;
					for(int32 childIndex=0;childIndex < noOfChildElements;childIndex++)
					{
						XMLReference childRef = tagPtr->GetNthChild(childIndex);
						IIDXMLElement * childTagPtr = childRef.Instantiate();
						if(childTagPtr == nil)
							continue;

						
					}
					*/
			   	}
				else if(tInfo.typeID == -5 )
				{
					//CA("isCustomTabbedText = kTrue");
					isCustomTabbedText = kTrue;
				}
				else if(tInfo.tableType == 7/*tInfo.typeID == -116*/)
				{
					//CA("tInfo.tableType == 7");
					isAllStandardTables = kTrue;
				}
				else
				{					
					//CA("here");
					eleValObj = ptrIAppFramework->StructureCache_getItemGroupCopyAttributesByLanguageId( tInfo.languageid);
				}
			}
			break;
		case 4:
			if(tableFlagValue == "-11")
			{
				//CA("tableFlagValue == -11 6 ");
				isItemTableInTabbedText = kTrue;
			}
			else if(tInfo.typeID == -5)
			{
				//CA("isCustomTabbedText = kTrue");
				isCustomTabbedText = kTrue;
			}
			else if(tInfo.typeID == -57)
			{
				//CA("isCustomTabbedText = kTrue");
				isPivotList = kTrue;
			}
            else if(tInfo.dataType == 6)
            {
                //CA("isCustomTabbedText = kTrue");
                isItemItemCopyAttribute = kTrue;
            }
			else
			{
				//CA("tableFlagValue == -1001");
				if(tableFlagValue == "-1001")
				{					
					isItemItemCopyAttribute = kTrue;
				}
				else
				{
					isProductItemCopyAttribute = kTrue;
				}
			}
            
			//CA("This condition is now handled");			
			break;
		}
		if(isItemTableInTabbedText)
		{
			//CA("isItemTableInTabbedText == kTrue");
			bool16 result = kFalse;
			do
			{					
				PMString eName("");
				VectorTypeInfoPtr typeValObj = nil;
				if(tStruct.whichTab == 3)
					typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
				else if(tStruct.whichTab == 4)
					typeValObj =ptrIAppFramework->StructureCache_getListTableTypes();

				if(typeValObj==nil)
				{
					ptrIAppFramework->LogError("AP7_RefreshContent::BoxReader::AppendElementInfo::typeValObj==nil");
					break;
				}
				
				//if((tInfo.typeID != -111) && (tInfo.typeID != -112)&& (tInfo.typeID != -113) && (tInfo.typeID != -114))
				if((tInfo.tableType != 4)&& (tInfo.tableType != 5) && (tInfo.tableType != 6))
				{
					VectorTypeInfoValue::iterator it1;
					for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
					{	
						if((it1->getTypeId() == tStruct.typeId)&& (/*tStruct.elementId == -101*/tStruct.childTag == 1 || tStruct.elementId == -103 || tStruct.elementId == -1))//4 Sept ItemTableHeader
						{
							eName = it1->getName();
							eName.Append("_TabbedText");
						}

					}
				}
				//Added to handle Med Custom Table refresh i.e entry should be come in listBox By Dattatray on 17/11
				/*if(tInfo.typeID == -111)
				{
					eName = "Custom Table";	
					eName.Append("_TabbedText");
				}*/
				if(tInfo.tableType == 4/*tInfo.typeID == -112*/)
				{
					eName = "Component Table";	
					eName.Append("_TabbedText");
				}
				if(tInfo.tableType == 6/*tInfo.typeID == -113*/)
				{
					eName = "XRef Table";	
					eName.Append("_TabbedText");
				}
				if(tInfo.tableType == 5/*tInfo.typeID == -114*/)
				{
					eName = "Accessory Table";	
					eName.Append("_TabbedText");
				}

				//end here 17/11
				
				if(eName.NumUTF16TextChars()==0)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::BoxReader::AppendElementInfo::eName.NumUTF16TextChars()==0");					
					break;//CA(eName);
				}
                
                int32 tagStartPos = -1;
                int32 tagEndPos = -1;
                Utils<IXMLUtils>()->GetElementIndices(tStruct.tagPtr, &tagStartPos, &tagEndPos);
                tagStartPos = tagStartPos + 1;
                tagEndPos = tagEndPos -1;
                
                ITextModel* textModel = Utils<IXMLUtils>()->QueryTextModel(tStruct.tagPtr);
                
                PMString entireStory("");
                bool16 result1 = kFalse;
                result1= GetTextstoryFromBox(textModel, tagStartPos, tagEndPos, entireStory);
                tInfo.value = entireStory;
                
				tInfo.elementName=eName;
				pData.objectDataList[index].elementList.push_back(tInfo);
				result = kTrue;
			}while(kFalse);
			return result;
		}

		//20Jun.ItemCopyTag handling.
		//This is the newly added code and put in the if block.
		if(isItemItemCopyAttribute || isProductItemCopyAttribute)
		{
			//CA("isItemItemCopyAttribute || isProductItemCopyAttribute  8");
			bool16 result = kFalse;
			do
			{
				//if(tStruct.elementId == -701 )//added by Tushar on 28/12/06
				//{
				//	eName = "Event Price";
				//	result = kTrue;
				//	
				//}	
				//else if(tStruct.elementId == -702)
				//{
				//	eName = "Event Suffix";
				//	result = kTrue;
				//	
				//}
				//else 
				if(tStruct.elementId == -703 )
				{
					eName = "$ Off";
					result = kTrue;
					
				}
				else if(tStruct.elementId==-803)
				{
					//CA("-803");
					eName = "LetterKey";
					result = kTrue;
				}
				else if(tStruct.elementId == -704)
				{
					eName = "% Off";
					result = kTrue;				
				}
				//ended on 25Sept..EventPrice addition.
				//else 

				else if(tInfo.elementID == -401)
					eName = "Make";
				else if(tInfo.elementID == -402)
					eName = "Model";
				else if(tInfo.elementID == -403)
					eName = "Year";
				else if(tInfo.elementID==-225)
				{
					eName = "Vendor Description 1";						
				}
				else if(tInfo.elementID==-226)
				{
					eName = "Vendor Description 2";	
				}
				else if(/*tInfo.elementID  == -121*/tInfo.dataType == 4 && tInfo.tableFlag == 0)
				{
					//CA("Put Table Name in eName");
					eName = "Table Name";	
				}
				else if(tInfo.elementID==-980 && tInfo.dataType==5 && tInfo.tableFlag==0)
				{
					eName = "Note1";	
				}
				else if(tInfo.elementID==-979 && tInfo.dataType==5 && tInfo.tableFlag==0)
				{
					eName = "Note2";	
				}
				else if(tInfo.elementID==-978 && tInfo.dataType==5 && tInfo.tableFlag==0)
				{
					eName = "Note3";	
				}
				else if(tInfo.elementID==-977 && tInfo.dataType==5 && tInfo.tableFlag==0)
				{
					eName = "Note4";	
				}
				else if(tInfo.elementID==-976 && tInfo.dataType==5 && tInfo.tableFlag==0)
				{
					eName = "Note5";	
				}
                else if(tInfo.elementID==-975 && tInfo.dataType==5 && tInfo.tableFlag==0)
				{
					eName = "Adv. Table Note1";
				}
				else if(tInfo.elementID==-974 && tInfo.dataType==5 && tInfo.tableFlag==0)
				{
					eName = "Adv. Table Note2";
				}
				else if(tInfo.elementID==-973 && tInfo.dataType==5 && tInfo.tableFlag==0)
				{
					eName = "Adv. Table Note3";
				}
				else if(tInfo.elementID==-972 && tInfo.dataType==5 && tInfo.tableFlag==0)
				{
					eName = "Adv. Table Note4";
				}
				else if(tInfo.elementID==-971 && tInfo.dataType==5 && tInfo.tableFlag==0)
				{
					eName = "Adv. Table Note5";
				}
				else if(tInfo.elementID==-983 && tInfo.dataType==5 && tInfo.tableFlag==0)
				{
					eName = "List Discription";	
				}
				else if(tInfo.elementID==-982 && tInfo.dataType==5 && tInfo.tableFlag==0)
				{
					eName = "Stencil Name";	
				}
                else if(tInfo.elementID==-982 && tInfo.dataType==5 && tInfo.tableFlag==0)
                {
                    eName = "Stencil Name";
                }
                
				else
					eName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(tStruct.elementId,tStruct.languageID );

			
				if(eName.NumUTF16TextChars()==0)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::BoxReader::AppendElementInfo::eName.NumUTF16TextChars()==0");					
					break;
				}
                
                int32 tagStartPos = -1;
                int32 tagEndPos = -1;
                Utils<IXMLUtils>()->GetElementIndices(tStruct.tagPtr, &tagStartPos, &tagEndPos);
                
                tagStartPos = tagStartPos + 1;
                tagEndPos = tagEndPos -1;
                
                ITextModel* textModel = Utils<IXMLUtils>()->QueryTextModel(tStruct.tagPtr);
                
                PMString entireStory("");
                bool16 result1 = kFalse;
                result1= GetTextstoryFromBox(textModel, tagStartPos, tagEndPos, entireStory);
                tInfo.value = entireStory;
                
				tInfo.elementName = eName;
                
				pData.objectDataList[index].elementList.push_back(tInfo);
				result = kTrue;
			}while(kFalse);
			return result;
		}		
		if(isPivotList==kTrue)
		{
		
			bool16 result = kFalse;
			eName = "Pivot List"; 
			
			if(eName.NumUTF16TextChars()==0)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::BoxReader::AppendElementInfo::isCustomTabbedText:eName.NumUTF16TextChars()==0");					
				return result;
			}
			tInfo.elementName = eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			result = kTrue;
			
			return result;
		}
		if(isCustomTabbedText == kTrue)
		{
			//CA("isCustomTabbedText == kTrue");
			bool16 result = kFalse;
			eName = "CustomTabbedText"; 
			
			if(eName.NumUTF16TextChars()==0)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::BoxReader::AppendElementInfo::isCustomTabbedText:eName.NumUTF16TextChars()==0");					
				return result;
			}
			tInfo.elementName = eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			result = kTrue;
			
			return result;
		}

		//No need to "else" as the previous if block returns from the function.
		if(/*tInfo.elementID  == -121*/tInfo.dataType == 4 /*&& tInfo.tableFlag == 0 */)
		{
			//CA("Put Table Name in eName");
			eName = "Table Name";	
			tInfo.elementName=eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			return kTrue;
		}
		if(tInfo.elementID==-980 && tInfo.dataType==5 && tInfo.tableFlag==0)
		{
			eName = "Note 1";
			tInfo.elementName=eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			return kTrue;
		}
		if(tInfo.elementID==-979 && tInfo.dataType==5 && tInfo.tableFlag==0)
		{
			eName = "Note 2";
			tInfo.elementName=eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			return kTrue;
		}
		if(tInfo.elementID==-978 && tInfo.dataType==5 && tInfo.tableFlag==0)
		{
			eName = "Note 3";
			tInfo.elementName=eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			return kTrue;
		}
		if(tInfo.elementID==-977 && tInfo.dataType==5 && tInfo.tableFlag==0)
		{
			eName = "Note 4";
			tInfo.elementName=eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			return kTrue;
		}
		if(tInfo.elementID==-976 && tInfo.dataType==5 && tInfo.tableFlag==0)
		{
			eName = "Note 5";
			tInfo.elementName=eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			return kTrue;
		}
        if(tInfo.elementID==-975 && tInfo.dataType==5 && tInfo.tableFlag==0)
		{
			eName = "Adv. Table Note 1";
			tInfo.elementName=eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			return kTrue;
		}
		if(tInfo.elementID==-974 && tInfo.dataType==5 && tInfo.tableFlag==0)
		{
			eName = "Adv. Table Note 2";
			tInfo.elementName=eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			return kTrue;
		}
		if(tInfo.elementID==-973 && tInfo.dataType==5 && tInfo.tableFlag==0)
		{
			eName = "Adv. Table Note 3";
			tInfo.elementName=eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			return kTrue;
		}
		if(tInfo.elementID==-972 && tInfo.dataType==5 && tInfo.tableFlag==0)
		{
			eName = "Adv. Table Note 4";
			tInfo.elementName=eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			return kTrue;
		}
		if(tInfo.elementID==-971 && tInfo.dataType==5 && tInfo.tableFlag==0)
		{
			eName = "Adv. Table Note 5";
			tInfo.elementName=eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			return kTrue;
		}
		if(tInfo.elementID==-983 && tInfo.dataType==5 && tInfo.tableFlag==0)
		{
			eName = "List Discription";	
			tInfo.elementName=eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			return kTrue;
		}
		if(tInfo.elementID==-982 && tInfo.dataType==5 && tInfo.tableFlag==0)
		{
			eName = "Stencil Name";	
			tInfo.elementName=eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			return kTrue;
		}
		if(isAllStandardTables)
		{
			//CA("isAllStandardTables");
			eName = "All Standard Tables_TabbedText";	
			tInfo.elementName=eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			return kTrue;
		}
		if(tInfo.elementID==-225)
		{
			eName = "Vendor Description 1";	
			tInfo.elementName=eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			return kTrue;
		}
		if(tInfo.elementID==-226)
		{
			eName = "Vendor Description 2";	
			tInfo.elementName=eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			return kTrue;
		}
//CA("1");
//CA(eName);

		if(eleValObj==nil)
			return kFalse;
		VectorElementInfoValue::iterator it;

		for(it=eleValObj->begin();it!=eleValObj->end();it++)
		{
			if(it->getElementId()==tStruct.elementId)
			{
				eName=it->getDisplayName();
				break;
			}
		}

		if(eleValObj)
			delete eleValObj;
	
		if(eName.NumUTF16TextChars()==0)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BoxReader::AppendElementInfo::eName.NumUTF16TextChars()==0");							
			return kFalse;
		}

		tInfo.elementName=eName;
		pData.objectDataList[index].elementList.push_back(tInfo);
		return kTrue;

	}
	else if((tInfo.imageFlag == 1 || tInfo.tableFlag == 1) && tInfo.tableType != 2/*tInfo.typeID != -3*/)//Images
	{
		//CA(" tInfo.imageFlag == 1 || tInfo.tableFlag == 1 ");
		VectorTypeInfoPtr vinfoPtr=nil;
		bool16 isImageType = kFalse;
		switch(tStruct.whichTab)
		{
		case 1:
			//vinfoPtr=ptrIAppFramework->ElementCache_getImageAttributesByIndex(1);
			break;
		case 2:
			//vinfoPtr=ptrIAppFramework->ElementCache_getImageAttributesByIndex(1);
			break;
		case 3:
			if(tInfo.tableFlag == 1)
			{
				/*if(tInfo.typeID == -111)
				{
					break;
				}*/
				vinfoPtr=ptrIAppFramework->StructureCache_getListTableTypes();				
				break;
			}
			//added by name
			isImageType = kTrue;
			if(tInfo.whichTab==3 && tInfo.typeID == -98)
			{
				
				eName="First Available Item Group Image";
				break;
			}	

			if(tInfo.typeID == -207 || tInfo.typeID == -208 || tInfo.typeID == -209|| tInfo.typeID == -210 || tInfo.typeID == -211)
			{
				eName = "Brand Image ";
				if(tInfo.typeID == -207)
					eName.Append("1");
				else if(tInfo.typeID == -208)
					eName.Append("2");
				else if(tInfo.typeID == -209)
					eName.Append("3");
				else if(tInfo.typeID == -210)
					eName.Append("4");
				else if(tInfo.typeID == -211)
					eName.Append("5");
				break;
			}
			if(tInfo.typeID == -212 || tInfo.typeID == -213 || tInfo.typeID == -214 || tInfo.typeID == -215 || tInfo.typeID == -216)
			{
				eName = "Manufacturer Image ";
				if(tInfo.typeID == -212)
					eName.Append("1");
				else if(tInfo.typeID == -213)
					eName.Append("2");
				else if(tInfo.typeID == -214)
					eName.Append("3");
				else if(tInfo.typeID == -215)
					eName.Append("4");
				else if(tInfo.typeID == -216)
					eName.Append("5");
				break;
			}
			if(tInfo.typeID == -217 || tInfo.typeID == -218 || tInfo.typeID == -219 || tInfo.typeID == -220 || tInfo.typeID == -221)
			{
				eName = "Vendor Image "; //Supplier
				if(tInfo.typeID == -217)
					eName.Append("1");
				else if(tInfo.typeID == -218)
					eName.Append("2");
				else if(tInfo.typeID == -219)
					eName.Append("3");
				else if(tInfo.typeID == -220)
					eName.Append("4");
				else if(tInfo.typeID == -221)
					eName.Append("5");
				break;
			}
			if(tInfo.typeID == -222 || tInfo.typeID == -223 )
			{
				eName = "Publication Logo ";
				if(tInfo.typeID == -222)
					eName.Append("1");
				else if(tInfo.typeID == -223)
					eName.Append("2");
				break;
			}
			eName= ptrIAppFramework->StructureCache_TYPECACHE_getTypeNameById(tStruct.typeId);
			break;
			
		case 4:
			if(tInfo.tableFlag == 1)
			{  
				/*if((tInfo.typeID == -112) || (tInfo.typeID == -113) || (tInfo.typeID == -114) )*/
				if((tInfo.tableType == 4) || (tInfo.tableType == 5) || (tInfo.tableType == 6) )
				{
					break;
				}
				//No need to "else" as the previous if block returns from the function.
				if(tInfo.dataType == 4)
				{
					//CA("Put Table Name in eName");
					eName = "Table Name";	
					tInfo.elementName=eName;
					pData.objectDataList[index].elementList.push_back(tInfo);
					return kTrue;
				}
				vinfoPtr=ptrIAppFramework-> StructureCache_getListTableTypes();
				break;
			}
			isImageType = kTrue;
			if(tInfo.whichTab==4 && tInfo.typeID == -99)
			{
				eName="First Available Item Image";
				break;
			}
			if(tInfo.typeID == -207 || tInfo.typeID == -208 || tInfo.typeID == -209|| tInfo.typeID == -210 || tInfo.typeID == -211)
			{
				eName = "Brand Image ";
				if(tInfo.typeID == -207)
					eName.Append("1");
				else if(tInfo.typeID == -208)
					eName.Append("2");
				else if(tInfo.typeID == -209)
					eName.Append("3");
				else if(tInfo.typeID == -210)
					eName.Append("4");
				else if(tInfo.typeID == -211)
					eName.Append("5");
				break;
			}
			if(tInfo.typeID == -212 || tInfo.typeID == -213 || tInfo.typeID == -214 || tInfo.typeID == -215 || tInfo.typeID == -216)
			{
				eName = "Manufacturer Image ";
				if(tInfo.typeID == -212)
					eName.Append("1");
				else if(tInfo.typeID == -213)
					eName.Append("2");
				else if(tInfo.typeID == -214)
					eName.Append("3");
				else if(tInfo.typeID == -215)
					eName.Append("4");
				else if(tInfo.typeID == -216)
					eName.Append("5");
				break;
			}
			if(tInfo.typeID == -217 || tInfo.typeID == -218 || tInfo.typeID == -219 || tInfo.typeID == -220 || tInfo.typeID == -221)
			{
				eName = "Supplier Image ";
				if(tInfo.typeID == -217)
					eName.Append("1");
				else if(tInfo.typeID == -218)
					eName.Append("2");
				else if(tInfo.typeID == -219)
					eName.Append("3");
				else if(tInfo.typeID == -220)
					eName.Append("4");
				else if(tInfo.typeID == -221)
					eName.Append("5");
				break;
			}
			if(tInfo.typeID == -222 || tInfo.typeID == -223 )
			{
				eName = "Publication Logo ";
				if(tInfo.typeID == -222)
					eName.Append("1");
				else if(tInfo.typeID == -223)
					eName.Append("2");
				break;
			}
            if(tInfo.typeID == -1 && tInfo.elementID != -1)
            {
                eName= ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(tStruct.elementId,tStruct.languageID );
                break;
            }
			//if (tStruct.rowno != 0 || tStruct.rowno != -1)
			//{	
				
				eName= ptrIAppFramework->StructureCache_TYPECACHE_getTypeNameById(tStruct.typeId);
				break;

			//}
			//vinfoPtr=ptrIAppFramework->AttributeCache_getItemImages();
			//break;
		}
		if(isImageType == kFalse)
		{	
			//CA(" isImageType == kFalse ");
			/*if(tInfo.typeID == -111)
			{
				eName = "Custom Table";				
			}
			else*/ if(tInfo.tableType == 4/*tInfo.typeID == -112*/)
			{
				//CA("eName = Component Table ");
				eName = "Component Table";				
			}
			else if(tInfo.tableType == 6/*tInfo.typeID == -113*/)
			{
				//CA("eName = Component Table ");
				eName = "XRef Table";				
			}
			else if(tInfo.tableType == 5/*tInfo.typeID == -114*/)
			{
				//CA("eName = Component Table ");
				eName = "Accessory Table";				
			}
			else
			{
				VectorTypeInfoValue::iterator it;
				for(it=vinfoPtr->begin(); it!=vinfoPtr->end(); it++)
				{
					if(it->getTypeId()==tInfo.typeID)
					{
						// Awasthi
						eName=it->getName();				
						break;
					}
				}

			if(vinfoPtr)
				delete vinfoPtr;
			}
		}
		if(eName.NumUTF16TextChars())
		{	
			tInfo.elementName=eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			return kTrue;
		}
	}
	else if(tInfo.imageFlag == 1 || (tInfo.tableFlag == 1 && tInfo.tableType == 2/*tInfo.typeID == -3*/))//Images
	{
		//CA("image flag is ");
		eName = tStruct.tagPtr->GetTagString();
		if(eName == "PSTable0" && tStruct.rowno == -904)
			eName = "MMY Table";
		else if(eName == "PSTable0")
			eName = "Custom Item Table";
		//CA(eName);
		if(eName.NumUTF16TextChars())
		{		
			tInfo.elementName=eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			return kTrue;
		}
	}
	return kFalse;
}


bool16 BoxReader::objectIDExists(const PageData& pData, const TagStruct& tInfo, int32* index)
{
	//CA(__FUNCTION__);
	for(int i=0; i<pData.objectDataList.size(); i++)
	{
		ObjectData oData=pData.objectDataList[i];

		if(refreshTableByAttribute && tInfo.tableType != 3)
		{
			//CA("tInfo.tableType != 3");
			if(oData.objectID==tInfo.typeId && oData.publicationID==tInfo.sectionID)
			{
				//CA("oData.objectID==tInfo.typeId");
				*index=i;
				return kTrue;
			}
		}
		else
		{
			//CA("tInfo.tableType != 3");
			if(oData.objectID==tInfo.parentId && oData.publicationID==tInfo.sectionID)
			{
				//CA("oData.objectID==tInfo.parentId");
				*index=i;
				return kTrue;
			}
		}
		
	}
	return kFalse;
}


bool16 BoxReader::getBoxInformation(const UIDRef& boxUIDRef, PageData& pData, int32 start, int32 end)
{
	//CA(__FUNCTION__);
	pData.boxID=boxUIDRef.GetUID();
	pData.BoxUIDRef =boxUIDRef;
	//TagReader tReader;
	InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader){ 
		ptrIAppFramework->LogDebug("AP7_RefreshContent::BoxReader::getBoxInformation::!itagReader");					
		return kFalse;
	}

	TagList tList;
	tList=itagReader->getTagsFromBox(boxUIDRef);

	if(!tList.size())//The box contains no tags..But it can be the tagged frame!!!!!
	{
		tList=itagReader->getFrameTags(boxUIDRef);
		if(tList.size()<=0)
		{
			//It really is not our box
			return kFalse;
		}
	}
	int32 index;

	for(int i=0; i<tList.size(); i++)
	{
		TagStruct tInfo=tList[i];
		if(tInfo.endIndex<=end && tInfo.startIndex>=start)
		{
			if(this->objectIDExists(pData, tInfo, &index))
			{
				this->AppendElementInfo(tInfo, pData, index);
			}
			else
			{
				this->AppendObjectInfo(tInfo, pData);
			}
		}
	}
	//-------lalit-----
	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}
	return kTrue;
}

bool16 BoxReader::GetTextstoryFromBox(ITextModel* iModel, int32 startIndex, int32 finishIndex, PMString& story)
{
    TextIterator begin(iModel, startIndex);
    TextIterator end(iModel, finishIndex);
    
    for (TextIterator iter = begin; iter <= end; iter++)
    {
        const textchar characterCode = (*iter).GetValue();
        char buf;
        ::CTUnicodeTranslator::Instance()->TextCharToChar(&characterCode, 1, &buf, 1);
        story.Append(buf);
    }	
    return kTrue;
}
