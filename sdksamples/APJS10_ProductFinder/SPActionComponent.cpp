//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ISession.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"

// General includes:
#include "CActionComponent.h"
#include "CAlert.h"

// Project includes:
#include "SPID.h"
#include "SPActionComponent.h"
//#include "IClientOptions.h"
#include "MediatorClass.h"
//#include "IPaletteMgr.h"   //Commented By sachin sharma on 29/06/07
#include "PaletteRefUtils.h"  //Added
#include "IPanelMgr.h"
#include "ITextControlData.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "IAppFramework.h"
#include "RsrcSpec.h"
#include "SPSelectionObserver.h"
#include "SPPRImageHelper.h"
#include "ISPXLibraryViewController.h"
//#include "ISearch.h"
#include "IWindow.h"
#include "IWidgetParent.h"
#include "ITriStateControlData.h"
#include "ICategoryBrowser.h"

/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

	
	@ingroup apjs9_productfinder

*/

IDialog* SPActionComponent::DlgPtr =0;
//IPaletteMgr* SPActionComponent::palettePanelPtr=0; //Commented By Sachin sharma

int32 Flag1= 0;

int32 ListFlag= 0; // Global Flag when ListFlag = 0 select All, 1 = Products Only, 2 = Items Only. 
extern double CurrentSelectedSubSection;
extern double CurrentSelectedSection;
extern int32 global_project_level ;
extern bool8 FilterFlag;
extern int32 check;
//extern vector<bool8> GlobalDesignerActionFlags;

extern bool16 isSprayItemPerFrameFlag ;	//-------
extern bool16 isItemHorizontalFlow;

#define CA(z) CAlert::InformationAlert(z)
bool16 isThumb = kFalse; // Chetan --
//extern K2Vector<PMString> imageVector2 ; // Chetan --
extern bool16 RefreshFlag ;
bool16 searchResult = kFalse;
VectorObjectInfoPtr global_vectorObjectValuePtr = NULL;
VectorItemModelPtr global_vectorItemModelPtr = NULL;
bool16 LeadingItemFlag = kFalse;

extern double global_lang_id ;


//class SPActionComponent : public CActionComponent
//{
//public:
///**
// Constructor.
// @param boss interface ptr from boss object on which this interface is aggregated.
// */
//		SPActionComponent(IPMUnknown* boss);
//
//		/** The action component should perform the requested action.
//			This is where the menu item's action is taken.
//			When a menu item is selected, the Menu Manager determines
//			which plug-in is responsible for it, and calls its DoAction
//			with the ID for the menu item chosen.
//
//			@param actionID identifies the menu item that was selected.
//			@param ac active context
//			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
//			@param widget contains the widget that invoked this action. May be nil. 
//			*/
//		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);
//
//	private:
//		/** Encapsulates functionality for the about menu item. */
//		void DoAbout();
//		
//
//
//};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(SPActionComponent, kSPActionComponentImpl)

/* SPActionComponent Constructor
*/
SPActionComponent::SPActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{
}

/* DoAction
*/
void SPActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{//CA("doaction");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil){
		//CA("No ptrIAppFramework");
		return;
	}
	switch (actionID.Get())
	{

		case kSPPopupAboutThisActionID:
		case kSPAboutActionID:
		{
			this->DoAbout();
			break;
		}

		case kSPPanelPSMenuActionID:
		case kSPPanelWidgetActionID:
		{
			//CA("ActionComponent 1");
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
				break;
			//CA("ActionComponent 2");
			bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
			if(!isUserLoggedIn)
				break;
			//CA("ActionComponent 3");
			/*InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
			if(!ptrIClientOptions)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPActionComponent::DoAction::kSPPanelWidgetActionID--No ptrIClientOptions");								
				break;
			}*/

			//CA("ActionComponent 4");

			if(Flag1==0)
				Flag1=1;
			PMString defPubName("");
			double defPubId=-1;
			Mediator md;
			//CA("ActionComponent 5");
			//defPubId=ptrIClientOptions->getDefPublication(defPubName);
			defPubId = ptrIAppFramework->getCatalogId();
			if(defPubId<=0){
				//CA("defPubId<=0");

				InterfacePtr<ICategoryBrowser> CatalogBrowserPtr((ICategoryBrowser*)::CreateObject(kCTBCategoryBrowserBoss, IID_ICATEGORYBROWSER));
				if(!CatalogBrowserPtr)
				{
					ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::Update::kSPSelectClassWidgetID--No CatalogBrowserPtr");
					return ;
				}
				CatalogBrowserPtr->OpenCategoryBrowser(0, 0, 150, 130 ,1 , -1);

				ptrIAppFramework->LogError("AP7_ProductFinder::SPActionComponent::DoAction::kSPPanelWidgetActionID--  defPubId<=0");								
				
				break;
			}
			//CA("ActionComponent 6");

			md.setPublicationRoot(defPubId);
			check = 1;
			this->DoPalette();
			break;
		}
					

		case kSPDialogActionID:
		{
			this->DoDialog();
			break;
		}
		
		case kSPMenuItem1ActionID:
		{
			//CA("ActionComponent 8 ");
			this->DoMenuItem1(ac);
			//CA("ActionComponent 9");
			break;
		}

		case kSPHybridTableOnlyActionID:
			{
				//ListFlag = 3; 
				//LeadingItemFlag = kFalse;
				//SPSelectionObserver selectoinObj(this);
				//if(FilterFlag ==  kTrue)
				//{
				//	if(global_project_level == 3)						
				//		selectoinObj.populateProductListforSectionWithDesignerActions(CurrentSelectedSubSection,GlobalDesignerActionFlags[0],GlobalDesignerActionFlags[1],GlobalDesignerActionFlags[2],GlobalDesignerActionFlags[3],GlobalDesignerActionFlags[4],GlobalDesignerActionFlags[5],GlobalDesignerActionFlags[6], GlobalDesignerActionFlags[7]);
				//	else if(global_project_level == 2)						
				//		selectoinObj.populateProductListforSectionWithDesignerActions(CurrentSelectedSection,GlobalDesignerActionFlags[0],GlobalDesignerActionFlags[1],GlobalDesignerActionFlags[2],GlobalDesignerActionFlags[3],GlobalDesignerActionFlags[4],GlobalDesignerActionFlags[5],GlobalDesignerActionFlags[6], GlobalDesignerActionFlags[7]);
				//	else if(global_project_level == 4)
				//		selectoinObj.populateProductListforSection(CurrentSelectedSection);  // currentClassId is saved in CurrentSelectedSection

				//}
				//else
				//{
				//	if(global_project_level == 3)
				//		selectoinObj.populateProductListforSection(CurrentSelectedSubSection);
				//	else if(global_project_level == 2)
				//		selectoinObj.populateProductListforSection(CurrentSelectedSection);
				//	else if(global_project_level == 4)
				//		selectoinObj.populateProductListforSection(CurrentSelectedSection);  // currentClassId is saved in CurrentSelectedSection
				//}
				break;
			}

		case kSPProductOnlyActionID:
			{

				ListFlag = 1;
				LeadingItemFlag = kFalse;
				SPSelectionObserver selectoinObj(this);
				//if(FilterFlag ==  kTrue)
				//{
				//	if(global_project_level == 3)						
				//		selectoinObj.populateProductListforSectionWithDesignerActions(CurrentSelectedSubSection,GlobalDesignerActionFlags[0],GlobalDesignerActionFlags[1],GlobalDesignerActionFlags[2],GlobalDesignerActionFlags[3],GlobalDesignerActionFlags[4],GlobalDesignerActionFlags[5],GlobalDesignerActionFlags[6], GlobalDesignerActionFlags[7]);
				//	else if(global_project_level == 2)						
				//		selectoinObj.populateProductListforSectionWithDesignerActions(CurrentSelectedSection,GlobalDesignerActionFlags[0],GlobalDesignerActionFlags[1],GlobalDesignerActionFlags[2],GlobalDesignerActionFlags[3],GlobalDesignerActionFlags[4],GlobalDesignerActionFlags[5],GlobalDesignerActionFlags[6], GlobalDesignerActionFlags[7]);
				//	else if(global_project_level == 4)
				//		selectoinObj.populateProductListforSection(CurrentSelectedSection);  // currentClassId is saved in CurrentSelectedSection

				//}
				//else
				{
					if(global_project_level == 3)
						selectoinObj.populateProductListforSection(CurrentSelectedSubSection);
					else if(global_project_level == 2)
						selectoinObj.populateProductListforSection(CurrentSelectedSection);
					else if(global_project_level == 4)
						selectoinObj.populateProductListforSection(CurrentSelectedSection);  // currentClassId is saved in CurrentSelectedSection
				}
				break;
			}

		case kSPItemOnlyActionID:
			{

				ListFlag = 2;
				LeadingItemFlag = kFalse;
				SPSelectionObserver selectoinObj(this);
				//if(FilterFlag ==  kTrue)
				//{
				//	//CA("FilterFlag ==  kTrue");
				//	if(global_project_level == 3)		
				//	{
				//		//CA("global_project_level == 3");
				//		selectoinObj.populateProductListforSectionWithDesignerActions(CurrentSelectedSubSection,GlobalDesignerActionFlags[0],GlobalDesignerActionFlags[1],GlobalDesignerActionFlags[2],GlobalDesignerActionFlags[3],GlobalDesignerActionFlags[4],GlobalDesignerActionFlags[5],GlobalDesignerActionFlags[6], GlobalDesignerActionFlags[7]);
				//	}
				//	else if(global_project_level == 2)						
				//	{
				//		//CA("global_project_level == 2");
				//		selectoinObj.populateProductListforSectionWithDesignerActions(CurrentSelectedSection,GlobalDesignerActionFlags[0],GlobalDesignerActionFlags[1],GlobalDesignerActionFlags[2],GlobalDesignerActionFlags[3],GlobalDesignerActionFlags[4],GlobalDesignerActionFlags[5],GlobalDesignerActionFlags[6], GlobalDesignerActionFlags[7]);
				//	}
				//	else if(global_project_level == 4)
				//	{
				//		//CA("global_project_level == 4");
				//		selectoinObj.populateProductListforSection(CurrentSelectedSection);  // currentClassId is saved in CurrentSelectedSection
				//	}

				//}
				//else
				{
					//CA("FilterFlag ==  kFalse");
					if(global_project_level == 3)
					{
						//CA("global_project_level == 3");
						selectoinObj.populateProductListforSection(CurrentSelectedSubSection);
					}
					else if(global_project_level == 2)
					{
						//CA("global_project_level == 2");
						selectoinObj.populateProductListforSection(CurrentSelectedSection);
					}
					else if(global_project_level == 4)
					{
						//CA("global_project_level == 4");
						selectoinObj.populateProductListforSection(CurrentSelectedSection);  // currentClassId is saved in CurrentSelectedSection
					}
				}

				break;
			}
		case kSPLeadingItemActionID:
			{

				ListFlag = 2;
				LeadingItemFlag = kTrue;
				SPSelectionObserver selectoinObj(this);
				//if(FilterFlag ==  kTrue)
				//{
				//	//CA("FilterFlag ==  kTrue");
				//	if(global_project_level == 3)		
				//	{
				//		//CA("global_project_level == 3");
				//		selectoinObj.populateProductListforSectionWithDesignerActions(CurrentSelectedSubSection,GlobalDesignerActionFlags[0],GlobalDesignerActionFlags[1],GlobalDesignerActionFlags[2],GlobalDesignerActionFlags[3],GlobalDesignerActionFlags[4],GlobalDesignerActionFlags[5],GlobalDesignerActionFlags[6], GlobalDesignerActionFlags[7]);
				//	}
				//	else if(global_project_level == 2)						
				//	{
				//		//CA("global_project_level == 2");
				//		selectoinObj.populateProductListforSectionWithDesignerActions(CurrentSelectedSection,GlobalDesignerActionFlags[0],GlobalDesignerActionFlags[1],GlobalDesignerActionFlags[2],GlobalDesignerActionFlags[3],GlobalDesignerActionFlags[4],GlobalDesignerActionFlags[5],GlobalDesignerActionFlags[6], GlobalDesignerActionFlags[7]);
				//	}
				//	else if(global_project_level == 4)
				//	{
				//		//CA("global_project_level == 4");
				//		selectoinObj.populateProductListforSection(CurrentSelectedSection);  // currentClassId is saved in CurrentSelectedSection
				//	}

				//}
				//else
				{
					//CA("FilterFlag ==  kFalse");
					if(global_project_level == 3)
					{
						//CA("global_project_level == 3");
						selectoinObj.populateProductListforSection(CurrentSelectedSubSection);
					}
					else if(global_project_level == 2)
					{
						selectoinObj.populateProductListforSection(CurrentSelectedSection);
					}
					else if(global_project_level == 4)
					{
						//CA("global_project_level == 4");
						selectoinObj.populateProductListforSection(CurrentSelectedSection);  // currentClassId is saved in CurrentSelectedSection
					}
				}

				break;
			}

		case kSPALLActionID:
			{
				ListFlag = 0;
				LeadingItemFlag = kFalse;
				SPSelectionObserver selectoinObj(this);

				//if(FilterFlag ==  kTrue)
				//{
				//	if(global_project_level == 3)						
				//		selectoinObj.populateProductListforSectionWithDesignerActions(CurrentSelectedSubSection,GlobalDesignerActionFlags[0],GlobalDesignerActionFlags[1],GlobalDesignerActionFlags[2],GlobalDesignerActionFlags[3],GlobalDesignerActionFlags[4],GlobalDesignerActionFlags[5],GlobalDesignerActionFlags[6], GlobalDesignerActionFlags[7]);
				//	else if(global_project_level == 2)						
				//		selectoinObj.populateProductListforSectionWithDesignerActions(CurrentSelectedSection,GlobalDesignerActionFlags[0],GlobalDesignerActionFlags[1],GlobalDesignerActionFlags[2],GlobalDesignerActionFlags[3],GlobalDesignerActionFlags[4],GlobalDesignerActionFlags[5],GlobalDesignerActionFlags[6], GlobalDesignerActionFlags[7]);
				//	else if(global_project_level == 4)
				//		selectoinObj.populateProductListforSection(CurrentSelectedSection);  // currentClassId is saved in CurrentSelectedSection

				//}
				//else
				{
					if(global_project_level == 3)
						selectoinObj.populateProductListforSection(CurrentSelectedSubSection);
					else if(global_project_level == 2)
						selectoinObj.populateProductListforSection(CurrentSelectedSection);
					else if(global_project_level == 4)
						selectoinObj.populateProductListforSection(CurrentSelectedSection);  // currentClassId is saved in CurrentSelectedSection
					break;
				}

			}
		//--------Commented by lalit----
/*		case kSPIndividualItemStencilSprayActionID:
			{
				isSprayItemPerFrameFlag = !isSprayItemPerFrameFlag;			
				break;
			}
		case kSPIndividualItemStencilHorizFlowActionID:
			{
				isItemHorizontalFlow = !isItemHorizontalFlow;	
				break;
			}
*/
/// For Search
//
//		case kSPShowSearchPanelID:
//			{
//				searchResult = kTrue;
//				InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //CS4
//				if(iApplication==nil){ 
//					CA("No iApplication");
//					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::iApplication==nil");
//					break;
//				}
//				InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()/*, UseDefaultIID()*/); 
//				if(iPanelMgr == nil){ 
//					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::iPanelMgr == nil");
//					break;
//				}
//
//				IControlView* pnlControlView = iPanelMgr->GetPanelFromWidgetID(kSPPanelWidgetID);
//				if(pnlControlView == NULL)
//				{
//					CA("pnlControlView is NULL");
//					break;
//				}
//				UID paletteUID = kInvalidUID;
//				int32 TemplateTop	=0;	
//				int32 TemplateLeft	=0;	
//				int32 TemplateRight	=0;	
//				int32 TemplateBottom =0;	
//				const ActionID MyPalleteActionID = kSPPanelWidgetActionID;
//
//				InterfacePtr<IWidgetParent> panelWidgetParent( pnlControlView, UseDefaultIID());
//				if(panelWidgetParent == NULL)
//				{
//					//CA("panelWidgetParent is NULL");
//					break;
//				}
//				InterfacePtr<IWindow> palette((IWindow*)panelWidgetParent->QueryParentFor(IWindow::kDefaultIID));
//				if(palette == NULL)
//				{
//					//CA("palette is NULL");
//					break;
//				}
//
//				PaletteRef palRef=iPanelMgr->GetPaletteRefContainingPanel(pnlControlView);
//
//				if(!palRef.IsValid())
//				{
//					CA("IsInValid ");
//				}
//
//				bool16 retval = PaletteRefUtils::IsPaletteVisible(palRef);
//		
//				if(palette)
//				{
//						
//					//CA("pallete found ");
//					palette->AddRef();
//					GSysRect PalleteBounds = palette->GetFrameBBox();
//
//					TemplateTop		= PalleteBounds.top -30;
//					TemplateLeft	= PalleteBounds.left;
//					TemplateRight	= PalleteBounds.right;
//					TemplateBottom	= PalleteBounds.bottom;
//
//					PMString dimension("Dimension::");
//					dimension.AppendNumber(TemplateTop);
//					dimension.Append("*****");
//					dimension.AppendNumber(TemplateLeft);
//					dimension.Append("*****");
//					dimension.AppendNumber(TemplateRight);
//					dimension.Append("*****");
//					dimension.AppendNumber(TemplateBottom);
//					dimension.Append("*****");
//						//CA(dimension);
//
//					InterfacePtr<ISearch> SearchPtr((ISearch*)::CreateObject(kSEASearchBoss, IID_ISEARCH));
//					if(!SearchPtr)
//					{
//						ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::Pointre to TemplateBuilderPtr not found");
//						return ;
//					}
//					SearchPtr->OpenSearchPalette(TemplateTop, TemplateLeft, TemplateBottom, TemplateRight);
//					palette->Release();
//				}
//
//				Mediator md;
//				IPanelControlData* iPanelControlData = md.getMainPanelCtrlData();
//				IControlView* iPubGPCtrlView = iPanelControlData->FindWidget(kSPPublicationGPWidgetID);
//				IControlView* iONEGPCtrlView = iPanelControlData->FindWidget(kSPONEGPWidgetID);
//				IControlView* iSSPGPCtrlView = iPanelControlData->FindWidget(kSPSearchPnlWidgetID);
//
//				if(searchResult)
//				{
//					iSSPGPCtrlView->ShowView();
//					iPubGPCtrlView->HideView();
//					iONEGPCtrlView->HideView();
//				}
//				else
//				{
//					iSSPGPCtrlView->HideView();
//					iPubGPCtrlView->ShowView();
//					iONEGPCtrlView->ShowView();
//				}
//				break;
//			}

		case kSPThumbViewActionID:
			{
				isThumb = kTrue;
				InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
				if(ptrIAppFramework == nil)
					return ;
				
				bool16 result=ptrIAppFramework->getLoginStatus();
				if(!result)
					return;	
			
				//InterfacePtr<ISPPRImageHelper> ptrImageHelper((static_cast<ISPPRImageHelper*> (CreateObject(kSPProductImageIFaceBoss ,ISPPRImageHelper::kDefaultIID))));
			 //   if(ptrImageHelper == nil)
			 //   {
				//	//CA("ProductImages plugin not found ");
				//	return;
			 //   }
  
				//ptrImageHelper->EmptyImageGrid();
				//imageVector2 .clear(); // Chetan --


				InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
				if(app == NULL) 
				 { 
					//CA("No Application");
					break;
				 }
				 
				//InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
				//if(paletteMgr == NULL) 
				//{ 	//CA("No IPaletteMgr");	
				//	break;
				//}
				
				InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
				if(panelMgr == NULL) 
				{	//	CA("No IPanelMgr");	
					break;
				}
				
				IControlView* myPanel = panelMgr->GetVisiblePanel(kSPPanelWidgetID);
				if(!myPanel) 
				{	//CA("No PnlControlView");
					break;
				}
				
				InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
				if(!panelControlData) 
				{
					//CA("No PanelControlData");
					break;
				}
				
				IControlView* lstgridWidget = panelControlData->FindWidget( kSPListBoxWrapperGroupPanelWidgetID);
				if(lstgridWidget == NULL)
				{
					//CA("No DCNListControlView");
					break;
				}
	
				lstgridWidget->HideView();//tO hide the ListBox And So It will Show ImagePanel
				
				//IControlView* lstWidget = panelControlData->FindWidget( kSPListBoxWidgetID);
				//if(lstWidget == NULL)
				//{
				//	CA("No DCNListControlView");
				//	break;
				//}
	
				//lstWidget->HideView();//tO hide the ListBox And So It will Show ImagePanel

				//IControlView* Widget = panelControlData->FindWidget( kSPThumbPnlWidgetID);
				//if(Widget == NULL)
				//{
				//	//CA("No pnlControlView");
				//	break;
				//}
				//Widget->ShowView();
				//Widget->Enable();

				SPSelectionObserver selObserver(this);

				/*if(FilterFlag ==  kTrue)
				{					
					selObserver.populateProductListforSectionWithDesignerActions(CurrentSelectedSubSection,GlobalDesignerActionFlags[0],GlobalDesignerActionFlags[1],GlobalDesignerActionFlags[2],GlobalDesignerActionFlags[3],GlobalDesignerActionFlags[4],GlobalDesignerActionFlags[5],GlobalDesignerActionFlags[6], GlobalDesignerActionFlags[7]);
				}
				else*/
				{
					/*if(searchResult)
						selObserver.populateProductListforSectionforSearch(global_vectorObjectValuePtr , global_vectorItemModelPtr);
					else*/
						selObserver.populateProductListforSection(CurrentSelectedSubSection);
				}
				//----------	
//				IControlView *checkBoxGroupPanelControlView=panelControlData->FindWidget(kSPCheckBoxGroupPanelWidgetID);
//				if(checkBoxGroupPanelControlView == nil)
//				{
//					//CA("checkBoxGroupPanelControlView == nil");
//					return;
//				}
//				checkBoxGroupPanelControlView->HideView();

				IControlView *newCheckBoxGroupPanelControlView=panelControlData->FindWidget(kSPNewCheckBoxGroupPanelWidgetID );
				if(newCheckBoxGroupPanelControlView == nil)
				{
					//CA("newCheckBoxGroupPanelControlView == nil");
					return;
				}
				newCheckBoxGroupPanelControlView->ShowView();

				/*IControlView *newHorizontalFlowCheckBoxControlView=panelControlData->FindWidget(kNewHorizontalFlowCheckBoxWidgetID);
				if(newHorizontalFlowCheckBoxControlView == nil)
				{
					//CA("newHorizontalFlowCheckBoxControlView == nil");
					return;
				}
				InterfacePtr<ITriStateControlData> newHorizontalFlowCtrlData(newHorizontalFlowCheckBoxControlView,UseDefaultIID());
				if(newHorizontalFlowCtrlData == nil)
				{
					//CA("newHorizontalFlowCtrlData == nil");
					return;
				}
				if(isItemHorizontalFlow == kTrue)
					newHorizontalFlowCtrlData->Select();
				else
					newHorizontalFlowCtrlData->Deselect();

				IControlView *newItemPerFrameCheckBoxControlView=panelControlData->FindWidget(kNewSprayItemPerFrameCheckBoxWidgetID);
				if(newItemPerFrameCheckBoxControlView == nil)
				{
					//CA("newItemPerFrameCheckBoxControlView == nil");
					return;
				}
				InterfacePtr<ITriStateControlData> newItemPerFrameCtrlData(newItemPerFrameCheckBoxControlView,UseDefaultIID());
				if(newItemPerFrameCtrlData == nil)
				{
					//CA("newItemPerFrameCtrlData == nil");
					return;
				}
				if(isSprayItemPerFrameFlag == kTrue)
					newItemPerFrameCtrlData->Select();
				else
				{
					newItemPerFrameCtrlData->Deselect();
					newHorizontalFlowCheckBoxControlView->Disable();
				}
				*/
				
				
				break;
			}
		case kSPListViewActionID:
			{				
				isThumb = kFalse;

				//InterfacePtr<ISPPRImageHelper> ptrImageHelper((static_cast<ISPPRImageHelper*> (CreateObject(kSPProductImageIFaceBoss ,ISPPRImageHelper::kDefaultIID))));
			 //   if(ptrImageHelper == nil)
			 //   {
				//	//CA("ProductImages plugin not found ");
				//	return;
			 //   }
  
				//ptrImageHelper->EmptyImageGrid();
				//imageVector2 .clear(); // Chetan --

				InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
				if(app == NULL) 
				 { 
					//CA("No Application");
					break;
				 }
				 
				//InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
				//if(paletteMgr == NULL) 
				//{ 	//CA("No IPaletteMgr");	
				//	break;
				//}
				
				InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
				if(panelMgr == NULL) 
				{	//	CA("No IPanelMgr");	
					break;
				}
				
				IControlView* myPanel = panelMgr->GetVisiblePanel(kSPPanelWidgetID);
				if(!myPanel) 
				{	//CA("No PnlControlView");
					break;
				}
				
				InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
				if(!panelControlData) 
				{
					//CA("No PanelControlData");
					break;
				}
				//IControlView* Widget = panelControlData->FindWidget( kSPThumbPnlWidgetID);
				//if(Widget == NULL)
				//{
				//	//CA("No pnlControlView");
				//	break;
				//}
				//Widget->HideView();				
				IControlView* lstgridWidget = panelControlData->FindWidget( kSPListBoxWrapperGroupPanelWidgetID);
				if(lstgridWidget == NULL)
				{
					//CA("No DCNListControlView");
					break;
				}
	
				lstgridWidget->ShowView();//tO hide the ListBox And So It will Show ImagePanel
				//IControlView* lstWidget = panelControlData->FindWidget( kSPListBoxWidgetID);
				//if(lstWidget == NULL)
				//{
				//	//CA("No DCNListControlView");
				//	break;
				//}
	
				//lstWidget->ShowView();//tO hide the ListBox And So It will Show ImagePanel
				SPSelectionObserver selObserver(this);
				/*if(FilterFlag ==  kTrue)
				{					
					selObserver.populateProductListforSectionWithDesignerActions(CurrentSelectedSubSection,GlobalDesignerActionFlags[0],GlobalDesignerActionFlags[1],GlobalDesignerActionFlags[2],GlobalDesignerActionFlags[3],GlobalDesignerActionFlags[4],GlobalDesignerActionFlags[5],GlobalDesignerActionFlags[6], GlobalDesignerActionFlags[7]);
				}
				else*/
				{
					/*if(searchResult)
						selObserver.populateProductListforSectionforSearch(global_vectorObjectValuePtr , global_vectorItemModelPtr);
					else*/
						selObserver.populateProductListforSection(CurrentSelectedSubSection);
				}
				/*selObserver.populateProductListforSection(CurrentSelectedSubSection);  Chetan -- 22/8/07  */
			
				//--------------
				IControlView *newCheckBoxGroupPanelControlView=panelControlData->FindWidget(kSPNewCheckBoxGroupPanelWidgetID);
				if(newCheckBoxGroupPanelControlView == nil)
				{
					//CA("newCheckBoxGroupPanelControlView == nil");
					return;
				}
				newCheckBoxGroupPanelControlView->HideView();	

//				IControlView *checkBoxGroupPanelControlView=panelControlData->FindWidget(kSPCheckBoxGroupPanelWidgetID);
//				if(checkBoxGroupPanelControlView == nil)
//				{
//					//CA("checkBoxGroupPanelControlView == nil");
//					return;
//				}
//				checkBoxGroupPanelControlView->ShowView();
								
				//IControlView *itemPerFrameCheckBoxControlView=panelControlData->FindWidget(kSprayItemPerFrameCheckBoxWidgetID);
				//if(itemPerFrameCheckBoxControlView == nil)
				//{
				//	//CA("itemPerFrameCheckBoxControlView == nil");
				//	return;
				//}
				//InterfacePtr<ITriStateControlData> itemPerFrameCtrlData(itemPerFrameCheckBoxControlView,UseDefaultIID());
				//if(itemPerFrameCtrlData == nil)
				//{
				//	//CA("itemPerFrameCtrlData == nil");
				//	return;
				//}
				//if(isSprayItemPerFrameFlag == kTrue)
				//	itemPerFrameCtrlData->Select();
				//else
				//	itemPerFrameCtrlData->Deselect();

				//IControlView *horizontalFlowCheckBoxControlView=panelControlData->FindWidget(kHorizontalFlowCheckBoxWidgetID);
				//if(horizontalFlowCheckBoxControlView == nil)
				//{
				//	//CA("horizontalFlowCheckBoxControlView == nil");
				//	return;
				//}
				//InterfacePtr<ITriStateControlData> horizontalFlowCtrlData(horizontalFlowCheckBoxControlView,UseDefaultIID());
				//if(horizontalFlowCtrlData == nil)
				//{
				//	//CA("horizontalFlowCtrlData == nil");
				//	return;
				//}
				//if(isItemHorizontalFlow == kTrue)
				//	horizontalFlowCtrlData->Select();
				//else
				//	horizontalFlowCtrlData->Deselect();
				

				break; 	
			}



		case kSPTemplateInfoActionID:
			{
			//Apsiva 9 Comment
			//	VectorPubObjectValuePtr VectorFamilyInfoValuePtr = NULL;			
			//	VectorFamilyInfoValuePtr = ptrIAppFramework->getProductsAndItemsForSection(CurrentSelectedSubSection, global_lang_id);
			//	if(VectorFamilyInfoValuePtr == nil)
			//	{
			//		ptrIAppFramework->LogError("VectorFamilyInfoValuePtr == nil");
			//		break;
			//	}

			//	if(VectorFamilyInfoValuePtr->size() < 0){
			//		VectorFamilyInfoValuePtr->clear();
			//		delete VectorFamilyInfoValuePtr;
			//		break;
			//	}


			////get 'Item_Template_Name' Attribute Id from Config
			//	int32 ItemTemplateNameAttributeId = -1;

			//	/*PMStringVecPtr pPMStringVec = ptrIAppFramework->CONFIGCACHE_getByConfigName("ADLC_TEMPLATE_NAME_ATTRIBUTE");
			//	if(pPMStringVec == NULL || pPMStringVec->size() <= 0)
			//	{
			//		ptrIAppFramework->LogError("CONFIGCACHE_getByConfigName for config 'ADLC_TEMPLATE_NAME_ATTRIBUTE' returns NULL");
			//		break;
			//	}*/

			//	
			//	//ItemTemplateNameAttributeId = pPMStringVec->at(0).GetAsNumber();
			//	ItemTemplateNameAttributeId = ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("ADLC_TEMPLATE_NAME_ATTRIBUTE");

			//	/*PMString attrIdStr("ItemTemplateNameAttributeId = ");
			//	attrIdStr.AppendNumber(ItemTemplateNameAttributeId);
			//	CA(attrIdStr);*/

			//	if(ItemTemplateNameAttributeId <= 0)
			//	{
			//		ptrIAppFramework->LogError("fillSprayTypeInfoListForADLC::ItemTemplateNameAttributeId not found");
			//		break;
			//	}
			//	//delete pPMStringVec;
			//	//pPMStringVec = NULL;

			////get Left_Page_Template Attribute Id from Config
			//	int32 LeftPageTemplateNameAttributeId = -1;
			//	/*pPMStringVec = ptrIAppFramework->CONFIGCACHE_getByConfigName("ADLC_LEFT_PAGE_TEMPLATE_NAME_ATTRIBUTE");
			//	if(pPMStringVec == NULL || pPMStringVec->size() <= 0)
			//	{
			//		ptrIAppFramework->LogError("CONFIGCACHE_getByConfigName for config 'ADLC_LEFT_PAGE_TEMPLATE_NAME_ATTRIBUTE' returns NULL");
			//		break;
			//	}*/
			//	
			//	//LeftPageTemplateNameAttributeId = pPMStringVec->at(0).GetAsNumber();
			//	LeftPageTemplateNameAttributeId = ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("ADLC_LEFT_PAGE_TEMPLATE_NAME_ATTRIBUTE");

			//	if(LeftPageTemplateNameAttributeId <= 0)
			//	{
			//		ptrIAppFramework->LogError("fillSprayTypeInfoListForADLC::LeftPageTemplateNameAttributeId not found");
			//		//delete pPMStringVec;
			//		break;
			//	}
			//	
			//	//delete pPMStringVec;
			//	//pPMStringVec = NULL;

			////get Right_Page_Template Attribute Id from Config
			//	int32 RightPageTemplateNameAttributeId = -1;
			//	/*pPMStringVec = ptrIAppFramework->CONFIGCACHE_getByConfigName("ADLC_RIGHT_PAGE_TEMPLATE_NAME_ATTRIBUTE");
			//	if(pPMStringVec == NULL || pPMStringVec->size() <= 0)
			//	{
			//		ptrIAppFramework->LogError("CONFIGCACHE_getByConfigName  for config 'ADLC_RIGHT_PAGE_TEMPLATE_NAME_ATTRIBUTE' returns NULL");
			//		break;
			//	}
			//	
			//	RightPageTemplateNameAttributeId = pPMStringVec->at(0).GetAsNumber();*/
			//	RightPageTemplateNameAttributeId =  ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("ADLC_RIGHT_PAGE_TEMPLATE_NAME_ATTRIBUTE");

			//	if(RightPageTemplateNameAttributeId <= 0)
			//	{
			//		ptrIAppFramework->LogError("fillSprayTypeInfoListForADLC::RightPageTemplateNameAttributeId not found");
			//		//delete pPMStringVec;
			//		break;
			//	}
			//	
			//	//delete pPMStringVec;
			//	//pPMStringVec = NULL;

			////get Section_Template Attribute Id from Config
			//	int32 SectionTemplateNameAttributeId = -1;
			//	/*pPMStringVec = ptrIAppFramework->CONFIGCACHE_getByConfigName("ADLC_SECTION_TEMPLATE_NAME_ATTRIBUTE");
			//	if(pPMStringVec == NULL || pPMStringVec->size() <= 0)
			//	{
			//		ptrIAppFramework->LogError("CONFIGCACHE_getByConfigName for config 'ADLC_SECTION_TEMPLATE_NAME_ATTRIBUTE' returns NULL");
			//		break;
			//	}
			//	
			//	SectionTemplateNameAttributeId = pPMStringVec->at(0).GetAsNumber();*/
			//	SectionTemplateNameAttributeId =  ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("ADLC_SECTION_TEMPLATE_NAME_ATTRIBUTE");

			//	if(SectionTemplateNameAttributeId <= 0)
			//	{
			//		ptrIAppFramework->LogError("fillSprayTypeInfoListForADLC::SectionTemplateNameAttributeId not found");
			//		//delete pPMStringVec;
			//		break;
			//	}
			//	
			//	//delete pPMStringVec;
			//	//pPMStringVec = NULL;


			//	GetSectionData getSectionData;		
			//	
			//	getSectionData.addCopyFlag = kTrue;		
			//
			//	getSectionData.addSectionLevelCopyAttrFlag = kTrue;
			//	getSectionData.addPublicationLevelCopyAttrFlag = kTrue;
			//	getSectionData.addCatagoryLevelCopyAttrFlag = kTrue;				
			//
			//	getSectionData.isOneSource = kFalse;

			//	getSectionData.SectionId = CurrentSelectedSubSection;

			//	CPubModel objPubModel = ptrIAppFramework->getpubModelByPubID(CurrentSelectedSubSection , global_lang_id);
			//	getSectionData.PublicationId = objPubModel.getRootID();
			//	
			//	getSectionData.languageId = global_lang_id;
			//	getSectionData.CatagoryId = -1;

			//	getSectionData.itemIdList.push_back(-1);
			//	getSectionData.productIdList.push_back(-1);
			//	getSectionData.hybridIdList.push_back(-1);

			//	getSectionData.isGetWholePublicationOrCatagoryDataFlag = kTrue;
			//	
			//	ptrIAppFramework->GetSectionData_getDataForPubOrCat(getSectionData);


			//	PMString templateInfo("");
			//	
			//	PMString sectionTemplate = ptrIAppFramework->getPubSecSubsecCopyAttributeValue(CurrentSelectedSubSection, SectionTemplateNameAttributeId,global_lang_id);
			//	PMString LeftPageTemplate = ptrIAppFramework->getPubSecSubsecCopyAttributeValue(CurrentSelectedSubSection, LeftPageTemplateNameAttributeId,global_lang_id);
			//	PMString RightPageTemplate = ptrIAppFramework->getPubSecSubsecCopyAttributeValue(CurrentSelectedSubSection, RightPageTemplateNameAttributeId,global_lang_id);
			//	
			//	templateInfo.Append("Section Template : ");
			//	templateInfo.Append(sectionTemplate);
			//	templateInfo.Append("\n");
			//	templateInfo.Append("Left Page Template : ");
			//	templateInfo.Append(LeftPageTemplate);
			//	templateInfo.Append("\n");
			//	templateInfo.Append("Right Page Template : ");
			//	templateInfo.Append(RightPageTemplate);
			//	templateInfo.Append("\n");
			//	templateInfo.Append("\n");

			//	

			//	VectorPubObjectValue::iterator it;
			//	for(it = VectorFamilyInfoValuePtr->begin(); it!=VectorFamilyInfoValuePtr->end(); it++)
			//	{
			//		PMString templateName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(it->getObject_id(), ItemTemplateNameAttributeId, global_lang_id , kFalse);
			//		//templateName.Append(".indt");
			//		templateInfo.Append(it->getItemModel().getItemNo());
			//		templateInfo.Append(" : ");
			//		templateInfo.Append(templateName);
			//		templateInfo.Append("\n");
			//	}

			//	CA(templateInfo);   // Dont't Comment This Line

			//	ptrIAppFramework->GetSectionData_clearSectionDataCache();
			//	ptrIAppFramework->clearAllStaticObjects();
				break;
			}					
		default:
		{
			break;
		}
	}
}

/* DoAbout
*/
void SPActionComponent::DoAbout()
{
	CAlert::ModalAlert
	(
		kSPAboutBoxStringKey,				// Alert string
		kOKString, 						// OK button
		kNullString, 						// No second button
		kNullString, 						// No third button
		1,							// Set OK button to default
		CAlert::eInformationIcon				// Information icon.
	);
}
/* DoMenuItem1
*/
void SPActionComponent::DoMenuItem1(IActiveContext* ac)
{
	//CA("ActionComponent 11");
	//CAlert::InformationAlert(kSPMenuItem1StringKey);
	//CA("ActionComponent 12");
}

/* DoDialog
*/
void SPActionComponent::DoDialog()
{
	do
	{
	

	// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		//ASSERT(application);
		if (application == nil) {	
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		//ASSERT(dialogMgr);
		if (dialogMgr == nil) {
			break;
		}






		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kSPPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			kSDKDefDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);//::kModeless );//
		//ASSERT(dialog);
		if (dialog == nil) {
			break;
		}

		// Open the dialog.
		dialog->Open(); 
		//CA(" -- ActionComponent  opendialog done -- ");
		//CA("ActionComponent 17");
		DlgPtr = dialog;
	
	} while (false);			
}

/////////////////////////////////////////
void SPActionComponent::CloseDialog()
{
	//CA("Inside CloseDialog()");
	//CA("ActionComponent 18");
	if(DlgPtr){
		if(DlgPtr->IsOpen())
		{
			//CA("ActionComponent 19");
			DlgPtr->Close();
			//CA("ActionComponent 20");
		}
	}
}

void SPActionComponent::UpdateActionStates(IActiveContext* ac, IActionStateList* listToUpdate, GSysPoint mousePoint, IPMUnknown* widget)
{
	//CA("ActionComponent 21");
	if (ac == nil)
	{
		ASSERT(ac);
		return;
	}

	//CA("ActionComponent 22");

		for(int32 iter = 0; iter < listToUpdate->Length(); iter++) 
		{
			//CA("ActionComponent 23");
			ActionID actionID = listToUpdate->GetNthAction(iter);
			//if (actionID.Get() == kSPPanelWidgetActionID) 
			//{	
			//	//CA("ActionComponent 24");

			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
				return;
			//CA("ActionComponent 25");
			bool16 result=ptrIAppFramework->getLoginStatus();
				
			//	if(result)
			//	{	
			//		//CA("ActionComponent 26");
			//		listToUpdate->SetNthActionState(iter, kEnabledAction);
			//	}
			//	else
			//	{		
			//		//CA("ActionComponent 27");
			//		listToUpdate->SetNthActionState(iter,kDisabled_Unselected);
			//	}
			//}
//
			if(actionID ==  kSPThumbViewActionID)
			{ 	
				if(isThumb)
                    listToUpdate->SetNthActionState(iter,kEnabledAction | kSelectedAction);
				else
					listToUpdate->SetNthActionState(iter,kEnabledAction);
			}	
			
			if(actionID ==  kSPListViewActionID )
			{ 	
				if(isThumb)
					listToUpdate->SetNthActionState(iter,kEnabledAction);
				else
					listToUpdate->SetNthActionState(iter,kEnabledAction | kSelectedAction);
			}	
//
			switch(actionID.Get())
			{
				case kSPPanelPSMenuActionID:
				case kSPPanelWidgetActionID:
				{
					//PMString string("Setting ActionState", -1, PMString::kNoTranslate);
					//CAlert::InformationAlert(string);
					if(result)
					{
						InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
						ASSERT(app);
						if(!app)
						{
							ptrIAppFramework->LogDebug("AP7_ProductFinder::SPActionComponent::UpdateActionStates::kSPPanelWidgetActionID--No app");												
							return;
						}
						//Commented By Sachin sharma 0n 29/06/07
						/*InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
						ASSERT(paletteMgr);
						if(!paletteMgr)
						{
							return;
						}
						InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());*/
						InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
						
						if(!panelMgr)
						{
							ASSERT(panelMgr);
							return;
						}
						if(panelMgr->IsPanelWithMenuIDMostlyVisible(kSPPanelWidgetID)) //if(panelMgr->IsPanelWithWidgetIDVisible(kSPPanelWidgetID))//Commented By SAchin Sharma on 3/07/07
						{
							listToUpdate->SetNthActionState(iter,kEnabledAction | kSelectedAction);
						}
						else
						{
							listToUpdate->SetNthActionState(iter,kEnabledAction);
						}

					}
					else
					{
						listToUpdate->SetNthActionState(iter,kDisabled_Unselected);
					}
					break;
				}
				case kSPHybridTableOnlyActionID:
					{
						if(result)
						{
							if(ListFlag == 3)
							{
								listToUpdate->SetNthActionState(iter,kEnabledAction | kSelectedAction);
							}
							else
							{
								listToUpdate->SetNthActionState(iter,kEnabledAction);
							}
						}
						else
						{
							listToUpdate->SetNthActionState(iter,kDisabled_Unselected);
						}
						break;
					}
				case kSPProductOnlyActionID:
					{
						if(result)
						{
							if(ListFlag == 1)
							{
								listToUpdate->SetNthActionState(iter,kEnabledAction | kSelectedAction);
							}
							else
							{
								listToUpdate->SetNthActionState(iter,kEnabledAction);
							}
						}
						else
						{
							listToUpdate->SetNthActionState(iter,kDisabled_Unselected);
						}
						break;
					}
				case kSPItemOnlyActionID:
					{
						if(result)
						{
							if(ListFlag == 2 && LeadingItemFlag == kFalse)
							{
								listToUpdate->SetNthActionState(iter,kEnabledAction | kSelectedAction);
							}
							else
							{
								listToUpdate->SetNthActionState(iter,kEnabledAction);
							}
						}
						else
						{
							listToUpdate->SetNthActionState(iter,kDisabled_Unselected);
						}
						break;
					}
				case kSPLeadingItemActionID:
				{
					if(result)
					{
						if(ListFlag == 2 && LeadingItemFlag == kTrue)
						{
							listToUpdate->SetNthActionState(iter,kEnabledAction | kSelectedAction);
						}
						else
						{
							listToUpdate->SetNthActionState(iter,kEnabledAction);
						}
					}
					else
					{
						listToUpdate->SetNthActionState(iter,kDisabled_Unselected);
					}
					break;
				}
				case kSPALLActionID:
					{
						if(result)
						{
							if(ListFlag == 0)
							{
								listToUpdate->SetNthActionState(iter,kEnabledAction | kSelectedAction);
							}							
							else
							{
								listToUpdate->SetNthActionState(iter,kEnabledAction);
							}
						}
						else
						{
							listToUpdate->SetNthActionState(iter,kDisabled_Unselected);
						}
						break;
					}
				//--------Commented by lalit----
/*				case kSPIndividualItemStencilSprayActionID:
					{
						if(result)
						{
							if(isSprayItemPerFrameFlag)
							{
								listToUpdate->SetNthActionState(iter,kEnabledAction | kSelectedAction);
							}							
							else
							{
								listToUpdate->SetNthActionState(iter,kEnabledAction);
							}
						}
						else
						{
							listToUpdate->SetNthActionState(iter,kDisabled_Unselected);
						}
						break;
					}

				case kSPIndividualItemStencilHorizFlowActionID:
					{
						if(result)
						{
							if(isItemHorizontalFlow)
							{
								listToUpdate->SetNthActionState(iter,kEnabledAction | kSelectedAction);
							}							
							else
							{
								listToUpdate->SetNthActionState(iter,kEnabledAction);
							}
						}
						else
						{
							listToUpdate->SetNthActionState(iter,kDisabled_Unselected);
						}
						break;
					}
*/
/// For Search
			//	case kSPShowSearchPanelID:
			//	{	
			//		if(result )
			//		{
			//			listToUpdate->SetNthActionState(iter,kEnabledAction);
			//		}
			//		else
			//		{
			//			listToUpdate->SetNthActionState(iter,kDisabled_Unselected);
			//		}
			//		break;
			//	}
/// For Search Ends
				case kSPThumbViewActionID:
					{
						if(result)
						{
							if(isThumb)
							{
								listToUpdate->SetNthActionState(iter,kEnabledAction | kSelectedAction);
							}							
							else
							{
								listToUpdate->SetNthActionState(iter,kEnabledAction);
							}
						}
						else
						{
							listToUpdate->SetNthActionState(iter,kDisabled_Unselected);
						}
						break;
					}
				case kSPListViewActionID:
					{
						if(result)
						{
							if(isThumb)
							{
								listToUpdate->SetNthActionState(iter,kEnabledAction);				
							}							
							else
							{
								listToUpdate->SetNthActionState(iter,kEnabledAction | kSelectedAction);
							}
						}
						else
						{
							listToUpdate->SetNthActionState(iter,kDisabled_Unselected);
						}
						break;
					}

				case kSPTemplateInfoActionID:
					{
						// Apsiva 9 Comment
						//if(result && ptrIAppFramework->getClientID() == 221)
						//{
						//	//if(ListFlag == 0)
						//	//{
						//	//	listToUpdate->SetNthActionState(iter,kEnabledAction | kSelectedAction);
						//	//}							
						//	//else
						//	{
						//		listToUpdate->SetNthActionState(iter,kEnabledAction);
						//	}
						//}
						//else
						{
							listToUpdate->SetNthActionState(iter,kDisabled_Unselected);
						}
						break;
					}
				default:
				{
					break;
				}
			}
		}
}


void SPActionComponent::DoPalette()
{
	//CA("SPActionComponent::DoPalette");
	do
	{	
		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		//CA("ActionComponent 29");
		if(iApplication==nil)
		{	
			//CA("iApplication==nil");
			break;
		}

		//CA("ActionComponent 30");
		//Commented By Sachin Sharma on 29/06/07
		////InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPaletteManager());
		////if(iPaletteMgr==nil)
		////{			
		////	break;
		////}

		//////CA("ActionComponent 31");
		////InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr, UseDefaultIID()); 
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
		if(iPanelMgr == nil)
		{		
			//CA("iPanelMgr == nil");
			break;
		}

		PMLocaleId nLocale=LocaleSetting::GetLocale();
		iPanelMgr->ShowPanelByMenuID(kSPPanelWidgetActionID);

		
		//SPActionComponent::palettePanelPtr=(iPanelMgr);//Commented By Sachin sharma on 3/07/07
		
	//	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	//	if (panelControlData == nil)
	//	{
	//		CA("panelControlData invalid");
	//		break;
	//	}
	//	
	//	// for Image List Box
	//	InterfacePtr<IControlView> imageListControl(panelControlData->FindWidget(kImageGroupPanelWidgetID), UseDefaultIID());

	//	imageListControl->HideView();

		IControlView* icontrol = iPanelMgr->GetVisiblePanel(kSPPanelWidgetID);		
		if (icontrol == nil)
		{
			//CA("icontrol invalid");
			break;
		}		
		//icontrol->Resize(PMPoint(PMReal(207),PMReal(291)));//15-10-08

	//// Bottom Group Panel
	//	InterfacePtr<IControlView> listBoxControl(panelControlData->FindWidget(kGroupPanelWidgetID), UseDefaultIID());
	//	if (listBoxControl == nil)
	//	{
	//		ASSERT_FAIL("listBoxControl invalid");
	//		break;
	//	}

	//	// main Group Panel
	//	InterfacePtr<IControlView> mainGroupPanelWidget(panelControlData->FindWidget(kMainGroupPanelWidgetID), UseDefaultIID());
	//	if (mainGroupPanelWidget == nil)
	//	{
	//		ASSERT_FAIL("mainGroupPanelWidget invalid");
	//		break;
	//	}

	//	// Middle Group Panel
	//	InterfacePtr<IControlView> middleGroupPanelWidget(panelControlData->FindWidget(kMiddleGroupPanelWidgetID), UseDefaultIID());
	//	if (middleGroupPanelWidget == nil)
	//	{
	//		ASSERT_FAIL("middleGroupPanelWidget invalid");
	//		break;
	//	}

	//	listBoxControl->MoveTo(PMPoint(PMReal(0),PMReal(112)));

	//	mainGroupPanelWidget->MoveTo(PMPoint(PMReal(0),PMReal(2)));

	//	middleGroupPanelWidget->MoveTo(PMPoint(PMReal(0),PMReal(84)));

	}while(kFalse);
}

void SPActionComponent::ClosePSPPalette()
{	
	do
	{	

		//CA("closing psppalette..ActionComponent 33");
		/********************* Disable all Ctrlviews ************************/
		
		Mediator md;
		IPanelControlData* panelCntrlData=md.getMainPanelCtrlData();
		if(panelCntrlData == nil)
		{
			//CA("panelCntrlData is nil");
			break;
		}

		//CA("ActionComponent 34");
		//================================================Added
		InterfacePtr<IApplication>iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if(!iApplication)
		{
			ASSERT(iApplication);
			return;
		}
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
		if(iPanelMgr == nil)
		{
			//CA("iPanelMgr == nil");
			break;
		}
		//InterfacePtr<IControlView> panelView((const IPMUnknown*)::GetDataBase(iPanelMgr), UseDefaultIID());//Added
		//if(!panelView)
		//{
		//	ASSERT(panelView);
		//	break;
		//}
		IControlView* panelView = iPanelMgr->GetPanelFromActionID(kSPPanelWidgetActionID);
		if(panelView == NULL)
		{
			CA("panelView is NULL");
			return;
		}
		
		////----------------New CS3 Changes--------------------//
			PaletteRef palRef=iPanelMgr->GetPaletteRefContainingPanel(panelView);
		////----------------New CS3 Changes--------------------//
			bool16 retval = PaletteRefUtils::IsPaletteVisible(palRef);
		//================================================UpTo Here
		
		
		if(retval)								//if(SPActionComponent::palettePanelPtr)//Commented by Sachin sharma on 3/07/07
		{ 

			//CA("ActionComponent 35");
			//Commented By Sachin sharma  //CS2 Code
			//InterfacePtr<IPanelMgr> iPanelMgr((const IPMUnknown *)SPActionComponent::palettePanelPtr, UseDefaultIID()); //Commented By Sachin Sharma on 3/07/07
			//if(iPanelMgr == nil)
			//break;
//==========++++++++++++++++++++++++++++++ADded==
			/*InterfacePtr<IApplication>iApplication(gSession->QueryAppllication());
			if(!iApplication)
			{
				return;
			}
			InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager());
			if(!iPanelMgr)
			{
				ASSERT(iPanelMgr);
				return;
			}*/
//================++++++++++++++++++++++++++++Upto Here==
			if(iPanelMgr->IsPanelWithMenuIDMostlyVisible(kSPPanelWidgetActionID))						//if(iPanelMgr->IsPanelWithMenuIDVisible(kSPPanelWidgetActionID))//Commented By Sachin sharma on 3/07/07 
			{
				iPanelMgr->HidePanelByMenuID(kSPPanelWidgetActionID);
			}
		}

		//md.setMainPanelCntrlData1(md.getMainPanelCtrlData());
		//md.setMainPanelCntrlData(nil);
		
	}while(kFalse);
	
}


