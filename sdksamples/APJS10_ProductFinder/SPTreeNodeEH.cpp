//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/SPTreeNodeEH.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rahul $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: 1.2 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPluginHeaders.h"

// Interfaces
#include "ITreeNodeIDData.h"
#include "ILayoutUtils.h"
#include "IDocument.h"

// General includes:
#include "CEventHandler.h"
#include "CAlert.h"

// Project includes:
#include "SPID.h"
#include "MediatorClass.h"
#include "ISelectionManager.h"
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
#include "ITextEditSuite.h"
//#include "ITextFrame.h"         //Removed from CS3 API
#include "ITextModel.h"
#include "ITableUtils.h"
#include "ITreeViewController.h"
#include "IntNodeID.h"
#include "PublicationNode.h"
#include "SPTreeDataCache.h"
#include "ILayoutSelectionSuite.h"
//#include "ITableModelList.h"
#include "ILayoutUIUtils.h"
#include "IXMLUtils.h"
#include "ITextStoryThreadDict.h"
#include "ITblBscSuite.h"
#include <IFrameUtils.h>
#include "CAlert.h"
//#include "IMessageServer.h"
//---------CS3 Addition-----------------//
#include "IHierarchy.h"
#include "ITextFrameColumn.h"
#include "SDKLayoutHelper.h"
#include "IGraphicFrameData.h"
#include "ITextTarget.h"
#include "ITextFocus.h"
#include "ITriStateControlData.h"

//#include "IDialogController.h"
//#include "ILoginHelper.h"
#include "MediatorClass.h"
#include "TSTableSourceHelper.h"
#include "AcquireModalCursor.h"
#include "IDataSprayer.h"
#include "ISubSectionSprayer.h"
//#include "GlobalData.h"
#include "SubSectionSprayer.h"
#include "IAppFramework.h"
#include "K2Vector.tpp"
#include "PRImageHelper.h"
#include "CTGID.h"
#include "ICTGCatsyTaggerHelper.h"


#define FILENAME			PMString("SPTreeNodeEH.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAlert::InformationAlert(X)//CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
extern int32 CurrentSelectedProductRow;
extern SPTreeDataCache dc;
extern double global_lang_id ;
extern int32 checkTreeType;
extern PublicationNodeList orignalpNodeDataList;
extern int32 firstpNodeListCount;
extern bool16 singleSelectionSpray;
extern bool16 isCancelButtonClick;
extern PublicationNodeList pNodeDataList; 
extern double CurrentSelectedSection;

/** 
	Implements IEventHandler; allows this plug-in's code 
	to catch the double-click events without needing 
	access to the implementation headers.

	@author Ian Paterson
	@ingroup paneltreeview
*/

class SPTreeNodeEH : public CEventHandler
{
public:

	/** Constructor.
		@param boss interface ptr on the boss object to which the interface implemented here belongs.
	*/	
	SPTreeNodeEH(IPMUnknown* boss);
	
	/** Destructor
	*/	
	virtual ~SPTreeNodeEH(){}

	/**  Window has been activated. Traditional response is to
		activate the controls in the window.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Activate(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->Activate(e);  return retval; }
		
	/** Window has been deactivated. Traditional response is to
		deactivate the controls in the window.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Deactivate(IEvent* e) 
	{ bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->Deactivate(e);  return retval; }
	
	/** Application has been suspended. Control is passed to
		another application. 
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Suspend(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->Suspend(e);  return retval; }
	
	/** Application has been resumed. Control is passed back to the
		application from another application.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Resume(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->Resume(e);  return retval; }
		
	/** Mouse has moved outside the sensitive region.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MouseMove(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->MouseMove(e);  return retval; } 
		 
	/** User is holding down the mouse button and dragging the mouse.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MouseDrag(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->MouseDrag(e);  return retval; }
		 
	/** Left mouse button (or only mouse button) has been pressed.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/ 
	virtual bool16 LButtonDn(IEvent* e);// { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->LButtonDn(e);  return retval; }
		 
	/** Right mouse button (or second button) has been pressed.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 RButtonDn(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->RButtonDn(e);  return retval; }
		 
	/** Middle mouse button of a 3 button mouse has been pressed.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MButtonDn(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->MButtonDn(e);  return retval; }
		
	/** Left mouse button released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 LButtonUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->LButtonUp(e);  return retval; } 
		 
	/** Right mouse button released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 RButtonUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->RButtonUp(e);  return retval; } 
		 
	/** Middle mouse button released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MButtonUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->MButtonUp(e);  return retval; } 
		 
	/** Double click with any button; this is the only event that we're interested in here-
		on this event we load the placegun with an asset if it can be imported.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonDblClk(IEvent* e);
	/** Triple click with any button.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonTrplClk(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->ButtonTrplClk(e);  return retval; }
		 
	/** Quadruple click with any button.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonQuadClk(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->ButtonQuadClk(e);  return retval; }
		 
	/** Quintuple click with any button.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonQuintClk(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->ButtonQuintClk(e);  return retval; }
		 
	/** Event for a particular control. Used only on Windows.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ControlCmd(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->ControlCmd(e);  return retval; } 
		
		
	// Keyboard Related Events
	
	/** Keyboard key down for every key.  Normally you want to override KeyCmd, rather than KeyDown.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 KeyDown(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->KeyDown(e);  return retval; }
		 
	/** Keyboard key down that generates a character.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 KeyCmd(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->KeyCmd(e);  return retval; }
		
	/** Keyboard key released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 KeyUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->KeyUp(e);  return retval; }
		 
	
	// Keyboard Focus Related Functions
	
	/** Key focus is now passed to the window.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 GetKeyFocus(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->GetKeyFocus(e);  return retval; }
		
	/** Window has lost key focus.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	//virtual bool16 GiveUpKeyFocus(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->GiveUpKeyFocus(e);  return retval; }
		
	/** Typically called before GiveUpKeyFocus() is called. Return kFalse
		to hold onto the keyboard focus.
		@return kFalse to hold onto the keyboard focus
	*/
	virtual bool16 WillingToGiveUpKeyFocus() { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->WillingToGiveUpKeyFocus();  return retval; }
		 
	/** The keyboard is temporarily being taken away. Remember enough state
		to resume where you left off. 
		@return kTrue if you really suspended
		yourself. If you simply gave up the keyboard, return kFalse.
	*/
	virtual bool16 SuspendKeyFocus() { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->SuspendKeyFocus();  return retval; }
		 
	/** The keyboard has been handed back. 
		@return kTrue if you resumed yourself. Otherwise, return kFalse.
	*/
	virtual bool16 ResumeKeyFocus() { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->ResumeKeyFocus();  return retval; }
		 
	/** Determine if this eventhandler can be focus of keyboard event 
		@return kTrue if this eventhandler supports being the focus
		of keyboard event
	*/
	virtual bool16 CanHaveKeyFocus() const { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->CanHaveKeyFocus();  return retval; }
		 
	/** Return kTrue if this event handler wants to get keyboard focus
		while tabbing through widgets. Note: For almost all event handlers
		CanHaveKeyFocus and WantsTabKeyFocus will return the same value.
		If WantsTabKeyFocus returns kTrue then CanHaveKeyFocus should also return kTrue
		for the event handler to actually get keyboard focus. If WantsTabKeyFocus returns
		kFalse then the event handler is skipped.
		@return kTrue if event handler wants to get focus during tabbing, kFalse otherwise
	*/
	virtual bool16 WantsTabKeyFocus() const { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->WantsTabKeyFocus();  return retval; }
		 

	/** Platform independent menu event
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/

	//---------------Removed from CS3 API
//	virtual bool16 Menu(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->Menu(e);  return retval; }
		 
	/** Window needs to repaint.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 Update(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->Update(e);  return retval; }
		
	/** Method to handle platform specific events
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 PlatformEvent(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->PlatformEvent(e);  return retval; }
		 
	/** Call the base system event handler.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 CallSysEventHandler(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  retval =  delegate->CallSysEventHandler(e);  return retval; }
		
		
	/** Temporary.
	*/
	virtual void SetView(IControlView* view)
	{  
		InterfacePtr<IEventHandler> 
			delegate(this,IID_ISPSHADOWEVENTHANDLER); 
		delegate->SetView(view);  
	}
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE( SPTreeNodeEH, kSPTreeNodeEHImpl)

	
SPTreeNodeEH::SPTreeNodeEH(IPMUnknown* boss) :
	CEventHandler(boss)
{

}

bool16 SPTreeNodeEH::ButtonDblClk(IEvent* e) 
{
	bool16 retval = kFalse; 
	InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  
	retval =  delegate->LButtonDn(e);  
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		ptrIAppFramework->LogDebug("AP7_TemplateBuilder::SPTreeNodeEH::LButtonDn::ptrIAppFramework == nil");
		return retval;
	}

	bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
	if(!isUserLoggedIn)			
		return retval;
			
	InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
	if(!DataSprayerPtr)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer::Pointre to DataSprayerPtr not found");
		return retval;
	}
	//CA("theSelectedWidget==kSPSprayButtonWidgetID && theChange==kTrueStateMessage");
	if(CurrentSelectedProductRow == -100)//-100 is taken purposely.-100 does not exists.So something has to be selected from the list box.
	{				
		ptrIAppFramework->LogInfo("AP7_ProductFinder::SPSelectionObserver::Update::kSPSprayButtonWidgetID--CurrentSelectedProductRow == -100");
		return retval;
	}

	//CA("SubSectionSprayButtonWidgetID");
	//Apsiva 9 Comment
	InterfacePtr<ISubSectionSprayer> iSSSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
	if(iSSSprayer==nil)
	{
		ptrIAppFramework->LogInfo("AP7_ProductFinder::SPSelectionObserver::Update::kSPSprayButtonWidgetID::iSSSprayer==nil");
		return retval;
	}

	//added on 8Nov..
	K2Vector<int32> multipleSelection ;	
	PublicationNodeList temppNodeDataList = pNodeDataList;

	if(firstpNodeListCount == 0)
		orignalpNodeDataList = pNodeDataList;
	else
		temppNodeDataList = orignalpNodeDataList;

	//SDKListBoxHelper listHelper(this, kSPPluginID);			
	//IControlView * listBox = listHelper.FindCurrentListBox(iPanelControlData, 1);
	Mediator md;
	IControlView* iControlView = md.getMainPanelCtrlData()->FindWidget(kSPTreeListBoxWidgetID);
	if(iControlView != nil) 
	{
					
				
		InterfacePtr<ITreeViewController> treeViewCntrl(iControlView, UseDefaultIID());
		if(treeViewCntrl==nil) 
		{
			//CA("treeViewCntrl==nil");
			ptrIAppFramework->LogError("AP7_ProductFinder::SPSelectionObserver::Update::treeViewCntrl == nil");			
			return retval;
		}

		NodeIDList selectedItem;
			
		treeViewCntrl->GetSelectedItemsDisplayOrder(selectedItem);
		if(selectedItem.size()<=0)
		{
			//CA("selectedItem.size()<=0");
			ptrIAppFramework->LogInfo("AP7_ProductFinder::SPSelectionObserver::Update::selectedItem.size()<=0");			
			return retval;
		}

		NodeID nid=selectedItem[0];
		TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);
		int32 uid= uidNodeIDTemp->Get();
				
		const int32 kSelectionLength = selectedItem.size();
		if(kSelectionLength > 1)
		{
			singleSelectionSpray = kFalse;
			Mediator::setIsMultipleSelection(kTrue);

			//CA("kSelectionLength > 1");
					
			pNodeDataList.clear();
			for(int32 count = 0;count < kSelectionLength ; count++)
			{						
				NodeID nid=selectedItem[count];
				TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);
				int32 uid= uidNodeIDTemp->Get();
				PublicationNode pNode;
				bool16 isIdExist = dc.isExist(uid, pNode );
						
				if(isIdExist)
				{
					pNodeDataList.push_back(pNode);
				}
			}
					
			SubSectionSprayer SSsp;			
			SSsp.selectedSubSection.Append("Selected Item Group/Item List");	
			SSsp.selectedSubSection.SetTranslatable(kFalse);
			iSSSprayer->setSprayCustomProductOrItemListFlag(kTrue);
			SSsp.startSprayingSubSection();	
					
			firstpNodeListCount++;
					
			if(isCancelButtonClick == kFalse)
			{
				pNodeDataList.clear();
				CurrentSelectedProductRow =0; //multipleSelection[0];//-100 ; //**********
				pNodeDataList = temppNodeDataList;
						
				//listCntl->DeselectAll();
				isCancelButtonClick = kFalse;
				return retval;
			}
			else 
			{
				isCancelButtonClick = kTrue;
				return retval;
			}
		}
		else if(kSelectionLength == 1)
		{
			singleSelectionSpray = kTrue;
			Mediator::setIsMultipleSelection(kTrue);

			//CA("kSelectionLength == 1");
					
			pNodeDataList.clear();
			for(int32 count = 0;count < kSelectionLength ; count++)
			{						
				NodeID nid=selectedItem[count];
				TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);
				int32 uid= uidNodeIDTemp->Get();
				PublicationNode pNode;
				bool16 isIdExist = dc.isExist(uid, pNode );
						
				if(isIdExist)
				{
					pNodeDataList.push_back(pNode);
				}
			}

			SubSectionSprayer SSsp;			
			SSsp.selectedSubSection.Append("Selected Item Group/Item");	
			SSsp.selectedSubSection.SetTranslatable(kFalse);
			iSSSprayer->setSprayCustomProductOrItemListFlag(kTrue);
			SSsp.startSprayingSubSection();	
					
			firstpNodeListCount++;
					
			if(isCancelButtonClick == kFalse)
			{
				pNodeDataList.clear();
				CurrentSelectedProductRow =0;// multipleSelection[0];//-100 ; //**********
				pNodeDataList = temppNodeDataList;
				iSSSprayer->setSprayCustomProductOrItemListFlag(kFalse);

				//listCntl->DeselectAll();
				isCancelButtonClick = kFalse;
				return retval;
			}
			else 
			{
				isCancelButtonClick = kTrue;
				return retval;
			}
		}
	}

	return kFalse;
} 

bool16 SPTreeNodeEH::LButtonDn(IEvent* e)
{
	//CA("LButton Down");
	bool16 retval = kFalse; 
	InterfacePtr<IEventHandler> delegate(this,IID_ISPSHADOWEVENTHANDLER);  
	retval =  delegate->LButtonDn(e);  
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		ptrIAppFramework->LogDebug("AP7_TemplateBuilder::SPTreeNodeEH::LButtonDn::ptrIAppFramework == nil");
		return retval;
	}

	Mediator md;


	IControlView* iControlView = md.getMainPanelCtrlData()->FindWidget(kSPTreeListBoxWidgetID);
	if(iControlView != nil) 
	{
					
		InterfacePtr<ITreeViewController> treeViewCntrl(iControlView, UseDefaultIID());
		if(treeViewCntrl==nil) 
		{
			//CA("treeViewCntrl==nil");
			ptrIAppFramework->LogError("AP7_ProductFinder::SPSelectionObserver::Update::treeViewCntrl == nil");			
			return retval;
		}

		NodeIDList selectedItem;
			
		treeViewCntrl->GetSelectedItemsDisplayOrder(selectedItem);
		if(selectedItem.size()<=0)
		{
			//CA("selectedItem.size()<=0");
			ptrIAppFramework->LogInfo("AP7_ProductFinder::SPSelectionObserver::Update::selectedItem.size()<=0");			
			return retval;
		}

		NodeID nid=selectedItem[0];
		TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);
		int32 uid= uidNodeIDTemp->Get();
				
		const int32 kSelectionLength = selectedItem.size();
		PublicationNode pNode;
		if(kSelectionLength > 0)
		{	
			//CA("kSelectionLength > 1");	
									
			NodeID nid=selectedItem[0];
			TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);
			int32 uid= uidNodeIDTemp->Get();				
			bool16 isIdExist = dc.isExist(uid, pNode );		
			CurrentSelectedProductRow = pNode.getSequence();
			
		}
		
		/////Refresh TABLESource
		InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
		if(ptrTableSourceHelper != nil)
		{
			AcquireWaitCursor awc ;
			awc.Animate(); 
			if(ptrTableSourceHelper->isTABLESourcePanelOpen())
			{
				double objectId = pNode.getPubId();
				double pbObjectID = pNode.getPBObjectID();
				
				if(pNode.getIsProduct() == 1)
				{
					checkTreeType=3;
					ptrTableSourceHelper->showTableList(objectId , global_lang_id , Mediator::parentID , pNode.getTypeId() , Mediator::sectionID ,checkTreeType,pbObjectID);
				}
				if(pNode.getIsProduct() == 0)
				{
					checkTreeType=4;
					ptrTableSourceHelper->showTableList(objectId , global_lang_id , Mediator::parentID ,  pNode.getTypeId() , Mediator::sectionID ,checkTreeType,pbObjectID);
				}								
			}			
		}

				

		//------------------------------------refresh image list box ---------------------------------------//
				
		InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(/*kProductImageIFaceBoss*/kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
		if(ptrImageHelper != nil)
		{	
			AcquireWaitCursor awc ;
			awc.Animate() ; 
			if(ptrImageHelper->isImagePanelOpen())
			{

				PMString itemFieldIds("");
				PMString itemAssetTypeIds("");
				PMString itemGroupFieldIds("");
				PMString itemGroupAssetTypeIds("");
				PMString listTypeIds("");
				PMString listItemFieldIds("");
				bool16 isSprayItemPerFrameFlag = kTrue;
				double langId = ptrIAppFramework->getLocaleId();
				PMString currentSectionitemGroupIds("");
				PMString currentSectionitemIds("");
				

				if(pNode.getIsProduct() == 1)
				{			
					isSprayItemPerFrameFlag = kTrue;
					currentSectionitemGroupIds.AppendNumber(PMReal(pNode.getPubId()));
				}
				else if(pNode.getIsProduct() == 0)
				{
					isSprayItemPerFrameFlag = kFalse;
					currentSectionitemIds.AppendNumber(PMReal(pNode.getPubId()));
				}
			
				ptrIAppFramework->clearAllStaticObjects();

				ptrIAppFramework->EventCache_setCurrentSectionData( CurrentSelectedSection, langId , currentSectionitemGroupIds, currentSectionitemIds, itemFieldIds, itemAssetTypeIds, itemGroupFieldIds,itemGroupAssetTypeIds,listTypeIds, listItemFieldIds, kFalse, isSprayItemPerFrameFlag);



				double objectId = pNode.getPubId();

				if(pNode.getIsProduct() == 1)
					ptrImageHelper->showProductImages(objectId , global_lang_id , Mediator::parentID , Mediator::parentTypeID , Mediator::sectionID ) ;
				if(pNode.getIsProduct() == 0)
					ptrImageHelper->showItemImages(objectId , global_lang_id , Mediator::parentID , pNodeDataList[CurrentSelectedProductRow].getTypeId() , Mediator::sectionID, kFalse);				
			}
		}
        
        // ------ populate Catsy Tagger -------
        InterfacePtr<ICTGCatsyTaggerHelper> ptrCatsyTaggerHelper((static_cast<ICTGCatsyTaggerHelper*> (CreateObject(kCTGCatsyTaggerHelperBoss,ICTGCatsyTaggerHelper::kDefaultIID))));
        if(ptrCatsyTaggerHelper != nil)
        {
            AcquireWaitCursor awc ;
            awc.Animate() ;
            
            if(pNode.getIsProduct() == 0)
            {
                bool16 result = ptrCatsyTaggerHelper->populateTreeForItemAttributes( CurrentSelectedSection, pNode.getPubId(), pNode.getTypeId(), pNode.getName() );
                if(result)
                {
                    ptrIAppFramework->LogInfo("AP7_ProductFinder::SPTreeNodeEH::LButtonDn::CatsyTagger Populated Successfully");
                }
                else
                {
                    ptrIAppFramework->LogInfo("AP7_ProductFinder::SPTreeNodeEH::LButtonDn::CatsyTagger Populated not populated");
                }
                                                                                    
            }
        }
        else
        {
            ptrIAppFramework->LogInfo("AP7_ProductFinder::SPTreeNodeEH::LButtonDn::CatsyTagger not available");
        }
        
            
    }
        
        




	return retval;

}
//	end, File:	SPTreeNodeEH.cpp
