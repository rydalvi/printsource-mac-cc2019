#ifndef __PAGEDATA_H__
#define __PAGEDATA_H__

#include "VCPluginHeaders.h"
#include "TagStruct.h"
#include "vector"

using namespace std;

class TagInfo
{
public:
	double		elementID;
	PMString	elementName;
	int32       StartIndex;
	int32       EndIndex;
	double		typeID;
	int32		whichTab;
	bool16		isTaggedFrame;
	int32 tableFlag;
	int32 imageFlag;
	TagInfo()
	{
		elementID=-1;
		typeID=-1;
		isTaggedFrame=kFalse;
		whichTab=-1;
	}
};

typedef vector<TagInfo> ElementInfoList;

class ObjectData
{
public:
	double			objectID;
	double			objectTypeID;
	double			publicationID;
	PMString		objectName;
	bool16			isTableFlag;
	bool16			isImageFlag;
	int32			whichTab;
	ElementInfoList elementList;
	ObjectData()
	{
		objectID=-1;
		objectTypeID=-1;
		publicationID=-1;
		whichTab=-1;
	}
};



typedef vector<ObjectData> ObjectDataList;

class PageData
{
public:
	UIDRef BoxUIDRef;
	UID boxID;
	ObjectDataList objectDataList;
	PageData()
	{
		boxID=kInvalidUID;
	}
};
class TextVector
{
public:
PMString Word;
UIDRef BoxUIDRef;
int32 StartIndex;
int32 EndIndex;
TextVector()
{
StartIndex=0;
EndIndex=0;
}
};
typedef vector<TextVector> TextVectorList;

#endif