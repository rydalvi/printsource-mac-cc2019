#include "VCPlugInHeaders.h"
#include "IRegisterEvent.h"
#include "SPID.h"
#include "ILoginEvent.h"
#include "CAlert.h"
#include "SPLoginEventsHandler.h"
#include "SPSelectionObserver.h"
//#include "IPaletteMgr.h"   //Commented By sachin Sharma on 29/06/07
#include  "PaletteRefUtils.h"
#include "IPanelMgr.h"
#include "LocaleSetting.h"
#include "IWindow.h"
#include "IApplication.h"
#include "SPActionComponent.h"
#include "IWidgetParent.h"
//#include "ILoggerFile.h"
#include "IClientOptions.h"
#include "MediatorClass.h"
#include "IDialog.h"
#include "SPTreeDataCache.h"
//#include "GlobalData.h"
#define CA(z) CAlert::InformationAlert(z)

CREATE_PMINTERFACE(SPLoginEventsHandler,kSPLoginEventsHandlerImpl)

extern int32 check;
extern bool8 FilterFlag;
//extern vector<bool8> GlobalDesignerActionFlags;



SPLoginEventsHandler::SPLoginEventsHandler
(IPMUnknown* boss):CPMUnknown<ILoginEvent>(boss)
{
}

SPLoginEventsHandler::~SPLoginEventsHandler()
{}

bool16 SPLoginEventsHandler::userLoggedIn(void)
{
	//CA("user is loggin in");
	do
	{
		/* Check if the Template palette is on screen */
/*		InterfacePtr<IApplication> iApplication(gSession->QueryApplication());
		if(!iApplication)
		{
			CA("1");
			return kFalse;
		}

		InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPaletteManager());
		if(!iPaletteMgr)
		{
			CA("2");
			return kFalse;
		}
		
		InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr, UseDefaultIID()); 
		if(!iPanelMgr)
		{
			CA("3");
			return kFalse;
		}
		
		PMLocaleId nLocale = LocaleSetting::GetLocale();	

		//iPaletteMgr->Startup(nLocale);
		IWindow* wndPtr=iPaletteMgr->GetPalette(0);
		if(!wndPtr)
		{
			CA("4");
			return kFalse;
		}
CA("No Probs at all");*/
		//CA("User Logged in: PSPLoginEventsHandler");
		/*PSPActionComponent actionObsever(this);
		//PSPActionComponent obj(this);
	
		actionObsever.DoPalette();
		actionObsever.ClosePSPPalette();*/
		//CA("Logged in");
		//check = 1;
		//CA("user is loggin in ProductFinder");
		SPSelectionObserver selObserver(this);
		//////CA("user is loggin 3");
		selObserver.loadPaletteData();
		//CA("after user logged in");
		
	}while(kFalse);	
	
	return kTrue;	
}

bool16 SPLoginEventsHandler::userLoggedOut(void)
{
	//CA("User logged out");
	check = 0;
	SPActionComponent actionComponetObj(this);
	actionComponetObj.ClosePSPPalette();

	SPTreeDataCache dc;
	dc.clearMap();

	//Mediator md;
	//if(md.getMainPanelCtrlData() != NULL)
	//	md.getMainPanelCtrlData()->Release();
	//if(md.getMainPanelCtrlData1() != NULL)
	//	md.getMainPanelCtrlData1()->Release();

	if(FilterFlag == kTrue)
	{
		FilterFlag = kFalse;
		/*GlobalDesignerActionFlags.clear();
		GlobalDesignerActionFlags.push_back(kFalse);
		GlobalDesignerActionFlags.push_back(kFalse);
		GlobalDesignerActionFlags.push_back(kFalse);
		GlobalDesignerActionFlags.push_back(kFalse);
		GlobalDesignerActionFlags.push_back(kFalse);
		GlobalDesignerActionFlags.push_back(kFalse);
		GlobalDesignerActionFlags.push_back(kFalse);
		GlobalDesignerActionFlags.push_back(kFalse);*/
	}
	return kTrue;	
}

/*
IPanelControlData* SPLoginEventsHandler::QueryPanelControlData()
{



	do
	{
		InterfacePtr<IWidgetParent> iWidgetParent(this, UseDefaultIID());
		if (iWidgetParent == nil)
			break;

		InterfacePtr<IPanelControlData> iPanelControlData(iWidgetParent->GetParent(), UseDefaultIID());
		if (iPanelControlData == nil)
			break;
		iPanelControlData->AddRef();
		iPanel = iPanelControlData;
	}
	while (false); 
	return iPanel;
}
*/

bool16 SPLoginEventsHandler::resetPlugIn(void)
{
	//CA("SPLoginEventsHandler::resetPlugIn");
	do
	{	//check = 0;
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;

		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if(iApplication==nil){ 
			//CA("No iApplication");
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPLoginEventsHandler::resetPlugIn::iApplication==nil");
			break;
		}
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()/*, UseDefaultIID()*/); 
		if(iPanelMgr == nil){ 
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPLoginEventsHandler::resetPlugIn::iPanelMgr == nil");
			break;
		}

		bool16 isPanelOpen = iPanelMgr->IsPanelWithMenuIDMostlyVisible(kSPPanelWidgetActionID);
		if(!isPanelOpen){
			break;
		}
		
		SPActionComponent actionComponetObj(this);
		actionComponetObj.ClosePSPPalette();
		check = 1;
						
		bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
		if(!isUserLoggedIn)
			break;

		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(!ptrIClientOptions)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPLoginEventsHandler::resetPlugIn::kSPPanelWidgetActionID--No ptrIClientOptions");								
			break;
		}

		//if(Flag1==0)
			//Flag1=1;
		PMString defPubName("");
		double defPubId=-1;
		Mediator md;

		
		IPanelControlData* iPControlData = nil;
		md.setMainPanelCntrlData(iPControlData);
		md.panelControlData1 = nil;

		IControlView* iControlView = nil;
		md.setAssignBtnView(iControlView);
		md.setDesignerActionWidgetView(iControlView);
		md.setDropDownView(iControlView);
		md.setGreenFilterBtnView(iControlView);
		md.setIconView(iControlView);
		md.setIconViewNew(iControlView);
		md.setListBoxView(iControlView);
		md.setLocateWidgetView(iControlView);
		md.setPreviewWidgetView(iControlView);
		md.setPubNameTextView(iControlView);
		md.setRefreshButtonView(iControlView);
		md.setSectionDropDownLevel3View(iControlView);
		md.setShowPubTreeView(iControlView);
		md.setSprayButtonView(iControlView);
		md.setSubSecSprayButtonView(iControlView);
		md.setSubSectionDropDownLevel3View(iControlView);
		
		md.setTableSourceBtnView(iControlView);//-----
		md.setSearchView(iControlView);

		defPubId=ptrIClientOptions->getDefPublication(defPubName);
		if(defPubId<=0)
			break;

		md.setPublicationRoot(defPubId);

		PMLocaleId nLocale=LocaleSetting::GetLocale();
		iPanelMgr->ShowPanelByMenuID(kSPPanelWidgetActionID);

		
		//SPActionComponent::palettePanelPtr=(iPanelMgr);//Commented By Sachin sharma on 3/07/07
		
	//	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	//	if (panelControlData == nil)
	//	{
	//		CA("panelControlData invalid");
	//		break;
	//	}
	//	
	//	// for Image List Box
	//	InterfacePtr<IControlView> imageListControl(panelControlData->FindWidget(kImageGroupPanelWidgetID), UseDefaultIID());

	//	imageListControl->HideView();

		IControlView* icontrol = iPanelMgr->GetVisiblePanel(kSPPanelWidgetID);		
		if (icontrol == nil)
		{
			//CA("icontrol invalid");
			break;
		}		
		icontrol->Resize(PMPoint(PMReal(207),PMReal(291)));
		
	//// Bottom Group Panel
	//	InterfacePtr<IControlView> listBoxControl(panelControlData->FindWidget(kGroupPanelWidgetID), UseDefaultIID());
	//	if (listBoxControl == nil)
	//	{
	//		ASSERT_FAIL("listBoxControl invalid");
	//		break;
	//	}

	//	// main Group Panel
	//	InterfacePtr<IControlView> mainGroupPanelWidget(panelControlData->FindWidget(kMainGroupPanelWidgetID), UseDefaultIID());
	//	if (mainGroupPanelWidget == nil)
	//	{
	//		ASSERT_FAIL("mainGroupPanelWidget invalid");
	//		break;
	//	}

	//	// Middle Group Panel
	//	InterfacePtr<IControlView> middleGroupPanelWidget(panelControlData->FindWidget(kMiddleGroupPanelWidgetID), UseDefaultIID());
	//	if (middleGroupPanelWidget == nil)
	//	{
	//		ASSERT_FAIL("middleGroupPanelWidget invalid");
	//		break;
	//	}

	//	listBoxControl->MoveTo(PMPoint(PMReal(0),PMReal(112)));

	//	mainGroupPanelWidget->MoveTo(PMPoint(PMReal(0),PMReal(2)));

	//	middleGroupPanelWidget->MoveTo(PMPoint(PMReal(0),PMReal(84)));

		check = 1;
		
		SPSelectionObserver selObserver(this);
		
		selObserver.loadPaletteData();

	}while(kFalse);
	return kTrue;
}

