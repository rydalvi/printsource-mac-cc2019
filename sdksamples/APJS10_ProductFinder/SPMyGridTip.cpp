#include "VCPlugInHeaders.h"

//interfaces
#include "ITip.h"
//#include "ITool.h"
#include "IControlView.h"
#include "IWidgetUtils.h"
#include "IPanelControlData.h"
#include "Utils.h"
#include "TipsID.h"

//includes
#include "PMString.h"
#include "Trace.h"
#include "HelperInterface.h"
#include "StringUtils.h"
#include "CAlert.h"
#include "SPID.h"

class SPMyGridTip : public ITip
{ 
	public: 
		SPMyGridTip(IPMUnknown *boss); 
		virtual ~SPMyGridTip(); 
		virtual PMString GetTipText(const PMPoint& mouseLocation/*, ITip::TipWindowPlacementAdvice *outWindowPlacement*/); 
		virtual bool16 UpdateToolTipOnMouseMove  (  ) ; 

		DECLARE_HELPER_METHODS()
}; 

CREATE_PMINTERFACE( SPMyGridTip,kSPMyGridTipImpl )
DEFINE_HELPER_METHODS( SPMyGridTip )

SPMyGridTip::SPMyGridTip(IPMUnknown *boss)
	:HELPER_METHODS_INIT(boss)
{
}

SPMyGridTip::~SPMyGridTip()
{
}

PMString SPMyGridTip::GetTipText(const PMPoint& mouseLocation/*, ITip::TipWindowPlacementAdvice *outWindowPlacement*/) 
{ 

	do {
        InterfacePtr<IPanelControlData> data(this, IID_IPANELCONTROLDATA);
		ASSERT(data); if (!data) break;

		if ( data && data->Length() > 0)
		{ 
			for ( int32 i = data->Length() - 1; i >= 0; i-- ) // check children in reverse order of PanelView::Draw
			{ 
				if(data->IsWidgetInstantiated(i))
				{
					IControlView *  childView = data->GetWidget( i );
					SysPoint localWindowPoint = Utils<IWidgetUtils>()->ConvertPointToLocalWindow(mouseLocation, childView);
					if ( childView && childView->IsVisible() && childView->HitTest( localWindowPoint ) ) 
					{
						InterfacePtr<ITip> childTip(childView,IID_ITIP);
						if(childTip)
						{
							
							return childTip->GetTipText(localWindowPoint/*, outWindowPlacement*/);
						}
					}
				}
			}
		}
		} while(false);
	return PMString();
}
 
 bool16 SPMyGridTip::UpdateToolTipOnMouseMove( ) 
 {
	 return kTrue;
 }