#ifndef	__TABLEUTILITY_H__
#define	__TABLEUTILITY_H__

#include "VCPluginHeaders.h"
#include "vector"

using namespace std;


class TableUtility
{
public:
	bool16 isTablePresent(const UIDRef&, UIDRef&, int32 tableNumber=1);
	void setTableRowColData(const UIDRef&, const PMString&, const int32&, const int32&);
	void fillDataInTable(const UIDRef& tableUIDRef, double objectId, double tableTypeId,double& tableId, double CurrSectionid);
	void putHeaderDataInTable(const UIDRef& tableUIDRef, vector<double> vec_tableheaders, bool16 isTranspose);
	void resizeTable(const UIDRef& tableUIDRef, const int32& numRows, const int32& numCols, bool16 isTranspose);

};

#endif