
#pragma once
#ifndef	__ISPXLibraryButtonData__
#define	__ISPXLibraryButtonData__

class ISPXLibraryButtonData : public IPMUnknown
{
public:
	virtual void		SetName(PMString& theName) = 0;
	virtual PMString&	GetName() = 0;
	
protected:

	PMString			fName;
};

#endif