//========================================================================================
//  
//  $File: //depot/indesign_4.0/gm/source/sdksamples/basicdialog/SelectCustomerDialogController.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: Amit Kalmegh
//  
//  $DateTime: 2009/10/02 16:45:04 $
//  
//  $Revision: #1 $
//  
//  $Change: 323509 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:

// General includes:
#include "CDialogController.h"
#include "SystemUtils.h"

// Project includes:
#include "SPID.h"
#include "MediatorClass.h"
#include "ITextControlData.h"
#include "IAppFramework.h"
#include "ISpecialChar.h"
#include "CAlert.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"

#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
#define CA(X)	CAlert::InformationAlert(X);
//extern IDialog* dialog1;

/** Implements IDialogController based on the partial implementation CDialogController; 
	its methods allow for the initialization, validation, and application of dialog widget values.
  
	The methods take an additional parameter for 3.0, of type IActiveContext.
	See the design document for an explanation of the rationale CMMCustomIconWidgetEH this
	new parameter and the renaming of the methods that CDialogController supports.
	
	
	@ingroup basicdialog	
*/
class SelectCustomerDialogController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		SelectCustomerDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/**
			Initializes each widget in the dialog with its default value.
			Called when the dialog is opened.
			@param Context
		*/
		virtual void InitializeDialogFields( IActiveContext* dlgContext);

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			this method to be called. When all widgets are valid, 
			ApplyFields will be called.		
			@param myContext
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.
		*/
		virtual WidgetID ValidateDialogFields( IActiveContext* myContext);

		/**
			Retrieve the values from the widgets and act on them.
			@param myContext
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields( IActiveContext* myContext, const WidgetID& widgetId);
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(SelectCustomerDialogController, kSelectCustomerDialogControllerImpl)

/* ApplyFields
*/
void SelectCustomerDialogController::InitializeDialogFields( IActiveContext* dlgContext) 
{
	// Put code to initialize widget values here.
	do
	{		
		CDialogController::InitializeDialogFields(dlgContext);
		InterfacePtr<IDialogController> dlgController(this,IID_IDIALOGCONTROLLER);
		if(dlgController==nil)
		{
			break;
		}
		
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SelectCustomerDialogController::InitializeDialogFields::ptrIAppFramework nil");
			break;
		}
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(!iConverter)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SelectCustomerDialogController::InitializeDialogFields::iConverter nil");
			break;
		}
		
		do{
			InterfacePtr<IStringListControlData> CustSelDropListData(dlgController->QueryListControlDataInterface(kCustomerSelectionDropDownWidgetID));
			if(CustSelDropListData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SelectCustomerDialogController::InitializeDialogFields::CustSelDropListData nil");
				break;
			}
			CustSelDropListData->Clear(kFalse,kFalse);	

			InterfacePtr<IDropDownListController> CustSelDropListController(CustSelDropListData,UseDefaultIID());
			if (CustSelDropListController == nil)
			{
				//CA("CustSelDropListController nil");
				break;
			}


			double CustomerPVTypeId	= ptrIAppFramework->CONFIGCACHE_getCustomerPVTypeId(1);
		
			//VectorPossibeValueModelPtr PVModelPtr = ptrIAppFramework->POSSIBELVALUECACHE_getAllPossibleValuesByTypeId(CustomerPVTypeId);
			PicklistGroup pickListGroupObj = ptrIAppFramework->StructureCache_getPickListGroups(CustomerPVTypeId);
			
			if(pickListGroupObj.getPicklistValues().size()==0)
			{
				//CA("PVModelPtr->size()==0");
				ptrIAppFramework->LogError("AP7_ProductFinder::SelectCustomerDialogController::InitializeDialogFields::pickListGroupObj.getPicklistValues().size()==0");				
				break;
			}
			

			vector<PicklistValue> picklistValues = pickListGroupObj.getPicklistValues();
			int32 dropDownListRowIndex = 0;
				
			for(int32 j=0; j < picklistValues.size(); j++)
			{	
				PMString CustomerName("");
				PMString TempBuffer = picklistValues.at(j).getPossibleValue();
				if(iConverter)
				{ 
					CustomerName=iConverter->translateString(TempBuffer);
				}
				else
				{ 				
					CustomerName = TempBuffer;
				}
				
				if(dropDownListRowIndex == 0)
				{
					PMString select("--Select--");
					select.SetTranslatable(kFalse);
					CustSelDropListData->AddString(select, dropDownListRowIndex, kFalse, kFalse);
					dropDownListRowIndex++;
				}
				CustomerName.SetTranslatable(kFalse);
				CustSelDropListData->AddString(CustomerName, dropDownListRowIndex, kFalse, kFalse);
				dropDownListRowIndex++;

				Mediator::CustomerVector.push_back(picklistValues.at(j));
			}
			
			int32 selectedIndex = Mediator::getDropDownListSelectedIndex(1);
			if(selectedIndex > 0)
				CustSelDropListController->Select(selectedIndex);
			else
				CustSelDropListController->Select(0);
			
		}while(kFalse);
		do{
			InterfacePtr<IStringListControlData> DivSelDropListData(dlgController->QueryListControlDataInterface(kDivisionSelectionDropDownWidgetID));
			if(DivSelDropListData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SelectCustomerDialogController::InitializeDialogFields::DivSelDropListData nil");
				break;
			}
			DivSelDropListData->Clear(kFalse,kFalse);	

			InterfacePtr<IDropDownListController> DivSelDropListController(DivSelDropListData,UseDefaultIID());
			if (DivSelDropListController == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SelectCustomerDialogController::InitializeDialogFields::DivSelDropListController nil");
				break;
			}

			double DivisionPVTypeId	= ptrIAppFramework->CONFIGCACHE_getCustomerPVTypeId(0);
		
			//VectorPossibeValueModelPtr PVModelPtr = ptrIAppFramework->POSSIBELVALUECACHE_getAllPossibleValuesByTypeId(DivisionPVTypeId);
			PicklistGroup pickListGroupObj = ptrIAppFramework->StructureCache_getPickListGroups(DivisionPVTypeId);
			
			if(pickListGroupObj.getPicklistValues().size()==0)
			{
				//CA("PVModelPtr->size()==0");
				ptrIAppFramework->LogError("AP7_ProductFinder::SelectCustomerDialogController::InitializeDialogFields::pickListGroupObj.getPicklistValues().size()==0");
				break;
			}
			
			vector<PicklistValue> picklistValues = pickListGroupObj.getPicklistValues();
			int32 dropDownListRowIndex = 0;
				
			for(int32 j=0; j < picklistValues.size(); j++)
			{	
				PMString DivisionName("");
				PMString TempBuffer = picklistValues.at(j).getPossibleValue();
				if(iConverter)
				{ 
					DivisionName=iConverter->translateString(TempBuffer);
				}
				else
				{ 				
					DivisionName = TempBuffer;
				}
				
				if(dropDownListRowIndex == 0)
				{
					PMString select("--Select--");
					select.SetTranslatable(kFalse);
					DivSelDropListData->AddString(select, dropDownListRowIndex, kFalse, kFalse);
					dropDownListRowIndex++;
				}
				DivisionName.SetTranslatable(kFalse);
				DivSelDropListData->AddString(DivisionName, dropDownListRowIndex, kFalse, kFalse);
				dropDownListRowIndex++;

				Mediator::DivisionVector.push_back(picklistValues.at(j));
			}
			
			int32 selectedIndex = Mediator::getDropDownListSelectedIndex(0);
			if(selectedIndex > 0)
				DivSelDropListController->Select(selectedIndex);
			else
				DivSelDropListController->Select(0);
			
		}while(kFalse);
	}while(kFalse);
	// Put code to initialize widget values here.
}


/* ValidateFields
*/
WidgetID SelectCustomerDialogController::ValidateDialogFields( IActiveContext* myContext) 
{
	WidgetID result = kNoInvalidWidgets;


	// Put code to validate widget values here.


	return result;
}

/* ApplyFields
*/
void SelectCustomerDialogController::ApplyDialogFields( IActiveContext* myContext, const WidgetID& widgetId) 
{
	//CA("in apply dialog fields");
	do{
	}while(kFalse);
}

// End, SelectCustomerDialogController.cpp.



