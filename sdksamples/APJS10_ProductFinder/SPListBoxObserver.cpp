#include "VCPlugInHeaders.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IListBoxController.h"
#include "CAlert.h"
#include "CObserver.h"
#include "SPID.h"
#include "SDKListBoxHelper.h"
#include "IPanelControlData.h"
//#include "TPLMediatorClass.h"
//#include "TPLListboxData.h"
#include "IListControlData.h"
//#include "GlobalData.h"
#include "FilterData.h"
#include "MediatorClass.h"
//#include "IDataSprayer.h"
#include "PublicationNode.h"
#include "IAppFramework.h"
#include "IBooleanControlData.h"
//#include "PRImageHelper.h"
#include "FileUtils.h"
#include "IWindow.h"
#include "IDataBase.h"
#include "IPanelMgr.h"
//#include "IPaletteMgr.h"  //Commented By SAchin Sharma on 29/06/07
#include "PaletteRefUtils.h"
#include "IApplication.h"
#include "AcquireModalCursor.h"
//#include "TSTableSourceHelper.h"
//#include "SPSelectionObserver.h"
#include "FileUtils.h"

#define CA(x)	CAlert::InformationAlert(x)
int32 PFRow = -1;
extern FilterDataList samitvflist;
//extern FilterDataList tempMileStoneListBoxValues;
extern PublicationNodeList pNodeDataList;
extern int32 CurrentSelectedSection;
extern int32 CurrentSelectedPublicationID;
extern int32 CurrentSelectedSubSection;
extern int32 CurrentSelectedProductRow;
K2Vector<int32> CurrentListofProducts;

int32 CurrentLstBoxIndex;
// new added  @Vaibhav
extern K2Vector<PMString>imageVector1;
extern bool16 searchResult;
extern K2Vector<PMString>imageType;
extern K2Vector<int32>imageTypeID;
extern int32 global_lang_id ;
extern bool16 isItemHorizontalFlow;

extern int32 checkTreeType;
namespace
{
	GSysRect getBoundingBoxOfWindow()
	{
		do
		{
				InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
				if(iApplication==nil)
				{ 
					//CA("iApplication == nil");
					break;
				}				
	
				InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager());
				if(iPanelMgr == nil)
				{ 
					//CA("iPanelMgr == nil");
					break ;
				}
				
				IWindow* palette = nil ;

				IDataBase* db = ::GetDataBase(/*gSession*/GetExecutionContextSession()); //Cs4
				if (db == nil)
				{
					//CA("db == nil");				
					break;
				}					
					
		}
		while(kFalse);

		GSysRect PalleteBounds ;			
		PalleteBounds.top = 0 ;
		PalleteBounds.left =  0 ;
		PalleteBounds.right = 0 ;
		PalleteBounds.bottom = 0 ;

		return PalleteBounds ;
	}
}


class SPListBoxObserver : public CObserver
{
public:
	SPListBoxObserver(IPMUnknown *boss);
	~SPListBoxObserver();
	virtual void AutoAttach();
	virtual void AutoDetach();
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
	// new added @Vaibhav
	void populateProductImageListBox();
	bool16	fileExists(PMString& path, PMString& name);
private:
	void updateListBox(bool16 isAttaching);
};

CREATE_PMINTERFACE(SPListBoxObserver, kSPListBoxObserverImpl)

SPListBoxObserver::SPListBoxObserver(IPMUnknown* boss)
: CObserver(boss)
{
}

SPListBoxObserver::~SPListBoxObserver()
{
}

void SPListBoxObserver::AutoAttach()
{
	
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
		
	}
	
	updateListBox(kTrue);

}

void SPListBoxObserver::AutoDetach()
{
	updateListBox(kFalse);
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this,IID_ILISTCONTROLDATA);  //DetachObserver(this);====Commented By Sachin Sharma on 29/06/07
	}
	
}

void SPListBoxObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	//CA("SPListBoxObserver::Update");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return;
	}
	if ((protocol == IID_ILISTCONTROLDATA) && (theChange == kListSelectionChangedByUserMessage)|| (theChange == kListSelectionChangedMessage))//kListSelectionChangedByUserMessage) ) 
	{		
		do 
		{			
			//CA("inside update of listbox");
			InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
			if (panelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPListBoxObserver::Update::No panelControlData");
				return;
			}			
			SDKListBoxHelper listHelper(this, kSPPluginID);
			
			/*if(listHelper==nil)
				CA("hiu");*/
			IControlView * listBox = listHelper.FindCurrentListBox(panelControlData, 1);
			if(listBox != nil) 
			{
				//CA("listBox == nil");
				IControlView * listBox1 = listHelper.FindCurrentListBox(panelControlData, 1);
				if(listBox1 == nil) 
				{
					ptrIAppFramework->LogDebug("AP7_ProductFinder::SPListBoxObserver::Update::No listBox1");
					break;
				}
				
				InterfacePtr<IListBoxController> listCntl(listBox1,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
				if(!listCntl)
				{
					ptrIAppFramework->LogDebug("AP7_ProductFinder::SPListBoxObserver::Update::No listCntl");
					break;
				}
				
				K2Vector<int32> multipleSelection ;				
				listCntl->GetSelected( multipleSelection );
				
				const int kSelectionLength =  multipleSelection.Length();
				/*PMString len("The selected length is ");
				len.AppendNumber(kSelectionLength);
				CA(len);*/

				CurrentListofProducts = multipleSelection;
				
				if (kSelectionLength> 0 )
				{	
					
					int indexSelected ;
					//CA("Inside");
					for(int i=0; i < multipleSelection.Length() ; i++)
					{
						indexSelected = multipleSelection[i];
						
						if(i==0)
						CurrentSelectedProductRow = indexSelected;
					}

					CurrentSelectedProductRow = CurrentLstBoxIndex = indexSelected ;
					Mediator  md;
					if(md.getSprayButtonView()== nil)
					{
						//CA("ListBoxObserver::md.getSprayButtonView()==nil");
					}
					else
					md.getSprayButtonView()->Enable(kTrue);
					
					if(md.getIconView() == nil){
						//CA("ListBoxObserver::md.getIconView() == nil");
					}
					else
					md.getIconView()->Enable(kTrue);

					if(md.getIconViewNew() == nil){
						//CA("ListBoxObserver::md.getIconView() == nil");
					}
					else
					md.getIconViewNew()->Enable(kTrue);

					if(md.getPreviewWidgetView()==nil)
					{
						//CA("ListBoxObserver::md.getPreviewWidgetView()==nil");
					}
					else
                    	md.getPreviewWidgetView()->Enable(kTrue);

					
					
					if(md.getDesignerActionWidgetView()== nil)	
					{
						//CA("ListBoxObserver::md.getDesignerActionWidgetView()== nil");
					}
					else
					{	///////
						/*InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
						if(ptrIAppFramework == nil)
							break;
						CMilestoneModel  * milestoneval = NULL;

						milestoneval = ptrIAppFramework->MilestoneCache_getDesignerMilestone();

						if(milestoneval == NULL)*/
						{	
							md.getDesignerActionWidgetView()->Disable();							
						}
						//else
						//{
						//	// Rolr ID = 1 not requireed here ... Taken from Userprofile in Application framework
						//	bool16 success = ptrIAppFramework->MILESTONECACHE_getRoleMilestoneWriteAccess(1 , milestoneval->getId());
						//	if(success)
						//	{							
						//		md.getDesignerActionWidgetView()->Enable();
						//		md.getDesignerActionWidgetView()->ShowView();
						//	}
						//	if(!success)
						//	{							
						//		md.getDesignerActionWidgetView()->Disable();
						//		md.getDesignerActionWidgetView()->HideView();
						//	}													
						//}
						///////						
					}

					if(md.getRefreshButtonView() == nil)
					{
						//CA("md.getRefreshButtonView()==nil");
					}
					else
					/////////////md.getRefreshButtonView()->Enable();
					md.getRefreshButtonView()->Enable();
							
					//}

					//(md.vMultipleSelection).clear();
					md.vMultipleSelection  = multipleSelection;
					//ended by Yogesh on 25.2.05
						
					/*InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
					if(!DataSprayerPtr)
					{
						ptrIAppFramework->LogDebug("AP7_ProductFinder::SPListBoxObserver::Update::No DataSprayerPtr");
						return;
					}*/
										
					
					md.setCurrentObjectID(pNodeDataList[CurrentSelectedProductRow].getPubId());
					md.setPBObjectID(pNodeDataList[CurrentSelectedProductRow].getPBObjectID());

					
					//DataSprayerPtr->FillPnodeStruct(pNodeDataList[CurrentSelectedProductRow],CurrentSelectedSection, CurrentSelectedPublicationID, CurrentSelectedSubSection);
				
					//DataSprayerPtr->setFlow(isItemHorizontalFlow);
				
					//DataSprayerPtr->startSpraying();
					
				}

				int32 PId = pNodeDataList[CurrentSelectedProductRow].getParentId();
				int32 typeID = pNodeDataList[CurrentSelectedProductRow].getTypeId();
				
				Mediator::parentID = PId;
				Mediator::parentTypeID = typeID;
				
			}
			
			

		} while(0);
		//------------------------------------refresh image list box ---------------------------------------//
			
		//Apsiva 9 comment
		//InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(/*kProductImageIFaceBoss*/kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
		//if(ptrImageHelper != nil)
		//{	
		//	AcquireWaitCursor awc ;
		//	awc.Animate() ; 
		//	if(ptrImageHelper->isImagePanelOpen())
		//	{
		//		int32 objectId = pNodeDataList[CurrentSelectedProductRow].getPubId();

		//						if(searchResult)
		//		{//CA("if Search result");
		//			if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 1)
		//				ptrImageHelper->showProductImages(objectId , global_lang_id , pNodeDataList[CurrentSelectedProductRow].getParentId() , pNodeDataList[CurrentSelectedProductRow].getTypeId() , pNodeDataList[CurrentSelectedProductRow].getSectionID() ) ;
		//			if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 0)
		//				ptrImageHelper->showItemImages(objectId , global_lang_id , pNodeDataList[CurrentSelectedProductRow].getParentId() , pNodeDataList[CurrentSelectedProductRow].getTypeId() , pNodeDataList[CurrentSelectedProductRow].getSectionID(), kFalse);
		//		}
		//		else
		//		{
		//			//CA("else SpListBoxObserver::Update");
		//			if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 1)
		//				ptrImageHelper->showProductImages(objectId , global_lang_id , Mediator::parentID , Mediator::parentTypeID , Mediator::sectionID ) ;
		//			if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 0)
		//				ptrImageHelper->showItemImages(objectId , global_lang_id , Mediator::parentID , pNodeDataList[CurrentSelectedProductRow].getTypeId() , Mediator::sectionID, kFalse);
		//		}
		//	}
		//}
		////-----------------------------------------------------------------------------------------------------//	
		///////Refresh TABLESource
		//InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
		//if(ptrTableSourceHelper != nil)
		//{
		//	AcquireWaitCursor awc ;
		//	awc.Animate(); 
		//	if(ptrTableSourceHelper->isTABLESourcePanelOpen())
		//	{
		//		int32 objectId = pNodeDataList[CurrentSelectedProductRow].getPubId();
		//		int32 pbObjectID = pNodeDataList[CurrentSelectedProductRow].getPBObjectID();
		//		
		//		if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 1)
		//		{
		//			checkTreeType=3;
		//			ptrTableSourceHelper->showTableList(objectId , global_lang_id , Mediator::parentID , Mediator::parentTypeID , Mediator::sectionID ,checkTreeType,pbObjectID);
		//		}
		//		if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 0)
		//		{
		//			checkTreeType=4;
		//			ptrTableSourceHelper->showTableList(objectId , global_lang_id , Mediator::parentID , Mediator::parentTypeID , Mediator::sectionID ,checkTreeType,pbObjectID);
		//		}
		//						
		//	}
		//	return;
		//}
		////END
	}

	if ((protocol == IID_ILISTCONTROLDATA) && (theChange == kListSelectionChangedByUserMessage))//kListSelectionChangedByUserMessage) ) 
	{	
		do 
		{
			InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
			if (panelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPListBoxObserver::Update::No panelControlData");
				return;
			}
			
			SDKListBoxHelper listHelper(this, kSPPluginID);
			/*if(listHelper==nil)
				CA("hiu");*/
			IControlView * listBox = listHelper.FindCurrentListBox(panelControlData, 2);
			if(listBox != nil) 
			{				
				InterfacePtr<IListBoxController> listCntl(listBox,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
				if(listCntl == nil)
				{
					ptrIAppFramework->LogDebug("AP7_ProductFinder::SPListBoxObserver::Update::No listCntl");
					break;
				}

				K2Vector<int32> multipleSelection ;
				listCntl->GetSelected( multipleSelection ) ;
				const int kSelectionLength =  multipleSelection.Length() ;
				
				if (kSelectionLength> 0 )
				{
					int indexSelected ;
					PMString dbgInfoString("Selected item(s): ");
					for(int i=0; i < multipleSelection.Length() ; i++) 
					{
						indexSelected = multipleSelection[i];
						//tempMileStoneListBoxValues[indexSelected].isSelected=(tempMileStoneListBoxValues[indexSelected].isSelected)? kFalse: kTrue;
						//listHelper.CheckUncheckRow(listBox,indexSelected,tempMileStoneListBoxValues[indexSelected].isSelected);
					}
				}
			}
		} while(0);
		//CA("at the end of list box observer 2");
	}
	//CA("at the end of list box observer 3");
}

//////////////////////


void SPListBoxObserver::updateListBox(bool16 isAttaching)
{	
			//CA("Inside updateListBox");
			/*CA("11");
			InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
			if (panelControlData == nil)
			{
				CA("panelControlData == nil");
				return;
			}

			SDKListBoxHelper listHelper(this, kSPPluginID);
			IControlView * listBox = listHelper.FindCurrentListBox(panelControlData, 2);
			if(listBox == nil) {
				CA("listBox == nil");
			}
			PMString as;
			as.Append("Amit kuamr");

			listHelper.AddElement(listBox , as, kSPRFltOptTextWidgetID,0);*/
}

void SPListBoxObserver::populateProductImageListBox()
{
	
	////CA("SPListBoxObserver::populateProductImageListBox()");
	//InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	//if(ptrIClientOptions==nil)
	//{
	//	CAlert::ErrorAlert("Interface for IClientOptions not found.");
	//	return ;
	//}
	//
	//PMString imagePath=ptrIClientOptions->getImageDownloadPath();
	//if(imagePath!="")
	//{
	//	char *imageP=imagePath.GrabCString();
	//	if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
	//		#ifdef MACINTOSH
	//			imagePath+="/";
	//		#else
	//			imagePath+="\\";
	//		#endif
	//}

	////CA("Before Image Path");
	//if(imagePath=="")
	//	return ;
	////CA(imagePath);
	//PMString imagePathWithSubdir = imagePath;

	//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//if(ptrIAppFramework == nil)
	//	return ;
	//	//break;

	//VectorTypeInfoPtr typeValObj1=
	//ptrIAppFramework->ElementCache_getImageAttributesByIndex(3);
	//if(typeValObj1==nil)
	//	return ;
	//	//break;
	//
	//VectorTypeInfoValue::iterator it2;
	//
	//for(it2=typeValObj1->begin();it2!=typeValObj1->end();it2++)
	//{	
	//	//Following variables stores Type ID of image and image name				
	//	int32 Imagetypeid =it2->getType_id() ;

	//	imageTypeID.push_back(Imagetypeid);  // typeID of Image Vaibhav_24April
	//	
	//	PMString ImageTypeName = it2->getName();
	//	
	//	imageType.push_back(ImageTypeName);   // ImageTypeName Vaibhav_24April		

	//	int imageTextIndex = 0;
	//	//CA("Before Image Type Name");
	//	//CA(ImageTypeName);
	//	PMString fileName("");
	//	int32 assetID;
	//	CObjectValue oVal;
	//	int32 objectId = pNodeDataList[CurrentSelectedProductRow].getPubId();
	//		
	//	oVal=ptrIAppFramework->GETProduct_getObjectElementValue(objectId);  // new added in appFramework
	//	int32 parentTypeID= oVal.getObject_type_id();

	//	do
	//	{			
	//		VectorAssetValuePtr AssetValuePtrObj = ptrIAppFramework->GETAssets_GetAssetByParentAndType(objectId,parentTypeID,Imagetypeid);
	//		if(AssetValuePtrObj == NULL)
	//			continue ;

	//		if(AssetValuePtrObj->size() ==0)
	//			continue ;

	//		VectorAssetValue::iterator it; // iterator of Asset value

	//		CAssetValue objCAssetvalue;
	//		for(it = AssetValuePtrObj->begin();it!=AssetValuePtrObj->end();it++)
	//		{
	//			objCAssetvalue = *it;
	//			fileName = objCAssetvalue.getFile_name();
	//			assetID = objCAssetvalue.getAsset_id();
	//			//CA(fileName);
	//		}

	//		if(fileName=="")
	//		continue ;

	//		do
	//		{
	//			SDKUtilities::Replace(fileName,"%20"," ");
	//		}while(fileName.IndexOfString("%20") != -1);

	//		//CA(fileName);
	//		PMString typeCodeString;

	//		typeCodeString=ptrIAppFramework->ClientActionGetAssets_getProductAssetFolder(assetID);
	//		//CA("typeCodeString : " + typeCodeString);
	//		if(typeCodeString.NumUTF16TextChars()!=0)
	//			imagePathWithSubdir.Append(typeCodeString);	

	//		FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir));
	//		SDKUtilities ::AppendPathSeparator(imagePathWithSubdir);

	//		//CA("Image Path = " + imagePathWithSubdir + " FileName " + fileName);
	//		if(!fileExists(imagePathWithSubdir,fileName))
	//		{
	//			if(!ptrIAppFramework->GETAsset_downLoadProductAsset(assetID,imagePathWithSubdir))
	//			{					
	//				//CA("download From Server Failed");
	//				continue;
	//			}
	//		}

	//		if(!fileExists(imagePathWithSubdir,fileName))	
	//		{
	//			//CA("Image Not Found on Local Drive");
	//			continue ; 
	//		}

	//		PMString ImagePath=imagePathWithSubdir+fileName;

	//	
	//		// commented by Vaibhav 
	//	/*	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	//		if (panelControlData == nil)
	//		{
	//			CA("panelControlData invalid");
	//			continue;
	//		}*/
	//		IPanelControlData *panelControlData = Mediator::panelControlData1;

	//		SDKListBoxHelper listHelper(this, kSPPluginID);
	//		IControlView* lstboxControlView=panelControlData->FindWidget(kImageListboxWidgetID);
	//		if(lstboxControlView == nil)
	//		{
	//			CA("lstboxControlView == nil");
	//			return;
	//		}
	//		// for displaing text in bottom of Image

	//		//IControlView *staticTextImageWidgetId = panelControlData->FindWidget(kImageTextWidgetId);

	//		//if(staticTextImageWidgetId == nil)
	//		//{
	//		//	CA("staticTextImageWidgetId == nil");	
	//		//}

	//		//InterfacePtr<ITextControlData> textControlData(staticTextImageWidgetId,UseDefaultIID());

	//		//if (textControlData == nil)
	//		//{
	//		//	CAlert::InformationAlert("ITextControlData nil");	
	//		//}

	//			//CA("Before AddElement ");
	//		imageVector1.push_back(ImagePath);
	//		
	//		//listHelper.AddElementImage(lstboxControlView,ImageTypeName,ImagePath, kPRLCustomPanelViewWidgetID, 0,0,1);
	//			//CA("After Add Element");
	//		//textControlData->SetString(ImageTypeName);
	//		//listHelper.AddElement(lstboxControlView,ImageTypeName,kImageTextWidgetId,0,1);
	//		//imageTextIndex++;
	//	//	CA("8");

	//	}while(0);
	//}
}






bool16 SPListBoxObserver::fileExists(PMString& path, PMString& name)
{
	PMString theName("");
	theName.Append(path);
	theName.Append(name);

	 const char *file=/*const_cast<char*>*/(theName.GrabCString()/*GetPlatformString().c_str()*/); //Cs4

	FILE *fp=NULL;

	fp=std::fopen(file, "r");
	if(fp!=NULL)
	{
		std::fclose(fp);
		return kTrue;
	}
	return kFalse;
}



