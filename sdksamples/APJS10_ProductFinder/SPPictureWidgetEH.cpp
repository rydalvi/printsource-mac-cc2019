//========================================================================================
//  
//  $File: //depot/indesign_4.0/gm/source/sdksamples/pictureicon/SPPictureWidgetEH.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2005/03/08 13:31:35 $
//  
//  $Revision: #1 $
//  
//  $Change: 323509 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPluginHeaders.h"

// Interfaces
#include "IControlView.h"
#include "IBooleanControlData.h"
#include "IUIFontSpec.h"
// General includes:
#include "CEventHandler.h"
#include "CAlert.h"
#include "IWidgetParent.h"
#include "IPanelControlData.h"

// Project includes:
#include "SPID.h"
#include "IAppFramework.h"
#include "PublicationNode.h"
#define CA(X) CAlert::InformationAlert(X)

extern PublicationNodeList pNodeDataList; 
extern int32 CurrentSelectedSubSection;
extern bool16 isThumb;
extern int32 curWidgetIndex;
/** 
	Traps LButtonDn events.

	The only method that is overridden by this class is
	LButtonDn; this is to create a picture-based button which can use
	Mac PICT or Windows BITMAP resources rather than just icons as in the
	other buttons in the widget set.
	The implementation is quite sparse in that there are only two different
	pictures for the selected and uselected states. There is
	none of the sophistication of the real iconic buttons such as
	being able to display a highlighted state when the end-user is clicking on the
	button.

	@ingroup pictureicon
	
*/

class SPPictureWidgetEH : public CEventHandler
{
public:

	/**
		ctor. 
		@param boss interface ptr on the boss object to which the interface implemented here belongs.
	*/	
	SPPictureWidgetEH(IPMUnknown *boss);
	
	/**
		Destructor
	*/	
	virtual ~SPPictureWidgetEH(){}

	/**
		Handle left button down events.
		Change the data model, and also change the picture displayed.
		This is because there is no platform peer that knows how to change its visual representation
		so we have to do it ourselves.

		@param e event of interest.
	*/
	virtual bool16 LButtonDn(IEvent* e);
};

CREATE_PMINTERFACE( SPPictureWidgetEH, kSPCustomPictureWidgetEHImpl)

	
SPPictureWidgetEH::SPPictureWidgetEH(IPMUnknown *boss) :
	CEventHandler(boss)
{
}

bool16 SPPictureWidgetEH::LButtonDn(IEvent* e)
{

	do {
		//CA("...LButtonDn...");	
		int32 rsrcID = 0;

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CA(" ptrIAppFramework nil ");
			return 1;
		}

		InterfacePtr<IBooleanControlData> controlData(this, IID_IBOOLEANCONTROLDATA); // no kDefaultIID
		if(controlData==nil) {
			CA("SPPictureWidgetEH::LButtonDn - No IBooleanControlData found on this boss.");
			break;
		}
		InterfacePtr<IUIFontSpec> fontSpec(this, IID_IUIFONTSPEC); // no kDefaultIID
		if(fontSpec==nil) {
			CA("SPPictureWidgetEH::LButtonDn - No IUIFontSpec found on this boss.");
			break;
		}

		InterfacePtr<IWidgetParent> findParentOfRollOverIconButtonWidget(this,UseDefaultIID());
		if(findParentOfRollOverIconButtonWidget == nil)
		{
			
			ptrIAppFramework->LogDebug("AP7CreateMedia::Update::findParentOfRollOverIconButtonWidget == nil");
			break;
		}
		IPMUnknown *  primaryResourcePanelWidgetUnKnown = findParentOfRollOverIconButtonWidget->GetParent();
		if(primaryResourcePanelWidgetUnKnown == nil)
		{
			ptrIAppFramework->LogDebug("AP7CreateMedia::Update::primaryResourcePanelWidgetUnKnown == nil");
			break;
		}
		InterfacePtr<IControlView>primaryResourcePanelWidgetControlView(primaryResourcePanelWidgetUnKnown,UseDefaultIID());
		if(primaryResourcePanelWidgetControlView == nil)
		{
			ptrIAppFramework->LogDebug("AP7CreateMedia::Update::primaryResourcePanelWidgetControlView == nil");
			break;
		}
		InterfacePtr<IPanelControlData>primaryResourcePanelWidgetPanelControlData(primaryResourcePanelWidgetControlView,UseDefaultIID());
		if(primaryResourcePanelWidgetPanelControlData == nil)			
		{
			ptrIAppFramework->LogDebug("AP7CreateMedia::Update::primaryResourcePanelWidgetPanelControlData == nil");
			break;
		}

		//find parent of primaryResourcePanelWidgetPanel which is a cellpanelwidget
		InterfacePtr<IWidgetParent> findParentOfPrimaryResourcePanelWidget(primaryResourcePanelWidgetPanelControlData,UseDefaultIID());
		if(findParentOfPrimaryResourcePanelWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7CreateMedia::Update::findParentOfPrimaryResourcePanelWidget == nil");
			break;
		}
		IPMUnknown *  cellPanelWidgetUnKnown = findParentOfPrimaryResourcePanelWidget->GetParent();
		if(cellPanelWidgetUnKnown == nil)
		{
			ptrIAppFramework->LogDebug("AP7CreateMedia::Update::cellPanelWidgetUnKnown == nil");
			break;
		}
		InterfacePtr<IPanelControlData>cellPanelWidgetPanelControlData(cellPanelWidgetUnKnown,UseDefaultIID());
		if(cellPanelWidgetPanelControlData == nil)
		{
			ptrIAppFramework->LogDebug("AP7CreateMedia::Update::cellPanelWidgetPanelControlData == nil");
			break;
		}
		//MediatorClass :: iPanelCntrlDataPtr = cellPanelWidgetPanelControlData;/*19-jan*/
		int32 listBoxRowIndex = cellPanelWidgetPanelControlData->GetIndex(primaryResourcePanelWidgetControlView);
		/*PMString ss;
		ss.AppendNumber(listBoxRowIndex);	
		CA(ss);*/
		
		//////////	Added By Amit
		InterfacePtr<IControlView> controlView(this, UseDefaultIID());
		if (controlView == nil)
		{  
			//CA("controlView == nil");
			break;	
		}
		WidgetID theSelectedWidget = controlView->GetWidgetID();
		///////////	End

		bool16 isPressedIn = controlData->IsSelected();
		if(theSelectedWidget==kWhiteStarButtonWidgetID)
		{
			if(isPressedIn == kTrue) 
			{
			//	CA("already Selected going to unselect");
				controlData->Deselect();	
				rsrcID = fontSpec->GetFontID();
				///////////////////////		Added By Amit	
			//	CA("kWhiteStarButtonWidgetID");
				pNodeDataList[listBoxRowIndex].setIsStarred(kFalse);
				//ptrIAppFramework->UpdateStarFlagforPbObject(CurrentSelectedSubSection, pNodeDataList[listBoxRowIndex].getPubId(), false);
				/////////////	End
			}
			else
			{
		//		CA("already Unselected going for selection ");
				controlData->Select();
				rsrcID =fontSpec->GetHiliteFontID();
				////////////////	Added By Amit
			//	CA("kWhiteStarButtonWidgetID");
				pNodeDataList[listBoxRowIndex].setIsStarred(kTrue);
				//ptrIAppFramework->UpdateStarFlagforPbObject(CurrentSelectedSubSection, pNodeDataList[listBoxRowIndex].getPubId(), true);
				/////////////	End
			}
			if(rsrcID != 0)
			{
				controlView->SetRsrcID(rsrcID);
			}
		}
		else if(theSelectedWidget==kNewWhitePictureButtonWidgetID)
		{
			if(isPressedIn == kTrue) 
			{
			//	CA("already Selected going to unselect");
				controlData->Deselect();	
				rsrcID = fontSpec->GetFontID();
				///////////////////////		Added By Amit	
			//	CA("New Product");
				pNodeDataList[listBoxRowIndex].setNewProduct(0);
				//ptrIAppFramework->UpdateNewFlagforPbObject(CurrentSelectedSubSection, pNodeDataList[listBoxRowIndex].getPubId(), false);
				/////////////	End
			}
			else
			{
				//CA("already Unselected going for selection ");
				controlData->Select();
				rsrcID =fontSpec->GetHiliteFontID();
				////////////////	Added By Amit
			//	CA("New Product");
				pNodeDataList[listBoxRowIndex].setNewProduct(1);
				//ptrIAppFramework->UpdateNewFlagforPbObject(CurrentSelectedSubSection, pNodeDataList[listBoxRowIndex].getPubId(), true);
				/////////////	End		
			}		
			if(rsrcID != 0)
			{
				controlView->SetRsrcID(rsrcID);
			}
		}
		else if(theSelectedWidget==kWhiteStarButtonThumbWidgetID)
		{//CA("theSelectedWidget==kWhiteStarButtonWidgetID");
			if(isPressedIn == kTrue) 
			{
				//CA("already Selected going to unselect");
				controlData->Deselect();	
				rsrcID = fontSpec->GetFontID();
				///////////////////////		Added By Amit	
			//	CA("kWhiteStarButtonWidgetID");
				pNodeDataList[curWidgetIndex].setIsStarred(kFalse);
				//ptrIAppFramework->UpdateStarFlagforPbObject(CurrentSelectedSubSection, pNodeDataList[curWidgetIndex].getPubId(), false);
				/////////////	End
			}
			else
			{
				//CA("already Unselected going for selection ");
				controlData->Select();
				rsrcID =fontSpec->GetHiliteFontID();
				////////////////	Added By Amit
			//	CA("kWhiteStarButtonWidgetID");
				pNodeDataList[curWidgetIndex].setIsStarred(kTrue);
				//ptrIAppFramework->UpdateStarFlagforPbObject(CurrentSelectedSubSection, pNodeDataList[curWidgetIndex].getPubId(), true);
				/////////////	End
			}
			if(rsrcID != 0)
			{
				controlView->SetRsrcID(rsrcID);
			}
		}
		else if(theSelectedWidget==kNewWhitePictureButtonThumbWidgetID)
		{
			if(isPressedIn == kTrue) 
			{
			//	CA("already Selected going to unselect");
				controlData->Deselect();	
				rsrcID = fontSpec->GetFontID();
				///////////////////////		Added By Amit	
			//	CA("New Product");
				pNodeDataList[curWidgetIndex].setNewProduct(0);
				//ptrIAppFramework->UpdateNewFlagforPbObject(CurrentSelectedSubSection, pNodeDataList[curWidgetIndex].getPubId(), false);
				/////////////	End
			}
			else
			{
				//CA("already Unselected going for selection ");
				controlData->Select();
				rsrcID =fontSpec->GetHiliteFontID();
				////////////////	Added By Amit
			//	CA("New Product");
				pNodeDataList[curWidgetIndex].setNewProduct(1);
				//ptrIAppFramework->UpdateNewFlagforPbObject(CurrentSelectedSubSection, pNodeDataList[curWidgetIndex].getPubId(), true);
				/////////////	End		
			}		
			if(rsrcID != 0)
			{
				controlView->SetRsrcID(rsrcID);
			}
		}
		return kTrue;
		// early return as here we have already
		// handled the event and do not need to
		// delegate any processing to the base class.
	} while(0);

	return CEventHandler::LButtonDn(e);
}



