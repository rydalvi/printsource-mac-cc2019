#include "VCPluginHeaders.h"
#include "MediatorClass.h"
#include "PicklistValue.h"



double Mediator::sectionID = -1 ;
double Mediator::parentTypeID = -1 ;
double Mediator::parentID = -1 ;
double Mediator::languageID = -1 ;

PMString Mediator::imageType;
UIDRef Mediator::ImageBoxUIDRef;
IListBoxController* Mediator::imageListBoxController = nil;

IPanelControlData* Mediator::panelControlData1 = nil;
IPanelControlData*	Mediator::mainPanelCntrlData=nil;
IPanelControlData*	Mediator::mainPanelCntrlData1=nil;

IControlView* Mediator::view=nil;
IControlView* Mediator::ListBoxView=nil;
IControlView* Mediator::IconButtonView=nil;
IControlView* Mediator::IconTemplateButtonView=nil;
IControlView* Mediator::RefreshButtonView=nil;
IControlView* Mediator::SprayButtonView=nil;
IControlView* Mediator::SubSecSprayButtonView=nil;
IControlView* Mediator::PreviewButtonView=nil;
IControlView* Mediator::DropDownView=nil;
IControlView* Mediator::AssignBtnView=nil;
IControlView* Mediator::DesignerMilestoneView=nil;
IControlView* Mediator::previewwidgetview = nil;

IControlView* Mediator::GreenFilterBtnView=nil;
IControlView * Mediator ::locatewidgetview = nil;
IControlView* Mediator::SectionDropDownLevel3View= nil;	
IControlView* Mediator::SubSectionDropDownLevel3View= nil;	


IControlView* Mediator::showPubTreeView;
IControlView* Mediator::searchView;

PMString Mediator::toolTipText;
PMString Mediator::CurSecName("");
PMString Mediator::CurSubSecName("");

double Mediator::SelectedPubId=-1;
double Mediator::SelectedPublicationRoot=-1;
double Mediator::CurrSectionID =-1;
double Mediator::CurrSubSectionID=-1;
double Mediator::CurrentObjectID=-1;
int32 Mediator::CurrentSelectedRow=-1;
bool16 Mediator:: loadDataFlag = kFalse;
double Mediator::PBObjectID = -1;
int32 Mediator::ResizeFlag = 0;

bool16 Mediator::isDesignerMilestoneAvailable=kFalse;
K2Vector<double> Mediator::vMultipleSelection ;//= nil;

vector<PicklistValue> Mediator::CustomerVector;
vector<PicklistValue> Mediator::DivisionVector;

int32 Mediator::CurrentSubSectionRow=-1;
int32 Mediator::CustDropDownListSelectedIndex=-1;
int32 Mediator::DivDropDownListSelectedIndex=-1;

int32 Mediator::testFlag = 0;
int32 Mediator::imageIndex =0;

bool16 Mediator::isMultipleSelection = kFalse;

IControlView* Mediator::tableSourceBtnCntViw = nil;	//----
IControlView* Mediator::whiteBoardBtnCntViw = nil;	
int32 Mediator::FuncCalledCount = 0;