#ifndef __SPPRIMAGEHELPER_H__
#define __SPPRIMAGEHELPER_H__

#include "IPMUnknown.h"
#include "SPID.h"

class ISPPRImageHelper : public IPMUnknown
{
public:
		enum	{kDefaultIID = IID_ISPPRODUCTIMAGEIFACE};
		//IPRImageHelper(){};
		virtual void showImagePanel() = 0 ;
		virtual void showProductImages(int32 objectID , int32 langID , int32 parentID , int32 parentTypeID , int32 sectionID , PMString name ) = 0 ;
		virtual void positionImagePanel(int32 left , int32 righ , int32 top , int32 bottom) = 0 ;
		virtual void closeImagePanel() = 0 ;
		virtual void showItemImages(int32 objectID , int32 langID , int32 parentID , int32 parentTypeID , int32 sectionID, int32 isProduct , PMString name) = 0 ;
		virtual bool16 isImagePanelOpen() = 0 ;
		virtual bool16 EmptyImageGrid() = 0;
		virtual void WrapperForAddThumbnailImage(PMString wrpperPath, PMString wrpperDisplayName) = 0 ;
		
};


#endif