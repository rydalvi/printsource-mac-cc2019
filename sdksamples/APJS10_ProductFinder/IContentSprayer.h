#ifndef __IContentSprayer__
#define __IContentSprayer__

#include "IPMUnknown.h"
#include "SPID.h"
#include "IAppFramework.h"

class IContentSprayer : public IPMUnknown
{
    public:
		enum { kDefaultIID = IID_ICONTENTSPRAYER };
		//virtual void isClass()=0;
		virtual bool16 setClassNameAndClassID(const double classID, const PMString className , bool16 checkONEsource, vector<CPubModel>* pCPubModelVec = NULL ,int32 isCheckCount = 0, ItemProductCountMapPtr temProductSectionCountPtr = NULL)=0;
		virtual void fillListForSearch(const VectorObjectInfoPtr vectorObjectValuePtr, const VectorItemModelPtr vectorItemModelPtr)=0;
		virtual void openContentSprayerPanelFromRefreshContent(double sectionID,double langID) = 0;
		virtual void closeContentSprayerPanelFromRefreshContent(double sectionID,double langID) = 0;
};

#endif