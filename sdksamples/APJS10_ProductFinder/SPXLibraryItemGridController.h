
#pragma once
#ifndef __SPXLibraryItemGridController__
#define __SPXLibraryItemGridController__


class IControlView;
typedef uint16 LocalMenuID;

#include "ISPXLibraryViewController.h"

//========================================================================================
// CLASS ILibraryThumbnailGridController
//========================================================================================

class SPXLibraryItemGridController : public ISPXLibraryViewController
{
public:
	SPXLibraryItemGridController(IPMUnknown *boss);
	virtual ~SPXLibraryItemGridController();
		
//	virtual IControlView*				CreateViewItemWidget(ILibraryAssetProxy* assetProxy);
	virtual IControlView*				CreateViewItemWidget(PMString& useName);
										// Create a new view item widget
	
	virtual void					InsertViewItemWidget(IControlView* viewWidget );
										// Inserts the specified widget into the view
	
	virtual void					InsertViewItemWidgetAndUpdate(IControlView* viewWidget,
											bool16 autoSelect, bool16 autoScroll,const PMString &path);
										// Inserts the specified widget into the view and
										// updates the display
	
	virtual void					DeleteViewItemWidget(IControlView* viewWidget);
	virtual void					DeleteViewItemWidget(int32 position);
										// Deletes the specified widget from the view
	
	virtual void					DeleteViewItemWidgetAndUpdate(IControlView* viewWidget);
	virtual void					DeleteViewItemWidgetAndUpdate(int32 position);
										// Deletes the specified widget from the view and
										// updates the display
	
	virtual void					ViewItemWidgetChanged(IControlView* viewWidget,
											bool16 autoScroll);
	virtual void					ViewItemWidgetChanged(int32 position,
											bool16 autoScroll);
										// Resorts the grid based on the changed widget
	
	virtual void					SelectViewItemWidget(IControlView* viewWidget);
	virtual void					DeselectViewItemWidget(IControlView* viewWidget);
										// Select/Deselect the specified widget in the view
										
	virtual void					ScrollIntoView(IControlView* viewWidget);
										// Scroll the view to show the specified widget
	
	virtual int32					GetScrollValue();
	virtual void					SetScrollValue(int32 value);
										// Get and set grid view Y scroll bar values
										
	virtual void					UpdateView();
										// Forces an update of the view (redraw and scroll bar adjustment) 
	
	virtual void					SizeChanged();
										// Notify the Controller that the view size changed
											
	virtual void					SetupScrollBarConnections();
	virtual void					ReleaseScrollBar();
										
	virtual void					SetViewType( ViewType viewMethod );
										// Change the view method tothe new type
	
	virtual ViewType				GetViewType() const {return fViewType;}
										// Return the current view method
																									
	virtual void					SetSortOrder( SortType sortMethod );
										// Change the sort method tothe new type
																
	virtual SortType				GetSortOrder() const {return kSortByName;}
										// Return the current sort method

	virtual void					Select(int32 index, bool16 invalidate, bool16 notifyOnChange);
	virtual void					SelectAll(bool16 invalidate, bool16 notifyOnChange);
	virtual void 					Deselect(int32 index, bool16 invalidate, bool16 notifyOnChange );
	virtual void					DeselectAll(bool16 invalidate, bool16 notifyOnChange);
	
	virtual bool16					IsSelected(int32 index) const;
	virtual int32					GetSelected() const;
	virtual void					GetSelected(K2Vector<int32>& multipleSelection) const;

protected:		
	void							UpdateSelectionObservers();
	virtual void					UpdateGridDimensions();
	virtual void					UpdateGridLayout();
	void							SortItems();
	void							Redraw();
	void							UpdateScrollBars();
	IControlView*					GetViewCenterViewItemWidget();
	
	ViewType						fViewType;
	IScrollBarPanoramaSync*			fScrollSync;
	
	DECLARE_HELPER_METHODS()
};

#endif