//========================================================================================
//  
//  $File: $
//  
//  Owner: Sachin Sharma
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"


#include "IPreflightProfile.h"
#include "IPreflightRuleData.h"
#include "IPreflightRuleUI.h"
#include "CPMUnknown.h"
#include "SPPreflightConsts.h"
#include "SPID.h"

/**
    
    @ingroup ap46_productfinder
*/
class SPPreflightRuleUI : CPMUnknown<IPreflightRuleUI>
{
private:

public:
    SPPreflightRuleUI(IPMUnknown* iBoss)
        : CPMUnknown<IPreflightRuleUI>(iBoss) {}

	/** See IPreflightRuleUI::HasUI
	*/
	virtual bool HasUI() const {return kFalse;}

	/** See IPreflightRuleUI::CreateCustomUI
	*/
    virtual IControlView* CreateCustomUI (IDataBase* iUIDataBase) const;

	/** See IPreflightRuleUI::ApplyDataToCustomUI
	*/
    virtual bool ApplyDataToCustomUI (IControlView* iCustomUI, IPreflightRuleData* iRuleDataToApply) const;

 	/** See IPreflightRuleUI::GetDataFromCustomUI
	*/
   virtual bool GetDataFromCustomUI (IControlView* iCustomUI, IPreflightRuleData* iRuleData) const;

	/** See IPreflightRuleUI::GetDataParemeterDescription
	*/
    virtual PMString GetDataParemeterDescription (const IPreflightRuleData::Key& key) const;

	/** See IPreflightRuleUI::UnitType
	*/
    virtual IPreflightRuleUI::UnitType GetDataParemeterUnitType (const IPreflightRuleData::Key& key) const;

	/** See IPreflightRuleUI::GetDataParemeterRange
	*/
    virtual bool GetDataParemeterRange (const IPreflightRuleData::Key& key, ScriptData& maxData, ScriptData& minData) const;

	/** See IPreflightRuleUI::ValidateParameterData
	*/
    virtual bool ValidateParameterData (const IPreflightRuleData::Key& key, const ScriptData& newData, PMString& errorString) const;

	/** See IPreflightRuleUI::GetDataParemeterMapping
	*/
    virtual bool GetDataParemeterMapping (const IPreflightRuleData::Key& key, ScriptListData& enumData, ScriptListData& stringData) const;

	/** See IPreflightRuleUI::GetDataParamRoot
	*/
    virtual IPreflightRuleData::Key GetDataParamRoot () const;

	/** See IPreflightRuleUI::GetDataParamParent
	*/
    virtual void GetDataParamParent (const IPreflightRuleData::Key& key, IPreflightRuleData::Key& keyParent) const;
	
	/** See IPreflightRuleUI::GetDataParamDirectChildren
	*/
    virtual bool GetDataParamDirectChildren (const IPreflightRuleData::Key& key, std::vector<IPreflightRuleData::Key> & keyList) const;
	
	/** See IPreflightRuleUI::GetSubpartName
	*/
	virtual PMString GetSubpartName(ClassID subpartID) const;
};

CREATE_PMINTERFACE(SPPreflightRuleUI, kSPPreflightRuleUIImpl);


IControlView* SPPreflightRuleUI::CreateCustomUI (IDataBase* iUIDataBase) const
{
	// In most cases, auto generated UI is good enough, but you can supply your own UI.  In order to create the custom UI, you will have to create the usual
	// panel for it in your .fr file, then you initialize it using the IPrefightRuleData on the same rule boss as this interface to set up the
	// widgets.  e.g.
	/*
        IControlView*    iPanel = static_cast<IControlView *>(::CreateObject (
                                        iUIDataBase,
                                        RsrcSpec(LocaleSetting::GetLocale(), kPackageAndPreflightUIPlugInID, kViewRsrcType, kProfileDlgLinksRulePanelRsrcID),
                                        IID_ICONTROLVIEW));
        if (iPanel != nil)
        {
            InterfacePtr<IPreflightRuleData> iRuleData (this, UseDefaultIID ());
            InterfacePtr<IPanelControlData>    iPanelData (iPanel, UseDefaultIID ());

            bool16        doMissingLinks = GetDataParam (iRuleData, "missing");
            bool16        doModifiedLinks = GetDataParam (iRuleData, "modified");;

            SetCheckbox (iPanelData, kProfileDlgRuleMissingLinkCheckBoxWID, doMissingLinks);
            SetCheckbox (iPanelData, kProfileDlgRuleModifiedLinkCheckBoxWID, doModifiedLinks);
        }

        return iPanel;
  	
	*/
	return nil;
}

bool SPPreflightRuleUI::ApplyDataToCustomUI (IControlView* iCustomUI, IPreflightRuleData* iRuleDataToApply) const
{
	// This should be similiar to CreateCustomUI, except you set the widget values on the custom UI based on the IPreflightRuleData parameter passed in to you.
	/*
		bool	handled = kFalse;
        IControlView*    iLinksRuleUI = nil;;
        InterfacePtr<IPanelControlData>    iPanelData (iCustomUI, UseDefaultIID ());
        if (iCustomUI != nil && iPanelData != nil &&
            iCustomUI->GetWidgetID () != kProfileDlgRuleLinksRulePanelWID)
        {
            iLinksRuleUI = iPanelData->FindWidget (kProfileDlgRuleLinksRulePanelWID);
        }
        else if (iCustomUI != nil)
        {
            iLinksRuleUI = iCustomUI;
        }

        if (iLinksRuleUI != nil)
        {
            handled = kTrue;

            bool16        doMissingLinks = GetDataParam (iRuleDataToApply, "missing");
            bool16        doModifiedLinks = GetDataParam (iRuleDataToApply, "modified");;

            InterfacePtr<IPanelControlData>    iiLinksRulePanelData (iLinksRuleUI, UseDefaultIID ());
            SetCheckbox (iPanelData, kProfileDlgRuleMissingLinkCheckBoxWID, doMissingLinks);
            SetCheckbox (iPanelData, kProfileDlgRuleModifiedLinkCheckBoxWID, doModifiedLinks);
        }

        return handled;	
	*/
	return kFalse;
}

bool SPPreflightRuleUI::GetDataFromCustomUI (IControlView* iCustomUI, IPreflightRuleData* iRuleData) const
{
	return kFalse;
}

PMString SPPreflightRuleUI::GetDataParemeterDescription (const IPreflightRuleData::Key& key) const
{
	if (key == kSPRuleParameter)
		return PMString (kSPRuleParameterLabel);
	else
	{
		PMString	keyString (key);
		ASSERT_FAIL (FORMAT_ARGS ("Unrecognized key (%s)", keyString.GetPlatformString ().c_str ()));
		return keyString;
	}
}

IPreflightRuleUI::UnitType SPPreflightRuleUI::GetDataParemeterUnitType (const IPreflightRuleData::Key& key) const
{
    return IPreflightRuleUI::kDefaultUnit;
}

bool SPPreflightRuleUI::GetDataParemeterRange (const IPreflightRuleData::Key& key, ScriptData& maxData, ScriptData& minData) const
{
	return kFalse;
}

bool SPPreflightRuleUI::ValidateParameterData (const IPreflightRuleData::Key& key, const ScriptData& newData, PMString& errorString) const
{
	return kFalse;
}

bool SPPreflightRuleUI::GetDataParemeterMapping (const IPreflightRuleData::Key& key, ScriptListData& enumData, ScriptListData& stringData) const
{
	return kFalse;
}

IPreflightRuleData::Key SPPreflightRuleUI::GetDataParamRoot () const
{
    IPreflightRuleData::Key    rootKey ("");
    return rootKey;
}

void SPPreflightRuleUI::GetDataParamParent (const IPreflightRuleData::Key& key, IPreflightRuleData::Key& keyParent) const
{
	keyParent = GetDataParamRoot ();
}
bool SPPreflightRuleUI::GetDataParamDirectChildren (const IPreflightRuleData::Key& key, std::vector<IPreflightRuleData::Key> & keyList) const
{
	if (key == GetDataParamRoot ())
	{
		InterfacePtr<IPreflightRuleData>	iRuleData (this, UseDefaultIID ());
		iRuleData->GetAllKeys (keyList);
		return keyList.size () > 0;
	}
	else
	{
		return kFalse;
	}
}

PMString SPPreflightRuleUI::GetSubpartName(ClassID subpartID) const
{
	return PMString();
}



//  Code generated by DollyXs code generator
