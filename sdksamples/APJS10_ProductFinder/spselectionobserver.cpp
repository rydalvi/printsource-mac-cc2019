#include "VCPluginHeaders.h"
#include "ISelectionManager.h"
#include "SelectionObserver.h"
#include "SPID.h"
#include "Trace.h"
#include "MediatorClass.h"
#include "CAlert.h"
#include "IWidgetParent.h"
#include "ISubject.h"
#include "ITriStateControlData.h"
#include "IClientOptions.h"
#include "ITextControlData.h"
#include "IAppFramework.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "IDialogController.h"
#include "ITreeViewMgr.h"
#include "DragDropHelper.h"
#include "ISubSectionSprayer.h"
#include "SubSectionSprayer.h"
#include "SPSelectionObserver.h"
#include "SDKListBoxHelper.h"
#include "IPanelMgr.h"
#include "IDocument.h"
#include "SPActionComponent.h"
#include "IDialogController.h"
#include "SectionData.h"
#include "PublicationNode.h"
#include "GlobalData.h"
#include "SPActionComponent.h"
#include "ISelectionUtils.h"
#include "ProductSpray.h"
#include "IListControlData.h"
#include "IListBoxController.h"
#include "ILayoutUIUtils.h"
#include "ISpecialChar.h"
#include "vector"
#include "iterator"
#include "PRImageHelper.h"
#include "AcquireModalCursor.h"
#include "ILayoutSelectionSuite.h"
#include "IStrokeAttributeSuite.h"
#include "FileUtils.h"
#include "IWindow.h"
#include "IDataBase.h"
#include "PaletteRef.h"
#include "PaletteRefUtils.h"
#include "ICategoryBrowser.h"
#include "ISPXLibraryButtonData.h"
#include "LayerPanelID.h"
#include "ILayerList.h"
#include "IDocumentLayer.h"
#include "INewLayerCmdData.h"
#include "ISpreadLayer.h"
#include "ISpread.h"
#include "ITransformFacade.h"
#include "IPathUtils.h"
#include "Utils.h"
#include "INewPageItemCmdData.h"
#include "SDKLayoutHelper.h"
#include "SnpGraphicHelper.h"
#include "InstStrokeFillID.h"
#include "ISpreadList.h"
#include "IPageList.h"
#include "ICopyCmdData.h"
#include "IClipboardController.h"
#include "ITableUtils.h"
#include "IResizeItemsCmdData.h"
#include "IBoundsData.h"
#include "IPageItemUtils.h"
#include "IActiveContext.h"
#include "ITableModelList.h"
#include "ITextFrameColumn.h"
#include "IReferencePointData.h"
#include "IRefPointUtils.h"
#include "TSTableSourceHelper.h"
#include "ILoginHelper.h"
#include "IImportExportFacade.h"
#include "IScrapSuite.h"
#include "MilestoneModel.h"
#include "XMLReference.h"
#include "IXMLElementCommands.h"
#include "IXMLUtils.h"
#include "IXMLReferenceData.h"
#include "IIDXMLElement.h"
#include "IHierarchy.h"
#include "IBoolData.h"
#include "IGraphicAttributeUtils.h"
#include "ISwatchList.h"
#include "ISwatchUtils.h"
#include "IFrameUtils.h"
#include "ITextMiscellanySuite.h"
#include "TransformUtils.h"
#include "SDKUtilities.h"
#include "SPTreeModel.h"
#include "SPTreeDataCache.h"
#include "ITreeViewController.h"
#include "K2Vector.h"
#include "K2Vector.tpp" 
#include "NodeID.h"
#include "IntNodeID.h"
#include "SPTreeDataCache.h"
#include "StringUtils.h"
// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"

#define CA(x)	CAlert::InformationAlert(x)
#define YP(x) CAlert::InformationAlert(x)
#define CA_NUM(i,Message,NUM) PMString K##i(Message);K##i.AppendNumber(NUM);CA(K##i)

double CurrentSelectedSection = -1;
double CurrentSelectedPublicationID= -1;
double CurrentSelectedSubSection = -1;
int32 CurrentSelectedProductRow = -100;
//extern PublicationNode pNodeDataList;
//extern int32 Flag1;
int32 noOfRows = 0;
int32 check=0,flg=0;
int32 NewFlgForMinmise=0;
int32 selectrowForRefresh;
extern int32 Flag1;
extern SPTreeDataCache dc;

//extern vector<bool8> GlobalDesignerActionFlags;
//extern bool8 milestone_selected;
//extern bool8 ShowAllProductsRadioTriStateFlag;
//extern bool8 FilterByDesignerActionsRadioTriStateFlag;
//extern bool8 FilterByMilestonesRadioTriStateFlag;
extern int32 selecteduserID,selectedUserRow,selectedroleID,selectedRoleRow;
extern bool8 completeRadioSelected;
extern int32 ListFlag; // Global Flag when ListFlag = 0 select All, 1 = Products Only, 2 = Items Only. 

bool8 IsNewSectionSelected;
bool8 IsTextFrameStrokeChanged = kFalse;
IStrokeAttributeSuite* tempstrokeAttributeSuite = nil;
UIDList  tempitemList;
VectorPubModel global_vector_pubmodel ;
VectorPubModel global_vector_pubmodel_for_subsection ;
int32 global_project_level ;
double global_lang_id ;
double global_publication_id ;
double global_subsection_id ;
bool16 RefreshFlag = kFalse;
bool16 isONEsource = kFalse; //It is used when switching between Onesource publication and rest of other
//extern int32 flag;//added by Tushar on 11 Oct for filter case
extern double global_classID;
extern bool16 isItemHorizontalFlow;
extern bool16 isThumb;
//extern K2Vector<PMString> imageVector2 ;
VectorPubObjectValuePtr ptrForThumbnail = NULL;
extern K2Vector<double> multipleSelectionForThumb;
VectorPubObjectValue vectorForThumbNail;

//VectorAPpubCommentValue global_VectorAPpubCommentValue;//all comments are stored in this.
extern bool16 searchResult;
int32 objIndex = 0;
//----------
bool16 isSprayItemPerFrameFlag = kFalse;
bool16 isItemHorizontalFlow = kFalse;

int32 checkTreeType=0;
//extern bool16 LeadingItemFlag ;

PublicationNodeList orignalpNodeDataList;
int32 firstpNodeListCount = 0;

extern bool16 LeadingItemFlag ;
extern bool16 isCancelButtonClick;

bool16 singleSelectionSpray = kFalse;

extern bool8 FilterFlag;
extern double GCurrentSelectedSection;
extern int32 NewFlag;
extern SPTreeDataCache dc;

namespace
{
	GSysRect getBoundingBoxOfWindow()
	{
		do
		{
			InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
			if(iApplication==nil)
			{ 
				//CA("iApplication == nil");
				break;
			}
			InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager());
			if(iPanelMgr == nil)
			{ 
				//CA("iPanelMgr == nil");
				break ;
			}

			UID paletteUID = kInvalidUID;
			const ActionID MyPalleteActionID = kSPPanelWidgetActionID;

            IControlView* panelView = NULL;
			panelView = iPanelMgr->GetPanelFromActionID(MyPalleteActionID);
			if(!panelView)
			{
				//CA("panelView is nil");
			}

			int32 TemplateTop	=0;	
			int32 TemplateLeft	=0;	
			int32 TemplateRight	=0;	
			int32 TemplateBottom =0;
            
			PaletteRef palRef = iPanelMgr->GetPaletteRefContainingPanel(panelView );
			if(!palRef.IsValid())
			{
				//CA("palRef is invalid");
				break;
			}
			PaletteRef parentRef = PaletteRefUtils::GetParentOfPalette( palRef ); 
			if(!parentRef.IsValid())
			{
				//CA("parentRef is invalid");
				break;
			}

			PaletteRef parentparentRef = PaletteRefUtils::GetParentOfPalette( parentRef ); 
			if(!parentparentRef.IsValid())
			{
				//CA("parentparentRef is invalid");
				break;
			}
            bool16 retval = PaletteRefUtils::IsPaletteVisible(parentparentRef);
            if(retval)
            {
                SysRect PalleteBounds = PaletteRefUtils::GetPaletteBounds(parentparentRef);
                #ifdef WINDOWS
					
                TemplateTop		= PalleteBounds.top;
                TemplateLeft	= PalleteBounds.left;
                TemplateRight	= PalleteBounds.right;
                TemplateBottom	= PalleteBounds.bottom;
                #else
					
                TemplateTop = SysRectTop(PalleteBounds);
                TemplateLeft = SysRectLeft(PalleteBounds);
                TemplateRight = SysRectRight(PalleteBounds);
                TemplateBottom =SysRectBottom(PalleteBounds);
                
                #endif
                return PalleteBounds ;
            }
				
		}
		while(kFalse);

		GSysRect PalleteBounds1 ;
		return PalleteBounds1 ;
	}
}

K2Vector<PMString>imageVector1;

K2Vector<PMString>imageType;
K2Vector<double>imageTypeID;

//ImageVector imageVector;


CREATE_PMINTERFACE(SPSelectionObserver, kSPSelectionObserverImpl);

SPSelectionObserver::SPSelectionObserver(IPMUnknown *boss) :
	ActiveSelectionObserver(boss){}

SPSelectionObserver::~SPSelectionObserver(){}

void SPSelectionObserver::AutoAttach()
{
	//flag = 0;	
	static int32 i;
	ActiveSelectionObserver::AutoAttach();

	flg=0;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	return;

	Mediator md;
	md.setLoadDataFlag(kTrue);
	bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();

	check = 1;

	InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
	if(iPanelControlData==nil)
	{	
		//CA("iPanelControlData==nil");
		ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::AutoAttach::No iPanelControlData");
		return;
	}
	else
	{
		md.setMainPanelCntrlData(iPanelControlData);
		AttachWidget(iPanelControlData, kSPSubSectionSprayButtonWidgetID, IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData, kSPSectionDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
		AttachWidget(iPanelControlData, kSPSprayButtonWidgetID, IID_ITRISTATECONTROLDATA);
		//AttachWidget(iPanelControlData, kAssignButtonWidgetID, IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData, kSPSectionDropDown_1WidgetID, IID_ISTRINGLISTCONTROLDATA);
		AttachWidget(iPanelControlData, kSPSetDesignerMilestoneWidgetID,IID_ITRISTATECONTROLDATA); //IID_IBOOLEANCONTROLDATA);
		AttachWidget(iPanelControlData,	kPRLPreviewButtonWidgetID, IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData, kGreenFilterButtonWidgetID,IID_ITRISTATECONTROLDATA);
	//	AttachWidget(md.getMainPanelCtrlData(), kClearButtonWidgetID, IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData, kSPShowWhiteBoardWidgetID, IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData, kSPRefreshWidgetID, IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData, kSPShowPubTreeWidgetID, IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData, kSPSubSectionDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
		AttachWidget(iPanelControlData,	kSPSelectClassWidgetID, IID_ITRISTATECONTROLDATA);
		//AttachWidget(iPanelControlData,	kSPViewWhiteBoardWidgetID, IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData,	kSPRevertSearchWidgetID, IID_ITRISTATECONTROLDATA);

//		AttachWidget(iPanelControlData, kHorizontalFlowCheckBoxWidgetID,IID_ITRISTATECONTROLDATA); //-------
//		AttachWidget(iPanelControlData, kSprayItemPerFrameCheckBoxWidgetID,IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData, kNewSprayItemPerFrameCheckBoxWidgetID,IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData, kNewHorizontalFlowCheckBoxWidgetID,IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData, kSPShowTableSourceWidgetID, IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData, kSPSelectCustomerWidgetID, IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData, kSPsearchWidgetID, IID_ITRISTATECONTROLDATA);

		//AttachWidget(iPanelControlData, kSPTemplatePreviewWidgetID, IID_ITRISTATECONTROLDATA);
	}

	if(!isUserLoggedIn)
	{
		if(i==0)
		{			
			check=1;
		}
		i++;		
		check = 1;		

		do{

			IControlView* subSectionDropListCtrlView=iPanelControlData->FindWidget(kSPSectionDropDownWidgetID);
			if(subSectionDropListCtrlView==nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::AutoAttach::No subSectionDropListCtrlView");				
				break;
			}

			InterfacePtr<IDropDownListController> subSectionDropListCntrler(subSectionDropListCtrlView, UseDefaultIID());
			if(subSectionDropListCntrler==nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::AutoAttach::No subSectionDropListCntrler");			
				break;
			}
	
			InterfacePtr<IStringListControlData> subSectionDropListCtrlData(subSectionDropListCntrler, UseDefaultIID());
			if(subSectionDropListCtrlData==nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::AutoAttach::No subSectionDropListCtrlData");			
				break;
			}
	
			subSectionDropListCtrlData->Clear(kFalse, kFalse);

			IControlView * iView=iPanelControlData->FindWidget(kSPPubNameWidgetID);
			if(!iView)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::AutoAttach::No iView");
				break;
			}

			InterfacePtr<ITextControlData> iData(iView, UseDefaultIID());
			if(iData==nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::AutoAttach::No iData");				
				break;	
			}

			iData->SetString("");


	}while(kFalse);

		loadPaletteData();	
	}
	do
	{
		if(md.getMainPanelCtrlData()!=nil && Flag1==0)
		{
				if(isUserLoggedIn)
				{// CA("Before loadpalletedata ");
					loadPaletteData();
				}

		}
		else
		{
			//check=1;
			Flag1=0;
			loadPaletteData();
		}
	}while(kFalse);

}

void SPSelectionObserver::AutoDetach()
{
	Mediator md;
	
	if(check == 1)
	{	//CA("Makes Check = 0");
		check = 0;
		NewFlgForMinmise = 1;
		md.setLoadDataFlag(kFalse);
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			return;
		bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();

		if(isUserLoggedIn)
		{
			ActiveSelectionObserver::AutoDetach();
			if(!md.getMainPanelCtrlData())
			{
			//	CA("AutoDetach:SelectionObserver:md.getMainPanelControlData is nil");
				return;
			}

			//CA("Detach 0");
			DetachWidget(md.getMainPanelCtrlData(), kSPSectionDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
			DetachWidget(md.getMainPanelCtrlData(), kSPSubSectionDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
			DetachWidget(md.getMainPanelCtrlData(), kSPSprayButtonWidgetID, IID_ITRISTATECONTROLDATA);
			//DetachWidget(md.getMainPanelCtrlData(), kAssignButtonWidgetID, IID_ITRISTATECONTROLDATA);
			DetachWidget(md.getMainPanelCtrlData(), kSPSectionDropDown_1WidgetID, IID_ISTRINGLISTCONTROLDATA);		
			DetachWidget(md.getMainPanelCtrlData(), kSPSetDesignerMilestoneWidgetID,IID_ITRISTATECONTROLDATA); //IID_IBOOLEANCONTROLDATA);
			DetachWidget(md.getMainPanelCtrlData(),	kPRLPreviewButtonWidgetID, IID_ITRISTATECONTROLDATA);		
			DetachWidget(md.getMainPanelCtrlData(), kGreenFilterButtonWidgetID,IID_ITRISTATECONTROLDATA);		
			//DetachWidget(md.getMainPanelCtrlData(), kClearButtonWidgetID, IID_ITRISTATECONTROLDATA);
			DetachWidget(md.getMainPanelCtrlData(), kSPShowWhiteBoardWidgetID, IID_ITRISTATECONTROLDATA);
			DetachWidget(md.getMainPanelCtrlData(), kSPRefreshWidgetID, IID_ITRISTATECONTROLDATA);
			DetachWidget(md.getMainPanelCtrlData(), kSPShowPubTreeWidgetID, IID_ITRISTATECONTROLDATA);
			DetachWidget(md.getMainPanelCtrlData(), kSPSubSectionSprayButtonWidgetID, IID_ITRISTATECONTROLDATA);
			DetachWidget(md.getMainPanelCtrlData(),	kSPSelectClassWidgetID, IID_ITRISTATECONTROLDATA);
		//	DetachWidget(md.getMainPanelCtrlData(),	kSPViewWhiteBoardWidgetID, IID_ITRISTATECONTROLDATA);
			DetachWidget(md.getMainPanelCtrlData(),	kSPRevertSearchWidgetID, IID_ITRISTATECONTROLDATA);
			
//			DetachWidget(md.getMainPanelCtrlData(),kHorizontalFlowCheckBoxWidgetID,IID_ITRISTATECONTROLDATA); //----
//			DetachWidget(md.getMainPanelCtrlData(),kSprayItemPerFrameCheckBoxWidgetID,IID_ITRISTATECONTROLDATA);
			DetachWidget(md.getMainPanelCtrlData(),kNewSprayItemPerFrameCheckBoxWidgetID,IID_ITRISTATECONTROLDATA);
			DetachWidget(md.getMainPanelCtrlData(),kNewHorizontalFlowCheckBoxWidgetID,IID_ITRISTATECONTROLDATA);
			DetachWidget(md.getMainPanelCtrlData(), kSPShowTableSourceWidgetID, IID_ITRISTATECONTROLDATA);
			DetachWidget(md.getMainPanelCtrlData(), kSPSelectCustomerWidgetID, IID_ITRISTATECONTROLDATA);
			DetachWidget(md.getMainPanelCtrlData(),	kSPsearchWidgetID, IID_ITRISTATECONTROLDATA);
			//DetachWidget(md.getMainPanelCtrlData(), kSPTemplatePreviewWidgetID, IID_ITRISTATECONTROLDATA);
			
		}
		return;
	}
	
	do{
		//CA("in else  of detach");
		if(!md.getMainPanelCtrlData())
		{
			//CA("AutoDetach:SelectionObserver:Do:md.getMainPanelControlData is nil");
			break;
		}
		DetachWidget(md.getMainPanelCtrlData(), kSPSectionDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
		DetachWidget(md.getMainPanelCtrlData(), kSPSprayButtonWidgetID, IID_ITRISTATECONTROLDATA);
		//DetachWidget(md.getMainPanelCtrlData(), kAssignButtonWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(md.getMainPanelCtrlData(), kSPSetDesignerMilestoneWidgetID,IID_ITRISTATECONTROLDATA); //IID_IBOOLEANCONTROLDATA);
		DetachWidget(md.getMainPanelCtrlData(),	kPRLPreviewButtonWidgetID, IID_ITRISTATECONTROLDATA);		
		DetachWidget(md.getMainPanelCtrlData(), kGreenFilterButtonWidgetID, IID_ITRISTATECONTROLDATA);		
		//DetachWidget(md.getMainPanelCtrlData(), kClearButtonWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(md.getMainPanelCtrlData(), kSPShowWhiteBoardWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(md.getMainPanelCtrlData(), kSPRefreshWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(md.getMainPanelCtrlData(), kSPShowPubTreeWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(md.getMainPanelCtrlData(), kSPSubSectionSprayButtonWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(md.getMainPanelCtrlData(),	kSPSelectClassWidgetID, IID_ITRISTATECONTROLDATA);
	//	DetachWidget(md.getMainPanelCtrlData(),	kSPViewWhiteBoardWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(md.getMainPanelCtrlData(),	kSPRevertSearchWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(md.getMainPanelCtrlData(),	kSPsearchWidgetID, IID_ITRISTATECONTROLDATA);
		md.setLoadDataFlag(kFalse);

	}while(kFalse);	
}

IPanelControlData* SPSelectionObserver::QueryPanelControlData()
{
	//CA("Inside QueryPanelControlData");
	IPanelControlData* iPanel = nil;
	do
	{
		InterfacePtr<IWidgetParent> iWidgetParent(this, UseDefaultIID()); 
		if (iWidgetParent == nil)
		{
			//PMString str(kSPBlankStringKey);
			//CA(str);
			break;
		}
		//CA("Didnt Break");
		InterfacePtr<IPanelControlData> iPanelControlData(iWidgetParent->GetParent(), UseDefaultIID());
		if (iPanelControlData == nil)
			break;
		iPanelControlData->AddRef();
		iPanel = iPanelControlData;
	}
	while (false); 
	//CA("end of  QueryPanelControlData");
	return iPanel;
}

void SPSelectionObserver::AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil)
			break;
		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil)
			break;
		iSubject->AttachObserver(this, interfaceID);
	}
	while (false); 
}

void SPSelectionObserver::DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil)
			break;
		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil)
			break;
		iSubject->DetachObserver(this, interfaceID);
	}
	while (false); 
}

void SPSelectionObserver::Update
(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy)
{
	//CA("selection observer Update");	
	ActiveSelectionObserver::Update(theChange, theSubject, protocol, changedBy);

	InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
	if(iPanelControlData == nil)
	{
		return;
	}

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return;
	}

	do
	{		
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{  
			//CA("controlView == nil");
			return;	
		}
		WidgetID theSelectedWidget = controlView->GetWidgetID();

		if(theSelectedWidget==kNewSprayItemPerFrameCheckBoxWidgetID && theChange==kTrueStateMessage)
		{
			//CA("SprayItem TRUE 1");
			isSprayItemPerFrameFlag=kTrue;
			IControlView *newHorizontalFlowCheckBoxControlView=iPanelControlData->FindWidget(kNewHorizontalFlowCheckBoxWidgetID);
			if(newHorizontalFlowCheckBoxControlView == nil)
			{
				//CA("newHorizontalFlowCheckBoxControlView == nil");
				return;
			}
			newHorizontalFlowCheckBoxControlView->Enable();
		}
		if(theSelectedWidget==kNewSprayItemPerFrameCheckBoxWidgetID && theChange==kFalseStateMessage)
		{
			//CA("SprayItem False 1");
			isSprayItemPerFrameFlag=kFalse;
			IControlView *newHorizontalFlowCheckBoxControlView=iPanelControlData->FindWidget(kNewHorizontalFlowCheckBoxWidgetID);
			if(newHorizontalFlowCheckBoxControlView == nil)
			{
				//CA("newHorizontalFlowCheckBoxControlView == nil");
				return;
			}
			InterfacePtr<ITriStateControlData> newHorizontalFlowCtrlData (newHorizontalFlowCheckBoxControlView,UseDefaultIID());
			if(newHorizontalFlowCtrlData == nil)
			{
			//	CA("newHorizontalFlowCtrlData == nil");
				return;
			}
			newHorizontalFlowCtrlData->Deselect();
			newHorizontalFlowCheckBoxControlView->Disable();		
		}
		if(theSelectedWidget==kNewHorizontalFlowCheckBoxWidgetID && theChange==kTrueStateMessage)
		{
			//CA("HorizontalFlow True");
			isItemHorizontalFlow=kTrue;
		}
		if(theSelectedWidget==kNewHorizontalFlowCheckBoxWidgetID && theChange==kFalseStateMessage)
		{
			//CA("HorizontalFlow False");
			isItemHorizontalFlow=kFalse;
		}
	
		//----------
		if(theSelectedWidget==kPRLPreviewButtonWidgetID && theChange==kTrueStateMessage)
		{
			//1PreviewFlag = kTrue;
			bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
			if(!isUserLoggedIn)			
				return;
			if(CurrentSelectedProductRow == -100)//-100 is a nonexist entry in the list box.
			{
				break;
			}
			
			//CA(" calling preview method ");
			//Apsiva 9 Comment
			//ptrIAppFramework->UTILITYMngr_getPreviewURL(pNodeDataList[CurrentSelectedProductRow].getIsONEsource(), pNodeDataList[CurrentSelectedProductRow].getIsProduct(), pNodeDataList[CurrentSelectedProductRow].getPubId(), pNodeDataList[CurrentSelectedProductRow].getPBObjectID()); 

		}	
		
		if(theSelectedWidget == kSPSetDesignerMilestoneWidgetID && theChange == kTrueStateMessage)
		{			
			////CA("Set designerActioncomplete clicked!");
			//bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
			//if(!isUserLoggedIn)			
			//	return;
			//if(CurrentSelectedProductRow == -100)
			//{
			//	//CA(" CurrentSelectedProductRow == -100 ");
			//	break;
			//}
			//int16 val = CAlert::ModalAlert("Do you really want to update the Designer Milestone values?","Yes","No",kNullString,1,CAlert::eQuestionIcon);
			//if(val==2)
			//{	
			//	IControlView * listView = listHelper.FindCurrentListBox(iPanelControlData, 1);
			//	if(listView == nil)
			//	break;

			//	InterfacePtr<IListBoxController>iListBoxCntrler(listView,UseDefaultIID());
			//	if(iListBoxCntrler == nil)
			//	break;
			//
			//	iListBoxCntrler->DeselectAll();
			//	//ended on 3 march
			//	Mediator md;
			//	md.getDesignerActionWidgetView()->Disable();
			//	md.vMultipleSelection.clear();
			//	return;
			//}
			//
			//CMilestoneModel  *milestoneModel = NULL;			
			////milestoneModel = ptrIAppFramework->MilestoneCache_getDesignerMilestone();
			//if(milestoneModel == NULL)
			//{
			//	ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::Update::kSPSetDesignerMilestoneWidgetID--No milestoneModel");
			//	break;
			//}			
			//int32 milestoneID = milestoneModel->getId();			
			//Mediator md;
			// //functionality for multiple selection will do later
			//int32 length = (md.vMultipleSelection).Length();
			//PMString len("The length of the selection is ");
			//len.AppendNumber(length);
			////CA(len);
			//
			//bool16 milestonevaluechangedflag=kFalse;
			//bool16 updateMilestoneflag = kFalse;
			//bool16 insertMilestoneflag = kFalse;
			////CA(" can do something ");
			//for(int32 i = 0 ; i < length ; i++)
			//{
			//	CObjectMilestoneValue *objmilestoneval=NULL;
			//	int32 objectID = (pNodeDataList[md.vMultipleSelection[i]]).getPBObjectID();
			//	objmilestoneval = ptrIAppFramework->findCObjectMilestoneByObjectIdMilestoneId(objectID,milestoneID);
			//
			//	//PMString objid(" The object id is = ");
			//	//objid.AppendNumber(objectID);	
			//	//CA(objid);

			//		if(objmilestoneval == nil)
			//		{						
			//			bool16 status = ptrIAppFramework->insertObjectMilestone(objectID,milestoneID);//objmilestoneval
			//			if(status)
			//			{
			//				//CA(" insertObjectMilestone done ");
			//			}
			//			else 
			//			{
			//				//CA(" insertObjectMilestone not done ");
			//			}
			//			//bool16 status1 = ptrIAppFramework->update_reset_add_to_spread(objectID);
			//			milestonevaluechangedflag = kTrue;
			//			//insertMilestoneFlag = kTrue;
			//			//insertMilestoneReport.Append((pNodeDataList[md.vMultipleSelection[i]]).getName());
			//			//updateMilestoneReport.Append((pNodeDataList[md.vMultipleSelection[i]]).getName());
			//			//insertMilestoneReport.Append("\n");
			//			//updateMilestoneReport.Append("\n");
			//		}
			//		else
			//		{						
			//			//bool16 status = ptrIAppFramework->updateObjectMilestone(objectID,milestoneID);
			//			bool16 status = ptrIAppFramework->updateObjectMilestone(*objmilestoneval);
			//			if(status)
			//			{
			//				//CA(" updateObjectMilestone done ");
			//			}
			//			else 
			//			{
			//				//CA(" updateObjectMilestone not done ");
			//			}
			//			//bool16 status1 = ptrIAppFramework->update_reset_add_to_spread(objectID);
			//			milestonevaluechangedflag = kTrue;
			//			//updateMilestoneFlag = kTrue;
			//			//updateMilestoneReport.Append((pNodeDataList[md.vMultipleSelection[i]]).getName());
			//			//updateMilestoneReport.Append("\n");						
			//		}
			//}

			//if(milestonevaluechangedflag)
			//{
			//	/*PMString finalReport("");
			//	finalReport.Append(updateMilestoneReport);
			//	finalReport.Append(insertMilestoneReport);
			//	CAlert::InformationAlert(finalReport);*/
			//}
			////functionality for multiple selection will do later
			//
			////added on 3 march. will deselect the selected list elements
			//IControlView * listView = listHelper.FindCurrentListBox(iPanelControlData, 1);
			//if(listView == nil)
			//{
			//	ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::Update::kSPSetDesignerMilestoneWidgetID---No listView");			
			//	break;
			//}

			//InterfacePtr<IListBoxController>iListBoxCntrler(listView,UseDefaultIID());
			//if(iListBoxCntrler == nil)
			//{
			//	ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::Update::kSPSetDesignerMilestoneWidgetID--No iListBoxCntrler");						
			//	break;
			//}
			//
			//iListBoxCntrler->DeselectAll();
			////ended on 3 march
			//			
			//md.getDesignerActionWidgetView()->Disable();
			//md.vMultipleSelection.clear();
			break;		
		} 
							  	
		if(theSelectedWidget==kSPShowPubTreeWidgetID && theChange==kTrueStateMessage)
		{
			//CA(" show pub clicked  ");
			bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
			if(!isUserLoggedIn)			
				return;

			InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
			if(iApplication==nil)
			{
				//CA("iApplication==nil");
				break;
			}
			InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
			if(iPanelMgr == nil)
			{
				//CA("iPanelMgr == nil");
				break;
			}
			UID paletteUID = kInvalidUID;
			int32 TemplateTop	=0;	
			int32 TemplateLeft	=0;		
			int32 TemplateRight	=0;	
			int32 TemplateBottom =0;	

			const ActionID MyPalleteActionID = kCTBPanelWidgetActionID;

			IControlView* pnlControlView = iPanelMgr->GetPanelFromActionID(MyPalleteActionID);
			if(pnlControlView == NULL)
			{
				//CA("pnlControlView is NULL");
				break;
			}

			InterfacePtr<IWidgetParent> panelWidgetParent( pnlControlView, UseDefaultIID());
			if(panelWidgetParent == NULL)
			{
				//CA("panelWidgetParent is NULL");
				break;
			}
			InterfacePtr<IWindow> palette((IWindow*)panelWidgetParent->QueryParentFor(IWindow::kDefaultIID));
			if(palette == NULL)
			{
				//CA("palette is NULL");
				break;
			}
			
			if(palette)
			{

				TemplateTop	=0;
				TemplateLeft = 0;	
				TemplateRight =150;	
				TemplateBottom	=300;
		
				InterfacePtr<ICategoryBrowser> CatalogBrowserPtr((ICategoryBrowser*)::CreateObject(kCTBCategoryBrowserBoss, IID_ICATEGORYBROWSER));
				if(!CatalogBrowserPtr)
				{
					ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::Update::kSPSelectClassWidgetID--No CatalogBrowserPtr");
					return ;
				}
				CatalogBrowserPtr->OpenCategoryBrowser(TemplateTop, TemplateLeft, TemplateBottom, TemplateRight , 1 , -1);
			}

		}
		/************ Get currently selected subSection and display PF tree accordingly *************/
		if(theSelectedWidget==kSPSectionDropDownWidgetID && theChange==kPopupChangeStateMessage)
		{ // Level 3
		
			//CA(" -- SectionDropDown clicked  Level 3 -- ");
			bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
			if(!isUserLoggedIn)	
			{
				ptrIAppFramework->LogError("!isUserLoggedIn return");
				return;	
			}
			
			CurrentSelectedProductRow = -100;
			Mediator md ;
			IsNewSectionSelected = kTrue;
			PMString as;
			
			int32 CSS=-1;
			CSS = getSelectedSectionId(controlView,as);
			//CA_NUM(789 , " selected section id " , CSS);

			if(!RefreshFlag)
				md.setCurrentSubSectionRow(-1);

			this->fillSubSectionList(CSS);
			
			if(Mediator::ResizeFlag == 1)
				this->hideImagePanel();
				
			CurrentSelectedSection = CSS;
			md.setCurrSectionID(CSS);
			md.setCurSecName(as);
			break ; 		
	
		}
		
		/********************************* Spray Product Data **********************************/
		if(theSelectedWidget==kSPSprayButtonWidgetID && theChange==kTrueStateMessage)
		{
			//CA("theSelectedWidget==kSPSprayButtonWidgetID");
			bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
			if(!isUserLoggedIn)			
				return;
			
			InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
			if(!DataSprayerPtr)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer::Pointre to DataSprayerPtr not found");
				return;
			}
			//CA("theSelectedWidget==kSPSprayButtonWidgetID && theChange==kTrueStateMessage");
			if(CurrentSelectedProductRow == -100)//-100 is taken purposely.-100 does not exists.So something has to be selected from the list box.
			{				
				ptrIAppFramework->LogInfo("AP7_ProductFinder::SPSelectionObserver::Update::kSPSprayButtonWidgetID--CurrentSelectedProductRow == -100");
				break;
			}

			InterfacePtr<ISubSectionSprayer> iSSSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
			if(iSSSprayer==nil)
			{
				ptrIAppFramework->LogInfo("AP7_ProductFinder::SPSelectionObserver::Update::kSPSprayButtonWidgetID::iSSSprayer==nil");
				return;
			}

			K2Vector<double> multipleSelection ;	
			PublicationNodeList temppNodeDataList = pNodeDataList;

			if(firstpNodeListCount == 0)
				orignalpNodeDataList = pNodeDataList;
			else
				temppNodeDataList = orignalpNodeDataList;

			IControlView* iControlView = iPanelControlData->FindWidget(kSPTreeListBoxWidgetID);
			if(iControlView != nil) 
			{
				InterfacePtr<ITreeViewController> treeViewCntrl(iControlView, UseDefaultIID());
				if(treeViewCntrl==nil) 
				{
					//CA("treeViewCntrl==nil");
					ptrIAppFramework->LogError("AP7_ProductFinder::SPSelectionObserver::Update::treeViewCntrl == nil");
					break ;
				}

				NodeIDList selectedItem;
			
				treeViewCntrl->GetSelectedItemsDisplayOrder(selectedItem);
				if(selectedItem.size()<=0)
				{
					//CA("selectedItem.size()<=0");
					ptrIAppFramework->LogInfo("AP7_ProductFinder::SPSelectionObserver::Update::selectedItem.size()<=0");
					break ;
				}

				NodeID nid=selectedItem[0];
				TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);
				int32 uid= uidNodeIDTemp->Get();
				
				const int32 kSelectionLength = selectedItem.size();
				if(kSelectionLength > 1)
				{
					singleSelectionSpray = kFalse;
					Mediator::setIsMultipleSelection(kTrue);

					//CA("kSelectionLength > 1");
					
					pNodeDataList.clear();
					for(int32 count = 0;count < kSelectionLength ; count++)
					{						
						NodeID nid=selectedItem[count];
						TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);
						int32 uid= uidNodeIDTemp->Get();
						PublicationNode pNode;
						bool16 isIdExist = dc.isExist(uid, pNode );
						
						if(isIdExist)
						{
							pNodeDataList.push_back(pNode);
						}
					}
					
					SubSectionSprayer SSsp;			
					SSsp.selectedSubSection.Append("Selected Item Group/Item List");	
					SSsp.selectedSubSection.SetTranslatable(kFalse);
					iSSSprayer->setSprayCustomProductOrItemListFlag(kTrue);
					SSsp.startSprayingSubSection();	
					
					firstpNodeListCount++;
					
					if(isCancelButtonClick == kFalse)
					{
						pNodeDataList.clear();
						CurrentSelectedProductRow =0; //multipleSelection[0];//-100 ; //**********
						pNodeDataList = temppNodeDataList;
						
						//listCntl->DeselectAll();
						isCancelButtonClick = kFalse;
						break;
					}
					else 
					{
						isCancelButtonClick = kTrue;
						break;
					}
				}
				else if(kSelectionLength == 1)
				{
					singleSelectionSpray = kTrue;
					Mediator::setIsMultipleSelection(kTrue);
					
					pNodeDataList.clear();
					for(int32 count = 0;count < kSelectionLength ; count++)
					{						
						NodeID nid=selectedItem[count];
						TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);
						int32 uid= uidNodeIDTemp->Get();
						PublicationNode pNode;
						bool16 isIdExist = dc.isExist(uid, pNode );
						
						if(isIdExist)
						{
							pNodeDataList.push_back(pNode);
						}
					}

					SubSectionSprayer SSsp;			
					SSsp.selectedSubSection.Append("Selected Family/Item");
					SSsp.selectedSubSection.SetTranslatable(kFalse);
					iSSSprayer->setSprayCustomProductOrItemListFlag(kTrue);
					SSsp.startSprayingSubSection();	
					
					firstpNodeListCount++;
					
					if(isCancelButtonClick == kFalse)
					{
						pNodeDataList.clear();
						CurrentSelectedProductRow =0;
						pNodeDataList = temppNodeDataList;
						iSSSprayer->setSprayCustomProductOrItemListFlag(kFalse);
						isCancelButtonClick = kFalse;
						break;
					}
					else 
					{
						isCancelButtonClick = kTrue;
						break;
					}
				}
			}
			pNodeDataList = temppNodeDataList;
//			//ended on 8Nov..
//			AcquireWaitCursor awc;
//			awc.Animate(); 
//			
//			ProductSpray PrSpryObj;
//			PrSpryObj.startSpraying();
//
//			//PrSpryObj.startSpraying();
//			//NewProductSprayer  ASD;
//			//ASD.startSpraying();		
		} 

		/********************************* Refresh PF Tree **********************************/

		if(theSelectedWidget == kSPRefreshWidgetID && theChange == kTrueStateMessage)
		{
			bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
			if(!isUserLoggedIn)			
				return;
			//--------Added on 12/10 To handle Refresh when ONEsource is selected
			Mediator md;
			
	
			ptrIAppFramework->clearAllStaticObjects();
			ptrIAppFramework->EventCache_clearInstance();
			ptrIAppFramework->clearConfigCache();
	

			PMString pub_Name = "";
			double pubParent_ID = -1;
			bool16 currentOneSource =kFalse;

			InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
			if(ptrIClientOptions == nil)//-100 is taken purposely.-100 does not exists.So something has to be selected from the list box.
			{
				//CAlert::ErrorAlert("Interface for IClientOptions not found.");
				break;
			}
			
			ptrIClientOptions->setdoRefreshFlag(kTrue);
			PMString retVal1;
			PMString language_name("");
			double language_id = -1 ;
			//int32 root1 = ptrIClientOptions->getDefPublication(retVal1);
			
			language_id = ptrIClientOptions->getDefaultLocale(language_name);
			global_lang_id = language_id;

			double root1 = Mediator::sectionID ;	//---For Current Selected DropDown Index-----

			PMString CatalogName("");
			currentOneSource = ptrIAppFramework->get_isONEsourceMode();
			if(currentOneSource == kTrue)
			{
				//CA("currentOneSource == kTrue");
				// global_project_level = 4;	//-----------
				// global_project_level = 2;

				//CA("ptrIAppFramework->get_isONEsourceMode()");

				VectorClassInfoPtr vectClsValuePtr = ptrIAppFramework->ClassificationTree_getRoot(global_lang_id); 
				if(vectClsValuePtr == nil){
					ptrIAppFramework->LogDebug("AP7_ProductFinder::GeneralPanelObserver::setDefaultValuesOfGeneralTab::ectClsValuePtr == nil ");
					return ;
				}
				VectorClassInfoValue::iterator itr;
				itr = vectClsValuePtr->begin();
				CatalogName.Append(itr->getClassification_name());
											
			}
			else
			{
				//CA("currentOneSource == kFalse");
				CPubModel pubModelRootValue = ptrIAppFramework->getpubModelByPubID(root1,global_lang_id);
				double defParentId = pubModelRootValue.getParentId();

				CPubModel pubDefRootValue = ptrIAppFramework->getpubModelByPubID(defParentId,global_lang_id);
											
				pubParent_ID = pubDefRootValue.getParentId();
				double rootPubID = pubDefRootValue.getRootID();

				CPubModel pubModelValue = ptrIAppFramework->getpubModelByPubID(rootPubID,global_lang_id);
				
				PMString TempBuff = pubModelValue.getName();
				
                pub_Name = TempBuff;
				this->CallLevelTwoThree();
			}
				
			//*added by Tushar on 11 Oct/////////////////////////////////////////////
			//if(FilterFlag == kTrue)
			//{
			//	CA("FilterFlag == kTrue");
			//	int root = md.getPublicationRoot();				

			//	PMString retVal1;
			//	int32 root1 = ptrIClientOptions->getDefPublication(retVal1);
			//	
			//	InterfacePtr<ITextControlData> pubNameTxtCntrlData(md.getPubNameTextView(), UseDefaultIID());
			//	if(!pubNameTxtCntrlData)
			//	{
			//		//CAlert::ErrorAlert("Interface for ITextControlData not found.");
			//		break;
			//	}
			//	
			//	if(root != root1)
			//	{
			//		//CA("FilterFlag == kTrue && root != root1");
			//		CurrentSelectedPublicationID = root1 ;

			//		global_publication_id = CurrentSelectedPublicationID ;
			//		
			//		PMString language_name("");
			//		int32 language_id = -1 ;
			//		language_id = ptrIClientOptions->getDefaultLocale(language_name);
			//		
			//		pubNameTxtCntrlData->SetString(retVal1);
			//		md.setPublicationRoot(root1);//Root
			//		md.setSelectedPub(-1);//Subsection
			//		md.setCurrentSelectedRow(0);
			//		  			
			//		global_lang_id = language_id ;
			//		if(currentOneSource == kFalse)
			//			populateSectionDropDownList(root1);
			//		else
			//		{
			//			this->levelFour();
			//			if(md.getRefreshButtonView()==NULL)
			//				break;
			//			else
			//				md.getRefreshButtonView()->Enable();
			//				
			//		}
			//	}
			//	
			//	if(root == root1)
			//	{
			//		CA("FilterFlag == kTrue && root == root1");
			//		//if we closed content sprayer by keeping filter flag true and
			//		//after that if we change catalog from options dialog then after
			//		//reopening of content spayer if we click on refresh button then
			//		//and then only the following condition gets satisfied...otherwise 
			//		//it will go into the normal filter case.. 
			//		if(flag == 0)
			//		{
			//			CA("flag == 0");
			//			pubNameTxtCntrlData->SetString(retVal1);
			//			//md.setSelectedPub(-1);//Subsection
			//			populateSectionDropDownList(md.getPublicationRoot());

			//			int32 InputID = 0;
			//			if(global_project_level == 3)
			//			{
			//				this->fillSubSectionList(CurrentSelectedSection);
			//				/*PMString SubSecName("");
			//				int32 subsectionID = getSelectedSubsectionId(controlView, SubSecName);
			//				CurrentSelectedSubSection = subsectionID;*/
			//				InputID = CurrentSelectedSubSection;
			//		
			//			}
			//			else if(global_project_level ==2)
			//			{
			//				InputID = CurrentSelectedSection;
			//			}

			//			else if(global_project_level ==4)
			//			{
			//				InputID = global_classID;//It is ClassID which is declared global in ContentSprayer.cpp
			//				this->levelFour();//Added on 8/11
			//			}
			//			this->populateProductListforSection(InputID);
			//			CA("Before Break from Refresh");
			//			break;
			//		}
			//		else
			//			{
			//				CA("flag!=0");
			//				int32 InputID = 0;
			//				InputID = global_classID;
			//				this->populateProductListforSection(InputID);
			//				//Added on 8/11 to hide and show green widget
			//					Mediator m;
			//					if(m.getGreenFilterBtnView()==nil)
			//					{
			//						//CA("Dialog Observer::m.getGreenFilterBtnView()==nil");
			//					}
			//					else
			//					{
			//						m.getGreenFilterBtnView()->HideView();	
			//					}
			//			
			//					if(m.getAssignBtnView()==nil)
			//					{
			//						//CA("Dialog Observer::m.getAssignBtnView()==nil");
			//					}
			//					else
			//					{
			//						m.getAssignBtnView()->ShowView();
			//						m.getAssignBtnView()->Enable();
			//					}
			//				//8/11
			//				break;
			//			}
			//		//FilterFlag = kTrue;
			//	}
			//}
		///*/////////////////////////////////////////////////////////////////////

			//if(CurrentSelectedProductRow == -100)//-100 is taken purposely.-100 does not exists.So something has to be selected from the list box.
			//{
			//	CA("CurrentSelectedProductRow == -100");
			//	break;
			//}
						
		///**added by Tushar on 10 oct///////////////////////////////////////////////

			//Mediator md;
			int root = md.getPublicationRoot();
			
			//root1 = ptrIClientOptions->getDefPublication(retVal1);
			//APSIVA 9 comment
			//if(kFalse/*ptrIAppFramework->CONFIGCACHE_getCmShowEventNumber()*/)
			//{
				/*CPubModel pubModelObj = ptrIAppFramework->getpubModelByPubID(root1, global_lang_id);
				if(pubModelObj.getNumber() != "-1" )
				{
					PMString asf;
					asf.Append(pubModelObj.getNumber());
					asf.Append("-");
					asf.Append(retVal1);
					retVal1.clear();
					retVal1.Append(asf);								
				}*/
			//}

			
			InterfacePtr<ITextControlData> pubNameTxtCntrlData(md.getPubNameTextView(), UseDefaultIID());
			if(!pubNameTxtCntrlData)
			{
				//CAlert::ErrorAlert("Interface for ITextControlData not found.");
				break;
			}
			
			if(FilterFlag == kFalse && root != root1)
			{
				//CA("FilterFlag == kFalse && root != root1");
				CurrentSelectedPublicationID = root1 ;
				global_publication_id = CurrentSelectedPublicationID ;

				PMString pub_newname("");
				if(isONEsource)
				{
					pub_newname.Append(/*retVal1*/CatalogName);
					
				}
				else
				{
					pub_newname = "Event";
					pub_newname.Append(" : ");
					pub_newname.Append(pub_Name/*retVal1*/);
				}
				
				pub_newname.SetTranslatable(kFalse);
                pub_newname.ParseForEmbeddedCharacters();
				pubNameTxtCntrlData->SetString(pub_newname);

				md.setPublicationRoot(root1);//Root
				md.setSelectedPub(-1);//Subsection
				md.setCurrentSelectedRow(0);
				  			
				global_lang_id = language_id ;
				if(currentOneSource == kFalse)
					populateSectionDropDownList(root1);
				else
				{
					populateSectionDropDownList(root1);
					this->populateProductListforSection(root1);
				}
				
			}

			if(CurrentSelectedSection == -1){
				//CA("CurrentSelectedSection == -1");
				break;
			}
			

			RefreshFlag = kTrue;
			AcquireWaitCursor awc;
			awc.Animate(); 

			if(FilterFlag == kFalse)
			{
				if(root == root1)
				{
					///CA("FilterFlag == kFalse && root == root1");
					
					//----------------
					PMString pubname_new("");
					if(isONEsource)
					{
						pubname_new.Append(CatalogName);
					}
					else
					{
						pubname_new = "Event";
						pubname_new.Append(" : ");
						pubname_new.Append(pub_Name/*retVal1*/);
					}

					pubname_new.SetTranslatable(kFalse);
                    pubname_new.ParseForEmbeddedCharacters();
					pubNameTxtCntrlData->SetString(pubname_new);

					populateSectionDropDownList(root1);	
					
					double InputID = 0;
					if(global_project_level == 3)
					{
						this->fillSubSectionList(CurrentSelectedSection);
						InputID = CurrentSelectedSubSection;
				
					}
					else if(global_project_level ==2)
					{
						InputID = CurrentSelectedSection;
					}

					if(isONEsource)		//-----Added----
					{
						//CA("isONEsource");
						InputID = root1/*global_classID*/;
					}
					this->populateProductListforSection(InputID);

				}
			}
			else
			{
				//APSIVA 9 comment
				////CA("FilterFlag == kTrue   ... 123456");
				//// FilterFlag == kTrue  //CA("RefreshWidgetID 1.3");
				////AddProductsToListbox(GlobalDesignerActionFlags,this);
				////Mediator md;			

				////	//PublicatioNode pNode will be used as a element in the PublicationNodeList pNodeDataList.
				//PublicationNode pNode;
				//SDKListBoxHelper listHelperForRefresh(this, kSPPluginID);
				//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
				//if(ptrIAppFramework == nil)
				//{//3
				//	//CAlert::InformationAlert("Err ptrIAppFramework is nil");				
				//	break;
				//}//2
				////CA("RefreshWidgetID 1.4");
				//int32 ParentTypeID = ptrIAppFramework->TYPEMngr_getObjectTypeID("PRODUCT_LEVEL");
				//
				//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				//
				//VectorPubObjectMilestoneValuePPtr VectorFamilyInfoValuePtr = nil;
				//
				//PMString milestoneid("");
				//bool8 check=FALSE;
				//
				//for(int32 i=0;i<samitvflist.size();i++)//
				//{//3
				//	//CA("Inside the for loop in dialog update");
				//	if(samitvflist[i].isSelected==kTrue)
				//	{
				//		//			//CA("8");
				//		if(check==TRUE)
				//		milestoneid.Append(",");
				//		milestoneid.AppendNumber(samitvflist[i].milestone_id);
				//		check=TRUE;
				//		//CA(milestoneid);
				//	}//3
				//}//2
			
				//int32 curSelSubecId;
				//if(global_project_level == 3)
				//{
				//	curSelSubecId = CurrentSelectedSubSection;
				//}
				//else if(global_project_level == 2)
				//{
				//	curSelSubecId = CurrentSelectedSection;
				//}
				//else if(global_project_level == 4)	
				//{
				//	curSelSubecId = global_classID;
				//}
				//
				////curSelSubecId = md.getCurrSectionID();
			
				//if(ShowAllProductsRadioTriStateFlag)
				//{
				//			
				//	//GlobalDesignerActionFlags[0] = GlobalDesignerActionFlags[1] = GlobalDesignerActionFlags[2] = GlobalDesignerActionFlags[3] = GlobalDesignerActionFlags[4] = GlobalDesignerActionFlags[5] = GlobalDesignerActionFlags[6] = kFalse;		
				//	//VectorFamilyInfoValuePtr = ptrIAppFramework->PBObjMngr_getProductsForSubSection(curSelSubecId, ParentTypeID);

				//	populateProductListforSection(curSelSubecId);
				//	RefreshFlag = kFalse;
				//	break;
				//}
				//	 
				//if(FilterByMilestonesRadioTriStateFlag)
				//{
				//	//CA("inside milestones");
				//	//GlobalDesignerActionFlags[0] = GlobalDesignerActionFlags[1] = GlobalDesignerActionFlags[2] = GlobalDesignerActionFlags[3] = GlobalDesignerActionFlags[4] = GlobalDesignerActionFlags[5] = GlobalDesignerActionFlags[6] = kFalse;		
				//	if(milestone_selected)
				//	{
				//		//CA("A");
				//		
				//		//VectorFamilyInfoValuePtr = ptrIAppFramework->PUBCntrller_getProductsBySubSectionIdMilestoneIds(curSelSubecId,milestoneid.GetPlatformString().c_str(),kTrue);//,DesignerActionFlags[0],DesignerActionFlags[1],DesignerActionFlags[2],DesignerActionFlags[3],DesignerActionFlags[4],DesignerActionFlags[5],DesignerActionFlags[6]);//8.2.5
				//		//VectorFamilyInfoValuePtr = ptrIAppFramework->GetProjectProductAction_getProductsBySubSectionIdMilestoneIds(curSelSubecId,milestoneid.GetPlatformString().c_str(),completeRadioSelected, selectedroleID,selecteduserID); //Cs3
				//		VectorFamilyInfoValuePtr = ptrIAppFramework->GetProjectProductAction_getProductsBySubSectionIdMilestoneIds(curSelSubecId,
				//																												    const_cast<char*>(milestoneid.GetPlatformString().c_str()),
				//																													completeRadioSelected, 
				//																													selectedroleID,selecteduserID);
				//	}
				//	else
				//	{//CA("B");
				//			VectorFamilyInfoValuePtr = nil;

				//	}
				//}
				//	
				//if(FilterByDesignerActionsRadioTriStateFlag)
				//{
				//	if(!(GlobalDesignerActionFlags[0] || GlobalDesignerActionFlags[1] || GlobalDesignerActionFlags[2] || GlobalDesignerActionFlags[3] || GlobalDesignerActionFlags[4] || GlobalDesignerActionFlags[5] || GlobalDesignerActionFlags[6] || GlobalDesignerActionFlags[7]))
				//	{
				//				//VectorFamilyInfoValuePtr = nil;
				//		populateProductListforSection(curSelSubecId);
				//		RefreshFlag = kFalse;
				//		break;
				//	}
				//	else
				//	{
				//		//CA(" Before populateProductListforSectionWithDesignerActions");
				//		//VectorFamilyInfoValuePtr = ptrIAppFramework->GetProjectProductAction_getProductsBySubSectionIdTypeIdMstoneIdDueUserIdDesignerActions(curSelSubecId,milestoneid.GetPlatformString().c_str(),GlobalDesignerActionFlags[0],GlobalDesignerActionFlags[1],GlobalDesignerActionFlags[2],GlobalDesignerActionFlags[3],GlobalDesignerActionFlags[4],GlobalDesignerActionFlags[5],GlobalDesignerActionFlags[6]);//8.2.5
				//		populateProductListforSectionWithDesignerActions(curSelSubecId,GlobalDesignerActionFlags[0],GlobalDesignerActionFlags[1],GlobalDesignerActionFlags[2],GlobalDesignerActionFlags[3],GlobalDesignerActionFlags[4],GlobalDesignerActionFlags[5],GlobalDesignerActionFlags[6], GlobalDesignerActionFlags[7]);

				//		RefreshFlag = kFalse;
				//		break;
				//		//VectorFamilyInfoValuePtr = ptrIAppFramework->PBObjMngr_getProductsForSubSection(curSelSubecId, ParentTypeID);
				//	}

				//}
				//			
			
				//pNodeDataList.clear();		
				//IControlView * FirstListBoxView = md.getListBoxView();
			
				//if(!FirstListBoxView)
				//{//3
				//
				//	//CA("!FirstListBoxView");
				//	return;
				//}//2
				//////CA("14");
			
				//////InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
				//IPanelControlData* iPanelControlData;
				//iPanelControlData=md.getMainPanelCtrlData();

				//if(!iPanelControlData)
				//{
				//	//CA("iPanelControlData nil");			
				//	break;
				//}
				//////CA("15");
				//////end 8.2.5
				//if(VectorFamilyInfoValuePtr == nil)
				//{//3
				//
				//	listHelperForRefresh.EmptyCurrentListBox(iPanelControlData,1);
				//	break;
				//}//2
			
				//if(VectorFamilyInfoValuePtr->size()==0)
				//{//3
				//	//CA("no product found");
				//	delete VectorFamilyInfoValuePtr;
				//	break;
				//}//2
				////CA("RefreshWidgetID 1.7");
				//listHelperForRefresh.EmptyCurrentListBox(iPanelControlData,1);

				//// -- commented on 31 jan 06 --
				//VectorPubObjectValuePointer::iterator it1;
				//VectorPubObjectValue::iterator it2;
				//				
				//int count=0;
				//PMString itemCountString("");
				//int32 itemCountNum =0;


				////CA("RefreshWidgetID 1.8");
				//for(it1 = VectorFamilyInfoValuePtr->begin(); it1 != VectorFamilyInfoValuePtr->end(); it1++)
				//{//3
				//	//CA("RefreshWidgetID 1.9");
				//	//	------------------------- commented on 31 jan 06 -----------------------------------------//
				//	for(it2=(*it1)->begin(); it2 !=(*it1)->end(); it2++)
				//	{	//4
				//		//CA("for 17.1");
				//		pNode.setLevel(it2->getObjectValue().getLevel_no());
				//		pNode.setParentId(it2->getObjectValue().getParent_id());
				//		pNode.setSequence(it2->getIndex());
				//		pNode.setPubId(it2->getObjectValue().getObject_id());
				//		pNode.setIsStarred(it2->getStarredFlag1());

				//		int32 NewProductFlag =1;
				//		if(it2->getNew_product() == kTrue){

				//			pNode.setNewProduct(1);
				//			NewProductFlag =2;
				//		}
				//		else{
				//			pNode.setNewProduct(0);
				//			NewProductFlag =1;
				//		}


				//		int32 StarFlag =1;
				//		if(it2->getStarredFlag1()) // if Selected Green Star
				//			StarFlag =2;

				//		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				//		if(!iConverter)
				//		{
				//			pNode.setPublicationName(it2->getObjectValue().getName());
				//			//CA(it2->getObjectValue().getName());
				//		}//4
				//		else
				//		{//5
				//			//CA("22.22");
				//			pNode.setPublicationName(iConverter->translateString(it2->getObjectValue().getName()));
				//			//CA(it2->getObjectValue().getName());
				//		}//4
				//		//		
				//		pNode.setChildCount(it2->getObjectValue().getChildCount());
				//		pNode.setReferenceId(it2->getObjectValue().getRef_id());
				//		pNode.setTypeId(it2->getObjectValue().getObject_type_id());

				//		int icon_count = 111;
				//		
				//		//bool8 none_of_the_designer_action_selected = kFalse;
				//		if((FilterByDesignerActionsRadioTriStateFlag )&&(GlobalDesignerActionFlags[0] || GlobalDesignerActionFlags[1] || GlobalDesignerActionFlags[2] || GlobalDesignerActionFlags[3] || GlobalDesignerActionFlags[4] || GlobalDesignerActionFlags[5] || GlobalDesignerActionFlags[6]))
				//		{
				//			//none_of_the_designer_action_selected = kFalse;
				//			
				//			if(it2->getNew_product() || it2->getAdd_to_spread() || it2->getUpdate_spread() || it2->getUpdate_copy()  || it2->getUpdate_art() || it2->getUpdate_item_table()  ||  it2->getDelete_from_spread() )
				//			{
				//				if((it2->getNew_product()&& GlobalDesignerActionFlags[0]) || (it2->getAdd_to_spread() && GlobalDesignerActionFlags[1]) || (it2->getUpdate_spread() && GlobalDesignerActionFlags[2]) || (it2->getUpdate_copy() && GlobalDesignerActionFlags[3]) || (it2->getUpdate_art() && GlobalDesignerActionFlags[4]) || (it2->getUpdate_item_table() && GlobalDesignerActionFlags[5]) ||  (it2->getDelete_from_spread() && GlobalDesignerActionFlags[6]))
				//				{
				//					
				//					pNodeDataList.push_back(pNode);
				//					icon_count = GetIconCountForUnfilteredProductList(it2);

				//					//icon_count = GetIconCountWhenDesignerActionSelected(it2 ,GlobalDesignerActionFlags);
				//				}
				//				else
				//					continue;
				//				
				//				//pNodeDataList.push_back(pNode);
				//			}
				//			else
				//				continue;
				//		}
				//		else
				//		{
				//			//list of all unfiltered products
				//			pNodeDataList.push_back(pNode);
				//			icon_count = GetIconCountForUnfilteredProductList(it2);
				//			//none_of_the_designer_action_selected = kTrue;
				//		}

				//		
				//		//	if( (!none_of_the_designer_action_selected) && (pNodeDataList.size()>0))
				//		//	{

				//		//		icon_count = GetIconCountWhenDesignerActionSelected(it2 ,GlobalDesignerActionFlags);
				//		//		
				//		//	}

				//		//itemCountNum = it2->getChildCount();  //****
				//		if(itemCountNum  >1)
				//		{
				//			itemCountString.Append("(");
				//			itemCountString.AppendNumber(itemCountNum);
				//			itemCountString.Append(")");
				//		}
				//		else
				//			itemCountString.Append("");

				//		listHelperForRefresh.AddElement(FirstListBoxView , pNode.getName(), kSPTextWidgetID, count );
				//		listHelperForRefresh.SetDesignerAction(FirstListBoxView, count, kTrue,icon_count, StarFlag,NewProductFlag,itemCountString); //****
				//		count++;

				//		itemCountString.Clear();
				//		itemCountNum =0;
				//

				//	}//3

				//}	//2
				//-------------------------------------------- commented on 31 jan 06 -----------------------------------//
				//	CA("RefreshWidgetID 1.10");
			}//1
				//commented to provide new refresh functionality.
				//commented on 4/4/05

			RefreshFlag = kFalse;

		}//0
		//------------------Show Table Source----------------------------------------
		if(theSelectedWidget==kSPShowTableSourceWidgetID && theChange==kTrueStateMessage)
		{
			////CA("Table Source");
			bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
			if(!isUserLoggedIn)			
				return;
			do
			{
				InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
			    if(ptrTableSourceHelper == nil)
			    {
					//CA("TABLEsource not found ");
					return;
			    }
				if(pNodeDataList.size() < 1)
				{
					break;
				}

				bool16 isTableSourceShown = ptrTableSourceHelper->isTABLESourcePanelOpen();
				if(isTableSourceShown == kFalse)
					ptrTableSourceHelper->showTableSourcePanel();

				double objectId = pNodeDataList[CurrentSelectedProductRow].getPubId();
				double pbObjectID = pNodeDataList[CurrentSelectedProductRow].getPBObjectID();
				GSysRect PalleteBounds  = getBoundingBoxOfWindow() ;

				int32 TemplateTop		= 0; //PalleteBounds.top;
				int32 TemplateLeft	    = 0; //PalleteBounds.left;
				int32 TemplateRight	    = 0; //PalleteBounds.right;
				int32 TemplateBottom	= 0; // PalleteBounds.bottom;
				
                #ifdef  WINDOWS
                    TemplateTop		= PalleteBounds.top;
                    TemplateLeft	    = PalleteBounds.left;
                    TemplateRight	    = PalleteBounds.right;
                    TemplateBottom	= PalleteBounds.bottom;
                #else
				
                TemplateTop = SysRectTop(PalleteBounds);
                TemplateLeft=SysRectLeft(PalleteBounds);
                TemplateRight=SysRectRight(PalleteBounds);
                TemplateBottom=SysRectBottom(PalleteBounds);
				
                #endif
				//if(isTableSourceShown == kFalse)
				//	ptrTableSourceHelper->positionTABLESourcePanel(TemplateTop , TemplateLeft , TemplateBottom , TemplateRight);
				
				if(searchResult)
				{
					
				}
				else
				{
					//CA("else");
					if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 1 )
					{
						checkTreeType=3;
						ptrTableSourceHelper->showTableList(objectId , global_lang_id , Mediator::parentID , pNodeDataList[CurrentSelectedProductRow].getTypeId() , Mediator::sectionID ,checkTreeType,pbObjectID);
					}
					if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 0)
					{
						checkTreeType=4;
						ptrTableSourceHelper->showTableList(objectId , global_lang_id , Mediator::parentID , pNodeDataList[CurrentSelectedProductRow].getTypeId() , Mediator::sectionID ,checkTreeType,pbObjectID);
					}					
				}
			}while(kFalse);
		}
		if(theSelectedWidget==kSPSelectCustomerWidgetID && theChange==kTrueStateMessage)
		{
			//CA("kSPSelectCustomerWidgetID");
			do
			{
				// Get the application interface and the DialogMgr.	
				InterfacePtr<IApplication> application(/*gSession*/GetExecutionContextSession()->QueryApplication());
				if (application == nil) 
				{	
					//CA("application == nil");
					break;
				}
				InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
				if (dialogMgr == nil) 
				{
					//CA("dialogMgr == nil");
					break;
				}
				// Load the plug-in's resource.
				PMLocaleId nLocale = LocaleSetting::GetLocale();
				RsrcSpec dialogSpec
				(
					nLocale,							// Locale index from PMLocaleIDs.h. 
					kSPPluginID,						// Our Plug-in ID  
					kViewRsrcType,						// This is the kViewRsrcType.
					kSelectCustomerDialogResourceID,	// Resource ID for our dialog.
					kTrue								// Initially visible.
				);
				// CreateNewDialog takes the dialogSpec created above, and also
				// the type of dialog being created (kMovableModal).
				IDialog * dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal,kTrue,kFalse);
				if (dialog == nil) 
				{
					//CA("dialog == nil");
					break;
				}
				// Open the dialog.
				dialog->Open();
			
			} while (false);
		}
		//**********************************Template button*********************************
		if(theSelectedWidget==kSPTemplatePreviewWidgetID && theChange==kTrueStateMessage)
		{
			////CA("theSelectedWidget==kSPTemplatePreviewWidgetID");
			//bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
			//if(!isUserLoggedIn)			
			//	return;
			//		
			//do
			//{
			//	InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(kDDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID1))));
			//    if(ptrImageHelper == nil)
			//    {
			//		//CA("IMAGEsource not found ");
			//		return;
			//    }
			//	
			//	bool16 isImagePanelShown = ptrImageHelper->isImagePanelOpen();
			//	if(isImagePanelShown == kFalse)	
			//		ptrImageHelper->showImagePanel();

			//	int32 objectId = pNodeDataList[CurrentSelectedProductRow].getPubId();
			//
			//	GSysRect PalleteBounds  = getBoundingBoxOfWindow() ;
			//	
			//	if(ptrImageHelper->isAtt_EleIDSVecFilled() == kFalse)
			//	{
			//		
			//		do{
			//			vector<int32> ElementIDSWithPVMPVImgTyp;

			//			VectorElementInfoPtr eleValObj = ptrIAppFramework->ElementCache_getCopyAttributesByIndex(3, global_lang_id);
			//			if(eleValObj==nil){
			//				ptrIAppFramework->LogError("AP7_ProductFinder::SPSelectionObserver::Update::::ElementCache_getCopyAttributesByIndex's eleValObj==nil");
			//				break;
			//			}
			//			//CA("eleValObj== not nil ");
			//			VectorElementInfoValue::iterator it4;
			//			for(it4=eleValObj->begin();it4!=eleValObj->end();it4++)
			//				if(it4->getPv_type_id() > 0)
			//					ElementIDSWithPVMPVImgTyp.push_back(it4->getElement_id());						
			//			
			//			ptrImageHelper->setattributesWithPVMPV(ElementIDSWithPVMPVImgTyp,1);

			//			ElementIDSWithPVMPVImgTyp.clear();
			//			if(eleValObj)
			//			{
			//				eleValObj->clear();
			//				delete eleValObj;
			//			}
			//		}while(kFalse);
			//		do{
			//			
			//			vector<int32> attrIDSWithPVMPVImgTyp;
			//			
			//	
			//			if(global_classID == 0 || global_classID == -1)
			//			{
			//				
			//				VectorClassInfoPtr vectClassInfoValuePtr = ptrIAppFramework->ClassificationTree_getRoot(1); // 1 default for English
			//				if(vectClassInfoValuePtr==nil)
			//				{		
			//					ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::populateItemPanelLstbox::ClassificationTree_getRoot's vectClassInfoValuePtr==nil");
			//					break;
			//				}
			//				VectorClassInfoValue::iterator it;
			//				it=vectClassInfoValuePtr->begin();
			//				global_classID = it->getClass_id();		

			//				 if(vectClassInfoValuePtr)
			//					delete vectClassInfoValuePtr;
			//			}

			//			VectorAttributeInfoPtr AttrInfoVectPtr = NULL;
			//			
			//			AttrInfoVectPtr = ptrIAppFramework->AttributeCache_getItemAttributesForClassAndParents(global_classID, global_lang_id);
			//			if(AttrInfoVectPtr== NULL){
			//				ptrIAppFramework->LogError("AP7_ProductFinder::SPSelectionObserver::Update::::AttributeCache_getItemAttributesForClassAndParents's AttrInfoVectPtr is nil ");
			//				break;
			//			}
			//			VectorAttributeInfoValue::iterator it2;

			//			for(it2=AttrInfoVectPtr->begin();it2!=AttrInfoVectPtr->end();it2++)
			//				if(it2->getPv_type_id() > 0)
			//					attrIDSWithPVMPVImgTyp.push_back(it2->getAttribute_id());

			//			ptrImageHelper->setattributesWithPVMPV(attrIDSWithPVMPVImgTyp,0);

			//			attrIDSWithPVMPVImgTyp.clear();
			//			if(AttrInfoVectPtr)
			//			{
			//				AttrInfoVectPtr->clear();
			//				delete AttrInfoVectPtr;
			//			}
			//		}while(kFalse);
			//	}				

			//	int32 TemplateTop		= PalleteBounds.top;
			//	int32 TemplateLeft	    = PalleteBounds.left;
			//	int32 TemplateRight	    = PalleteBounds.right;
			//	int32 TemplateBottom	= PalleteBounds.bottom;
			//	
			//	if(isImagePanelShown == kFalse)	
			//		ptrImageHelper->positionImagePanel(TemplateTop , TemplateLeft , TemplateBottom , TemplateRight);
			//	
			//	AcquireWaitCursor awc;
			//	awc.Animate(); 
			//	if(searchResult)
			//	{
			//		//CA("if Search result");
			//		if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 1)
			//			ptrImageHelper->showProductImages(objectId , global_lang_id , pNodeDataList[CurrentSelectedProductRow].getParentId() , pNodeDataList[CurrentSelectedProductRow].getTypeId() , pNodeDataList[CurrentSelectedProductRow].getSectionID() ) ;
			//		if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 0)
			//			ptrImageHelper->showItemImages(objectId , global_lang_id , pNodeDataList[CurrentSelectedProductRow].getParentId() , pNodeDataList[CurrentSelectedProductRow].getTypeId() , pNodeDataList[CurrentSelectedProductRow].getSectionID(), kFalse);
			//	}
			//	else
			//	{
			//		//CA("else");
			//		if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 1)
			//			ptrImageHelper->showProductImages(objectId , global_lang_id , Mediator::parentID , Mediator::parentTypeID , Mediator::sectionID ) ;
			//		if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 0)
			//			ptrImageHelper->showItemImages(objectId , global_lang_id , Mediator::parentID , pNodeDataList[CurrentSelectedProductRow].getTypeId() , Mediator::sectionID, kFalse);
			//	}
			//}
			//while(0);

		}
		/* ******************************** Show Whiteboard ********************************* */
		if(theSelectedWidget==kSPShowWhiteBoardWidgetID && theChange==kTrueStateMessage)
		{
			//CA("theSelectedWidget==kSPShowWhiteBoardWidgetID ");
			bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
			if(!isUserLoggedIn)			
				return;
			
								
			do
			{
				InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
			    if(ptrImageHelper == nil)
			    {
					//CA("IMAGEsource not found ");
					return;
			    }
				
				bool16 isImagePanelShown = ptrImageHelper->isImagePanelOpen();
				if(isImagePanelShown == kFalse)	
					ptrImageHelper->showImagePanel();

				double objectId = pNodeDataList[CurrentSelectedProductRow].getPubId();
			
				GSysRect PalleteBounds  = getBoundingBoxOfWindow() ;
				
				if(ptrImageHelper->isAtt_EleIDSVecFilled() == kFalse)
				{
					//CA("ptrImageHelper->isAtt_EleIDSVecFilled() == kFalse");
					do{
						vector<double> ElementIDSWithPVMPVImgTyp;

						VectorElementInfoPtr eleValObj = ptrIAppFramework->StructureCache_getItemGroupCopyAttributesByLanguageId( global_lang_id);
						if(eleValObj==nil){
							ptrIAppFramework->LogError("AP7_ProductFinder::SPSelectionObserver::Update::::ElementCache_getCopyAttributesByIndex's eleValObj==nil");
							break;
						}
						//CA("eleValObj== not nil ");
						VectorElementInfoValue::iterator it4;
						for(it4=eleValObj->begin();it4!=eleValObj->end();it4++)
							if(it4->getPicklistGroupId() > 0)
								ElementIDSWithPVMPVImgTyp.push_back(it4->getElementId());						
						
						ptrImageHelper->setattributesWithPVMPV(ElementIDSWithPVMPVImgTyp,1);

						ElementIDSWithPVMPVImgTyp.clear();
						if(eleValObj)
						{
							eleValObj->clear();
							delete eleValObj;
						}
					}while(kFalse);
					do{
						vector<double> attrIDSWithPVMPVImgTyp;
						
				
						if(global_classID == 0 || global_classID == -1)
						{
							//CA("CurrentClassID == -1");
							VectorClassInfoPtr vectClassInfoValuePtr = ptrIAppFramework->ClassificationTree_getRoot(1); // 1 default for English
							if(vectClassInfoValuePtr==nil)
							{		
								ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::populateItemPanelLstbox::ClassificationTree_getRoot's vectClassInfoValuePtr==nil");
								break;
							}
							VectorClassInfoValue::iterator it;
							it=vectClassInfoValuePtr->begin();
							global_classID = it->getClass_id();		

							 if(vectClassInfoValuePtr)
								delete vectClassInfoValuePtr;
						}

						VectorAttributeInfoPtr AttrInfoVectPtr = NULL;
						
						AttrInfoVectPtr = ptrIAppFramework->StructureCache_getItemAttributesForClassAndParents(global_classID, global_lang_id);
						if(AttrInfoVectPtr== NULL){
							ptrIAppFramework->LogError("AP7_ProductFinder::SPSelectionObserver::Update::::AttributeCache_getItemAttributesForClassAndParents's AttrInfoVectPtr is nil ");
							break;
						}
						VectorAttributeInfoValue::iterator it2;

						for(it2=AttrInfoVectPtr->begin();it2!=AttrInfoVectPtr->end();it2++)
							if(it2->getPicklistGroupId() > 0)
								attrIDSWithPVMPVImgTyp.push_back(it2->getAttributeId());

						ptrImageHelper->setattributesWithPVMPV(attrIDSWithPVMPVImgTyp,0);

						attrIDSWithPVMPVImgTyp.clear();
						if(AttrInfoVectPtr)
						{
							AttrInfoVectPtr->clear();
							delete AttrInfoVectPtr;
						}
					}while(kFalse);
				}				

				int32 TemplateTop		= 0; // PalleteBounds.top;
				int32 TemplateLeft	    = 0; //PalleteBounds.left;
				int32 TemplateRight	    = 270; //PalleteBounds.right;
				int32 TemplateBottom	= 230; //PalleteBounds.bottom;
				
				//if(isImagePanelShown == kFalse)
					//ptrImageHelper->positionImagePanel(TemplateTop , TemplateLeft , TemplateBottom , TemplateRight);
				
				AcquireWaitCursor awc;
				awc.Animate(); 

				PMString itemFieldIds("");
				PMString itemAssetTypeIds("");
				PMString itemGroupFieldIds("");
				PMString itemGroupAssetTypeIds("");
				PMString listTypeIds("");
				PMString listItemFieldIds("");
				bool16 isSprayItemPerFrameFlag = kTrue;
				double langId = ptrIAppFramework->getLocaleId();
				PMString currentSectionitemGroupIds("");
				PMString currentSectionitemIds("");
				

				if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 1)
				{			
					isSprayItemPerFrameFlag = kTrue;
					currentSectionitemGroupIds.AppendNumber(PMReal(pNodeDataList[CurrentSelectedProductRow].getPubId()));
				}
				else if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 0)
				{
					isSprayItemPerFrameFlag = kFalse;
					currentSectionitemIds.AppendNumber(PMReal(pNodeDataList[CurrentSelectedProductRow].getPubId()));
				}
			
				ptrIAppFramework->clearAllStaticObjects();

				ptrIAppFramework->EventCache_setCurrentSectionData( CurrentSelectedSection, langId , currentSectionitemGroupIds, currentSectionitemIds, itemFieldIds, itemAssetTypeIds, itemGroupFieldIds,itemGroupAssetTypeIds,listTypeIds, listItemFieldIds, kFalse, isSprayItemPerFrameFlag);


				if(searchResult)
				{//CA("if Search result");
					if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 1)
						ptrImageHelper->showProductImages(objectId , global_lang_id , pNodeDataList[CurrentSelectedProductRow].getParentId() , pNodeDataList[CurrentSelectedProductRow].getTypeId() , pNodeDataList[CurrentSelectedProductRow].getSectionID() ) ;
					if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 0)
						ptrImageHelper->showItemImages(objectId , global_lang_id , pNodeDataList[CurrentSelectedProductRow].getParentId() , pNodeDataList[CurrentSelectedProductRow].getTypeId() , pNodeDataList[CurrentSelectedProductRow].getSectionID(), kFalse);
				}
				else
				{//CA("else");
					if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 1)
						ptrImageHelper->showProductImages(objectId , global_lang_id , Mediator::parentID , Mediator::parentTypeID , Mediator::sectionID ) ;
					if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 0)
						ptrImageHelper->showItemImages(objectId , global_lang_id , Mediator::parentID , pNodeDataList[CurrentSelectedProductRow].getTypeId() , Mediator::sectionID, kFalse);
				}

			}
			while(0);
		}
		/********************************* spray SubSection **********************************/

		if(theSelectedWidget==kSPSubSectionSprayButtonWidgetID && theChange==kTrueStateMessage)
		{
			singleSelectionSpray = kFalse;
			bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
			if(!isUserLoggedIn)			
				return;
			
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
			{
				//CA(" ptrIAppFramework nil ");
				break;
			}
            
			ptrIAppFramework->clearAllStaticObjects();

			Mediator::setIsMultipleSelection(kFalse);
			
			Mediator md;
			SubSectionSprayer SSsp;

			if(global_project_level == 3)
			{
				SSsp.selectedSubSection = md.getCurSubSecName();
			}
			else if(global_project_level == 2)
			{
				SSsp.selectedSubSection = md.getCurSecName();
			}

			InterfacePtr<ISubSectionSprayer> iSSSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
			if(iSSSprayer==nil)
			{
				ptrIAppFramework->LogInfo("AP46_ProductFinder::SPSelectionObserver::Update::kSPSprayButtonWidgetID::iSSSprayer==nil");
				return;
			}
			iSSSprayer->setSprayCustomProductOrItemListFlag(kTrue);
			iSSSprayer->setIsSectionSprayFlag(kTrue);

			SSsp.startSprayingSubSection();
			iSSSprayer->setIsSectionSprayFlag(kFalse);
			iSSSprayer->setSprayCustomProductOrItemListFlag(kFalse);

		}
		/*********************************** Assign Button ********************************************************/

		if((theSelectedWidget == kAssignButtonWidgetID && theChange == kTrueStateMessage) ||(theSelectedWidget==kGreenFilterButtonWidgetID && theChange==kTrueStateMessage) )
		{
			bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
			if(!isUserLoggedIn)			
				return;
			
			//CA("theSelectedWidget == kAssignButtonWidgetID && theChange == kTrueStateMessage) ||(theSelectedWidget==kGreenFilterButtonWidgetID && theChange==kTrueStateMessage");
			CurrentSelectedProductRow = -100;
			//Mediator md;
			Mediator::vMultipleSelection.clear();
			
			//CA("AssignButtonWidgetID 1");
			//Mediator md;

			/*if(!md.getClearBtnView())
			{
				CA("No control view from mediator ");
				break;
			}*/

			/*md.getClearBtnView()->Enable();*/

			//CA("inside");
			SPActionComponent as(this);
			//FilterFlag=kFalse
			//CA(" opening designer milestone dialog ");
			as.DoDialog();

			//CA("AssignButtonWidgetID 2");

		}
		/*********************************** Clear Button ********************************************************/
		//if(theSelectedWidget==kClearButtonWidgetID && theChange==kTrueStateMessage)
		//{
		//	//CA("ClearButtonWidgetID");
		//	listHelper.EmptyCurrentListBox(iPanelControlData, 1);
		//	pNodeDataList.clear();
		//	FilterFlag=kFalse;
		//	Mediator md;
		//	this->populateProductListforSection(md.getCurrSectionID());
		//	//Mediator md;
		//	//md.getClearBtnView()->Disable();
		//}

		//-------------- subsection dropdown clicked here ------------------------------------------------//
		if(theSelectedWidget == kSPSubSectionDropDownWidgetID  && theChange == kPopupChangeStateMessage)
		{
			//CA("SubsectionDropDown");
			bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
			if(!isUserLoggedIn)			
				return;
			
			Mediator md;
			if(md.getRefreshButtonView() == nil)
			{
				//CA("refresh button view is nil");
			}
			else
			{
				//CA("refresh button enabled");
				md.getRefreshButtonView()->Enable();
			}
			//Level 3
			//CA(" subsection clicked ");
			
			PMString SubSecName("");

			double subsectionID = getSelectedSubsectionId(controlView, SubSecName);
			CurrentSelectedSubSection = subsectionID;
			Mediator::sectionID = subsectionID; 
			bool16 isListBoxPopulated = kFalse;
			/*if(FilterFlag == kTrue)
			{
				isListBoxPopulated = populateProductListforSectionWithDesignerActions(subsectionID,GlobalDesignerActionFlags[0],GlobalDesignerActionFlags[1],GlobalDesignerActionFlags[2],GlobalDesignerActionFlags[3],GlobalDesignerActionFlags[4],GlobalDesignerActionFlags[5],GlobalDesignerActionFlags[6], GlobalDesignerActionFlags[7]);
			}
			else*/
			{
				isListBoxPopulated = this->populateProductListforSection(subsectionID);
			}
			if(!isListBoxPopulated)
			{
					
			}
			md.setCurrSubSectionID(subsectionID);
			md.setCurSubSecName(SubSecName);
	
			if(strcmp(SubSecName.GetPlatformString().c_str(),"") == 0)
			{
				/*if(Mediator::ResizeFlag == 1)			
					this->hideImagePanel();*/
				/*
					hide image panel ..... 
				*/
				//InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(/*kProductImageIFaceBoss*/kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
			 //   if(ptrImageHelper != nil)
			 //   {
				//	ptrImageHelper->closeImagePanel();					
			 //   }

				
			}

			if(subsectionID>0)
			{		
				if(FilterFlag == kTrue)
				{
					if(md.getAssignBtnView()==nil)
					{
						//CA("Dialog Observer::m.getAssignBtnView()==nil");
					}
					else						
						md.getAssignBtnView()->HideView();		

					if(md.getGreenFilterBtnView()==nil)
					{
						//CA("Dialog Observer::m.getGreenFilterBtnView()==nil");
					}
					else
					{
						md.getGreenFilterBtnView()->ShowView();	
						md.getGreenFilterBtnView()->Enable();
					}
				}
				else 
				{
					if(md.getAssignBtnView()==nil)
					{						
					}
					else
					{
						md.getAssignBtnView()->ShowView();
						md.getAssignBtnView()->Enable();	
					}
					
					if(md.getGreenFilterBtnView()==nil)
					{
						//CA("Dialog Observer::m.getGreenFilterBtnView()==nil");
					}
					else
					{
						md.getGreenFilterBtnView()->HideView();
					}
				}

				if(md.getSubSecSprayButtonView()== nil)
				{
					
				}
				else
					md.getSubSecSprayButtonView()->Enable();
				
				
				if(md.getIconView()== nil)
				{
					
				}
				else
					md.getIconView()->Enable();

				if(md.getIconViewNew()== nil)
				{
					
				}
				else
					md.getIconViewNew()->Enable();
				
				/*if(md.getClearBtnView()== nil)
				{
					CA("md.getClearBtnView()==nil");
				}
				else
				md.getClearBtnView()->Disable();*/
				////////////////8.4.05////////////////
				if(md.getPreviewWidgetView() == nil)
				{
					
				}
				else
					md.getPreviewWidgetView()->Enable();

				//if(md.getLocateWidgetView() == nil)//--COMMENTED ON 25 FEB 2006 --
				//{
				//	//CA("md.getLocateWidgetView()==nil");
				//}
				//else
				//	md.getLocateWidgetView()->Enable();
				////////////////8.4.0f////////////////				
			}
			else
			{
				if(FilterFlag == kTrue)
				{
					if(md.getAssignBtnView()==nil)
					{
						//CA("Dialog Observer::m.getAssignBtnView()==nil");
					}
					else						
						md.getAssignBtnView()->HideView();		

					if(md.getGreenFilterBtnView()==nil)
					{
						//CA("Dialog Observer::m.getGreenFilterBtnView()==nil");
					}
					else
					{
						md.getGreenFilterBtnView()->ShowView();	
						//md.getGreenFilterBtnView()->Disable();
					}
				}
				else
				{
					if(md.getAssignBtnView()==nil)
					{						
					}
					else
					{
						md.getAssignBtnView()->ShowView();
						//md.getAssignBtnView()->Disable();
					}

					if(md.getGreenFilterBtnView()==nil)
					{
						//CA("Dialog Observer::m.getGreenFilterBtnView()==nil");
					}
					else
					{
						md.getGreenFilterBtnView()->HideView();							
					}
				}

				/*if(md.getClearBtnView() == nil)
				{
					CA("md.getClearBtnView()==nil");
				}
				else				
				md.getClearBtnView()->Disable();*/


				if(md.getSubSecSprayButtonView() == nil)
				{
					
				}
				else
					md.getSubSecSprayButtonView()->Disable();
				
				if(md.getIconView() == nil)
				{
					
				}
				else
				{
					md.getIconView()->Disable();				
					md.setCurrSectionID(-1);				
				}

				if(md.getIconViewNew() == nil)
				{
					
				}
				else
				{
					md.getIconViewNew()->Disable();				
					md.setCurrSectionID(-1);				
				}
				//md.setCurrentSelectedRow(0);

				if(md.getPreviewWidgetView() == nil)
				{
					
				}
				else
					md.getPreviewWidgetView()->Disable();

				/*if(md.getLocateWidgetView() == nil) //--COMMENTED ON 25 FEB 2006 --
				{
					CA("md.getLocateWidgetView()==nil");
				}
				else
					md.getLocateWidgetView()->Disable();*/
			
			}

			if(isListBoxPopulated)
			{
				if(md.getRefreshButtonView() == nil)
				{
				}
				else
					md.getRefreshButtonView()->Enable();				
				//md.getSubSecSprayButtonView()->Enable();			
				
				if(FilterFlag == kTrue)
				{
					if(md.getGreenFilterBtnView() == nil)
					{
						//CA("md.getGreenFilterBtnView()==nil");
					}
					else
					{
						md.getGreenFilterBtnView()->ShowView();
						md.getGreenFilterBtnView()->Enable();
					}
					//CA("isListBoxPopulated 3");
					if(md.getAssignBtnView() == nil)
					{
						//CA("md.getAssignBtnView()==nil");
					}
					else
						md.getAssignBtnView()->HideView();

				}
				else
				{
					if(md.getGreenFilterBtnView() == nil)
					{
						//CA("md.getGreenFilterBtnView()==nil");
					}
					else
						md.getGreenFilterBtnView()->HideView();
					//CA("isListBoxPopulated 3");
					if(md.getAssignBtnView() == nil)
					{
						//CA("md.getAssignBtnView()==nil");
					}
					else
					{
						md.getAssignBtnView()->ShowView();
						md.getAssignBtnView()->Enable();
					}
				}
					//CA("isListBoxPopulated 4");
			}
			else
			{
				//----------------disable show white board button-----------------------//
				IControlView* whiteBoardView = iPanelControlData->FindWidget(kSPShowWhiteBoardWidgetID);
				if(whiteBoardView)
				{
					whiteBoardView->Disable();
					/*
						now close imagepanel .....
					*/
					//InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(/*kProductImageIFaceBoss*/ kDCNProductImageIFaceBoss,IPRImageHelper::kDefaultIID))));
					//if(ptrImageHelper != nil)
					//{
					//	ptrImageHelper->closeImagePanel();
					//}

					
				}
				//IControlView* TemplateBtnView = iPanelControlData->FindWidget(kSPTemplatePreviewWidgetID);
				//if(TemplateBtnView)
				//{
				//	TemplateBtnView->Disable();
				//	/*
				//		now close imagepanel .....
				//	*/
				//	/*InterfacePtr<IPRImageHelper> ptrImageHelperTempBtn((static_cast<IPRImageHelper*> (CreateObject(kDDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID1))));
				//	if(ptrImageHelperTempBtn != nil)
				//	{
				//		ptrImageHelperTempBtn->closeImagePanel();
				//	}*/

				//	
				//}
				//---------------------------------------------------------------------//

				if(md.getRefreshButtonView() == nil)
				{
					//CA("md.getRefreshButtonView()==nil");
				}
				else
					md.getRefreshButtonView()->Enable();
				
				//md.getSubSecSprayButtonView()->Disable();	
				if(FilterFlag == kTrue)
				{
					if(md.getGreenFilterBtnView() == nil)
					{
						
					}
					else
					{
						md.getGreenFilterBtnView()->ShowView();
						//md.getGreenFilterBtnView()->Disable();
					}
					
					if(	md.getAssignBtnView() == nil)
					{
						//CA("md.getAssignBtnView()==nil");
					}
					else
					{
						md.getAssignBtnView()->HideView();						
					}
				}
				else
				{
					if(md.getGreenFilterBtnView() == nil)
					{
						
					}
					else
						md.getGreenFilterBtnView()->HideView();
					
					if(	md.getAssignBtnView() == nil)
					{
						//CA("md.getAssignBtnView()==nil");
					}
					else
					{
						md.getAssignBtnView()->ShowView();
						//md.getAssignBtnView()->Disable();
					}
				}
				//CA("isListBoxPopulated  else 5");
				if(md.getPreviewWidgetView()== nil)
				{
					//CA("md.getPreviewWidgetView()==nil");
				}
				else
				{
                    md.getPreviewWidgetView()->Disable();
				}

				if(md.getSubSecSprayButtonView() == nil)
				{
					
				}
				else
					md.getSubSecSprayButtonView()->Disable();

				//CA("isListBoxPopulated  else 6");
				/*if(md.getLocateWidgetView() == nil)//--COMMENTED ON 25 FEB 2006 --
				{
					CA("md.getLocateWidgetView() == nil");
				}
				else
					md.getLocateWidgetView()->Disable();*/
				//CA("isListBoxPopulated  else 7");
				//md.getSprayButtonView()->Disable();
			}

			break ;
		}

		//---------------------- section dropdown (when level is 2) clicked  --------------- 
		if(theSelectedWidget == kSPSectionDropDown_1WidgetID  && theChange == kPopupChangeStateMessage)
		{
			// Level 2
			//CA("from here");
			bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
			if(!isUserLoggedIn)			
				return;
			//CA("SectionDropDownWidgetID	2.1");
			
			InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
			if(!ptrIClientOptions)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No ptrIClientOptions ");					
				break;
			}

			double lang_id = -1 ;
			PMString lang_name("");

			//lang_id = ptrIClientOptions->getDefaultLocale(lang_name);
			/*fglobal_lang_id = lang_id ;*/


			PMString as ;
			Mediator md;
			double subsectionID = getSelectedSectionId(controlView,as);

			md.setCurrSubSectionID(subsectionID);
			CurrentSelectedSection = subsectionID;
			CurrentSelectedSubSection = subsectionID;
			Mediator::sectionID = subsectionID;  // section ID Vaibhav_24April
			bool16 isListBoxPopulated = kFalse;
			
			InterfacePtr<IClientOptions> cop((IClientOptions*) ::CreateObject(kClientOptionsReaderBoss,IID_ICLIENTOPTIONS));
			if(cop == nil)
			{
				//CA("cop == nil");
				ptrIAppFramework->LogDebug("AP7_CategoryBrowser::CTBSelectionObserver::Update::cop == nil");
				break;
			}

			PMString nilSectionName = "";
			double nilSectionID = -1;

			if(ptrIAppFramework->get_isONEsourceMode())
			{
				cop->setPublication(as , Mediator::sectionID);
			}
			else
			{
				//CA("!!!ptrIAppFramework->get_isONEsourceMode()");
				CPubModel pubModelRootValue = ptrIAppFramework->getpubModelByPubID(Mediator::sectionID,global_lang_id);
				
				double rootPubID = pubModelRootValue.getRootID();
				CPubModel pubRootValue = ptrIAppFramework->getpubModelByPubID(rootPubID,global_lang_id);
				PMString rootPubName = pubRootValue.getName();
				

				cop->setPublication(rootPubName , rootPubID);
				if(rootPubID == Mediator::sectionID)
				{
					cop->setSection(nilSectionName,nilSectionID );
				}
				else
				{
					cop->setSection(as,Mediator::sectionID );
				}
				
			}

			
			/*if(FilterFlag == kTrue)
			{
				isListBoxPopulated = populateProductListforSectionWithDesignerActions(subsectionID,GlobalDesignerActionFlags[0],GlobalDesignerActionFlags[1],GlobalDesignerActionFlags[2],GlobalDesignerActionFlags[3],GlobalDesignerActionFlags[4],GlobalDesignerActionFlags[5],GlobalDesignerActionFlags[6], GlobalDesignerActionFlags[7]);
			}
			else*/
				isListBoxPopulated = this->populateProductListforSection(subsectionID);
		
			if(subsectionID>0)
			{				
				//CA("SectionDropDownWidgetID	2.1");
				if(FilterFlag == kTrue)
				{
					if(md.getAssignBtnView()==nil)
					{
						//CA("Dialog Observer::m.getAssignBtnView()==nil");
					}
					else						
						md.getAssignBtnView()->HideView();

					if(md.getGreenFilterBtnView()==nil)
					{
						//CA("Dialog Observer::m.getGreenFilterBtnView()==nil");
					}
					else
					{
						md.getGreenFilterBtnView()->ShowView();	
						md.getGreenFilterBtnView()->Enable();
					}
				}
				else 
				{
					if(md.getAssignBtnView()==nil)
					{						
					}
					else
					{
						md.getAssignBtnView()->ShowView();
						md.getAssignBtnView()->Enable();	
					}
					
					if(md.getGreenFilterBtnView()==nil)
					{
						//CA("Dialog Observer::m.getGreenFilterBtnView()==nil");
					}
					else
					{
						md.getGreenFilterBtnView()->HideView();						
					}
				}				

				if(md.getSubSecSprayButtonView()== nil)
				{
					//CA("md.getSubSecSprayButtonView()==nil");
				}
				else
					md.getSubSecSprayButtonView()->Enable();				
				
				if(md.getIconView()== nil)
				{
					//CA("md.getIconView()==nil");
				}
				else
					md.getIconView()->Enable();

				if(md.getIconViewNew()== nil)
				{
					//CA("md.getIconView()==nil");
				}
				else
					md.getIconViewNew()->Enable();
				
				/*if(md.getClearBtnView()== nil)
				{
					CA("md.getClearBtnView()==nil");
				}
				else
				md.getClearBtnView()->Disable();*/
				////////////////8.4.05////////////////
				if(md.getPreviewWidgetView() == nil)
				{
					//CA("md.getPreviewWidgetView()==nil");
				}
				/*else
					md.getPreviewWidgetView()->Disable();*/ //-- commented 15 feb mangesh 

				/*if(md.getLocateWidgetView() == nil)//--COMMENTED ON 25 FEB 2006 --
				{
					CA("md.getLocateWidgetView()==nil");
				}*/
				/*else
					md.getLocateWidgetView()->Disable();*/// -- commented 15 feb mangesh 
				////////////////8.4.0f////////////////				
			}
			else
			{
				
				//CA("SectionDropDownWidgetID	2.2");
				if(md.getAssignBtnView()==nil)
				{
					//CA("md.getAssignBtnView()==nil");
				}
				/*else
				md.getAssignBtnView()->Disable();*//// -- commented 15 feb mangesh 

				/*if(md.getClearBtnView() == nil)
				{
					CA("md.getClearBtnView()==nil");
				}
				else				
				md.getClearBtnView()->Disable();*/

				if(md.getSubSecSprayButtonView() == nil)
				{
					//CA("md.getSubSecSprayButtonView()==nil");
				}
				/*else
				md.getSubSecSprayButtonView()->Disable();*//// -- commented 15 feb mangesh 
				
				if(md.getIconView() == nil)
				{
					//CA("md.getIconView()==nil");
				}
				else
					md.getIconView()->Disable();	

				if(md.getIconViewNew() == nil)
				{
					//CA("md.getIconView()==nil");
				}
				else
					md.getIconViewNew()->Disable();
					md.setCurrSectionID(-1);				
					md.setCurrentSelectedRow(0);

				if(md.getPreviewWidgetView() == nil)
				{
					//CA("md.getPreviewWidgetView()==nil");
				}
				/*else
				md.getPreviewWidgetView()->Disable();*//// -- commented 15 feb mangesh 


				/*if(md.getLocateWidgetView() == nil)//--COMMENTED ON 25 FEB 2006 --
				{
					CA("md.getLocateWidgetView()==nil");
				}*/
				/*else
				md.getLocateWidgetView()->Disable();*//// -- commented 15 feb mangesh 
			
			}

			if(isListBoxPopulated)
			{
				//CA("isListBoxPopulated 1");
				md.getTableSourceBtnView()->Enable();	//-------
				if(md.getRefreshButtonView() == nil)
				{
					//CA("md.getRefreshButtonView()==nil");
				}
				else
                    md.getRefreshButtonView()->Enable();//--/ -- commented 15 feb mangesh
			
				if(FilterFlag == kTrue)
				{
					if(md.getGreenFilterBtnView() == nil)
					{
						//CA("md.getGreenFilterBtnView()==nil");
					}
					else
					{
						md.getGreenFilterBtnView()->ShowView();
						md.getGreenFilterBtnView()->Enable();
					}
					//CA("isListBoxPopulated 3");
					if(md.getAssignBtnView() == nil)
					{
						//CA("md.getAssignBtnView()==nil");
					}
					else
					md.getAssignBtnView()->HideView();

				}
				else
				{
					if(md.getGreenFilterBtnView() == nil)
					{
						//CA("md.getGreenFilterBtnView()==nil");
					}
					else
					md.getGreenFilterBtnView()->HideView();
					//CA("isListBoxPopulated 3");
					if(md.getAssignBtnView() == nil)
					{
						//CA("md.getAssignBtnView()==nil");
					}
					else
					{
						md.getAssignBtnView()->ShowView();
						md.getAssignBtnView()->Enable();
					}
				}
			}
			else
			{
				
				//----------------disable show white board button-----------------------//
				IControlView* whiteBoardView = iPanelControlData->FindWidget(kSPShowWhiteBoardWidgetID);
				if(whiteBoardView)
				{
					whiteBoardView->Disable();
					/*
						now close imagepanel .....
					*/
					//InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(/*kProductImageIFaceBoss*/kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
					//if(ptrImageHelper != nil)
					//{
					//	ptrImageHelper->closeImagePanel();					
					//}

					
				}
				/*IControlView* TemplateBtndView = iPanelControlData->FindWidget(kSPTemplatePreviewWidgetID);
				if(TemplateBtndView)
				{
					TemplateBtndView->Disable();
				}*/
				//---------------------------------------------------------------------//
				
				md.getTableSourceBtnView()->Disable();	//-------
				md.getTableSourceBtnView()->Enable();
				//CA("isListBoxPopulated  else 1");
				if(md.getRefreshButtonView() == nil)
				{
					//CA("md.getRefreshButtonView()==nil");
				}
				else
                    md.getRefreshButtonView()->Enable();
				//CA("isListBoxPopulated  else 2");
				//md.getSubSecSprayButtonView()->Disable();	//----
				md.getSubSecSprayButtonView()->Enable();
				if(FilterFlag == kTrue)
				{
					//CA("FilterFlag == kTrue");
					if(	md.getAssignBtnView() == nil)
					{
						//CA("md.getAssignBtnView()==nil");
					}
					else
					{
						md.getAssignBtnView()->HideView();	
					}
					if(md.getGreenFilterBtnView() == nil)
					{
						//CA("md.getGreenFilterBtnView() == nil");
					}
					else
					{
						md.getGreenFilterBtnView()->ShowView();
						md.getGreenFilterBtnView()->Enable();
					}
					
				}
				else
				{
					if(md.getGreenFilterBtnView() == nil)
					{
						
					}
					else{
						md.getGreenFilterBtnView()->HideView();
					}
					
					if(	md.getAssignBtnView() == nil)
					{
						//CA("md.getAssignBtnView()==nil");
					}
					else
					{
						//md.getAssignBtnView()->ShowView();
						md.getAssignBtnView()->Disable();	//-------
					}
				}
				//CA("isListBoxPopulated  else 5");
				if(md.getPreviewWidgetView()== nil)
				{
					//CA("md.getPreviewWidgetView()==nil");
				}
				else
				{
                    md.getPreviewWidgetView()->Disable();
				}

				//CA("isListBoxPopulated  else 6");
				/*if(md.getLocateWidgetView() == nil)//--COMMENTED ON 25 FEB 2006 --
				{
					CA("md.getLocateWidgetView() == nil");
				}
				else
					md.getLocateWidgetView()->Disable();*/
				//CA("isListBoxPopulated  else 7");
				//md.getSprayButtonView()->Disable();
			}
			//FilterFlag = kFalse;
		}
	
		//Added  on 19/09 For Showing Class and related products
		if(theSelectedWidget == kSPSelectClassWidgetID && theChange==kTrueStateMessage)
		{
			//CA("theSelectedWidget == kSPSelectClassWidgetID");
			bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
			if(!isUserLoggedIn)			
				return;

			InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
			if(iApplication==nil)
			{
				//CA("iApplication==nil");
				break;
			}

			InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
			if(iPanelMgr == nil)
			{
				//CA("iPanelMgr == nil");
				break;
			}
			UID paletteUID = kInvalidUID;
			int32 TemplateTop	=0;	
			int32 TemplateLeft	=0;	
			int32 TemplateRight	=0;	
			int32 TemplateBottom =0;
			const ActionID MyPalleteActionID =kSPPanelWidgetActionID;

			IControlView* pnlControlView = iPanelMgr->GetPanelFromActionID(MyPalleteActionID);
			if(pnlControlView == NULL)
			{
				//CA("pnlControlView is NULL");
				break;
			}

			InterfacePtr<IWidgetParent> panelWidgetParent( pnlControlView, UseDefaultIID());
			if(panelWidgetParent == NULL)
			{
				//CA("panelWidgetParent is NULL");
				break;
			}
			InterfacePtr<IWindow> palette((IWindow*)panelWidgetParent->QueryParentFor(IWindow::kDefaultIID));
			if(palette == NULL)
			{
				//CA("palette is NULL");
				break;
			}

            InterfacePtr<IPMUnknown> unknown(iPanelMgr, IID_IUNKNOWN);
            PaletteRef palRef=iPanelMgr->GetPaletteRefContainingPanel(unknown);

            bool16 retval = PaletteRefUtils::IsPaletteVisible(palRef);
			
			if(palette)						//if(palette)
			{
				//CA("pallete found ");
				palette->AddRef();
				GSysRect PalleteBounds = palette->GetFrameBBox();
		
                #ifdef WINDOWS
                    TemplateTop		= PalleteBounds.top -30;
                    TemplateLeft	= PalleteBounds.left;
                    TemplateRight	= PalleteBounds.right;
                    TemplateBottom	= PalleteBounds.bottom;
                #else
                    TemplateTop		=SysRectTop(PalleteBounds)-30;
                    TemplateLeft    =SysRectLeft(PalleteBounds);
                    TemplateRight   =SysRectRight(PalleteBounds);
                    TemplateBottom  =SysRectBottom(PalleteBounds);
                #endif
                
				InterfacePtr<ICategoryBrowser> CatalogBrowserPtr((ICategoryBrowser*)::CreateObject(kCTBCategoryBrowserBoss, IID_ICATEGORYBROWSER));
				if(!CatalogBrowserPtr)
				{
					ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::Update::kSPSelectClassWidgetID--No CatalogBrowserPtr");
					return ;
				}
				CatalogBrowserPtr->OpenCategoryBrowser(TemplateTop, TemplateLeft, TemplateBottom, TemplateRight ,1 , -1);
			}
		}
		if(theSelectedWidget == kSPRevertSearchWidgetID && theChange==kTrueStateMessage)			
		{
			//CA("kSPRevertSearchWidgetID");
			/*Mediator md;
			InterfacePtr<ISearch> SearchPtr((ISearch*)::CreateObject(kSEASearchBoss, IID_ISEARCH));
			if(!SearchPtr)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::update::Pointre to ISearch not found");
				return ;
			}
			SearchPtr->CloseSearchPalette();
			searchResult = kFalse;
			if(FilterFlag ==  kTrue)
			{					
				populateProductListforSectionWithDesignerActions(md.getCurrSubSectionID(),GlobalDesignerActionFlags[0],GlobalDesignerActionFlags[1],GlobalDesignerActionFlags[2],GlobalDesignerActionFlags[3],GlobalDesignerActionFlags[4],GlobalDesignerActionFlags[5],GlobalDesignerActionFlags[6], GlobalDesignerActionFlags[7]);
			}
			else
			{
				populateProductListforSection(md.getCurrSubSectionID());
			}*/
		}
		if(theSelectedWidget == kSPsearchWidgetID && theChange==kTrueStateMessage)			
		{
			////CA("theSelectedWidget == kSPsearchWidgetID");
			//searchResult = kTrue;
			//InterfacePtr<IApplication>iApplication(GetExecutionContextSession()->QueryApplication()); 
			//if(iApplication==nil){ 
			//	//CA("No iApplication");
			//	ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::iApplication==nil");
			//	break;
			//}
			//InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
			//if(iPanelMgr == nil){ 
			//	ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::iPanelMgr == nil");
			//	break;
			//}

			//IControlView* pnlControlView = iPanelMgr->GetPanelFromWidgetID(kSPPanelWidgetID);
			//if(pnlControlView == NULL)
			//{
			//	//CA("pnlControlView is NULL");
			//	break;
			//}
			//UID paletteUID = kInvalidUID;
			//int32 TemplateTop	=0;	
			//int32 TemplateLeft	=0;	
			//int32 TemplateRight	=0;	
			//int32 TemplateBottom =0;	
			//const ActionID MyPalleteActionID = kSPPanelWidgetActionID;

			//InterfacePtr<IWidgetParent> panelWidgetParent( pnlControlView, UseDefaultIID());
			//if(panelWidgetParent == NULL)
			//{
			//	//CA("panelWidgetParent is NULL");
			//	break;
			//}
			//InterfacePtr<IWindow> palette((IWindow*)panelWidgetParent->QueryParentFor(IWindow::kDefaultIID));
			//if(palette == NULL)
			//{
			//	//CA("palette is NULL");
			//	break;
			//}

			//PaletteRef palRef=iPanelMgr->GetPaletteRefContainingPanel(pnlControlView);

			//if(!palRef.IsValid())
			//{
			//	//CA("IsInValid ");
			//}

			//bool16 retval = PaletteRefUtils::IsPaletteVisible(palRef);
	
			//if(palette)
			//{
			//		
			//	//CA("pallete found ");
			//	palette->AddRef();
			//	GSysRect PalleteBounds = palette->GetFrameBBox();

			//	TemplateTop		= PalleteBounds.top -30;
			//	TemplateLeft	= PalleteBounds.left;
			//	TemplateRight	= PalleteBounds.right;
			//	TemplateBottom	= PalleteBounds.bottom;

			//	InterfacePtr<ISearch> SearchPtr((ISearch*)::CreateObject(kSEASearchBoss, IID_ISEARCH));
			//	if(!SearchPtr)
			//	{
			//		ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::Pointre to TemplateBuilderPtr not found");
			//		return ;
			//	}
			//	SearchPtr->OpenSearchPalette(TemplateTop, TemplateLeft, TemplateBottom, TemplateRight);
			//	palette->Release();
			//}

			//Mediator md;
			//IPanelControlData* iPanelControlData = md.getMainPanelCtrlData();
			//IControlView* iPubGPCtrlView = iPanelControlData->FindWidget(kSPPublicationGPWidgetID);
			//IControlView* iONEGPCtrlView = iPanelControlData->FindWidget(kSPONEGPWidgetID);
			//IControlView* iSSPGPCtrlView = iPanelControlData->FindWidget(kSPSearchPnlWidgetID);

			//if(searchResult)
			//{
			//	//iSSPGPCtrlView->ShowView();
			//	//iPubGPCtrlView->HideView();
			//	//iONEGPCtrlView->HideView();
			//}
			//else
			//{
			//	iSSPGPCtrlView->HideView();
			//	iPubGPCtrlView->ShowView();
			//	iONEGPCtrlView->ShowView();
			//}
		}
	}while (kFalse);
}

void SPSelectionObserver::loadPaletteData()
{ 
	//CA(__FUNCTION__);
	
	Mediator md;

	int32 level;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return;
	}
	if(check == 0)
	{
		//CA("LoadPaletteData 1");
		flg = 1;
		return;
	}

	if(NewFlgForMinmise == 1)
	{
		//CA("NewFlgForMinmise return");
		NewFlgForMinmise = 0;

		InterfacePtr<ILoginHelper> ptrLogInHelper(static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss,ILoginHelper::kDefaultIID)));
		if(ptrLogInHelper == nil)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::ptrLogInHelper == nil ");
			return ;
		}
		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(!ptrIClientOptions)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No ptrIClientOptions ");
			return;
		}
		Mediator md;
		double previousSectionID = md.getCurrSectionID();  //-when Content Sprayer is closed, and select Event\Section ID than this Method contain Previous selected Event\Section ID. 

		LoginInfoValue cserverInfoValue;
		bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue);
		
		ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();

		PMString strSectionID =	clientInfoObj.getSectionID();
		double section_ID = strSectionID.GetAsDouble();
		//PMString strIsONE = clientInfoObj.getIsONESource();

		PMString strIsONE = "false";
		///int32 section_ID = -1;


		PMString defPubName("");
		double defPubId=-1;

		defPubId = ptrIClientOptions->getDefPublication(defPubName);
		
		if(defPubId <= 0)
			return;
		
		SDKListBoxHelper listHelper(this, kSPPluginID);	

		InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
		if(iPanelControlData == nil)
		{
			return;
		}
		IControlView * listBox = listHelper.FindCurrentListBox(iPanelControlData, 1);
		if(listBox != nil) 
		{
			//CA("listBox != nil");
			//IPanelControlData* iPanelControlData;
			IControlView * listBox1 = listHelper.FindCurrentListBox(iPanelControlData, 1);
			if(listBox1 != nil) 
			{
				InterfacePtr<IListBoxController> listCntl(listBox1,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
				if(listCntl)
				{
					K2Vector<int32> multipleSelection ;
					listCntl->GetSelected( multipleSelection );
					const int32 kSelectionLength = multipleSelection.size();
					if(kSelectionLength > 1)
					{
						listCntl->DeselectAll();
					}
				}
			}
		}
			
			
		if(strIsONE  == "false")
		{
			if(section_ID != -1)
			{
				if(section_ID == previousSectionID)
					return;
			}
			else
			{
				if(defPubId == previousSectionID)
					return;
			}
		}
		else
		{
			return;
		}

	}
	if(md.getLoadDataFlag()==kTrue)
	{
		//CA("LoadPaletteData 2");
	}

	do
	{
		//CA("LoadPaletteData 3");
		IPanelControlData* iPanelControlData;
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CA(" ptrIAppFramework nil ");
			break;
		}

		bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();

		if(!md.getMainPanelCtrlData())
		{		
			//CA("!md.getMainPanelCtrlData()....and going to call QueryPanelControlData");
			//iPanelControlData = QueryPanelControlData();
			//added by avinash
			InterfacePtr<IApplication>iApplication(GetExecutionContextSession()->QueryApplication()); 
			if(iApplication==nil){ 
				//CA("No iApplication");
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::iApplication==nil");
				break;
			}
			InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
			if(iPanelMgr == nil){ 
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::iPanelMgr == nil");
				break;
			}

			IControlView* pnlControlView = iPanelMgr->GetPanelFromWidgetID(kSPPanelWidgetID);
			if(pnlControlView == NULL)
			{
				//CA("pnlControlView is NULL");
				break;
			}
			InterfacePtr<IWidgetParent> panelWidgetParent( pnlControlView, UseDefaultIID());
			if(panelWidgetParent == NULL)
			{
				//CA("panelWidgetParent is NULL");
				break;
			}
			InterfacePtr<IPanelControlData> iPanelControlData2(panelWidgetParent->GetParent(), UseDefaultIID());
			if(iPanelControlData2 != nil)
			{
				//CA("Not Nil");
				iPanelControlData2->AddRef();
				iPanelControlData = iPanelControlData2;
			}
			else
			{
				//CA("NIL");
				iPanelControlData = nil;
			}

			// for testing
			if(iPanelControlData == nil)
			{					
				//CA("Panel Control Data is nil11111111111111111");
				md.setMainPanelCntrlData(md.getMainPanelCtrlData1());
				//CA("asdddddd");
				if(md.getMainPanelCtrlData1() == nil)
				{
						//CA("FFFFFFFFFFFFFFFF");
				}
				else
						//CA("NOT NULL");
				break;
			}
			else
			{						
				md.setMainPanelCntrlData(iPanelControlData);
				md.setMainPanelCntrlData1(iPanelControlData);
			}
		}

			
		//CA("LoadPaletteData 5");
		SPActionComponent actionObsever(this);		
		actionObsever.DoPalette();
		iPanelControlData=md.getMainPanelCtrlData();
		
		if(!iPanelControlData)
		{
			//////CA("LoadPaletteData 11");
			//CA("No panelcontrol data");
			break;
		}
		
//-----------Added on 23/09 to hide level 4 panel initially
		IControlView* SPOneView = iPanelControlData->FindWidget( kSPOnePnlWidgetID);
		if(SPOneView == NULL)
		{
			//CA("SPOneView == NULL");
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::SPOneView  == NULL");
			break;
		}
		SPOneView->HideView();

		bool16 isONE = kFalse;			
		isONE=ptrIAppFramework->get_isONEsourceMode();
		//If Master(ONEsource) then..
		if(isONE == kTrue)
		{
			//CA(" level==4 ");
			//global_project_level=4;		//------------
			//level=4;
			
			global_project_level=2;
			level=2;
			isONEsource = kTrue;

			//this->CallLevelTwoThree();
			
			//IControlView* SPOneView = iPanelControlData->FindWidget( kSPOnePnlWidgetID);
			//if(SPOneView == NULL)
			//{
			//	ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No SPOneView ");
			//	break;
			//}
			//SPOneView->ShowView(); //Show the panel having showClass widget(having Master(ONEsource))
			//SPOneView->Enable();
			//this->levelFour();
		}
		else
		{
			//CA(" else level=4 ");
			//level = ptrIAppFramework->getPM_Project_Levels(); //---Comment For Always Open Project Level 2 ------
			level = 2;	//--------------
			global_project_level = level ;
			isONEsource = kFalse;
			this->CallLevelTwoThree();

		}
	//------------------------------------upto here
		/*PMString e("global_project_level  :  ");
		e.AppendNumber(global_project_level);
		CA(e);*/

		/*IControlView* ThumbWidgetView = iPanelControlData->FindWidget( kSPThumbPnlWidgetID);
		if(ThumbWidgetView == NULL)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No ThumbWidgetView ");
			break;
		}
		ThumbWidgetView->HideView();*/

		//---------------------- this part will get level and set one or 2 drop down visible -------------//
		//int32 level = ptrIAppFramework->getPM_Project_Levels();
		//global_project_level = level ;

		if(level == 3)
		{
			//CA("level == 3");
			Mediator::sectionID = CurrentSelectedSubSection;
			PMString secStr("");
			secStr.AppendNumber(CurrentSelectedSection);
			//CA("Selected sub section section");
			//CA(secStr);
			//-- all text widgets 
			IControlView *sectionTextView = iPanelControlData->FindWidget(kSectionTextWidgetID);
			if(sectionTextView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No sectionTextView ");
				break;
			}

			IControlView *subSectionTextView = iPanelControlData->FindWidget(kSubSectionTextWidgetID);
			if(subSectionTextView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No subSectionTextView ");				
				break;
			}

			IControlView *sectionText1View = iPanelControlData->FindWidget(kSectionText_1WidgetID);
			if(sectionText1View == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No sectionText1View ");								
				break;
			}

			sectionTextView->ShowView();
			subSectionTextView->ShowView();
			sectionText1View->HideView();
			//----------------- all drop downs 
			IControlView *sectionDrop = iPanelControlData->FindWidget(kSPSectionDropDownWidgetID);
			if(sectionDrop == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No sectionDrop ");												
				break;
			}

			IControlView *subsectionDrop = iPanelControlData->FindWidget(kSPSubSectionDropDownWidgetID);
			if(subsectionDrop == nil)
				break;

			IControlView *sectionDrop1 = iPanelControlData->FindWidget(kSPSectionDropDown_1WidgetID);
			if(sectionDrop1 == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No sectionDrop1 ");																
				break;
			}

			sectionDrop->ShowView();
			subsectionDrop->ShowView();
			sectionDrop1->HideView();
			
		}

		if(level == 2)
		{				
			//CA("level == 2");
			Mediator::sectionID = CurrentSelectedSection;
			PMString secStr("");
			secStr.AppendNumber(CurrentSelectedSection);
			//CA("Selected section");
			//CA(secStr);
			//-- all text widgets 
			IControlView *sectionTextView = iPanelControlData->FindWidget(kSectionTextWidgetID);
			if(sectionTextView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No sectionTextView ");																				
				break;
			}

			IControlView *subSectionTextView = iPanelControlData->FindWidget(kSubSectionTextWidgetID);
			if(subSectionTextView == nil)
				break;

			IControlView *sectionText1View = iPanelControlData->FindWidget(kSectionText_1WidgetID);
			if(sectionText1View == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No sectionText1View ");				
				break;
			}

			sectionTextView->HideView();
			subSectionTextView->HideView();
			sectionText1View->ShowView();
			sectionText1View->Enable();
			//----------------- all drop downs 
			IControlView *sectionDrop = iPanelControlData->FindWidget(kSPSectionDropDownWidgetID);
			if(sectionDrop == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No sectionDrop ");				
				break;
			}

			IControlView *subsectionDrop = iPanelControlData->FindWidget(kSPSubSectionDropDownWidgetID);
			if(subsectionDrop == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No subsectionDrop ");
				break;
			}

			IControlView *sectionDrop1 = iPanelControlData->FindWidget(kSPSectionDropDown_1WidgetID);
			if(sectionDrop1 == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No sectionDrop1 ");				
				break;
			}

			sectionDrop->HideView();
			subsectionDrop->HideView();
			sectionDrop1->ShowView();
			sectionDrop1->Enable();
		}
					
		IControlView* showPubTreeView=iPanelControlData->FindWidget(kSPShowPubTreeWidgetID);
		if(showPubTreeView)
		{
			showPubTreeView->Enable();
			md.setShowPubTreeView(showPubTreeView);
		}
		IControlView* searchView=iPanelControlData->FindWidget(kSPsearchWidgetID);
		if(searchView)
		{
			searchView->Enable();
			md.setSearchView(searchView);
		}
		
		IControlView* whiteBoardView=iPanelControlData->FindWidget(kSPShowWhiteBoardWidgetID);
		if(whiteBoardView)
		{
			whiteBoardView->Disable();		
		}
		
		IControlView* spryButtonView=iPanelControlData->FindWidget(kSPSprayButtonWidgetID);
		if(spryButtonView)
		{
			spryButtonView->Disable();
		}
		IControlView* refreshButtonView=iPanelControlData->FindWidget(kSPRefreshWidgetID);
		if(refreshButtonView)
		{
			refreshButtonView->Enable();
		}

		//IControlView* AssignBtnView=iPanelControlData->FindWidget(kAssignButtonWidgetID);
		//if(AssignBtnView)
		//{
		//	//CA("LoadPaletteData 21");
		//	AssignBtnView->ShowView();
		//	//AssignBtnView->Disable();
		//	AssignBtnView->Enable();
		//}			
		
		IControlView* GreenFilterBtnView=iPanelControlData->FindWidget(kGreenFilterButtonWidgetID);
		if(GreenFilterBtnView)
		{
			GreenFilterBtnView->Disable();
			GreenFilterBtnView->HideView();
		}
		
		IControlView* DesignerActionWidgetView=iPanelControlData->FindWidget(kSPSetDesignerMilestoneWidgetID);
		if(DesignerActionWidgetView)
		{
			//28.2.05

			//CA("HHHHHHHHHHHHHHH");
			//CMilestoneModel  * milestoneval = NULL;
			//milestoneval = ptrIAppFramework->MilestoneCache_getDesignerMilestone();
			//if(milestoneval)
			//{
			//	//CA("milestoneval");
			//	bool16 success = ptrIAppFramework->MILESTONECACHE_getRoleMilestoneWriteAccess(1 , milestoneval->getId());
			//	if(!success)
			//	{
			//		//CA("HIDE");
					DesignerActionWidgetView->HideView();
			//	}
			//	else
			//	{
			//		//CA("SHOW");
			//		DesignerActionWidgetView->ShowView();
			//	}
			//}
			//else
			//{
			//	//CA("NOT milestoneval");
			//	//DesignerActionWidgetView->HideView(); //og
			//	DesignerActionWidgetView->ShowView();
			//}
		}	//-------if end -------------
		IControlView* previewwidgetview=iPanelControlData->FindWidget(kPRLPreviewButtonWidgetID);
		if(previewwidgetview)
		{
			previewwidgetview->Disable();				
		}			

		IControlView* SubSecSprayButtonView=iPanelControlData->FindWidget(kSPSubSectionSprayButtonWidgetID);
		if(SubSecSprayButtonView)
		{
			SubSecSprayButtonView->Disable();
		}
		
		IControlView* iFirstListBoxView = iPanelControlData->FindWidget(kSPTreeListBoxWidgetID);
		if(iFirstListBoxView)
		{
			iFirstListBoxView->Enable();
		}

		if(level == 2)
		{
			IControlView* iSectionDropDown = iPanelControlData->FindWidget(/*kSPSectionDropDownWidgetID*/kSPSectionDropDown_1WidgetID);
			if(iSectionDropDown)
			{
				iSectionDropDown->Enable();	//--------
			}
			md.setDropDownView(iSectionDropDown);
		}

		if(level == 3)
		{
			//ptrIAppFramework->LogDebug("level == 3");
			IControlView *subsectionDropLevel3 = iPanelControlData->FindWidget(kSPSubSectionDropDownWidgetID);
			if(subsectionDropLevel3 == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No subsectionDropLevel3 ");				
				break;
			}
			else
				subsectionDropLevel3->Disable();
			
			md.setSubSectionDropDownLevel3View(subsectionDropLevel3);

			IControlView *sectionDropLevel3 = iPanelControlData->FindWidget(kSPSectionDropDown_1WidgetID);
			if(sectionDropLevel3 == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No sectionDropLevel3 ");								
				break;
			}
			else
				sectionDropLevel3->Disable();
			md.setSectionDropDownLevel3View(sectionDropLevel3);
		}

		IControlView *showWhiteBoardControlView=iPanelControlData->FindWidget(kSPShowWhiteBoardWidgetID);
		if(showWhiteBoardControlView == nil)
		{
			//CA("showWhiteBoardControlView == nil");
			showWhiteBoardControlView->Disable();
			return;
		}
        
		IControlView *showTableSourceControlView=iPanelControlData->FindWidget(kSPShowTableSourceWidgetID);
		if(showTableSourceControlView == nil)
		{
			//CA("showTableSourceControlView == nil");
			showTableSourceControlView->Disable();
			return;
		}

 		md.setIconView(showWhiteBoardControlView);
		md.setSprayButtonView(spryButtonView);
		md.setRefreshButtonView(refreshButtonView);
		md.setGreenFilterBtnView(GreenFilterBtnView);			
		md.setSubSecSprayButtonView(SubSecSprayButtonView);
		md.setListBoxView(iFirstListBoxView);
		
		md.setShowPubTreeView(showPubTreeView);
		md.setSearchView(searchView);
		md.setDesignerActionWidgetView(DesignerActionWidgetView);
		md.setPreviewWidgetView(previewwidgetview);
        md.setTableSourceBtnView(showTableSourceControlView);
		md.setWhiteBoardBtnView(showWhiteBoardControlView);
		
		if(isUserLoggedIn) //-- original if 
		{ //-- original if opening brace 
				
				//listHelper.EmptyCurrentListBox(iPanelControlData, 1);
			IControlView * SelectCustomerWidgetView = iPanelControlData->FindWidget(kSPSelectCustomerWidgetID);
			if(!SelectCustomerWidgetView)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::SelectCustomerWidgetView ");					
				break;
			}	
			
			if(ptrIAppFramework->CONFIGCACHE_getShowCustomerInIndesign())
			{
				SelectCustomerWidgetView->ShowView();
			}
			else
				SelectCustomerWidgetView->HideView();

			InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
			if(!ptrIClientOptions)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No ptrIClientOptions ");					
				break;
			}

			double lang_id = -1 ;
			PMString lang_name("");

			lang_id = ptrIClientOptions->getDefaultLocale(lang_name);
			global_lang_id = lang_id ;

			PMString defPubName("");
			double defPubId=-1;

			defPubId = ptrIClientOptions->getDefPublication(defPubName);
			if(defPubId <= 0)
			{
				//CA(" defPubId <= 0 ");
				break;
			}


			/*if(ptrIAppFramework->CONFIGCACHE_getCmShowEventNumber())
			{
				CPubModel pubModelObj = ptrIAppFramework->getpubModelByPubID(defPubId, lang_id);
				if(pubModelObj.getNumber() != "-1" )
				{
					PMString asf;
					asf.Append(pubModelObj.getNumber());
					asf.Append("-");
					asf.Append(defPubName);
					defPubName.clear();
					defPubName.Append(asf);				
				}
			}	*/	

			CurrentSelectedPublicationID = defPubId;
			global_publication_id = CurrentSelectedPublicationID ;

			//------------------------------------------------------------------------------------//
			IControlView * iView = iPanelControlData->FindWidget(kSPPubNameWidgetID);
			if(!iView)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No iView ");					
				break;
			}

			InterfacePtr<ITextControlData> iData(iView, UseDefaultIID());
			if(iData==nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No iData ");					
				break;
			}

			PMString pubName("");
			/*if(isONEsource)	
			{
				pubName.Append(defPubName);
			}
			else
			{
				pubName = "Publication";
				pubName = ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename(pubName),global_lang_id);
				pubName.Append(" : ");
				pubName.Append(defPubName);
			}*/
			
			PMString PubTip = defPubName;

			md.setPublicationRoot(defPubId);
            
				//---------------------------- if level 3 ----------------------------------------------------------------//
			if(global_project_level == 3 )
			{
				//CA("Inside Populate Section Drop Down List");	
				IControlView * iView=iPanelControlData->FindWidget(kSPPubNameWidgetID);

				if(!iView)
					break;

				InterfacePtr<ITextControlData> iData(iView, UseDefaultIID());
					if(iData==nil)
						break;
				md.setPubNameTextView(iView);


				VectorPubModelPtr vec_pub_model = ptrIAppFramework->/*getAllSectionsByPubIdAndLanguageId*/ProjectCache_getAllChildren(defPubId , lang_id);

				if(vec_pub_model->size() <= 0)
				{
					//CA(" nothing fiund ");
					return ;
				}

				IControlView* subSectionDropListCntrlView=iPanelControlData->FindWidget(kSPSectionDropDownWidgetID);//kSubSectionDropDownWidgetID);//Awasthi
				if(subSectionDropListCntrlView==nil)
				{
					//////CA("LoadPaletteData 45");
					//CA("subSectionDropListCntrlView");
					break;
				}
				else
				{
					//////CA("LoadPaletteData 46");
					subSectionDropListCntrlView->Enable(); // added on 14 feb 2006 
					//md.setsubSectionDropListView(subSectionDropListCntrlView);
					md.setDropDownView(subSectionDropListCntrlView);
				}
							
				InterfacePtr<IDropDownListController> subSectionDropListCntrler(subSectionDropListCntrlView, UseDefaultIID());
				if(subSectionDropListCntrler==nil)
				{
					//		CA("subSectionDropListCntrler is nil");
					break;
				}
								
								
				InterfacePtr<IStringListControlData> subSectionDropListCntrlData(subSectionDropListCntrler, UseDefaultIID());
				if(subSectionDropListCntrlData==nil)
				{
					break;
				}
				
				SectionData publdata;
				publdata.clearSectionVector();
				publdata.clearSubsectionVector();

				//AttachWidget(iPanelControlData, kSPSectionDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);

				//if(md.getDropDownView()==nil)
					//CA("View is nil");
				IControlView* iSectionDropDown1 = iPanelControlData->FindWidget(kSPSectionDropDownWidgetID);
				if(iSectionDropDown1)
					iSectionDropDown1->Enable();

				subSectionDropListCntrlData->Clear();
				PMString as;
				as.Append("--Select--");
				as.SetTranslatable(kFalse);
				subSectionDropListCntrlData->AddString(as);

				if(vec_pub_model->size() > 0)
				{
					int32 size = static_cast<int32>(vec_pub_model->size());
					global_vector_pubmodel.clear();
					for(int32 k = 0 ; k < size ; k++)
					{
						CPubModel model = (*vec_pub_model)[k];
						double sectid=model.getEventId();
						PMString pubname=model.getName();
						int32 lvl= 0  ; //it->getLevel_no();
						double rootid =   model.getRootID();
						double Typeid = model.getTypeID();
						PMString PubComment = ""; // model.getComments();

						publdata.setSectionVectorInfo(sectid,pubname,lvl, rootid, Typeid, PubComment);

						subSectionDropListCntrlData->AddString(pubname);
						global_vector_pubmodel.push_back(model);
					}
					
				}
				if(vec_pub_model->size() > 0)
				{
					subSectionDropListCntrler->Select(1);

					PubData Temppubldata = publdata.getSectionVectorInfo(0);
					PMString SectionComment = Temppubldata.getPubName();
					PMString seccomm = Temppubldata.getComment();
					if(seccomm.NumUTF16TextChars() >0)
					{
						SectionComment.Append("\n");
						SectionComment.Append(seccomm);
					}
					//InterfacePtr<ISPXLibraryButtonData> data( subSectionDropListCntrlView ,IID_ISPXLIBRARYBUTTONDATA );
					//if ( data ){
					//	//PMString ToolTip(" Tool Tip By Rahul Dalvi " );
					//	data->SetName(SectionComment);
					//}

				}
				else
				{
					subSectionDropListCntrler->Select(0);

					PMString SectionComment("");
					//InterfacePtr<ISPXLibraryButtonData> data( subSectionDropListCntrlView ,IID_ISPXLIBRARYBUTTONDATA );
					//if ( data ){
					//	//PMString ToolTip(" Tool Tip By Rahul Dalvi " );
					//	data->SetName(SectionComment);
					//}

				}
                
				//SDKListBoxHelper listHelperInsideLoadPaletteData(this, kSPPluginID);
				//listHelperInsideLoadPaletteData.EmptyCurrentListBox(iPanelControlData, 1);
				if(vec_pub_model)
					delete vec_pub_model;
							
								
				
				//--------------			
				pubName.Clear();
				pubName = "";
				if(ptrIAppFramework->get_isONEsourceMode())
				{
					pubName.Append(defPubName);
				}
				else
				{
					pubName = "Event";
					pubName.Append(" : ");
					pubName.Append(defPubName);
				}
				
				//CA("1");
                pubName.ParseForEmbeddedCharacters();
				iData->SetString(pubName);

				CMilestoneModel  * milestoneval = NULL;
				//milestoneval = ptrIAppFramework->MilestoneCache_getDesignerMilestone();
				if(DesignerActionWidgetView != NULL)
				{
				//	if(milestoneval == NULL)
						DesignerActionWidgetView->Disable();
				//	else
				//		DesignerActionWidgetView->Enable();
				}
			}// --------- if level 3 -----------

			if(global_project_level == 2 )
			{
				//CA(" After UserLoggedIn global_project_level == 2 ");
				
				IControlView *sectionText1View = iPanelControlData->FindWidget(kSectionText_1WidgetID);
				if(sectionText1View == nil)
				{
					ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No sectionText1View");				
					break;
				}

				IControlView *sectionDrop1 = iPanelControlData->FindWidget(kSPSectionDropDown_1WidgetID);
				if(sectionDrop1 == nil)
				{
					ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No sectionDrop1");				
					break;
				}

				int32 selRowIndex = 1;

				//IControlView * iView=iPanelControlData->FindWidget(kSPPubNameWidgetID);
				//if(!iView)
				//{
				//	//CA("!iView");
				//	break;
				//}

				//InterfacePtr<ITextControlData> iData(iView,UseDefaultIID());
				//	if(iData==nil)
				//		break;

				//iData->SetString(defPubName);
				//CA("Iview1");
				md.setPubNameTextView(iView);

				PMString categoryNewName("");
				if(isONEsource)
				{
					VectorClassInfoPtr vectClsValuePtr = ptrIAppFramework->ClassificationTree_getRoot(lang_id); 
					if(vectClsValuePtr == nil){
						//CA("vectClsValuePtr == nil");
						ptrIAppFramework->LogError("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::vectClsValuePtr == nil ");
						return ;
					}
					VectorClassInfoValue::iterator itr;
					itr = vectClsValuePtr->begin();
					categoryNewName.Append(itr->getClassification_name());

				}

				VectorPubModelPtr vec_pub_model = NULL;
				VectorClassInfoPtr vect = NULL;
				int32 vectSize  = 0;
				PMString setNAME = "";
				double section_ID = -1;
				
				LoginInfoValue cserverInfoValue;
				int32 isCheckCount = 0;
				ItemProductCountMapPtr temProductSectionCountPtr = NULL;
				if(isONEsource)
				{
					//CA("isONEsource_2");
					// Apsiva 9 Comment
					//CClassificationValue classValObj = ptrIAppFramework->StructureCache_getCClassificationValueByClassId(defPubId, lang_id);
					//int32 parent_ID = classValObj.getParent_id();

					//vect = ptrIAppFramework->ClassificationTree_getAllChildren(parent_ID/*defPubId*/, lang_id);
					//if(vect==nil)
					//{
					//	//CA("vect==nil");
					//	ptrIAppFramework->LogError("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::vect==nil");
					//	break;
					//}

					//vectSize =	static_cast<int32>(vect->size());
					//if(vectSize == 0)
					//	return;

				} 
				else
				{
					//CA("!!isONEsource");
				
					InterfacePtr<ILoginHelper> ptrLogInHelper(static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss,ILoginHelper::kDefaultIID)));
					if(ptrLogInHelper == nil)
					{
						//CAlert::InformationAlert("Pointer to ptrLogInHelper is nil.");
						return ;
					}
					
					PMString name("");
					//CServerInfoValue cserverInfoValue;

					bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue);
					
					ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();

					PMString strSectionID =	clientInfoObj.getSectionID();
					double section_ID = strSectionID.GetAsDouble();
					isCheckCount = clientInfoObj.getShowObjectCountFlag();//---------

					//int32 section_ID =  -1;

					if(section_ID != -1)
					{
						//CA("section_ID != -1");

						defPubId = section_ID;
						CPubModel pubModelValue = ptrIAppFramework->getpubModelByPubID(section_ID, lang_id);
						double pubParentID = pubModelValue.getParentId();

						vec_pub_model = ptrIAppFramework->ProjectCache_getAllChildren(pubParentID,lang_id);
						if(vec_pub_model == NULL)
						{
							ptrIAppFramework->LogError("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::ProjectCache_getAllChildren'vec_pub_model==nil");
							return ;
						}
						double rootPubID = pubModelValue.getRootID();


						CPubModel pubModelRootValue = ptrIAppFramework->getpubModelByPubID(rootPubID,lang_id);
						
						PMString TempBuff = pubModelRootValue.getName();
					
						InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
						if(iConverter)
						{
							setNAME=iConverter->translateString(TempBuff);
						}
						else
							setNAME = TempBuff;

					}	
					else
					{
						//CA("section_ID == -1");
						CPubModel pubModelRootValue = ptrIAppFramework->getpubModelByPubID(defPubId, lang_id);
										
						double pubParent_ID = pubModelRootValue.getParentId();
						double rootPubID =pubModelRootValue.getRootID();

						CPubModel pubModelValue = ptrIAppFramework->getpubModelByPubID(rootPubID,lang_id);
						
						PMString TempBuff = pubModelValue.getName();
						InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
						if(iConverter)
						{
							setNAME=iConverter->translateString(TempBuff);
						}
						else
							setNAME = TempBuff;


						if(defPubId == rootPubID)
						{

							vec_pub_model = ptrIAppFramework->/*getAllSubsectionsByPubIdAndLanguageId*/ProjectCache_getAllChildren(defPubId, lang_id);
							if(vec_pub_model == NULL)
							{
							//	CA("vec_pub_model == NULL");
								//pubName = ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename("Publication"),global_lang_id);
								pubName = "Event";
								pubName.Append(" : ");
								pubName.Append(setNAME);
								//CA("2");
                                pubName.ParseForEmbeddedCharacters();
								iData->SetString(pubName);
								ptrIAppFramework->LogError("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::getAllSubsectionsByPubIdAndLanguageId'vec_pub_model==nil");
								return ;
							}		
						}
						else
						{

							vec_pub_model = ptrIAppFramework->ProjectCache_getAllChildren(pubParent_ID,lang_id);
							if(vec_pub_model == NULL)
							{
								ptrIAppFramework->LogError("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::ProjectCache_getAllChildren'vec_pub_model==nil");
								return ;
							}
						}
					}
					if(vec_pub_model->size() == 0)
					{
						//CA("vec_PubSize == 0");
						return ;
					}
					/*if(isCheckCount)
					{
						temProductSectionCountPtr = ptrIAppFramework->GETPROJECTPRODUCTS_getItemProductCountForSection(0);
					}*/

				}
				
				pubName.clear();

				if(isONEsource)
				{
					pubName.Append(categoryNewName);
				}
				else
				{
					//pubName = ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename("Publication"),global_lang_id);
					pubName = "Event";
					pubName.Append(" : ");
					pubName.Append(setNAME);
				}
				//CA("3");
				pubName.SetTranslatable(kFalse);
                pubName.ParseForEmbeddedCharacters();
				iData->SetString(pubName);

				/*VectorPubModelPtr vec_pub_model = ptrIAppFramework->getAllSubsectionsByPubIdAndLanguageId(defPubId , lang_id);
				if(vec_pub_model == nil)
				{
					CA("vec_pub_model == nil");
					break;
				}*/
				

				IControlView* subSectionDropListCntrlView = iPanelControlData->FindWidget(kSPSectionDropDown_1WidgetID);//kSubSectionDropDownWidgetID);//Awasthi
				if(subSectionDropListCntrlView == nil)
				{
					//CA("subSectionDropListCntrlView == nil");
					break;
				}
				else
				{
					md.setDropDownView(subSectionDropListCntrlView);
					subSectionDropListCntrlView->Enable(); // added on 14 feb 2006 
				}
				
				InterfacePtr<IDropDownListController> subSectionDropListCntrler(subSectionDropListCntrlView, UseDefaultIID());
				if(subSectionDropListCntrler==nil)
				{
					//CA("subSectionDropListCntrler is nil");
					break;
				}
							
				InterfacePtr<IStringListControlData> subSectionDropListCntrlData(subSectionDropListCntrler, UseDefaultIID());
				if(subSectionDropListCntrlData==nil)
				{
					//CA("subSectionDropListCntrlData is nil");
					break;
				}
					
				SectionData publdata;
				publdata.clearSectionVector();
				publdata.clearSubsectionVector();

				//AttachWidget(iPanelControlData, kSPSectionDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);

				//if(md.getDropDownView()==nil)
					//CA("View is nil");
					/*IControlView* iSectionDropDown1 = iPanelControlData->FindWidget(kSPSectionDropDownWidgetID);
					if(iSectionDropDown1)
						iSectionDropDown1->Enable();*/

				subSectionDropListCntrlData->Clear();
				PMString as;
				as.Append(kSPSelectStringKey);
				subSectionDropListCntrlData->AddString(as);
				if(isONEsource)
				{
					if(vectSize > 0)
					{
						int32 Cat_index  = 1;
						for(int32 i=0  ; i < vectSize ; i++ , Cat_index++)
						{
							CClassificationValue cClssValue = (*vect)[i];
							PMString clsName = cClssValue.getClassification_name();
							double clsID = cClssValue.getClass_id();
							double clsParentID 	= cClssValue.getParent_id();
							int32 clsLevel 	= cClssValue.getClass_level();
							
							if(clsID == defPubId )
							{
								selRowIndex = Cat_index;
							}

							InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
							if(!iConverter)
							{
								clsName=clsName;
								return;
							}
							clsName=iConverter->translateString(clsName);
							//CA("clsName  :  "+clsName);
							publdata.setSectionVectorInfo(clsID , clsName ,clsLevel , clsParentID , -1 , "");
							subSectionDropListCntrlData->AddString(clsName);

						}
					}

				}
				else
				{
					if(vec_pub_model->size() > 0)
					{
						int32 size =static_cast<int32> (vec_pub_model->size());
						global_vector_pubmodel.clear();
						int32 index= 1;
						for(int32 k = 0 ; k < size ; k++ , index++ )
						{
							CPubModel model = (*vec_pub_model)[k];
							double sectid=model.getEventId();
							PMString pubname=model.getName();
							int32 lvl= 0  ; //it->getLevel_no();
							double rootid = model.getRootID();
							double Typeid = model.getTypeID();
							PMString PubComment = ""; //model.getComments();

							if(sectid == defPubId)
							{
								selRowIndex = index ;
							}
							InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
							if(!iConverter)
							{
								//CA("!iConverter");
								pubname=pubname;
								return;
							}
							pubname=iConverter->translateString(pubname);
							
						//	CA("pubname  :   "+pubname);
							publdata.setSectionVectorInfo(sectid,pubname,lvl, rootid, Typeid, PubComment);
							if(isCheckCount)
							{
								if(temProductSectionCountPtr == NULL)
								{
									ptrIAppFramework->LogError("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::ItemProductCountMapPtr::temProductSectionCountPointer == NULL");
									return ;
								}
								if(temProductSectionCountPtr->size() == 0)
								{
									ptrIAppFramework->LogError("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::ItemProductCountMapPtr::temProductSectionCountPtr->size() == 0");
									return ;
								}
								map<double, double>::iterator mapItr;
								mapItr = temProductSectionCountPtr->find(sectid);
								int32 secChild_Count = mapItr->second;
								pubname.Append(" (");
								pubname.AppendNumber(secChild_Count);
								pubname.Append(")");
								pubname.SetTranslatable(kFalse);
								subSectionDropListCntrlData->AddString(pubname);

							}
							else
							{
								pubname.SetTranslatable(kFalse);
								subSectionDropListCntrlData->AddString(pubname);
							}
                            //CA("pubname  = "+pubname);
							global_vector_pubmodel.push_back(model);
						}
						
					}
				}
				
				//SDKListBoxHelper listHelperInsideLoadPaletteData(this, kSPPluginID);
				//listHelperInsideLoadPaletteData.EmptyCurrentListBox(iPanelControlData, 1);

				IControlView* spTreeListBoxWidgetView = iPanelControlData->FindWidget(kSPTreeListBoxWidgetID);
				InterfacePtr<ITreeViewMgr> treeViewMgr(spTreeListBoxWidgetView, UseDefaultIID());
				if(!treeViewMgr)
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");					
					return ;
				}				
				dc.clearMap();
				pNodeDataList.clear();
				SPTreeModel pModel;
				PMString pfName("Root");
				pModel.setRoot(-1, pfName, 1);
				treeViewMgr->ClearTree(kTrue);
				pModel.GetRootUID();
				treeViewMgr->ChangeRoot();

				

				if(isONEsource)
				{
					if(vectSize > 0)
					{
						subSectionDropListCntrler->Select(selRowIndex/*1*/);
						PubData clssficNameData = publdata.getSectionVectorInfo(0);
						PMString clssficName = clssficNameData.getPubName();
						InterfacePtr<ISPXLibraryButtonData> commData(subSectionDropListCntrlView ,IID_ISPXLIBRARYBUTTONDATA );
						if(commData)
						{
							commData->SetName(clssficName);
						}

					}
					else
					{
						subSectionDropListCntrler->Select(0);
						PMString clsComment("");
						InterfacePtr<ISPXLibraryButtonData> commData( subSectionDropListCntrlView ,IID_ISPXLIBRARYBUTTONDATA );
						if ( commData ){
							commData->SetName(clsComment);
						}
					}
				}
				else
				{
					if(vec_pub_model->size() >0 )
					{
						subSectionDropListCntrler->Select(selRowIndex);

						PubData Temppubldata = publdata.getSectionVectorInfo(0);
						PMString SectionComment = Temppubldata.getPubName();
						PMString seccomm = Temppubldata.getComment();
						if(seccomm.NumUTF16TextChars() >0)
						{
							SectionComment.Append("\n");
							SectionComment.Append(seccomm);
						}
						InterfacePtr<ISPXLibraryButtonData> data(subSectionDropListCntrlView ,IID_ISPXLIBRARYBUTTONDATA );
						if ( data ){
							//PMString ToolTip(" Tool Tip By Rahul Dalvi " );
							data->SetName(SectionComment);
						}
					}
					else
					{
						subSectionDropListCntrler->Select(0);
						
						PMString SectionComment("");
						InterfacePtr<ISPXLibraryButtonData> data( subSectionDropListCntrlView ,IID_ISPXLIBRARYBUTTONDATA );
						if ( data ){
							//PMString ToolTip(" Tool Tip By Rahul Dalvi " );
							data->SetName(SectionComment);
						}
					}
				}
				md.setCurrentSelectedRow(selRowIndex);	//-------
				
				if(vec_pub_model)
					delete vec_pub_model;
						
				if(vect)
					delete vect;
				if(temProductSectionCountPtr)
				{
					delete temProductSectionCountPtr;
					temProductSectionCountPtr = NULL;
				}

				CMilestoneModel  * milestoneval = NULL;
				//milestoneval = ptrIAppFramework->MilestoneCache_getDesignerMilestone();
				if(DesignerActionWidgetView != NULL)
				{
					//	if(milestoneval == NULL )
							//CA("2");
							//DesignerActionWidgetView->Disable(); // og was not commented 
							DesignerActionWidgetView->Enable(); //added by avinash
					//	else
					//		DesignerActionWidgetView->Enable();
				}
			}

		} //-- original if closing brace .......
	  
		if(!isUserLoggedIn)
		{
			//ptrIAppFramework->LogDebug("----SPSelectionObserver::loadPaletteData 333-!isUserLoggedIn-");			
			//CA("Invalid selection.  Please log into PRINTsource before continuing.");

			IControlView * SelectCustomerWidgetView = iPanelControlData->FindWidget(kSPSelectCustomerWidgetID);
			if(!SelectCustomerWidgetView)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::SelectCustomerWidgetView ");					
				break;
			}	
			SelectCustomerWidgetView->HideView();

			//SDKListBoxHelper listHelperInsideLoadPaletteData(this, kSPPluginID);
			//listHelperInsideLoadPaletteData.EmptyCurrentListBox(iPanelControlData, 1);
			IControlView* spTreeListBoxWidgetView = iPanelControlData->FindWidget(kSPTreeListBoxWidgetID);
			InterfacePtr<ITreeViewMgr> treeViewMgr(spTreeListBoxWidgetView, UseDefaultIID());
			if(!treeViewMgr)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");					
				return ;
			}				
			dc.clearMap();
			pNodeDataList.clear();
			SPTreeModel pModel;
			PMString pfName("Root");
			pModel.setRoot(-1, pfName, 1);
			treeViewMgr->ClearTree(kTrue);
			pModel.GetRootUID();
			treeViewMgr->ChangeRoot();

			SectionData publdata;
			publdata.clearSectionVector();
			publdata.clearSubsectionVector();

			DesignerActionWidgetView->Disable();
			showWhiteBoardControlView->Disable();
			spryButtonView->Disable();
			refreshButtonView->Disable();
			SubSecSprayButtonView->Disable();
			iFirstListBoxView->Disable();
            
			if(level == 2){
				InterfacePtr<IDropDownListController> SectionDropListCntrlerLevel2(md.getDropDownView(), UseDefaultIID());
				if(SectionDropListCntrlerLevel2)
					SectionDropListCntrlerLevel2->Select(-1);					
				md.getDropDownView()->Disable();
			}
			else if(level == 3)
			{
				InterfacePtr<IDropDownListController> subSectionDropListCntrler2(md.getSubSectionDropDownLevel3View(), UseDefaultIID());
				if(subSectionDropListCntrler2)
					subSectionDropListCntrler2->Select(-1);

				InterfacePtr<IDropDownListController> subSectionDropListCntrler3(md.getSectionDropDownLevel3View(), UseDefaultIID());
				if(subSectionDropListCntrler3)
					subSectionDropListCntrler3->Select(-1);

				md.getSectionDropDownLevel3View()->Disable();
				md.getSubSectionDropDownLevel3View()->Disable();
			}
			showPubTreeView->Disable();
			searchView->Disable();
			
			showTableSourceControlView->Disable();
		
		}

	}while(kFalse);

}


void SPSelectionObserver::populateSectionDropDownList(double rootId)
{
	//CA("Inside SPSelectionObserver::populateSectionDropDownList");
	noOfRows = 0;
	
	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
			break;

		InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
		if (iPanelControlData == nil){
			break;
		}
		
		IControlView* subSectionDropListCtrlView = NULL;

		if(global_project_level ==3)
		{
		
			subSectionDropListCtrlView =iPanelControlData->FindWidget(kSPSectionDropDownWidgetID);
			if(subSectionDropListCtrlView==nil)
				break;

		}else if( global_project_level ==2)
		{
			subSectionDropListCtrlView =iPanelControlData->FindWidget(kSPSectionDropDown_1WidgetID);
			if(subSectionDropListCtrlView==nil)
				break;

		}

		InterfacePtr<IDropDownListController> subSectionDropListCntrler(subSectionDropListCtrlView, UseDefaultIID());
		if(subSectionDropListCntrler==nil)
			break;
		
		InterfacePtr<IStringListControlData> subSectionDropListCtrlData(subSectionDropListCntrler, UseDefaultIID());
		if(subSectionDropListCtrlData==nil)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::populateSectionDropDownList::No subSectionDropListCtrlData");		
			break;
		}

		subSectionDropListCtrlData->Clear(kFalse, kFalse);
		PMString str1("--Select--");
		str1.SetTranslatable(kFalse);
		subSectionDropListCtrlData->AddString(str1);

		Mediator md;
		double defPubId=md.getPublicationRoot();

		SectionData publdata;
		double sectionID = -1;
		publdata.clearSectionVector();
		publdata.clearSubsectionVector();
		global_vector_pubmodel.clear();
        
		double language_id = -1 ;
		language_id = global_lang_id ; /*ptrIClientOptions->getDefaultLocale(language_name);*/
		VectorPubModelPtr vector_pubmodel = NULL;
		VectorClassInfoPtr vectClassValuePtr = NULL;
		int32 selRowIndex = 1;
		
		
		InterfacePtr<ILoginHelper> ptrLogInHelper(static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss,ILoginHelper::kDefaultIID)));
		if(ptrLogInHelper == nil)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::populateSectionDropDownList::ptrLogInHelper == nil ");
			return ;
		}
		
		LoginInfoValue cserverInfoValue;
		int32 isCheckCount = 0;
		ptrLogInHelper->getCurrentServerInfo(cserverInfoValue);
		ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();
		isCheckCount =  clientInfoObj.getShowObjectCountFlag();
		
		ItemProductCountMapPtr temProductSectionCountPtr = NULL;	//--------

		if( global_project_level == 3)
		{			
			vector_pubmodel = ptrIAppFramework->ProjectCache_getAllChildren(rootId , language_id);
			if(vector_pubmodel == NULL)
			{
				//CA("vector_pubmodel == NULL");
				break;
			}
			if(vector_pubmodel->size() >= 1) 
			{
				//CA(" got sections  for level 3");
			}
			else
			{
				//CA(" nothing ");
			}

		}
		else if( global_project_level ==2)
		{
			if(isONEsource)
			{
				//CA("isONEsource_3");
				CClassificationValue classValueObj = ptrIAppFramework->StructureCache_getCClassificationValueByClassId(rootId/*global_classID*/,global_lang_id);
				double parentID = classValueObj.getParent_id();

				vectClassValuePtr = ptrIAppFramework->ClassificationTree_getAllChildren(parentID/*rootId*/,language_id);
				if(vectClassValuePtr == nil)
				{
					//CA("vectClassValuePtr == nil");
					ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::populateSectionDropDownList::vectClassValuePtr == nil ");
					break;
				}
					
			}
			else
			{
				//CA("!!!!!!!!!isONEsource");
				CPubModel pubModelValue = ptrIAppFramework->getpubModelByPubID(rootId , language_id);
									
				double pubParent_ID = pubModelValue.getParentId();
				double rootPubID =  pubModelValue.getRootID();	
	
				if(rootId == rootPubID)
				{
					//CA("getAllSubsectionsByPubIdAndLanguageId");
					vector_pubmodel = ptrIAppFramework->/*getAllSubsectionsByPubIdAndLanguageId*/ProjectCache_getAllChildren(rootId , language_id);
					if(vector_pubmodel == NULL)
					{
						//CA("vector_pubmodel == NULL");
						break;
					}
				}
				else
				{
					vector_pubmodel = ptrIAppFramework->ProjectCache_getAllChildren(pubParent_ID,language_id);
					//CA("ProjectCache_getAllChildren");
					if(vector_pubmodel == NULL)
					{
						//CA("vector_pubmodel == NULL");
						ptrIAppFramework->LogError("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::ProjectCache_getAllChildren'vector_pubmodel==nil");
						break;
					}
				}
				/*if(isCheckCount)
				{
					temProductSectionCountPtr = ptrIAppFramework->GETPROJECTPRODUCTS_getItemProductCountForSection(0);
				}*/
		
			}
		}

		if(isONEsource)
		{
			if(vectClassValuePtr)
			{
				if(vectClassValuePtr->size() == 0)
				{
					break;
				}
				int32 Index= 1;
				VectorClassInfoValue::iterator itr;
				for(itr = vectClassValuePtr->begin(); itr!= vectClassValuePtr->end(); itr++ , Index++)
				{
					PMString clsName = itr->getClassification_name();
					double clsID = itr->getClass_id();
					double clsParentID 	= itr->getParent_id();
					int32 clsLevel 	= itr->getClass_level();
					if(clsID == rootId)
					{
						selRowIndex = Index ;
					}
					/*InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
					if(!iConverter)
					{
						clsName=clsName;
						break ;
					}
					clsName=iConverter->translateString(clsName);*/
					publdata.setSectionVectorInfo(clsID , clsName ,clsLevel , clsParentID , -1 , "");

					subSectionDropListCtrlData->AddString(clsName);
				}
			}
		}
		else
		{
			if(vector_pubmodel)
			{
				if(vector_pubmodel->size() < 1) 
				{
					break;
				}
				int32 index = 1;
				VectorPubModel::iterator it;
				for(it=vector_pubmodel->begin(); it!=vector_pubmodel->end(); it++ ,index++ )
				{
					double sectid=it->getEventId();
					PMString pubname=it->getName();
					int32 lvl= 0  ; //it->getLevel_no();
					double rootid = it->getRootID();
					double type_id = it->getTypeID(); 
					PMString PubComment = ""; //it->getComments();
					if(sectid == rootId)
					{
						selRowIndex = index ;
					}					
					//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
					//if(!iConverter)
					//{
					//	//CA("!iConverter");
					//	pubname=pubname;
					//	return;
					//}
					//
					//pubname=iConverter->translateString(pubname);
					publdata.setSectionVectorInfo(sectid,pubname,lvl, rootid, type_id, PubComment);
					if(isCheckCount)
					{
						if(temProductSectionCountPtr == NULL)
						{
							ptrIAppFramework->LogError("AP7_ProductFinder::SPSelectionObserver::populateSectionDropDownList::ItemProductCountMapPtr::temProductSectionCountPointer == NULL");
							return ;
						}
						if(temProductSectionCountPtr->size() == 0)
						{
							ptrIAppFramework->LogError("AP7_ProductFinder::SPSelectionObserver::populateSectionDropDownList::ItemProductCountMapPtr::temProductSectionCountPtr->size() == 0");
							return ;
						}
						map<double, double>::iterator mapItr;
						mapItr = temProductSectionCountPtr->find(sectid);
						int32 secChild_Count = mapItr->second;
						pubname.Append(" (");
						pubname.AppendNumber(secChild_Count);
						pubname.Append(")");
						//pubname.SetTranslatable(kFalse);
						//subSectionDropListCtrlData->AddString(pubname);
						WideString ws;
						StringUtils::ConvertUTF8ToWideString(pubname.GetUTF8String(), ws);
						subSectionDropListCtrlData->AddString(PMString(ws).SetTranslatable(kFalse));

					}
					else
					{
						//pubname.SetTranslatable(kFalse);
						//subSectionDropListCtrlData->AddString(pubname);
						WideString ws;
						StringUtils::ConvertUTF8ToWideString(pubname.GetUTF8String(), ws);
						subSectionDropListCtrlData->AddString(PMString(ws).SetTranslatable(kFalse));
					}
				}
			}
		}

		if(temProductSectionCountPtr)
		{
			delete temProductSectionCountPtr;
			temProductSectionCountPtr = NULL;
		}
		//subSectionDropListCntrler->Select(0);
		//CA("In Select Subsection");
		if(md.getCurrentSelectedRow()>0)
		{
			//CA("md.getCurrentSelectedRow()>0");
			subSectionDropListCntrler->Select(md.getCurrentSelectedRow());
		}
		else
		{
			//CA("In Select Subsection else part ");
			if(isONEsource)
			{
				if(vectClassValuePtr->size() >0)
				{
					//CA("vectClassValuePtr->size() >0");
					subSectionDropListCntrler->Select(selRowIndex/*1*/);
				}
				else{
					//CA("else part vectClassValuePtr->size() =<0");
					subSectionDropListCntrler->Select(0);
				}
			}
			else
			{
				if(vector_pubmodel->size() >0)
				{
					//CA("vector_pubmodel->size() >0");
					subSectionDropListCntrler->Select(selRowIndex/*1*/);
				}
				else{
					//CA(" else part vector_pubmodel->size() =<0");
					subSectionDropListCntrler->Select(0);
				}
			}
		}
		
	}while(kFalse);
	
}

bool8 SPSelectionObserver::populateProductListforSection (double curSelSubecId)
{
//	CA(__FUNCTION__);

	firstpNodeListCount = 0;
	vectorForThumbNail.clear();
	bool8 isListBoxPopulated = kFalse;

	AcquireWaitCursor awc;
	awc.Animate(); 

	//InterfacePtr<ISPPRImageHelper> ptrImageHelper((static_cast<ISPPRImageHelper*> (CreateObject(kSPProductImageIFaceBoss ,ISPPRImageHelper::kDefaultIID))));
	//if(ptrImageHelper == nil)
	//{
	//	//CA("ProductImages plugin not found ");
	//	return kFalse;
	//}
	//ptrImageHelper->EmptyImageGrid();
	//imageVector2.clear();

	Mediator md;
	IPanelControlData* iPanelControlData=md.getMainPanelCtrlData();
	if(iPanelControlData == nil)
	{
		//CA("panelCntrlData is nil");
		return kFalse;
	}
    ////////////////////////////// for search
	IControlView* iPubGPCtrlView = iPanelControlData->FindWidget(kSPPublicationGPWidgetID);
	IControlView* iONEGPCtrlView = iPanelControlData->FindWidget(kSPONEGPWidgetID);
	IControlView* iSSPGPCtrlView = iPanelControlData->FindWidget(kSPSearchPnlWidgetID);
	IControlView* spTreeListBoxWidgetView = iPanelControlData->FindWidget(kSPTreeListBoxWidgetID);

	if(searchResult)
	{
		iSSPGPCtrlView->ShowView();
		iPubGPCtrlView->HideView();
		iONEGPCtrlView->HideView();
	}
	else
	{	
		iSSPGPCtrlView->HideView();
		iPubGPCtrlView->ShowView();
		iPubGPCtrlView->Enable();
		iONEGPCtrlView->ShowView();
	}
    ////////////////////////////// for search ends
	IControlView* listBox = iPanelControlData->FindWidget(kSPListBoxWrapperGroupPanelWidgetID);		
	//IControlView* thumb = iPanelControlData->FindWidget(kSPThumbPnlWidgetID);
	if(isThumb){
		listBox ->HideView();		
		//thumb ->ShowView();
	}
	else
	{
		//thumb->HideView();
		listBox->ShowView();		
	}

	do
	{
		Mediator md;
		IPanelControlData* iPanelControlData;
		PublicationNode pNode;

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			CAlert::InformationAlert("Err ptrIAppFramework is nil");
			break;
		}
		/*InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));*/
		iPanelControlData=md.getMainPanelCtrlData();
		if(!iPanelControlData)
		{
			//CA("Serious Error Again");	
			break;
		}

		dc.clearMap();
		pNodeDataList.clear();

		md.setSelectedPub(curSelSubecId);
		md.setCurrSubSectionID(curSelSubecId);

		//bool16 isONEsource = ptrIAppFramework->get_isONEsourceMode();
		//ItemMapPtr pItemMap = NULL;

		if(!isONEsource)
		{
			//CA("Publication Mode");	
			//int32 ParentTypeID = ptrIAppFramework->TYPEMngr_getObjectTypeID("PRODUCT_LEVEL");			
			VectorPubObjectValuePtr VectorFamilyInfoValuePtr = NULL;			
			//VectorFamilyInfoValuePtr = ptrIAppFramework->PBObjMngr_getProductsForSubSection(curSelSubecId, ParentTypeID);

			if(ListFlag == 0)
			{
				//CA("111AAA");
				VectorFamilyInfoValuePtr = ptrIAppFramework->getProductsAndItemsForSection(curSelSubecId, global_lang_id );
			//	pItemMap =  ptrIAppFramework->GetProjectProducts_getAllLeadingItemIds(curSelSubecId);
			}else if(ListFlag == 1)
			{
				//CA("222BBB");
				VectorFamilyInfoValuePtr = ptrIAppFramework->getProductsForSubSection/*GetProjectProducts_getProductsBySubSection*/(curSelSubecId, global_lang_id );
			//	pItemMap =  ptrIAppFramework->GetProjectProducts_getAllLeadingItemIds(curSelSubecId);
			}else if(ListFlag ==2)
			{
				VectorFamilyInfoValuePtr = ptrIAppFramework->getItemsForSubSection/*GetProjectProducts_getItemsForSubSection*/(curSelSubecId, global_lang_id );
			//	pItemMap =  ptrIAppFramework->GetProjectProducts_getAllLeadingItemIds(curSelSubecId);

			}
			//else if(ListFlag ==3)
			//{
			//	//CA("4444DDD");
			//	VectorFamilyInfoValuePtr = ptrIAppFramework->GetProjectProducts_getHybridTablesForSubSection(curSelSubecId, global_lang_id );
			//}			
			if(VectorFamilyInfoValuePtr == nil)
			{

				InterfacePtr<ITreeViewMgr> treeViewMgr(spTreeListBoxWidgetView, UseDefaultIID());
				if(!treeViewMgr)
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");					
					break;
				}

				SPTreeModel pModel;
				PMString pfName("Root");
				pModel.setRoot(-1, pfName, 1);
				treeViewMgr->ClearTree(kTrue);
				pModel.GetRootUID();
				treeViewMgr->ChangeRoot();

				if(md.getSprayButtonView()){ //CA("Disabling Spray Button.");
					md.getSprayButtonView()->Disable();}
				if(md.getAssignBtnView())
						md.getAssignBtnView()->Disable();
				if(md.getPreviewWidgetView())
						md.getPreviewWidgetView()->Disable();
				if(md.getSubSecSprayButtonView())
					md.getSubSecSprayButtonView()->Disable();

				if(md.getRefreshButtonView())
					md.getRefreshButtonView()->Disable();
				if(md.getIconView())
					md.getIconView()->Disable();
				if(md.getIconViewNew())
					md.getIconViewNew()->Disable();
				//----------
				if(md.getGreenFilterBtnView())
					md.getGreenFilterBtnView()->HideView();
				if(md.getTableSourceBtnView())
					md.getTableSourceBtnView()->Disable();

				

				break;
			}

			if(VectorFamilyInfoValuePtr->size()==0)
			{
				InterfacePtr<ITreeViewMgr> treeViewMgr(spTreeListBoxWidgetView, UseDefaultIID());
				if(!treeViewMgr)
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");					
					break;
				}

				SPTreeModel pModel;
				PMString pfName("Root");
				pModel.setRoot(-1, pfName, 1);
				treeViewMgr->ClearTree(kTrue);
				pModel.GetRootUID();
				treeViewMgr->ChangeRoot();

				//CA("VectorFamilyInfoValuePtr->size()==0");
				if(md.getSprayButtonView()){ //CA("Disabling Spray Button.");
					md.getSprayButtonView()->Disable();}
				if(md.getAssignBtnView())
						md.getAssignBtnView()->Disable();
				if(md.getPreviewWidgetView())
						md.getPreviewWidgetView()->Disable();
				if(md.getSubSecSprayButtonView())
					md.getSubSecSprayButtonView()->Disable();
				if(md.getRefreshButtonView())
					md.getRefreshButtonView()->Disable();
				if(md.getIconView())
					md.getIconView()->Disable();
				if(md.getIconViewNew())
					md.getIconViewNew()->Disable();
				//--------
				if(md.getGreenFilterBtnView())
					md.getGreenFilterBtnView()->HideView();
				if(md.getTableSourceBtnView())
					md.getTableSourceBtnView()->Disable();

				delete VectorFamilyInfoValuePtr;
				break;
			}
		
			int32 size =static_cast<int32> (VectorFamilyInfoValuePtr->size());

			VectorPubObjectValue::iterator it2;
			int count=0;
			PMString itemCountString("");
			int itemCountNum=0;
			for(it2 = VectorFamilyInfoValuePtr->begin(); it2 != VectorFamilyInfoValuePtr->end(); it2++)
			{	
				vectorForThumbNail.push_back(*it2);
				pNode.setPBObjectID(it2->getPub_object_id());
				pNode.setSequence(it2->getSeq_order());//(it2->getIndex());	
				pNode.setIsProduct(it2->getisProduct());
				pNode.setIsONEsource(kFalse);
				pNode.setIsStarred(it2->getStarredFlag1());
				pNode.setSectionID(curSelSubecId);
				pNode.setPublicationID(CurrentSelectedPublicationID);
				pNode.setParentId(curSelSubecId);
				pNode.setTypeId(it2->getobject_type_id());
				int32 NewProductFlag =0;

				//CA(it2->getName());
				if(it2->getNew_product()  == kTrue){
					NewProductFlag =2;
					pNode.setNewProduct(1);
				}
				else{
					NewProductFlag =1;
					pNode.setNewProduct(0);
				}

				//Hybrid Table Change
				if(it2->getisProduct() == 2)
				{
					pNode.setPubId(it2->getObjectValue().getObject_id());

                    PMString pName1 = it2->getName();
						pNode.setPublicationName(pName1);
					
					pNode.setChildCount(0);
					pNode.setChildItemCount(it2->getObjectValue().getChildCount());
					pNode.setTypeId(it2->getObjectValue().getObject_type_id());									
				}

				if(it2->getisProduct() == 1)
				{
					pNode.setPubId(it2->getObjectValue().getObject_id());

                    PMString pName1 = it2->getName();
                    pNode.setPublicationName(pName1);
                    
                    ptrIAppFramework->LogDebug("pNode.setPublicationName : ");
                    ptrIAppFramework->LogDebug(pName1);
					pNode.setChildCount(0);
					pNode.setChildItemCount(it2->getObjectValue().getChildCount());
					pNode.setIsProduct(1);
					//pNode.setReferenceId(it2->getObjectValue().getRef_id());
					pNode.setTypeId(it2->getObjectValue().getObject_type_id());									
				}
				
                if(it2->getisProduct() == 0)
				{
					pNode.setPubId(it2->getItemModel().getItemID());

                    PMString pName1 = it2->getName();
                    pNode.setPublicationName(pName1);

					pNode.setTypeId(it2->getobject_type_id());
					pNode.setIsProduct(0);
					pNode.setChildCount(0);
					pNode.setChildItemCount(it2->getItemModel().getChildItemCount());

				}
				int icon_count = GetIconCountForUnfilteredProductList(it2);
				pNode.setIconCount(icon_count);
				pNodeDataList.push_back(pNode);
				
				int32 StarFlag =1;
				if(it2->getStarredFlag1()) // if Selected Green Star
					StarFlag =2;
//CA("Before showImageAsThumbnail");

				int32 isProduct = it2->getisProduct();
				
				//Apsiva 9 comment
				if(LeadingItemFlag == kFalse)
				{
				//	//CA("LeadingItemFlag == kFalse");
				//	if( pItemMap != NULL && pItemMap->size() > 0)
				//	{
				//		ItemMap::iterator itr;
				//		int32 searchItemId = pNode.getPubId();
				//		itr = pItemMap->find(searchItemId);
				//		if(itr != pItemMap->end())
				//		{
				//			if(ListFlag == 2 && itr->second.itemType == 0)
				//			{
				//				vectorForThumbNail.pop_back();
				//				continue;
				//			}
				//			else
				//			{
				//				//CA("Else");
				//				if(itr->second.itemType == 0)
				//				{
				//					isProduct = 3;
				//					itemCountNum = itr->second.count ;  //***
				//					//CA("itemType == 0");
				//				}
				//				else if(itr->second.itemType == 1)
				//				{
				//					isProduct = 4;
				//					
				//				}
				//				else if(itr->second.itemType == 2)
				//				{
				//					isProduct = 1;
				//					itemCountNum = itr->second.count ;  //***
				//					//CA("//itemType == 2");
				//				}

				//				
				//			}
				//		}

				//	}

					if(!isThumb)
					{
						/*if(itemCountNum  >1)
						{
							itemCountString.SetTranslatable(kFalse);
							itemCountString.Append("(");
							itemCountString.AppendNumber(itemCountNum);
							itemCountString.Append(")");
						}
						else
							itemCountString.Append("");*/
						//CA("itemCountNum = "+itemCountString);
                        
							//listHelperInsidePopulateProductListForSection.AddElement(lstboxControlView , pNode.getName(), kSPTextWidgetID, count );				
						
							//CA("Commenting");
							//listHelperInsidePopulateProductListForSection.SetDesignerAction(lstboxControlView, count, isProduct ,icon_count, StarFlag,NewProductFlag,itemCountString); //og
						count++;
						
						itemCountString.Clear();
						itemCountNum=0;
					}
					else
					{
				 		showImageAsThumbnail(pNode, curSelSubecId);
					}
				
				}
				else
				{
					//Apsiva 9 comment
					//CA("LeadingItemFlag == kTrue : 5417");
					//if(isProduct == 0 && pItemMap != NULL && pItemMap->size() > 0)
					//{
					//	ItemMap::iterator itr;
					//	int32 searchItemId = pNode.getPubId();
					//	itr = pItemMap->find(searchItemId);
					//	if(itr != pItemMap->end())
					//	{
					//		if(itr->second.itemType == 0)
					//		{
					//			isProduct = 3;
					//			itemCountNum = itr->second.count ;  //***
					//		}
					//		else if(itr->second.itemType == 1)
					//			isProduct = 4;
					//	}
					//}
					if(isProduct != 3)
					{
						vectorForThumbNail.pop_back();
						continue;
					}

					if(!isThumb)
					{

						/*if(itemCountNum  >1)
						{
							itemCountString.Append("(");
							itemCountString.AppendNumber(itemCountNum);
							itemCountString.Append(")");
						}
						else
							itemCountString.Append("");*/

						//listHelperInsidePopulateProductListForSection.AddElement(lstboxControlView , pNode.getName(), kSPTextWidgetID, count );				
						
						//listHelperInsidePopulateProductListForSection.SetDesignerAction(lstboxControlView, count, isProduct ,icon_count, StarFlag,NewProductFlag,itemCountString);//***
						count++;
						itemCountString.Clear();
						itemCountNum=0;
					}
					else
					{
				 		showImageAsThumbnail(pNode, curSelSubecId);
					}
				}
				
			}
			if(VectorFamilyInfoValuePtr)
				delete VectorFamilyInfoValuePtr;
		}
//		else
//		{  // For ONEsource Mode
//			//CA("ONEsource Mode");
//			CurrentSelectedSection = curSelSubecId;
//			CurrentSelectedSubSection = curSelSubecId;
//			VectorObjectInfoPtr vectorObjectValuePtr = NULL;
//			VectorItemModelPtr vectorItemModelPtr = NULL;
//			//ItemMapPtr pItemMap = NULL;
//
//			if(ListFlag == 0)
//			{
//				//CA("1");
//				vectorObjectValuePtr = ptrIAppFramework->GetONEsourceObjects_getONEsourceProductsByClassId(curSelSubecId,global_lang_id);
//				vectorItemModelPtr =  ptrIAppFramework->GetONEsourceObjects_getONEsourceItemsByClassId(curSelSubecId,global_lang_id);
//				//pItemMap = ptrIAppFramework->GetProjectProducts_getAllLeadingItemIds(curSelSubecId);
//				
//			}else if(ListFlag == 1)
//			{
//				//CA("2");
//				vectorObjectValuePtr = ptrIAppFramework->GetONEsourceObjects_getONEsourceProductsByClassId(curSelSubecId,global_lang_id);
//				//pItemMap = ptrIAppFramework->GetProjectProducts_getAllLeadingItemIds(curSelSubecId);
//
//			}else if(ListFlag ==2)
//			{
//				//CA("3");
//				vectorItemModelPtr =  ptrIAppFramework->GetONEsourceObjects_getONEsourceItemsByClassId(curSelSubecId,global_lang_id);
//				//pItemMap = ptrIAppFramework->GetProjectProducts_getAllLeadingItemIds(curSelSubecId);
//			}
//			
//			bool16 QuitFlag = kFalse;
//
//			do
//			{
//				if((ListFlag == 0))
//				{					
//					if((vectorObjectValuePtr == NULL) && (vectorItemModelPtr == NULL))
//					{
//						
//						QuitFlag = kTrue;
//						break;
//					}	
//					
//					if(vectorItemModelPtr != NULL)
//					{						
//						if((vectorObjectValuePtr == NULL) && (vectorItemModelPtr->size() == 0))
//						{
//							QuitFlag = kTrue;
//							break;
//						}
//					}				
//					if(vectorObjectValuePtr !=NULL)
//					{
//						if((vectorObjectValuePtr->size() == 0) && (vectorItemModelPtr == NULL))
//						{
//							QuitFlag = kTrue;
//							break;
//						}
//					}
//					
//				/*	if((vectorObjectValuePtr->size() == 0) && (vectorItemModelPtr->size() == 0))
//					{
//						QuitFlag = kTrue;
//						break;
//					}	*/				
//				}
//				else if((ListFlag == 1))
//				{
//					if(vectorObjectValuePtr == NULL)
//					{
//						QuitFlag = kTrue;
//						break;
//					}
//					if((vectorObjectValuePtr->size() == 0))
//					{
//						QuitFlag = kTrue;
//						break;
//					}
//				}
//				else if((ListFlag == 2))
//				{
//					if(vectorItemModelPtr == NULL)
//					{
//						QuitFlag = kTrue;
//						break;
//					}
//					if((vectorItemModelPtr->size() == 0))
//					{
//						QuitFlag = kTrue;
//						break;
//					}
//				}
//			}while(0);
//					
//			if(QuitFlag)
//			{
//				//CA(" QuitFlag True");
//				if(md.getSprayButtonView()){ //CA("Disabling Spray Button.");
//					md.getSprayButtonView()->Disable();}
//				if(md.getAssignBtnView())
//						md.getAssignBtnView()->Disable();
//				/*if(md.getLocateWidgetView())
//						md.getLocateWidgetView()->Disable();*///--COMMENTED ON 25 FEB 2006 --
//				if(md.getPreviewWidgetView())
//						md.getPreviewWidgetView()->Disable();
//				if(md.getSubSecSprayButtonView())
//					md.getSubSecSprayButtonView()->Disable();
//				if(md.getRefreshButtonView())
//					md.getRefreshButtonView()->Disable();
//				if(md.getIconView())
//					md.getIconView()->Disable();
//				if(md.getIconViewNew())
//					md.getIconViewNew()->Disable();
//
//				//---------
//				if(md.getGreenFilterBtnView())
//					md.getGreenFilterBtnView()->Disable();
//				if(md.getTableSourceBtnView())
//					md.getTableSourceBtnView()->Disable();
//
//				ptrIAppFramework->LogInfo("AP7_ProductFinder::SPSelectionObserver::populateProductListforSection::QuitFlag");
//				break;
//			}
//			int count=0;
//			PMString itemCountString("");
//			int32 itemCountNum =0;
//			if((ListFlag == 0) || (ListFlag == 1) )
//			{
//				if(vectorObjectValuePtr != NULL)
//				{
//					int32 size = static_cast<int32>(vectorObjectValuePtr->size());		
//					if(size > 0)
//					{	
//						VectorObjectInfoValue::iterator it2;				
//						
//						for(it2 = vectorObjectValuePtr->begin(); it2 != vectorObjectValuePtr->end(); it2++)
//						{
//////////////////////////////////////// Chetan --
//							CPbObjectValue obj;
//							CObjectValue cObjectValuesObj;
//
//							obj.setPub_object_id (curSelSubecId);
//							obj.setIndex(it2->getseq_order());
//							obj.setisProduct(1);
//
//							cObjectValuesObj.setLevel_no(it2->getLevel_no());
//							cObjectValuesObj.setParent_id(it2->getParent_id());
//							cObjectValuesObj.setObject_id(it2->getObject_id());
//							cObjectValuesObj.setName(it2->getName());
//							cObjectValuesObj.setChildCount(it2->getChildCount());
//							cObjectValuesObj.setRef_id(it2->getRef_id());
//							cObjectValuesObj.setObject_type_id(it2->getObject_type_id());
//
//							obj.setObjectValue(cObjectValuesObj);
//////////////////////////////////////// Chetan --
//
//							pNode.setPBObjectID(curSelSubecId);
//							pNode.setSequence(it2->getseq_order());	
//							pNode.setIsProduct(1);
//							pNode.setIsONEsource(kTrue);						
//												
//							pNode.setLevel(it2->getLevel_no());
//							pNode.setParentId(it2->getclass_id());					
//							pNode.setPubId(it2->getObject_id());
//
//							if(!iConverter)
//								pNode.setPublicationName(it2->getName());
//							else
//								pNode.setPublicationName(iConverter->translateString(it2->getName()));
//							
//							pNode.setChildCount(it2->getChildCount());
//							pNode.setReferenceId(it2->getRef_id());
//							pNode.setTypeId(it2->getObject_type_id());
//						
//							//PMString ASD("getObject_type_id : ");
//							//ASD.AppendNumber(pNode.getTypeId());
//							//CA(ASD);							
//						
//							int icon_count = 111; //GetIconCountForUnfilteredProductList(it2);
//							pNodeDataList.push_back(pNode);
//
//							vectorForThumbNail.push_back(obj);
//
//							//**********************
//							if( pItemMap != NULL && pItemMap->size() > 0)
//							{
//								ItemMap::iterator itr;
//								int32 searchItemId = pNode.getPubId();
//								itr = pItemMap->find(searchItemId);
//								if(itr != pItemMap->end())
//								{
//									if(ListFlag == 2 && itr->second.itemType == 0)
//									{
//										//vectorForThumbNail.pop_back();
//										continue;
//									}
//									else
//									{
//										//CA("Else");
//										if(itr->second.itemType == 0)
//										{
//										//	isProduct = 3;
//											itemCountNum = itr->second.count ;  //***
//											//CA("itemType == 0");
//										}
//										else if(itr->second.itemType == 1)
//										{
//											//isProduct = 4;
//											
//										}
//										else if(itr->second.itemType == 2)
//										{
//											//isProduct = 1;
//											itemCountNum = itr->second.count ;  //***
//											//CA("//itemType == 2");
//										}										
//									}
//								}
//
//							}
//							//*******************
//
//
//
//							if(!isThumb)
//							{
//								//CA("!isThumb .. Foer Item GRP");
//								if(itemCountNum  >1)
//								{
//									itemCountString.Append("(");
//									itemCountString.AppendNumber(itemCountNum);
//									itemCountString.Append(")");
//								}
//								else
//									itemCountString.Append("");
//								listHelperInsidePopulateProductListForSection.AddElement(lstboxControlView , pNode.getName(), kSPTextWidgetID, count );				
//								listHelperInsidePopulateProductListForSection.SetDesignerAction(lstboxControlView, count, kTrue,icon_count, 0,0,itemCountString);
//								count++;																// 0,0 is set to hide Star & New Product icon in ONEsource Mode.
//								itemCountString.Clear();
//								itemCountNum =0;	
//							}
//							else
//							{
//								showImageAsThumbnail(pNode, curSelSubecId);
//							}
//
//						}	
//					}
//				}
//			}
//			if((ListFlag == 0) || (ListFlag == 2) )
//			{
//
//				if(vectorItemModelPtr != NULL)
//				{
//					int32/*int64*/ size =static_cast<int32> (vectorItemModelPtr->size());		
//					if(size > 0)
//					{
//						//CA("vectorItemModelPtr");
//						VectorItemModelValue::iterator it2;
//						
//						PMString itemCountString("");
//						int32 itemCountNum =0;
//						//int count=0;
//						for(it2 = vectorItemModelPtr->begin(); it2 != vectorItemModelPtr->end(); it2++)
//						{
//////////////////////////////////////// Chetan --
//							CPbObjectValue obj;
//							CItemModel cItemModelsobj;
//
//							obj.setPub_object_id (curSelSubecId);
//							obj.setIndex(1);
//							obj.setisProduct(0);
//							obj.setobject_type_id(ptrIAppFramework->TYPECACHE_getTypeByCode("ITEM_LEVEL"));
//
//							cItemModelsobj.setItemID(it2->getItemID());
//							cItemModelsobj.setClassID(it2->getClassID());
//							cItemModelsobj.setItemID(it2->getItemID());
//							cItemModelsobj.setItemNo(it2->getItemNo());
//							cItemModelsobj.setItemDesc(it2->getItemDesc());
//
//							obj.setItemModel(cItemModelsobj);
//////////////////////////////////////// Chetan --
//							pNode.setPBObjectID(curSelSubecId);
//							pNode.setSequence(1);	
//							pNode.setIsProduct(0);
//							pNode.setIsONEsource(kTrue);
//							pNode.setParentId(it2->getClassID());												
//							pNode.setPubId(it2->getItemID());
//
//							if(!iConverter)
//							{
//								PMString ItemNO(it2->getItemNo());
//								PMString ItemDesp(it2->getItemDesc());
//								if(ItemDesp != "")
//								{
//									ItemNO.Append(": "+ ItemDesp);
//								}
//								pNode.setPublicationName(ItemNO);
//								//CA(ItemNO);
//							}
//							else
//							{
//								PMString ItemNO(iConverter->translateString(it2->getItemNo()));
//								PMString ItemDesp(iConverter->translateString(it2->getItemDesc()));
//								if(ItemDesp != "")
//								{
//									ItemNO.Append(": "+ ItemDesp);
//								}
//								//CA(ItemNO);
//								pNode.setPublicationName(ItemNO);						
//							}
//							pNode.setTypeId(ptrIAppFramework->TYPECACHE_getTypeByCode("ITEM_LEVEL"));
//							
//							int icon_count = 111;// GetIconCountForUnfilteredProductList(it2);
//							pNodeDataList.push_back(pNode);
//							
//							vectorForThumbNail.push_back(obj);  // Chetan --
//
//							//**********************
//							if( pItemMap != NULL && pItemMap->size() > 0)
//							{
//								ItemMap::iterator itr;
//								int32 searchItemId = pNode.getPubId();
//								itr = pItemMap->find(searchItemId);
//								if(itr != pItemMap->end())
//								{
//									if(ListFlag == 2 && itr->second.itemType == 0)
//									{
//										//vectorForThumbNail.pop_back();
//										continue;
//									}
//									else
//									{
//										//CA("Else");
//										if(itr->second.itemType == 0)
//										{
//										//	isProduct = 3;
//											itemCountNum = itr->second.count ;  //***
//											//CA("itemType == 0");
//										}
//										else if(itr->second.itemType == 1)
//										{
//											//isProduct = 4;
//											
//										}
//										else if(itr->second.itemType == 2)
//										{
//											//isProduct = 1;
//											itemCountNum = itr->second.count ;  //***
//											//CA("//itemType == 2");
//										}										
//									}
//								}
//
//							}
//							//*******************
//
//
//							if(!isThumb)
//							{
//
//								if(itemCountNum  >1)
//								{
//									itemCountString.Append("(");
//									itemCountString.AppendNumber(itemCountNum);
//									itemCountString.Append(")");
//								}
//								else
//									itemCountString.Append("");
//								listHelperInsidePopulateProductListForSection.AddElement(lstboxControlView , pNode.getName(), kSPTextWidgetID, count );				
//								listHelperInsidePopulateProductListForSection.SetDesignerAction(lstboxControlView, count, kFalse,icon_count, 0,0,itemCountString);//****
//								count++;																// 0,0 is set to hide Star & New Product icon in ONEsource Mode.
//								itemCountString.Clear();
//								itemCountNum =0;
//							}
//							else
//							{
//								showImageAsThumbnail(pNode, curSelSubecId);
//							}													// 0,0 is set to hide Star & New Product icon in ONEsource Mode.
//						}	
//					}
//				}
//			}
//
//			if(vectorObjectValuePtr)
//				delete vectorObjectValuePtr;
//			if(vectorItemModelPtr)
//				delete vectorItemModelPtr;
//
//		}

////////////////////////////////////////////////////////////////////////////////////////////////////

		if( pNodeDataList.size() > 0 )
		{
			InterfacePtr<ITreeViewMgr> treeViewMgr(spTreeListBoxWidgetView, UseDefaultIID());
			if(!treeViewMgr)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");					
				break;
			}
						
			SPTreeModel pModel;
			PMString pfName("Root");
			pModel.setRoot(-1, pfName, 1);
			treeViewMgr->ClearTree(kTrue);
			pModel.GetRootUID();
			treeViewMgr->ChangeRoot();

			isListBoxPopulated = kTrue;

			//InterfacePtr<IListBoxController> listStringControlData(lstboxControlView,UseDefaultIID());
			//if(listStringControlData)
			{
				CurrentSelectedProductRow = 0; // First Product of List

				IControlView *view1 = iPanelControlData->FindWidget(kSPRefreshWidgetID);
				if(view1)
				{
					view1->Enable();
				}

				IControlView *view2 = iPanelControlData->FindWidget(kSPSubSectionSprayButtonWidgetID);
				if(view2)
				{
					view2->Enable();
				}

				IControlView *view4 = iPanelControlData->FindWidget(kPRLPreviewButtonWidgetID);
				if(view4)
				{   
					view4->Enable();
				}

				IControlView *view6 = iPanelControlData->FindWidget(kSPSprayButtonWidgetID);
				if(view6)
				{	
					view6->Enable();
				}

				md.setCurrentObjectID(pNodeDataList[CurrentSelectedProductRow].getPubId());
				md.setPBObjectID(pNodeDataList[CurrentSelectedProductRow].getPBObjectID());
				Mediator::parentTypeID = pNodeDataList[CurrentSelectedProductRow].getTypeId();//Added on 060209 Amit

				//---------
				if(md.getTableSourceBtnView())
					md.getTableSourceBtnView()->Enable();

				//APSIVA 9 Comment
				//InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(/*kProductImageIFaceBoss*/kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
				//if(ptrImageHelper != nil)
				//{	
				//	AcquireWaitCursor awc ;
				//	awc.Animate() ; 
				//	if(ptrImageHelper->isImagePanelOpen())
				//	{
				//		int32 objectId = pNodeDataList[CurrentSelectedProductRow].getPubId();

				//		/////////////////////////////////////////////////////////////////////////////////////
				//		/* if repositioning of imagepanel is not needed 
				//			comment code enclosed in ///
				//		*/
				//	/*	GSysRect PalleteBounds  = getBoundingBoxOfWindow() ;

				//		int32 TemplateTop		= PalleteBounds.top;
				//		int32 TemplateLeft	    = PalleteBounds.left;
				//		int32 TemplateRight	    = PalleteBounds.right;
				//		int32 TemplateBottom	= PalleteBounds.bottom;

				//		ptrImageHelper->positionImagePanel(TemplateTop , TemplateLeft , TemplateBottom , TemplateRight);*/
				//		/////////////////////////////////////////////////////////////////////////////////////
				//		if(searchResult)
				//		{//CA("if Search result");
				//			if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 1)
				//				ptrImageHelper->showProductImages(objectId , global_lang_id , pNodeDataList[CurrentSelectedProductRow].getParentId() , pNodeDataList[CurrentSelectedProductRow].getTypeId() , pNodeDataList[CurrentSelectedProductRow].getSectionID() ) ;
				//			if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 0)
				//				ptrImageHelper->showItemImages(objectId , global_lang_id , pNodeDataList[CurrentSelectedProductRow].getParentId() , pNodeDataList[CurrentSelectedProductRow].getTypeId() , pNodeDataList[CurrentSelectedProductRow].getSectionID(), kFalse);
				//		}
				//		else
				//		{
				//			//CA("else SpListBoxObserver::Update");
				//			if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 1)
				//				ptrImageHelper->showProductImages(objectId , global_lang_id , Mediator::parentID , Mediator::parentTypeID , Mediator::sectionID ) ;
				//			if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 0)
				//				ptrImageHelper->showItemImages(objectId , global_lang_id , Mediator::parentID , pNodeDataList[CurrentSelectedProductRow].getTypeId() , Mediator::sectionID, kFalse);
				//		}
				//	}
				//}
			
			}
		}
		else
		{
			//CA("isListBoxPopulated =  kFalse");
			isListBoxPopulated =  kFalse;
			if(md.getSprayButtonView()){ //CA("Disabling Spray Button.");
				md.getSprayButtonView()->Disable();}
			if(md.getAssignBtnView())
					md.getAssignBtnView()->Disable();
			/*if(md.getLocateWidgetView())
					md.getLocateWidgetView()->Disable();*/
			if(md.getPreviewWidgetView())
					md.getPreviewWidgetView()->Disable();
			if(md.getSubSecSprayButtonView())
				md.getSubSecSprayButtonView()->Disable();
			if(md.getRefreshButtonView())			
				md.getRefreshButtonView()->Enable();
			//-----------
			if(md.getGreenFilterBtnView())
				md.getGreenFilterBtnView()->HideView();
			if(md.getTableSourceBtnView())
				md.getTableSourceBtnView()->Disable();
			//md.getRefreshButtonView()->Disable();
		}

		/*if(pItemMap)
			delete pItemMap;*/

	}while(kFalse);

	//CA("returning populateProductListforSection()");
	return isListBoxPopulated;
}

//bool8 SPSelectionObserver::populateProductListforSection(int32 curSelSubecId)
//{
//	bool8 isListBoxPopulated = kFalse;
//
//	do
//	{
//		Mediator md;
//		IPanelControlData* iPanelControlData;
//		PublicationNode pNode;
//
//		iPanelControlData=md.getMainPanelCtrlData();
//
//		if(!iPanelControlData)
//		{
//			//CA("Serious Error Again");
//			break;
//		}
//
//		SDKListBoxHelper listHelperInsidePopulateProductListForSection(this, kSPPluginID);
//		//CA("1");
//		IControlView* lstboxControlView=
//		listHelperInsidePopulateProductListForSection.FindCurrentListBox(iPanelControlData, 1);
//		if(lstboxControlView==nil)
//		{
//			CA("lstboxControlView==nil");
//			break;
//		}
//		
//		listHelperInsidePopulateProductListForSection.EmptyCurrentListBox(iPanelControlData, 1);
//
//		md.setSelectedPub(curSelSubecId);
//		md.setCurrSubSectionID(curSelSubecId);
//
//		//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == nil)
//		{
//			CAlert::InformationAlert("Err ptrIAppFramework is nil");
//			//CA("nil");
//			break;
//		}
//
//		//int32 ParentTypeID = ptrIAppFramework->TYPEMngr_getObjectTypeID("PRODUCT_LEVEL");
//		
//		//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//		VectorPubObjectValuePtr VectorFamilyInfoValuePtr = nil;
//		
//		//VectorFamilyInfoValuePtr = ptrIAppFramework->PBObjMngr_getProductsForSubSection(curSelSubecId, ParentTypeID);
//
//		if(ListFlag == 0)
//		{
//			VectorFamilyInfoValuePtr = ptrIAppFramework->getProductsAndItemsForSection(curSelSubecId, global_lang_id );
//
//		}else if(ListFlag == 1)
//		{
//			VectorFamilyInfoValuePtr = ptrIAppFramework->getProductsForSubSection/*GetProjectProducts_getProductsBySubSection*/(curSelSubecId, global_lang_id );
//		}else if(ListFlag ==2)
//		{
//			VectorFamilyInfoValuePtr = ptrIAppFramework->getItemsForSubSection/*GetProjectProducts_getItemsForSubSection*/(curSelSubecId, global_lang_id );
//		}
//
//
//				
//		if(VectorFamilyInfoValuePtr == nil)
//		{
//			//CA(" VectorFamilyInfoValuePtr == NULL ");
//			if(md.getSprayButtonView()){ //CA("Disabling Spray Button.");
//				md.getSprayButtonView()->Disable();}
//			if(md.getAssignBtnView())
//					md.getAssignBtnView()->Disable();
//			/*if(md.getLocateWidgetView())
//					md.getLocateWidgetView()->Disable();*///--COMMENTED ON 25 FEB 2006 --
//			if(md.getPreviewWidgetView())
//					md.getPreviewWidgetView()->Disable();
//			if(md.getSubSecSprayButtonView())
//				md.getSubSecSprayButtonView()->Disable();
//			if(md.getRefreshButtonView())
//				md.getRefreshButtonView()->Disable();
//			break;
//		}
//
//		if(VectorFamilyInfoValuePtr->size()==0)
//		{
//			//CA("VectorFamilyInfoValuePtr->size()==0");
//			if(md.getSprayButtonView()){ //CA("Disabling Spray Button.");
//				md.getSprayButtonView()->Disable();}
//			if(md.getAssignBtnView())
//					md.getAssignBtnView()->Disable();
//			/*if(md.getLocateWidgetView())//--COMMENTED ON 25 FEB 2006 --
//					md.getLocateWidgetView()->Disable();*/
//			if(md.getPreviewWidgetView())
//					md.getPreviewWidgetView()->Disable();
//			if(md.getSubSecSprayButtonView())
//				md.getSubSecSprayButtonView()->Disable();
//			if(md.getRefreshButtonView())
//				md.getRefreshButtonView()->Disable();
//			delete VectorFamilyInfoValuePtr;
//			break;
//		}
//	
//		int32 size = VectorFamilyInfoValuePtr->size();		
//		
//		VectorPubObjectValue::iterator it2;
//		
//		int count=0;
//		for(it2 = VectorFamilyInfoValuePtr->begin(); it2 != VectorFamilyInfoValuePtr->end(); it2++)
//		{	
//				pNode.setPBObjectID(it2->getPub_object_id());
//				pNode.setSequence(it2->getIndex());	
//				pNode.setIsProduct(it2->getisProduct());
//
//				if(it2->getisProduct())
//				{
//					//PMString ASD("getLevel_no : ");
//					//it2->getObjectValue().getName();				
//					//ASD.AppendNumber(it2->getObjectValue().getLevel_no());
//					//CA(it2->getObjectValue().getName());
//					pNode.setLevel(it2->getObjectValue().getLevel_no());
//					pNode.setParentId(it2->getObjectValue().getParent_id());					
//					pNode.setPubId(it2->getObjectValue().getObject_id());
//
//					if(!iConverter)
//						pNode.setPublicationName(it2->getObjectValue().getName());
//					else
//						pNode.setPublicationName(iConverter->translateString(it2->getObjectValue().getName()));
//					
//					pNode.setChildCount(it2->getObjectValue().getChildCount());
//					pNode.setReferenceId(it2->getObjectValue().getRef_id());
//					pNode.setTypeId(it2->getObjectValue().getObject_type_id());
//				
//					//PMString ASD("getObject_type_id : ");
//					//ASD.AppendNumber(pNode.getTypeId());
//					//CA(ASD);					
//				}
//				else
//				{
//					pNode.setPubId(it2->getItemModel().getItemID());
//
//					if(!iConverter)
//					{
//						PMString ItemNO(it2->getItemModel().getItemNo());
//						PMString ItemDesp(it2->getItemModel().getItemDesc());
//						if(ItemDesp != "")
//						{
//							ItemNO.Append(": "+ ItemDesp);
//						}
//						pNode.setPublicationName(ItemNO);
//						//CA(ItemNO);
//					}
//					else
//					{
//						PMString ItemNO(iConverter->translateString(it2->getItemModel().getItemNo()));
//						PMString ItemDesp(iConverter->translateString(it2->getItemModel().getItemDesc()));
//						if(ItemDesp != "")
//						{
//							ItemNO.Append(": "+ ItemDesp);
//						}
//						pNode.setPublicationName(ItemNO);						
//					}
//					pNode.setTypeId(it2->getobject_type_id());
//
//				}
//				int icon_count = GetIconCountForUnfilteredProductList(it2);
//				pNodeDataList.push_back(pNode);
//				
//				listHelperInsidePopulateProductListForSection.AddElement(lstboxControlView , pNode.getName(), kSPTextWidgetID, count );				
//				listHelperInsidePopulateProductListForSection.SetDesignerAction(lstboxControlView, count, it2->getisProduct() ,icon_count);
//				count++;			
//		}		
//
//		InterfacePtr<IListControlData>listcontrol(lstboxControlView,UseDefaultIID());
//		if(!listcontrol)
//		{
//			//CA("No list control");
//			break;
//		}
//		if(listcontrol->Length()>0)
//		{		
//			isListBoxPopulated = kTrue;
//			/*
//				added on 15 feb 2006.....
//			*/
//			InterfacePtr<IListBoxController> listStringControlData(lstboxControlView,UseDefaultIID());
//			if(listStringControlData)
//			{
//				//listStringControlData->SetClickItem(0);
//				listStringControlData->Select(0);
//				CurrentSelectedProductRow = 0; // First Product of List      
//				/*if(md.getSprayButtonView())
//						md.getSprayButtonView()->Enable();
//				if(md.getAssignBtnView())
//						md.getAssignBtnView()->Enable();
//				if(md.getLocateWidgetView())
//						md.getLocateWidgetView()->Enable();
//				if(md.getPreviewWidgetView())
//						md.getPreviewWidgetView()->Enable();*/
//				//-------------------------------------------------------------------------------------//
//				IControlView *view1 = iPanelControlData->FindWidget(kSPRefreshWidgetID);
//				if(view1)
//				{
//					view1->Enable();
//				}
//
//				IControlView *view2 = iPanelControlData->FindWidget(kSPSubSectionSprayButtonWidgetID);
//				if(view2)
//				{
//					view2->Enable();
//				}
//
//				IControlView *view3 = iPanelControlData->FindWidget(kAssignButtonWidgetID);
//				if(view3)
//				{
//					view3->Enable();
//				}
//
//				IControlView *view4 = iPanelControlData->FindWidget(kPRLPreviewButtonWidgetID);
//				if(view4)
//				{   
//					view4->Enable();
//				}
//
//				/*IControlView *view5 = iPanelControlData->FindWidget(kLocateButtonWidgetID);
//				if(view5) //--COMMENTED ON 25 FEB 2006 --
//				{	
//					view5->Enable();
//				}*/
//
//				IControlView *view6 = iPanelControlData->FindWidget(kSPSprayButtonWidgetID);
//				if(view6)
//				{	
//					view6->Enable();
//				}
//
//				InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
//				if(!DataSprayerPtr)
//				{
//					CA("Pointer to DataSprayerPtr not found");
//					break;
//				}
//
//				md.setCurrentObjectID(pNodeDataList[CurrentSelectedProductRow].getPubId());
//				md.setPBObjectID(pNodeDataList[CurrentSelectedProductRow].getPBObjectID());
//				/*PMString objid("The selected object id is ");
//				objid.AppendNumber(pNodeDataList[CurrentSelectedProductRow].getPubId());*/
//				//CA(objid);
//				
//				DataSprayerPtr->FillPnodeStruct(pNodeDataList[CurrentSelectedProductRow],CurrentSelectedSection, CurrentSelectedPublicationID, CurrentSelectedSubSection);
//			}
//		}
//		else
//		{
//			//CA("isListBoxPopulated =  kFalse");
//			isListBoxPopulated =  kFalse;
//			if(md.getSprayButtonView()){ CA("Disabling Spray Button.");
//				md.getSprayButtonView()->Disable();}
//			if(md.getAssignBtnView())
//					md.getAssignBtnView()->Disable();
//			/*if(md.getLocateWidgetView())
//					md.getLocateWidgetView()->Disable();*/
//			if(md.getPreviewWidgetView())
//					md.getPreviewWidgetView()->Disable();
//			if(md.getSubSecSprayButtonView())
//				md.getSubSecSprayButtonView()->Disable();
//			if(md.getRefreshButtonView())
//				md.getRefreshButtonView()->Disable();
//
//		}
//
//	}while(kFalse);
//
//	//CA("returning populateProductListforSection()");
//	return isListBoxPopulated;
//}

double SPSelectionObserver::getSelectedSectionId(IControlView* cntrlView, PMString& curSelSecName)
{
	//CA(" inside getSelectedSectionId ");
	double curSelSecId=-1;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return 0;
	}
	do{
		InterfacePtr<IStringListControlData> sectionDropListData(cntrlView,UseDefaultIID());	
		if (sectionDropListData==nil)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::getSelectedSectionId::No sectionDropListData");		
			break;
		}

		InterfacePtr<IDropDownListController> sectionDropListController(sectionDropListData, UseDefaultIID());
		if (sectionDropListController==nil)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::getSelectedSectionId::No sectionDropListController");		
			break;
		}

		int32 selectedRow = sectionDropListController->GetSelected();

		if(selectedRow <=0)
		{
			PMString SectionComment("");
			//SectionComment.Append("\n Second Line");

			//InterfacePtr<ISPXLibraryButtonData> data( cntrlView ,IID_ISPXLIBRARYBUTTONDATA );
			//if ( data ){
			//	//PMString ToolTip(" Tool Tip By Rahul Dalvi " );
			//	data->SetName(SectionComment);
			//}
			//ptrIAppFramework->LogInfo("AP7_ProductFinder::SPSelectionObserver::getSelectedSectionId::selectedRow <=0");		
			break;
		}

		SectionData sectionInfo;
		PubData publdata = sectionInfo.getSectionVectorInfo(selectedRow-1);
		curSelSecId=publdata.getPubId();

		Mediator md;
		md.setCurrSectionID(curSelSecId);
		md.setCurSecName(publdata.getPubName());
		md.setCurrentSelectedRow(selectedRow);
		curSelSecName=publdata.getPubName();

		PMString SectionComment = publdata.getPubName();	
		PMString seccomm = publdata.getComment();
		if(seccomm.NumUTF16TextChars() >0)
		{
			SectionComment.Append("\n");
			SectionComment.Append(seccomm);
		}
		//CA(SectionComment);
		//IPanelControlData* iPanelControlData;
		//iPanelControlData=md.getMainPanelCtrlData();

		//IControlView *sectionText1View = iPanelControlData->FindWidget(kSectionText_1WidgetID);
		//if(sectionText1View == nil)
		//{
		//	//CA("sectionText1View == nil ");
		//	ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::CallLevelTwoThree::No sectionText1View");				
		//	break;
		//}

		//InterfacePtr<ISPXLibraryButtonData> data( cntrlView,IID_ISPXLIBRARYBUTTONDATA );
		//if ( data ){
		//	//CA(" Tool Tip By Rahul Dalvi " + SectionComment );
		//	data->SetName(SectionComment);
		//}

		/*CPubModel model = global_vector_pubmodel[selectedRow - 1];
		curSelSecId = model.getEventId();*/

	}while(kFalse);
	return curSelSecId;
}

double SPSelectionObserver::getSelectedSubsectionId(IControlView* cntrlView, PMString& curSubSecName)
{
	//CA("getSelectedSubsectionId");
	Mediator md;
	double curSelSubsecId=-1;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return 0;
	}
	do{
		InterfacePtr<IStringListControlData> subSectionDropListData(cntrlView,UseDefaultIID());	
		if(subSectionDropListData==nil)
		{
			//CA("subSectionDropListData nil");
			break;
		}

		InterfacePtr<IDropDownListController> subSectionDropListController(subSectionDropListData, UseDefaultIID());
		if(subSectionDropListController==nil)
		{
			//CA("subSectionDropListController");
			PMString SectionComment("");
			//InterfacePtr<ISPXLibraryButtonData> data( cntrlView ,IID_ISPXLIBRARYBUTTONDATA );
			//if ( data ){
			//	//PMString ToolTip(" Tool Tip By Rahul Dalvi " );
			//	data->SetName(SectionComment);
			//}
			break;
		}

		int32 selectedRow=subSectionDropListController->GetSelected();
		if(selectedRow <=0)
		{
			//CA("selectedRow is 0");
			break;
		}
		md.setCurrentSubSectionRow(selectedRow);		

		SectionData subSectionInfo;
		PubData publdata=subSectionInfo.getSubsectionVectorInfo(selectedRow-1);
		curSelSubsecId=publdata.getPubId();
 
		Mediator md;
		md.setCurSubSecName(publdata.getPubName());
		curSubSecName=publdata.getPubName();

		PMString SectionComment = publdata.getPubName();
		PMString seccomm = publdata.getComment();
		if(seccomm.NumUTF16TextChars() >0)
		{
			SectionComment.Append("\n");
			SectionComment.Append(seccomm);
		}

		//InterfacePtr<ISPXLibraryButtonData> data( cntrlView ,IID_ISPXLIBRARYBUTTONDATA );
		//if ( data ){
		//	//PMString ToolTip(" Tool Tip By Rahul Dalvi " );
		//	data->SetName(SectionComment);
		//}


		
	}while(kFalse);
	return curSelSubsecId;
}
double SPSelectionObserver::getSelectedSubSEctionId(IControlView* cntrlView)
{
	//CA("getSelectedSubSEctionId");
	double curSelSubsecId=-1;
	/*do{
		InterfacePtr<IStringListControlData> subSectionDropListData(cntrlView,UseDefaultIID());	
		if (subSectionDropListData==nil)
			break;

		InterfacePtr<IDropDownListController> subSectionDropListController(subSectionDropListData, UseDefaultIID());
		if (subSectionDropListController==nil)
			break;

		int32 selectedRow=subSectionDropListController->GetSelected();

		selectrowForRefresh = selectedRow;

		if(selectedRow <=0)
			break;
		
		SectionData subSectionInfo;
		PubData publdata=subSectionInfo.getSubsectionVectorInfo(selectedRow-1);
		curSelSubsecId=publdata.getPubId();
		CurrentSelectedSection=curSelSubsecId;
	}while(kFalse);*/
	return curSelSubsecId;
}

void SPSelectionObserver::populateFilteredProductList(double curSelSubecId,PMString as)
{
	//CA("populateFilteredProductList");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return;
	}
	
		do
		{
				Mediator md;
				IPanelControlData* iPanelControlData;
				PublicationNode pNode;

				iPanelControlData=md.getMainPanelCtrlData();

				if(!iPanelControlData)
				{
					//CA("This is too much");
					break;
				}

				SDKListBoxHelper listHelperInsidePopulateFilteredProductList(this, kSPPluginID);
				//CA("1");
				IControlView* lstboxControlView=
				listHelperInsidePopulateFilteredProductList.FindCurrentListBox(iPanelControlData, 1);
				if(lstboxControlView==nil)
				{
					//CA("lstboxControlView==nil");
					break;
				}

				md.setSelectedPub(curSelSubecId);
				md.setCurrSubSectionID(curSelSubecId);

				InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
				if(ptrIAppFramework == nil)
				{
					//CAlert::InformationAlert("Err ptrIAppFramework is nil");
					//("nil");
					break;
				}

				//int32 ParentTypeID = ptrIAppFramework->TYPEMngr_getObjectTypeID("PRODUCT_LEVEL");
				
				//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));

				//VectorPubObjectValuePPtr VectorFamilyInfoValuePtr = nil;
				//VectorFamilyInfoValuePtr = ptrIAppFramework->PBObjMngr_getProductsForSubSection(curSelSubecId, ParentTypeID);
				//		VectorFamilyInfoValuePtr = ptrIAppFramework->PUBCntrller_getProductsBySubSecIdTypeIdMstoneIdDueUserId(curSelSubecId,as.GetPlatformString().c_str());

				//if(VectorFamilyInfoValuePtr == nil)
					//break;
				//CA("VectorFamilyInfoValuePtr ssss");

				//if(VectorFamilyInfoValuePtr->size()==0)
				//{
				//	//removed comment
				//	CA("VectorFamilyInfoValuePtr->size()==0");
				//	delete VectorFamilyInfoValuePtr;
				//	break;
				//}
			
				listHelperInsidePopulateFilteredProductList.EmptyCurrentListBox(iPanelControlData, 1);

				/* -- commented on 31 jan 06 4 - 22
				VectorPubObjectValuePointer::iterator it1;
				VectorPubObjectValue::iterator it2;
				*/

				//CA("2");
				int count=0;
				//for(it1 = VectorFamilyInfoValuePtr->begin(); it1 != VectorFamilyInfoValuePtr->end(); it1++)
				//{	
				//	for(it2=(*it1)->begin(); it2 !=(*it1)->end(); it2++)
				//	{	
				//		//int32 Sec; 
				//		pNode.setLevel(it2->getObjectValue().getLevel_no());
				//		pNode.setParentId(it2->getObjectValue().getParent_id());
				//		pNode.setSequence(it2->getIndex());
				//		

				//		pNode.setPubId(it2->getObjectValue().getObject_id());
				//		if(!iConverter)
				//			pNode.setPublicationName(it2->getObjectValue().getName());
				//		else
				//			pNode.setPublicationName(iConverter->translateString(it2->getObjectValue().getName()));
				//		
				//		pNode.setChildCount(it2->getObjectValue().getChildCount());
				//		pNode.setReferenceId(it2->getObjectValue().getRef_id());
				//		pNode.setTypeId(it2->getObjectValue().getObject_type_id());
				//		//pNodeDataList.push_back(pNode);
				//		pNodeDataList.push_back(pNode);
				//		
				//		//CA("3");
				//		//CA(pNode.getName());

				//		
				//		int icon_count = GetIconCountForUnfilteredProductList(it2);

				//		 listHelperInsidePopulateFilteredProductList.AddElement(lstboxControlView , pNode.getName(), kSPTextWidgetID,count );
				//		 listHelperInsidePopulateFilteredProductList.SetDesignerAction(lstboxControlView, count, kTrue,icon_count);
				//		//Sec= rand()%60;
				//		//CA(ASD);
				//		//int32 ABC= Sec%3;
				//		//listHelper.AddIcons(FirstListBoxView, count, ABC);
				//		count++;
				//	}
				//}
						
				if(md.getSprayButtonView()== nil)
				{
					//CA("md.getSprayButtonView() == nil");
				}
				else
					md.getSprayButtonView()->Disable();

	}while(kFalse);	

}

int SPSelectionObserver :: GetIconCountForUnfilteredProductList(VectorPubObjectValue::iterator it2)
{
					/* entire fun commented on 31 jan 06 , 4-18 pm */
					/*CA("Inside GetIconCountForUnfilteredProductList");*/

					int icon_count = 0;
					bool8 isAnyFlagSet = kFalse;
					bool8 isNewProductFlagSet = kFalse;

					if(it2->getNew_product()  == kTrue)
					{	
						//pNode.setNewProduct( 0 );
						//CA("it2->getNew_product()  == kTrue");
						isNewProductFlagSet=kTrue;
						icon_count = 0;
						isAnyFlagSet = kTrue;

					}

					if(isNewProductFlagSet)
					{
						if(it2->getAdd_to_spread())
						{	
							//CA("getAdd_to_spread");
							//pNode.setDesignerAction( 1 );
							icon_count = 7;
							isAnyFlagSet = kTrue;
						}


						else if(it2->getUpdate_spread())
						{	
							//CA("getAdd_to_spread");
							bool8 isUpdateCopyCheck=kFalse,isUpdateArtCheck=kFalse,isUpdateItemTableCheck=kFalse;
							isAnyFlagSet = kTrue;
							icon_count = 21;
							if(it2->getUpdate_copy())	
							{
								//pNode.setDesignerAction( 2 );
								icon_count = 8;
								isAnyFlagSet = kTrue;
								isUpdateCopyCheck = kTrue;
							}

							if(it2->getUpdate_art())
							{
								//pNode.setDesignerAction( 3 );
								icon_count = 9;
								isAnyFlagSet = kTrue;
								isUpdateArtCheck = kTrue;
							}

							if(it2->getUpdate_item_table())
							{
								//pNode.setDesignerAction( 4 );
								icon_count = 10;
								isAnyFlagSet = kTrue;
								isUpdateItemTableCheck = kTrue;
							}


							if(isUpdateCopyCheck && isUpdateArtCheck && isUpdateItemTableCheck)
							{
								icon_count = 11;
								

							}
							else if(isUpdateCopyCheck && isUpdateArtCheck)
							{
								icon_count = 12;
							}
							else if(isUpdateArtCheck && isUpdateItemTableCheck)
							{
								icon_count = 13;
							}
							else if(isUpdateCopyCheck && isUpdateItemTableCheck)
							{
								icon_count = 14;
							}
							}

						else if(it2->getDelete_from_spread())
						{	
							//CA("getDelete_from_spread");
							//pNode.setDesignerAction( 5);
							icon_count = 15;
							isAnyFlagSet = kTrue;
						}		

						if(!isAnyFlagSet)
						{
							icon_count=111;							
						}
					}//if(isNewProductFlagSet) is ended
					else
					{
						
						//////////////////////
						if(it2->getAdd_to_spread())
						{	//CA("getAdd_to_spread");
							//pNode.setDesignerAction( 1 );
							icon_count = 1;
							isAnyFlagSet = kTrue;
						}


						else if(it2->getUpdate_spread())
						{	
							//CA("getUpdate_spread");
							bool8 isUpdateCopyCheck=kFalse,isUpdateArtCheck=kFalse,isUpdateItemTableCheck=kFalse;
							icon_count = 20;
							isAnyFlagSet = kTrue;
							if(it2->getUpdate_copy())	
							{
								//pNode.setDesignerAction( 2 );
								//CA("getUpdate_copy");
								icon_count = 2;
								isAnyFlagSet = kTrue;
								isUpdateCopyCheck = kTrue;
							}

							if(it2->getUpdate_art())
							{
								//pNode.setDesignerAction( 3 );
								//CA("getUpdate_art");
								icon_count = 3;
								isAnyFlagSet = kTrue;
								isUpdateArtCheck = kTrue;
							}

							if(it2->getUpdate_item_table())
							{
								//pNode.setDesignerAction( 4 );
								//CA("getUpdate_item_table");
								icon_count = 4;
								isAnyFlagSet = kTrue;
								isUpdateItemTableCheck = kFalse;
							}


							if(isUpdateCopyCheck && isUpdateArtCheck && isUpdateItemTableCheck)
							{											
								icon_count = 16;

							}
							else if(isUpdateCopyCheck && isUpdateArtCheck)
							{
								
								icon_count = 17;
							}
							else if(isUpdateArtCheck && isUpdateItemTableCheck)
							{
								icon_count = 18;
							}
							else if(isUpdateCopyCheck && isUpdateItemTableCheck)
							{
								icon_count = 19;
							}
							}

							else if(it2->getDelete_from_spread())
								{	//CA("getDelete_from_spread");
									//pNode.setDesignerAction( 5);
									icon_count = 5;
									isAnyFlagSet = kTrue;
								}		

						if(!isAnyFlagSet)
						{
							icon_count=111;							
						}
						//////////////////////

					}

					/*CA(" LEAVING GetIconCountForUnfilteredProductList ");*/
					return icon_count;
					//return 1 ;
}


//int SPSelectionObserver :: GetIconCountWhenDesignerActionSelected(VectorPubObjectValue::iterator it2 , vector<bool8> DesignerActionFlags)
//{
//					int icon_count =111;
//					//-------------------- all fun commented on 31 jan 06 -------------------------------//
//
//					//if(it2->getNew_product() && DesignerActionFlags[0])
//					//{
//					//	
//					//	icon_count =0;
//					//	
//					//			if((it2->getAdd_to_spread() && DesignerActionFlags[1]))
//					//			{
//					//				icon_count = 7;//new + add icon
//					//			}
//					//			else if((it2->getUpdate_spread() && DesignerActionFlags[2]))
//					//			{
//					//				//CA("1");
//					//				icon_count = 21; //new + update icon
//					//					bool8 isUpdateCopyCheck=kFalse,isUpdateArtCheck=kFalse,isUpdateItemTableCheck=kFalse;
//					//					if(it2->getUpdate_copy() && DesignerActionFlags[3])
//					//					{
//					//						icon_count = 8;  //new + update + copy icon
//					//						isUpdateCopyCheck=kTrue;
//					//					}
//					//					if(it2->getUpdate_art() && DesignerActionFlags[4])
//					//					{
//					//						icon_count = 9;//new + update + art icon
//					//						isUpdateArtCheck=kTrue;
//
//					//					}
//					//					if(it2->getUpdate_item_table() && DesignerActionFlags[5])
//					//					{
//					//						icon_count = 10 ;//new + update + item_table icon
//					//						isUpdateItemTableCheck = kTrue;
//					//					}
//
//					//					if(isUpdateCopyCheck && isUpdateArtCheck && isUpdateItemTableCheck)
//					//					{
//					//						icon_count = 11;
//	
//					//					}
//					//					else if(isUpdateCopyCheck && isUpdateArtCheck)
//					//					{
//					//						icon_count = 12;
//					//					}
//					//					else if(isUpdateArtCheck && isUpdateItemTableCheck)
//					//					{
//					//						icon_count = 13;
//					//					}
//					//					else if(isUpdateCopyCheck && isUpdateItemTableCheck)
//					//					{
//					//						icon_count = 14;
//					//					}
//					//			}
//					//			else if(it2->getDelete_from_spread() && DesignerActionFlags[6])
//					//			{
//					//				icon_count = 15;
//					//			}
//					//}
//					//else
//					//{
//					//	//icon_count = 0;
//					//	
//					//			if((it2->getAdd_to_spread() && DesignerActionFlags[1]))
//					//			{
//					//				icon_count = 1;//only add_to_spread
//					//			}
//					//			else if((it2->getUpdate_spread() &&DesignerActionFlags[2]))
//					//			{
//					//				icon_count = 20 ;//only update
//					//					bool8 isUpdateCopyCheck=kFalse,isUpdateArtCheck=kFalse,isUpdateItemTableCheck=kFalse;
//					//					if(it2->getUpdate_copy() && DesignerActionFlags[3])
//					//					{
//					//						icon_count = 2;//only updateproduct and update copy
//					//						isUpdateCopyCheck=kTrue;
//					//					}
//					//					if(it2->getUpdate_art() && DesignerActionFlags[4])
//					//					{
//					//						icon_count = 3;
//					//						isUpdateArtCheck=kTrue;
//
//					//					}
//					//					if(it2->getUpdate_item_table() && DesignerActionFlags[5])
//					//					{
//					//						icon_count = 4;
//					//						isUpdateItemTableCheck = kTrue;
//					//					}
//
//					//					if(isUpdateCopyCheck && isUpdateArtCheck && isUpdateItemTableCheck)
//					//					{
//					//						icon_count = 16;
//	
//					//					}
//					//					else if(isUpdateCopyCheck && isUpdateArtCheck)
//					//					{
//					//						icon_count = 17;
//					//					}
//					//					else if(isUpdateArtCheck && isUpdateItemTableCheck)
//					//					{
//					//						icon_count = 18;
//					//					}
//					//					else if(isUpdateCopyCheck && isUpdateItemTableCheck)
//					//					{
//					//						icon_count = 19;
//					//					}
//					//			}
//					//			else if(it2->getDelete_from_spread() && DesignerActionFlags[6])
//					//			{
//					//				icon_count = 5;
//					//			}
//					//
//					//}
//		return icon_count;
//}



void SPSelectionObserver :: AddProductsToListbox(vector<bool8> DesignerActionFlags, IPMUnknown *boss )
{
	//CA("AddProductsToListbox");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CAlert::InformationAlert("Err ptrIAppFramework is nil");						
		return;
	}
	do{

		PublicationNode pNode;
		Mediator md;
		
		
		//int32 ParentTypeID = ptrIAppFramework->TYPEMngr_getObjectTypeID("PRODUCT_LEVEL");		
		//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));		
		/*if(!iConverter)
		{
			CA("No iconverter");
			break;
		}*/
		//VectorPubObjectMilestoneValuePPtr VectorFamilyInfoValuePtr = nil;	

		PMString milestoneid("");
		bool8 check=FALSE;

		//milestone_selected=kFalse;
		for(int32 i=0;i<samitvflist.size();i++)//
		{
			//CA("Inside the for loop in dialog update");
			if(samitvflist[i].isSelected==kTrue)
			{
				//CA("8");
				//milestone_selected=kTrue;
				if(check==TRUE)
				milestoneid.Append(",");
				milestoneid.AppendNumber(samitvflist[i].milestone_id);
				check=TRUE;
				//CA(milestoneid);
			}
		}


		double curSelSubecId;
		curSelSubecId = md.getCurrSectionID();
		//start 7/3/05
		PMString cursec("The current selection ID is ");
		cursec.AppendNumber(curSelSubecId);
		
		//GlobalDesignerActionFlags.clear();
		//GlobalDesignerActionFlags = DesignerActionFlags;
		
		
		
		//if(milestone_selected)
		//{
		//	//VectorFamilyInfoValuePtr = ptrIAppFramework->PUBCntrller_getProductsBySubSectionIdTypeIdMstoneIdDueUserIdDesignerActions(curSelSubecId,milestoneid.GetPlatformString().c_str(),DesignerActionFlags[0],DesignerActionFlags[1],DesignerActionFlags[2],DesignerActionFlags[3],DesignerActionFlags[4],DesignerActionFlags[5],DesignerActionFlags[6]);//8.2.5
		//}
		//else
		//{
		//	//	VectorFamilyInfoValuePtr = ptrIAppFramework->PBObjMngr_getProductsForSubSection(curSelSubecId, ParentTypeID);

		//}

		pNodeDataList.clear();
		
		IControlView * FirstListBoxView = md.getListBoxView();
       						
		if(!FirstListBoxView)
		{
		//CA("!FirstListBoxView");
			return;
		}
		
		PMString as;
		as.Append("Amit");
		//InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		IPanelControlData* iPanelControlData;
		iPanelControlData=md.getMainPanelCtrlData();

		if(!iPanelControlData)
		{
			//CA("No iPanelControlData");
			break;
		}
		
		//end 8.2.5

		SDKListBoxHelper listHelperInsideUpdateInsideFilter(boss, kSPPluginID);

		//if(VectorFamilyInfoValuePtr == nil)
		{
			
			listHelperInsideUpdateInsideFilter.EmptyCurrentListBox(iPanelControlData,1);
			break;
		}
		
		PMString vectorsize("The size of the product vector is ");
		//vectorsize.AppendNumber(VectorFamilyInfoValuePtr->size());

		/*if(VectorFamilyInfoValuePtr->size()<=0)
		{
			CA("no product found");
			delete VectorFamilyInfoValuePtr;
			break;
		}*/

		listHelperInsideUpdateInsideFilter.EmptyCurrentListBox(iPanelControlData,1);
		/* -- commented on 31 jan 06 4-23 
		VectorPubObjectValuePointer::iterator it1;
		VectorPubObjectValue::iterator it2;
		*/
		
		int count=0;
		bool8 none_of_the_designer_action_selected = kFalse;
		vector<PMString> vectorpNodeGetName;// = nil;
		vector<int> vectorIconCount;// = nil;
		//CA("6");
		//for(it1 = VectorFamilyInfoValuePtr->begin(); it1 != VectorFamilyInfoValuePtr->end(); it1++)
		//	{
		//		//CA("for 17");
		//		for(it2=(*it1)->begin(); it2 !=(*it1)->end(); it2++)
		//		{	
		//			//CA("for 17.1");
		//			pNode.setLevel(it2->getObjectValue().getLevel_no());

		//			pNode.setParentId(it2->getObjectValue().getParent_id());
		//			pNode.setSequence(it2->getIndex());
		//			

		//			pNode.setPubId(it2->getObjectValue().getObject_id());
		//			if(!iConverter)
		//			{
		//				
		//				pNode.setPublicationName(it2->getObjectValue().getName());
		//				
		//			}
		//			else
		//			{
		//				
		//				pNode.setPublicationName(iConverter->translateString(it2->getObjectValue().getName()));
		//				
		//			}
		//			
		//			pNode.setChildCount(it2->getObjectValue().getChildCount());
		//			pNode.setReferenceId(it2->getObjectValue().getRef_id());
		//			pNode.setTypeId(it2->getObjectValue().getObject_type_id());
		//			
		//			int icon_count=111;//for default i.e all icons are hidden
		//			if(DesignerActionFlags.size()!= 7)
		//			{
		//				CA("DesignerActionFlags size is not equal to 7");
		//				break;
		//			}

		//			if(DesignerActionFlags[0] || DesignerActionFlags[1] || DesignerActionFlags[2] || DesignerActionFlags[3] || DesignerActionFlags[4] || DesignerActionFlags[5] || DesignerActionFlags[6])
		//			{
		//				none_of_the_designer_action_selected = kFalse;
		//				if(it2->getNew_product() || it2->getAdd_to_spread() || it2->getUpdate_spread() || it2->getUpdate_copy() || it2->getUpdate_art() || it2->getUpdate_item_table() ||  it2->getDelete_from_spread() )
		//				{
		//					if((it2->getNew_product()&& DesignerActionFlags[0]) || (it2->getAdd_to_spread() && DesignerActionFlags[1]) || (it2->getUpdate_spread() && DesignerActionFlags[2]) || (it2->getUpdate_copy() && DesignerActionFlags[3]) || (it2->getUpdate_art() && DesignerActionFlags[4]) || (it2->getUpdate_item_table() && DesignerActionFlags[5]) ||  (it2->getDelete_from_spread() && DesignerActionFlags[6]))
		//					{
		//						pNodeDataList.push_back(pNode);
		//						SPSelectionObserver sp(boss);									
		//						icon_count =sp.GetIconCountWhenDesignerActionSelected(it2 , DesignerActionFlags);
		//					}
		//					else
		//						continue;
		//				}
		//				else
		//					continue;
		//			}
		//			else
		//			{
		//				//list of unfiltered products
		//				pNodeDataList.push_back(pNode);
		//				none_of_the_designer_action_selected = kTrue;

		//				SPSelectionObserver sObserver(boss);
		//				icon_count = sObserver.GetIconCountForUnfilteredProductList(it2);
		//			}	//all icons are hidden i.e no icon
		//			
		//			//vectorpNodeGetName.push_back(pNode.getName());
		//			//CA("before add element");
		//			//listHelperInsideUpdateInsideFilter.AddElement(FirstListBoxView , pNode.getName(), kSPTextWidgetID, count );
		//			//CA("after addelement");
		//			//added on 15.2.05
		//			/*if( (!none_of_the_designer_action_selected) && (pNodeDataList.size()>0))
		//			{
		//				SPSelectionObserver sp(this);									
		//				icon_count =sp.GetIconCountWhenDesignerActionSelected(it2 , DesignerActionFlags);
		//				//vectorIconCount.push_back(icon_count);
		//			}*/
		//		
		//			listHelperInsideUpdateInsideFilter.AddElement(FirstListBoxView , pNode.getName(), kSPTextWidgetID, count );							
		//			listHelperInsideUpdateInsideFilter.SetDesignerAction(FirstListBoxView, count, kTrue,icon_count);//2.2.5			
		//			count++; 
		//		}//inner for completed


		//	}//outer for completed
//CA("7");

		/*for(int i=0;i<=vectorIconCount.size();i++)
		{
			CA("In for");
			if(vectorpNodeGetName[i]=="")
			{
				CA("NO name");
			}
			listHelper.AddElement(FirstListBoxView ,vectorpNodeGetName[i] , kSPTextWidgetID, i);
			CA("before designeraction");
			listHelper.SetDesignerAction(FirstListBoxView, i, kTrue,vectorIconCount[i]);//2.2.5
			CA("end for");
		}
		
		vectorpNodeGetName.clear();
		vectorIconCount.clear();*/



		//if(VectorFamilyInfoValuePtr)
		//{
		//	
		//	for(it1=VectorFamilyInfoValuePtr->begin(); it1!=VectorFamilyInfoValuePtr->end(); it1++)
		//	{
		//		
		//		it2 = (*it1)->begin();
		//	} 
		//	it1= VectorFamilyInfoValuePtr->begin(); 
		//}//added on 1/3/05


		/*if(DesignerActionFlags[0] || DesignerActionFlags[1] || DesignerActionFlags[2] || DesignerActionFlags[3] || DesignerActionFlags[4] || DesignerActionFlags[5] || DesignerActionFlags[6])
		{
			md.getRefreshButtonView()->Enable(kTrue);//1.3.5
			//md.getSprayButtonView()->Enable();//1.3.5	
			//md.getSubSecSprayButtonView()->Enable();
			md.getAssignBtnView()->HideView();
			md.getGreenFilterBtnView()->ShowView();
			md.getGreenFilterBtnView()->Enable();
			md.getIconView()->Enable();
		}
		else
		{
			md.getSprayButtonView()->Disable();//1.3.5	
			md.getGreenFilterBtnView()->HideView();
			md.getAssignBtnView()->ShowView();
			md.getAssignBtnView()->Enable();
			md.getIconView()->Enable(kTrue);

		}*/

	}
	while(kFalse);

			
}

//added on 3 march
void SPSelectionObserver :: SPRefresh()
{
	InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
	if(iPanelControlData == nil)
	return;	
	SDKListBoxHelper listHelper(this, kSPPluginID);

	listHelper.EmptyCurrentListBox(iPanelControlData, 1);
	
	Mediator md;
	if(FilterFlag==kFalse)
	{//2
		pNodeDataList.clear();

		md.setSelectedPub(-1);//Subsection
		populateSectionDropDownList(md.getPublicationRoot());
	}//1
	else
	{//2

	Mediator md;

    //	//PublicatioNode pNode will be used as a element in the PublicationNodeList pNodeDataList.
	PublicationNode pNode;
	SDKListBoxHelper listHelperForRefresh(this, kSPPluginID);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{//3
		//CAlert::InformationAlert("Err ptrIAppFramework is nil");				
		return;
	}//2
	//CA("RefreshWidgetID 1.4");
	//int32 ParentTypeID = ptrIAppFramework->TYPEMngr_getObjectTypeID("PRODUCT_LEVEL");
	////CA("5");
	//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	////CA("6");
	//VectorPubObjectMilestoneValuePPtr VectorFamilyInfoValuePtr = nil;
	//
	PMString milestoneid("");
	bool8 check=FALSE;
	////CA("7");
	for(int32 i=0;i<samitvflist.size();i++)//
	{//3
		//		//CA("Inside the for loop in dialog update");
		if(samitvflist[i].isSelected==kTrue)
		{//4
			//			//CA("8");
			if(check==TRUE)
			milestoneid.Append(",");
			milestoneid.AppendNumber(samitvflist[i].milestone_id);
			check=TRUE;
			//CA(milestoneid);
		}//3
	}//2
	//CA("RefreshWidgetID 1.5");
	double curSelSubecId;
	curSelSubecId = md.getCurrSectionID();

	//if(ShowAllProductsRadioTriStateFlag)
	//{
				
		//GlobalDesignerActionFlags[0] = GlobalDesignerActionFlags[1] = GlobalDesignerActionFlags[2] = GlobalDesignerActionFlags[3] = GlobalDesignerActionFlags[4] = GlobalDesignerActionFlags[5] = GlobalDesignerActionFlags[6] = kFalse;		
		// Commented By Rahul				//VectorFamilyInfoValuePtr = ptrIAppFramework->PBObjMngr_getProductsForSubSection(curSelSubecId, ParentTypeID);
	//}
	// if(FilterByMilestonesRadioTriStateFlag)
	//{
			//CA("inside milestones");
			//GlobalDesignerActionFlags[0] = GlobalDesignerActionFlags[1] = GlobalDesignerActionFlags[2] = GlobalDesignerActionFlags[3] = GlobalDesignerActionFlags[4] = GlobalDesignerActionFlags[5] = GlobalDesignerActionFlags[6] = kFalse;		
			//if(milestone_selected)
			//{
			//	//CA("A");
			//		//VectorFamilyInfoValuePtr = ptrIAppFramework->PUBCntrller_getProductsBySubSectionIdTypeIdMstoneIdDueUserIdDesignerActions(curSelSubecId,milestoneid.GetPlatformString().c_str(),GlobalDesignerActionFlags[0],GlobalDesignerActionFlags[1],GlobalDesignerActionFlags[2],GlobalDesignerActionFlags[3],GlobalDesignerActionFlags[4],GlobalDesignerActionFlags[5],GlobalDesignerActionFlags[6]);//8.2.5
			//	//VectorFamilyInfoValuePtr = ptrIAppFramework->PUBCntrller_getProductsBySubSectionIdMilestoneIds(curSelSubecId,milestoneid.GetPlatformString().c_str(),kTrue);//,DesignerActionFlags[0],DesignerActionFlags[1],DesignerActionFlags[2],DesignerActionFlags[3],DesignerActionFlags[4],DesignerActionFlags[5],DesignerActionFlags[6]);//8.2.5
			//	//VectorFamilyInfoValuePtr =  ptrIAppFramework->PUBCntrller_getProductsBySubSecIdTypeIdMstoneIdDueUserId(curSelSubecId,milestoneid.GetPlatformString().c_str());
			//}
			//else
			//{
			//		//VectorFamilyInfoValuePtr = nil;

			//}
	//}
	
	//if(FilterByDesignerActionsRadioTriStateFlag)
	//{
	//		if(!(GlobalDesignerActionFlags[0] || GlobalDesignerActionFlags[1] || GlobalDesignerActionFlags[2] || GlobalDesignerActionFlags[3] || GlobalDesignerActionFlags[4] || GlobalDesignerActionFlags[5] || GlobalDesignerActionFlags[6]))
	//		{
	//				//	VectorFamilyInfoValuePtr = nil;
	//		}
	//		else
	//		{
	//			//CA("1");
	//				//VectorFamilyInfoValuePtr = ptrIAppFramework->PUBCntrller_getProductsBySubSectionIdTypeIdMstoneIdDueUserIdDesignerActions(curSelSubecId,milestoneid.GetPlatformString().c_str(),GlobalDesignerActionFlags[0],GlobalDesignerActionFlags[1],GlobalDesignerActionFlags[2],GlobalDesignerActionFlags[3],GlobalDesignerActionFlags[4],GlobalDesignerActionFlags[5],GlobalDesignerActionFlags[6]);//8.2.5
	//			//VectorFamilyInfoValuePtr = ptrIAppFramework->PBObjMngr_getProductsForSubSection(curSelSubecId, ParentTypeID);
	//		}

	//}
				
	//CA("RefreshWidgetID 1.6");
	//
	////8.2.5
	pNodeDataList.clear();
	////CA("12");
	IControlView * FirstListBoxView = md.getListBoxView();
	////CA("13");
		if(!FirstListBoxView){//3
		//CA("!FirstListBoxView");
			return;
		}//2
	////CA("14");
		PMString as;
		as.Append("Amit");
	////InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	IPanelControlData* iPanelControlData;
	iPanelControlData=md.getMainPanelCtrlData();

	if(!iPanelControlData)
	{
		//CA("iPanelControlData nil");			
		return;
	}
	////CA("15");
	////end 8.2.5
	if(/*VectorFamilyInfoValuePtr == nil*/1)
	{//3
	
		listHelperForRefresh.EmptyCurrentListBox(iPanelControlData,1);
		return;
	}//2
	
	if(/*VectorFamilyInfoValuePtr->size()==0*/1)
	{//3
		//CA("no product found");
		/*delete VectorFamilyInfoValuePtr;*/
		return;
	}//2
	//CA("RefreshWidgetID 1.7");
	listHelperForRefresh.EmptyCurrentListBox(iPanelControlData,1);

	}//1
}//0
//ended on 3 march

//10.3.05
void SPSelectionObserver::SelectFrame(UIDList & itemList)
{

		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
		if (!layoutSelectionSuite) 
		{	
			//CA("!layoutSelectionSuite");
			return;
		}
        iSelectionManager->DeselectAll(nil);

        layoutSelectionSuite->SelectPageItems(itemList, Selection::kReplace,  Selection::kAlwaysCenterInView);
		
}



void SPSelectionObserver :: fillSubSectionList(double sectionid)
{
	do
	{
		//CA("INSIDE SPSelectionObserver :: fillSubSectionList");
		Mediator md;
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		return;

		InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
		if(iPanelControlData==nil)
		{
			break;
		}

		IControlView* subSectionDropListCntrlView = iPanelControlData->FindWidget(kSPSubSectionDropDownWidgetID);
		if(subSectionDropListCntrlView==nil)
		{
			break;
		}
						
		InterfacePtr<IDropDownListController> subSectionDropListCntrler(subSectionDropListCntrlView, UseDefaultIID());
		if(subSectionDropListCntrler==nil)
		{
			break;
		}
				
		InterfacePtr<IStringListControlData> subSectionDropListCntrlData(subSectionDropListCntrler, UseDefaultIID());
		if(subSectionDropListCntrlData==nil)
		{
			break;
		}

		subSectionDropListCntrlData->Clear();		
		subSectionDropListCntrlData->AddString("-Select-");

		

		VectorPubModelPtr vec_pubmodel = ptrIAppFramework->/*getAllSubsectionsBySectionIdAndLanguageId*/ProjectCache_getAllChildren(sectionid , global_lang_id);
		SectionData publdata;
		publdata.clearSubsectionVector();

		if(vec_pubmodel->size() > 0)
		{
			subSectionDropListCntrlView->Enable();	
			int32/*int64*/ size = static_cast<int32>(vec_pubmodel->size());
			global_vector_pubmodel_for_subsection.clear();
			for(int32 k = 0 ; k < size ; k++)
			{
				CPubModel model = (*vec_pubmodel)[k];
				double sectid=model.getEventId();
				PMString pubname=model.getName();
				int32 lvl= 0  ; //it->getLevel_no();
				double rootid = model.getRootID();
				double Typeid = model.getTypeID();
				PMString PubComment = ""; //model.getComments();

				publdata.setSubsectionVectorInfo(sectid,pubname,lvl, rootid, Typeid, PubComment);
				subSectionDropListCntrlData->AddString(pubname);
				global_vector_pubmodel_for_subsection.push_back(model);
			}
			
		}

		if(md.getCurrentSubSectionRow()>0)
		{	
			subSectionDropListCntrler->Select(md.getCurrentSubSectionRow());
		}
		else
		{  
			subSectionDropListCntrler->Select(0);	
		}
			
		//---------------------- added on 23 feb 2006 7-07 pm ----------------------------------//
		if(vec_pubmodel->size() <= 0)
		{
			subSectionDropListCntrler->Select(-1);
			subSectionDropListCntrlView->Disable();		
		}
		//--------------------------------------------------------------------------------------//
				
		SDKListBoxHelper listHelperInsidePopulateProductListForSection(this, kSPPluginID);
		listHelperInsidePopulateProductListForSection.EmptyCurrentListBox(iPanelControlData, 1);


	}
	while(kFalse);
}

double SPSelectionObserver :: getSelectedSubSectionID()
{
	double subsectionID = -1 ;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return 0;
	}

	do
	{
			InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
			if(iPanelControlData==nil)
			{
				break;
			}
			IControlView* subSectionDropListCntrlView = iPanelControlData->FindWidget(kSPSubSectionDropDownWidgetID);
			if(subSectionDropListCntrlView==nil)
			{
				break;
			}
						
			InterfacePtr<IDropDownListController> subSectionDropListCntrler(subSectionDropListCntrlView, UseDefaultIID());
			if(subSectionDropListCntrler==nil)
			{
				break;
			}			
			
			InterfacePtr<IStringListControlData> subSectionDropListCntrlData(subSectionDropListCntrler, UseDefaultIID());
			if(subSectionDropListCntrlData==nil)
			{
				break;
			}

			int32 selectedRow = subSectionDropListCntrler->GetSelected();
			if(selectedRow <= 0)
				break ;
			
			CPubModel model = global_vector_pubmodel_for_subsection[selectedRow - 1];
			subsectionID = model.getEventId();

	}
	while(kFalse);	
	return subsectionID ;
}



//following function is added by vijay choudhari on 8-4-2006///////////////

void SPSelectionObserver :: populateProductImageListBox()
{
	//InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	//if(ptrIClientOptions == nil)
	//{
	//	CAlert::ErrorAlert("Interface for IClientOptions not found.");
	//	return ;
	//}
	//
	//PMString imagePath = ptrIClientOptions->getImageDownloadPath();
	//if(imagePath != "")
	//{
	//	char *imageP=imagePath.GetPlatformString().c_str();
	//	if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
	//		#ifdef MACINTOSH
	//			imagePath+="/";
	//		#else
	//			imagePath+="\\";
	//		#endif
	//}

	//if(imagePath == "")
	//	return ;
	//
	//PMString imagePathWithSubdir = imagePath;

	//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//if(ptrIAppFramework == nil)
	//	return ;

	//VectorTypeInfoPtr typeValObj1 =
	//ptrIAppFramework->ElementCache_getImageAttributesByIndex(3);
	//if(typeValObj1==nil)
	//	return ;
	//	
	//VectorTypeInfoValue::iterator it2;
	//
	//for(it2=typeValObj1->begin();it2!=typeValObj1->end();it2++)
	//{				
	//	int32 Imagetypeid =it2->getType_id() ;

	//	PMString typIDString("");
	//	typIDString.AppendNumber(Imagetypeid);
	//	
	//	imageTypeID.push_back(Imagetypeid);
	//
	//	PMString ImageTypeName = it2->getName();
	//	int imageTextIndex = 0;
	//	
	//	PMString fileName("");
	//	int32 assetID;
	//	CObjectValue oVal;
	//	int32 objectId = pNodeDataList[CurrentSelectedProductRow].getPubId();
	//		
	//	oVal=ptrIAppFramework->GETProduct_getObjectElementValue(objectId);  // new added in appFramework

	//	int32 parentTypeID= oVal.getObject_type_id();

	//	do
	//	{			
	//		VectorAssetValuePtr AssetValuePtrObj = ptrIAppFramework->GETAssets_GetAssetByParentAndType(objectId,parentTypeID,Imagetypeid);
	//		if(AssetValuePtrObj == NULL)
	//			continue ;

	//		if(AssetValuePtrObj->size() ==0)
	//			continue ;

	//		VectorAssetValue::iterator it; // iterator of Asset value

	//		CAssetValue objCAssetvalue;
	//		for(it = AssetValuePtrObj->begin();it!=AssetValuePtrObj->end();it++)
	//		{
	//			objCAssetvalue = *it;
	//			fileName = objCAssetvalue.getFile_name();
	//			assetID = objCAssetvalue.getAsset_id();
	//		}

	//		if(fileName=="")
	//		continue ;
	//		
	//		Mediator::imageType = fileName;

	//		do
	//		{
	//			SDKUtilities::Replace(fileName,"%20"," ");
	//		}
	//		while(fileName.IndexOfString("%20") != -1);

	//		PMString typeCodeString;

	//		typeCodeString=ptrIAppFramework->ClientActionGetAssets_getProductAssetFolder(assetID);
	//		
	//		if(typeCodeString.NumUTF16TextChars()!=0)
	//			imagePathWithSubdir.Append(typeCodeString);	

	//		FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir));
	//		SDKUtilities ::AppendPathSeparator(imagePathWithSubdir);

	//		if(!fileExists(imagePathWithSubdir,fileName))
	//		{
	//			if(!ptrIAppFramework->GETAsset_downLoadProductAsset(assetID,imagePathWithSubdir))
	//			{					
	//				continue;
	//			}
	//		}

	//		if(!fileExists(imagePathWithSubdir,fileName))	
	//		{
	//			continue ; 
	//		}

	//		PMString ImagePath=imagePathWithSubdir+fileName;

	//		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	//		if (panelControlData == nil)
	//		{
	//			CA("panelControlData invalid");
	//			continue;
	//		}
	//		Mediator::panelControlData1 = panelControlData; 

	//		SDKListBoxHelper listHelper(this, kSPPluginID);
	//		IControlView* lstboxControlView=panelControlData->FindWidget(kImageListboxWidgetID);

	//		imageVector1.push_back(ImagePath);

	//		imageType.push_back(ImageTypeName);
	//		
	//		listHelper.AddElementImage(lstboxControlView,ImageTypeName,ImagePath, kPRLCustomPanelViewWidgetID, 0,0,1);
	//	}
	//	while(0);
	//}
}


bool16 SPSelectionObserver ::fileExists(PMString& path, PMString& name)
{
	PMString theName("");
	theName.Append(path);
	theName.Append(name);

	const char *file=(theName.GetPlatformString().c_str());

	FILE *fp=NULL;

	fp=std::fopen(file, "r");
	if(fp!=NULL)
	{
		std::fclose(fp);
		return kTrue;
	}
	return kFalse;
}


// showing Image Panel

void SPSelectionObserver::showImagePanel()
{

}

// hide Image Panel

void SPSelectionObserver::hideImagePanel()
{

}

//This Function is for Master(ONEsource) 
void SPSelectionObserver::levelFour()
{		
	//CA(__FUNCTION__);
	Mediator md;	
	IPanelControlData* panelControlData =nil;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return;
	}

	panelControlData=md.getMainPanelCtrlData();
	if(panelControlData == nil)
	{
		//CA("nukul");	
		return;
	}

	//This ControlView is set in the mediator class.
	IControlView * iView=panelControlData->FindWidget(kSPPubNameWidgetID);
	if(!iView)
	{
		//CA("No iView");
		return;
	}
	md.setPubNameTextView(iView);
	IControlView* SectionTextView = panelControlData->FindWidget( kSectionTextWidgetID);
	if(SectionTextView == NULL)
	{
		//CA("No SectionTextView");
		return;
	}
	SectionTextView->HideView();//tO hide the SectionTextWidget 
	IControlView* SPSectionDropDownView = panelControlData->FindWidget( kSPSectionDropDownWidgetID);
	if(SPSectionDropDownView == NULL)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::levelFour::No SPSectionDropDownView");	
		return;
	}
	SPSectionDropDownView->HideView();//To Hide SectionDropDownWidget
	IControlView* SubSectionTextView = panelControlData->FindWidget( kSubSectionTextWidgetID);
	if(SubSectionTextView == NULL)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::levelFour::No SubSectionTextView");	
		return;
	}
	SubSectionTextView->HideView();//To Hide SubSectionTextViewWidget
	IControlView* SubSectionDropDownView = panelControlData->FindWidget( kSPSubSectionDropDownWidgetID);
	if(SubSectionDropDownView == NULL)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::levelFour::No SubSectionDropDownView");	
		return;
	}
	SubSectionDropDownView->HideView();//To Hide SubSectionDropDownWidget
	IControlView* SectionText_1View = panelControlData->FindWidget( kSectionText_1WidgetID);
	if(SectionText_1View == NULL)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::levelFour::No SectionText_1View");	
		return;
	}
	SectionText_1View->HideView();//To Hide SectionText_1ViewWidget
	IControlView* SPSectionDropDown_1View = panelControlData->FindWidget( kSPSectionDropDown_1WidgetID);
	if(SPSectionDropDown_1View == NULL)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::levelFour::No SPSectionDropDown_1View");	
		return;
	}
	SPSectionDropDown_1View->HideView();//To Hide SectionDropDownWidget
	IControlView* SPOneView = panelControlData->FindWidget( kSPOnePnlWidgetID);
	if(SPOneView == NULL)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::levelFour::No SPOneView");	
		return;
	}
	SPOneView->ShowView();
	SPOneView->Enable();
	IControlView* SPMultilineView = panelControlData->FindWidget( kSPMultilineTextWidgetID);
	if(SPMultilineView == NULL)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::levelFour::No SPMultilineView");	
		return;
	}
	SPMultilineView->ShowView();
	SPMultilineView->Enable();
	InterfacePtr<ITextControlData> iData(SPMultilineView, UseDefaultIID());
	if(iData==nil)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::levelFour::No iData");		
		return;	
	}
	if(RefreshFlag !=kTrue)//If it is not Through Refresh then change the multilineText widget string
		iData->SetString("Select Category");
	IControlView* SPSelectClassIconView = panelControlData->FindWidget( kSPSelectClassWidgetID);
	if(SPSelectClassIconView == NULL)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::levelFour::No SPSelectClassIconView");		
		return;
	}
	SPSelectClassIconView->ShowView();
	SPSelectClassIconView->Enable();
	//CA("Calling populateProductListforSection to clear listbox");
	this->populateProductListforSection(-1);//To clear the listBox initially when ONEsource is selected.

	return;
}

//This Function is for when swithing between Master(ONEsource) and rest of all publication
void SPSelectionObserver::CallLevelTwoThree()
{
	//CA("Insdie CallLevelTwoThree");
	Mediator md;
	do
	{
		IPanelControlData* iPanelControlData;
		iPanelControlData=md.getMainPanelCtrlData();
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CA(" ptrIAppFramework nil ");
			break;
		}

		int32 level = 2;//ptrIAppFramework->getPM_Project_Levels();
		global_project_level = level ;
		if(level == 3)
		{
			Mediator::sectionID = CurrentSelectedSubSection;
			IControlView *sectionTextView = iPanelControlData->FindWidget(kSectionTextWidgetID);
			if(sectionTextView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::CallLevelTwoThree::No sectionTextView");						
				break;
			}
			IControlView *subSectionTextView = iPanelControlData->FindWidget(kSubSectionTextWidgetID);
			if(subSectionTextView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::CallLevelTwoThree::No subSectionTextView");				
				break;
			}
			IControlView *sectionText1View = iPanelControlData->FindWidget(kSectionText_1WidgetID);
			if(sectionText1View == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::CallLevelTwoThree::No sectionText1View");				
				break;
			}
			sectionTextView->ShowView();
			subSectionTextView->ShowView();
			sectionText1View->HideView();
			//----------------- all drop downs 
			IControlView *sectionDrop = iPanelControlData->FindWidget(kSPSectionDropDownWidgetID);
			if(sectionDrop == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::CallLevelTwoThree::No sectionDrop");				
				break;
			}
			IControlView *subsectionDrop = iPanelControlData->FindWidget(kSPSubSectionDropDownWidgetID);
			if(subsectionDrop == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::CallLevelTwoThree::No subsectionDrop");				
				break;
			}
			IControlView *sectionDrop1 = iPanelControlData->FindWidget(kSPSectionDropDown_1WidgetID);
			if(sectionDrop1 == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::CallLevelTwoThree::No sectionDrop1");				
				break;
			}
			sectionDrop->ShowView();
			subsectionDrop->ShowView();
			sectionDrop1->HideView();
		}

		if(level == 2)
		{
			Mediator::sectionID = CurrentSelectedSection;
			PMString secStr("");
			secStr.AppendNumber(PMReal(CurrentSelectedSection));
			IControlView *sectionTextView = iPanelControlData->FindWidget(kSectionTextWidgetID);
			if(sectionTextView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::CallLevelTwoThree::No sectionTextView");				
				break;
			}
			IControlView *subSectionTextView = iPanelControlData->FindWidget(kSubSectionTextWidgetID);
			if(subSectionTextView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::CallLevelTwoThree::No subSectionTextView");				
				break;
			}
			IControlView *sectionText1View = iPanelControlData->FindWidget(kSectionText_1WidgetID);
			if(sectionText1View == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::CallLevelTwoThree::No sectionText1View");				
				break;
			}
			sectionTextView->HideView();
			subSectionTextView->HideView();
			sectionText1View->ShowView();
			sectionText1View->Enable();
			//----------------- all drop downs 
			IControlView *sectionDrop = iPanelControlData->FindWidget(kSPSectionDropDownWidgetID);
			if(sectionDrop == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::CallLevelTwoThree::No sectionDrop");				
				break;
			}

			IControlView *subsectionDrop = iPanelControlData->FindWidget(kSPSubSectionDropDownWidgetID);
			if(subsectionDrop == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::CallLevelTwoThree::No subsectionDrop");				
				break;
			}
			IControlView *sectionDrop1 = iPanelControlData->FindWidget(kSPSectionDropDown_1WidgetID);
			if(sectionDrop1 == nil)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::CallLevelTwoThree::No sectionDrop1");				
				break;
			}
			sectionDrop->HideView();
			subsectionDrop->HideView();
			sectionDrop1->ShowView();
			sectionDrop1->Enable();

		}

		IControlView* SPOneView = iPanelControlData->FindWidget( kSPOnePnlWidgetID);
		if(SPOneView == NULL)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::CallLevelTwoThree::No SPOneView");
			break;
		}
		SPOneView->HideView();			
		IControlView* SPMultilineView = iPanelControlData->FindWidget( kSPMultilineTextWidgetID);
		if(SPMultilineView == NULL)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::CallLevelTwoThree::No SPMultilineView");
			break;
		}
		SPMultilineView->HideView();		
		IControlView* SPSelectClassIconView = iPanelControlData->FindWidget( kSPSelectClassWidgetID);
		if(SPSelectClassIconView == NULL)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::CallLevelTwoThree::No SPSelectClassIconView");
			break;
		}
		SPSelectClassIconView->HideView();
			
	}while(0);
}




bool8 SPSelectionObserver::populateProductListforSectionWithDesignerActions(double curSelSubecId, bool8 new_product,bool8 add_to_spread,bool8 update_spread,bool8 update_copy,bool8 update_art,bool8 update_item_tables,bool8 delete_from_spread, bool8 starred_product)
{
	//CA("populateProductListforSectionWithDesignerActions");
	vectorForThumbNail.clear();

	AcquireWaitCursor awc;
	awc.Animate(); 

	//CA(__FUNCTION__);
	bool8 isListBoxPopulated = kFalse;


	/*InterfacePtr<ISPPRImageHelper> ptrImageHelper((static_cast<ISPPRImageHelper*> (CreateObject(kSPProductImageIFaceBoss ,ISPPRImageHelper::kDefaultIID))));
	if(ptrImageHelper == nil)
	{
		CA("ProductImages plugin not found ");
		return kFalse;
	}*/

	Mediator md;
	IPanelControlData* iPanelControlData=md.getMainPanelCtrlData();
	if(iPanelControlData == nil)
	{
		CA("panelCntrlData is nil");
		return kFalse;
	}
	IControlView* listBox = iPanelControlData->FindWidget(kSPListBoxWrapperGroupPanelWidgetID);				
	//IControlView* thumb = iPanelControlData->FindWidget(kSPThumbPnlWidgetID);
	if(isThumb)
	{
		listBox ->HideView ();		
		//thumb ->ShowView ();
	}
	else
	{
		//thumb->HideView ();
		listBox->ShowView();		
	}

	do
	{
		PublicationNode pNode;

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			CAlert::InformationAlert("Err ptrIAppFramework is nil");
			//CA("nil");
			break;
		}
		//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));

		SDKListBoxHelper listHelperInsidePopulateProductListForSection(this, kSPPluginID);
//CA("1");
		IControlView* lstboxControlView=
		listHelperInsidePopulateProductListForSection.FindCurrentListBox(iPanelControlData, 1);
		if(lstboxControlView==nil)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::populateProductListforSection::No lstboxControlView");
			break;
		}
		/*listHelperInsidePopulateProductListForSection.EmptyCurrentListBox(iPanelControlData, 1);*/

		md.setSelectedPub(curSelSubecId);
		md.setCurrSubSectionID(curSelSubecId);

		bool16 isONEsource = ptrIAppFramework->get_isONEsourceMode();
		//ItemMapPtr pItemMap = NULL; //******
		if(!isONEsource)
		{
			listHelperInsidePopulateProductListForSection.EmptyCurrentListBox(iPanelControlData, 1);
			//ptrImageHelper->EmptyImageGrid();
			//imageVector2.clear();
			//CA("Publication Mode");
			//int32 ParentTypeID = ptrIAppFramework->TYPEMngr_getObjectTypeID("PRODUCT_LEVEL");			
			VectorPubObjectValuePtr VectorFamilyInfoValuePtr = NULL;			
			//VectorFamilyInfoValuePtr = ptrIAppFramework->PBObjMngr_getProductsForSubSection(curSelSubecId, ParentTypeID);

			if(ListFlag == 0)
			{
				VectorFamilyInfoValuePtr = ptrIAppFramework->getProductsAndItemsForSection(curSelSubecId, global_lang_id );
				//pItemMap = ptrIAppFramework->GetProjectProducts_getAllLeadingItemIds(curSelSubecId) ;

			}else if(ListFlag == 1)
			{
				VectorFamilyInfoValuePtr = ptrIAppFramework->getProductsForSubSection/*GetProjectProducts_getProductsBySubSection*/(curSelSubecId, global_lang_id );
				//pItemMap = ptrIAppFramework->GetProjectProducts_getAllLeadingItemIds(curSelSubecId) ;
			}else if(ListFlag ==2)
			{
				VectorFamilyInfoValuePtr = ptrIAppFramework->getItemsForSubSection/*GetProjectProducts_getItemsForSubSection*/(curSelSubecId, global_lang_id );
				//pItemMap = ptrIAppFramework->GetProjectProducts_getAllLeadingItemIds(curSelSubecId);
			}
			/*else if(ListFlag ==3)
			{
				VectorFamilyInfoValuePtr = ptrIAppFramework->GetProjectProducts_getHybridTablesForSubSection(curSelSubecId, global_lang_id );
			}*/
					
			if(VectorFamilyInfoValuePtr == nil)
			{
				//CA(" VectorFamilyInfoValuePtr == NULL ");
				if(md.getSprayButtonView()){ //CA("Disabling Spray Button.");
					md.getSprayButtonView()->Disable();}
				if(md.getAssignBtnView())
					md.getAssignBtnView()->HideView();/*Disable();*/
				/*if(md.getLocateWidgetView())
						md.getLocateWidgetView()->Disable();*///--COMMENTED ON 25 FEB 2006 --
				if(md.getPreviewWidgetView())
					md.getPreviewWidgetView()->Disable();
				if(md.getSubSecSprayButtonView())
					md.getSubSecSprayButtonView()->Disable();
				if(md.getRefreshButtonView())
					md.getRefreshButtonView()->Disable();
				if(md.getIconView())
					md.getIconView()->Disable();
				if(md.getIconViewNew())
					md.getIconViewNew()->Disable();

				//--------
				if(md.getGreenFilterBtnView())
					md.getGreenFilterBtnView()->Disable();

				if(md.getWhiteBoardBtnView())
				{
					md.getWhiteBoardBtnView()->Disable();
					/*InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
					if(ptrImageHelper != nil)
					{
						ptrImageHelper->closeImagePanel();
					}*/
				}
				if(md.getTableSourceBtnView())
				{
					md.getTableSourceBtnView()->Disable();
				}

				break;
			}

			if(VectorFamilyInfoValuePtr->size()==0)
			{
				//CA("VectorFamilyInfoValuePtr->size()==0");
				if(md.getSprayButtonView()){ //CA("Disabling Spray Button.");
					md.getSprayButtonView()->Disable();}
				if(md.getAssignBtnView())
					md.getAssignBtnView()->HideView();/*Disable();*/
				/*if(md.getLocateWidgetView())//--COMMENTED ON 25 FEB 2006 --
						md.getLocateWidgetView()->Disable();*/
				if(md.getPreviewWidgetView())
						md.getPreviewWidgetView()->Disable();
				if(md.getSubSecSprayButtonView())
					md.getSubSecSprayButtonView()->Disable();
				if(md.getRefreshButtonView())
					md.getRefreshButtonView()->Disable();
				if(md.getIconView())
					md.getIconView()->Disable();
				if(md.getIconViewNew())
					md.getIconViewNew()->Disable();

				//--------
				if(md.getGreenFilterBtnView())
					md.getGreenFilterBtnView()->Disable();
				if(md.getWhiteBoardBtnView())
				{
					md.getWhiteBoardBtnView()->Disable();
					/*InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
					if(ptrImageHelper != nil)
					{
						ptrImageHelper->closeImagePanel();					
					}*/
				}
				if(md.getTableSourceBtnView())
				{
					md.getTableSourceBtnView()->Disable();
				}

				delete VectorFamilyInfoValuePtr;
				break;
			}
		
			int32/*int64*/ size =static_cast<int32> (VectorFamilyInfoValuePtr->size());

			VectorPubObjectValue::iterator it2;
			vector<CPbObjectValue> vectorForThumb1;
			int count=0;
			PMString itemCountString("");
			int32 itemCountNum =0;
			for(it2 = VectorFamilyInfoValuePtr->begin(); it2 != VectorFamilyInfoValuePtr->end(); it2++)
			{		
				pNode.setPBObjectID(it2->getPub_object_id());
				pNode.setSequence(it2->getIndex());	
				pNode.setIsProduct(it2->getisProduct());
				pNode.setIsONEsource(kFalse);
				pNode.setIsStarred(it2->getStarredFlag1());
				int32 NewProductFlag =0;
				if(it2->getNew_product()== kTrue){
					pNode.setNewProduct(1);
					NewProductFlag =2;
				}
				else
				{
					pNode.setNewProduct(0);
					NewProductFlag =1;
				}

				if(it2->getisProduct() == 2)
				{
					//PMString ASD("getLevel_no : ");
					//it2->getObjectValue().getName();				
					//ASD.AppendNumber(it2->getObjectValue().getLevel_no());
					//CA(it2->getObjectValue().getName());
				/*	PMString ASD("ParentId : ");
					ASD.AppendNumber(it2->getObjectValue().getParent_id());
					CA(ASD);*/	
					//pNode.setLevel(it2->getObjectValue().getLevel_no());
					pNode.setParentId(it2->getObjectValue().getParent_id());					
					pNode.setPubId(it2->getObjectValue().getObject_id());

					/*if(!iConverter)*/
                    PMString pName1 = it2->getObjectValue().getName();
						pNode.setPublicationName(pName1);
					/*else
						pNode.setPublicationName(iConverter->translateString(it2->getObjectValue().getName()));*/
					
					pNode.setChildCount(it2->getObjectValue().getChildCount());
					//pNode.setReferenceId(it2->getObjectValue().getRef_id());
					pNode.setTypeId(it2->getObjectValue().getObject_type_id());									
				}

				if(it2->getisProduct() == 1)
				{
					//PMString ASD("getLevel_no : ");
					//it2->getObjectValue().getName();				
					//ASD.AppendNumber(it2->getObjectValue().getLevel_no());
					//CA(it2->getObjectValue().getName());
				/*	PMString ASD("ParentId : ");
					ASD.AppendNumber(it2->getObjectValue().getParent_id());
					CA(ASD);*/	
					//pNode.setLevel(it2->getObjectValue().getLevel_no());
					pNode.setParentId(it2->getObjectValue().getParent_id());					
					pNode.setPubId(it2->getObjectValue().getObject_id());

					/*if(!iConverter)*/
                    PMString pName1 = it2->getName();
						pNode.setPublicationName(pName1);
					//else
					//	pNode.setPublicationName(iConverter->translateString(it2->/*getObjectValue().*/getName()));
					
					pNode.setChildCount(it2->getObjectValue().getChildCount());
					//pNode.setReferenceId(it2->getObjectValue().getRef_id());
					pNode.setTypeId(it2->getObjectValue().getObject_type_id());									
				}
				if(it2->getisProduct() == 0)
				{
					pNode.setPubId(it2->getItemModel().getItemID());

					/*if(!iConverter)*/
					{
						PMString ItemNO(it2->getItemModel().getItemNo());
						PMString ItemDesp(it2->getItemModel().getItemDesc());
						if(ItemDesp != "")
						{
							ItemNO.Append(": "+ ItemDesp);
						}
						pNode.setPublicationName(ItemNO);
						//CA(ItemNO);
					}
					//else
					//{
					//	PMString ItemNO(iConverter->translateString(it2->getItemModel().getItemNo()));
					//	PMString ItemDesp(iConverter->translateString(it2->getItemModel().getItemDesc()));
					//	if(ItemDesp != "")
					//	{
					//		ItemNO.Append(": "+ ItemDesp);
					//	}
					//	pNode.setPublicationName(ItemNO);	
					//	//CA(ItemNO);
					//}
					pNode.setTypeId(it2->getobject_type_id());

				}
//CA("1");

				

				bool16 AddToListFlag = kFalse;

				if(new_product == kTrue )
				{
					if(it2->getNew_product() == kTrue)
						AddToListFlag = kTrue;
				}
				if(add_to_spread == kTrue )
				{
					if(it2->getAdd_to_spread() == kTrue)
						AddToListFlag = kTrue;
				}
				if(update_spread == kTrue )
				{
					if(it2->getUpdate_spread() == kTrue)
						AddToListFlag = kTrue;
				}
				if(update_copy == kTrue )
				{
					if(it2->getUpdate_copy() == kTrue)
						AddToListFlag = kTrue;
				}
				if(update_art == kTrue )
				{
					if(it2->getUpdate_art() == kTrue)
						AddToListFlag = kTrue;
				}
				if(update_item_tables == kTrue )
				{
					if(it2->getUpdate_item_table() == kTrue)
						AddToListFlag = kTrue;
				}
				if(delete_from_spread == kTrue )
				{
					if(it2->getDelete_from_spread() == kTrue)
						AddToListFlag = kTrue;
				}					
				if(starred_product == kTrue )
				{
					if(it2->getStarredFlag1() == kTrue)
						AddToListFlag = kTrue;
				}

				if(AddToListFlag == kFalse) // this is when Product / item does not satisfies the Designer action filter criteria.
					continue;

				if(AddToListFlag == kTrue)
				{
					CPbObjectValue obj = (*it2);
					//ptrForThumbnail->push_back(obj);
					vectorForThumbNail.push_back(obj);
				}
				
				int icon_count = GetIconCountForUnfilteredProductList(it2);
				pNodeDataList.push_back(pNode);
				
				int32 StarFlag =1;
				if(it2->getStarredFlag1()) // if Selected Green Star
					StarFlag =2;
				
				//*************
				int32 isProduct= it2->getisProduct();
				//if( pItemMap != NULL && pItemMap->size() > 0)
				//{
				//	ItemMap::iterator itr;
				//	int32 searchItemId = pNode.getPubId();
				//	itr = pItemMap->find(searchItemId);
				//	if(itr != pItemMap->end())
				//	{
				//		if(ListFlag == 2 && itr->second.itemType == 0)
				//		{
				//			//vectorForThumbNail.pop_back();
				//			continue;
				//		}
				//		else
				//		{
				//			//CA("Else");
				//			if(itr->second.itemType == 0)
				//			{
				//				isProduct = 3;
				//				
				//				itemCountNum = itr->second.count ;  //***
				//				//CA("itemType == 0");
				//			}
				//			else if(itr->second.itemType == 1)
				//			{
				//				isProduct = 4;
				//				
				//				
				//			}
				//			else if(itr->second.itemType == 2)
				//			{
				//				isProduct = 1;
				//				
				//				itemCountNum = itr->second.count ;  //***
				//				//CA("//itemType == 2");
				//			}							
				//		}
				//	}
				//}
				//***************

				if(!isThumb)
				{
					//itemCountNum = it2->getChildCount();

					if(itemCountNum  >1)
					{
						itemCountString.Append("(");
						itemCountString.AppendNumber(itemCountNum);
						itemCountString.Append(")");
					}
					else
						itemCountString.Append("");
					//CA("Line 8646: "+itemCountString);
                    PMString dummyString =  pNode.getName();
					listHelperInsidePopulateProductListForSection.AddElement(lstboxControlView , dummyString, kSPTextWidgetID, count );				
					listHelperInsidePopulateProductListForSection.SetDesignerAction(lstboxControlView, count, isProduct/*it2->getisProduct()*/ ,icon_count, StarFlag,NewProductFlag,itemCountString);//**** $$$
					count++;				
					itemCountString.Clear();
					itemCountNum=0;

				}
				else
				{
					//CA("isThumb == kTrue");
					showImageAsThumbnail(pNode, curSelSubecId);
				}
			}	
		}

		InterfacePtr<IListControlData>listcontrol(lstboxControlView,UseDefaultIID());
		if(!listcontrol)
		{
			//CA("No list control");
			break;
		}
		if(listcontrol->Length()>0 /*|| imageVector2.size()>0*/)
		{

			isListBoxPopulated = kTrue;
			/*
				added on 15 feb 2006.....
			*/
			InterfacePtr<IListBoxController> listStringControlData(lstboxControlView,UseDefaultIID());
			if(listStringControlData)
			{
				//listStringControlData->SetClickItem(0);
				listStringControlData->Select(0);
				CurrentSelectedProductRow = 0; // First Product of List

				VectorPubObjectValue::iterator it2;

				it2 = vectorForThumbNail.begin();
				int32 iconCount = GetIconCountForUnfilteredProductList (it2);
				PMString str("");

				int32 isProduct = 0;

				//Hybrid Table Change
				if(it2->getisProduct() == 2)
				{
					isProduct = 2;
					/*if(!iConverter)*/
						str = it2->getObjectValue().getName();
					/*else
						str = iConverter->translateString(it2->getObjectValue().getName());*/
	
				}
				//Hybrid Table Change up to here
				if(it2->getisProduct() == 1)
				{
					isProduct = 1;
					/*if(!iConverter)*/
						str = it2->/*getObjectValue().*/getName();
					//else
					//	str = iConverter->translateString(it2->/*getObjectValue().*/getName());
	
				}
				if(it2->getisProduct() == 0)
				{
					isProduct = 0;
					/*if(!iConverter)*/
					{
						PMString ItemNO(it2->getItemModel().getItemNo());
						PMString ItemDesp(it2->getItemModel().getItemDesc());
						if(ItemDesp != "")
						{
							ItemNO.Append(": "+ ItemDesp);
						}
						str = ItemNO;
					}
					/*else
					{
						PMString ItemNO(iConverter->translateString(it2->getItemModel().getItemNo()));
						PMString ItemDesp(iConverter->translateString(it2->getItemModel().getItemDesc()));
						if(ItemDesp != "")
						{
							ItemNO.Append(": "+ ItemDesp);
						}
						str = ItemNO;
					}*/
				}
				int32 NewProductFlag =0;
				if(ptrIAppFramework->get_isONEsourceMode()){
					NewProductFlag = 0;
				}
				else if(it2->getNew_product()  == kTrue){
					NewProductFlag =2;
				}
				else{
					NewProductFlag =1;
				}
                
				int32 StarFlag =0;
				if(ptrIAppFramework->get_isONEsourceMode()){
					StarFlag = 0;
				}
				else if(it2->getStarredFlag1()== kTrue) // if Selected Green Star
					StarFlag =2;
				else
					StarFlag = 1;

				//SPXLibraryItemGridEH spXLibraryItemGridEHObj(this);
				//spXLibraryItemGridEHObj.SetDesignerActionThumbnail (iPanelControlData, str, isProduct, iconCount, StarFlag, NewProductFlag);

				//IControlView* gridWidget = iPanelControlData->FindWidget( kSPXLibraryItemGridWidgetId );
				//InterfacePtr<ISPXLibraryViewController> gridController( gridWidget,IID_ISPXLIBRARYVIEWCONTROLLER );
	
				//if ( gridController == NULL )
				//{
				//	//TRACEFLOW("toms"," \001\004NO IXLibraryViewController\001\004\n");
				//	return kFalse;
				//}
				//gridController->Select(0);

				IControlView *view1 = iPanelControlData->FindWidget(kSPRefreshWidgetID);
				if(view1)
				{
					view1->Enable();
				}

				IControlView *view2 = iPanelControlData->FindWidget(kSPSubSectionSprayButtonWidgetID);
				if(view2)
				{
					view2->Enable();
				}

				IControlView *view4 = iPanelControlData->FindWidget(kPRLPreviewButtonWidgetID);
				if(view4)
				{   
					view4->Enable();
				}

				IControlView *view6 = iPanelControlData->FindWidget(kSPSprayButtonWidgetID);
				if(view6)
				{	
					view6->Enable();
				}

				if(md.getGreenFilterBtnView())
					md.getGreenFilterBtnView()->ShowView();

				IControlView *view7 = iPanelControlData->FindWidget(kSPShowWhiteBoardWidgetID);
				if(view7)
				{
					view7->Enable();
				}

				IControlView *view8 = iPanelControlData->FindWidget(kSPShowTableSourceWidgetID);
				if(view8)
				{   
					view8->Enable();
				}


				md.setCurrentObjectID(pNodeDataList[CurrentSelectedProductRow].getPubId());
				md.setPBObjectID(pNodeDataList[CurrentSelectedProductRow].getPBObjectID());
				/*PMString objid("The selected object id is ");
				objid.AppendNumber(pNodeDataList[CurrentSelectedProductRow].getPubId());*/
				//CA(objid);
				
				/*if(searchResult)
					DataSprayerPtr->FillPnodeStruct(pNodeDataList[CurrentSelectedProductRow],pNodeDataList[CurrentSelectedProductRow].getSectionID(), pNodeDataList[CurrentSelectedProductRow].getPublicationID(), pNodeDataList[CurrentSelectedProductRow].getSubSectionID());
				else
					DataSprayerPtr->FillPnodeStruct(pNodeDataList[CurrentSelectedProductRow],CurrentSelectedSection, CurrentSelectedPublicationID, CurrentSelectedSubSection);

				DataSprayerPtr->setFlow(isItemHorizontalFlow);*/
			
			}
		}
		else
		{
			//CA("isListBoxPopulated =  kFalse");
			isListBoxPopulated =  kFalse;
			if(md.getSprayButtonView()){ //CA("Disabling Spray Button.");
				md.getSprayButtonView()->Disable();}
			
			if(md.getAssignBtnView())
					md.getAssignBtnView()->HideView();
			if(md.getGreenFilterBtnView())
				md.getGreenFilterBtnView()->ShowView();

			if(md.getPreviewWidgetView())
					md.getPreviewWidgetView()->Disable();
			if(md.getSubSecSprayButtonView())
				md.getSubSecSprayButtonView()->Disable();
			if(md.getRefreshButtonView())			
				md.getRefreshButtonView()->Enable();
			
			//--------
			if(md.getWhiteBoardBtnView())
			{
				md.getWhiteBoardBtnView()->Disable();
				/*InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
				if(ptrImageHelper != nil)
				{
					ptrImageHelper->closeImagePanel();					
				}*/
			}
			if(md.getTableSourceBtnView())
			{
				md.getTableSourceBtnView()->Disable();
			}
			//md.getRefreshButtonView()->Disable();
		}

	}while(kFalse);

	//CA("returning populateProductListforSection()");
	return isListBoxPopulated;
}

//end 10.3.05
//  Code generated by DollyXS code generator


////////////////////////// For Thumbnail View ////////////////////
bool8 SPSelectionObserver::showImageAsThumbnail(PublicationNode& pNode, double& curSelSubecId)
{//CA(__FUNCTION__);

	//using std::vector; 
	//vector<double>::size_type n = 0; 


	bool8 result = kFalse;
	//int32 objID = pNode .getPubId ();
	//int32 parentID = pNode .getParentId ();
	//int32 parentTypeID = pNode .getTypeId ();
	//PMString name = pNode.getName();


	//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//if(ptrIAppFramework == nil)
	//{
	//	CAlert::InformationAlert("Err ptrIAppFramework is nil");
	//	//CA("nil");
	//	return result;
	//}

	////ptrIAppFramework->clearAllStaticObjects();

	//InterfacePtr<ISPPRImageHelper> ptrImageHelper((static_cast<ISPPRImageHelper*> (CreateObject(kSPProductImageIFaceBoss ,ISPPRImageHelper::kDefaultIID))));
	//if(ptrImageHelper == nil)
	//{
	//	//CA("ProductImages plugin not found ");
	//	return result;
	//}

	//if(pNode .getIsProduct () == 2)
	//{
	//	ptrImageHelper->showProductImages(objID, global_lang_id, parentID, parentTypeID, curSelSubecId, name);
	//	result = kTrue;
	//}
	//if(pNode .getIsProduct () == 1)
	//{
	//	ptrImageHelper->showProductImages(objID, global_lang_id, parentID, parentTypeID, curSelSubecId, name);
	//	result = kTrue;
	//}
	//if(pNode .getIsProduct () == 0)
	//{
	//	ptrImageHelper->showItemImages(objID, global_lang_id, parentID, parentTypeID, curSelSubecId, kFalse, name);
	//	result = kTrue;
	//}

	return result;
}
////////////////////////// Thumbnail View Ends ///////////////////
//This function creates a new layer on currenly available layers.  
bool16 SPSelectionObserver::createNewLayer()
{
	bool16 result = kFalse;
	do
	{
		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutData == nil)
		{
			//CA("layoutData == nil");
			break;
		}

		IDocument* document = layoutData->GetDocument();
		if (document == nil)
		{			
			//CA("document == nil");
			break;
		}

		// Create the NewLayerCmd:
		InterfacePtr<ICommand> layerCmd(CmdUtils::CreateCommand(kNewLayerCmdBoss));

		// Set NewLayerCmd arguments:
		InterfacePtr<INewLayerCmdData> layerData(layerCmd, IID_INEWLAYERCMDDATA);
		if (layerData == nil)
		{
			//CA("layerData is nil");
			break;
		}

		layerData->Set
		(
			GetUIDRef(document),	// const UIDRef& layerList
			nil,							// const PMString *name = nil
			kTrue,							// bool16 isVisible = kTrue, 
			kFalse,							// bool16 isLocked = kFalse, 
			kFalse,							// bool16 isGuideVisible = kTrue, 
			kFalse,							// bool16 isGuideLocked = kFalse, 
			kInvalidUID,					// UID layerColor = kInvalidUID, 
			kTrue,							// bool16 isUILayer = kTrue,
			kTrue							// bool16 isExpendable = kTrue;
		);	

		ErrorCode error = kFailure;
		// Process the command:
		error = CmdUtils::ProcessCommand(layerCmd);
		result = kTrue;
	}while(false);
	return result;
}

void SPSelectionObserver::createCommentsOnLayer()
{
//	int32 commentDataPtrVectorLength =static_cast<int32> (global_VectorAPpubCommentValue.size());
//
//	if(commentDataPtrVectorLength  > 0)
//	{
//		//CA("Inside if");
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(!ptrIAppFramework)
//		{
//			//CA("ptrIAppFramework is NULL");		
//			return;
//		}
//		
//		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//		if(!iConverter)
//		{
//			//CA("!iConverter");		
//			return;
//		}
//
//		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
//		if(ptrIClientOptions==NULL)
//		{
//			ptrIAppFramework->LogError("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer::!ptrIClientOptions");
//			return;
//		}
//
//		///////////////// Adding Table types here ////////////////
//		VectorTypeInfoPtr typeValObjForProductTable = ptrIAppFramework->ElementCache_getProductTableTypes();
//		if(typeValObjForProductTable==nil)
//		{
//			ptrIAppFramework->LogError("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer::ElementCache_getProductTableTypes's typeValObj is nil");
//			return;
//		}
//
//		VectorTypeInfoPtr typeValObjForItemTable = ptrIAppFramework->AttributeCache_getItemTableTypes();
//		if(typeValObjForItemTable==nil){
//			ptrIAppFramework->LogError("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer::AttributeCache_getItemTableTypes's typeValObj is nil");	
//			return;
//		}
//
//		////////////////Adding Image Types here//////////////
//
//		VectorTypeInfoPtr typeValObjForProductImage=
//				ptrIAppFramework->ElementCache_getImageAttributesByIndex(3);
//
//		//VectorTypeInfoPtr typeValObjForItemImage = ptrIAppFramework->AttributeCache_getItemImagesForClassAndParents(CurrentClassID);
//
//		////////////////////////////////////////////////////////////////////////////////////
//		
//		
//		IDocument* frontDocument = Utils<ILayoutUIUtils>()->GetFrontDocument(); 
//		if(frontDocument == nil)
//		{
//			//CA("frontDocument is NULL");		
//			return;
//		}
//
//		PMString imagePath=ptrIClientOptions->getImageDownloadPath();
//		if(imagePath!="")
//		{
//			const char *imageP=(imagePath.GetPlatformString().c_str()/*GetPlatformString().c_str()*/); //Cs4
//			if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
//				#ifdef MACINTOSH
//					imagePath+="/";
//				#else
//					imagePath+="\\";
//				#endif
//		}
//
//		if(imagePath=="")
//		{
//			ptrIAppFramework->LogError("SPSelectionObserver::createCommentsOnLayer::imagePath is blank");
//			return;
//		}
//
//		int32 maxSpreadNumber = 0;
//		for(int32 j = 0;j < commentDataPtrVectorLength ; j++)
//		{
//			APpubComment objAPpubComment = global_VectorAPpubCommentValue[j];
//			if(objAPpubComment.spreadNumber > maxSpreadNumber)
//				maxSpreadNumber = objAPpubComment.spreadNumber;
//		}
//
//		/*PMString maxSpreadCount("");
//		maxSpreadCount.AppendNumber(maxSpreadNumber);
//		CA("maxSpreadCount = " + maxSpreadCount);*/
//		
//		AddOrDeleteSpreads(maxSpreadNumber);
//
//		int32 i = 0;
//		for(;i < commentDataPtrVectorLength ; i++)
//		{
//			
//			APpubComment objAPpubComment = global_VectorAPpubCommentValue[i];
//			objIndex = i;
//
//			UIDRef frameUIDRef;
//
//			PMRect box(objAPpubComment.TOP_X_COORDINATE,objAPpubComment.TOP_Y_COORDINATE ,objAPpubComment.BOT_X_COORDINATE, objAPpubComment.BOT_Y_COORDINATE);
//
//			PMPathPointList pathPointList;
//			pathPointList.push_back(PMPathPoint(PMPoint(0.0, 0.0)));
//			pathPointList.push_back(PMPathPoint(PMPoint(100.0, 100.0)));
//
//			/*PMString temp("objAPpubComment.comment_type = ");
//			temp.AppendNumber(objAPpubComment.comment_type);
//			CA(temp);*/
//			CPbObjectValue* CPbObjectValueptr;
//
//			switch(objAPpubComment.comment_type)
//			{
//				case 1:
//					//CA("Case 1");
//					createRectangle(box,objAPpubComment.stage, frameUIDRef ,objAPpubComment.spreadNumber);
//					break;
//
//				case 2:
//					//CA("Case 2");
//					createRectangle(box,objAPpubComment.stage, frameUIDRef ,objAPpubComment.spreadNumber);
//					break;
//
//				case 3:
//					//CA("Case 3");
//					createRectangle(box,objAPpubComment.stage, frameUIDRef ,objAPpubComment.spreadNumber);
//					break;
//
//				case 4:
//					//CA("Case 4");
//					createOval(box,objAPpubComment.stage,frontDocument, frameUIDRef);
//					break;
//
//				case 5:
//					//CA("Case 6");
//					createArrowGraphic(box,objAPpubComment.stage,frontDocument, frameUIDRef,pathPointList);
//					break;
//
//				case 6:
//					//CA("Case 6");
//					createRectangle(box,objAPpubComment.stage, frameUIDRef ,objAPpubComment.spreadNumber);
//					break;
//
//				case 7:
//				//	//CA("case 7");
//					createRectangle(box,objAPpubComment.stage, frameUIDRef ,objAPpubComment.spreadNumber);
//					break; 
//
//				case 8:
//					//CA("case 8");
//					//objAPpubComment.assetId = 1220;
//					//objAPpubComment.comment.id = 30022350;
//					//CPbObjectValueptr = ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(objAPpubComment.SECTION_NO , objAPpubComment.comment.id);
//					//objAPpubComment.comment_type = 9;
//					createTableFrame(box,objAPpubComment.stage, frameUIDRef ,objAPpubComment.spreadNumber,objAPpubComment.comment.id, objAPpubComment.assetId, objAPpubComment);
//					break;
//
//				case 9:
//					//CA("case 9");
//					CPbObjectValueptr = ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(objAPpubComment.SECTION_NO , objAPpubComment.comment.id);
//					createTableFrame(box,objAPpubComment.stage, frameUIDRef ,objAPpubComment.spreadNumber,CPbObjectValueptr->getPub_object_id()/*objAPpubComment.comment.id*/, objAPpubComment.assetId, objAPpubComment);
//					break;
//
//				case 10:
//					//CA("Case 10");
//					createRectangle(box, objAPpubComment, frameUIDRef);
//					break;
//
//				case 11:
//					createRectangle(box, objAPpubComment, frameUIDRef);
//					break;
//			}
//	
//			if((objAPpubComment.comment_type == 1 ||objAPpubComment.comment_type == 2 ||objAPpubComment.comment_type == 3 ||objAPpubComment.comment_type == 4||objAPpubComment.comment_type == 5 ))
//			{
//				InterfacePtr<IPMUnknown> unknown(frameUIDRef, IID_IUNKNOWN);
//				if(!unknown)
//					break;
//
//				UID textFrameUID=Utils<IFrameUtils>()->GetTextFrameUID(unknown);
//				if(textFrameUID==kInvalidUID )
//				{
//					this->convertBoxToTextBox(frameUIDRef);
//					InterfacePtr<IPMUnknown> unknown(frameUIDRef, IID_IUNKNOWN);
//					if(!unknown)
//						break;
//					textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
//
//					if(objAPpubComment.comment_type == 5)
//					{
//						this->addTagToGraphicFrame(frameUIDRef);
//						objAPpubComment.isCommentPresentOnDocument = kTrue;
//						continue;
//					}
//				}
//
//				textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
//				if(textFrameUID == kInvalidUID)
//					break;
//
//				//---------------------CS3 TextFrame change---------------------------//
//				/*InterfacePtr<ITextFrame> textFrame(frameUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
//				if(!textFrame)
//					break;
//
//				ITextModel* txtModel = textFrame->QueryTextModel();
//				if (txtModel == nil)
//					break;
//				
//				InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
//				if (textModel == nil)
//				{
//					//CA("textModel == nil");
//					break;
//				}*/
//
//				InterfacePtr<IHierarchy> graphicFrameHierarchy(frameUIDRef, UseDefaultIID());
//				if (graphicFrameHierarchy == nil) 
//				{
//					//CA("graphicFrameHierarchy is NULL");
//					flag=0;
//					break;
//				}
//								
//				InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
//				if (!multiColumnItemHierarchy) {
//					//CA("multiColumnItemHierarchy is NULL");
//					flag=0;
//					break;
//				}
//
//				InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
//				if (!multiColumnItemTextFrame) {
//					//CA("multiColumnItemTextFrame is NULL");
//					flag=0;
//					break;
//				}
//				InterfacePtr<IHierarchy>
//				frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
//				if (!frameItemHierarchy) {
//					//CA("frameItemHierarchy is NULL");
//					flag=0;
//					break;
//				}
//
//				InterfacePtr<ITextFrameColumn>
//				frameItemTFC(frameItemHierarchy, UseDefaultIID());
//				if (!frameItemTFC) {
//					//CA("!!ITextFrameColumn");
//					flag=0;
//					break;
//				}
//
//				InterfacePtr<ITextModel> txtModel(frameItemTFC->QueryTextModel());
//				if(!txtModel)
//				{
//					//CA("!textModel" );
//					flag=0;
//					break;
//				}
//
////------------------------CS3 Change---------------------------------------------------------------//
//
//		//		CA(commentDataPtrVector[i]->comment.commentString);
//				PMString textToBeDisplayed(objAPpubComment.comment.commentString);
//				if(objAPpubComment.comment_type == 1)//for product
//				{
//					textToBeDisplayed.Append("\n");
//					CObjectValue cObjValue= ptrIAppFramework->GETProduct_getObjectElementValue(objAPpubComment.comment.id,objAPpubComment.languageId);
//					PMString productName = iConverter->translateString(cObjValue.getName());
//					textToBeDisplayed.Append(productName);
//			//		CA(textToBeDisplayed);
//					textToBeDisplayed.Append("\n");
//					textToBeDisplayed.AppendNumber(objAPpubComment.comment.id);
//			//		CA(textToBeDisplayed);
//				}
//				else if(objAPpubComment.comment_type == 2)//for item
//				{
//			//		CA("for item");
//					textToBeDisplayed.Append("\n");
//					CItemModel cItemModel= ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(objAPpubComment.comment.id,objAPpubComment.languageId);
//					PMString itemName = iConverter->translateString(cItemModel.getItemDesc());
//					textToBeDisplayed.Append(itemName);
//			//		CA(textToBeDisplayed);
//					textToBeDisplayed.Append("\n");
//					textToBeDisplayed.AppendNumber(objAPpubComment.comment.id);
//			//		CA(textToBeDisplayed);
//				}
//
//				WideString* displayString= &WideString(textToBeDisplayed);
//				//.........
//					int32 stringLength = displayString->Length();
//				//.............
//							//	
//				txtModel->Insert(0,displayString);
//			//	CA("b4 addTagToText"+ commentDataPtrVector[i]->comment.displayName);
//				addTagToText(frameUIDRef,txtModel,objAPpubComment.comment.displayName);
//			//	CA("after addTagToText");
//
//				objAPpubComment.isCommentPresentOnDocument = kTrue;
//			}
//			if((objAPpubComment.comment_type == 6) || (objAPpubComment.comment_type == 7))//for product Image 
//			{
//				PMString imagePathWithSubdir = imagePath;
//				PMString typeCodeString;
//				int32 ImageTypeId;
//				do{
//						//typeCodeString=ptrIAppFramework->ClientActionGetAssets_getProductAssetFolder(objAPpubComment.assetId);
//						VectorAssetValuePtr AssetValuePtrObj = ptrIAppFramework->GETAssets_getAssetByAssetId(objAPpubComment.assetId);
//						if(AssetValuePtrObj == NULL){
//							//CA("Show Product images : AssetValuePtrObj == NULL");
//							ptrIAppFramework->LogDebug("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer::AssetValuePtrObj == NULL");
//							break ;
//						}
//				
//						if(AssetValuePtrObj->size() ==0){
//							//CA("AssetValuePtrObj->size() ==0");
//							ptrIAppFramework->LogDebug("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer::AssetValuePtrObj->size() ==0");
//							break ;			
//						}
//
//						PMString fileName("");
//						VectorAssetValue::iterator it; // iterator of Asset value
//
//						
//						CAssetValue objCAssetvalue;
//						for(it = AssetValuePtrObj->begin();it!=AssetValuePtrObj->end();it++)
//						{
//							objCAssetvalue = *it;
//							fileName = objCAssetvalue.getFile_name();
//							
//							int32 assetID = objCAssetvalue.getAsset_id();					
//
//							if(fileName=="")
//								continue ;
//							
//							int32 image_mpv = objCAssetvalue.getMpv_value_id();
//																	
//							ImageTypeId = objCAssetvalue.getType_id();
//							do{
//								SDKUtilities::Replace(fileName,"%20"," ");
//							}while(fileName.IndexOfString("%20") != -1);
//
//						}
//
//						if(objAPpubComment.comment_type == 6)
//							typeCodeString = ptrIAppFramework->ClientActionGetAssets_getProductAssetFolder(objAPpubComment.assetId);
//						if(objAPpubComment.comment_type == 7)
//							typeCodeString = ptrIAppFramework->ClientActionGetAssets_getItemAssetFolder(objAPpubComment.assetId);
//						
//						if(typeCodeString.NumUTF16TextChars()!=0)
//						{ 				
//							PMString TempString = typeCodeString;		
//							CharCounter ABC = 0;
//							bool16 Flag = kTrue;
//							bool16 isFirst = kTrue;
//							do
//							{					
//								if(TempString.Contains("/", ABC))
//								{
//			 						ABC = TempString.IndexOfString("/"); 				
//								
//		 							PMString * FirstString = TempString.Substring(0, ABC);		 		
//		 							PMString newsubString = *FirstString;		 	
//											 		
//		 							if(!isFirst)
//		 							imagePathWithSubdir.Append("\\");
//									
//		 							imagePathWithSubdir.Append(newsubString);		 	
//		 							FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir), kTrue);					 		
//		 							isFirst = kFalse;					 		
//		 							PMString * SecondString = TempString.Substring(ABC+1);
//		 							PMString SSSTring =  *SecondString;		 		
//		 							TempString = SSSTring;		 		
//								}
//								else
//								{				
//									if(!isFirst)
//		 							imagePathWithSubdir.Append("\\");
//									
//		 							imagePathWithSubdir.Append(TempString);		 		
//		 							FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir), kTrue);		 		
//		 							isFirst = kFalse;				
//									Flag= kFalse;							
//								}			
//							}while(Flag);
//						}
//
//						FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir));
//						SDKUtilities ::AppendPathSeparator(imagePathWithSubdir);
//										
//						if(ptrIAppFramework->getAssetServerOption() == 0)
//						{
//							if(objAPpubComment.comment_type == 6)
//							{
//								if(!ptrIAppFramework->GETAsset_downLoadProductAsset(objAPpubComment.assetId,imagePathWithSubdir))
//								{
//									ptrIAppFramework->LogDebug("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer::!GETAsset_downLoadProductAsset");				
//									break;
//								}
//							}
//							
//							if(objAPpubComment.comment_type == 7)
//							{
//								if(!ptrIAppFramework->GETAsset_downLoadItemAsset(objAPpubComment.assetId,imagePathWithSubdir))
//								{
//									ptrIAppFramework->LogDebug("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer::!GETAsset_downLoadProductAsset");				
//									break;
//								}
//							}					
//						}
//
//						if(!fileExists(imagePathWithSubdir,fileName))	
//						{
//							ptrIAppFramework->LogDebug("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer: Image Not Found on Local Drive");
//							break; 
//						}
//						PMString total=imagePathWithSubdir+fileName;	
//											
//						if(ImportFileInFrame(frameUIDRef, total))
//						{
//							fitImageInBox(frameUIDRef);
//						}
//				}while(0);
//				////image info in text frame
//				//	//PMString textToBeDisplayed = ptrIAppFramework->TYPECACHE_getTypeNameById(ImageTypeId);
//				//	PMString textToBeDisplayed("");
//				//	if(typeValObjForProductImage!=nil)
//				//	{	
//				//		VectorTypeInfoValue::iterator it2;
//				//
//				//		for(it2=typeValObjForProductImage->begin();it2!=typeValObjForProductImage->end();it2++)
//				//		{	
//				//			int32 typeID = it2->getType_id();
//				//			if(typeID == ImageTypeId)
//				//			{
//				//				textToBeDisplayed = it2->getName();
//				//				break;
//				//			}
//				//		}
//				//	}
//
//				//	//CA("textToBeDisplayed = " + textToBeDisplayed);
//
//				//	InterfacePtr<IPMUnknown> unknown(frameUIDRef, IID_IUNKNOWN);
//				//	if(!unknown)
//				//		break;
//
//				//	UID textFrameUID=Utils<IFrameUtils>()->GetTextFrameUID(unknown);
//				//	if(textFrameUID==kInvalidUID )
//				//	{
//				//		this->convertBoxToTextBox(frameUIDRef);
//				//		InterfacePtr<IPMUnknown> unknown(frameUIDRef, IID_IUNKNOWN);
//				//		if(!unknown)
//				//			break;
//				//		textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
//
//				//		if(objAPpubComment.comment_type == 5)
//				//		{
//				//			this->addTagToGraphicFrame(frameUIDRef);
//				//			objAPpubComment.isCommentPresentOnDocument = kTrue;
//				//			continue;
//				//		}
//				//	}
//
//				//	textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
//				//	if(textFrameUID == kInvalidUID)
//				//		break;
//
//				//	InterfacePtr<ITextFrame> textFrame(frameUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
//				//	if(!textFrame)
//				//		break;
//
//				//	ITextModel* txtModel = textFrame->QueryTextModel();
//				//	if (txtModel == nil)
//				//		break;
//				//	
//				//	InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
//				//	if (textModel == nil)
//				//	{
//				//		//CA("textModel == nil");
//				//		break;
//				//	}
//
//				//	CObjectValue cObjValue= ptrIAppFramework->GETProduct_getObjectElementValue(objAPpubComment.comment.id,objAPpubComment.languageId);
//				//	PMString productName = iConverter->translateString(cObjValue.getName());
//				//	
//				//	PMString temp("Static Text for Product Image");
//				//	temp.Append("\n");
//				//	temp.Append("Type :- ");
//				//	temp.Append(textToBeDisplayed);
//				//	temp.Append("\n");
//				//	temp.Append("Product Name :- ");
//				//	temp.Append(productName);
//				//	temp.Append("\n");
//				//	temp.Append("Product ID :- ");
//				//	temp.AppendNumber(objAPpubComment.comment.id);
//				//	
//				//	textToBeDisplayed.Clear();
//				//	textToBeDisplayed = temp;
//
//
//				//	WideString* displayString= &WideString(textToBeDisplayed);
//				//	//.........
//				//		int32 stringLength = displayString->Length();
//				//	//.............
//				//				//	
//				//	txtModel->Insert(kTrue,0,displayString);
//				////	CA("b4 addTagToText"+ commentDataPtrVector[i]->comment.displayName);
//				//	addTagToText(frameUIDRef,txtModel,objAPpubComment.comment.displayName);
//
//				////image info in text frame ends
//
//				
//				/// Creating and Adding tag to newly created Item image box
//				InterfacePtr<IPMUnknown> unknown(frameUIDRef, IID_IUNKNOWN);
//				if(unknown == NULL){
//					ptrIAppFramework->LogDebug("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer: unknown NULL");
//					return ;
//				}
//				InterfacePtr<IDocument> doc(frameUIDRef.GetDataBase(), frameUIDRef.GetDataBase()->GetRootUID(), UseDefaultIID());
//				if(doc == NULL){
//					ptrIAppFramework->LogDebug("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer: Document not found...");
//					return ;
//				}
//				InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(doc));
//				if (rootElement == NULL){			
//					ptrIAppFramework->LogDebug("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer: IIDXMLElement NIL...");
//					return ;
//				}
//				
//				XMLReference parent = rootElement->GetXMLReference();
//
//				PMString tagName("");
//				if(typeValObjForProductImage!=nil)
//				{	
//					VectorTypeInfoValue::iterator it2;
//			
//					for(it2=typeValObjForProductImage->begin();it2!=typeValObjForProductImage->end();it2++)
//					{	
//						int32 typeID = it2->getType_id();
//						if(typeID == ImageTypeId)
//						{
//							//CA("typeID == ImageTypeId");
//							tagName = it2->getName();
//							break;
//						}
//					}
//				}
//
//				PMString TagName("");				
//				
//				if(objAPpubComment.comment_type == 7)
//				{
//					 tagName = ptrIAppFramework->TYPECACHE_getTypeNameById(ImageTypeId);
//				}
//				
//				TagName = prepareTagName(tagName);
//
//				CPbObjectValue* CPbObjectValueptr = ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(objAPpubComment.SECTION_NO , objAPpubComment.comment.id);
//
//				XMLReference resultXMLRef=kInvalidXMLReference;
//				do {
//					// Acquire the IXLMElementCommands interface on the Utils boss, and use its method
//					// to tag the frame.  Arbitrarily ask for the element to be the 0th child of it's parent.
//					ErrorCode errCode = Utils<IXMLElementCommands>()->CreateElement(WideString(TagName), frameUIDRef.GetUID(), parent, 0, &resultXMLRef);
//					// Verify the results: no errors, valid XMLRef returned, we can instantiate it.
//					if (errCode != kSuccess)
//					{
//						//CA("ExpXMLActionComponent::TagFrameElement - CreateElement failed");
//						break;
//					}		
//					if (resultXMLRef == kInvalidXMLReference)
//					{
//						//CA("ExpXMLActionComponent::TagFrameElement - Can't create new XMLReference");
//						break;
//					}		
//					InterfacePtr<IIDXMLElement> newXMLElement (resultXMLRef.Instantiate());
//					if (newXMLElement==NULL) 
//					{
//						//CA("newXMLElement==NULL");
//						ptrIAppFramework->LogInfo("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer: ExpXMLActionComponent::TagFrameElement - Can't instantiate new XML element");
//					}		
//					
//					PMString attribName("ID");
//					PMString attribVal("");
//					attribVal.AppendNumber(-1);							
//					ErrorCode err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal)); //Cs4
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "typeId";
//					attribVal.AppendNumber(ImageTypeId);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal)); //Cs4
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "index";
//
//					if(objAPpubComment.comment_type == 6)
//					{
//						attribVal.AppendNumber(3);	
//					}
//					if(objAPpubComment.comment_type == 7)
//					{
//						attribVal.AppendNumber(4);	
//					}
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal)); //cs4
//					
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "imgFlag";
//					attribVal.AppendNumber(1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal)); //cs4
//				
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "parentID";
//					attribVal.AppendNumber(objAPpubComment.comment.id);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal)); //Cs4
//					
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "parentTypeID";
//					attribVal.AppendNumber(CPbObjectValueptr->getobject_type_id());							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal)); //Cs4
//					
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "sectionID";
//					attribVal.AppendNumber(CurrentSelectedSection);						
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
//				
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "tableFlag";
//					attribVal.AppendNumber(0);						
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "LanguageID";								
//					attribVal.AppendNumber(objAPpubComment.languageId);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "isAutoResize";							
//					attribVal.AppendNumber(-1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "rowno";
//					attribVal.AppendNumber(-1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "colno";
//					attribVal.AppendNumber(-1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
//				}while (false);
//			}			
//
//			if((objAPpubComment.comment_type == 8) || (objAPpubComment.comment_type == 9))//for product Table 
//			{
//				//CA("objAPpubComment.comment_type == 9");
//				/*PMString tableTypeID = "tableTypeID";
//				tableTypeID.AppendNumber(objAPpubComment.assetId);
//				CA(tableTypeID);*/
//
//				int32 TableTypeID = objAPpubComment.assetId;
//
//				PMString tagName("");
//				if(objAPpubComment.comment_type == 8)
//				{
//					if(typeValObjForProductTable!=nil)
//					{	
//						VectorTypeInfoValue::iterator it2;
//				
//						for(it2=typeValObjForProductTable->begin();it2!=typeValObjForProductTable->end();it2++)
//						{	
//							int32 typeID = it2->getType_id();
//							if(typeID == TableTypeID)
//							{
//								//CA("typeID == ImageTypeId");
//								tagName = it2->getName();
//								break;
//							}
//						}
//					}
//				}
//				if(objAPpubComment.comment_type == 9)
//				{
//					if(typeValObjForItemTable!=nil)
//					{	
//						VectorTypeInfoValue::iterator it2;				
//						for(it2=typeValObjForItemTable->begin();it2!=typeValObjForItemTable->end();it2++)
//						{	
//							int32 typeID = it2->getType_id();
//							if(typeID == TableTypeID)
//							{
//								//CA("typeID == TableTypeID");
//								tagName = it2->getName();
//								break;
//							}
//						}
//					}
//				}
//			
//				InterfacePtr<IPMUnknown> unknown(frameUIDRef, IID_IUNKNOWN);
//				if(unknown == NULL){
//					ptrIAppFramework->LogDebug("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer: unknown NULL");
//					return ;
//				}
//				InterfacePtr<IDocument> doc(frameUIDRef.GetDataBase(), frameUIDRef.GetDataBase()->GetRootUID(), UseDefaultIID());
//				if(doc == NULL){
//					ptrIAppFramework->LogDebug("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer: Document not found...");
//					return ;
//				}
//				InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(doc));
//				if (rootElement == NULL){			
//					ptrIAppFramework->LogDebug("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer: IIDXMLElement NIL...");
//					return ;
//				}
//				
//				XMLReference parent = rootElement->GetXMLReference();
//
//				PMString TagName = prepareTagName(tagName);
//
//				CPbObjectValueptr = ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(objAPpubComment.SECTION_NO , objAPpubComment.comment.id);
//				//CPbObjectValue* CPbObjectValueptr = ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(objAPpubComment.SECTION_NO , objAPpubComment.comment.id);
//
//				XMLReference resultXMLRef=kInvalidXMLReference;
//				do {
//					// Acquire the IXLMElementCommands interface on the Utils boss, and use its method
//					// to tag the frame.  Arbitrarily ask for the element to be the 0th child of it's parent.
//					ErrorCode errCode = Utils<IXMLElementCommands>()->CreateElement(WideString(TagName), frameUIDRef.GetUID(), parent, 0, &resultXMLRef); //Cs4
//					// Verify the results: no errors, valid XMLRef returned, we can instantiate it.
//					if (errCode != kSuccess)
//					{
//						//CA("ExpXMLActionComponent::TagFrameElement - CreateElement failed");
//						break;
//					}		
//					if (resultXMLRef == kInvalidXMLReference)
//					{
//						//CA("ExpXMLActionComponent::TagFrameElement - Can't create new XMLReference");
//						break;
//					}		
//					InterfacePtr<IIDXMLElement> newXMLElement (resultXMLRef.Instantiate());
//					if (newXMLElement==NULL) 
//					{
//						//CA("newXMLElement==NULL");
//						ptrIAppFramework->LogInfo("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer: ExpXMLActionComponent::TagFrameElement - Can't instantiate new XML element");
//					}		
//					
//					PMString attribName("ID");
//					PMString attribVal("");
//					attribVal.AppendNumber(-1);							
//					ErrorCode err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "typeId";
//					attribVal.AppendNumber(TableTypeID);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "index";
//
//					if(objAPpubComment.comment_type == 8)
//					{
//						attribVal.AppendNumber(3);	
//					}
//					if(objAPpubComment.comment_type == 9)
//					{
//						attribVal.AppendNumber(4);	
//					}
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
//					
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "imgFlag";
//					attribVal.AppendNumber(0);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
//				
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "parentID";
//					attribVal.AppendNumber(objAPpubComment.comment.id);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
//					
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "parentTypeID";
//					attribVal.AppendNumber(CPbObjectValueptr->getobject_type_id());							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
//					
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "sectionID";
//					attribVal.AppendNumber(CurrentSelectedSection);						
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
//				
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "tableFlag";
//					attribVal.AppendNumber(1);						
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "LanguageID";								
//					attribVal.AppendNumber(objAPpubComment.languageId);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "isAutoResize";							
//					attribVal.AppendNumber(-1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "rowno";
//					attribVal.AppendNumber(-1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "colno";
//					attribVal.AppendNumber(-1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
//				}while (false);
//
//				InterfacePtr<ITableUtility> iTableUtlObj
//				((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
//				if(!iTableUtlObj)
//				{
//					// CA("!iTableUtlObj");
//					ptrIAppFramework->LogError("AP7_RefreshContent::TableUtility::fillDataInTable::!iTableUtlObj");
//					return ;
//				}
//
//				InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
//				if(!DataSprayerPtr)
//				{
//                    ptrIAppFramework->LogDebug("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer::Pointre to DataSprayerPtr not found");
//					return;
//				}
//
//				InterfacePtr<ITagReader> itagReader
//				((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
//				if(!itagReader)
//				{
//					ptrIAppFramework->LogDebug("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer:itagReader == nil");
//					return;
//				}
//
//				IIDXMLElement* ptr = NULL;
//				TagList tagList = itagReader->getFrameTags(frameUIDRef);
//
//				/*PMString tagListSize("Inside product finder tagList.size = ");
//				tagListSize.AppendNumber(tagList.size());
//				CA(tagListSize);*/
//
//				TagStruct tagStruct = tagList[0];
//
//				PublicationNode refpNode;
//				PMString PubName("PubName");
//
//				if(tagStruct.whichTab == 3)
//					refpNode.setAll(PubName, tagStruct.parentId, CPbObjectValueptr->getPub_object_id(),tagStruct.sectionID,1,1,1,tagStruct.typeId,0,kTrue,kFalse);
//				else if(tagStruct.whichTab == 4)
//					refpNode.setAll(PubName, tagStruct.parentId, CPbObjectValueptr->getPub_object_id(),tagStruct.sectionID,1,1,1,tagStruct.typeId,0,kFalse,kFalse);
//			
//				DataSprayerPtr->FillPnodeStruct(refpNode, tagStruct.sectionID, 1, tagStruct.sectionID);
//				
//				DataSprayerPtr->getAllIds(tagStruct.parentId);
//				iTableUtlObj->SetDBTableStatus(kTrue);
//				
//				int32 tableId=0;
//				//CA("Going for DataSprayer fillDataInTable ");
//				UIDRef tableUIDRef;
//				if(isTablePresent(frameUIDRef, tableUIDRef))
//				{
//					if(objAPpubComment.comment_type == 8)
//					{
//						iTableUtlObj->fillDataInTable(tableUIDRef, refpNode, tagStruct, tableId, frameUIDRef);
//					}
//					if(objAPpubComment.comment_type == 9)
//					{
//						XMLReference boxXMLRef = tagStruct.tagPtr->GetXMLReference();
//						iTableUtlObj->FillDataInsideItemTableOfItem(boxXMLRef ,tableUIDRef);
//					}
//				}
//				//-------lalit-----
//				for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
//				{
//					tagList[tagIndex].tagPtr->Release();
//				}
//			}
//			if(objAPpubComment.comment_type == 10 || objAPpubComment.comment_type == 11)
//			{
//			//	CA("1");
//				InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
//				if(!DataSprayerPtr)
//				{
//                    ptrIAppFramework->LogDebug("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer::Pointre to DataSprayerPtr not found");
//					return;
//				}
//
//			//	CA("2");
//				InterfacePtr<ITagReader> itagReader
//				((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
//				if(!itagReader)
//				{
//					ptrIAppFramework->LogDebug("AAP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer:itagReader == nil");
//					return;
//				}
//
//				CPbObjectValue* CPbObjectValueptr = ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(objAPpubComment.SECTION_NO , objAPpubComment.comment.id);
//				
//			//	CA("3");
//				PublicationNode pNode;
//				
//				//pNode.setPublicationName();
//				pNode.setPubId(objAPpubComment.comment.id);
//				pNode.setParentId(objAPpubComment.SECTION_NO);
//				pNode.setLevel(CPbObjectValueptr->getLevel_no());
//				pNode.setSequence(CPbObjectValueptr->getSeq_order());
//				pNode.setChildCount(CPbObjectValueptr->getChildCount());
//				//pNode.setHitCount();
//				//pNode.setReferenceId();
//				pNode.setTypeId(CPbObjectValueptr->getobject_type_id());
//				//pNode.setDesignerAction();
//				pNode.setNewProduct(CPbObjectValueptr->getNew_product());
//				pNode.setPBObjectID(CPbObjectValueptr->getPub_object_id());
//				pNode.setIsProduct(CPbObjectValueptr->getisProduct());
//				pNode.setIsONEsource(kFalse);
//				pNode.setIsStarred(CPbObjectValueptr->getStarredFlag1());
////CA("before fill node list");
//				DataSprayerPtr->FillPnodeStruct(pNode,objAPpubComment.SECTION_NO,CPbObjectValueptr->getPublication_id(),objAPpubComment.SECTION_NO);
//
//				DataSprayerPtr->setFlow(kFalse);
//				DataSprayerPtr->getAllIds(pNode.getPubId());//For these PF, PR, PG ITEM ID's we have to spray the data
//
////CA("after fill node list");
//
//				IIDXMLElement* ptr = NULL;
//				TagList tagList = itagReader->getTagsFromBox(frameUIDRef, &ptr);
//				
//				//PMString tagListSize("Inside product finder tagList.size = ");
//				//tagListSize.AppendNumber(tagList.size());
//				//CA(tagListSize);
//				
//				//CA("4");
//				if(tagList.size()<=0)//This can be a Tagged Frame
//				{
//					if(DataSprayerPtr->isFrameTagged(frameUIDRef))
//					{	
//						//CA("isFrameTagged(selectUIDList.GetRef(i))");
//						DataSprayerPtr->sprayForTaggedBox(frameUIDRef);				
//					}
//					continue;
//				}
//				else
//					DataSprayerPtr->sprayForThisBox(frameUIDRef, tagList);
//				
//
//				
//				UIDList list(frameUIDRef);
//				resizeTextFrame(frameUIDRef,list,objAPpubComment.BOT_X_COORDINATE - objAPpubComment.TOP_X_COORDINATE, objAPpubComment.BOT_Y_COORDINATE - objAPpubComment.TOP_Y_COORDINATE);
//				//DataSprayerPtr->sprayForThisBox(frameUIDRef,);
//				//CA("5");
//
//				//------------
//				for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
//				{
//					tagList[tagIndex].tagPtr->Release();
//				}
//			}
//		}
//		objIndex = 0;
//	}
}

 void SPSelectionObserver::createRectangle(PMRect box,int32 layerNo,UIDRef& frameUIDRef, int32 spreadNumber)
{
	//UIDRef result = UIDRef::gNull;
	//
	///*PMString ASD("spreadNumber : ");
	//ASD.AppendNumber(spreadNumber);
	//CA(ASD);*/

	//do
	//{
	//	addLayerIfNeeded();

	//	InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
	//	if (layoutData == nil)
	//	{
	//		CA("ILayoutControlData is nil");
	//		break;
	//	}

	//	IDocument* document = layoutData->GetDocument();
	//	if (document == nil)
	//	{
	//		CA("document is nil");
	//		break;
	//	}
	//	
	//	IDataBase* database = ::GetDataBase(document);		
	//	if(database == nil)
	//	{
	//		CA("database == nil");
	//		break;
	//	}

	//	InterfacePtr<ISpreadList>spreadList(document,UseDefaultIID());
	//	if(spreadList == nil)
	//	{
	//		CA("spreadList == nil");
	//		break;
	//	}	

	//	UIDRef firstSreadUIDRef(database, spreadList->GetNthSpreadUID(spreadNumber-1)); 
	//	InterfacePtr<ISpread> iSpread(firstSreadUIDRef, IID_ISPREAD); 
	//	if(iSpread == nil)
	//	{
	//		CA("spread == nil");
	//		spreadNumber;
	//	}
	///*	IGeometry* spreadItem = layoutData->GetSpread();
	//	if(spreadItem == nil)
	//	{
	//		CA("spreadItem is nil");		
	//		break;
	//	}

	//	InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
	//	if (iSpread == nil)
	//	{
	//		CA("iSpread is nil");				
	//		break;
	//	}*/

	//	// Get the list of document layers
	//	InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);
	//	if (layerList == nil)
	//	{
	//		CA("layerList is nil");			
	//		break;
	//	}

	//	/*PMString ASD("layerNo : ");
	//	ASD.AppendNumber(layerNo);
	//	CA(ASD);*/
	//	layerNo = layerNo +2;

	//	IDocumentLayer* iDocumentLayer = layerList->QueryLayer(layerNo);
	//	if (iDocumentLayer == nil)
	//	{

	//		CA("iDocumentLayer is nil");				
	//		break;
	//	}
	//	iDocumentLayer->SetVisible(kTrue);

	//	InterfacePtr<ISpreadLayer> spreadLayer(iSpread->QueryLayer(iDocumentLayer));
	//	if (spreadLayer == nil)
	//	{
	//		CA("spreadLayer is nil");		
	//		break;
	//	}

	//	UIDRef layerRef = ::GetUIDRef(spreadLayer);

	//	InterfacePtr<IGeometry> pageGeo(layerRef.GetDataBase(), layoutData->GetPage(), UseDefaultIID());
	//	if (pageGeo == nil )
	//	{
	//		//CA("pageGeo is nil");
	//		break;
	//	}

	//	frameUIDRef = Utils<IPathUtils>()->CreateRectangleSpline(layerRef,	box, 
	//												INewPageItemCmdData::kGraphicFrameAttributes,0x263);

	//	const UIDList splineItemList(frameUIDRef);

	//	ICommandSequence *sequence=CmdUtils::BeginCommandSequence();
	//	if (sequence==nil)
	//	{
	//		CA("sequence is nil");		
	//		break;
	//	}

	//	PMReal strokeWeight(0.0);

	//	InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->
	//	CreateStrokeWeightCommand(strokeWeight,&splineItemList,kTrue,kTrue));

	//	if(strokeSplineCmd==nil)
	//	{
	//		CA("strokeSplineCmd is nil");
	//		break;
	//	}

	//	if(CmdUtils::ProcessCommand(strokeSplineCmd)==kFailure)
	//	{
	//		CA("command  is failed");		
	//		break;
	//	}

	//	InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QueryActiveSwatchList());
	//	if(iSwatchList==nil)
	//	{
	//		CA("iSwatchList is nil");		
	//		break;
	//	}

	//	UID blackUID = iSwatchList->GetPaperSwatchUID(); 
	//	//UID blackUID = iSwatchList->GetBlackSwatchUID(); 
	//	InterfacePtr<ICommand> applyStrokeCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeRenderingCommand(blackUID,&splineItemList,kTrue,kTrue));

	//	if(applyStrokeCmd==nil)
	//	{
	//		CA("applyStrokeCmd is nil");		
	//		break;
	//	}

	//	if(CmdUtils::ProcessCommand(applyStrokeCmd)!=kSuccess)
	//	{
	//		CA("processCmd is failed");		
	//		break;
	//	}

	//	CmdUtils::EndCommandSequence(sequence);

	//	bool16 selectFrame = SelectFrame(frameUIDRef);

	//	//*****************move newly created frame to specified cordinates
	//	moveCreatedFrame(frameUIDRef,box);
	//	
	//}while(false);
	//CA("Before return");
	return ;
}


//void SPSelectionObserver::createRectangle(PMRect box,int32 layerNo,UIDRef& frameUIDRef,int32 maxSpread)
//{
//	UIDRef result = UIDRef::gNull;
//	do
//	{
//		addLayerIfNeeded();
//
//		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
//		if (layoutData == nil)
//		{
//			CA("ILayoutControlData is nil");
//			break;
//		}
//
//		IDocument* document = layoutData->GetDocument();
//		if (document == nil)
//		{
//			CA("document is nil");
//			break;
//		}
//
//		InterfacePtr<ISpreadList>spreadList(document,UseDefaultIID());
//		if(spreadList == nil)
//		{
//			CA("spreadList == nil");
//			continue;
//		}
//
//		int32 spreadCount = spreadList->GetSpreadCount();
//
//		/*PMString totalSpreads("");
//		totalSpreads.AppendNumber(spreadCount);
//		CA("totalSpreads = " + totalSpreads);*/
//
//		IGeometry* spreadItem = layoutData->GetSpread();
//		if(spreadItem == nil)
//		{
//			CA("spreadItem is nil");		
//			break;
//		}
//
//		InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
//		if (iSpread == nil)
//		{
//			CA("iSpread is nil");				
//			break;
//		}
//
//		// Get the list of document layers
//		InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);
//		if (layerList == nil)
//		{
//			CA("layerList is nil");			
//			break;
//		}
//		
//		int32 layerCount = layerList->GetCount();
//		
//		/*PMString Temp("layerCount = ");
//		Temp.AppendNumber(layerCount);
//		Temp.Append(" , layerNo = ");
//		Temp.AppendNumber(layerNo);
//		CA(Temp);*/
//		/*bool16 status = kFalse;
//		while(1)
//		{
//			if(layerCount <= layerNo)
//			{
//				status = createNewLayer();
//				if(status == kFalse)
//					break;
//				layerCount++;
//			}
//			else 
//				break;
//		}*/
//
//		
//		layerNo++;
//		layerNo++;
//		IDocumentLayer* iDocumentLayer = layerList->GetLayer(layerNo);
//		if (iDocumentLayer == nil)
//		{
//			CA("iDocumentLayer is nil");				
//			break;
//		}
//
//		InterfacePtr<ISpreadLayer> spreadLayer(iSpread->QueryLayer(iDocumentLayer));
//		if (spreadLayer == nil)
//		{
//			CA("spreadLayer is nil");		
//			break;
//		}
//
//		UIDRef layerRef = ::GetUIDRef(spreadLayer);
//
//		InterfacePtr<IGeometry> pageGeo(layerRef.GetDataBase(), layoutData->GetPage(), UseDefaultIID());
//		if (pageGeo == nil )
//		{
//			//CA("pageGeo is nil");
//			break;
//		}
//		//CA("before CreateRectangleSpline");
//		frameUIDRef = Utils<IPathUtils>()->CreateRectangleSpline(layerRef,	box, 
//													INewPageItemCmdData::kGraphicFrameAttributes,0x263);
//		//CA("after CreateRectangleSpline");
//		const UIDList splineItemList(frameUIDRef);
//
//		ICommandSequence *sequence=CmdUtils::BeginCommandSequence();
//		if (sequence==nil)
//		{
//			CA("sequence is nil");		
//			break;
//		}
//
//		PMReal strokeWeight(0.0);
//
//		InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->
//		CreateStrokeWeightCommand(strokeWeight,&splineItemList,kTrue,kTrue));
//
//		if(strokeSplineCmd==nil)
//		{
//			CA("strokeSplineCmd is nil");
//			break;
//		}
//
//		if(CmdUtils::ProcessCommand(strokeSplineCmd)==kFailure)
//		{
//			//CA("command  is failed");		
//			break;
//		}
//
//		InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QueryActiveSwatchList());
//		if(iSwatchList==nil)
//		{
//			//CA("iSwatchList is nil");		
//			break;
//		}
//
//		//UID blackUID = iSwatchList->GetPaperSwatchUID(); 
//		UID blackUID = iSwatchList->GetBlackSwatchUID(); 
//		InterfacePtr<ICommand> applyStrokeCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeRenderingCommand(blackUID,&splineItemList,kTrue,kTrue));
//
//		if(applyStrokeCmd==nil)
//		{
//			//CA("applyStrokeCmd is nil");		
//			break;
//		}
//
//		if(CmdUtils::ProcessCommand(applyStrokeCmd)!=kSuccess)
//		{
//			//CA("processCmd is failed");		
//			break;
//		}
//
//		CmdUtils::EndCommandSequence(sequence);
//		//CA("before SelectFrame");
//		bool16 selectFrame = SelectFrame(frameUIDRef);
//		//CA("before moveCreatedFrame");
//		//*****************move newly created frame to specified cordinates
//		moveCreatedFrame(frameUIDRef,box);
//		//CA("After moveCreatedFrame");
//	}while(false);
//	return ;
//}

void SPSelectionObserver::createOval(PMRect box,int32 layerNo,IDocument* document,UIDRef& frameUIDRef)
{
	//UIDRef result = UIDRef::gNull;
	//do
	//{
	//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//	if(ptrIAppFramework == nil)
	//		break;

	//	InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
	//	if (layoutData == nil)
	//		break;

	//	/*--------------CS3 Change---------------- */
	//	/*IGeometry* spreadItem = layoutData->GetSpread();
	//	if(spreadItem == nil)
	//		break;

	//	InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
	//	if (iSpread == nil)
	//		break;*/

	//	UIDRef spreadRef=layoutData->GetSpreadRef();

	//	InterfacePtr<ISpread> iSpread(spreadRef, UseDefaultIID());
	//	if (iSpread == nil)
	//	{
	//		ptrIAppFramework->LogDebug("AP7ProductFinder::SPSelectionObserver::createOval::iSpread is NULL");				
	//		break;
	//	}

	//	/*--------------CS3 Change---------------- */
	//	InterfacePtr <IDocument> myDoc(document, UseDefaultIID());
	//	if(!myDoc)
	//		break;

	//	// Get the list of document layers
	//	InterfacePtr<ILayerList> layerList(myDoc, IID_ILAYERLIST);
	//	if(!layerList)
	//		break;

	//	int32 layerCount = layerList->GetCount();

	//	IDocumentLayer* iDocumentLayer = layerList->QueryLayer(layerNo);
	//	if(!iDocumentLayer )
	//		break;

	//	InterfacePtr<ISpreadLayer> spreadLayer(iSpread->QueryLayer(iDocumentLayer));
	//	if (spreadLayer == nil)
	//		break;

	//	UIDRef layerRef = ::GetUIDRef(spreadLayer);

	//	InterfacePtr<IGeometry> pageGeo(layerRef.GetDataBase(), layoutData->GetPage(), UseDefaultIID());
	//	if (pageGeo == nil )
	//		break;

	//	frameUIDRef = Utils<IPathUtils>()->CreateOvalSpline(layerRef,	box, 
	//												INewPageItemCmdData::kGraphicFrameAttributes,0x263);

	//	
	//	const UIDList splineItemList(frameUIDRef);

	//	ICommandSequence *sequence=CmdUtils::BeginCommandSequence();
	//	if (sequence==nil)
	//		break;

	//	PMReal strokeWeight(0.0);

	//	InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->
	//	CreateStrokeWeightCommand(strokeWeight,&splineItemList,kTrue,kTrue));

	//	if(strokeSplineCmd==nil)
	//		break;

	//	if(CmdUtils::ProcessCommand(strokeSplineCmd)==kFailure)
	//		break;

	//	InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QueryActiveSwatchList());
	//	if(iSwatchList==nil)
	//		break;

	//	UID blackUID = iSwatchList->GetPaperSwatchUID(); //GetNoneSwatchUID();

	//	InterfacePtr<ICommand> applyStrokeCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeRenderingCommand(blackUID,&splineItemList,kTrue,kTrue));

	//	if(applyStrokeCmd==nil)
	//		break;

	//	if(CmdUtils::ProcessCommand(applyStrokeCmd)!=kSuccess)
	//		break;

	//	CmdUtils::EndCommandSequence(sequence);

	//	bool16 selectFrame = SelectFrame(frameUIDRef);

	//	//*****************move newly created frame to specified cordinates
	//	moveCreatedFrame(frameUIDRef,box);
	//	
	//}while(false);
	return ;
}

///////////////////////////////////////////////////////////////////////////////

void SPSelectionObserver::createArrowGraphic(PMRect box,int32 layerNo,IDocument* document,UIDRef& frameUIDRef,PMPathPointList& pathPointList)
{
	//UIDRef result = UIDRef::gNull;
	//SDKLayoutHelper layoutHelper;

	//do
	//{
	//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//	if(ptrIAppFramework == nil)
	//		break;

	//	InterfacePtr<ILayoutControlData> layoutControlData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
	//	if (layoutControlData == nil)
	//		break;

	//			
	//	/*--------------CS3 Change---------------- */
	//	/*IGeometry* spreadItem = layoutData->GetSpread();
	//	if(spreadItem == nil)
	//		break;

	//	InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
	//	if (iSpread == nil)
	//		break;*/

	//	UIDRef spreadRef=layoutControlData->GetSpreadRef();

	//	InterfacePtr<ISpread> iSpread(spreadRef, UseDefaultIID());
	//	if (iSpread == nil)
	//	{
	//		ptrIAppFramework->LogDebug("AP7ProductFinder::SPSelectionObserver::createOval::iSpread is NULL");				
	//		break;
	//	}

	//	/*--------------CS3 Change---------------- */
	//	
	//	// Get the list of document layers
	//	InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);

	//	int32 layerCount = layerList->GetCount();
	//	
	//	IDocumentLayer* iDocumentLayer = layerList->QueryLayer(layerNo);

	//	InterfacePtr<ISpreadLayer> spreadLayer(iSpread->QueryLayer(iDocumentLayer));
	//	if (spreadLayer == nil)
	//		break;

	//	UIDRef parentUIDRef = ::GetUIDRef(spreadLayer);

	//	if (layoutControlData->GetPage() == kInvalidUID)
	//		break;
	//	
	//	UIDRef pageUIDRef(parentUIDRef.GetDataBase(), layoutControlData->GetPage());
	//	PMRect boundsInParentCoords = layoutHelper.PageToSpread(pageUIDRef, box);

	//	// Create a series of path segments to describe the spline.
	//	PMRect boundingBoxInnerCoords (0.0, 0.0, 100.0, 100.0);
	//		
	//	// Create the spline.
	//	frameUIDRef = layoutHelper.CreateSplineGraphic(parentUIDRef, box, boundingBoxInnerCoords, pathPointList, kFalse /*not a closed path*/);
	//	if (frameUIDRef == UIDRef::gNull)
	//	{
	//		break;
	//	}

	//	// Apply some graphic attributes to specify how the path should be drawn,
	//	SnpGraphicHelper graphicHelper(frameUIDRef);
	//	graphicHelper.AddStrokeWeight(5.0);
	//	graphicHelper.AddStrokeRendering(graphicHelper.GetNamedSwatch(PMString("Black"), frameUIDRef.GetDataBase()));
	//	graphicHelper.AddLineImplementation(kSolidPathStrokerBoss);//kDashedPathStrokerBoss,kSolidPathStrokerBoss,kCannedDotPathStrokerBoss,kCannedDash3x2PathStrokerBoss
	//	K2Vector<PMReal> dashAndGapValues;
	//	const PMReal dash0(32.0);
	//	const PMReal gap0(16.0);
	//	dashAndGapValues.push_back(dash0);
	//	dashAndGapValues.push_back(gap0);
	//	graphicHelper.AddDashedValues(dashAndGapValues);
	//	graphicHelper.AddJoinType(SnpGraphicHelper::kJTRound);
	//	graphicHelper.AddLineCap(SnpGraphicHelper::kLCRound);
	//	graphicHelper.AddLineEndEndImplementation(kBarbedArrowHeadBoss);//kBarbedArrowHeadBoss,kCircleSolidArrowHeadBoss,kTriangleArrowHeadBoss
	//	graphicHelper.ApplyAttributes();

	//	bool16 selectFrame = SelectFrame(frameUIDRef);

	//	//*****************move newly created frame to specified cordinates
	//	moveCreatedFrame(frameUIDRef,box);

	//}while(false);
	
	return ;
}


bool16 SPSelectionObserver::SelectFrame(const UIDRef& frameUIDRef)
{
	bool16 result = kFalse;
	do 
	{
		//Select the frame.
		InterfacePtr<ISelectionManager> selectionManager(Utils<ISelectionUtils>()->QueryActiveSelection ());
		if (selectionManager == nil)
		{
			//CA("selectionManager == nil");
			break;
		}

		// Deselect everything.
		selectionManager->DeselectAll(nil); // deselect every active CSB
			
		// Make a layout selection.
		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
		if (!layoutSelectionSuite) {
			break;
		}
		layoutSelectionSuite->SelectPageItems(UIDList(frameUIDRef), 
			Selection::kReplace,  Selection::kDontScrollLayoutSelection);
				
		result = kTrue;
	} while(false);
	return result;
}

void SPSelectionObserver::moveCreatedFrame(UIDRef frameUIDRef,PMRect box)
{
	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;

		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{	
			//CA("Selection Manager is Null");
			break;
		}

		UIDRef ref;

		InterfacePtr<ITextMiscellanySuite> txtMiscellanySuite(iSelectionManager,UseDefaultIID());//IID_ITEXTMISCELLANYSUITEE);
		if(!txtMiscellanySuite){
			//CA("Text Selection Suite is null");
			break;
		}
		//CA("Text Miscellany Suite");
		
		UIDList  TempUidList;
		if(!txtMiscellanySuite->GetUidList(TempUidList))
		{
			//CA("!!!!GetUidList");
			break;
		}
		//CA("GetUidList");

		InterfacePtr<ILayoutControlData> layoutData1(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutData1 == nil)
			break;

		UIDRef spreadRef=layoutData1->GetSpreadRef();

		InterfacePtr<ISpread> iSpread1(spreadRef, UseDefaultIID());
		if (iSpread1 == nil)
		{
			ptrIAppFramework->LogDebug("AP7ProductFinder::SPSelectionObserver::createOval::iSpread1 is NULL");				
			break;
		}

		/*--------------CS3 Change---------------- */

		Transform::CoordinateSpace coordinateSpace_1 = Transform::PasteboardCoordinates() ;
		PMRect pageMargin = iSpread1->GetPagesBounds(coordinateSpace_1);//----CS5-----

		PMReal left3,top3,right3,bottom3;
		left3 = pageMargin.Left();
		top3 = pageMargin.Top();
		right3 = pageMargin.Right();
		bottom3 = pageMargin.Bottom();
		/*PMString Dimentions3;
		Dimentions3.Append(" LEFT3 = ");
		Dimentions3.AppendNumber(left3);
		Dimentions3.Append(", TOP3 = ");
		Dimentions3.AppendNumber(top3);
		Dimentions3.Append(", RIGHT3 = ");
		Dimentions3.AppendNumber(right3);
		Dimentions3.Append(", BOTTOM3 = ");
		Dimentions3.AppendNumber(bottom3);
		CA(Dimentions3);*/

		//int32 pageWidth = ToInt32(right3);
		int32 leftOfPage = ToInt32(left3);
		int32 rightOfPage = ToInt32(right3);
		int32 topOfPage = ToInt32(top3);
		int32 bottomOfPage = ToInt32(bottom3);
		int32 halfOfPageHeight = (bottomOfPage - topOfPage)/2;

		PMRect maxLimitsOftextFrames;
		bool16 status = getMaxLimitsOfBoxes(TempUidList,maxLimitsOftextFrames);

		PMReal left2,top2,right2,bottom2;
		left2 = maxLimitsOftextFrames.Left();
		top2 = maxLimitsOftextFrames.Top() + halfOfPageHeight;
		right2 = maxLimitsOftextFrames.Right();
		bottom2 = maxLimitsOftextFrames.Bottom() + halfOfPageHeight;
		/*PMString Dimentions2;
		Dimentions2.Append(" LEFT2 = ");
		Dimentions2.AppendNumber(left2);
		Dimentions2.Append(", TOP2 = ");
		Dimentions2.AppendNumber(top2);
		Dimentions2.Append(", RIGHT2 = ");
		Dimentions2.AppendNumber(right2);
		Dimentions2.Append(", BOTTOM2 = ");
		Dimentions2.AppendNumber(bottom2);
		CA(Dimentions2);*/

		int32 numberOfPagesInCurrentSpread = getNoOfPagesfromCurrentSread();

		int32 pageWidth=0;
		int32 sideOfPage = 0; //0 for left & 1 for right
		if(numberOfPagesInCurrentSpread == 1)
		{
			if(leftOfPage == 0)
			{
				pageWidth = rightOfPage;
				sideOfPage = 1;
			}
			else
			{
				pageWidth = -leftOfPage;
				sideOfPage = 0;
			}
		}
		else if(numberOfPagesInCurrentSpread == 2)
		{
			pageWidth = (rightOfPage - leftOfPage)/2;
		}
		PMString widthOfPage;
		widthOfPage.AppendNumber(pageWidth);
		//CA("widthOfPage = " + widthOfPage);

		int32 CurrentX = ToInt32(left2);
		int32 CurrentY = ToInt32(top2);
					
		PMReal left4,top4,right4,bottom4;
		left4 = box.Left();
		top4 = box.Top();
		right4 = box.Right();
		bottom4 = box.Bottom();
		
		int32 X = ToInt32(left4);
		int32 Y = ToInt32(top4);
				
		getRelativeMoveAmountOfxAndy(X ,Y,numberOfPagesInCurrentSpread,CurrentX,CurrentY,pageWidth,sideOfPage);
		
		PMPoint temp(X,Y);

		/*-----------------------------CS3 Change----------------------------------*/
		//Utils<Facade::ITransformFacade>()->Move(TempUidList,temp,kFalse,ITransform::kTransformItemAndChildren);
		
		ErrorCode errorCode;
		Transform::CoordinateSpace coordinateSpace = Transform::PasteboardCoordinates() ;
		PBPMPoint referencePoint(PMPoint(0,0));
		errorCode =  Utils<Facade::ITransformFacade>()->TransformItems( TempUidList, coordinateSpace, referencePoint, Transform::TranslateBy(temp.X(),temp.Y()));
		/*-----------------------------CS3 Change----------------------------------*/
	}while(kFalse);

}

bool16 SPSelectionObserver::getMaxLimitsOfBoxes(UIDList  TempUidList, PMRect& maxBounds)
{
	int32 length = TempUidList.Length();
	
	UID Frame_uId = TempUidList[0];

	K2Vector<PMRect> boxMarginList;
	boxMarginList.clear();

	for(int32 i = 0 ; i < length ; i++)
	{

		UIDRef textFrameUIDRef=TempUidList.GetRef(i);
		
		PMRect textFrameBounds;
			
		InterfacePtr<IGeometry> textFrameGeometry(textFrameUIDRef, UseDefaultIID());
		if (textFrameGeometry == nil)
		{
			//CA("textFrameGeometry == nil ");
			break;
		}

		//CA("textFrameGeometry");
		textFrameBounds = textFrameGeometry->GetStrokeBoundingBox();
		
		PMReal left,top,right,bottom;
		left = textFrameBounds.Left();
		top = textFrameBounds.Top();
		right = textFrameBounds.Right();
		bottom = textFrameBounds.Bottom();
		/*PMString Dimentions;
		Dimentions.Append(" LEFT = ");
		Dimentions.AppendNumber(left);
		Dimentions.Append(", TOP = ");
		Dimentions.AppendNumber(top);
		Dimentions.Append(", RIGHT = ");
		Dimentions.AppendNumber(right);
		Dimentions.Append(", BOTTOM = ");
		Dimentions.AppendNumber(bottom);
		CA(Dimentions);*/

		InterfacePtr<ITransform> transform(textFrameGeometry, UseDefaultIID());
		if (transform == nil)
		{
			//CA("transform == nil ");
			break;
		}
		//CA("transform ");
		
		//PMRect* rectangle = &textFrameBounds;
		//transform->InverseTransform(&textFrameBounds);
		
		/*------------CS3 Change --------------=*/
		//transform->Transform(&textFrameBounds);
		::TransformInnerRectToParent(transform, &textFrameBounds);
		/*------------CS3 Change --------------=*/

		PMReal left1,top1,right1,bottom1;
		left1 = textFrameBounds.Left();
		top1 = textFrameBounds.Top() + 396;
		right1 = textFrameBounds.Right();
		bottom1 = textFrameBounds.Bottom() + 396;
		/*PMString Dimentions1;
		Dimentions1.Append(" LEFT1 = ");
		Dimentions1.AppendNumber(left1);
		Dimentions1.Append(", TOP1 = ");
		Dimentions1.AppendNumber(top1);
		Dimentions1.Append(", RIGHT1 = ");
		Dimentions1.AppendNumber(right1);
		Dimentions1.Append(", BOTTOM1 = ");
		Dimentions1.AppendNumber(bottom1);
		CA(Dimentions +"\n" +Dimentions1);*/

		boxMarginList.push_back(textFrameBounds);
	}

	if(boxMarginList.size() == 0)
		return kFalse;

	PMReal minTop=0.0, minLeft=0.0 , maxBottom=0.0 , maxRight=0.0;

	PMReal top = boxMarginList[0].Top();
	PMReal left = boxMarginList[0].Left();
	PMReal bottom = boxMarginList[0].Bottom();
	PMReal right = boxMarginList[0].Right();

	/*PMString ASD("boxMarginList[0].Top() : ");
	ASD.AppendNumber((boxMarginList[0].Top()));
	ASD.Append("  boxMarginList[0].Left() : ");
	ASD.AppendNumber((boxMarginList[0].Left()));
	ASD.Append("  boxMarginList[0].Bottom() : ");
	ASD.AppendNumber((boxMarginList[0].Bottom()));
	ASD.Append("  boxMarginList[0].Right() : ");
	ASD.AppendNumber((boxMarginList[0].Right()));
	CA(ASD);*/
		
	minTop = top;
	minLeft = left;
	maxBottom = bottom;
	maxRight = right;

	for(int i=1;i<boxMarginList.size();i++)
	{
		
		top = boxMarginList[i].Top();
		left = boxMarginList[i].Left();
		bottom = boxMarginList[i].Bottom();
		right = boxMarginList[i].Right();

		/*PMString ASD("boxMarginList[i].Top() : ");
		ASD.AppendNumber((boxMarginList[i].Top()));
		ASD.Append("  boxMarginList[i].Left() : ");
		ASD.AppendNumber((boxMarginList[i].Left()));
		ASD.Append("  boxMarginList[i].Bottom() : ");
		ASD.AppendNumber((boxMarginList[i].Bottom()));
		ASD.Append("  boxMarginList[i].Right() : ");
		ASD.AppendNumber((boxMarginList[i].Right()));
		CA(ASD);*/

		if(top < minTop)
			minTop = top;
		if(left < minLeft)
			minLeft = left;
		if(bottom > maxBottom)
			maxBottom = bottom;
		if(right > maxRight)
			maxRight = right;
	}

	maxBounds.Top(minTop);
	maxBounds.Left(minLeft);
	maxBounds.Bottom(maxBottom);
	maxBounds.Right(maxRight);

	return kTrue;
}

int32 SPSelectionObserver::getNoOfPagesfromCurrentSread()
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return 0;

	InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
	if (layoutData == nil)
		return 0;


	UIDRef spreadRef=layoutData->GetSpreadRef();

	InterfacePtr<ISpread> iSpread(spreadRef, UseDefaultIID());
	if (iSpread == nil)
	{
		ptrIAppFramework->LogDebug("AP7ProductFinder::SPSelectionObserver::getNoOfPagesfromCurrentSread::iSpread is NULL");				
		return 0;
	}

	int numPages=iSpread->GetNumPages();
	return numPages;
}

void SPSelectionObserver::getRelativeMoveAmountOfxAndy(int32 &X,int32 &Y,int32 numberOfPagesInCurrentSpread,int32 CurrentX,int32 CurrentY,int32 pageWidth,int32 sideOfPage)
{
	if(X == 0 && Y == 0 && CurrentX == 0 && CurrentY == 0)
	{
		//CA("dont do anything"); ///old
		if(numberOfPagesInCurrentSpread ==2)
		{
			X = - pageWidth;
		}
	}
	else if(X == 0 && Y == 0 && ((CurrentX != 0 && CurrentX > 0) || (CurrentY != 0 && CurrentY > 0 && CurrentX > 0)) && numberOfPagesInCurrentSpread == 1)
	{
		//CA("1");					//old
		X = X - CurrentX;
		Y = Y - CurrentY;
	}
	else if(X == 0 && Y == 0 && ((CurrentX != 0 && CurrentX > 0) || (CurrentY != 0 && CurrentY > 0 && CurrentX > 0)) && numberOfPagesInCurrentSpread == 2)
	{
		//CA("5");					
		X = -(pageWidth + CurrentX);
		Y = -CurrentY;
	}
	else if(X == 0 && Y == 0 && ((CurrentX != 0 && CurrentX < 0) || (CurrentY != 0 && CurrentY < 0)) && numberOfPagesInCurrentSpread == 2)
	{
		//CA("3");
		X = 0 - (CurrentX + pageWidth);
		Y = - CurrentY;
	}
	else if(X == 0 && Y == 0 && ((CurrentX != 0 && CurrentX < 0) || (CurrentY != 0 && CurrentY < 0)) && numberOfPagesInCurrentSpread == 1)
	{
		//CA(" for left page");
		X = - (pageWidth + CurrentX);
		Y = - CurrentY;
	}
	else if(X != 0 && Y != 0 && ((CurrentX != 0 && CurrentX < 0) || (CurrentY != 0 && CurrentY < 0)))
	{
		//CA("4");
		X = X - (CurrentX + pageWidth);
		Y = Y - CurrentY;
	}
	else if(X == 0 && Y != 0 && numberOfPagesInCurrentSpread == 1)
	{
		//CA("new one for left page == 1");
		if(sideOfPage == 0)
		{
			X = -(pageWidth + CurrentX);			
			Y = Y - CurrentY;
		}
		else
		{
			//CA("right page");
			X = X - CurrentX;			
			Y = Y - CurrentY;
		}

	}
	else if(X != 0 && Y == 0 && numberOfPagesInCurrentSpread == 1)
	{
		//CA("new one for left page ");
		if(sideOfPage == 0)
		{
			//CA("left page");
			X = X - (pageWidth + CurrentX); 
			Y = Y - CurrentY;
		}
		else
		{
			//CA("right page");
			X = X - CurrentX;			
			Y = Y - CurrentY;
		}
	}
	else if(numberOfPagesInCurrentSpread == 1)
	{	
		//CA("numberOfPagesInCurrentSpread == 1");
		X = X - CurrentX;			//old
		Y = Y - CurrentY;
	}
	else if(numberOfPagesInCurrentSpread == 2)
	{
		//CA("numberOfPagesInCurrentSpread == 2");
		X = X - pageWidth - CurrentX;
		Y = Y - CurrentY;
	}
	return;
}

void SPSelectionObserver::addLayerIfNeeded()
{
	////CA(__FUNCTION__);
	//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//if(ptrIAppFramework == nil)
	//{
	//	//CA("ptrIAppFramework == nil");	
	//	return;
	//}

	//int32 numberOfStages=ptrIAppFramework->getStageBySectionId(Mediator::sectionID);
	////Default Document has 2 layers so 
	////numberOfStages += 2;
	//numberOfStages += 3;//default document has 2 layers and we want to add comments on red layer
	//					//so adding 3 ....
	//
	//IDocument * theFrontDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();

	//InterfacePtr<ILayerList> layerList(theFrontDoc, IID_ILAYERLIST);

	//int32 layerCount = layerList->GetCount();
	///*PMString lc("layerCount::");
	//lc.AppendNumber(layerCount);
	//lc.Append("____NumberOfStages");
	//lc.AppendNumber(numberOfStages);
	//CA(lc);*/
	//
	//if(numberOfStages == layerCount)
	//{
	//	//CA("numberOfStages == layerCount");		
	//	return;
	//}

	//else if(numberOfStages > layerCount)
	//{
	//	int32 noOfLayersToBeAdded = numberOfStages - layerCount;
	//	
	//	for(int32 i = 0; i <noOfLayersToBeAdded;i++)
	//	{
	//		IDataBase* database = ::GetDataBase(theFrontDoc);
	//												
	//		InterfacePtr<IDocument> document(database, database->GetRootUID(), UseDefaultIID());
	//		if (!document)
	//		{
	//			CA("!document");				
	//			return;
	//		}

	//		InterfacePtr<ILayerList> layerList(theFrontDoc, IID_ILAYERLIST);
	//		if (!layerList)
	//		{
	//			CA("!layerList");			
	//			return;
	//		}
	//				
	//		InterfacePtr<ICommand> newLayerCmd(CmdUtils::CreateCommand(kNewLayerCmdBoss));
	//		InterfacePtr<INewLayerCmdData> newLayerCmdData(newLayerCmd, IID_INEWLAYERCMDDATA);
	//		newLayerCmdData->Set(GetUIDRef(document));
	//		CmdUtils::ProcessCommand(newLayerCmd);
	//	}
	//}
}

bool16 SPSelectionObserver::convertBoxToTextBox(UIDRef& boxUIDRef)
{
		InterfacePtr<ICommand> command ( CmdUtils::CreateCommand(kConvertItemToTextCmdBoss));
		if(!command)
			return kFalse;
		command->SetItemList(UIDList(boxUIDRef));
		if(CmdUtils::ProcessCommand(command)!=kSuccess)
			return kFalse;

	return kTrue;

}

void SPSelectionObserver::addTagToGraphicFrame(UIDRef& curBox)
{
	////CA(__FUNCTION__);
	//do{
	//	APpubComment objAPpubComment = global_VectorAPpubCommentValue[objIndex];
	//	
	//	InterfacePtr<IPMUnknown> unknown(curBox, IID_IUNKNOWN);
	//	if(unknown == nil){
	//		break;
	//	}
	//	InterfacePtr<IDocument> doc(curBox.GetDataBase(), curBox.GetDataBase()->GetRootUID(), UseDefaultIID());
	//	if(doc == nil)
	//		break;

	//	InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(doc));
	//	if (rootElement == nil)			
	//		break;

	//	XMLReference parent = rootElement->GetXMLReference();
	//	
	//	XMLReference storyXMLRef = this->TagFrameElement(parent, curBox.GetUID(),objAPpubComment.comment.displayName);
	//	if(storyXMLRef == kInvalidXMLReference)
	//	{
	//		break;
	//	}
	//	this->attachAttributes(&storyXMLRef,kTrue);
	//}while(kFalse);
}


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
XMLReference SPSelectionObserver::TagFrameElement(const XMLReference& newElementParent,UID frameUID,PMString& frameTagName)
{
	//CA(__FUNCTION__);
	XMLReference resultXMLRef=kInvalidXMLReference;
	do {
		// Acquire the IXLMElementCommands interface on the Utils boss, and use its method
		// to tag the frame.  Arbitrarily ask for the element to be the 0th child of it's parent.
		ErrorCode errCode = Utils<IXMLElementCommands>()->CreateElement(WideString(frameTagName), frameUID, newElementParent, 0, &resultXMLRef); //Cs4
		// Verify the results: no errors, valid XMLRef returned, we can instantiate it.
		if (errCode != kSuccess)
		{
			//CA("ExpXMLActionComponent::TagFrameElement - CreateElement failed");
			break;
		}		
		if (resultXMLRef == kInvalidXMLReference)
		{
			//CA("ExpXMLActionComponent::TagFrameElement - Can't create new XMLReference");
			break;
		}		
		InterfacePtr<IIDXMLElement> newXMLElement (resultXMLRef.Instantiate());
		if (newXMLElement==nil) 
		{
			//CA("ExpXMLActionComponent::TagFrameElement - Can't instantiate new XML element");
		}
	}while (false);

	return resultXMLRef;
}

void SPSelectionObserver::addTagToText(UIDRef& textFrameUIDRef,ITextModel* txtModel,PMString& displyName)
{
	do
	{
		//CA("Inside addTagToText ");
		UIDRef txtMdlUIDRef =::GetUIDRef(txtModel);
		if (txtMdlUIDRef.GetDataBase() == nil) 
			break;
 
		// Get the document's root element
		InterfacePtr<IDocument> doc(txtMdlUIDRef.GetDataBase(), txtMdlUIDRef.GetDataBase()->GetRootUID(), UseDefaultIID());
		InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(doc));

		if (rootElement == nil)
			break;
			
		// Before tagging the text itself, we have to verify the story itself is tagged.
		InterfacePtr<IXMLReferenceData> storyXMLRefData(txtMdlUIDRef, UseDefaultIID());
		
		if (storyXMLRefData == nil)
			break;
		
		XMLReference storyXMLRef = storyXMLRefData->GetReference();

		/// for attaching XMLtag to the selected range of text.. 
		//first we need to tag whole story innsdie the Text box
		// We can add the Text tag below the Frame Story Tag In our case the Story tag name  is 'PRINTsource'
		if (storyXMLRef == kInvalidXMLReference)   
		{							
			XMLReference parent = rootElement->GetXMLReference();
	
			PMString storyTagName("PRINTsource");
			UID txtUID = txtMdlUIDRef.GetUID();
			//Here first time attachAttributes() function is called
			storyXMLRef = tagFrameElement(parent, txtUID, storyTagName);
		
			if (storyXMLRef == kInvalidXMLReference)
			{				
				// We should have been able to create this element
				//CA("ExpXMLActionComponent::DoTagSelectedText - can't create XMLRef for story");
				break;
			}
		}	

		XMLReference* newTag = new XMLReference;
		
		int32 start = 0;
		int32 end=/*start+displyName.NumUTF16TextChars()*/ txtModel->TotalLength()-1 ;
		/*PMString ASD("End : ");
		ASD.AppendNumber(end);
		CA(ASD);
		*/
			
		//CA("Addd::"+displyName);
		Utils<IXMLElementCommands>()->CreateElement(WideString(displyName) , txtMdlUIDRef,start, end,kInvalidXMLReference, newTag); //Cs4
		//CA("After creating Element ");
		attachAttributes(newTag,kTrue);
		//CA("After attach attributes");
		//delete newTag;

	}while(false);
}

///////////////////////////////////////////////////////////////////////////////

XMLReference SPSelectionObserver::tagFrameElement(const XMLReference& newElementParent,UID& frameUID,const PMString& frameTagName)
{
	XMLReference resultXMLRef=kInvalidXMLReference;
	do {
		// Acquire the IXLMElementCommands interface on the Utils boss, and use its method
		// to tag the frame.  Arbitrarily ask for the element to be the 0th child of it's parent.
		ErrorCode errCode = Utils<IXMLElementCommands>()->CreateElement(WideString(frameTagName), frameUID, newElementParent, 0, &resultXMLRef); //Cs4
		// Verify the results: no errors, valid XMLRef returned, we can instantiate it.
		if (errCode != kSuccess)
			break;
		
		if (resultXMLRef == kInvalidXMLReference)
			break;
		
		InterfacePtr<IIDXMLElement> newXMLElement (resultXMLRef.Instantiate());
		if (newXMLElement==nil) 
		{
			//CA("AP7_ProductFinder::SPSelectionObserver::tagFrameElement - Can't instantiate new XML element");
			break;
		}
	}while (false);

	if(frameTagName == "PRINTsource")
	{
		this->attachAttributes(&resultXMLRef,kFalse);
	}
	return resultXMLRef;
}

///////////////////////////////////////////////////////////////////////////////

void SPSelectionObserver::attachAttributes(XMLReference* newTag,bool16 flag)
{
	//CA(__FUNCTION__);
	//APpubComment objAPpubComment = global_VectorAPpubCommentValue[objIndex];


	//PMString attribName("ID");
	//PMString attribVal("");
	//attribVal.AppendNumber(objAPpubComment.APpubCommentID);
	//if(!flag){
	//	attribVal="-1";
	//}

	//ErrorCode err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	//attribName.Clear();
	//attribVal.Clear();
	//attribName = "typeId";
	//attribVal.AppendNumber(objAPpubComment.comment_type);
	//if(!flag){
	//	attribVal="-1";
	//}
	//err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	//attribName.Clear();
	//attribVal.Clear();
	//attribName = "index";
	//attribVal.AppendNumber(6);
	//if(!flag){
	//	attribVal="-1";
	//}
	//err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	//attribName.Clear();
	//attribVal.Clear();
	//attribName = "imgFlag";
	//attribVal.AppendNumber(-1);
	//if(!flag){
	//	attribVal="-1";
	//}
	//err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	//attribName.Clear();
	//attribVal.Clear();
	//attribName = "parentID";
	//attribVal.AppendNumber(objAPpubComment.stage);
	//if(!flag){
	//	attribVal="-1";
	//}
	//err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	//attribName.Clear();
	//attribVal.Clear();
	//attribName = "parentTypeID";
	//attribVal.AppendNumber(-1);
	//if(!flag){
	//	attribVal="-1";
	//}
	//err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	//attribName.Clear();
	//attribVal.Clear();
	//attribName = "sectionID";
	//attribVal.AppendNumber(-1);
	//if(!flag){
	//	attribVal="-1";
	//}
	//err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	//attribName.Clear();
	//attribVal.Clear();
	//attribName = "tableFlag";
	//attribVal.AppendNumber(-1);
	//if(!flag){
	//	attribVal="-1";
	//}
	//err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	//attribName.Clear();
	//attribVal.Clear();
	//attribName = "LanguageID";	
	//attribVal.AppendNumber(-1);
	//if(!flag){
	//	attribVal="NULL";
	//}
	//err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	//attribName.Clear();
	//attribVal.Clear();
	//attribName = "isAutoResize";
	//attribVal="-1";
	//
	//err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4

	//attribName.Clear();
	//attribVal.Clear();
	//attribName = "rowno";
	//attribVal.AppendNumber(-1);
	//if(flag){
	//	attribVal="-1";
	//}
	//err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	//attribName.Clear();
	//attribVal.Clear();
	//attribName = "colno";
	//attribVal="-1";
	//
	//err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
}

bool16 SPSelectionObserver::ImportFileInFrame
(const UIDRef& imageBox, const PMString& fromPath)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}
	bool16 fileExists = SDKUtilities::FileExistsForRead(fromPath) == kSuccess;
	if(!fileExists) 
		return kFalse;	
	
	IDocument* doc =Utils<ILayoutUIUtils>()->GetFrontDocument(); 
	IDataBase* db = ::GetDataBase(doc); 
	if (db == NULL)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::ImportFileInFrame::db == NULL");																	
		return kFalse;
	}

	
	IDFile sysFile = SDKUtilities::PMStringToSysFile(const_cast<PMString* >(&fromPath));	
	
	PMString seqName("Import");
	CmdUtils::SequenceContext seq(&seqName);

	// If the place gun is already loaded abort it so we can re-load it. 
	InterfacePtr<IPlaceGun> placeGun(doc/*iDocument*/, UseDefaultIID());
	ASSERT(placeGun);
	if (!placeGun) 
	{
		ErrorUtils::PMSetGlobalErrorCode(kFailure);
		return kTrue;
	}
	
	if (Utils<Facade::IImportExportFacade>()->AbortPlaceGun( placeGun, Facade::IImportExportFacade::kAllItems ) != kSuccess) 
	{
		ASSERT_FAIL("AbortPlaceGun failed");
		ErrorUtils::PMSetGlobalErrorCode(kFailure);
		return kTrue;
	}

	const bool16 retainFormat = kFalse ;
	const bool16 convertQuotes = kFalse ;
	const bool16 applyCJKGrid = kFalse ;
	bool16 useClippingFrame, skipPlace ;
	
	const WideString strURI = (WideString)fromPath ; //sysFile.GetString();
	const URI uri(strURI);

	ErrorCode err = Utils<Facade::IImportExportFacade>()->ImportAndLoadPlaceGun(db, uri, kMinimalUI, 
		retainFormat, convertQuotes, applyCJKGrid, useClippingFrame, skipPlace ) ;
	ASSERT(err == kSuccess);
	if(err != kSuccess) {
		return kTrue;
	}	
	// Get the contents of the place gun as our return value
	UIDRef placedItem(db, placeGun->GetFirstPlaceGunItemUID());
	//imageBox = placedItem;
	
	return kTrue;
}

void SPSelectionObserver::fitImageInBox(const UIDRef& boxUIDRef)
{
	do 
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			return;
		}
		InterfacePtr<ICommand> iAlignCmd(CmdUtils::CreateCommand(kFitContentPropCmdBoss));
		if(!iAlignCmd)
		{
			ptrIAppFramework->LogError("AP7_ProductFinder::SPSelectionObserver::::fitImageInBox::!iAlignCmd");		
			return;
		}

		InterfacePtr<IHierarchy> iHier(boxUIDRef, IID_IHIERARCHY);
		if(!iHier)
		{
			ptrIAppFramework->LogError("AP7_ProductFinder::SPSelectionObserver::::fitImageInBox::!iHier");				
			return;
		}

		if(iHier->GetChildCount()==0)//There may not be any image at all????
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::::fitImageInBox::There may not be any image");						
			return;
		}
		UID childUID=iHier->GetChildUID(0);

		UIDRef newChildUIDRef(boxUIDRef.GetDataBase(), childUID);


		iAlignCmd->SetItemList(UIDList(newChildUIDRef));

		CmdUtils::ProcessCommand(iAlignCmd);
	} while (false);
}

void SPSelectionObserver::AddOrDeleteSpreads(int32 maxSpreadNumber)
{
	InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
	if (layoutData == nil)
	{
		CA("ILayoutControlData is nil");
		return;
	}

	IDocument* document = layoutData->GetDocument();
	if (document == nil)
	{
		CA("document is nil");
		return;
	}

	InterfacePtr<ISpreadList>spreadList(document,UseDefaultIID());
	if(spreadList == nil)
	{
		CA("spreadList == nil");
		return;
	}

	int32 spreadCount = spreadList->GetSpreadCount();

	/*PMString totalSpreads("");
	totalSpreads.AppendNumber(spreadCount);
	CA("totalSpreads = " + totalSpreads);*/

	int32 addSpreadsCount = 0;
	if(spreadCount < maxSpreadNumber)
	{
		addSpreadsCount = maxSpreadNumber - spreadCount;
	}
	for(int32 spreadIndex = 0; spreadIndex < spreadCount; spreadIndex++)
	{
		int32 noOfPages = getNoOfPagesfromSpreadNumber(spreadIndex);

		/*PMString noOfPagesStr("");
		noOfPagesStr.AppendNumber(noOfPages);
		CA("noOfPages = " + noOfPagesStr);*/
		
		if(noOfPages == 1)
		{
			for(int32 i = 0; i<=addSpreadsCount;i++)
			{
				Utils<ILayoutUIUtils>()->AddNewPage();
				Utils<ILayoutUIUtils>()->AddNewPage();
			}
		
			////keepSpreadsTogether
			IDataBase* database = ::GetDataBase(document);
			if(database==nil)
			{
				CA("AP7_ProductFinder::SPSelectionObserver::AddOrDeleteSpreads::No database");
				return;
			}
			UIDList spreadUIDList(database);
			
			for(int32 spreadIndex = 0;spreadIndex < spreadList->GetSpreadCount();spreadIndex++)
			{
				UID spreadUID = spreadList->GetNthSpreadUID(spreadIndex);
				spreadUIDList.Append(spreadUID);
				
				
			}
			InterfacePtr<ICommand> islandCmd(CmdUtils::CreateCommand(kSetIslandSpreadCmdBoss)); 
			if(islandCmd == nil)
			{
				//CA("islandCmd == nil");
				return ;
			}
			bool16 bAllIslands  = kFalse;//kFalse for normal spread
					
			InterfacePtr<IBoolData> boolData(islandCmd, UseDefaultIID()); 
			if(boolData == nil)
			{
				//CA("boolData == nil");
				return ;
			}
			boolData->Set(!bAllIslands); 
			islandCmd->SetItemList(spreadUIDList); 
			
			ErrorCode status = CmdUtils::ProcessCommand(islandCmd); 

			/////Delete Page

			InterfacePtr<IPageList> iPageList(document,UseDefaultIID());
			if(iPageList == nil)
			{
				CA("iPageList == nil");
				return;
			}
			int32 pageCount = iPageList->GetPageCount();

			int32 indexOfLastPageDeleted = 0;//delete only the first page.

			UIDList toBeDeletedPagesUIDList(database);
			for(int32 pageIndex =0;pageIndex <=indexOfLastPageDeleted;pageIndex++)
			{
				toBeDeletedPagesUIDList.Append(iPageList->GetNthPageUID(pageIndex));
			}
			InterfacePtr<ICommand> iDeletePageCmd(CmdUtils::CreateCommand(kDeletePageCmdBoss));
			if (iDeletePageCmd == nil)
			{
				return;
			}
			
			InterfacePtr<IBoolData> iBoolData(iDeletePageCmd,UseDefaultIID());
			if (iBoolData == nil){
	            
				return;
			}
			iBoolData->Set(kFalse);
			
			iDeletePageCmd->SetItemList(toBeDeletedPagesUIDList);
			// process the command
			ErrorCode status1 = CmdUtils::ProcessCommand(iDeletePageCmd);

		}		
	}

}

int32 SPSelectionObserver::getNoOfPagesfromSpreadNumber(int32 spreadIndex)
{
	InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
	if (layoutData == nil)
		return 0;

	IDocument* document = layoutData->GetDocument();
	if (document == nil)
	{
		CA("document is nil");
		return 0;
	}

	InterfacePtr<ISpreadList>spreadList(document,UseDefaultIID());
	if(spreadList == nil)
	{
		CA("spreadList == nil");
		return 0;
	}

	InterfacePtr<IGeometry> spreadItem (spreadList->QueryNthSpread(spreadIndex));
	if(spreadItem == nil)
		return 0;

	InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
	if (iSpread == nil)
		return 0;

	int numPages=iSpread->GetNumPages();
	return numPages;
}

bool16 SPSelectionObserver::copyStencilsFromTheTemplateDocumentIntoScrapData(PMString & templateFilePath,InterfacePtr<IPageItemScrapData> & scrapData ,UIDList &itemList)
 {
	 bool16 result = kFalse;
	do
	{
		SDKLayoutHelper sdklhelp;

		PMString filePathItemsToBeCopiedFrom(templateFilePath);//"c:\\test\\aa.indt");
		IDFile templateIDFile(filePathItemsToBeCopiedFrom);
		UIDRef templateDocUIDRef = sdklhelp.OpenDocument(templateIDFile);
		if(templateDocUIDRef == UIDRef ::gNull)
		{
			CA("uidref of templatedoc is invalid");
			break;
		}						
					
		ErrorCode err = sdklhelp.OpenLayoutWindow(templateDocUIDRef);
		if(err == kFailure)
		{
			CA("Error occured while opening the layoutwindow of the template");
			break;
		}

		InterfacePtr<IDocument> templatedoc(templateDocUIDRef,UseDefaultIID());
		if(templatedoc == nil)
		{
			CA("templatedoc == nil");
			break;
		}
		InterfacePtr<ISpreadList>templateSpreadUIDList(templatedoc,UseDefaultIID());
		if(templateSpreadUIDList == nil)
		{
			CA("templateSpreadUIDList == nil");
			break;
		}
		IDataBase * templateDocDatabase = templateDocUIDRef.GetDataBase();
		if(templateDocDatabase == nil)
		{
			CA("templateDocDatabase == nil");
			break;
		}

		UIDRef templateDocFirstSpreadUIDRef(templateDocDatabase, templateSpreadUIDList->GetNthSpreadUID(0)); 
		InterfacePtr<ISpread> templateSpread(templateDocFirstSpreadUIDRef, IID_ISPREAD); 
		if(templateSpread == nil)
		{
			CA("templateSpread == nil");
			break;
		}
		UIDList templateFrameUIDList(templateDocDatabase);
		if(templateSpread->GetNthPageUID(0)== kInvalidUID) 
		{
			CA("pageUID is invalid");
			break;
		}		
		
		templateSpread->GetItemsOnPage(0,&templateFrameUIDList,kFalse);	

		itemList = templateFrameUIDList;
		
		/*int32 lenOfUIDList = itemList.Length();
		PMString lenStr("");
		lenStr.AppendNumber(lenOfUIDList);
		CA("lenStr = " + lenStr);
		lenOfUIDList = templateFrameUIDList.Length();
		lenStr.Append(" , templateFrameUIDList.Length() =");
		lenStr.AppendNumber(lenOfUIDList);
		CA("lenStr = " + lenStr);*/

		InterfacePtr<ICommand> copyStencilsCMD(CmdUtils::CreateCommand(kCopyCmdBoss));
		if(copyStencilsCMD == nil)
		{
			CA("copyStencilsCMD == nil");
			break;

		}
		InterfacePtr<ICopyCmdData> cmdData(copyStencilsCMD, IID_ICOPYCMDDATA);
		if(cmdData == nil)
		{
			CA("cmdData == nil");
			break;
		}
		// Copy cmd will own this list

		UIDList* listCopy = new UIDList(itemList);
		InterfacePtr<IClipboardController> clipboardController(/*gSession*/GetExecutionContextSession(),UseDefaultIID()); //Cs4
		if(clipboardController == nil)
		{
			CA("clipboardController == nil");
			break;
			
		}
		ErrorCode status = clipboardController->PrepareForCopy();
		if(status == kFailure)
		{
			CA("status == kFailure");
			break;
		}
		InterfacePtr<IDataExchangeHandler> scrapHandler(clipboardController->QueryHandler(kPageItemFlavor));
		if(scrapHandler == nil)
		{
			CA("scrapHandler == nil");
			break;
		}

		clipboardController->SetActiveScrapHandler(scrapHandler);

		InterfacePtr<IPageItemScrapData> localScrapData(scrapHandler, UseDefaultIID());
		if(localScrapData == nil)
		{
			CA("localScrapData == nil");
			break;
		}
		scrapData = localScrapData;
		// specify where to copy item

		UIDRef parent = scrapData->GetRootNode();

		cmdData->Set(copyStencilsCMD, listCopy, parent, scrapHandler);

		if(templateFrameUIDList.Length() == 0)
		{
			return kFalse;
		}
		else
		{
			status = CmdUtils::ProcessCommand(copyStencilsCMD);
		}

		if(status == kFailure)
		{
			CA("status == kFailure");
			break;
		}

	sdklhelp.CloseDocument(templateDocUIDRef,kFalse,K2::kSuppressUI);

		if(listCopy != NULL)
			delete listCopy;

	}while(kFalse);

	result = kTrue;
	return result;

 }

//==========================================================================================================
 bool16 SPSelectionObserver::pasteTheItemsFromScrapDataOntoOpenDocument(InterfacePtr<IPageItemScrapData> & scrapData,UIDRef &documentDocUIDRef ,UIDRef &layerRef)
 {
	bool16 result = kFalse;
	do
	{
		if (scrapData->IsEmpty()==kFalse)
		{//start if (scrapData->IsEmpty()==kFalse)
			//This will give the list of items present on the scrap
			UIDList* scrapContents = scrapData->CreateUIDList();
			if (scrapContents->Length() >= 1)
			{//start if (scrapContents->Length() >= 1)
				InterfacePtr<IDocument> dataToBeSprayedDocument(documentDocUIDRef,UseDefaultIID());
				if(dataToBeSprayedDocument == nil)
				{
					CA("dataToBeSprayedDocument == nil");
					break;
				}
				InterfacePtr<ISpreadList>dataToBeSprayedDocumentSpreadList(dataToBeSprayedDocument,UseDefaultIID());
				if(dataToBeSprayedDocumentSpreadList == nil)
				{
					CA("dataToBeSprayedDocumentSpreadList == nil");
					break;
				}
				IDataBase * dataToBeSprayedDocDatabase = documentDocUIDRef.GetDataBase();
				if(dataToBeSprayedDocDatabase == nil)
				{
					CA("dataToBeSprayedDocDatabase == nil");
					break;
				}

				UIDRef dataToBeSprayedDocFirstSpreadUIDRef(dataToBeSprayedDocDatabase, dataToBeSprayedDocumentSpreadList->GetNthSpreadUID(0)); 
				UIDRef spreadUIDRef;
				spreadUIDRef = dataToBeSprayedDocFirstSpreadUIDRef;
				SDKLayoutHelper sdklhelp;
				UIDRef parentLayerUIDRef =sdklhelp.GetSpreadLayerRef(spreadUIDRef);
				if(parentLayerUIDRef.GetUID() == kInvalidUID)
				{
					CA("parentLayerUIDRef.GetUID() == kInvalidUID");
					break;
				}
				InterfacePtr<ICommand> pasteToClipBoardCMD (CmdUtils::CreateCommand(kPasteCmdBoss));
				if(pasteToClipBoardCMD == nil)
				{
					CA("pasteToClipBoardCMD == nil");
					break;
				}
				InterfacePtr<ICopyCmdData> cmdData(pasteToClipBoardCMD, UseDefaultIID());
				if(cmdData == nil)
				{
					CA("cmdData == nil");
					break;
				}
				if(scrapContents == nil)
				{
					CA("scrapContents == nil");
					break;
				}
				if(scrapContents == nil || parentLayerUIDRef == UIDRef::gNull || pasteToClipBoardCMD == nil)
				{	
					break;
				}
				PMPoint offset(-130.0, 0.0);
				cmdData->SetOffset(offset);

				cmdData->Set(pasteToClipBoardCMD, scrapContents, layerRef/*parentLayerUIDRef*/);
				ErrorCode status = CmdUtils::ProcessCommand(pasteToClipBoardCMD); 
				if(status == kSuccess)
				{
					result = kTrue;
				}
			}//end if (scrapContents->Length() >= 1)

			if(scrapContents != NULL)
				delete scrapContents;
		}//end if (scrapData->IsEmpty()==kFalse)
	}while(kFalse);
	return result;
						
 }


//void SPSelectionObserver::createRectangle(PMRect box,APpubComment objAPpubComment,UIDRef& frameUIDRef)
//{//CA("case 10");
//	do
//	{
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == NULL)
//		{
//			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
//			return;
//		}
//
//		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
//		if (layoutData == nil)
//		{
//			CA("ILayoutControlData is nil");
//			break;
//		}
//
//		IDocument* document = layoutData->GetDocument();
//		if (document == nil)
//		{
//			CA("document is nil");
//			break;
//		}
//		
//		IDataBase* database = ::GetDataBase(document);		
//		if(database == nil)
//		{
//			CA("database == nil");
//			break;
//		}
//
//		UIDRef dataToBeSpreadDocumentUIDRef(nil, kInvalidUID);
//
//        UID documentUID = database->GetRootUID();
//		if (documentUID == kInvalidUID)
//			break;
//		dataToBeSpreadDocumentUIDRef = UIDRef(database, documentUID);
//
//		addLayerIfNeeded();
//			
//		PMString templateFolderPath = ptrIAppFramework->getCatalogPlanningTemplateFolderPath();
//		//CA(templateFolderPath);
//
//		SDKUtilities sdkUtil;
//		if(objAPpubComment.comment_type == 10)
//		{
//			sdkUtil.AppendPathSeparator(templateFolderPath);
//			templateFolderPath.Append("Product");
//		}
//		if(objAPpubComment.comment_type == 11)
//		{
//			sdkUtil.AppendPathSeparator(templateFolderPath);
//			templateFolderPath.Append("Item");
//		}
//
//		
//		sdkUtil.AppendPathSeparator(templateFolderPath);
//		//templateFolderPath.Append("/");
//		templateFolderPath.Append(objAPpubComment.stencilPath);
//		//templateFolderPath.Append(".indt");
//		//CA(objAPpubComment.stencilPath);
//		//CA(templateFolderPath);
//		//PMString CopyStencilFromDocument("C:\\Documents and Settings\\Administrator\\My Documents\\template\\Product\\ProductStencil1.indt");
//		PMString CopyStencilFromPath = templateFolderPath;
//
//		UIDList itemList;
//
//		/*int32 lenOfUIDList = itemList.Length(); 
//		PMString lenStr("");
//		lenStr.AppendNumber(lenOfUIDList);
//		CA("lenStr = " + lenStr);*/
//		InterfacePtr<IPageItemScrapData>  scrapData;
//		bool16 result =  copyStencilsFromTheTemplateDocumentIntoScrapData(CopyStencilFromPath, scrapData ,itemList);
//		if(result == kFalse)
//		{
//			//CA("result == kFalse");
//			ptrIAppFramework->LogDebug("AP7_ProductFinder :: SPSelectionObserver::createCommentsOnLayer::result == kFalse while copying");
//			break;
//		}
//
//		InterfacePtr<ISpreadList>spreadList(document,UseDefaultIID());
//		if(spreadList == nil)
//		{
//			CA("spreadList == nil");
//			break;
//		}	
//
//		/*UIDRef firstSreadUIDRef(database, spreadList->GetNthSpreadUID(0)); */
//		UIDRef firstSreadUIDRef(database, spreadList->GetNthSpreadUID(objAPpubComment.spreadNumber-1)); 
//		InterfacePtr<ISpread> iSpread(firstSreadUIDRef, IID_ISPREAD); 
//		if(iSpread == nil)
//		{
//			CA("spread == nil");
//			//spreadNumber;
//		}
//
//		// Get the list of document layers
//		InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);
//		if (layerList == nil)
//		{
//			CA("layerList is nil");			
//			break;
//		}
//
//		/*PMString ASD("layerNo : ");
//		ASD.AppendNumber(layerNo);
//		CA(ASD);*/
//
//		int32 layerNo = objAPpubComment.stage;
//		layerNo = layerNo + 2;
//
//		IDocumentLayer* iDocumentLayer = layerList->QueryLayer(layerNo);
//		if (iDocumentLayer == nil)
//		{
//			CA("iDocumentLayer is nil");				
//			break;
//		}
//		iDocumentLayer->SetVisible(kTrue);
//
//		InterfacePtr<ISpreadLayer> spreadLayer(iSpread->QueryLayer(iDocumentLayer));
//		if (spreadLayer == nil)
//		{
//			CA("spreadLayer is nil");		
//			break;
//		}
//
//		UIDRef layerRef = ::GetUIDRef(spreadLayer);
//		
//		//Get the item UIDList before Paste 
//		UIDList BeforePasteUIDList(database);
//
//		int numPages=iSpread->GetNumPages();
//
//		for(int i=0; i<numPages; i++)
//		{
//			UIDList tempList(database);
//			iSpread->GetItemsOnPage(i, &tempList, kFalse);
//			BeforePasteUIDList.Append(tempList);
//		}
//												
//		result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapData,dataToBeSpreadDocumentUIDRef,layerRef );
//		
//		//Get the item UIDList After Paste 
//		UIDList AfterPasteUIDList(database);
//
//		for(int i=0; i<numPages; i++)
//		{
//			UIDList tempList(database);
//			iSpread->GetItemsOnPage(i, &tempList, kFalse);
//			AfterPasteUIDList.Append(tempList);
//		}
//
//		//Get the item UIDList 
//		UIDList tempUIDList(database); 
//		
//		for(int32 i = 0 ; i < AfterPasteUIDList.Length() ; i++)
//		{
//			bool16 isUIDFound = kFalse;
//			for(int32 j = 0 ; j < BeforePasteUIDList.Length() ; j++)
//			{
//				if(AfterPasteUIDList[i] == BeforePasteUIDList[j])
//				{
//					isUIDFound = kTrue;
//					break;
//				}					
//			}
//			if(isUIDFound == kFalse)
//				tempUIDList.Append(AfterPasteUIDList[i]);
//		}
//				
//		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
//		if(iSelectionManager == nil)
//		{
//			CA("iSelectionManager == nil");
//			return;
//		}
//		
//		iSelectionManager->DeselectAll(nil);
//		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
//		//InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(Utils<ISelectionUtils>()->QuerySuite(IID_ILAYOUTSELECTION_ISUITE ),UseDefaultIID());
//		if (!layoutSelectionSuite) 
//		{	
//			CA("!layoutSelectionSuite");
//			return;				
//		}		
//		layoutSelectionSuite->SelectPageItems(tempUIDList, Selection::kReplace,  Selection::kAlwaysCenterInView);
//		
//		//*****************move newly created frame to specified cordinates
//		frameUIDRef = tempUIDList.GetRef(0);
//		/*InterfacePtr<IPMUnknown> unknown(frameUIDRef, IID_IUNKNOWN);
//		if(!unknown)
//			return;
//
//		
//		UID textFrameUID=Utils<IFrameUtils>()->GetTextFrameUID(unknown);
//		if(textFrameUID==kInvalidUID )
//		{
//			this->convertBoxToTextBox(frameUIDRef);
//		}*/
//		
//		moveCreatedFrame(frameUIDRef,box);
//
//
//	}while(false);
//}

PMString SPSelectionObserver::prepareTagName(PMString name)
{
	PMString tagName("");
	for(int i=0;i<name.NumUTF16TextChars(); i++)
	{
		if(name.GetChar(i) == '[' || name.GetChar(i) == ']' || name.GetChar(i) == '\n')
			continue;

		if(name.GetChar(i) ==' ')
		{
			tagName.Append("_");
		}
		else tagName.Append(name.GetChar(i));
	}
	PMString FinalTagName("");
	FinalTagName = keepOnlyAlphaNumeric(tagName); // removes the Special characters from the Name.

	return FinalTagName;
}
//Method Purpose: remove the Special character from the Name. 
PMString SPSelectionObserver::keepOnlyAlphaNumeric(PMString name)
{
	PMString tagName("");

	for(int i=0;i<name.NumUTF16TextChars(); i++)
	{
		bool isAlphaNumeric = false ;

		PlatformChar ch = name.GetChar(i);

		if(ch.IsAlpha() || ch.IsNumber())
			isAlphaNumeric = true ;

		if(ch.IsSpace())
		{
			isAlphaNumeric = true ;
			ch.Set('_') ;
		}

		if(ch == '_')
			isAlphaNumeric = true ;
	
		if(isAlphaNumeric) 
			tagName.Append(ch);
	}

	return tagName ;
}

//void SPSelectionObserver::createTableFrame(PMRect box,int32 layerNo,UIDRef& frameUIDRef, int32 spreadNumber ,int32 ObjectID, int32 tableTypeID ,APpubComment objAPpubComment)
//{
//	UIDRef result = UIDRef::gNull;
//	
//	/*PMString ASD("spreadNumber : ");
//	ASD.AppendNumber(spreadNumber);
//	CA(ASD);*/
//
//	do
//	{
//		addLayerIfNeeded();
//
//		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
//		if (layoutData == nil)
//		{
//			CA("ILayoutControlData is nil");
//			break;
//		}
//
//		IDocument* document = layoutData->GetDocument();
//		if (document == nil)
//		{
//			CA("document is nil");
//			break;
//		}
//		
//		IDataBase* database = ::GetDataBase(document);		
//		if(database == nil)
//		{
//			CA("database == nil");
//			break;
//		}
//
//		InterfacePtr<ISpreadList>spreadList(document,UseDefaultIID());
//		if(spreadList == nil)
//		{
//			CA("spreadList == nil");
//			break;
//		}	
//
//		UIDRef firstSreadUIDRef(database, spreadList->GetNthSpreadUID(spreadNumber-1)); 
//		InterfacePtr<ISpread> iSpread(firstSreadUIDRef, IID_ISPREAD); 
//		if(iSpread == nil)
//		{
//			CA("spread == nil");
//			break;
//		}
//	/*	IGeometry* spreadItem = layoutData->GetSpread();
//		if(spreadItem == nil)
//		{
//			CA("spreadItem is nil");		
//			break;
//		}
//
//		InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
//		if (iSpread == nil)
//		{
//			CA("iSpread is nil");				
//			break;
//		}*/
//
//		// Get the list of document layers
//		InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);
//		if (layerList == nil)
//		{
//			CA("layerList is nil");			
//			break;
//		}
//
//		/*PMString ASD("layerNo : ");
//		ASD.AppendNumber(layerNo);
//		CA(ASD);*/
//		layerNo = layerNo +2;
//
//		IDocumentLayer* iDocumentLayer = layerList->QueryLayer(layerNo);
//		if (iDocumentLayer == nil)
//		{
//			CA("iDocumentLayer is nil");				
//			break;
//		}
//		iDocumentLayer->SetVisible(kTrue);
//
//		InterfacePtr<ISpreadLayer> spreadLayer(iSpread->QueryLayer(iDocumentLayer));
//		if (spreadLayer == nil)
//		{
//			CA("spreadLayer is nil");		
//			break;
//		}
//
//		UIDRef layerRef = ::GetUIDRef(spreadLayer);
//
//		InterfacePtr<IGeometry> pageGeo(layerRef.GetDataBase(), layoutData->GetPage(), UseDefaultIID());
//		if (pageGeo == nil )
//		{
//			//CA("pageGeo is nil");
//			break;
//		}
//
//		frameUIDRef = Utils<IPathUtils>()->CreateRectangleSpline(layerRef,	box, 
//													INewPageItemCmdData::kGraphicFrameAttributes,0x263);
//
//		const UIDList splineItemList(frameUIDRef);
//
//		ICommandSequence *sequence=CmdUtils::BeginCommandSequence();
//		if (sequence==nil)
//		{
//			CA("sequence is nil");		
//			break;
//		}
//
//		PMReal strokeWeight(0.0);
//
//		InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->
//		CreateStrokeWeightCommand(strokeWeight,&splineItemList,kTrue,kTrue));
//
//		if(strokeSplineCmd==nil)
//		{
//			CA("strokeSplineCmd is nil");
//			break;
//		}
//
//		if(CmdUtils::ProcessCommand(strokeSplineCmd)==kFailure)
//		{
//			CA("command  is failed");		
//			break;
//		}
//
//		InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QueryActiveSwatchList());
//		if(iSwatchList==nil)
//		{
//			CA("iSwatchList is nil");		
//			break;
//		}
//
//		//UID blackUID = iSwatchList->GetPaperSwatchUID(); 
//		UID blackUID = iSwatchList->GetBlackSwatchUID(); 
//		InterfacePtr<ICommand> applyStrokeCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeRenderingCommand(blackUID,&splineItemList,kTrue,kTrue));
//
//		if(applyStrokeCmd==nil)
//		{
//			CA("applyStrokeCmd is nil");		
//			break;
//		}
//
//		if(CmdUtils::ProcessCommand(applyStrokeCmd)!=kSuccess)
//		{
//			CA("processCmd is failed");		
//			break;
//		}
//
//		CmdUtils::EndCommandSequence(sequence);
//
//		bool16 selectFrame = SelectFrame(frameUIDRef);
//
//		//*****************move newly created frame to specified cordinates
//		moveCreatedFrame(frameUIDRef,box);
//
//		
//		InterfacePtr<IPMUnknown> unknown(frameUIDRef, IID_IUNKNOWN);
//		if(!unknown)
//			break;
//
//		UID textFrameUID=Utils<IFrameUtils>()->GetTextFrameUID(unknown);
//		if(textFrameUID==kInvalidUID )
//		{
//			this->convertBoxToTextBox(frameUIDRef);			
//		}
//
//		textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
//		if(textFrameUID == kInvalidUID)
//			break;
//
//		
//
//		//---------------------CS3 TextFrame change---------------------------//
//		/*InterfacePtr<ITextFrame> textFrame(frameUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
//		if(!textFrame)
//		{
//			CA("!textFrame");
//			break;
//		}
//
//		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
//		if (textModel == nil)
//		{
//			CA("textModel == nil");
//			return;
//		}*/
//
//		InterfacePtr<IHierarchy> graphicFrameHierarchy(frameUIDRef, UseDefaultIID());
//		if (graphicFrameHierarchy == nil) 
//		{
//			//CA("graphicFrameHierarchy is NULL");
//			flag=0;
//			break;
//		}
//						
//		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
//		if (!multiColumnItemHierarchy) {
//			//CA("multiColumnItemHierarchy is NULL");
//			flag=0;
//			break;
//		}
//
//		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
//		if (!multiColumnItemTextFrame) {
//			//CA("multiColumnItemTextFrame is NULL");
//			flag=0;
//			break;
//		}
//		InterfacePtr<IHierarchy>
//		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
//		if (!frameItemHierarchy) {
//			//CA("frameItemHierarchy is NULL");
//			flag=0;
//			break;
//		}
//
//		InterfacePtr<ITextFrameColumn>
//		frameItemTFC(frameItemHierarchy, UseDefaultIID());
//		if (!frameItemTFC) {
//			//CA("!!ITextFrameColumn");
//			flag=0;
//			break;
//		}
//
//		InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
//		if(!textModel)
//		{
//			//CA("!textModel" );
//			flag=0;
//			break;
//		}
//
////------------------------CS3 Change---------------------------------------------------------------//
//
//		UIDRef textStoryUIDRef = ::GetUIDRef(textModel);
//
//		int32 start=0;
//
//		///get no of rows & cols
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == NULL)
//		{
//			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
//			break;
//		}
//		
//		int32 numRows = 0;
//		int32 numCols = 0;
//		
//		if(objAPpubComment.comment_type == 8)
//		{
//			VectorScreenTableInfoPtr tableInfo = NULL;
//			tableInfo= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(CurrentSelectedSection, ObjectID, kTrue);
//			if(tableInfo == NULL)
//				break;
//
//			CObjectTableValue oTableValue;
//			VectorScreenTableInfoValue::iterator it;
//
//			bool16 typeidFound=kFalse;
//			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
//			{
//				oTableValue = *it;
//				if(oTableValue.getTableTypeID() == tableTypeID)
//				{   				
//					typeidFound=kTrue;				
//					break;
//				}
//			}		
//
//			if(!typeidFound){
//				//CA("!typeidFound");
//				if(tableInfo)
//					delete tableInfo;
//				break;
//			}
//
//			numRows = static_cast<int32>(oTableValue.getTableData().size());
//			if(numRows<=0)
//				break;
//			numCols = static_cast<int32>(oTableValue.getTableHeader().size());
//			if(numCols<=0)
//				break;
//
//			if(tableInfo)
//				delete tableInfo;
//		}
//		if(objAPpubComment.comment_type == 9)
//		{
//			VectorScreenItemTableInfoPtr tableInfo=
//						ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(ObjectID);
//			if(tableInfo == NULL)
//				break;
//
//			CItemTableValue oTableValue;
//			VectorScreenItemTableInfoValue::iterator it;
//
//			bool16 typeidFound=kFalse;
//			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
//			{
//				//CA("Inside for");
//				oTableValue = *it;
//				if(oTableValue.getTableTypeID() == tableTypeID)
//				{  // CA("oTableValue.getTableTypeID() == tableTypeID");				
//					typeidFound=kTrue;				
//					break;
//				}
//			}
//		
//
//
//			if(!typeidFound){
//				//CA("!typeidFound");
//				if(tableInfo)
//					delete tableInfo;
//				break;
//			}
//
//			numRows =static_cast<int32> (oTableValue.getTableData().size());
//			if(numRows<=0)
//				break;
//			numCols = static_cast<int32>(oTableValue.getTableHeader().size());
//			if(numCols<=0)
//				break;
//
//			if(tableInfo)
//				delete tableInfo;
//		}
//
//
//		PMReal colWidth = (box.Right()- box.Left())/numCols;
//		//PMReal rowHight = (box.Bottom()- box.Top())/numRows;
//		//CA("create Table");
//		this->CreateTable(textStoryUIDRef, start, numRows, numCols,	20,	colWidth);
//		
//	}while(false);
//	//CA("Before return");
//	return ;
//}

ErrorCode SPSelectionObserver::CreateTable(const UIDRef& storyRef, 
											const TextIndex at,
											const int32 numRows,
											const int32 numCols,
											const PMReal rowHeight,
											const PMReal colWidth,
											const CellType cellType)
{
	ErrorCode status = kFailure;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CA("ptrIAppFramework is nil");		
		return status;
	}
	do {
		InterfacePtr<ITextModel> textModel(storyRef, UseDefaultIID());
		ASSERT(textModel);
		if(textModel == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::CreateTable::textModel == nil");		
			break;
		}

		// 1026750: Instead of processing kNewTableCmdBoss use the ITableUtils facade.
		Utils<ITableUtils> tableUtils;
		if (!tableUtils) 
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::CreateTable::!tableUtils");
			break;
		}
		tableUtils->InsertTable (textModel, at, 0,
									 numRows, numCols,
									 rowHeight, colWidth,
									 cellType,
									 ITableUtils::eSetSelectionInFirstCell);
		status = kSuccess;

	} while (false);

	return status;

}

void SPSelectionObserver::resizeTextFrame(UIDRef itemRef,UIDList list, PMReal bottomX, PMReal bottomY)
{
/*	InterfacePtr<ISpecifier> spec(::QueryActiveSpecifier());
	if(!spec)
		return 0;
*/
	//CA("resizeItem");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CA("ptrIAppFramework is nil");	
		return ;
	}

	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
	if(!iSelectionManager)
	{
		ptrIAppFramework->LogDebug("SPSelectionObserver::resizeTextFrame::!iSelectionManager");
		return;
	}
	InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
	( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
	
	/*ISpecifier *spec;
	txtMisSuite->GetCurrentSpecifier(spec);*/  ///*****Commented by Sachin Sharma
	
	//PBPMPoint referencePt=this->GetActiveReferencePoint();
	 PMRect  maxBounds;
	bool16 res = getMaxLimitsOfBoxes(list,maxBounds);
	PMString ref("Reference::");
	ref.AppendNumber(maxBounds.Left());
	ref.Append("::::");
	ref.AppendNumber(maxBounds.Top());
	ref.Append("::::");
	ref.AppendNumber(maxBounds.Right());
	ref.Append("::::");
	ref.AppendNumber(maxBounds.Bottom());
	//CA(ref);
	PBPMPoint referencePt(maxBounds.Left(),maxBounds.Top());
	PMReal xFactor = bottomX;
	PMReal yFactor = bottomY;

	InterfacePtr<ICommand> cmd(CmdUtils::CreateCommand(kResizeItemsCmdBoss));
	if(!cmd)
	{
		ptrIAppFramework->LogDebug("SPSelectionObserver::resizeTextFrame::!icmd");	
		return ;
	}
///****Commented By Sachin Sharma From Here
	InterfacePtr<IResizeItemsCmdData> data(cmd, IID_IRESIZEITEMSCMDDATA);
	if(!data)
	{	
		ptrIAppFramework->LogDebug("SPSelectionObserver::resizeTextFrame::!data");	
		return ;
	}


	InterfacePtr<IBoundsData> bounds(cmd, IID_IBOUNDSDATA);
	if(!bounds)
	{
		ptrIAppFramework->LogDebug("SPSelectionObserver::resizeTextFrame::!ibounds");	
		return ;
	}

	////////PageItemUtils::SetDefaultBoundsData(spec, bounds); ///***Commented By Sachin Sharma

	cmd->SetItemList(UIDList(itemRef));

    ///////////  for resizing according to coordinates getting through whiteboard  /// from here
    Transform::CoordinateSpace coordinateSpace = Transform::PasteboardCoordinates() ;
	Geometry::BoundsKind boundsKind = Geometry::OuterStrokeBounds();

	data->SetResizeData(coordinateSpace,boundsKind,referencePt,Geometry::ResizeTo( bottomX, bottomY));
    ///////////////////////////////////////////////////////////////////////////////// upto here
	ErrorCode result=CmdUtils::ProcessCommand(cmd);

	if(result==kFailure)
	{
	    CA("result==kFailure");
		ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLCommonFunctions::resizeItem::result == kFailure");	
		return ;
	}
	
	return ;


}

void SPSelectionObserver::deleteCommentsOnLayer()
{
	//int32 commentDataPtrVectorLength =static_cast<int32> (global_VectorAPpubCommentValue.size());
	//if(commentDataPtrVectorLength  > 0)
	//{

	//	IDocument* frontDocument = Utils<ILayoutUIUtils>()->GetFrontDocument(); 
	//	if(frontDocument == nil)
	//		return;

	//	IDataBase* database = ::GetDataBase(frontDocument);
	//	if(database==nil)
	//		return;

	//	InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)frontDocument,UseDefaultIID());
	//	if (iSpreadList==nil)
	//		return;

	//	UIDList allPageItems(database);

	//	for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
	//	{
	//		UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));

	//		InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
	//		if(!spread)
	//			return;

	//		int numPages=spread->GetNumPages();

	//		for(int i=0; i<numPages; i++)
	//		{
	//			UIDList tempList(database);
	//			spread->GetItemsOnPage(i, &tempList, kFalse);
	//			allPageItems.Append(tempList);
	//		}
	//	}

	//	/*PMString len("allPageItems.Length() = ");
	//	len.AppendNumber(allPageItems.Length());
	//	CA(len);*/

	//	int32 numOfCommentsToBeDeleted = static_cast<int32>(global_VectorAPpubCommentValue.size());
	//	for(int i=0;i<numOfCommentsToBeDeleted;i++)//iterate through an array of IDs to be deleted
	//	{
	//		APpubComment objAPpubComment = global_VectorAPpubCommentValue[i];
	//		int32 pubCommentIDToBeDelete = objAPpubComment.APpubCommentID;
	//		int32 parentIDToBeDelete = objAPpubComment.comment.id;
	//		
	//		/*PMString pubCommentIDToBeDeleteStr("pubCommentIDToBeDelete = ");
	//		pubCommentIDToBeDeleteStr.AppendNumber(pubCommentIDToBeDelete);
	//		CA(pubCommentIDToBeDeleteStr);*/
	//		
	//		for(int j=0; j<allPageItems.Length(); j++) //iterate throgh all frames present on current document
	//		{
	//			
	//			UIDRef boxID=allPageItems.GetRef(j);
	//			if(deleteSelectedCommentFromLayer(boxID,parentIDToBeDelete,pubCommentIDToBeDelete))
	//			{
	//				global_VectorAPpubCommentValue[i].isCommentPresentOnDocument = kFalse;
	//				allPageItems.Remove(j);
	//			}
	//		}
	//	}
	//}
	return ;
}

bool16 SPSelectionObserver::deleteSelectedCommentFromLayer(UIDRef& boxID,int32 parentIDToBeDelete,int32 pubCommentIDToBeDelete)
{
//	InterfacePtr<IPMUnknown> unknown(boxID, IID_IUNKNOWN);
//
//	UID frameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
//
//	if(frameUID == kInvalidUID)
//	{
//		
//		InterfacePtr<IXMLReferenceData> objXMLRefDat(Utils<IXMLUtils>()->QueryXMLReferenceData(unknown));
//		if (objXMLRefDat == nil)
//			return kFalse;
//
//		XMLReference xmlRef = objXMLRefDat->GetReference();
//
//		UIDRef refUID = xmlRef.GetUIDRef();
//
//		//IIDXMLElement *xmlElement = xmlRef.Instantiate();
//		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
//		if(xmlElement == nil)
//			return kFalse;
//
//		PMString attribName("parentID");
//		PMString attribVal=xmlElement->GetAttributeValue(WideString(attribName)); //Cs4
//		
//		if(pubCommentIDToBeDelete == attribVal.GetAsNumber())
//		{
//			InterfacePtr<ICommand> deleteCmd(CmdUtils::CreateCommand(kDeleteCmdBoss));
//			if (!deleteCmd) 
//				return kFalse;
//
//			deleteCmd->SetItemList(UIDList(boxID));
//			CmdUtils::ProcessCommand(deleteCmd);
//
//			ErrorCode errCode = Utils<IXMLElementCommands>()->DeleteElement(xmlRef, kTrue);
//			if (errCode != kSuccess)
//				return kFalse;
//			else
//			{
//				return kTrue;
//			}
//		}
//	}
//	else 
//	{
//		
//		//---------------------CS3 TextFrame change---------------------------//
//		/*InterfacePtr<ITextFrame> textFrame(boxID.GetDataBase(), frameUID, ITextFrame::kDefaultIID);
//		if (textFrame == nil)
//			return kFalse;
//		
//		InterfacePtr<ITextModel> objTxtMdl (textFrame->QueryTextModel());
//		if (objTxtMdl == nil)
//           	return kFalse;*/
//		
//
//		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxID, UseDefaultIID());
//		if (graphicFrameHierarchy == nil) 
//		{
//			//CA("graphicFrameHierarchy is NULL");
//			//flag=0;
//			return kFalse;
//		}
//						
//		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
//		if (!multiColumnItemHierarchy) {
//			//CA("multiColumnItemHierarchy is NULL");
//			//flag=0;
//			return kFalse;
//		}
//
//		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
//		if (!multiColumnItemTextFrame) {
//			//CA("multiColumnItemTextFrame is NULL");
//			flag=0;
//			return kFalse;
//		}
//		InterfacePtr<IHierarchy>
//		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
//		if (!frameItemHierarchy) {
//			//CA("frameItemHierarchy is NULL");
//			flag=0;
//			return kFalse;
//		}
//
//		InterfacePtr<ITextFrameColumn>
//		frameItemTFC(frameItemHierarchy, UseDefaultIID());
//		if (!frameItemTFC) {
//			//CA("!!ITextFrameColumn");
//			flag=0;
//			return kFalse;
//		}
//
//		InterfacePtr<ITextModel> objTxtMdl(frameItemTFC->QueryTextModel());
//		if(!objTxtMdl)
//		{
//			//CA("!textModel" );
//			flag=0;
//			return kFalse;
//		}
//
////------------------------CS3 Change---------------------------------------------------------------//
//
//		IXMLReferenceData* objXMLRefDat=(IXMLReferenceData*) objTxtMdl->QueryInterface(IID_IXMLREFERENCEDATA);
//		if (objXMLRefDat==nil)
//			return kFalse;
//
//		XMLReference xmlRef=objXMLRefDat->GetReference();
//
//		UIDRef refUID=xmlRef.GetUIDRef();		
//       	//IIDXMLElement *xmlElement=xmlRef.Instantiate();
//		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
//		if(xmlElement==nil)
//          	return kFalse;
//		
//		int32 childCount = xmlElement->GetChildCount();
//		if(childCount == 0)
//		{
//			//CA("childCount is zero");
//			return kFalse;
//		}
//
//		XMLReference elementXMLref=xmlElement->GetNthChild(0);
//
//		//IIDXMLElement * childElement=elementXMLref.Instantiate();
//		InterfacePtr<IIDXMLElement>childElement(elementXMLref.Instantiate());
//		if(childElement==nil)
//		return kFalse;
//		
//		PMString attribVal("");
//
//		int32 iDToBeDelete = -1;
//
//		PMString attribValIndex=childElement->GetAttributeValue(WideString("index"));  //Cs4
//		int32 index = attribValIndex.GetAsNumber();
//		if(index == 6)
//		{
//			iDToBeDelete = pubCommentIDToBeDelete;
//			attribVal=childElement->GetAttributeValue(WideString("ID")); //Cs4
//		}
//		else
//		{
//			iDToBeDelete = parentIDToBeDelete;
//			attribVal=childElement->GetAttributeValue(WideString("parentID")); //Cs4
//		}
//
//		if(iDToBeDelete == attribVal.GetAsNumber())
//		{
//			InterfacePtr<ICommand> deleteCmd(CmdUtils::CreateCommand(kDeleteCmdBoss));
//			if (!deleteCmd) 
//				return kFalse;
//
//			deleteCmd->SetItemList(UIDList(boxID));
//			CmdUtils::ProcessCommand(deleteCmd);
//
//			ErrorCode errCode = Utils<IXMLElementCommands>()->DeleteElement(xmlRef, kTrue);
//			if (errCode != kSuccess)
//				return kFalse;
//			else
//			{
//				return kTrue;
//			}
//		}
//	}
	return kFalse;
}

bool16 SPSelectionObserver::isTablePresent(const UIDRef& boxUIDRef, UIDRef& tableUIDRef, int32 tableNumber)
{
	bool16 returnValue=kFalse;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	return kFalse;
	do
	{
		InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);
		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
		if (textFrameUID == kInvalidUID)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::isTablePresent::textFrameUID == kInvalidUID");		
			break;
		}

		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			return kFalse;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			return kFalse;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			return kFalse;
		}
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			return kFalse;
		}

		InterfacePtr<ITextFrameColumn>
		frameItemTFC(frameItemHierarchy, UseDefaultIID());
		if (!frameItemTFC) {
			return kFalse;
		}

		InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
		if(!textModel)
		{
			return kFalse;
		}

		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		if(tableList==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::isTablePresent::tableList == nil");								
			break;
		}

		int32	tableIndex = tableList->GetModelCount() - 1;
		if(tableIndex<0)
		{
			break;
		}

		tableIndex=tableNumber-1;

		InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
		if(tableModel == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::TableUtility::isTablePresent::tableModel == nil");										
			break;
		}
		
		UIDRef tableRef(::GetUIDRef(tableModel));
		
		tableUIDRef=tableRef;

		returnValue=kTrue;
	}
	while(kFalse);

	return returnValue;
}


////// for search chetan
bool8 SPSelectionObserver::populateProductListforSectionforSearch (VectorObjectInfoPtr vectorObjectValuePtr, VectorItemModelPtr vectorItemModelPtr)
{
	//CA(__FUNCTION__);
	vectorForThumbNail.clear();
	bool8 isListBoxPopulated = kFalse;

	AcquireWaitCursor awc;
	awc.Animate(); 

	//InterfacePtr<ISPPRImageHelper> ptrImageHelper((static_cast<ISPPRImageHelper*> (CreateObject(kSPProductImageIFaceBoss ,ISPPRImageHelper::kDefaultIID))));
	//if(ptrImageHelper == nil)
	//{
	//	//CA("ProductImages plugin not found ");
	//	return kFalse;
	//}
	//ptrImageHelper->EmptyImageGrid();
	//imageVector2.clear();

	Mediator md;
	IPanelControlData* iPanelControlData=md.getMainPanelCtrlData();
	if(iPanelControlData == nil)
	{
		//CA("panelCntrlData is nil");
		return kFalse;
	}
////////////////////////////// for search
	IControlView* iPubGPCtrlView = iPanelControlData->FindWidget(kSPPublicationGPWidgetID);
	IControlView* iONEGPCtrlView = iPanelControlData->FindWidget(kSPONEGPWidgetID);
	IControlView* iSSPGPCtrlView = iPanelControlData->FindWidget(kSPSearchPnlWidgetID);

	if(searchResult)
	{
		iSSPGPCtrlView->ShowView();
		iPubGPCtrlView->HideView();
		iONEGPCtrlView->HideView();
	}
	else
	{
		iSSPGPCtrlView->HideView();
		iPubGPCtrlView->ShowView();
		iPubGPCtrlView->Enable();
		iONEGPCtrlView->ShowView();
	}
////////////////////////////// for search ends
	IControlView* listBox = iPanelControlData->FindWidget(kSPListBoxWrapperGroupPanelWidgetID);		
	//IControlView* thumb = iPanelControlData->FindWidget(kSPThumbPnlWidgetID);
	if(isThumb){
		listBox ->HideView ();		
		//thumb ->ShowView ();
	}
	else
	{
		//thumb->HideView ();
		listBox->ShowView();
		
	}

	do
	{
		Mediator md;
		IPanelControlData* iPanelControlData;
		PublicationNode pNode;

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			CAlert::InformationAlert("Err ptrIAppFramework is nil");
			break;
		}
		//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		iPanelControlData=md.getMainPanelCtrlData();
		if(!iPanelControlData)
		{
			//CA("Serious Error Again");	
			break;
		}
//CA("3");
		SDKListBoxHelper listHelperInsidePopulateProductListForSection(this, kSPPluginID);

		IControlView* lstboxControlView=
		listHelperInsidePopulateProductListForSection.FindCurrentListBox(iPanelControlData, 1);
		if(lstboxControlView==nil)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::populateProductListforSection::No lstboxControlView");
			break;
		}
		listHelperInsidePopulateProductListForSection.EmptyCurrentListBox(iPanelControlData, 1);

//		md.setSelectedPub(curSelSubecId);
//		md.setCurrSubSectionID(curSelSubecId);

		bool16 isONEsource = ptrIAppFramework->get_isONEsourceMode();


//		if(!isONEsource)
//		{
//			//CA("Publication Mode");	
//			//int32 ParentTypeID = ptrIAppFramework->TYPEMngr_getObjectTypeID("PRODUCT_LEVEL");			
//			VectorPubObjectValuePtr VectorFamilyInfoValuePtr = NULL;			
//			//VectorFamilyInfoValuePtr = ptrIAppFramework->PBObjMngr_getProductsForSubSection(curSelSubecId, ParentTypeID);
//
//			if(ListFlag == 0)
//			{
//				VectorFamilyInfoValuePtr = ptrIAppFramework->getProductsAndItemsForSection(curSelSubecId, global_lang_id );
//
//			}else if(ListFlag == 1)
//			{
//				VectorFamilyInfoValuePtr = ptrIAppFramework->getProductsForSubSection/*GetProjectProducts_getProductsBySubSection*/(curSelSubecId, global_lang_id );
//			}else if(ListFlag ==2)
//			{
//				VectorFamilyInfoValuePtr = ptrIAppFramework->getItemsForSubSection/*GetProjectProducts_getItemsForSubSection*/(curSelSubecId, global_lang_id );
//			}else if(ListFlag ==3)
//			{
//				VectorFamilyInfoValuePtr = ptrIAppFramework->GetProjectProducts_getHybridTablesForSubSection(curSelSubecId, global_lang_id );
//			}
//					
//			if(VectorFamilyInfoValuePtr == nil)
//			{
//				//CA(" VectorFamilyInfoValuePtr == NULL ");
//				if(md.getSprayButtonView()){ //CA("Disabling Spray Button.");
//					md.getSprayButtonView()->Disable();}
//				if(md.getAssignBtnView())
//						md.getAssignBtnView()->Disable();
//				/*if(md.getLocateWidgetView())			
//						md.getLocateWidgetView()->Disable();*///--COMMENTED ON 25 FEB 2006 --
//				if(md.getPreviewWidgetView())
//						md.getPreviewWidgetView()->Disable();
//				if(md.getSubSecSprayButtonView())
//					md.getSubSecSprayButtonView()->Disable();
//				if(md.getRefreshButtonView())
//					md.getRefreshButtonView()->Disable();
//				if(md.getIconView())
//					md.getIconView()->Disable();
//				break;
//			}
//
//			if(VectorFamilyInfoValuePtr->size()==0)
//			{
//				//CA("VectorFamilyInfoValuePtr->size()==0");
//				if(md.getSprayButtonView()){ //CA("Disabling Spray Button.");
//					md.getSprayButtonView()->Disable();}
//				if(md.getAssignBtnView())
//						md.getAssignBtnView()->Disable();
//				/*if(md.getLocateWidgetView())//--COMMENTED ON 25 FEB 2006 --
//						md.getLocateWidgetView()->Disable();*/
//				if(md.getPreviewWidgetView())
//						md.getPreviewWidgetView()->Disable();
//				if(md.getSubSecSprayButtonView())
//					md.getSubSecSprayButtonView()->Disable();
//				if(md.getRefreshButtonView())
//					md.getRefreshButtonView()->Disable();
//				if(md.getIconView())
//					md.getIconView()->Disable();
//				delete VectorFamilyInfoValuePtr;
//				break;
//			}
//		
//			int32 size = VectorFamilyInfoValuePtr->size();		
//			
//			/*PMString ASD("SIze : ");
//			ASD.AppendNumber(size);
//			CA(ASD);*/
//
//			VectorPubObjectValue::iterator it2;
//			int count=0;
//			for(it2 = VectorFamilyInfoValuePtr->begin(); it2 != VectorFamilyInfoValuePtr->end(); it2++)
//			{	
//					vectorForThumbNail.push_back(*it2);
//					pNode.setPBObjectID(it2->getPub_object_id());
//					pNode.setSequence(it2->getIndex());	
//					pNode.setIsProduct(it2->getisProduct());
//					pNode.setIsONEsource(kFalse);
//					pNode.setIsStarred(it2->getStarredFlag1());
//					int32 NewProductFlag =0;
//					//CA(it2->getName());
//					if(it2->getNew_product()  == kTrue){
//						NewProductFlag =2;
//						pNode.setNewProduct(1);
//					}
//					else{
//						NewProductFlag =1;
//						pNode.setNewProduct(0);
//					}
//
//					//Hybrid Table Change
//					if(it2->getisProduct() == 2)
//					{
//						//PMString ASD("getLevel_no : ");
//						//it2->getObjectValue().getName();				
//						//ASD.AppendNumber(it2->getObjectValue().getLevel_no());
//						//CA(it2->getObjectValue().getName());
//					/*	PMString ASD("ParentId : ");
//						ASD.AppendNumber(it2->getObjectValue().getParent_id());
//						CA(ASD);*/	
//						pNode.setLevel(it2->getObjectValue().getLevel_no());
//						pNode.setParentId(it2->getObjectValue().getParent_id());					
//						pNode.setPubId(it2->getObjectValue().getObject_id());
//
//						if(!iConverter)
//							pNode.setPublicationName(it2->getObjectValue().getName());
//						else
//							pNode.setPublicationName(iConverter->translateString(it2->getObjectValue().getName()));
//						
//						pNode.setChildCount(it2->getObjectValue().getChildCount());
//						pNode.setReferenceId(it2->getObjectValue().getRef_id());
//						pNode.setTypeId(it2->getObjectValue().getObject_type_id());									
//					}
//
//					if(it2->getisProduct() == 1)
//					{
//						//PMString ASD("getLevel_no : ");
//						//it2->getObjectValue().getName();				
//						//ASD.AppendNumber(it2->getObjectValue().getLevel_no());
//						//CA(it2->getObjectValue().getName());
//					/*	PMString ASD("ParentId : ");
//						ASD.AppendNumber(it2->getObjectValue().getParent_id());
//						CA(ASD);*/	
//						pNode.setLevel(it2->getObjectValue().getLevel_no());
//						pNode.setParentId(it2->getObjectValue().getParent_id());					
//						pNode.setPubId(it2->getObjectValue().getObject_id());
//
//						if(!iConverter)
//							pNode.setPublicationName(it2->getObjectValue().getName());
//					else
//							pNode.setPublicationName(iConverter->translateString(it2->getObjectValue().getName()));
//						
//						pNode.setChildCount(it2->getObjectValue().getChildCount());
//						pNode.setReferenceId(it2->getObjectValue().getRef_id());
//						pNode.setTypeId(it2->getObjectValue().getObject_type_id());									
//					}
//					if(it2->getisProduct() == 0)
//					{
//						pNode.setPubId(it2->getItemModel().getItemID());
//
//						if(!iConverter)
//						{
//							PMString ItemNO(it2->getItemModel().getItemNo());
//							PMString ItemDesp(it2->getItemModel().getItemDesc());
//							if(ItemDesp != "")
//							{
//								ItemNO.Append(": "+ ItemDesp);
//							}
//							pNode.setPublicationName(ItemNO);
//							//CA(ItemNO);
//						}
//						else
//						{
//							PMString ItemNO(iConverter->translateString(it2->getItemModel().getItemNo()));
//							PMString ItemDesp(iConverter->translateString(it2->getItemModel().getItemDesc()));
//							if(ItemDesp != "")
//							{
//								ItemNO.Append(": "+ ItemDesp);
//							}
//							pNode.setPublicationName(ItemNO);	
//							//CA(ItemNO);
//						}
//						pNode.setTypeId(it2->getobject_type_id());
//
//					}
//					int icon_count = GetIconCountForUnfilteredProductList(it2);
//					pNodeDataList.push_back(pNode);
//					
//					int32 StarFlag =1;
//					if(it2->getStarredFlag1()) // if Selected Green Star
//						StarFlag =2;
////CA("Before showImageAsThumbnail");
//
//				if(!isThumb)
//				{
//					listHelperInsidePopulateProductListForSection.AddElement(lstboxControlView , pNode.getName(), kSPTextWidgetID, count );				
//					listHelperInsidePopulateProductListForSection.SetDesignerAction(lstboxControlView, count, it2->getisProduct() ,icon_count, StarFlag,NewProductFlag);
//					count++;
//				}
//				else
//				{
//					showImageAsThumbnail(pNode, curSelSubecId);
//
//				}								
//			}	
//		}

		{  // For ONEsource Mode
			//CA("ONEsource Mode");
//			CurrentSelectedSection = curSelSubecId;
//			CurrentSelectedSubSection = curSelSubecId;
//			VectorObjectInfoPtr vectorObjectValuePtr = NULL;
//			VectorItemModelPtr vectorItemModelPtr = NULL;

//			if(ListFlag == 0)
//			{
//				vectorObjectValuePtr = ptrIAppFramework->GetONEsourceObjects_getONEsourceProductsByClassId(curSelSubecId,global_lang_id);
//				vectorItemModelPtr =  ptrIAppFramework->GetONEsourceObjects_getONEsourceItemsByClassId(curSelSubecId,global_lang_id);
				
//			}else if(ListFlag == 1)
//			{
//				vectorObjectValuePtr = ptrIAppFramework->GetONEsourceObjects_getONEsourceProductsByClassId(curSelSubecId,global_lang_id);
//			}else if(ListFlag ==2)
//			{
//				vectorItemModelPtr =  ptrIAppFramework->GetONEsourceObjects_getONEsourceItemsByClassId(curSelSubecId,global_lang_id);
//			}
			
			bool16 QuitFlag = kFalse;

			do
			{
				if(ListFlag == 0)
				{					
					if((vectorObjectValuePtr == NULL) && (vectorItemModelPtr == NULL))
					{
						
						QuitFlag = kTrue;
						break;
					}	
					
					if(vectorItemModelPtr != NULL)
					{						
						if((vectorObjectValuePtr == NULL) && (vectorItemModelPtr->size() == 0))
						{
							QuitFlag = kTrue;
							break;
						}
					}				
					if(vectorObjectValuePtr !=NULL)
					{
						if((vectorObjectValuePtr->size() == 0) && (vectorItemModelPtr == NULL))
						{
							QuitFlag = kTrue;
							break;
						}
					}
				}
				else if((ListFlag == 1))
				{
					if(vectorObjectValuePtr == NULL)
					{
						QuitFlag = kTrue;
						break;
					}
					if((vectorObjectValuePtr->size() == 0))
						{
						QuitFlag = kTrue;
						break;
					}
				}
				else if((ListFlag == 2))
				{
					if(vectorItemModelPtr == NULL)
					{
						QuitFlag = kTrue;
						break;
					}
					if((vectorItemModelPtr->size() == 0))
						{
						QuitFlag = kTrue;
						break;
					}
				}
			}while(0);
					
			if(QuitFlag)
			{
				//CA(" QuitFlag True");
				if(md.getSprayButtonView()){ //CA("Disabling Spray Button.");
					md.getSprayButtonView()->Disable();}
				if(md.getAssignBtnView())
						md.getAssignBtnView()->Disable();
				if(md.getPreviewWidgetView())
						md.getPreviewWidgetView()->Disable();
				if(md.getSubSecSprayButtonView())
					md.getSubSecSprayButtonView()->Disable();
				if(md.getRefreshButtonView())
					md.getRefreshButtonView()->Disable();
				if(md.getIconView())
					md.getIconView()->Disable();
				if(md.getIconViewNew())
					md.getIconViewNew()->Disable();
				ptrIAppFramework->LogInfo("AP46_ProductFinder::SPSelectionObserver::populateProductListforSection::QuitFlag");
				break;
			}
			int count=0;
			if((ListFlag == 0) || (ListFlag == 1) )
			{
				if(vectorObjectValuePtr != NULL)
				{
					int32 size =static_cast<int32> (vectorObjectValuePtr->size());		
					if(size > 0)
					{	
						VectorObjectInfoValue::iterator it2;			
						PMString itemCountString("");
						int32 itemCountNum =0;
						
						for(it2 = vectorObjectValuePtr->begin(); it2 != vectorObjectValuePtr->end(); it2++)
						{
							CPbObjectValue obj;
							CObjectValue cObjectValuesObj;

							obj.setPub_object_id (it2->getclass_id());
							//obj.setIndex(it2->getseq_order());
							obj.setisProduct(1);

							//cObjectValuesObj.setLevel_no(it2->getLevel_no());
							cObjectValuesObj.setParent_id(it2->getParent_id());
							cObjectValuesObj.setObject_id(it2->getObject_id());
							cObjectValuesObj.setName(it2->getName());
							cObjectValuesObj.setChildCount(it2->getChildCount());
							//cObjectValuesObj.setRef_id(it2->getRef_id());
							//cObjectValuesObj.setObject_type_id(it2->getObject_type_id());

							obj.setObjectValue(cObjectValuesObj);

							pNode.setPBObjectID(/*curSelSubecId*/it2->getclass_id());
							//pNode.setSequence(it2->getseq_order());	
							pNode.setIsProduct(1);
							pNode.setIsONEsource(kTrue);						
												
							//pNode.setLevel(it2->getLevel_no());
							pNode.setParentId(it2->getclass_id());					
							pNode.setPubId(it2->getObject_id());

							//if(!iConverter)
                            PMString dummyString = it2->getName();
								pNode.setPublicationName(dummyString);
							//else
							//	pNode.setPublicationName(iConverter->translateString(it2->getName()));
							
							pNode.setChildCount(it2->getChildCount());
							//pNode.setReferenceId(it2->getRef_id());
							pNode.setTypeId(it2->getObject_type_id());
							pNode.setPublicationID(it2->getclass_id());
							pNode.setSectionID(it2->getclass_id());
							pNode.setSubSectionID(it2->getclass_id());
						
							//PMString ASD("getObject_type_id : ");
							//ASD.AppendNumber(pNode.getTypeId());
							//CA(ASD);							
						
							int icon_count = 111; //GetIconCountForUnfilteredProductList(it2);
							pNodeDataList.push_back(pNode);

							vectorForThumbNail.push_back(obj);

							if(!isThumb)
							{
								if(itemCountNum  >1)
								{
									itemCountString.Append("(");
									itemCountString.AppendNumber(itemCountNum);
									itemCountString.Append(")");
								}
								else
									itemCountString.Append("");
								//CA("13475");
                                PMString dummyString = pNode.getName();
								listHelperInsidePopulateProductListForSection.AddElement(lstboxControlView , dummyString, kSPTextWidgetID, count );
								listHelperInsidePopulateProductListForSection.SetDesignerAction(lstboxControlView, count, kTrue,icon_count, 0,0,itemCountString);//****
								count++;																// 0,0 is set to hide Star & New Product icon in ONEsource Mode.
								itemCountString.Clear();
								itemCountNum =0;
							}
							else
							{
								double currentPublicationID = pNode.getPublicationID();
								showImageAsThumbnail(pNode, currentPublicationID );
							}

						}	
					}
				}
			}
			if((ListFlag == 0) || (ListFlag == 2) )
			{
				if(vectorItemModelPtr != NULL)
				{
					int32 size =static_cast<int32> (vectorItemModelPtr->size());		
					if(size > 0)
					{
						//CA("vectorItemModelPtr");
						VectorItemModelValue::iterator it2;

						PMString itemCountString("");
						int32 itemCountNum =0;
						
						//int count=0;
						for(it2 = vectorItemModelPtr->begin(); it2 != vectorItemModelPtr->end(); it2++)
						{
							CPbObjectValue obj;
							CItemModel cItemModelsobj;

							obj.setPub_object_id (it2->getClassID());
							obj.setIndex(1);
							obj.setisProduct(0);
							//obj.setobject_type_id(ptrIAppFramework->TYPECACHE_getTypeByCode("ITEM_LEVEL"));

//							cItemModelsobj.setItemID(it2->getItemID());
							cItemModelsobj.setClassID(it2->getClassID());
							cItemModelsobj.setItemID(it2->getItemID());
							cItemModelsobj.setItemNo(it2->getItemNo());
							cItemModelsobj.setItemDesc(it2->getItemDesc());

							obj.setItemModel(cItemModelsobj);
							pNode.setPBObjectID(/*curSelSubecId*/it2->getClassID());
							pNode.setSequence(1);	
							pNode.setIsProduct(0);
							pNode.setIsONEsource(kTrue);
							pNode.setParentId(it2->getClassID());												
							pNode.setPubId(it2->getItemID());
							pNode.setPublicationID(it2->getClassID());
							pNode.setSectionID(it2->getClassID());
							pNode.setSubSectionID(it2->getClassID());

							//if(!iConverter)
							{
								PMString ItemNO(it2->getItemNo());
								PMString ItemDesp(it2->getItemDesc());
								if(ItemDesp != "")
								{
									ItemNO.Append(": "+ ItemDesp);
								}
								pNode.setPublicationName(ItemNO);
								//CA(ItemNO);
							}
							//else
							//{
							//	PMString ItemNO(iConverter->translateString(it2->getItemNo()));
							//	PMString ItemDesp(iConverter->translateString(it2->getItemDesc()));
							//	if(ItemDesp != "")
							//	{
							//		ItemNO.Append(": "+ ItemDesp);
							//	}
							//	//CA(ItemNO);
							//	pNode.setPublicationName(ItemNO);						
							//}

							pNode.setTypeId(it2->getParent_type_id());
							
							int icon_count = 111; //GetIconCountForUnfilteredProductList(it2);
							pNodeDataList.push_back(pNode);
							
							vectorForThumbNail.push_back(obj);  // Chetan --

							if(!isThumb)
							{
								if(itemCountNum  >1)
								{
									itemCountString.Append("(");
									itemCountString.AppendNumber(itemCountNum);
									itemCountString.Append(")");
								}
								else
									itemCountString.Append("");
                                
                                PMString dummyString = pNode.getName();
								listHelperInsidePopulateProductListForSection.AddElement(lstboxControlView , dummyString, kSPTextWidgetID, count );				
								listHelperInsidePopulateProductListForSection.SetDesignerAction(lstboxControlView, count, kFalse,icon_count, 0,0,itemCountString); //*****
								count++;																// 0,0 is set to hide Star & New Product icon in ONEsource Mode.
								itemCountString.Clear();
								itemCountNum =0;
							}
							else
							{
								double currentPublicationID = pNode.getPublicationID();
								showImageAsThumbnail(pNode, currentPublicationID);
							}													// 0,0 is set to hide Star & New Product icon in ONEsource Mode.
						}	
					}
				}
			}

		}

		InterfacePtr<IListControlData>listcontrol(lstboxControlView,UseDefaultIID());
		if(!listcontrol)
		{
			//CA("No list control");
			break;
		}
		if(listcontrol->Length()>0 /*|| imageVector2.size()>0*/)
		{//CA("Inside if");
			isListBoxPopulated = kTrue;
			/*
				added on 15 feb 2006.....
			*/
			InterfacePtr<IListBoxController> listStringControlData(lstboxControlView,UseDefaultIID());
			if(listStringControlData)
			{
				//CA("InterfacePtr<IListBoxController>");
				//listStringControlData->SetClickItem(0);
				listStringControlData->Select(0);
				CurrentSelectedProductRow = 0; // First Product of List

				VectorPubObjectValue::iterator it2;			
				
				it2 = vectorForThumbNail.begin();
				int32 iconCount = GetIconCountForUnfilteredProductList (it2);

				if(ptrIAppFramework->get_isONEsourceMode()){
					iconCount = 0;
				}
				PMString str("");

				int32 isProduct = 0;
				if(it2->getisProduct() == 2)
				{
					isProduct = 2;
					//if(!iConverter)
						str = it2->getObjectValue().getName();
					//else
					//	str = iConverter->translateString(it2->getObjectValue().getName());
	
				}
				if(it2->getisProduct() == 1)
				{
					isProduct = 1;
					//if(!iConverter)
						str = it2->/*getObjectValue().*/getName();
					//else
					//	str = iConverter->translateString(it2->/*getObjectValue().*/getName());
	
				}
				if(it2->getisProduct() == 0)
				{
					isProduct = 0;
					//if(!iConverter)
					{
						PMString ItemNO(it2->getItemModel().getItemNo());
						PMString ItemDesp(it2->getItemModel().getItemDesc());
						if(ItemDesp != "")
						{
							ItemNO.Append(": "+ ItemDesp);
						}
						str = ItemNO;
					}

				}

				int32 NewProductFlag =0;				
				if(ptrIAppFramework->get_isONEsourceMode()){
					NewProductFlag = 0;
				}
				else if(it2->getNew_product()  == kTrue){
					NewProductFlag =2;
				}
				else{
					NewProductFlag =1;
				}

				int32 StarFlag =0;
				if(ptrIAppFramework->get_isONEsourceMode()){
					StarFlag = 0;
				}
				else if(it2->getStarredFlag1()== kTrue) // if Selected Green Star
					StarFlag =2;
				else
					StarFlag = 1;

				//SPXLibraryItemGridEH spXLibraryItemGridEHObj(this);
				//spXLibraryItemGridEHObj.SetDesignerActionThumbnail (iPanelControlData, str, isProduct, iconCount, StarFlag, NewProductFlag);

				//IControlView* gridWidget = iPanelControlData->FindWidget( kSPXLibraryItemGridWidgetId );
				//InterfacePtr<ISPXLibraryViewController> gridController( gridWidget,IID_ISPXLIBRARYVIEWCONTROLLER );
	
				//if ( gridController == NULL )
				//{
				//	//TRACEFLOW("toms"," \001\004NO IXLibraryViewController\001\004\n");
				//	return kFalse;
				//}
				//gridController->Select(0);

//////// Chetan --
				IControlView *view1 = iPanelControlData->FindWidget(kSPRefreshWidgetID);
				if(view1)
				{
					view1->Enable();
				}

				IControlView *view2 = iPanelControlData->FindWidget(kSPSubSectionSprayButtonWidgetID);
				if(view2)
				{
					view2->Enable();
				}

				/*IControlView *view3 = iPanelControlData->FindWidget(kAssignButtonWidgetID);
				if(view3)
				{
					view3->Enable();
				}*/

				IControlView *view4 = iPanelControlData->FindWidget(kPRLPreviewButtonWidgetID);
				if(view4)
				{   
					view4->Enable();
				}

				/*IControlView *view5 = iPanelControlData->FindWidget(kLocateButtonWidgetID);
				if(view5) //--COMMENTED ON 25 FEB 2006 --
				{	
					view5->Enable();
				}*/

				IControlView *view6 = iPanelControlData->FindWidget(kSPSprayButtonWidgetID);
				if(view6)
				{	
					view6->Enable();
				}

				//InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
				//if(!DataSprayerPtr)
				//{
				//	ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::populateProductListforSection::No DataSprayerPtr");
				//	break;
				//}

				md.setCurrentObjectID(pNodeDataList[CurrentSelectedProductRow].getPubId());
				md.setPBObjectID(pNodeDataList[CurrentSelectedProductRow].getPBObjectID());
				/*PMString objid("The selected object id is ");
				objid.AppendNumber(pNodeDataList[CurrentSelectedProductRow].getPubId());*/
				//CA(objid);
				
				//DataSprayerPtr->FillPnodeStruct(pNodeDataList[CurrentSelectedProductRow],pNodeDataList[CurrentSelectedProductRow].getSectionID(), pNodeDataList[CurrentSelectedProductRow].getPublicationID(), pNodeDataList[CurrentSelectedProductRow].getSubSectionID());
			
				//DataSprayerPtr->setFlow(isItemHorizontalFlow);
			
			}
		}
		else
		{
			//CA("isListBoxPopulated =  kFalse");
			isListBoxPopulated =  kFalse;
			if(md.getSprayButtonView()){ //CA("Disabling Spray Button.");
				md.getSprayButtonView()->Disable();}
			/*if(md.getAssignBtnView())
					md.getAssignBtnView()->Disable();*/
			/*if(md.getLocateWidgetView())
					md.getLocateWidgetView()->Disable();*/
			if(md.getPreviewWidgetView())
					md.getPreviewWidgetView()->Disable();
			if(md.getSubSecSprayButtonView())
				md.getSubSecSprayButtonView()->Disable();
			if(md.getRefreshButtonView())			
				md.getRefreshButtonView()->Enable();
			//md.getRefreshButtonView()->Disable();
		}

	}while(kFalse);

	//CA("returning populateProductListforSection()");
	return isListBoxPopulated;
}

////// for search chetan ends
