//
//  CTGSelectionObserver.cpp
//  APJS10_CatsyTagger
//
//  Created by Rahul Dalvi on 04.03.18.
//
//

#include "CTGSelectionObserver.h"
#include "IDropDownListController.h"
#include "IStringListControlData.h"
#include "ITextControlData.h"
#include "MediatorClass.h"
#include "IWidgetParent.h"
#include "ISubject.h"
#include "IApplication.h"
#include "IPanelMgr.h"
#include "CTGActionComponent.h"
#include "IClientOptions.h"
#include "IAppFramework.h"
#include "SectionData.h"
#include "AcquireModalCursor.h"
#include "CTGTreeDataCache.h"
#include "ITreeViewMgr.h"
#include "CTGTreeModel.h"
#include "ILoginHelper.h"

PublicationNodeList pNodeDataList;
extern CTGTreeDataCache dc;
int32 CurrentSelectedProductRow = -100;

CREATE_PMINTERFACE(CTGSelectionObserver, kCTGSelectionObserverImpl);

CTGSelectionObserver::CTGSelectionObserver(IPMUnknown *boss) :
ActiveSelectionObserver(boss){}

CTGSelectionObserver::~CTGSelectionObserver(){}

void CTGSelectionObserver::AutoAttach()
{
    ActiveSelectionObserver::AutoAttach();
    
    InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
    if(ptrIAppFramework == nil)
        return;
    
    Mediator md;
    md.setLoadDataFlag(kTrue);
    bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
    
    
    InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
    if(iPanelControlData==nil)
    {
        //CA("iPanelControlData==nil");
        ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::AutoAttach::No iPanelControlData");
        return;
    }
    else
    {
        //md.setMainPanelCntrlData(iPanelControlData);
        //AttachWidget(iPanelControlData, kSPSubSectionSprayButtonWidgetID, IID_ITRISTATECONTROLDATA);
        AttachWidget(iPanelControlData, kCTGSectionDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
        AttachWidget(iPanelControlData, kCTGItemFieldsDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
        
        //AttachWidget(iPanelControlData, kSPTemplatePreviewWidgetID, IID_ITRISTATECONTROLDATA);
    }
    
    if(!isUserLoggedIn)
    {
        
        do{
            
            IControlView* sectionDropListCtrlView=iPanelControlData->FindWidget(kCTGSectionDropDownWidgetID);
            if(sectionDropListCtrlView==nil)
            {
                ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGSelectionObserver::AutoAttach::No sectionDropListCtrlView");
                break;
            }
            
            InterfacePtr<IDropDownListController> sectionDropListCntrler(sectionDropListCtrlView, UseDefaultIID());
            if(sectionDropListCntrler==nil)
            {
                ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGSelectionObserver::AutoAttach::::No sectionDropListCntrler");
                break;
            }
            
            InterfacePtr<IStringListControlData> sectionDropListCtrlData(sectionDropListCntrler, UseDefaultIID());
            if(sectionDropListCtrlData==nil)
            {
                ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGSelectionObserver::AutoAttach::::No sectionDropListCtrlData");
                break;
            }
            
            sectionDropListCtrlData->Clear(kFalse, kFalse);
            
            
        }while(kFalse);
        
    }
    loadPaletteData();
    /*
    do
    {
        if(md.getMainPanelCtrlData()!=nil && Flag1==0)
        {
            if(isUserLoggedIn)
            {// CA("Before loadpalletedata ");
                loadPaletteData();
            }
            
        }
        else
        {
            //check=1;
            Flag1=0;
            loadPaletteData();
        }
    }while(kFalse);
    */
}

void CTGSelectionObserver::AutoDetach()
{
    Mediator md;
    md.setLoadDataFlag(kFalse);

    ActiveSelectionObserver::AutoDetach();
    if(!md.getMainPanelCtrlData())
    {
        //	CA("AutoDetach:SelectionObserver:md.getMainPanelControlData is nil");
        return;
    }
    
    //CA("Detach 0");
    DetachWidget(md.getMainPanelCtrlData(), kCTGSectionDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
    DetachWidget(md.getMainPanelCtrlData(), kCTGItemFieldsDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
    //DetachWidget(md.getMainPanelCtrlData(), kSPSubSectionDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
    
    /*
    DetachWidget(md.getMainPanelCtrlData(), kSPSprayButtonWidgetID, IID_ITRISTATECONTROLDATA);
    //DetachWidget(md.getMainPanelCtrlData(), kAssignButtonWidgetID, IID_ITRISTATECONTROLDATA);
    DetachWidget(md.getMainPanelCtrlData(), kSPSectionDropDown_1WidgetID, IID_ISTRINGLISTCONTROLDATA);
    DetachWidget(md.getMainPanelCtrlData(), kSPSetDesignerMilestoneWidgetID,IID_ITRISTATECONTROLDATA); //IID_IBOOLEANCONTROLDATA);
    DetachWidget(md.getMainPanelCtrlData(),	kPRLPreviewButtonWidgetID, IID_ITRISTATECONTROLDATA);
    DetachWidget(md.getMainPanelCtrlData(), kGreenFilterButtonWidgetID,IID_ITRISTATECONTROLDATA);
    //DetachWidget(md.getMainPanelCtrlData(), kClearButtonWidgetID, IID_ITRISTATECONTROLDATA);
    DetachWidget(md.getMainPanelCtrlData(), kSPShowWhiteBoardWidgetID, IID_ITRISTATECONTROLDATA);
    DetachWidget(md.getMainPanelCtrlData(), kSPRefreshWidgetID, IID_ITRISTATECONTROLDATA);
    DetachWidget(md.getMainPanelCtrlData(), kSPShowPubTreeWidgetID, IID_ITRISTATECONTROLDATA);
    DetachWidget(md.getMainPanelCtrlData(), kSPSubSectionSprayButtonWidgetID, IID_ITRISTATECONTROLDATA);
    DetachWidget(md.getMainPanelCtrlData(),	kSPSelectClassWidgetID, IID_ITRISTATECONTROLDATA);
    //	DetachWidget(md.getMainPanelCtrlData(),	kSPViewWhiteBoardWidgetID, IID_ITRISTATECONTROLDATA);
    DetachWidget(md.getMainPanelCtrlData(),	kSPRevertSearchWidgetID, IID_ITRISTATECONTROLDATA);
    
    //			DetachWidget(md.getMainPanelCtrlData(),kHorizontalFlowCheckBoxWidgetID,IID_ITRISTATECONTROLDATA); //----
    //			DetachWidget(md.getMainPanelCtrlData(),kSprayItemPerFrameCheckBoxWidgetID,IID_ITRISTATECONTROLDATA);
    DetachWidget(md.getMainPanelCtrlData(),kNewSprayItemPerFrameCheckBoxWidgetID,IID_ITRISTATECONTROLDATA);
    DetachWidget(md.getMainPanelCtrlData(),kNewHorizontalFlowCheckBoxWidgetID,IID_ITRISTATECONTROLDATA);
    DetachWidget(md.getMainPanelCtrlData(), kSPShowTableSourceWidgetID, IID_ITRISTATECONTROLDATA);
    DetachWidget(md.getMainPanelCtrlData(), kSPSelectCustomerWidgetID, IID_ITRISTATECONTROLDATA);
    DetachWidget(md.getMainPanelCtrlData(),	kSPsearchWidgetID, IID_ITRISTATECONTROLDATA);
    //DetachWidget(md.getMainPanelCtrlData(), kSPTemplatePreviewWidgetID, IID_ITRISTATECONTROLDATA);
    */
    
}

IPanelControlData* CTGSelectionObserver::QueryPanelControlData()
{
    //CA("Inside QueryPanelControlData");
    IPanelControlData* iPanel = nil;
    do
    {
        InterfacePtr<IWidgetParent> iWidgetParent(this, UseDefaultIID());
        if (iWidgetParent == nil)
        {
            //PMString str(kSPBlankStringKey);
            //CA(str);
            break;
        }
        //CA("Didnt Break");
        InterfacePtr<IPanelControlData> iPanelControlData(iWidgetParent->GetParent(), UseDefaultIID());
        if (iPanelControlData == nil)
            break;
        iPanelControlData->AddRef();
        iPanel = iPanelControlData;
    }
    while (false); 
    //CA("end of  QueryPanelControlData");
    return iPanel;
}

void CTGSelectionObserver::AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
    do
    {
        IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
        if (iControlView == nil)
            break;
        InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
        if (iSubject == nil)
            break;
        iSubject->AttachObserver(this, interfaceID);
    }
    while (false);
}

void CTGSelectionObserver::DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
    do
    {
        IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
        if (iControlView == nil)
            break;
        InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
        if (iSubject == nil)
            break;
        iSubject->DetachObserver(this, interfaceID);
    }
    while (false); 
}

void CTGSelectionObserver::Update
(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy)
{
    //CA("selection observer Update");
    ActiveSelectionObserver::Update(theChange, theSubject, protocol, changedBy);
    
    InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
    if(iPanelControlData == nil)
    {
        return;
    }
    
    InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
    if(ptrIAppFramework == nil)
    {
        //CA(" ptrIAppFramework nil ");
        return;
    }
    
    do
    {
        InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
        if (controlView == nil)
        {
            //CA("controlView == nil");
            return;
        }
        WidgetID theSelectedWidget = controlView->GetWidgetID();
        
        if(theSelectedWidget==kCTGSectionDropDownWidgetID && theChange==kPopupChangeStateMessage)
        {
            //CA("SubsectionDropDown");
            bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
            if(!isUserLoggedIn)
                return;
            
            Mediator md;
            /*
            if(md.getRefreshButtonView() == nil)
            {
                //CA("refresh button view is nil");
            }
            else
            {
                //CA("refresh button enabled");
                md.getRefreshButtonView()->Enable();
            }
            */
            
            PMString sectionName("");
            PMString attributeName("");
            
            double sectionId = getSelectedSectionId(controlView, sectionName);
            md.sectionID = sectionId;
            bool16 isListBoxPopulated = kFalse;
            bool16 isAssetAttribute = kFalse;
            double attributeId = -1;
            
            attributeId = getSelectedAttributeId(md.getAttributeDropDownView(), attributeName, isAssetAttribute);
        
            if(sectionId>0 && attributeId > 0)
            {
                 isListBoxPopulated = this->populateTree(sectionId, attributeId, isAssetAttribute);
            }
            else
            {
                

                
            }
            
            /*
            if(isListBoxPopulated)
            {
                
                if(md.getRefreshButtonView() == nil)
                {
                }
                else
                    md.getRefreshButtonView()->Enable();
                //md.getSubSecSprayButtonView()->Enable();
                
                //CA("isListBoxPopulated 4");
            }
            else
            {
                if(md.getRefreshButtonView() == nil)
                {
                    //CA("md.getRefreshButtonView()==nil");
                }
                else
                    md.getRefreshButtonView()->Enable();
                
            }
            */
            
            break ;
        }
        else if(theSelectedWidget==kCTGItemFieldsDropDownWidgetID && theChange==kPopupChangeStateMessage)
        {
            //CA("SubsectionDropDown");
            bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
            if(!isUserLoggedIn)
                return;
            
            Mediator md;
            /*
             if(md.getRefreshButtonView() == nil)
             {
             //CA("refresh button view is nil");
             }
             else
             {
             //CA("refresh button enabled");
             md.getRefreshButtonView()->Enable();
             }
             */
            
            PMString sectionName("");
            PMString attributeName("");
            bool16 isAssetAttribute = kFalse;
            
            double attributeId = getSelectedAttributeId(controlView, attributeName , isAssetAttribute);
            md.attributeID = attributeId;
            
            
            bool16 isListBoxPopulated = kFalse;
            
            if(attributeId > 0 && md.sectionID > 0)
            {
                isListBoxPopulated = this->populateTree(md.sectionID, attributeId, isAssetAttribute);
            }
            else
            {
            
            }
            
            break ;
        }
        
        
    }while (kFalse);
}

void CTGSelectionObserver::loadPaletteData()
{
    //CA(__FUNCTION__);
    
    Mediator md;
    
    int32 level;
    InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
    if(ptrIAppFramework == nil)
    {
        //CA(" ptrIAppFramework nil ");
        return;
    }
    
    /*
    if(check == 0)
    {
        //CA("LoadPaletteData 1");
        flg = 1;
        return;
    }
    */
    /*
    if(NewFlgForMinmise == 1)
    {
        //CA("NewFlgForMinmise return");
        NewFlgForMinmise = 0;
        
        InterfacePtr<ILoginHelper> ptrLogInHelper(static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss,ILoginHelper::kDefaultIID)));
        if(ptrLogInHelper == nil)
        {
            ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::ptrLogInHelper == nil ");
            return ;
        }
        InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
        if(!ptrIClientOptions)
        {
            ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::No ptrIClientOptions ");
            return;
        }
        Mediator md;
        double previousSectionID = md.getCurrSectionID();  //-when Content Sprayer is closed, and select Event\Section ID than this Method contain Previous selected Event\Section ID.
        
        LoginInfoValue cserverInfoValue;
        bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue);
        
        ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();
        
        PMString strSectionID =	clientInfoObj.getSectionID();
        double section_ID = strSectionID.GetAsDouble();
        //PMString strIsONE = clientInfoObj.getIsONESource();
        
        PMString strIsONE = "false";
        ///int32 section_ID = -1;
        
        
        PMString defPubName("");
        double defPubId=-1;
        
        defPubId = ptrIClientOptions->getDefPublication(defPubName);
        
        if(defPubId <= 0)
            return;
        
        SDKListBoxHelper listHelper(this, kSPPluginID);
        
        InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
        if(iPanelControlData == nil)
        {
            return;
        }
        IControlView * listBox = listHelper.FindCurrentListBox(iPanelControlData, 1);
        if(listBox != nil)
        {
            //CA("listBox != nil");
            //IPanelControlData* iPanelControlData;
            IControlView * listBox1 = listHelper.FindCurrentListBox(iPanelControlData, 1);
            if(listBox1 != nil)
            {
                InterfacePtr<IListBoxController> listCntl(listBox1,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
                if(listCntl)
                {
                    K2Vector<int32> multipleSelection ;
                    listCntl->GetSelected( multipleSelection );
                    const int32 kSelectionLength = multipleSelection.size();
                    if(kSelectionLength > 1)
                    {
                        listCntl->DeselectAll();
                    }
                }
            }
        }
        
        
        if(strIsONE  == "false")
        {
            if(section_ID != -1)
            {
                if(section_ID == previousSectionID)
                    return;
            }
            else
            {
                if(defPubId == previousSectionID)
                    return;
            }
        }
        else
        {
            return;
        }
        
    }
    */
    
    do
    {
        //CA("LoadPaletteData 3");
        IPanelControlData* iPanelControlData;
        InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
        if(ptrIAppFramework == nil)
        {
            //CA(" ptrIAppFramework nil ");
            break;
        }
        
        bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
        
        if(!md.getMainPanelCtrlData())
        {
            //CA("!md.getMainPanelCtrlData()....and going to call QueryPanelControlData");
            InterfacePtr<IApplication>iApplication(GetExecutionContextSession()->QueryApplication());
            if(iApplication==nil){
                //CA("No iApplication");
                ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGSelectionObserver::loadPaletteData::iApplication==nil");
                break;
            }
            InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager());
            if(iPanelMgr == nil){
                ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGSelectionObserver::loadPaletteData::iPanelMgr == nil");
                break;
            }
            
            IControlView* pnlControlView = iPanelMgr->GetPanelFromWidgetID(kCTGPanelWidgetID);
            if(pnlControlView == NULL)
            {
                //CA("pnlControlView is NULL");
                break;
            }
            InterfacePtr<IWidgetParent> panelWidgetParent( pnlControlView, UseDefaultIID());
            if(panelWidgetParent == NULL)
            {
                //CA("panelWidgetParent is NULL");
                break;
            }
            InterfacePtr<IPanelControlData> iPanelControlData2(panelWidgetParent->GetParent(), UseDefaultIID());
            if(iPanelControlData2 != nil)
            {
                //CA("Not Nil");
                iPanelControlData2->AddRef();
                iPanelControlData = iPanelControlData2;
                md.setMainPanelCntrlData(iPanelControlData);
            }
            else
            {
                //CA("NIL");
                iPanelControlData = nil;
            }
            
            // for testing
            /*
            if(iPanelControlData == nil)
            {
                //CA("Panel Control Data is nil11111111111111111");
                md.setMainPanelCntrlData(md.getMainPanelCtrlData1());
                //CA("asdddddd");
                if(md.getMainPanelCtrlData1() == nil)
                {
                    //CA("FFFFFFFFFFFFFFFF");
                }
                else
                    //CA("NOT NULL");
                    break;
            }
            else
            {
                md.setMainPanelCntrlData(iPanelControlData);
                md.setMainPanelCntrlData1(iPanelControlData);
            }
             */
        }
        
        
        //CA("LoadPaletteData 5");
        CTGActionComponent actionObsever(this);
        //actionObsever.DoPalette();
        iPanelControlData=md.getMainPanelCtrlData();
        if(!iPanelControlData)
        {
            //////CA("LoadPaletteData 11");
            //CA("No panelcontrol data");
            break;
        }

        
        if(isUserLoggedIn) //-- original if
        { //-- original if opening brace
            

            InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
            if(!ptrIClientOptions)
            {
                ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGSelectionObserver::loadPaletteData::No ptrIClientOptions ");
                break;
            }
            
            double lang_id = -1 ;
            PMString lang_name("");
            
            lang_id = ptrIClientOptions->getDefaultLocale(lang_name);
            md.languageID = lang_id ;
            
            PMString defPubName("");
            double defPubId=-1;
            
            defPubId = ptrIClientOptions->getDefPublication(defPubName);
            if(defPubId <= 0)
            {
                //CA(" defPubId <= 0 ");
                break;
            }
            md.CurrentSelectedPublicationID = defPubId;
        
            InterfacePtr<ILoginHelper> ptrLogInHelper(static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss,ILoginHelper::kDefaultIID)));
            if(ptrLogInHelper == nil)
            {
                ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::loadPaletteData::ptrLogInHelper == nil ");
                return ;
            }
            LoginInfoValue cserverInfoValue;
            bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue);
            ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();
            PMString strSectionID =	clientInfoObj.getSectionID();
            double section_ID = strSectionID.GetAsDouble();

            CPubModel pubModelValue = ptrIAppFramework->getpubModelByPubID(section_ID, lang_id);
            double pubParentID = pubModelValue.getParentId();
            
            VectorPubModelPtr vec_pub_model = ptrIAppFramework->ProjectCache_getAllChildren(pubParentID, lang_id);
            if( vec_pub_model == NULL)
            {
                //CA(" nothing fiund ");
                return ;
            }
            
            if(vec_pub_model->size() <= 0)
            {
                //CA(" nothing fiund ");
                return ;
            }
            
            IControlView* sectionDropListCntrlView=iPanelControlData->FindWidget(kCTGSectionDropDownWidgetID);                if(sectionDropListCntrlView==nil)
            {
                //////CA("LoadPaletteData 45");
                //CA("sectionDropListCntrlView");
                break;
            }
            else
            {
                //////CA("LoadPaletteData 46");
                sectionDropListCntrlView->Enable(); // added on 14 feb 2006
                //md.setsubSectionDropListView(subSectionDropListCntrlView);
                md.setSectionDropDownView(sectionDropListCntrlView);
            }
            
            InterfacePtr<IDropDownListController> sectionDropListCntrler(sectionDropListCntrlView, UseDefaultIID());
            if(sectionDropListCntrler==nil)
            {
                //		CA("sectionDropListCntrler is nil");
                break;
            }
            
            
            InterfacePtr<IStringListControlData> subSectionDropListCntrlData(sectionDropListCntrler, UseDefaultIID());
            if(subSectionDropListCntrlData==nil)
            {
                break;
            }
            
            SectionData publdata;
            publdata.clearSectionVector();
            
            subSectionDropListCntrlData->Clear();
            PMString as;
            as.Append("--Select--");
            as.SetTranslatable(kFalse);
            subSectionDropListCntrlData->AddString(as);
            
            if(vec_pub_model->size() > 0)
            {
                int32 size = static_cast<int32>(vec_pub_model->size());
                //global_vector_pubmodel.clear();
                for(int32 k = 0 ; k < size ; k++)
                {
                    CPubModel model = (*vec_pub_model)[k];
                    double sectid=model.getEventId();
                    PMString pubname=model.getName();
                    int32 lvl= 0  ; //it->getLevel_no();
                    double rootid =   model.getRootID();
                    double Typeid = model.getTypeID();
                    PMString PubComment = ""; // model.getComments();
                    
                    publdata.setSectionVectorInfo(sectid,pubname,lvl, rootid, Typeid, PubComment);
                    
                    subSectionDropListCntrlData->AddString(pubname);
                    //global_vector_pubmodel.push_back(model);
                }
                
            }
            if(vec_pub_model->size() > 0)
            {
                sectionDropListCntrler->Select(1);
            }
            else
            {
                sectionDropListCntrler->Select(0);

            }
            sectionDropListCntrlView->Disable();
            
            if(vec_pub_model)
                delete vec_pub_model;
            
                
            IControlView *attributeDropDownCntrlView = iPanelControlData->FindWidget(kCTGItemFieldsDropDownWidgetID);
            if(attributeDropDownCntrlView == nil)
            {
                ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGSelectionObserver::loadPaletteData::No attributeDropDownCntrlView");
                break;
            }
            else
            {
                md.setAttributeDropDownView(attributeDropDownCntrlView);
            }
            
            attributeDropDownCntrlView->HideView();
            
            /*
            InterfacePtr<IDropDownListController> attributeDropListCntrler(attributeDropDownCntrlView, UseDefaultIID());
            if(sectionDropListCntrler==nil)
            {
                //		CA("sectionDropListCntrler is nil");
                break;
            }
            
            InterfacePtr<IStringListControlData> attributeDropListCntrlData(attributeDropListCntrler, UseDefaultIID());
            if(attributeDropListCntrlData==nil)
            {
                break;
            }
            
            
            publdata.clearAttributeVector();
            
            VectorAttributeInfoPtr AttrInfoVectPtr = NULL;
            
            VectorClassInfoPtr vectClassInfoValuePtr = ptrIAppFramework->ClassificationTree_getRoot(md.languageID);
            if(vectClassInfoValuePtr==nil)
            {
                ptrIAppFramework->LogError("APJS10_CatsyTagger::CTGSelectionObserver::loadPaletteData::ClassificationTree_getRoot's vectClassInfoValuePtr==nil");
                break;
            }
            VectorClassInfoValue::iterator it;
            it=vectClassInfoValuePtr->begin();
            double CurrentClassID = it->getClass_id();
            
            if(vectClassInfoValuePtr)
                delete vectClassInfoValuePtr;
            
            
            AttrInfoVectPtr = ptrIAppFramework->StructureCache_getItemAttributesForClassAndParents(CurrentClassID, md.languageID);
            if(AttrInfoVectPtr== NULL){
                ptrIAppFramework->LogError("APJS10_CatsyTagger::CTGSelectionObserver::loadPaletteData::AttributeCache_getItemAttributesForClassAndParents's AttrInfoVectPtr is nil ");
                break;
            }

            attributeDropListCntrlData->Clear();
            PMString asd;
            asd.Append("--Select--");
            asd.SetTranslatable(kFalse);
            attributeDropListCntrlData->AddString(asd);
            
            if(AttrInfoVectPtr->size() > 0)
            {
                int32 size = static_cast<int32>(AttrInfoVectPtr->size());
                for(int32 k = 0 ; k < size ; k++)
                {
                    
                    Attribute attribute = (*AttrInfoVectPtr)[k];
                    
                    if(attribute.getDataType() == "ASSET")
                        continue;
                    
                    double sectid=attribute.getAttributeId();
                    PMString pubname=attribute.getDisplayName();
                    int32 lvl= 0  ;
                    double rootid = attribute.getParentAttributeId();
                    double Typeid = attribute.getTypeId();
                    PMString PubComment = "";
                    publdata.setAttributeVectorInfo(sectid,pubname,lvl, rootid, Typeid, PubComment);
                    attributeDropListCntrlData->AddString(pubname);
                }
                
                for(int32 k = 0 ; k < size ; k++)
                {
                    
                    Attribute attribute = (*AttrInfoVectPtr)[k];
                    
                    if(attribute.getDataType() != "ASSET")
                        continue;
                    
                    double sectid=attribute.getAttributeId();
                    PMString pubname=attribute.getDisplayName();
                    int32 lvl= 0  ;
                    double rootid = attribute.getParentAttributeId();
                    double Typeid = attribute.getTypeId();
                    PMString PubComment = "ASSET";
                    publdata.setAttributeVectorInfo(sectid,pubname,lvl, rootid, Typeid, PubComment);
                    attributeDropListCntrlData->AddString(pubname);
                }
                
            }
            if(AttrInfoVectPtr->size() > 0)
            {
                attributeDropListCntrler->Select(1);
            }
            else
            {
                attributeDropListCntrler->Select(0);
            }
            */

            /*
            IControlView* spTreeListBoxWidgetView = iPanelControlData->FindWidget(kSPTreeListBoxWidgetID);
            InterfacePtr<ITreeViewMgr> treeViewMgr(spTreeListBoxWidgetView, UseDefaultIID());
            if(!treeViewMgr)
            {
                ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");					
                return ;
            }				
            dc.clearMap();
            pNodeDataList.clear();
            SPTreeModel pModel;
            PMString pfName("Root");
            pModel.setRoot(-1, pfName, 1);
            treeViewMgr->ClearTree(kTrue);
            pModel.GetRootUID();
            treeViewMgr->ChangeRoot();
            
            */
                
        }
        
        if(!isUserLoggedIn)
        {

            /*
            IControlView* spTreeListBoxWidgetView = iPanelControlData->FindWidget(kSPTreeListBoxWidgetID);
            InterfacePtr<ITreeViewMgr> treeViewMgr(spTreeListBoxWidgetView, UseDefaultIID());
            if(!treeViewMgr)
            {
                ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");					
                return ;
            }				
            dc.clearMap();
            pNodeDataList.clear();
            SPTreeModel pModel;
            PMString pfName("Root");
            pModel.setRoot(-1, pfName, 1);
            treeViewMgr->ClearTree(kTrue);
            pModel.GetRootUID();
            treeViewMgr->ChangeRoot();
            */
            
            SectionData publdata;
            publdata.clearSectionVector();
            publdata.clearAttributeVector();
            

            InterfacePtr<IDropDownListController> sectionDropListCntrler(md.getSectionDropDownView() ,UseDefaultIID());
            if(sectionDropListCntrler)
                sectionDropListCntrler->Select(-1);
            
            InterfacePtr<IStringListControlData> subSectionDropListCntrlData(sectionDropListCntrler, UseDefaultIID());
            if(subSectionDropListCntrlData==nil)
            {
                break;
            }
            subSectionDropListCntrlData->Clear();
            
            md.getAttributeDropDownView()->HideView();
            /*
            InterfacePtr<IDropDownListController> attributeDropListCntrler(md.getAttributeDropDownView(), UseDefaultIID());
            if(attributeDropListCntrler)
                attributeDropListCntrler->Select(-1);
            
            InterfacePtr<IStringListControlData> attributeDropListCntrlData(attributeDropListCntrler, UseDefaultIID());
            if(attributeDropListCntrlData==nil)
            {
                break;
            }
            attributeDropListCntrlData->Clear();
             */
            
            //md.getSectionDropDownView()->Disable();
            //md.getAttributeDropDownView()->Disable();

            
        }
        
    }while(kFalse);
    
}

double CTGSelectionObserver::getSelectedSectionId(IControlView* cntrlView, PMString& curSelSecName)
{
    //CA(" inside getSelectedSectionId ");
    double curSelSecId=-1;
    InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
    if(ptrIAppFramework == nil)
    {
        //CA(" ptrIAppFramework nil ");
        return 0;
    }
    do{
        InterfacePtr<IStringListControlData> sectionDropListData(cntrlView,UseDefaultIID());
        if (sectionDropListData==nil)
        {
            ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGSelectionObserver::getSelectedSectionId::No sectionDropListData");
            break;
        }
        
        InterfacePtr<IDropDownListController> sectionDropListController(sectionDropListData, UseDefaultIID());
        if (sectionDropListController==nil)
        {
            ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGSelectionObserver::getSelectedSectionId::No sectionDropListController");
            break;
        }
        
        int32 selectedRow = sectionDropListController->GetSelected();
        
        if(selectedRow <=0)
        {
            break;
        }
        
        SectionData sectionInfo;
        PubData publdata = sectionInfo.getSectionVectorInfo(selectedRow-1);
        curSelSecId=publdata.getPubId();
        
        Mediator md;
        md.sectionID = curSelSecId;
        md.setCurrentSelectedRow(selectedRow);


        
    }while(kFalse);
    return curSelSecId;
}

double CTGSelectionObserver::getSelectedAttributeId(IControlView* cntrlView, PMString& curAttributeName, bool16 &isAssetAttribute)
{
    //CA(" inside getSelectedSectionId ");
    double curSelAttributeId=-1;
    InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
    if(ptrIAppFramework == nil)
    {
        //CA(" ptrIAppFramework nil ");
        return 0;
    }
    do{
        InterfacePtr<IStringListControlData> attributeDropListData(cntrlView,UseDefaultIID());
        if (attributeDropListData==nil)
        {
            ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGSelectionObserver::getSelectedSectionId::No attributeDropListData");
            break;
        }
        
        InterfacePtr<IDropDownListController> attributeDropListController(attributeDropListData, UseDefaultIID());
        if (attributeDropListController==nil)
        {
            ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGSelectionObserver::getSelectedSectionId::No attributeDropListController");
            break;
        }
        
        int32 selectedRow = attributeDropListController->GetSelected();
        
        if(selectedRow <=0)
        {
            break;
        }
        
        SectionData sectionInfo;
        PubData publdata = sectionInfo.getAttributeVectorInfo(selectedRow-1);
        curSelAttributeId=publdata.getPubId();
        
        if(publdata.getComment() == "ASSET")
            isAssetAttribute = kTrue;
        else
            isAssetAttribute = kFalse;
        
        
        Mediator md;
        md.attributeID = curSelAttributeId;
        md.setCurrentAttributeRow(selectedRow);
        
    }while(kFalse);
    return curSelAttributeId;
}

bool8 CTGSelectionObserver::populateTree(double curSelSubecId, double attributeId , bool16 isAssetAttribute)
{
    //	CA(__FUNCTION__);

    bool8 isListBoxPopulated = kFalse;
    
    AcquireWaitCursor awc;
    awc.Animate();
    
    
    Mediator md;
    IPanelControlData* iPanelControlData=md.getMainPanelCtrlData();
    if(iPanelControlData == nil)
    {
        //CA("panelCntrlData is nil");
        return kFalse;
    }

    IControlView* spTreeListBoxWidgetView = iPanelControlData->FindWidget(kCTGTreeListBoxWidgetID);

    ////////////////////////////// for search ends
    IControlView* listBox = iPanelControlData->FindWidget(kCTGTreeWrapperGroupPanelWidgetID);

    
    do
    {
        Mediator md;
        IPanelControlData* iPanelControlData;
        PublicationNode pNode;
        
        InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
        if(ptrIAppFramework == nil)
        {
            CAlert::InformationAlert("Err ptrIAppFramework is nil");
            break;
        }

        iPanelControlData=md.getMainPanelCtrlData();
        if(!iPanelControlData)
        {
            //CA("Serious Error Again");
            break;
        }
        
        dc.clearMap();
        pNodeDataList.clear();
        

        //if(!isONEsource)
        {
            //CA("Publication Mode");
            PMString itemFieldIds("");
            PMString itemAssetTypeIds("");
            PMString itemGroupFieldIds("");
            PMString itemGroupAssetTypeIds("");
            PMString listTypeIds("");
            PMString listItemFieldIds("");
            PMString itemIds("");
            PMString itemGroupIDs("");
            PMString tableIds("");
            PMString itemAttributeGroupIds("");
            bool16 isSprayItemPerFrameFlag = kTrue;
            bool16 useRefreshbyAttribute = kTrue;
            bool16 isCategorySpecificResultCall = kFalse;
            
            itemFieldIds.AppendNumber(PMReal(attributeId));
            
            ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGSelectionObserver::populateTree::Before EventCache_setCurrentSectionData");
            
            bool16 resultFlag = ptrIAppFramework->EventCache_setCurrentSectionData( curSelSubecId, md.languageID ,itemGroupIDs, itemIds,  itemFieldIds, itemAssetTypeIds, itemGroupFieldIds,itemGroupAssetTypeIds,listTypeIds, listItemFieldIds, isCategorySpecificResultCall , isSprayItemPerFrameFlag, itemAttributeGroupIds);
            if(resultFlag == kFalse)
            {
                break;
            }

            ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGSelectionObserver::populateTree::After EventCache_setCurrentSectionData");
            
            VectorPubObjectValuePtr VectorFamilyInfoValuePtr = NULL;
            PMString AttributeIdStr("");
            AttributeIdStr.AppendNumber(PMReal(attributeId));
            bool16 isChildDataRequire = kFalse;
            
            VectorFamilyInfoValuePtr = ptrIAppFramework->getProductsAndItemsForSection(curSelSubecId, md.languageID , AttributeIdStr,isChildDataRequire);
            if(VectorFamilyInfoValuePtr == nil)
            {
                
                InterfacePtr<ITreeViewMgr> treeViewMgr(spTreeListBoxWidgetView, UseDefaultIID());
                if(!treeViewMgr)
                {
                    ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");
                    break;
                }
                
                CTGTreeModel pModel;
                PMString pfName("Root");
                pModel.setRoot(-1, pfName, 1);
                treeViewMgr->ClearTree(kTrue);
                pModel.GetRootUID();
                treeViewMgr->ChangeRoot();

                break;
            }
            
            if(VectorFamilyInfoValuePtr->size()==0)
            {
                InterfacePtr<ITreeViewMgr> treeViewMgr(spTreeListBoxWidgetView, UseDefaultIID());
                if(!treeViewMgr)
                {
                    ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");
                    break;
                }
                
                CTGTreeModel pModel;
                PMString pfName("Root");
                pModel.setRoot(-1, pfName, 1);
                treeViewMgr->ClearTree(kTrue);
                pModel.GetRootUID();
                treeViewMgr->ChangeRoot();
                
 
                delete VectorFamilyInfoValuePtr;
                break;
            }
            
            int32 size =static_cast<int32> (VectorFamilyInfoValuePtr->size());
            
            VectorPubObjectValue::iterator it2;
            int count=0;
            int nodeSeq = 0;
            PMString itemCountString("");
            int itemCountNum=0;
            int32 root = -1;
            for(it2 = VectorFamilyInfoValuePtr->begin(); it2 != VectorFamilyInfoValuePtr->end(); it2++ )
            {
                count++;
                pNode.setTreeNodeId(count);
                pNode.setTreeParentNodeId(root);
                pNode.setPBObjectID(it2->getPub_object_id());
                pNode.setSequence(nodeSeq);//(it2->getIndex());
                pNode.setIsProduct(it2->getisProduct());
                pNode.setIsONEsource(kFalse);
                pNode.setIsStarred(isAssetAttribute);
                pNode.setSectionID(curSelSubecId);
                pNode.setPublicationID(md.CurrentSelectedPublicationID);
                pNode.setParentId(curSelSubecId);
                pNode.setTypeId(it2->getobject_type_id());
                int32 NewProductFlag =0;
                pNode.setSubSectionID(attributeId); // Used here to store attributeId of Item.
                
                //CA(it2->getName());
                if(it2->getNew_product()  == kTrue){
                    NewProductFlag =2;
                    pNode.setNewProduct(1);
                }
                else{
                    NewProductFlag =1;
                    pNode.setNewProduct(0);
                }
                
                //Hybrid Table Change
                if(it2->getisProduct() == 2)
                {
                    pNode.setPubId(it2->getObjectValue().getObject_id());
                    
                    PMString pName1 = it2->getName();
                    pNode.setPublicationName(pName1);
                    
                    pNode.setChildCount(it2->getObjectValue().getChildCount());
                    pNode.setChildItemCount(it2->getObjectValue().getChildCount());
                    pNode.setTypeId(it2->getObjectValue().getObject_type_id());
                    
                }
                
                if(it2->getisProduct() == 1)
                {
                    pNode.setPubId(it2->getObjectValue().getObject_id());
                    
                    PMString pName1 = it2->getName();
                    pNode.setPublicationName(pName1);
                    
                    VectorScreenTableInfoPtr tableInfo=NULL;
                    int32 childCount = 0;
                    tableInfo=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(curSelSubecId, it2->getObjectValue().getObject_id(),  md.languageID , kFalse);
                    
                    if(tableInfo == NULL || tableInfo->size() == 0)
                    {
                        childCount = 0;
                    }
                    else
                        //childCount = tableInfo->size();
                        childCount = 0;
                    
                    pNode.setChildCount(childCount);
                    pNode.setChildItemCount(childCount);

                    pNode.setChildCount(it2->getObjectValue().getChildCount());
                    pNode.setChildItemCount(it2->getObjectValue().getChildCount());
                    pNode.setIsProduct(1);
                    //pNode.setReferenceId(it2->getObjectValue().getRef_id());
                    pNode.setTypeId(it2->getObjectValue().getObject_type_id());
                    
                    if(tableInfo != NULL)
                        delete tableInfo;
                }
                
                if(it2->getisProduct() == 0)
                {
                    pNode.setPubId(it2->getItemModel().getItemID());
                    
                    pNode.setTypeId(it2->getobject_type_id());
                    pNode.setIsProduct(0);
                    
                    VectorScreenTableInfoPtr tableInfo=NULL;
                    int32 childCount = 0;
                    tableInfo=ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(it2->getItemModel().getItemID(), curSelSubecId, md.languageID );
                    
                    if(tableInfo == NULL || tableInfo->size() == 0)
                    {
                        childCount = 0;
                    }
                    else
                        childCount = tableInfo->size();
                    
                    pNode.setChildCount(childCount);
                    pNode.setChildItemCount(childCount);
                    
                    PMString pName1 = it2->getName();
                    pNode.setPublicationName(pName1);

                    PMString itemAttributeValue = it2->getItemModel().getAttributeValueStr();
                    
                    //pName1.Append("\t\t ");
                    //pName1.Append(itemAttributeValue);
                    
                    pNode.setItemAttributeValueStr(itemAttributeValue);
                    
                    if(tableInfo != NULL)
                        delete tableInfo;
                    
                    
                }
                int icon_count = 0;//GetIconCountForUnfilteredProductList(it2);
                pNode.setIconCount(icon_count);
                pNodeDataList.push_back(pNode);
                
                int32 StarFlag =1;
                if(it2->getStarredFlag1()) // if Selected Green Star
                    StarFlag =2;
                //CA("Before showImageAsThumbnail");
                
                int32 isProduct = it2->getisProduct();
                
                if(pNode.getChildCount() > 0)
                {
                    //ptrIAppFramework->LogDebug("Before populateChildItemsFromList");
                    populateListForNode(pNode, count,  isAssetAttribute);
                    //ptrIAppFramework->LogDebug("After populateChildItemsFromList");
                }
                nodeSeq++;
                
            }
            if(VectorFamilyInfoValuePtr)
                delete VectorFamilyInfoValuePtr;
        }
        
        if( pNodeDataList.size() > 0 )
        {
            InterfacePtr<ITreeViewMgr> treeViewMgr(spTreeListBoxWidgetView, UseDefaultIID());
            if(!treeViewMgr)
            {
                ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");					
                break;
            }
            
            CTGTreeModel pModel;
            PMString pfName("Root");
            pModel.setRoot(-1, pfName, 1);
            treeViewMgr->ClearTree(kTrue);
            pModel.GetRootUID();
            treeViewMgr->ChangeRoot();
            
            isListBoxPopulated = kTrue;
            
            //InterfacePtr<IListBoxController> listStringControlData(lstboxControlView,UseDefaultIID());
            //if(listStringControlData)
            {
                CurrentSelectedProductRow = 0; // First Product of List
                md.setCurrentObjectID(pNodeDataList[CurrentSelectedProductRow].getPubId());
                
            }
        }
        else
        {
            //CA("isListBoxPopulated =  kFalse");
            isListBoxPopulated =  kFalse;
            //md.getRefreshButtonView()->Disable();
        }
        
        
    }while(kFalse);
    
    //CA("returning populateProductListforSection()");
    return isListBoxPopulated;
}

void CTGSelectionObserver::populateChildItemsFromList(PublicationNode &pNode, int32 &count, bool16 isAssetAttribute)
{
    InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
    if(ptrIAppFramework == nil)
    {
        //CA(" ptrIAppFramework nil ");
        return;
    }
    
    vector<double>  FinalItemIds;
    vector<ChildItemModel> FinalChildItems;
    Mediator md;
    VectorScreenTableInfoPtr tableInfo = NULL;
    
    
    if(pNode.getIsProduct() == 0)
    {
        
        tableInfo= ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPubId(), pNode.getSectionID(), md.languageID );
        if(tableInfo == NULL)
        {
            ptrIAppFramework->LogError("tableinfo is NULL");
            return;
        }
    }
    else if(pNode.getIsProduct() == 1)
    {
        
        tableInfo= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid( pNode.getSectionID(), pNode.getPubId(), md.languageID , kFalse );
        if(tableInfo == NULL)
        {
            ptrIAppFramework->LogError("tableinfo is NULL");
            return;
        }
    }
    
    if(tableInfo->size()==0)
    {
        ptrIAppFramework->LogError(" tableInfo->size()==0");
        return;
    }
    
    
    CItemTableValue oTableValue;
    VectorScreenTableInfoValue::iterator it;
    bool16 typeidFound=kFalse;
    for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
    {
        oTableValue = *it;
        vector<double>  vec_items = oTableValue.getItemIds();
        vector<ChildItemModel> vecChildItems = oTableValue.getChildItems();
        
        
        bool16 isChildItemsPresent = kFalse;
        
        int vec_items_size = vec_items.size();
        
        if( vec_items_size > 0 && (vec_items_size == vecChildItems.size() ))
        {
            isChildItemsPresent = kTrue;
            ptrIAppFramework->LogDebug("populateChildItemsFromList isChildItemsPresent = kTrue");
            
        }
        else
        {
            isChildItemsPresent= kFalse;
            ptrIAppFramework->LogDebug("populateChildItemsFromList isChildItemsPresent = kFalse");
        }

        if(FinalItemIds.size() == 0)
        {
            FinalItemIds = vec_items;
            if(isChildItemsPresent)
                FinalChildItems = vecChildItems;
            
        }
        else
        {
            for(int32 i=0; i<vec_items.size(); i++)
            {
                bool16 Flag = kFalse;
                for(int32 j=0; j<FinalItemIds.size(); j++)
                {
                    if(vec_items[i] == FinalItemIds[j])
                    {
                        Flag = kTrue;
                        break;
                    }
                }
                if(!Flag )
                {
                    FinalItemIds.push_back(vec_items[i]);
                    if(isChildItemsPresent)
                        FinalChildItems.push_back(vecChildItems[i]);
                    
                }
            }
        }
    }
    
    if(FinalItemIds.size() == 0)
    {
        if(tableInfo != NULL)
            delete tableInfo;
        return;
    }
    
    for(int32 j=0; j<FinalItemIds.size(); j++)
    {
        PublicationNode pNodeChild;
        
        count++;
        pNodeChild.setTreeNodeId(count);
        pNodeChild.setTreeParentNodeId(pNode.getTreeNodeId());
        
        pNodeChild.setPBObjectID(pNode.getPubId());
        pNodeChild.setSequence(j);//(it2->getIndex());
        pNodeChild.setIsProduct(0);
        pNodeChild.setIsONEsource(kFalse);
        pNodeChild.setIsStarred(isAssetAttribute);
        pNodeChild.setSectionID(pNode.getSectionID());
        pNodeChild.setPublicationID(md.CurrentSelectedPublicationID);
        pNodeChild.setParentId(pNode.getPubId());
        pNodeChild.setTypeId(pNode.getTypeId());
        pNodeChild.setSubSectionID(pNode.getSubSectionID()); // Used here to store attributeId of Item.
        pNodeChild.setNewProduct(0);

        pNodeChild.setPubId(FinalItemIds[j]);
        
        PMString pName1("");
        
        if(FinalChildItems.size() > j)
         pName1 = FinalChildItems[j].getItemNo();
        else
            pName1.AppendNumber(PMReal(FinalItemIds[j]));
        
        pNodeChild.setPublicationName(pName1);
        
        pNodeChild.setTypeId(pNode.getTypeId());
        pNodeChild.setIsProduct(0);
        pNodeChild.setChildCount(0);
        pNodeChild.setChildItemCount(0);
        
        //ptrIAppFramework->LogDebug("populateChildItemsFromList 10");
        PMString itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(pNodeChild.getPubId(), pNodeChild.getSubSectionID(), md.languageID, pNodeChild.getSectionID(), kFalse );
        
        
        //pName1.Append("\t\t- ");
        //pName1.Append(itemAttributeValue);
        pNodeChild.setPublicationName(pName1);
        
        
        pNode.setItemAttributeValueStr(itemAttributeValue);
        
        int icon_count = 0;;
        pNodeChild.setIconCount(icon_count);
        
        pNodeDataList.push_back(pNodeChild);
        
        //ptrIAppFramework->LogDebug("populateChildItemsFromList 10.1");
        
    }

    if(tableInfo != NULL)
        delete tableInfo;

    
}

void CTGSelectionObserver::populateListForNode(PublicationNode &pNode, int32 &count, bool16 isAssetAttribute)
{
    InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
    if(ptrIAppFramework == nil)
    {
        //CA(" ptrIAppFramework nil ");
        return;
    }
    
    vector<double>  FinalItemIds;
    vector<ChildItemModel> FinalChildItems;
    Mediator md;
    VectorScreenTableInfoPtr tableInfo = NULL;
    
    
    if(pNode.getIsProduct() == 0)
    {
        
        tableInfo= ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPubId(), pNode.getSectionID(), md.languageID );
        if(tableInfo == NULL)
        {
            ptrIAppFramework->LogError("tableinfo is NULL");
            return;
        }
    }
    else if(pNode.getIsProduct() == 1)
    {
        
        tableInfo= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid( pNode.getSectionID(), pNode.getPubId(), md.languageID , kFalse );
        if(tableInfo == NULL)
        {
            ptrIAppFramework->LogError("tableinfo is NULL");
            return;
        }
    }
    
    if(tableInfo->size()==0)
    {
        ptrIAppFramework->LogError(" tableInfo->size()==0");
        if(tableInfo != NULL)
            delete tableInfo;
        
        return;
    }
    
    
    CItemTableValue oTableValue;
    VectorScreenTableInfoValue::iterator it;
    bool16 typeidFound=kFalse;
    int32 sequence = 0;
    for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
    {
        oTableValue = *it;
        vector<double>  vec_items = oTableValue.getItemIds();
        vector<ChildItemModel> vecChildItems = oTableValue.getChildItems();
        
        
        bool16 isChildItemsPresent = kFalse;
        
        int vec_items_size = vec_items.size();
        
        if( vec_items_size > 0 && (vec_items_size == vecChildItems.size() ))
        {
            isChildItemsPresent = kTrue;
            ptrIAppFramework->LogDebug("populateChildItemsFromList isChildItemsPresent = kTrue");
            
        }
        else
        {
            isChildItemsPresent= kFalse;
            ptrIAppFramework->LogDebug("populateChildItemsFromList isChildItemsPresent = kFalse");
            return;
        }
        
        PublicationNode pNodeChild;
        
        count++;
        pNodeChild.setTreeNodeId(count);
        pNodeChild.setTreeParentNodeId(pNode.getTreeNodeId());
        
        pNodeChild.setPBObjectID(pNode.getPubId());
        pNodeChild.setSequence(sequence);//(it2->getIndex());
        pNodeChild.setIsProduct(5); // List
        pNodeChild.setIsONEsource(kFalse);
        pNodeChild.setIsStarred(kFalse);
        pNodeChild.setSectionID(pNode.getSectionID());
        pNodeChild.setPublicationID(md.CurrentSelectedPublicationID);
        pNodeChild.setParentId(pNode.getPubId());
        pNodeChild.setTypeId(pNode.getTypeId());
        pNodeChild.setSubSectionID(pNode.getSubSectionID()); // Used here to store attributeId of Item.
        pNodeChild.setNewProduct(0);
        
        pNodeChild.setPubId(oTableValue.getTableID());
        
        PMString pName1("");
        
        pName1 = oTableValue.getName();

        
        pNodeChild.setPublicationName(pName1);
        
        pNodeChild.setTypeId(oTableValue.getTableTypeID());
        pNodeChild.setIsProduct(5);
        
        int32 childCount = 0;
        childCount = oTableValue.getItemIds().size();
        
        pNodeChild.setChildCount(childCount);
        pNodeChild.setChildItemCount(childCount);
        
        pNodeChild.setItemAttributeValueStr("");
        
        int icon_count = 0;;
        pNodeChild.setIconCount(icon_count);
        
        pNodeDataList.push_back(pNodeChild);
        
        if(pNode.getChildCount() > 0)
        {
            //ptrIAppFramework->LogDebug("Before populateChildItemsFromList");
            populateListChildItemForNode(pNodeChild, count,  isAssetAttribute, oTableValue);
            //ptrIAppFramework->LogDebug("After populateChildItemsFromList");
        }

        sequence++;
    }
    
    if(tableInfo != NULL)
        delete tableInfo;
    
}


void CTGSelectionObserver::populateListChildItemForNode(PublicationNode &pNode, int32 &count, bool16 isAssetAttribute, CItemTableValue &oTableValue)
{
    InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
    if(ptrIAppFramework == nil)
    {
        //CA(" ptrIAppFramework nil ");
        return;
    }
    
    Mediator md;

    bool16 typeidFound=kFalse;


    vector<double>  vec_items = oTableValue.getItemIds();
    vector<ChildItemModel> vecChildItems = oTableValue.getChildItems();
    int32 colSize = oTableValue.getColumn().size();
    
    
    bool16 isChildItemsPresent = kFalse;
    
    int vec_items_size = vec_items.size();
    
    if( (vec_items_size > 0) && (vecChildItems.size() > 0) && (vec_items_size == vecChildItems.size() ))
    {
        isChildItemsPresent = kTrue;
        ptrIAppFramework->LogDebug("populateChildItemsFromList isChildItemsPresent = kTrue");
        
    }
    else
    {
        isChildItemsPresent= kFalse;
        ptrIAppFramework->LogDebug("populateChildItemsFromList isChildItemsPresent = kFalse");
    }
        
    
    
    
    for(int32 j=0; j<vec_items.size(); j++)
    {
        PublicationNode pNodeChild;
        
        count++;
        pNodeChild.setTreeNodeId(count);
        pNodeChild.setTreeParentNodeId(pNode.getTreeNodeId());
        
        pNodeChild.setPBObjectID(pNode.getPubId());
        pNodeChild.setSequence(j);//(it2->getIndex());
        pNodeChild.setIsProduct(4);
        pNodeChild.setIsONEsource(kFalse);
        pNodeChild.setIsStarred(isAssetAttribute);
        pNodeChild.setSectionID(pNode.getSectionID());
        pNodeChild.setPublicationID(md.CurrentSelectedPublicationID);
        pNodeChild.setParentId(pNode.getPubId());
        pNodeChild.setTypeId(pNode.getTypeId());
        pNodeChild.setSubSectionID(pNode.getSubSectionID()); // Used here to store attributeId of Item.
        pNodeChild.setNewProduct(0);
        
        pNodeChild.setPubId(vec_items[j]);
        pNodeChild.setTypeId(pNode.getTypeId());
        pNodeChild.setIsProduct(4);
        
        
        pNodeChild.setChildCount(colSize);
        pNodeChild.setChildItemCount(colSize);
        
        //ptrIAppFramework->LogDebug("populateChildItemsFromList 10");
        PMString itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(pNodeChild.getPubId(), pNodeChild.getSubSectionID(), md.languageID, pNodeChild.getSectionID(), kFalse );
        
        PMString pName1("");
        pName1 = vecChildItems[j].getItemNo();
        
        //pName1.Append("\t\t ");
        //pName1.Append(itemAttributeValue);
        pNodeChild.setPublicationName(pName1);
        
        pNodeChild.setItemAttributeValueStr(itemAttributeValue);
        
        int icon_count = 0;;
        pNodeChild.setIconCount(icon_count);
        
        pNodeDataList.push_back(pNodeChild);
        
        if(pNode.getChildCount() > 0)
        {
            //ptrIAppFramework->LogDebug("Before populateChildItemsFromList");
            populateListChildItemRowsForNode(pNodeChild, count,  isAssetAttribute, oTableValue, j);
            //ptrIAppFramework->LogDebug("After populateChildItemsFromList");
        }
        //ptrIAppFramework->LogDebug("populateChildItemsFromList 10.1");
        
    }
}


void CTGSelectionObserver::populateListChildItemRowsForNode(PublicationNode &pNode, int32 &count, bool16 isAssetAttribute, CItemTableValue &oTableValue, int32 & rowCount)
{
    InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
    if(ptrIAppFramework == nil)
    {
        //CA(" ptrIAppFramework nil ");
        return;
    }
    
    Mediator md;
    
    bool16 typeidFound=kFalse;
    
    
    vector<double>  vec_items = oTableValue.getItemIds();
    vector<ChildItemModel> vecChildItems = oTableValue.getChildItems();
    int32 colSize = oTableValue.getColumn().size();
    vector<Column> vectorColValue = oTableValue.getColumn();
    vector<vector<PMString> > tableData = oTableValue.getTableData();
    vector<PMString> rowValues = tableData.at(rowCount);
    
    bool16 isChildItemsPresent = kFalse;
    
    int vec_items_size = vec_items.size();
    
    if( (vec_items_size > 0) && (vecChildItems.size() > 0) && (vectorColValue.size() > 0) && (vec_items_size == vecChildItems.size() ) && (rowValues.size() > 0) )
    {
        isChildItemsPresent = kTrue;
        ptrIAppFramework->LogDebug("populateChildItemsFromList isChildItemsPresent = kTrue");
        
    }
    else
    {
        isChildItemsPresent= kFalse;
        ptrIAppFramework->LogDebug("populateChildItemsFromList isChildItemsPresent = kFalse");
        return;
    }
    
    
    
    
    for(int32 j=0; j< colSize; j++)
    {
        PublicationNode pNodeChild;
        
        count++;
        pNodeChild.setTreeNodeId(count);
        pNodeChild.setTreeParentNodeId(pNode.getTreeNodeId());
        
        pNodeChild.setPBObjectID(pNode.getPubId());
        pNodeChild.setSequence(j);//(it2->getIndex());
        pNodeChild.setIsProduct(6);
        pNodeChild.setIsONEsource(kFalse);
        pNodeChild.setIsStarred(kFalse);
        pNodeChild.setSectionID(pNode.getSectionID());
        pNodeChild.setPublicationID(md.CurrentSelectedPublicationID);
        pNodeChild.setParentId(pNode.getPubId());
        pNodeChild.setTypeId(pNode.getTypeId());
        pNodeChild.setSubSectionID(vectorColValue[j].getFieldId()); // Used here to store attributeId of Item.
        pNodeChild.setNewProduct(0);
        
        pNodeChild.setPubId(vec_items[rowCount]);
        
        PMString pName1("");
        pName1 = vectorColValue[j].getFieldName();
        
        PMString itemAttributeValue = rowValues.at(j);
        
        //pName1.Append("\t\t ");
        //pName1.Append(itemAttributeValue);
        
        pNodeChild.setPublicationName(pName1);
        
        pNodeChild.setTypeId(pNode.getTypeId());
        
        pNodeChild.setIsProduct(6);
        pNodeChild.setChildCount(0);
        pNodeChild.setChildItemCount(0);
        
        //ptrIAppFramework->LogDebug("populateChildItemsFromList 10");
        
        //ptrIAppFramework->LogDebug("itemAttributeValue :" + itemAttributeValue);
        
        pNodeChild.setItemAttributeValueStr(itemAttributeValue);
        
        int icon_count = 0;;
        pNodeChild.setIconCount(icon_count);
        
        pNodeDataList.push_back(pNodeChild);
        
    }
}


bool8 CTGSelectionObserver::populateTreeForItemAttributes(double curSelSubecId, double itemId , double itemTypeId)
{
    //    CA(__FUNCTION__);
    
    bool8 isListBoxPopulated = kFalse;
    
    AcquireWaitCursor awc;
    awc.Animate();
    
    
    Mediator md;
    IPanelControlData* iPanelControlData=md.getMainPanelCtrlData();
    if(iPanelControlData == nil)
    {
        //CA("panelCntrlData is nil");
        return kFalse;
    }
    
    IControlView* spTreeListBoxWidgetView = iPanelControlData->FindWidget(kCTGTreeListBoxWidgetID);
    
    ////////////////////////////// for search ends
    IControlView* listBox = iPanelControlData->FindWidget(kCTGTreeWrapperGroupPanelWidgetID);
    
    
    do
    {
        Mediator md;
        IPanelControlData* iPanelControlData;
        PublicationNode pNode;
        
        InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
        if(ptrIAppFramework == nil)
        {
            CAlert::InformationAlert("Err ptrIAppFramework is nil");
            break;
        }
        
        iPanelControlData=md.getMainPanelCtrlData();
        if(!iPanelControlData)
        {
            //CA("Serious Error Again");
            break;
        }
        
        dc.clearMap();
        pNodeDataList.clear();
        

        //CA("Publication Mode");
        PMString itemFieldIds("");
        PMString itemAssetTypeIds("");
        PMString itemGroupFieldIds("");
        PMString itemGroupAssetTypeIds("");
        PMString listTypeIds("");
        PMString listItemFieldIds("");
        PMString itemIds("");
        PMString itemGroupIDs("");
        PMString tableIds("");
        PMString itemAttributeGroupIds("");
        bool16 isSprayItemPerFrameFlag = kTrue;
        bool16 useRefreshbyAttribute = kTrue;
        bool16 isCategorySpecificResultCall = kFalse;
        
        itemIds.AppendNumber(PMReal(itemId));
        
        ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGSelectionObserver::populateTree::Before EventCache_setCurrentSectionData");
        
        bool16 resultFlag = ptrIAppFramework->EventCache_setCurrentSectionData( curSelSubecId, md.languageID ,itemGroupIDs, itemIds,  itemFieldIds, itemAssetTypeIds, itemGroupFieldIds,itemGroupAssetTypeIds,listTypeIds, listItemFieldIds, isCategorySpecificResultCall , isSprayItemPerFrameFlag, itemAttributeGroupIds);
        if(resultFlag == kFalse)
        {
            break;
        }
        
        ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGSelectionObserver::populateTree::After EventCache_setCurrentSectionData");
        
        double CurrentClassID = -1;
        VectorAttributeInfoPtr AttrInfoVectPtr = NULL;
        if(CurrentClassID == -1)
        {
            //CA("CurrentClassID == -1");
            VectorClassInfoPtr vectClassInfoValuePtr = ptrIAppFramework->ClassificationTree_getRoot(md.languageID); // 1 default for English
            if(vectClassInfoValuePtr==nil)
            {
                ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::populateItemPanelLstbox::ClassificationTree_getRoot's vectClassInfoValuePtr==nil");
                break;
            }
            VectorClassInfoValue::iterator it;
            it=vectClassInfoValuePtr->begin();
            CurrentClassID = it->getClass_id();
            
            if(vectClassInfoValuePtr)
                delete vectClassInfoValuePtr;
        }
        
        AttrInfoVectPtr = ptrIAppFramework->StructureCache_getItemAttributesForClassAndParents(CurrentClassID, md.languageID);
        if(AttrInfoVectPtr== NULL){
            ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::populateItemPanelLstbox::AttributeCache_getItemAttributesForClassAndParents's AttrInfoVectPtr is nil ");
            
            InterfacePtr<ITreeViewMgr> treeViewMgr(spTreeListBoxWidgetView, UseDefaultIID());
            if(!treeViewMgr)
            {
                ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");
                break;
            }
            
            CTGTreeModel pModel;
            PMString pfName("Root");
            pModel.setRoot(-1, pfName, 1);
            treeViewMgr->ClearTree(kTrue);
            pModel.GetRootUID();
            treeViewMgr->ChangeRoot();
        
            break;
        }
        
        int count=0;
        int nodeSeq = 0;
        PMString itemCountString("");
        int itemCountNum=0;
        int32 root = -1;
        
        VectorAttributeInfoValue::iterator it2;
        
        for(it2=AttrInfoVectPtr->begin();it2!=AttrInfoVectPtr->end();it2++)
        {
            if(it2->getDataType() == "ASSET")
                continue;
            
            //it2->getDisplayName()
            //it2->getAttributeId()
        
            PublicationNode pNodeChild;
            
            count++;
            pNodeChild.setTreeNodeId(count);
            pNodeChild.setTreeParentNodeId(root);
            
            pNodeChild.setPBObjectID(itemId);
            pNodeChild.setSequence(nodeSeq);//(it2->getIndex());
            pNodeChild.setIsProduct(6);
            pNodeChild.setIsONEsource(kFalse);
            pNodeChild.setIsStarred(kFalse);
            pNodeChild.setSectionID(curSelSubecId);
            pNodeChild.setPublicationID(md.CurrentSelectedPublicationID);
            pNodeChild.setParentId(itemId);
            pNodeChild.setTypeId(itemTypeId);
            pNodeChild.setSubSectionID(it2->getAttributeId()); // Used here to store attributeId of Item.
            pNodeChild.setNewProduct(0);
            
            pNodeChild.setPubId(itemId);
            
            PMString pName1("");
            pName1 = it2->getDisplayName();
            
            PMString itemAttributeValue = ("");
            itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemId, it2->getAttributeId(), md.languageID, curSelSubecId, kFalse );
            
            //pName1.Append("\t\t ");
            //pName1.Append(itemAttributeValue);
            
            pNodeChild.setPublicationName(pName1);
            
            pNodeChild.setTypeId(pNode.getTypeId());
            
            pNodeChild.setIsProduct(6);
            pNodeChild.setChildCount(0);
            pNodeChild.setChildItemCount(0);
            
            //ptrIAppFramework->LogDebug("populateChildItemsFromList 10");
            
            //ptrIAppFramework->LogDebug("itemAttributeValue :" + itemAttributeValue);
            
            pNodeChild.setItemAttributeValueStr(itemAttributeValue);
            
            int icon_count = 0;;
            pNodeChild.setIconCount(icon_count);
            
            pNodeDataList.push_back(pNodeChild);
            
        }
        
        /*
         VectorAttributeInfoValue::iterator it5;
         for(it5=AttrInfoVectPtr->begin();it5!=AttrInfoVectPtr->end();it5++)
         {//CAlert::InformationAlert("4");
         
         if(it5->getDataType() == "ASSET")
         {
         PMString displayName("");
         displayName.Append(it5->getDisplayName());
         
         ItemData.setData
         (
         4,                        // which List 4 = item List
         i,                        // Index
         it5->getAttributeId(),// Attribute Id for Core element
         -1,                    // type id ... here we are going to feed Item id
         TPLMediatorClass::CurrLanguageID,
         displayName,//it4->getDisplay_name(),
         kTrue,
         "ASSET",//it4->getDisplay_name(),// Code is replaced by Attribute Name
         kTrue,
         0,           //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
         kFalse,
         -1,-1,-1,-1,intSprItemPerFrameFlow,-1,1,-1,1
         );
         
         ITEMListData.setAll(displayName,it5->getAttributeId(),1,1,0,1);
         ITEMListData.setHitCount(1);
         ITEMDataNodeList.push_back(ITEMListData);
         count4=i;
         i++;
         
         }
         }
         */

        if( pNodeDataList.size() > 0 )
        {
            InterfacePtr<ITreeViewMgr> treeViewMgr(spTreeListBoxWidgetView, UseDefaultIID());
            if(!treeViewMgr)
            {
                ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");
                break;
            }
            
            CTGTreeModel pModel;
            PMString pfName("Root");
            pModel.setRoot(-1, pfName, 1);
            treeViewMgr->ClearTree(kTrue);
            pModel.GetRootUID();
            treeViewMgr->ChangeRoot();
            
            isListBoxPopulated = kTrue;
            
            //InterfacePtr<IListBoxController> listStringControlData(lstboxControlView,UseDefaultIID());
            //if(listStringControlData)
            {
                CurrentSelectedProductRow = 0; // First Product of List
                md.setCurrentObjectID(pNodeDataList[CurrentSelectedProductRow].getPubId());
                
            }
        }
        else
        {
            //CA("isListBoxPopulated =  kFalse");
            isListBoxPopulated =  kFalse;
            //md.getRefreshButtonView()->Disable();
        }
        
        
    }while(kFalse);
    
    //CA("returning populateProductListforSection()");
    return isListBoxPopulated;
}

