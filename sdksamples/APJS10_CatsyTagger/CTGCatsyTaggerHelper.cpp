#include "VCPlugInHeaders.h"

#include "ICTGCatsyTaggerHelper.h"
#include "CTGID.h"

#include "IAppFramework.h"
#include "ICoreFilename.h"
#include "SDKUtilities.h"
#include "ICoreFilenameUtils.h"
#include "CAlert.h"
#include "IPanelControlData.h"
#include "IClientOptions.h"
#include "K2Vector.h" 
#include "FileUtils.h"
#include "IApplication.h"
#include "PaletteRefUtils.h"
#include "IPanelMgr.h"
#include "IDataBase.h"
#include "IApplication.h"
#include "ILibrary.h"
#include "ITextControlData.h"
#include "PMString.h"
#include "StreamUtil.h"

#include "PublicationNode.h"
#include "ITreeViewMgr.h"
#include "CTGTreeModel.h"
#include "MediatorClass.h"
#include "CTGTreeDataCache.h"
#include "AcquireModalCursor.h"
#include "Attribute.h"


#define CA(x) CAlert::InformationAlert(x)

using namespace std;
extern PublicationNodeList pNodeDataList;
extern CTGTreeDataCache dc;
extern int32 CurrentSelectedProductRow;

class CTGCatsyTaggerHelper : public CPMUnknown<ICTGCatsyTaggerHelper>
{
public :
	//TSTableSourceHelper();
	CTGCatsyTaggerHelper(IPMUnknown*  boss);
	~CTGCatsyTaggerHelper();

	virtual bool16 populateTreeForItemAttributes(double curSelSubecId, double itemId , double itemTypeId, PMString ItemDisplayName);

	IPMUnknown* boss_;
};

CREATE_PMINTERFACE(CTGCatsyTaggerHelper, kCTGCatsyTaggerHelperImpl)

CTGCatsyTaggerHelper::CTGCatsyTaggerHelper(IPMUnknown* boss):CPMUnknown<ICTGCatsyTaggerHelper>(boss)
{
	 boss_ = boss ;
}

CTGCatsyTaggerHelper ::~CTGCatsyTaggerHelper()
{}




bool16 CTGCatsyTaggerHelper::populateTreeForItemAttributes(double curSelSubecId, double itemId , double itemTypeId,  PMString ItemDisplayName)
{
    //    CA(__FUNCTION__);
    
    bool16 isListBoxPopulated = kFalse;
    
    AcquireWaitCursor awc;
    awc.Animate();
    
    Mediator md;
    IPanelControlData* iPanelControlData=md.getMainPanelCtrlData();
    if(iPanelControlData == nil)
    {
        //CA("panelCntrlData is nil");
        return kFalse;
    }
    
    IControlView* spTreeListBoxWidgetView = iPanelControlData->FindWidget(kCTGTreeListBoxWidgetID);
    ////////////////////////////// for search ends
    IControlView* listBox = iPanelControlData->FindWidget(kCTGTreeWrapperGroupPanelWidgetID);

    do
    {
        Mediator md;
        IPanelControlData* iPanelControlData;
        PublicationNode pNode;
        
        InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
        if(ptrIAppFramework == nil)
        {
            CAlert::InformationAlert("Err ptrIAppFramework is nil");
            break;
        }
        
        iPanelControlData=md.getMainPanelCtrlData();
        if(!iPanelControlData)
        {
            //CA("Serious Error Again");
            break;
        }
        
        IControlView* displayStringView = iPanelControlData->FindWidget( kCTGItemDisplayStaticTextWidgetID );
        ASSERT(displayStringView);
        if(displayStringView == nil)
        {
            ptrIAppFramework->LogDebug("APJS9_ProductFinder::CTGTreeViewWidgetMgr::ApplyNodeIDToWidget::displayStringView is nil");
            break;
        }
        InterfacePtr<ITextControlData>  textControlData( displayStringView, UseDefaultIID() );
        ASSERT(textControlData);
        if(textControlData== nil)
        {
            ptrIAppFramework->LogDebug("APJS9_ProductFinder::CTGTreeViewWidgetMgr::ApplyNodeIDToWidget::textControlData is nil");
            break;		
        }

        textControlData->SetString( ItemDisplayName );
        
        
        dc.clearMap();
        pNodeDataList.clear();
        
        
        //CA("Publication Mode");
        PMString itemFieldIds("");
        PMString itemAssetTypeIds("");
        PMString itemGroupFieldIds("");
        PMString itemGroupAssetTypeIds("");
        PMString listTypeIds("");
        PMString listItemFieldIds("");
        PMString itemIds("");
        PMString itemGroupIDs("");
        PMString tableIds("");
        PMString itemAttributeGroupIds("");
        bool16 isSprayItemPerFrameFlag = kTrue;
        bool16 useRefreshbyAttribute = kTrue;
        bool16 isCategorySpecificResultCall = kFalse;
        
        itemIds.AppendNumber(PMReal(itemId));
        
        ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGSelectionObserver::populateTree::Before EventCache_setCurrentSectionData");
        
        bool16 resultFlag = ptrIAppFramework->EventCache_setCurrentSectionData( curSelSubecId, md.languageID ,itemGroupIDs, itemIds,  itemFieldIds, itemAssetTypeIds, itemGroupFieldIds,itemGroupAssetTypeIds,listTypeIds, listItemFieldIds, isCategorySpecificResultCall , isSprayItemPerFrameFlag, itemAttributeGroupIds);
        if(resultFlag == kFalse)
        {
            break;
        }
        
        ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGSelectionObserver::populateTree::After EventCache_setCurrentSectionData");
        
        double CurrentClassID = -1;
        VectorAttributeInfoPtr AttrInfoVectPtr = NULL;
        if(CurrentClassID == -1)
        {
            //CA("CurrentClassID == -1");
            VectorClassInfoPtr vectClassInfoValuePtr = ptrIAppFramework->ClassificationTree_getRoot(md.languageID); // 1 default for English
            if(vectClassInfoValuePtr==nil)
            {
                ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::populateItemPanelLstbox::ClassificationTree_getRoot's vectClassInfoValuePtr==nil");
                break;
            }
            VectorClassInfoValue::iterator it;
            it=vectClassInfoValuePtr->begin();
            CurrentClassID = it->getClass_id();
            
            if(vectClassInfoValuePtr)
                delete vectClassInfoValuePtr;
        }
        
        AttrInfoVectPtr = ptrIAppFramework->StructureCache_getItemAttributesForClassAndParents(CurrentClassID, md.languageID);
        if(AttrInfoVectPtr== NULL){
            ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::populateItemPanelLstbox::AttributeCache_getItemAttributesForClassAndParents's AttrInfoVectPtr is nil ");
            
            InterfacePtr<ITreeViewMgr> treeViewMgr(spTreeListBoxWidgetView, UseDefaultIID());
            if(!treeViewMgr)
            {
                ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");
                break;
            }
            
            CTGTreeModel pModel;
            PMString pfName("Root");
            pModel.setRoot(-1, pfName, 1);
            treeViewMgr->ClearTree(kTrue);
            pModel.GetRootUID();
            treeViewMgr->ChangeRoot();
            
            break;
        }
        
        int count=0;
        int nodeSeq = 0;
        PMString itemCountString("");
        int itemCountNum=0;
        int32 root = -1;
        
        VectorAttributeInfoValue::iterator it2;
        
        for(it2=AttrInfoVectPtr->begin();it2!=AttrInfoVectPtr->end();it2++)
        {
            if(it2->getDataType() == "ASSET")
                continue;
            
            //it2->getDisplayName()
            //it2->getAttributeId()
            
            PublicationNode pNodeChild;
            
            count++;
            pNodeChild.setTreeNodeId(count);
            pNodeChild.setTreeParentNodeId(root);
            
            pNodeChild.setPBObjectID(itemId);
            pNodeChild.setSequence(nodeSeq);//(it2->getIndex());
            pNodeChild.setIsProduct(6);
            pNodeChild.setIsONEsource(kFalse);
            pNodeChild.setIsStarred(kFalse);
            pNodeChild.setSectionID(curSelSubecId);
            pNodeChild.setPublicationID(md.CurrentSelectedPublicationID);
            pNodeChild.setParentId(curSelSubecId);
            pNodeChild.setTypeId(itemTypeId);
            pNodeChild.setSubSectionID(it2->getAttributeId()); // Used here to store attributeId of Item.
            pNodeChild.setNewProduct(0);
            
            pNodeChild.setPubId(itemId);
            
            PMString pName1("");
            pName1 = it2->getDisplayName();
            
            PMString itemAttributeValue = ("");
            itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemId, it2->getAttributeId(), md.languageID, curSelSubecId, kFalse );
            
            //pName1.Append("\t\t ");
            //pName1.Append(itemAttributeValue);
            
            pNodeChild.setPublicationName(pName1);
            
            pNodeChild.setTypeId(pNode.getTypeId());
            
            pNodeChild.setIsProduct(6);
            pNodeChild.setChildCount(0);
            pNodeChild.setChildItemCount(0);
            
            //ptrIAppFramework->LogDebug("populateChildItemsFromList 10");
            
            //ptrIAppFramework->LogDebug("itemAttributeValue :" + itemAttributeValue);
            
            pNodeChild.setItemAttributeValueStr(itemAttributeValue);
            
            int icon_count = 0;;
            pNodeChild.setIconCount(icon_count);
            
            pNodeDataList.push_back(pNodeChild);
            
        }
        
        /*
         VectorAttributeInfoValue::iterator it5;
         for(it5=AttrInfoVectPtr->begin();it5!=AttrInfoVectPtr->end();it5++)
         {//CAlert::InformationAlert("4");
         
         if(it5->getDataType() == "ASSET")
         {
         PMString displayName("");
         displayName.Append(it5->getDisplayName());
         
         ItemData.setData
         (
         4,                        // which List 4 = item List
         i,                        // Index
         it5->getAttributeId(),// Attribute Id for Core element
         -1,                    // type id ... here we are going to feed Item id
         TPLMediatorClass::CurrLanguageID,
         displayName,//it4->getDisplay_name(),
         kTrue,
         "ASSET",//it4->getDisplay_name(),// Code is replaced by Attribute Name
         kTrue,
         0,           //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
         kFalse,
         -1,-1,-1,-1,intSprItemPerFrameFlow,-1,1,-1,1
         );
         
         ITEMListData.setAll(displayName,it5->getAttributeId(),1,1,0,1);
         ITEMListData.setHitCount(1);
         ITEMDataNodeList.push_back(ITEMListData);
         count4=i;
         i++;
         
         }
         }
         */
        
        if( pNodeDataList.size() > 0 )
        {
            InterfacePtr<ITreeViewMgr> treeViewMgr(spTreeListBoxWidgetView, UseDefaultIID());
            if(!treeViewMgr)
            {
                ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");
                break;
            }
            
            CTGTreeModel pModel;
            PMString pfName("Root");
            pModel.setRoot(-1, pfName, 1);
            treeViewMgr->ClearTree(kTrue);
            pModel.GetRootUID();
            treeViewMgr->ChangeRoot();
            
            isListBoxPopulated = kTrue;
            CurrentSelectedProductRow =0;
            
        }
        else
        {
            //CA("isListBoxPopulated =  kFalse");
            isListBoxPopulated =  kFalse;
            //md.getRefreshButtonView()->Disable();
        }
        
        
    }while(kFalse);
    
    //CA("returning populateProductListforSection()");
    return isListBoxPopulated;
}



