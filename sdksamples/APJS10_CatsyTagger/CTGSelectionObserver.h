#include "VCPluginHeaders.h"
#include "ISelectionManager.h"
#include "SelectionObserver.h"
#include "CTGID.h"
#include "IPanelControlData.h"
#include "IAppFramework.h"
#include "PublicationNode.h"
#include "ITextModel.h"
#include "PMPathPoint.h"
#include "IPageItemScrapData.h"
#include "ITableModel.h"

class CTGSelectionObserver : public ActiveSelectionObserver
{
public:
    CTGSelectionObserver(IPMUnknown *boss);
    virtual ~CTGSelectionObserver();
    void AutoAttach();
    void AutoDetach();
    void Update(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy);
    void loadPaletteData();
    void populateSectionDropDownList(double);
    double getSelectedSectionId(IControlView* , PMString&);
    
    XMLReference TagFrameElement(const XMLReference& newElementParent,UID frameUID,PMString& frameTagName);
    void addTagToText(UIDRef& textFrameUIDRef,ITextModel* txtModel,PMString& displyName);
    XMLReference tagFrameElement(const XMLReference& newElementParent,UID& frameUID,const PMString& frameTagName);
    void attachAttributes(XMLReference* newTag,bool16 flag);
    PMString prepareTagName(PMString name);
    PMString keepOnlyAlphaNumeric(PMString name);

    double getSelectedAttributeId(IControlView* cntrlView, PMString& curAttributeName , bool16 &isAssetAttribute);
    bool8 populateTree(double curSelSubecId, double attributeId, bool16 isAssetAttribute);
    void populateChildItemsFromList(PublicationNode &pNode, int32 &count, bool16 isAssetAttribute);
    
    void populateListForNode(PublicationNode &pNode, int32 &count, bool16 isAssetAttribute);
    void populateListChildItemForNode(PublicationNode &pNode, int32 &count, bool16 isAssetAttribute, CItemTableValue &oTableValue);
    
    void populateListChildItemRowsForNode(PublicationNode &pNode, int32 &count, bool16 isAssetAttribute, CItemTableValue &oTableValue, int32 & rowCount);
    
    bool8 populateTreeForItemAttributes(double curSelSubecId, double itemId , double itemTypeId);
    //void populateFilteredProductList(double,PMString);
    
    
    
    //double getSelectedSubsectionId(IControlView* , PMString&);
    //double getSelectedSubSEctionId(IControlView* cntrlView);
    //void AddProductsToListbox(vector<bool8> DesignerActionFlags,IPMUnknown *boss);
    //void fillSubSectionList(double sectionid);
    //double getSelectedSubSectionID();
    //bool16	fileExists(PMString& path, PMString& name);
    
    //int GetIconCountForUnfilteredProductList(VectorPubObjectValue::iterator it2);
    //int GetIconCountWhenDesignerActionSelected(VectorPubObjectValue::iterator it2 , vector<bool8> DesignerActionFlags);
    //void SelectFrame(UIDList & itemList);
    
    //following function is added by vijay choudhari on 8-4-2006
    //void populateProductImageListBox();
    // new added by VAibhav 14_April
    
    //void showImagePanel();
    //void hideImagePanel();
    
    //void levelFour(); //Added on 22/09/2006 For level4 i.e. ONEsource only
    //void CallLevelTwoThree();
    
    //bool8 populateProductListforSectionWithDesignerActions(double curSelSubecId, bool8 new_product,bool8 add_to_spread,bool8 update_spread,bool8 update_copy,bool8 update_art,bool8 update_item_tables,bool8 delete_from_spread,  bool8 starred_product);
    
    //bool8 showImageAsThumbnail(PublicationNode& , double& );
    
    //bool16 createNewLayer();
    //void createCommentsOnLayer();
    //void createRectangle(PMRect box,int32 layerNo,UIDRef& frameUIDRef ,int32 maxSpread);
    //void createOval(PMRect box,int32 layerNo,IDocument* document,UIDRef& frameUIDRef);
    //void createArrowGraphic(PMRect box,int32 layerNo,IDocument* document,UIDRef& frameUIDRef,PMPathPointList& pathPointList);
    //bool16 SelectFrame(const UIDRef& frameUIDRef);
    //void moveCreatedFrame(UIDRef frameUIDRef,PMRect box);
    //bool16 getMaxLimitsOfBoxes(UIDList  TempUidList, PMRect& maxBounds);
    //int32 getNoOfPagesfromCurrentSread();
    //void getRelativeMoveAmountOfxAndy(int32 &X,int32 &Y,int32 numberOfPagesInCurrentSpread,int32 CurrentX,int32 CurrentY,int32 pageWidth,int32 sideOfPage);
    //void addLayerIfNeeded();
    //bool16 convertBoxToTextBox(UIDRef& boxUIDRef);
    //void addTagToGraphicFrame(UIDRef& curBox);
    
    
    //bool16 ImportFileInFrame(const UIDRef& imageBox, const PMString& fromPath);
    //void fitImageInBox(const UIDRef& boxUIDRef);
    //void AddOrDeleteSpreads(int32 maxSpreadNumber);
    //int32 getNoOfPagesfromSpreadNumber(int32 index);
    //void createRectangle(PMRect box,APpubComment objAPpubComment,UIDRef& frameUIDRef);
    //bool16 copyStencilsFromTheTemplateDocumentIntoScrapData(PMString & templateFilePath,InterfacePtr<IPageItemScrapData> & scrapData,UIDList &itemList);
    //bool16 pasteTheItemsFromScrapDataOntoOpenDocument(InterfacePtr<IPageItemScrapData> & scrapData,UIDRef &documentDocUIDRef ,UIDRef &layerRef);

    //void createTableFrame(PMRect box,int32 layerNo,UIDRef& frameUIDRef, int32 spreadNumber, int32 ObjectID, int32 tableTypeID, APpubComment objAPpubComment);
    //ErrorCode CreateTable(const UIDRef& storyRef, const TextIndex at, const int32 numRows,const int32 numCols,const PMReal rowHeight,	const PMReal colWidth, const CellType cellType = kTextContentType);
    //void resizeTextFrame(UIDRef itemRef,UIDList list, PMReal bottomX, PMReal bottomY);
    //void deleteCommentsOnLayer();
    //bool16 deleteSelectedCommentFromLayer(UIDRef& boxID,int32 parentIDToBeDelete,int32 pubCommentIDToBeDelete);
    //bool16 isTablePresent(const UIDRef&, UIDRef&, int32 tableNumber=1);
    //bool8 populateProductListforSectionforSearch (VectorObjectInfoPtr vectorObjectValuePtr, VectorItemModelPtr vectorItemModelPtr);
    
protected:
    IPanelControlData* QueryPanelControlData();
    void AttachWidget(IPanelControlData* , const WidgetID& , const PMIID&);
    void DetachWidget(IPanelControlData* , const WidgetID& , const PMIID&);
    //void SPRefresh();
    //void HandleSelectionChanged(const ISelectionMessage* selectionMessage);
    //void HandleSelectionAttributeChanged(const ISelectionMessage* selectionMessage);
    //		virtual void HandleSelectionChanged(void);
};
