#ifndef __MEDIATORCLASS_H__
#define __MEDIATORCLASS_H__

#include "VCPluginHeaders.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "IListBoxController.h"
#include <vector>
#include "PicklistValue.h"
using namespace std;



class Mediator
{
private:
	
	
	static IPanelControlData*	mainPanelCntrlData;
    static bool16 loadDataFlag;
    static IControlView *sectionDropDownView;
    static IControlView *attributeDropDownView;
    static int32 CurrentSelectedRow;
    static int32 CurrentAttributeRow;
    static double CurrentObjectID;
    
    /*
	static IPanelControlData*	mainPanelCntrlData1;
	static IControlView* view;//Static text view
	static IControlView* ListBoxView;//PF tree view
	static IControlView* IconButtonView;//Buton view
	static IControlView* IconTemplateButtonView;//Buton view
	static IControlView* RefreshButtonView;
	static double SelectedPubId;
	static double SelectedPublicationRoot;
	static IControlView *SprayButtonView;
	static IControlView *SubSecSprayButtonView;
	static IControlView *PreviewButtonView;
	static IControlView *DropDownView;
	static IControlView *AssignBtnView;
	static IControlView *showPubTreeView;	
	static IControlView * previewwidgetview;	
	static IControlView* DesignerMilestoneView;	
	

	
	static double CurrSubSectionID;
	
	
	static PMString CurSecName;
	static PMString CurSubSecName;
	
	static double PBObjectID;	

	static IControlView *GreenFilterBtnView;	
	static IControlView * locatewidgetview;

	static int32 CurrentSubSectionRow;
	static int32 CustDropDownListSelectedIndex;
	static int32 DivDropDownListSelectedIndex;
	static bool16 isMultipleSelection;

	static IControlView *tableSourceBtnCntViw;
	static IControlView *whiteBoardBtnCntViw;
	static IControlView *searchView;
     */

public:
	
    static double sectionID;
    static double languageID;
    static double CurrentSelectedPublicationID;
    static double attributeID;
    
    /*
	
	static double languageID;
	static double parentID;
	static double parentTypeID;
    
	// end Vaibhav_24April
	// new Vaibhav_21April
	static PMString imageType;
	// end Vaibhav_21April
	static UIDRef ImageBoxUIDRef;

	// new added by Vaibhav_14April
	static IListBoxController *imageListBoxController;
	// end new added by Vaibhav_14April
	
	static IPanelControlData *panelControlData1;
	static int32 imageIndex;
	static PMString toolTipText;
	static int32 ResizeFlag;
	//New addtion added by Yogesh on 25.2.05
	static bool16 isDesignerMilestoneAvailable;
	static K2Vector<double> vMultipleSelection;
	//ended by Yogesh on 25.2.05
	static int32 testFlag;	
	static vector<PicklistValue> CustomerVector;
	static vector<PicklistValue> DivisionVector;

	// Sunil FROM HERE
	static int32 FuncCalledCount;
	// Sunil TILL HERE


	

    */
    
    bool16 getLoadDataFlag(){return this->loadDataFlag;}
    void setLoadDataFlag(bool16 flag){loadDataFlag=flag;}
	
	IPanelControlData* getMainPanelCtrlData(){return this->mainPanelCntrlData;}
	void setMainPanelCntrlData(IPanelControlData* iData){this->mainPanelCntrlData=iData;}
    
    IControlView* getSectionDropDownView(void){ return sectionDropDownView; }
    void setSectionDropDownView(IControlView* view) { this->sectionDropDownView=view; }
    
    IControlView* getAttributeDropDownView(void){ return attributeDropDownView; }
    void setAttributeDropDownView(IControlView *view){ this->attributeDropDownView=view; }
    
    void setCurrentSelectedRow(double row){ this->CurrentSelectedRow = row;}
    double getCurrentSelectedRow(void) { return this->CurrentSelectedRow;}
    
    void setCurrentAttributeRow(int32 row){ this->CurrentAttributeRow = row;}
    int32 getCurrentAttributeRow(void) { return this->CurrentAttributeRow;}
    
    void setCurrentObjectID(double id){ this->CurrentObjectID = id;}
    double getCurrentObjectID(void) { return this->CurrentObjectID;}
    
    /*
	IControlView* getShowPubTreeView(void){ return showPubTreeView; }
	void setShowPubTreeView(IControlView* view) { this->showPubTreeView=view; }
	
	IControlView* getAssignBtnView(void){ return AssignBtnView; }
	void setAssignBtnView(IControlView* view) { this->AssignBtnView=view; }

	IControlView* getGreenFilterBtnView(void){ return GreenFilterBtnView; }
	void setGreenFilterBtnView(IControlView* view) { this->GreenFilterBtnView = view; }
	
	//IControlView* getPreviewButtonView(void){ return PreviewButtonView; }
	//void setPreviewButtonView(IControlView* view) { this->PreviewButtonView=view; }	

	

	IControlView* getSubSecSprayButtonView(void){ return SubSecSprayButtonView; }
	void setSubSecSprayButtonView(IControlView* view) { this->SubSecSprayButtonView=view; }

	IControlView* getRefreshButtonView(void){ return RefreshButtonView; }
	void setRefreshButtonView(IControlView* view) { this->RefreshButtonView=view; }
	
	IControlView* getSprayButtonView(void){ return SprayButtonView; }
	void setSprayButtonView(IControlView* view) { this->SprayButtonView=view; }
	
	IControlView* getPubNameTextView(void){ return view; }
	void setPubNameTextView(IControlView* view) { this->view=view; }
	
	IControlView* getListBoxView(void){ return ListBoxView; }
	void setListBoxView(IControlView *view){ this->ListBoxView=view; }

	IControlView* getDesignerActionWidgetView(void){ return DesignerMilestoneView ; }
	void setDesignerActionWidgetView(IControlView *view){ this->DesignerMilestoneView=view; }
	
	IControlView* getPreviewWidgetView(void){ return previewwidgetview ; }
	void setPreviewWidgetView(IControlView *view){ this->previewwidgetview=view; }
	
	IControlView* getLocateWidgetView(void){ return locatewidgetview ; }
	void setLocateWidgetView(IControlView *view){ this->locatewidgetview=view; }

	IControlView* getSectionDropDownLevel3View(void){ return SectionDropDownLevel3View ; }
	void setSectionDropDownLevel3View(IControlView *view){ this->SectionDropDownLevel3View=view; }
	
	
			
	void setSelectedPub(double id){ this->SelectedPubId=id; }
	double getSelectedPubId(void){ return SelectedPubId; }

	IControlView* getIconView(void) { return this->IconTemplateButtonView; }
	void setIconView(IControlView* view) { this->IconTemplateButtonView=view; }
	
	void setIconViewNew(IControlView* view) { this->IconButtonView=view; }
	IControlView* getIconViewNew(void) { return this->IconButtonView; }
	
	void setPublicationRoot(double id){ this->SelectedPublicationRoot=id; }
	double getPublicationRoot(void){ return this->SelectedPublicationRoot; }

	void setCurrSectionID(double id){ this->CurrSectionID= id; }
	double getCurrSectionID(void){ return this->CurrSectionID; }

	void setCurrSubSectionID(double id){ this->CurrSubSectionID = id;}
	double getCurrSubSectionID(void) { return this->CurrSubSectionID;}

	void setCurrentObjectID(double id){ this->CurrentObjectID = id;}
	double getCurrentObjectID(void) { return this->CurrentObjectID;}

	

	void setCurSecName(PMString row){ this->CurSecName = row;}
	PMString getCurSecName(void) { return this->CurSecName;}

	void setCurSubSecName(PMString row){ this->CurSubSecName = row;}
	PMString getCurSubSecName(void) { return this->CurSubSecName;}

	void setPBObjectID(double objectID)
	{
		this->PBObjectID = objectID;
	}

	double getPBObjectID()
	{
		return (PBObjectID);
	}

	IPanelControlData* getMainPanelCtrlData1(){return this->mainPanelCntrlData1;}
	void setMainPanelCntrlData1(IPanelControlData* iData){this->mainPanelCntrlData1=iData;}

	void setCurrentSubSectionRow(int32 row){ this->CurrentSubSectionRow = row;}
	int32 getCurrentSubSectionRow(void) { return this->CurrentSubSectionRow;}

	static void setDropDownListSelectedIndex(int32 index,int32 isCustomer){ 
		if(isCustomer == 1)	
			CustDropDownListSelectedIndex = index;
		else
			DivDropDownListSelectedIndex = index;
	}
	static int32 getDropDownListSelectedIndex(int32 isCustomer) { 
		if(isCustomer == 1)
			return CustDropDownListSelectedIndex;
		else
			return DivDropDownListSelectedIndex;
	}
	static void setIsMultipleSelection(bool16 sel)
	{
		isMultipleSelection = sel;
	}
	static bool16 getIsMultipleSelection()
	{
		return isMultipleSelection;
	}

	//-----
	IControlView* getTableSourceBtnView(void)
	{ 
		return tableSourceBtnCntViw; 
	}
	void setTableSourceBtnView(IControlView* view) 
	{ 
		this->tableSourceBtnCntViw = view; 
	}
	IControlView* getWhiteBoardBtnView(void)
	{ 
		return whiteBoardBtnCntViw; 
	}
	void setWhiteBoardBtnView(IControlView* view) 
	{ 
		this->whiteBoardBtnCntViw = view; 
	}

	IControlView* getSearchView(void){ return searchView; }
	void setSearchView(IControlView* view) { this->searchView=view; }
     
    */
};

#endif
