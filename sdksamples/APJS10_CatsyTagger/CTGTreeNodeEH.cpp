//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/CTGTreeNodeEH.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rahul $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: 1.2 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPluginHeaders.h"

// Interfaces
#include "ITreeNodeIDData.h"
#include "ILayoutUtils.h"
#include "IDocument.h"

// General includes:
#include "CEventHandler.h"
#include "CAlert.h"

// Project includes:
#include "CTGID.h"
#include "MediatorClass.h"
#include "ISelectionManager.h"
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
#include "ITextEditSuite.h"
//#include "ITextFrame.h"         //Removed from CS3 API
#include "ITextModel.h"
#include "ITableUtils.h"
#include "ITreeViewController.h"
#include "IntNodeID.h"
#include "PublicationNode.h"
#include "CTGTreeDataCache.h"
#include "ILayoutSelectionSuite.h"
//#include "ITableModelList.h"
#include "ILayoutUIUtils.h"
#include "IXMLUtils.h"
#include "ITextStoryThreadDict.h"
//#include "ITblBscSuite.h"
#include <IFrameUtils.h>
#include "CAlert.h"
//#include "IMessageServer.h"
#include "IHierarchy.h"
#include "ITextFrameColumn.h"
#include "SDKLayoutHelper.h"
#include "IGraphicFrameData.h"
#include "ITextTarget.h"
#include "ITextFocus.h"
#include "ITriStateControlData.h"
//#include "IDialogController.h"
//#include "ILoginHelper.h"
#include "MediatorClass.h"
#include "AcquireModalCursor.h"
#include "IDataSprayer.h"
#include "ISubSectionSprayer.h"
//#include "GlobalData.h"
#include "IAppFramework.h"
#include "K2Vector.tpp"
#include "IDocument.h"
#include "CTGCommonFunctions.h"

#define FILENAME			PMString("CTGTreeNodeEH.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAlert::InformationAlert(X)//CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
extern int32 CurrentSelectedProductRow;
extern CTGTreeDataCache dc;


//extern int32 firstpNodeListCount;
//extern bool16 singleSelectionSpray;
//extern bool16 isCancelButtonClick;
extern PublicationNodeList pNodeDataList; 
extern double CurrentSelectedSection;

/** 
	Implements IEventHandler; allows this plug-in's code 
	to catch the double-click events without needing 
	access to the implementation headers.

	@author Ian Paterson
	@ingroup paneltreeview
*/

class CTGTreeNodeEH : public CEventHandler
{
public:

	/** Constructor.
		@param boss interface ptr on the boss object to which the interface implemented here belongs.
	*/	
	CTGTreeNodeEH(IPMUnknown* boss);
	
	/** Destructor
	*/	
	virtual ~CTGTreeNodeEH(){}

	/**  Window has been activated. Traditional response is to
		activate the controls in the window.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Activate(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->Activate(e);  return retval; }
		
	/** Window has been deactivated. Traditional response is to
		deactivate the controls in the window.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Deactivate(IEvent* e) 
	{ bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->Deactivate(e);  return retval; }
	
	/** Application has been suspended. Control is passed to
		another application. 
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Suspend(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->Suspend(e);  return retval; }
	
	/** Application has been resumed. Control is passed back to the
		application from another application.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 Resume(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->Resume(e);  return retval; }
		
	/** Mouse has moved outside the sensitive region.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MouseMove(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->MouseMove(e);  return retval; } 
		 
	/** User is holding down the mouse button and dragging the mouse.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MouseDrag(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->MouseDrag(e);  return retval; }
		 
	/** Left mouse button (or only mouse button) has been pressed.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/ 
	virtual bool16 LButtonDn(IEvent* e);// { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->LButtonDn(e);  return retval; }
		 
	/** Right mouse button (or second button) has been pressed.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 RButtonDn(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->RButtonDn(e);  return retval; }
		 
	/** Middle mouse button of a 3 button mouse has been pressed.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MButtonDn(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->MButtonDn(e);  return retval; }
		
	/** Left mouse button released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 LButtonUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->LButtonUp(e);  return retval; } 
		 
	/** Right mouse button released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 RButtonUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->RButtonUp(e);  return retval; } 
		 
	/** Middle mouse button released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 MButtonUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->MButtonUp(e);  return retval; } 
		 
	/** Double click with any button; this is the only event that we're interested in here-
		on this event we load the placegun with an asset if it can be imported.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonDblClk(IEvent* e);
	/** Triple click with any button.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonTrplClk(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->ButtonTrplClk(e);  return retval; }
		 
	/** Quadruple click with any button.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonQuadClk(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->ButtonQuadClk(e);  return retval; }
		 
	/** Quintuple click with any button.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ButtonQuintClk(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->ButtonQuintClk(e);  return retval; }
		 
	/** Event for a particular control. Used only on Windows.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 ControlCmd(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->ControlCmd(e);  return retval; } 
		
		
	// Keyboard Related Events
	
	/** Keyboard key down for every key.  Normally you want to override KeyCmd, rather than KeyDown.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 KeyDown(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->KeyDown(e);  return retval; }
		 
	/** Keyboard key down that generates a character.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 KeyCmd(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->KeyCmd(e);  return retval; }
		
	/** Keyboard key released.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	virtual bool16 KeyUp(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->KeyUp(e);  return retval; }
		 
	
	// Keyboard Focus Related Functions
	
	/** Key focus is now passed to the window.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 GetKeyFocus(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->GetKeyFocus(e);  return retval; }
		
	/** Window has lost key focus.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
	//virtual bool16 GiveUpKeyFocus(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->GiveUpKeyFocus(e);  return retval; }
		
	/** Typically called before GiveUpKeyFocus() is called. Return kFalse
		to hold onto the keyboard focus.
		@return kFalse to hold onto the keyboard focus
	*/
	virtual bool16 WillingToGiveUpKeyFocus() { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->WillingToGiveUpKeyFocus();  return retval; }
		 
	/** The keyboard is temporarily being taken away. Remember enough state
		to resume where you left off. 
		@return kTrue if you really suspended
		yourself. If you simply gave up the keyboard, return kFalse.
	*/
	virtual bool16 SuspendKeyFocus() { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->SuspendKeyFocus();  return retval; }
		 
	/** The keyboard has been handed back. 
		@return kTrue if you resumed yourself. Otherwise, return kFalse.
	*/
	virtual bool16 ResumeKeyFocus() { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->ResumeKeyFocus();  return retval; }
		 
	/** Determine if this eventhandler can be focus of keyboard event 
		@return kTrue if this eventhandler supports being the focus
		of keyboard event
	*/
	virtual bool16 CanHaveKeyFocus() const { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->CanHaveKeyFocus();  return retval; }
		 
	/** Return kTrue if this event handler wants to get keyboard focus
		while tabbing through widgets. Note: For almost all event handlers
		CanHaveKeyFocus and WantsTabKeyFocus will return the same value.
		If WantsTabKeyFocus returns kTrue then CanHaveKeyFocus should also return kTrue
		for the event handler to actually get keyboard focus. If WantsTabKeyFocus returns
		kFalse then the event handler is skipped.
		@return kTrue if event handler wants to get focus during tabbing, kFalse otherwise
	*/
	virtual bool16 WantsTabKeyFocus() const { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->WantsTabKeyFocus();  return retval; }
		 

	/** Platform independent menu event
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/

	//---------------Removed from CS3 API
//	virtual bool16 Menu(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->Menu(e);  return retval; }
		 
	/** Window needs to repaint.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 Update(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->Update(e);  return retval; }
		
	/** Method to handle platform specific events
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 PlatformEvent(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->PlatformEvent(e);  return retval; }
		 
	/** Call the base system event handler.
		@param e [IN] event of interest
		@return kTrue if event has been handled and should not be further dispatched, kFalse otherwise (pass event to next handler)
	*/
//	virtual bool16 CallSysEventHandler(IEvent* e) { bool16 retval; InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  retval =  delegate->CallSysEventHandler(e);  return retval; }
		
		
	/** Temporary.
	*/
	virtual void SetView(IControlView* view)
	{  
		InterfacePtr<IEventHandler> 
			delegate(this,IID_ICTGSHADOWEVENTHANDLER); 
		delegate->SetView(view);  
	}
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE( CTGTreeNodeEH, kCTGTreeNodeEHImpl)

	
CTGTreeNodeEH::CTGTreeNodeEH(IPMUnknown* boss) :
	CEventHandler(boss)
{

}

bool16 CTGTreeNodeEH::ButtonDblClk(IEvent* e) 
{
	bool16 retval = kFalse; 
	InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);  
	retval =  delegate->LButtonDn(e);  
	
    
    
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CTGTreeNodeEH::LButtonDn::ptrIAppFramework == nil");
		return retval;
	}

    ptrIAppFramework->LogInfo("APJS10_CatsyTagger::CTGTreeNodeEH::ButtonDblClk::entered" );
    
	bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
	if(!isUserLoggedIn)			
		return retval;
			
	if(CurrentSelectedProductRow == -100)//-100 is taken purposely.-100 does not exists.So something has to be selected from the list box.
	{				
		ptrIAppFramework->LogInfo("APJS10_CatsyTagger::CTGTreeNodeEH::--CurrentSelectedProductRow == -100");
		return retval;
	}

	//added on 8Nov..
	vector<int32> multipleSelection ;
	PublicationNodeList temppNodeDataList = pNodeDataList;
   
    PublicationNodeList localpNodeDataList;
    PublicationNode localpNode;

    CTGCommonFunctions ctgCommonFunction;

	Mediator md;
	IControlView* iControlView = md.getMainPanelCtrlData()->FindWidget(kCTGTreeListBoxWidgetID);
	if(iControlView != nil) 
	{
		InterfacePtr<ITreeViewController> treeViewCntrl(iControlView, UseDefaultIID());
		if(treeViewCntrl==nil) 
		{
			//CA("treeViewCntrl==nil");
			ptrIAppFramework->LogError("APJS10_CatsyTagger::CTGTreeNodeEH::treeViewCntrl == nil");
			return retval;
		}

		NodeIDList selectedItem;
		treeViewCntrl->GetSelectedItemsDisplayOrder(selectedItem);
		if(selectedItem.size()<=0)
		{
			//CA("selectedItem.size()<=0");
			ptrIAppFramework->LogInfo("APJS10_CatsyTagger::CTGTreeNodeEH::selectedItem.size()<=0");
			return retval;
		}

		NodeID nid=selectedItem[0];
		TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);
		int32 uid= uidNodeIDTemp->Get();
    
				
		const int32 kSelectionLength = selectedItem.size();
		if(kSelectionLength > 1)
		{
            ptrIAppFramework->LogInfo("APJS10_CatsyTagger::CTGTreeNodeEH::ButtonDblClk::kSelectionLength > 1" );
			return retval;
		}
		else if(kSelectionLength == 1)
		{
            ptrIAppFramework->LogInfo("APJS10_CatsyTagger::CTGTreeNodeEH::ButtonDblClk::kSelectionLength == 1" );
			//CA("kSelectionLength == 1");
            localpNodeDataList.clear();
			for(int32 count = 0;count < kSelectionLength ; count++)
			{						
				NodeID nid=selectedItem[count];
				TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);
				int32 uid= uidNodeIDTemp->Get();
				
				bool16 isIdExist = dc.isExist(uid, localpNode );
						
				if(isIdExist)
				{
					localpNodeDataList.push_back(localpNode);
                    ptrIAppFramework->LogInfo("APJS10_CatsyTagger::CTGTreeNodeEH::ButtonDblClk::pNode=" + localpNode.getName());
				}
                else
                {
                    ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGTreeNodeEH::ButtonDblClk::No Node found" );
                    return retval;
                }
                    
			}

            if(localpNode.getIsProduct() == 5 )
            {
                return retval;
            }
            
            
            InterfacePtr<IDocument> document(Utils<ILayoutUIUtils>()->GetFrontDocument(),UseDefaultIID());
            if(document == nil)
            {
                //CA("document nil");
                return retval;
            }
            
            InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(document));
            if (rootElement == nil)
            {
                //CA("rootElement is NULL");
                return retval;
            }
            
            InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
            if(!iSelectionManager)
            {	//CA("!iSelectionManager");
                return retval;
            }
            UIDRef ref;
            
            double attributeId = localpNode.getSubSectionID();
            Attribute attributeObj=ptrIAppFramework->StructureCache_getItemAttributeModelByAttributeId( attributeId);
            
            if(localpNode.getIsStarred() == 0) // Text Attributes
            {

                InterfacePtr<ITextMiscellanySuite> txtSelectionSuite(static_cast<ITextMiscellanySuite* >
                                                                     ( Utils<ISelectionUtils>()->QuerySuite(/*ITextMiscellanySuite::*/IID_ITPLTEXTMISCELLANYSUITEE,iSelectionManager)));
                if(!txtSelectionSuite){
                    //CA("Please Select a Type Tool");
                }
                else
                {
                    bool16 ISTextFrame = txtSelectionSuite->GetFrameUIDRef(ref);
                    
                    if(	ISTextFrame)
                    {
                        //CA("ISTextFrame");
                        
                        
                        PMString result =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(attributeId, md.languageID);
                        ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGTreeNodeEH::ButtonDblClk::AttributeName:" + result);
                        
                        
                        
                        InterfacePtr<ITextEditSuite> textEditSuite(iSelectionManager, UseDefaultIID());
                        
                        result = ctgCommonFunction.prepareTagName(result);
                        
                        int32 startIndex=0;
                        int32 endIndex=0;
                        txtSelectionSuite->GetTextSelectionRange(startIndex, endIndex);
                        
                        if(startIndex == endIndex )
                        {
                            ptrIAppFramework->LogDebug("APJS10_CatsyTagger::CTGTreeNodeEH::ButtonDblClk::No Selection found");
                            return retval;
                        }
                        
                        PMString ASD("startIndex = ");
                        ASD.AppendNumber(startIndex);
                        ASD.Append(" endIndex = ");
                        ASD.AppendNumber(endIndex);
                        ptrIAppFramework->LogDebug("Selection Range: "+ ASD);
                        
                        InterfacePtr<IHierarchy> graphicFrameHierarchy(ref, UseDefaultIID());
                        if (graphicFrameHierarchy == nil)
                        {
                            //CA("graphicFrameHierarchy is NULL");
                            return retval;
                        }
                        
                        int32 chldCount = graphicFrameHierarchy->GetChildCount();

                        InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
                        if (!multiColumnItemHierarchy) {
                            //CA("multiColumnItemHierarchy is NULL");
                        }
                        
                        InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
                        if (!multiColumnItemTextFrame) {
                            //CA("Its Not MultiColumn");
                            return retval;
                        }
                        
                        InterfacePtr<ITextModel> txtModel(multiColumnItemTextFrame->QueryTextModel());
                        if(!txtModel)
                        {
                            //	CA("!txtModel" );
                            return retval;
                        }
                        //---------------------CS3 TextFrame Change--------------------------------------------------------------------//
                        UIDRef ref1=::GetUIDRef(multiColumnItemTextFrame);
                        //UIDRef ref1 = ref;
                        bool16 isInsideTable = kFalse;
                        isInsideTable=Utils<ITableUtils>()->InsideTable(txtModel, startIndex);
                        if(isInsideTable)
                        {
                            ptrIAppFramework->LogDebug("Selection Range: isInsideTable");
                            TextIndex TablStartIndex = Utils<ITableUtils>()->TableToPrimaryTextIndex(txtModel,startIndex);
                            UIDRef tableRef1=( Utils<ITableUtils>()->GetTableModel(txtModel, TablStartIndex));
                            if (textEditSuite && textEditSuite->CanEditText())
                            {
                                result.ParseForEmbeddedCharacters();
                                //ErrorCode status = textEditSuite->InsertText(WideString(result));
                                //	ASSERT_MSG(status == kSuccess, "WFPDialogController::ApplyDialogFields: can't insert text"); 
                            }

                            PMString CellTagName = ctgCommonFunction.prepareTagName(result);

                            XMLReference xmlRef;
                            ctgCommonFunction.TagTable(tableRef1, ref1, "PRINTsourceTable", CellTagName, xmlRef,  localpNode, attributeObj, startIndex, endIndex);
                            
                            return retval;
                            
                        }
                        else 
                        {

                            ptrIAppFramework->LogDebug("Selection Range: is NOT InsideTable");
                            ctgCommonFunction.addTagToTextDoubleClickVersion( ref , localpNode, attributeObj, startIndex, endIndex );
                            
                            return retval;
                        }
                    }
                }
            }
            else if(localpNode.getIsStarred() == 1) // Image Attribute
            {
                InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
                if(!iSelectionManager)
                {	//CA("Slection nil");
                    //LogPtr->LogError("NO Iteam is selected IN Selection obsever iSelectionManager is NIL");
                    return retval;
                }
                
                InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
                                                               ( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager)));
                if(!txtMisSuite)
                {	//LogPtr->LogError("NO Iteam is selected IN Selection obsever iSelectionManager is NIL");
                    //CA("My Suit nil");
                    return retval;
                }
                
                UIDList uidLst;
                txtMisSuite->GetUidList(uidLst);
                UIDRef imageBox=uidLst.GetRef(0);
                
                bool16 isTextFrame = kFalse;
                
                UID textFrameUID = kInvalidUID;
                InterfacePtr<IGraphicFrameData> graphicFrameData(imageBox, UseDefaultIID());
                if (graphicFrameData)
                {
                    textFrameUID = graphicFrameData->GetTextContentUID();
                    if(textFrameUID != kInvalidUID)
                    {
                        isTextFrame = kTrue;
                        return retval;
                    }
                }
                
                if(isTextFrame== kFalse)
                {
                    ctgCommonFunction.addTagToGraphicFrame(imageBox, localpNode, attributeObj);
                    return retval;
                }
                
                

            }
		}
	}

	return retval;
} 

bool16 CTGTreeNodeEH::LButtonDn(IEvent* e)
{
    bool16 retval = kFalse;
    InterfacePtr<IEventHandler> delegate(this,IID_ICTGSHADOWEVENTHANDLER);
    retval =  delegate->LButtonDn(e);
	/*
    //CA("LButton Down");

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CTGTreeNodeEH::LButtonDn::ptrIAppFramework == nil");
		return retval;
	}

	Mediator md;


	IControlView* iControlView = md.getMainPanelCtrlData()->FindWidget(kSPTreeListBoxWidgetID);
	if(iControlView != nil) 
	{
					
		InterfacePtr<ITreeViewController> treeViewCntrl(iControlView, UseDefaultIID());
		if(treeViewCntrl==nil) 
		{
			//CA("treeViewCntrl==nil");
			ptrIAppFramework->LogError("AP7_ProductFinder::SPSelectionObserver::Update::treeViewCntrl == nil");			
			return retval;
		}

		NodeIDList selectedItem;
			
		treeViewCntrl->GetSelectedItemsDisplayOrder(selectedItem);
		if(selectedItem.size()<=0)
		{
			//CA("selectedItem.size()<=0");
			ptrIAppFramework->LogInfo("AP7_ProductFinder::SPSelectionObserver::Update::selectedItem.size()<=0");			
			return retval;
		}

		NodeID nid=selectedItem[0];
		TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);
		int32 uid= uidNodeIDTemp->Get();
				
		const int32 kSelectionLength = selectedItem.size();
		PublicationNode pNode;
		if(kSelectionLength > 0)
		{	
			//CA("kSelectionLength > 1");	
									
			NodeID nid=selectedItem[0];
			TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);
			int32 uid= uidNodeIDTemp->Get();				
			bool16 isIdExist = dc.isExist(uid, pNode );		
			CurrentSelectedProductRow = pNode.getSequence();
			
		}
		
		/////Refresh TABLESource
		InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
		if(ptrTableSourceHelper != nil)
		{
			AcquireWaitCursor awc ;
			awc.Animate(); 
			if(ptrTableSourceHelper->isTABLESourcePanelOpen())
			{
				double objectId = pNode.getPubId();
				double pbObjectID = pNode.getPBObjectID();
				
				if(pNode.getIsProduct() == 1)
				{
					checkTreeType=3;
					ptrTableSourceHelper->showTableList(objectId , global_lang_id , Mediator::parentID , pNode.getTypeId() , Mediator::sectionID ,checkTreeType,pbObjectID);
				}
				if(pNode.getIsProduct() == 0)
				{
					checkTreeType=4;
					ptrTableSourceHelper->showTableList(objectId , global_lang_id , Mediator::parentID ,  pNode.getTypeId() , Mediator::sectionID ,checkTreeType,pbObjectID);
				}								
			}			
		}

				

		//------------------------------------refresh image list box ---------------------------------------//
				
		InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
		if(ptrImageHelper != nil)
		{	
			AcquireWaitCursor awc ;
			awc.Animate() ; 
			if(ptrImageHelper->isImagePanelOpen())
			{

				PMString itemFieldIds("");
				PMString itemAssetTypeIds("");
				PMString itemGroupFieldIds("");
				PMString itemGroupAssetTypeIds("");
				PMString listTypeIds("");
				PMString listItemFieldIds("");
				bool16 isSprayItemPerFrameFlag = kTrue;
				double langId = ptrIAppFramework->getLocaleId();
				PMString currentSectionitemGroupIds("");
				PMString currentSectionitemIds("");
				

				if(pNode.getIsProduct() == 1)
				{			
					isSprayItemPerFrameFlag = kTrue;
					currentSectionitemGroupIds.AppendNumber(PMReal(pNode.getPubId()));
				}
				else if(pNode.getIsProduct() == 0)
				{
					isSprayItemPerFrameFlag = kFalse;
					currentSectionitemIds.AppendNumber(PMReal(pNode.getPubId()));
				}
			
				ptrIAppFramework->clearAllStaticObjects();

				ptrIAppFramework->EventCache_setCurrentSectionData( CurrentSelectedSection, langId , currentSectionitemGroupIds, currentSectionitemIds, itemFieldIds, itemAssetTypeIds, itemGroupFieldIds,itemGroupAssetTypeIds,listTypeIds, listItemFieldIds, kFalse, isSprayItemPerFrameFlag);



				double objectId = pNode.getPubId();

				if(pNode.getIsProduct() == 1)
					ptrImageHelper->showProductImages(objectId , global_lang_id , Mediator::parentID , Mediator::parentTypeID , Mediator::sectionID ) ;
				if(pNode.getIsProduct() == 0)
					ptrImageHelper->showItemImages(objectId , global_lang_id , Mediator::parentID , pNodeDataList[CurrentSelectedProductRow].getTypeId() , Mediator::sectionID, kFalse);				
			}
		}
	}


     */
	return retval;

}
//	end, File:	CTGTreeNodeEH.cpp
