#include "VCPlugInHeaders.h"
#include "IControlView.h"
#include "ITreeViewHierarchyAdapter.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "IntNodeID.h"
#include "CTreeViewWidgetMgr.h"
#include "CreateObject.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "SysControlIds.h"
#include "CTGID.h"
#include "CTGTreeModel.h"
#include "PublicationNode.h"
#include "MediatorClass.h"
#include "CAlert.h"
#include "CTGTreeDataCache.h"
//#include "ISpecialChar.h"
//#include "ISpecialChar.h"
#include "CAlert.h"
#include "IAppFramework.h"
#include "IBooleanControlData.h"
#include "IUIFontSpec.h"
//#include "IMessageServer.h"
//#define FILENAME			PMString("CTGTreeViewWidgetMgr.cpp")
//#define FUNCTIONNAME		PMString(__FUNCTION__)
//#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
//#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
//#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
#define CA(X) CAlert::InformationAlert(X); 



class CTGTreeViewWidgetMgr: public CTreeViewWidgetMgr
{
public:
	CTGTreeViewWidgetMgr(IPMUnknown* boss);
	virtual ~CTGTreeViewWidgetMgr() {}
	virtual	IControlView*	CreateWidgetForNode(const NodeID& node) const;
	virtual	WidgetID		GetWidgetTypeForNode(const NodeID& node) const;
	virtual	bool16 ApplyNodeIDToWidget
		( const NodeID& node, IControlView* widget, int32 message = 0 ) const;
	virtual PMReal GetIndentForNode(const NodeID& node) const;
private:
	PMString getNodeText(const int32& uid, int32 *RowNo) const;
	void indent( const NodeID& node, IControlView* widget, IControlView* staticTextWidget ) const;
	enum {ePFTreeIndentForNode=3};
};	

CREATE_PMINTERFACE(CTGTreeViewWidgetMgr, kCTGTreeViewWidgetMgrImpl)

CTGTreeViewWidgetMgr::CTGTreeViewWidgetMgr(IPMUnknown* boss) :
	CTreeViewWidgetMgr(boss)
{	//CA("Inside WidgetManager constructor");
}

IControlView* CTGTreeViewWidgetMgr::CreateWidgetForNode(const NodeID& node) const
{	//CA("CreateWidgetForNode");
	IControlView* retval =
		(IControlView*) ::CreateObject(::GetDataBase(this),
							RsrcSpec(LocaleSetting::GetLocale(), 
							kCTGPluginID,
							kViewRsrcType, 
							kCTGTreePanelNodeRsrcID),IID_ICONTROLVIEW);
	ASSERT(retval);
	return retval;
}

WidgetID CTGTreeViewWidgetMgr::GetWidgetTypeForNode(const NodeID& node) const
{	//CA("GetWidgetTypeForNode");
	return kCTGTreeParentWidgetId;
}

bool16 CTGTreeViewWidgetMgr::ApplyNodeIDToWidget
(const NodeID& node, IControlView* widget, int32 message) const
{	
	CTreeViewWidgetMgr::ApplyNodeIDToWidget( node, widget );
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return kFalse;
	}
	do
	{
	
		InterfacePtr<IPanelControlData> panelControlData(widget, UseDefaultIID());
		ASSERT(panelControlData);
		if(panelControlData==nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::CTGTreeViewWidgetMgr::ApplyNodeIDToWidget::panelControlData is nil");		
			break;
		}

		IControlView*   expanderWidget = panelControlData->FindWidget(kCTGTreeNodeExpanderWidgetID);
		ASSERT(expanderWidget);
		if(expanderWidget == nil) 
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::CTGTreeViewWidgetMgr::expanderWidget is nil");		
			break;
		}

		IControlView*   spTextWidget = panelControlData->FindWidget(kCTGTextWidgetID);
		ASSERT(spTextWidget);
		if(spTextWidget == nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::CTGTreeViewWidgetMgr::ApplyNodeIDToWidget::spTextWidget is nil");
			break;
		}
        

		IControlView*   spCountStaticTextWidget = panelControlData->FindWidget(kCTGcountStaticTextWidgetID);
		ASSERT(spCountStaticTextWidget);
		if(spCountStaticTextWidget == nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::CTGTreeViewWidgetMgr::ApplyNodeIDToWidget::spCountStaticTextWidget is nil");
			break;
		}

		IControlView*   spLeadingItemStaticTextWidget = panelControlData->FindWidget(kCTGChildItemStaticTextWidgetID);
		ASSERT(spLeadingItemStaticTextWidget);
		if(spLeadingItemStaticTextWidget == nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::TPLSelectionObserver::ApplyNodeIDToWidget::spLeadingItemStaticTextWidget is nil");
			break;
		}
        
        
        IControlView*   spListStaticTextWidget = panelControlData->FindWidget(kCTGListStaticTextWidgetID);
        ASSERT(spListStaticTextWidget);
        if(spListStaticTextWidget == nil)
        {
            ptrIAppFramework->LogDebug("APJS9_ProductFinder::CTGTreeViewWidgetMgr::ApplyNodeIDToWidget::spListStaticTextWidget is nil");
            break;
        }
        
        IControlView*   spAttributeStaticTextWidget = panelControlData->FindWidget(kCTGAttributeStaticTextWidgetID);
        ASSERT(spAttributeStaticTextWidget);
        if(spAttributeStaticTextWidget == nil)
        {
            ptrIAppFramework->LogDebug("APJS9_ProductFinder::CTGTreeViewWidgetMgr::ApplyNodeIDToWidget::spAttributeStaticTextWidget is nil");
            break;
        }
        
        
		IControlView*   spItemStaticTextWidget = panelControlData->FindWidget(kCTGItemStaticTextWidgetID);
		if(spItemStaticTextWidget == nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::TPLSelectionObserver::ApplyNodeIDToWidget::spItemStaticTextWidget is nil");
			break;
		}
	
		IControlView*   spItemGroupStaticTextWidget = panelControlData->FindWidget(kCTGItemGroupStaticTextWidgetID);
		ASSERT(spItemGroupStaticTextWidget);
		if(spItemGroupStaticTextWidget == nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::CTGTreeViewWidgetMgr::ApplyNodeIDToWidget::spItemGroupStaticTextWidget is nil");		
			break;
		}
	
        IControlView*   ctgCopyIconWidget = panelControlData->FindWidget(kCTGTreeCopyIconSuiteWidgetID);
        if(ctgCopyIconWidget == nil)
        {
            ptrIAppFramework->LogDebug("APJS9_ProductFinder::TPLSelectionObserver::ApplyNodeIDToWidget::ctgCopyIconWidget is nil");
            break;
        }
        
        IControlView*   ctgImageIconWidget = panelControlData->FindWidget(kCTGTreeImageIconSuiteWidgetID);
        if(ctgImageIconWidget == nil)
        {
            ptrIAppFramework->LogDebug("APJS9_ProductFinder::TPLSelectionObserver::ApplyNodeIDToWidget::ctgImageIconWidget is nil");
            break;
        }

        
        IControlView*   ctgListIconWidget = panelControlData->FindWidget(kCTGTreeListIconSuiteWidgetID);
        if(ctgListIconWidget == nil)
        {
            ptrIAppFramework->LogDebug("APJS9_ProductFinder::TPLSelectionObserver::ApplyNodeIDToWidget::ctgListIconWidget is nil");
            break;
        }
        
		InterfacePtr<const ITreeViewHierarchyAdapter>   adapter(this, UseDefaultIID());
		ASSERT(adapter);
		if(adapter==nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::CTGTreeViewWidgetMgr::ApplyNodeIDToWidget::adapter is nil");		
			break;
		}

		CTGTreeModel model;
		TreeNodePtr<IntNodeID>  uidNodeIDTemp(node);
		int32 uid= uidNodeIDTemp->Get();

		PublicationNode pNode;
		CTGTreeDataCache dc;
        Mediator md;

		dc.isExist(uid, pNode);

		TreeNodePtr<IntNodeID>  uidNodeID(node);
		ASSERT(uidNodeID);
		if(uidNodeID == nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::CTGTreeViewWidgetMgr::ApplyNodeIDToWidget::uidNodeID is nil");		
			break;
		}

		int32 *RowNo= NULL;
		int32 LocalRowNO;
		RowNo = &LocalRowNO;
        
        Bool16 isChildItem = kFalse;
        
        //if(pNode.getParentId() != pNode.getSectionID())
        //    isChildItem = kTrue;

		PMString stringToDisplay( this->getNodeText(uidNodeID->Get(), RowNo));
		stringToDisplay.SetTranslatable( kFalse );
		int result = -1; 

		int32 isProduct =pNode.getIsProduct();
        
        
        //if(isChildItem)
        //    isProduct = 3;

		if(isProduct == 2)
		{
			//CA("isProduct == 2");
			spItemStaticTextWidget->HideView();
			spItemGroupStaticTextWidget->HideView();
			//spTableStaticTextWidget->ShowView();
			spLeadingItemStaticTextWidget->HideView();
			//spSingleItemStaticTextWidget->HideView();
            ctgCopyIconWidget->HideView();
            ctgImageIconWidget->HideView();
            spListStaticTextWidget->HideView();
            spAttributeStaticTextWidget->HideView();
            ctgListIconWidget->HideView();
		}
		else if(isProduct == 1)
		{
			//CA("isProduct == 1");
			//spTableStaticTextWidget->HideView();
			spItemStaticTextWidget->HideView();
			spItemGroupStaticTextWidget->ShowView();
			spLeadingItemStaticTextWidget->HideView();
			//spSingleItemStaticTextWidget->HideView();
            spListStaticTextWidget->HideView();
            spAttributeStaticTextWidget->HideView();
            ctgListIconWidget->HideView();
		}
		else if(isProduct == 0)
		{	
			//CA("isProduct == 0");
			//spTableStaticTextWidget->HideView();
			spItemGroupStaticTextWidget->HideView();
			spItemStaticTextWidget->ShowView();
			spLeadingItemStaticTextWidget->HideView();
			//spSingleItemStaticTextWidget->HideView();
            ctgCopyIconWidget->ShowView();
            ctgImageIconWidget->HideView();
            spListStaticTextWidget->HideView();
            spAttributeStaticTextWidget->HideView();
            ctgListIconWidget->HideView();
		}
		else if(isProduct == 3)	//---For Leading Item
		{
			spLeadingItemStaticTextWidget->ShowView();
			//spTableStaticTextWidget->HideView();
			spItemGroupStaticTextWidget->HideView();
			spItemStaticTextWidget->HideView();
			//spSingleItemStaticTextWidget->HideView();
            ctgCopyIconWidget->ShowView();
            ctgImageIconWidget->HideView();
            spListStaticTextWidget->HideView();
            spAttributeStaticTextWidget->HideView();
            ctgListIconWidget->HideView();
		}
		else if(isProduct == 4)	//---For Single Item
		{
			//spSingleItemStaticTextWidget->ShowView();
			spLeadingItemStaticTextWidget->ShowView();
			//spTableStaticTextWidget->HideView();
			spItemGroupStaticTextWidget->HideView();
			spItemStaticTextWidget->HideView();
            spListStaticTextWidget->HideView();
            ctgCopyIconWidget->ShowView();
            ctgImageIconWidget->HideView();
            spAttributeStaticTextWidget->HideView();
            ctgListIconWidget->HideView();
            
		}
        else if(isProduct == 5)	//---For List
        {
            //spSingleItemStaticTextWidget->ShowView();
            spLeadingItemStaticTextWidget->HideView();
            //spTableStaticTextWidget->HideView();
            spItemGroupStaticTextWidget->HideView();
            spItemStaticTextWidget->HideView();
            ctgCopyIconWidget->HideView();
            ctgImageIconWidget->HideView();
            spListStaticTextWidget->ShowView();
            spAttributeStaticTextWidget->HideView();
            ctgListIconWidget->ShowView();
            
        }else if(isProduct == 6)	//---For Attributes
        {
            //spSingleItemStaticTextWidget->ShowView();
            spLeadingItemStaticTextWidget->HideView();
            //spTableStaticTextWidget->HideView();
            spItemGroupStaticTextWidget->HideView();
            spItemStaticTextWidget->HideView();
            ctgCopyIconWidget->HideView();
            ctgImageIconWidget->HideView();
            spListStaticTextWidget->HideView();
            spAttributeStaticTextWidget->ShowView();
            isChildItem = kTrue;
            ctgListIconWidget->HideView();
            
        }else if(isProduct == 7)	//---For Attributes
        {
            //spSingleItemStaticTextWidget->ShowView();
            spLeadingItemStaticTextWidget->HideView();
            //spTableStaticTextWidget->HideView();
            spItemGroupStaticTextWidget->HideView();
            spItemStaticTextWidget->HideView();
            ctgCopyIconWidget->HideView();
            ctgImageIconWidget->HideView();
            spListStaticTextWidget->HideView();
            spAttributeStaticTextWidget->ShowView();
            isChildItem = kTrue;
            ctgListIconWidget->HideView();
            
        }
        
        
        if(adapter->GetNumChildren(node)<=0)
            expanderWidget->HideView();	
        else
            expanderWidget->ShowView();
		

		IControlView* displayStringView = panelControlData->FindWidget( kCTGTextWidgetID );
		ASSERT(displayStringView);
		if(displayStringView == nil) 
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::CTGTreeViewWidgetMgr::ApplyNodeIDToWidget::displayStringView is nil");
			break;
		}
		InterfacePtr<ITextControlData>  textControlData( displayStringView, UseDefaultIID() );
		ASSERT(textControlData);
		if(textControlData== nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::CTGTreeViewWidgetMgr::ApplyNodeIDToWidget::textControlData is nil");		
			break;		
		}
		
        


		InterfacePtr<ITextControlData> itemCountControlData(spCountStaticTextWidget,IID_ITEXTCONTROLDATA);
		if(itemCountControlData == nil)
        {
            ptrIAppFramework->LogDebug("APJS9_ProductFinder::CTGTreeViewWidgetMgr::ApplyNodeIDToWidget::itemCountControlData is nil");
			break;
        }
        
        if(!isChildItem && isProduct != 4)
        {
            spCountStaticTextWidget->ShowView();
            PMString itemListCount("");
            if(pNode.getChildItemCount() > 0)
            {
                itemListCount.Append("(");
                itemListCount.AppendNumber(pNode.getChildItemCount());
                itemListCount.Append(")");
                itemListCount.SetTranslatable(kFalse);
                spCountStaticTextWidget->ShowView();
            }
            else
            {
                itemListCount.Append("(");
                itemListCount.AppendNumber(0);
                itemListCount.Append(")");
                itemListCount.SetTranslatable(kFalse);
                spCountStaticTextWidget->ShowView();
            }
            itemListCount.ParseForEmbeddedCharacters();
            itemCountControlData->SetString(itemListCount);
        }
        else
        {
            spCountStaticTextWidget->HideView();
        }
        
        /*
        IControlView* AttributeValueStringView = panelControlData->FindWidget( kCTGAttributeValueTextWidgetID );
        ASSERT(AttributeValueStringView);
        if(AttributeValueStringView == nil)
        {
            ptrIAppFramework->LogDebug("APJS9_ProductFinder::CTGTreeViewWidgetMgr::ApplyNodeIDToWidget::AttributeValueStringView is nil");
            break;
        }
        
        InterfacePtr<ITextControlData>  AttributetextControlData( AttributeValueStringView, UseDefaultIID() );
        ASSERT(AttributetextControlData);
        if(AttributetextControlData== nil)
        {
            ptrIAppFramework->LogDebug("APJS9_ProductFinder::CTGTreeViewWidgetMgr::ApplyNodeIDToWidget::AttributetextControlData is nil");
            break;
        }
        
         */
        PMString attributeValue("");

        
        if(isProduct == 0 || isProduct == 3 || isProduct == 4 || isProduct == 6 || isProduct == 7)
        {
            if(pNode.getIsStarred())
            {
                attributeValue = "AsId:";
                attributeValue.Append(pNode.getItemAttributeValueStr());
            }
            else
                attributeValue = pNode.getItemAttributeValueStr();
            
            if(pNode.getIsStarred())
            {
                ctgImageIconWidget->ShowView();
                ctgCopyIconWidget->HideView();
            }
            else
            {
                ctgImageIconWidget->HideView();
                ctgCopyIconWidget->ShowView();
            }
        }
        
        //ptrIAppFramework->LogDebug("attributeValue: " + attributeValue );
        attributeValue.ParseForEmbeddedCharacters();
        attributeValue.SetTranslatable( kFalse );
        
        stringToDisplay.Append(":\t ");
        stringToDisplay.Append(attributeValue);
        
        stringToDisplay.ParseForEmbeddedCharacters();
        
        textControlData->SetString(stringToDisplay);
        
        //AttributetextControlData->SetString(attributeValue);
        //AttributeValueStringView->ShowView();
        
        
        
//        IControlView* separatorControlView = panelControlData->FindWidget( kCTGSeparatorWidgetID );
//        ASSERT(separatorControlView);
//        if(separatorControlView == nil)
//        {
//            ptrIAppFramework->LogDebug("APJS9_ProductFinder::CTGTreeViewWidgetMgr::ApplyNodeIDToWidget::separatorControlView is nil");
//            break;
//        }
//        separatorControlView->ShowView();
        

		//CA(itemListCount);
		this->indent( node, widget, displayStringView );
	} while(kFalse);
	return kTrue;
}

PMReal CTGTreeViewWidgetMgr::GetIndentForNode(const NodeID& node) const
{	//CA("Inside GetIndentForNode");
	do
	{
		TreeNodePtr<IntNodeID>  uidNodeID(node);
		ASSERT(uidNodeID);
		if(uidNodeID == nil) 
			break;
		
		CTGTreeModel model;
		int nodePathLengthFromRoot = model.GetNodePathLengthFromRoot(uidNodeID->Get());

		if( nodePathLengthFromRoot <= 0 ) 
			return 0.0;
		
		return  PMReal((nodePathLengthFromRoot * ePFTreeIndentForNode)+0.5);
	} while(kFalse);
	return 0.0;
}

PMString CTGTreeViewWidgetMgr::getNodeText(const int32& uid, int32 *RowNo) const
{	//CA("getNodeText");
	CTGTreeModel model;
	return model.ToString(uid, RowNo);
}

void CTGTreeViewWidgetMgr::indent( const NodeID& node, IControlView* widget, IControlView* staticTextWidget) const
{	//CA("Inside indent");
	const PMReal indent = this->GetIndent(node);	
	PMRect widgetFrame = widget->GetFrame();
	widgetFrame.Left() = indent;
	widget->SetFrame( widgetFrame );
	staticTextWidget->WindowChanged();
	PMRect staticTextFrame = staticTextWidget->GetFrame();
	staticTextFrame.Right( widgetFrame.Right()+150 );
	
   
    //PMRect staticTextFrame2 = staticTextWidget2->GetFrame();
    //staticTextFrame2.Right( widgetFrame.Right()+1500);
    
    widgetFrame.Right(widgetFrame.Right()+150);
    widget->SetFrame(widgetFrame);
    
    
	
	staticTextWidget->SetFrame( staticTextFrame );
    //staticTextWidget2->SetFrame( staticTextFrame2 );
}
	
