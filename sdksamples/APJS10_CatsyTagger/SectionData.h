#ifndef __SectionData_h__
#define __SectionData_h__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "vector"

using namespace std;

class PubData{
	private:
		double pubId;
		PMString pubName;
		int32 pubLvlNo;
		double root_ID;
		double type_ID;
		PMString Comment;		//Added By Rahul
	public:
		PubData(){
			this->pubId=-1;
			this->pubName.Clear();
			this->pubLvlNo=-1;
			this->root_ID=-1;
			this->type_ID=-1;
			this->Comment.Clear();
		}
		double getPubId(void){
			return this->pubId;
		}
		void setPubId(double id){
			this->pubId=id;
		}
		PMString getPubName(void){
			return this->pubName;
		}
		void setPubName(PMString name){
			this->pubName=name;
		}
		double getroot_ID(void){
			return this->root_ID;
		}
		void setroot_ID(double lvl){
			this->root_ID=lvl;
		}

		void setPubLvlNo(int32 lvl){
			this->pubLvlNo=lvl;
		}
		int32 getPubLvlNo(void){
			return this->pubLvlNo;
		}
		void settype_ID(double lvl){
			this->type_ID=lvl;
		}
		double gettype_ID(void){
			return this->type_ID;
		}

		PMString getComment(void){
			return this->Comment;
		}
		void setComment(PMString name){
			this->Comment=name;
		}
};

class SectionData{
	private:
		static vector<PubData> sectionInfoVector;
		static vector<PubData> attributeInfoVector;
	public:
		void setSectionVectorInfo(double,PMString,int32,double, double, PMString);
		PubData getSectionVectorInfo(int32);
		void clearSectionVector();
		void setAttributeVectorInfo(double,PMString,int32, double, double, PMString);
		PubData getAttributeVectorInfo(int32);
		void clearAttributeVector();
};
#endif
