#ifndef __CTGTREEDATACACHE_H__
#define __CTGTREEDATACACHE_H__

#include "VCPlugInHeaders.h"
#include "PublicationNode.h"
#include "map"
#include "vector"


#define MAX_HITS_BEFORE_STALE	100	//After these hits, the data is stale

using namespace std;

class CTGTreeDataCache
{
private:
	static map<int32, PublicationNode>* dataCache;
public:
	 /*
		Ctor
		Will create a new map if map is NULL
	 */
	 CTGTreeDataCache();
	 /*
		Will check for the existence of data in the cache.
		@param1 the key to search for data
		@param2 out parameter containing the data
		@returns true if value found, else false
	 */
	 bool16 isExist(int32, PublicationNode&);
	 /*
		will check for a particular having parent id as param1 and sequence as param2
		@param1 parentid
		@param2 sequence
		@param3 out parameter containing the data
		@returns true if condition found, else false
	 */
	 bool16 isExist(int32 a, int32, PublicationNode&);
	 /*
		Will add a new node in the cache
		@param1 the data node. The pubId in the node will serve as the 
		unique key for  map
		@returns true if added successfully
	 */
	 bool16 add(PublicationNode&);
	 /*
		Will clear the map of all the data
		@returns true if the map was cleared successfully
	 */
	 bool16 clearMap(void);
	 /*
		get the idList for the level specified
	 */
	 bool16 getAllIdForLevel(int level, int32& numIds, vector<int32>& idList);
};

#endif
