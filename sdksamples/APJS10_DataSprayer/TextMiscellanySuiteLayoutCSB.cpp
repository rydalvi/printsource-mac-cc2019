#include "VCPlugInHeaders.h"	

#include "ILayoutTarget.h"

#include "ITextMiscellanySuite.h"
#include "CmdUtils.h"
#include "ISpecifier.h"
#include "PageItemUtils.h"

class TextMiscellanySuiteLayoutCSB : public CPMUnknown<ITextMiscellanySuite>
{
public:
	TextMiscellanySuiteLayoutCSB(IPMUnknown* iBoss);

	virtual	~TextMiscellanySuiteLayoutCSB(void);

	virtual bool16 GetCurrentSpecifier(ISpecifier* & );

	virtual bool16 GetUidList(UIDList &);

};
CREATE_PMINTERFACE(TextMiscellanySuiteLayoutCSB, kTextMiscellanySuiteLayoutCSBImpl1)

TextMiscellanySuiteLayoutCSB::TextMiscellanySuiteLayoutCSB(IPMUnknown* iBoss) :
	CPMUnknown<ITextMiscellanySuite>(iBoss)
{
}

/* Destructor
*/
TextMiscellanySuiteLayoutCSB::~TextMiscellanySuiteLayoutCSB(void)
{
}
bool16 TextMiscellanySuiteLayoutCSB::GetCurrentSpecifier(ISpecifier * & Xspec)
{
	InterfacePtr<ILayoutTarget>		iLayoutTarget(this, UseDefaultIID());
	const UIDList					selectedItems(iLayoutTarget->GetUIDList(kStripStandoffs));
	ISpecifier * spec = PageItemUtils::QuerySpecifier(selectedItems);
	if(spec )
	{
		Xspec = spec;
		return(1);
	}
	return(0);
}
bool16 TextMiscellanySuiteLayoutCSB::GetUidList(UIDList & TempUidList)
{
	InterfacePtr<ILayoutTarget>	iLayoutTarget(this, UseDefaultIID());
	const UIDList	selectedItems(iLayoutTarget->GetUIDList(kStripStandoffs));
	TempUidList = selectedItems;	
	return 1;
}