#include "VCPluginHeaders.h"
#include "CommonFunctions.h"
#include "PMString.h"

#include "IXMLAttributeCommands.h"
#include "Utils.h"
#include "IIDXMLElement.h"
//makeTheTagImpotent function includes
#include "IXMLUtils.h"
#include "ITextStoryThread.h"
#include "PublicationNode.h"
//#include "AdvanceTableScreenValue.h"
#include "IAppFramework.h"
#include "ITextModelCmds.h"

#include "CAlert.h"

#define pNode pNode1

extern PublicationNode pNode;
extern int32 global_project_level;
extern double CurrentSectionID;
extern double CurrentPublicationID;
extern double CurrentSubSectionID;

inline PMString  numToPMString(int32 num){
PMString numStr;
numStr.AppendNumber(num);
return numStr;
}


#define CA(X) CAlert::InformationAlert \
	( \
	PMString("CommonFunctions.cpp") + PMString("\n") + \
    PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + PMString("\n") + X)

#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

PMString prepareTagName(PMString name)
{
	PMString tagName("");
	for(int i=0;i<name.NumUTF16TextChars(); i++)
	{
		if(name.GetChar(i) == '[' || name.GetChar(i) == ']' || name.GetChar(i) == '\n')
			continue;

		if(name.GetChar(i) ==' ')
		{
			tagName.Append("_");
		}
		else tagName.Append(name.GetChar(i));
	}
	PMString FinalTagName("");
	FinalTagName = keepOnlyAlphaNumeric(tagName); // removes the Special characters from the Name.

	return FinalTagName;
}
//Method Purpose: remove the Special character from the Name. 
PMString keepOnlyAlphaNumeric(PMString name)
{
	PMString tagName("");

	for(int i=0;i<name.NumUTF16TextChars(); i++)
	{
		bool isAlphaNumeric = false ;

		PlatformChar ch = name.GetChar(i);

		if(ch.IsAlpha() || ch.IsNumber())
			isAlphaNumeric = true ;

		if(ch.IsSpace())
		{
			isAlphaNumeric = true ;
			ch.Set('_') ;
		}

		if(ch == '_')
			isAlphaNumeric = true ;
	
		if(isAlphaNumeric) 
			tagName.Append(ch);
	}

	return tagName ;
}


	void setAllXMLTagAttributeValues(XMLReference & xmlTagReference ,const XMLTagAttributeValue & tagAttrVal)
	{

		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("ID"),WideString(tagAttrVal.ID));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("typeId"),WideString(tagAttrVal.typeId));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("header"),WideString(tagAttrVal.header));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("isEventField"),WideString(tagAttrVal.isEventField));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("deleteIfEmpty"),WideString(tagAttrVal.deleteIfEmpty));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("dataType"),WideString(tagAttrVal.dataType));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("isAutoResize"),WideString(tagAttrVal.isAutoResize));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("LanguageID"),WideString(tagAttrVal.LanguageID));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("index"),WideString(tagAttrVal.index));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("pbObjectId"),WideString(tagAttrVal.pbObjectId));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("parentID"),WideString(tagAttrVal.parentID));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("childId"),WideString(tagAttrVal.childId));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("sectionID"),WideString(tagAttrVal.sectionID));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("parentTypeID"),WideString(tagAttrVal.parentTypeID));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("isSprayItemPerFrame"),WideString(tagAttrVal.isSprayItemPerFrame));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("catLevel"),WideString(tagAttrVal.catLevel));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("imgFlag"),WideString(tagAttrVal.imgFlag));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("imageIndex"),WideString(tagAttrVal.imageIndex));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("flowDir"),WideString(tagAttrVal.flowDir));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("childTag"),WideString(tagAttrVal.childTag));		
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("tableFlag"),WideString(tagAttrVal.tableFlag));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("tableType"),WideString(tagAttrVal.tableType));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("tableId"),WideString(tagAttrVal.tableId));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("rowno"),WideString(tagAttrVal.rowno));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("colno"),WideString(tagAttrVal.colno));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("field1"),WideString(tagAttrVal.field1));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("field2"),WideString(tagAttrVal.field2));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("field3"),WideString(tagAttrVal.field3));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("field4"),WideString(tagAttrVal.field4));
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("field5"),WideString(tagAttrVal.field5));
        try{
            Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlTagReference,WideString("groupKey"),WideString(tagAttrVal.groupKey));
        }catch(...){
            
        }
        
	}

	void createAllXMLTagAttributes(XMLReference & xmlRef,const XMLTagAttributeValue & tagAttrVal)
	{			
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("ID"),WideString(tagAttrVal.ID)); //Cs4 AAAALLLLLA
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("typeId"),WideString(tagAttrVal.typeId));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("header"),WideString(tagAttrVal.header));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("isEventField"),WideString(tagAttrVal.isEventField));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("deleteIfEmpty"),WideString(tagAttrVal.deleteIfEmpty));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("dataType"),WideString(tagAttrVal.dataType));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("isAutoResize"),WideString(tagAttrVal.isAutoResize));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("LanguageID"),WideString(tagAttrVal.LanguageID));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("index"),WideString(tagAttrVal.index));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("pbObjectId"),WideString(tagAttrVal.pbObjectId));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("parentID"),WideString(tagAttrVal.parentID));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("childId"),WideString(tagAttrVal.childId));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("sectionID"),WideString(tagAttrVal.sectionID));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("parentTypeID"),WideString(tagAttrVal.parentTypeID));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("isSprayItemPerFrame"),WideString(tagAttrVal.isSprayItemPerFrame));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("catLevel"),WideString(tagAttrVal.catLevel));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("imgFlag"),WideString(tagAttrVal.imgFlag));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("imageIndex"),WideString(tagAttrVal.imageIndex));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("flowDir"),WideString(tagAttrVal.flowDir));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("childTag"),WideString(tagAttrVal.childTag));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("tableFlag"),WideString(tagAttrVal.tableFlag));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("tableType"),WideString(tagAttrVal.tableType));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("tableId"),WideString(tagAttrVal.tableId));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("rowno"),WideString(tagAttrVal.rowno));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("colno"),WideString(tagAttrVal.colno));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("field1"),WideString(tagAttrVal.field1));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("field2"),WideString(tagAttrVal.field2));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("field3"),WideString(tagAttrVal.field3));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("field4"),WideString(tagAttrVal.field4));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("field5"),WideString(tagAttrVal.field5));
        try{
            Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("groupKey"),WideString(tagAttrVal.groupKey));
        }catch(...){
            
        }
	}

	void getAllXMLTagAttributeValues(XMLReference & xmlTagReference,XMLTagAttributeValue & tagAttrVal)
	{

		InterfacePtr<IIDXMLElement>tagXMLElementPtr(xmlTagReference.Instantiate());

		tagAttrVal.ID = tagXMLElementPtr->GetAttributeValue(WideString("ID"));
		tagAttrVal.typeId = tagXMLElementPtr->GetAttributeValue(WideString("typeId"));
		tagAttrVal.header = tagXMLElementPtr->GetAttributeValue(WideString("header"));
		tagAttrVal.isEventField = tagXMLElementPtr->GetAttributeValue(WideString("isEventField"));
		tagAttrVal.deleteIfEmpty = tagXMLElementPtr->GetAttributeValue(WideString("deleteIfEmpty"));
		tagAttrVal.dataType = tagXMLElementPtr->GetAttributeValue(WideString("dataType"));
		tagAttrVal.isAutoResize = tagXMLElementPtr->GetAttributeValue(WideString("isAutoResize"));
		tagAttrVal.LanguageID = tagXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
		tagAttrVal.index = tagXMLElementPtr->GetAttributeValue(WideString("index"));
		tagAttrVal.pbObjectId = tagXMLElementPtr->GetAttributeValue(WideString("pbObjectId"));
		tagAttrVal.parentID = tagXMLElementPtr->GetAttributeValue(WideString("parentID"));
		tagAttrVal.childId = tagXMLElementPtr->GetAttributeValue(WideString("childId"));
		tagAttrVal. sectionID = tagXMLElementPtr->GetAttributeValue(WideString("sectionID"));
		tagAttrVal. parentTypeID = tagXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
		tagAttrVal.isSprayItemPerFrame = tagXMLElementPtr->GetAttributeValue(WideString("isSprayItemPerFrame"));
		tagAttrVal.catLevel = tagXMLElementPtr->GetAttributeValue(WideString("catLevel"));
		tagAttrVal. imgFlag = tagXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
		tagAttrVal.imageIndex = tagXMLElementPtr->GetAttributeValue(WideString("imageIndex"));
		tagAttrVal.flowDir = tagXMLElementPtr->GetAttributeValue(WideString("flowDir"));
		tagAttrVal.childTag = tagXMLElementPtr->GetAttributeValue(WideString("childTag"));
		tagAttrVal. tableFlag = tagXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
		tagAttrVal.tableType = tagXMLElementPtr->GetAttributeValue(WideString("tableType"));
		tagAttrVal.tableId = tagXMLElementPtr->GetAttributeValue(WideString("tableId"));
		tagAttrVal. rowno = tagXMLElementPtr->GetAttributeValue(WideString("rowno"));
		tagAttrVal. colno = tagXMLElementPtr->GetAttributeValue(WideString("colno"));
		tagAttrVal.field1 = tagXMLElementPtr->GetAttributeValue(WideString("field1"));
		tagAttrVal.field2 = tagXMLElementPtr->GetAttributeValue(WideString("field2"));
		tagAttrVal.field3 = tagXMLElementPtr->GetAttributeValue(WideString("field3"));
		tagAttrVal.field4 = tagXMLElementPtr->GetAttributeValue(WideString("field4"));
		tagAttrVal.field5 = tagXMLElementPtr->GetAttributeValue(WideString("field5"));
        try{
            tagAttrVal.groupKey = tagXMLElementPtr->GetAttributeValue(WideString("groupKey"));
        }catch(...){
            tagAttrVal.groupKey ="";
        }
	}

	void makeTheTagImpotent(IIDXMLElement * xmlPtr)
	{
		//This is a special case.The tag is of product but the Item is
		//selected.Or the tag is of Item and the product is selected.
		//So make this tag unproductive for refresh.(Impotent)		
		do
		{
			int32 tagStart = -1;
			int32 tagEnd = -1;
			Utils<IXMLUtils>()->GetElementIndices(xmlPtr,&tagStart,&tagEnd);
			XMLReference xmlPtrXMLRef = xmlPtr->GetXMLReference();
			tagStart = tagStart +1;
			tagEnd = tagEnd - 1;

			InterfacePtr<ITextStoryThread> textStoryThread(xmlPtr->QueryContentTextStoryThread());
			if(textStoryThread == NULL)
			{
				//CA("textStoryThread == NULL");
				break;
			}
			InterfacePtr<ITextModel> textModel(textStoryThread->QueryTextModel());
			if(textModel == NULL)
			{
				//CA("textModel == NULL");
				break;
			}

			PMString str("");
			TextIndex position = tagStart;
			int32 length = tagEnd-tagStart + 1;
            str.ParseForEmbeddedCharacters();
			boost::shared_ptr<WideString> text(new WideString(str));

			if (position < 0 || position >= textModel->TotalLength()) {
				//CA("position invalid");
				break;
			}
			if (length < 1 || length >= textModel->TotalLength()) {
				//CA("length invalid");
				break;
			}
    		InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
    		ASSERT(textModelCmds);
    		if (!textModelCmds) {
				//CA("!textModelCmds");
				break;
			}


			InterfacePtr<ICommand> replaceCmd(textModelCmds->ReplaceCmd(position, length, text));
			ASSERT(replaceCmd);
			if (!replaceCmd) {
				//CA("!replaceCmd");
				break;
			}
			//CA("before Replace ");
			ErrorCode status = kFailure;
 			status = CmdUtils::ProcessCommand(replaceCmd);
			if(status != kSuccess)
			{
				//CA("status != kSuccess");
				break;
			}
			CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);

			//change the XMLTagAttribute values also
			PMString attributeValue;
			attributeValue.AppendNumber(PMReal(pNode.getPubId()));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlPtrXMLRef, WideString("parentID"),WideString(attributeValue)); //Cs4

			attributeValue.Clear();
			attributeValue.AppendNumber(PMReal(pNode.getTypeId()));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlPtrXMLRef, WideString("parentTypeID"),WideString(attributeValue)); //Cs4

			double sectionID = -1;
			if(global_project_level == 3)
				sectionID = CurrentSubSectionID;
			if(global_project_level == 2)
				sectionID = CurrentSectionID;

			attributeValue.Clear();
			attributeValue.AppendNumber(PMReal(sectionID));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlPtrXMLRef, WideString("sectionID"),WideString(attributeValue)); //Cs4

			//The tableFlag = -15 to indicate that this is impotent tag.
			Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlPtrXMLRef, WideString("tableFlag"),WideString("-15")); //Cs4
		}while(kFalse);
	}

	bool8 getAttributeIDFromNotes(const int32 & isProduct,const double & sectionid,const double & pubObjectId,const  double & selectedItemID,PMString & attributeIDfromNotes)
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
			return kFalse;

		double eventPriceTableTypeID = -1;/*??ptrIAppFramework->getPubItemPriceTableType();*/
		
		//CA_NUM("EventPriceTableTypeID : ",eventPriceTableTypeID);
		attributeIDfromNotes = "-1";
		bool8 isNotesValid = kFalse;
		
		do
		{
			if(isProduct == 1)
			{
				
				VectorScreenTableInfoPtr tableInfo = NULL;
			
				if(pNode.getIsONEsource())
				{
					// For ONEsource
					//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(pubObjectId);
				}else
				{
					//For publication mode is selected.
					double tempLangId =ptrIAppFramework->getLocaleId();
					tableInfo= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, pubObjectId, tempLangId, kFalse);
				}
				if(!tableInfo)
				{
					ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CommonFunction::getAttributeIDFromNotes: !tableInfo");
					break;
				}
				if(tableInfo->size()==0)
				{ 
					break;
				}
				bool8 isEventPriceTableIDPresent =kFalse;
				CItemTableValue oTableValue;
				VectorScreenTableInfoValue::iterator it;
				for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
				{	
					if(it->getTableTypeID() == eventPriceTableTypeID)
					{
						oTableValue = *it;
						isEventPriceTableIDPresent = kTrue;
						break;
					}
					
				}
				if(isEventPriceTableIDPresent == kFalse)
					break;

				vector <double> vec_ItemIDs = oTableValue.getItemIds();
				vector <PMString> vec_notes =  oTableValue.getNotesList();
				vector <double> :: iterator itemIterator;
				vector <PMString> ::iterator notesIterator =vec_notes.begin(); 
				for(itemIterator = vec_ItemIDs.begin();itemIterator != vec_ItemIDs.end();itemIterator++,notesIterator++)
				{
					if(selectedItemID == *itemIterator)
					{
						attributeIDfromNotes = *notesIterator;
						isNotesValid = kTrue;
						break;
					}
				}
			}
			if(isProduct == 0)
			{
				double tempLangId =ptrIAppFramework->getLocaleId();
				VectorScreenTableInfoPtr tableInfo=
						ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pubObjectId, sectionid, tempLangId );
				if(!tableInfo)
				{
					ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CommonFunction::getAttributeIDFromNotes:  tableinfo is NULL");
					break;
				}
				if(!tableInfo)
				{
					break;
				}
				if(tableInfo->size()==0)
				{	//CA("tableInfo->size()==0");
					break;
				}
				bool8 isEventPriceTableIDPresent =kFalse;
				CItemTableValue oTableValue;
				VectorScreenTableInfoValue::iterator it;
				for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
				{	
					if(it->getTableTypeID() == eventPriceTableTypeID)
					{
						oTableValue = *it;
						isEventPriceTableIDPresent = kTrue;
						break;
					}
					
				}
				if(tableInfo)
				{
					tableInfo->clear();
					delete tableInfo;
				}

				if(isEventPriceTableIDPresent == kFalse)
				{
					//CA("isEventPriceTableIDPresent == kFalse");
					break;
				}
				vector <double> vec_ItemIDs = oTableValue.getItemIds();
				vector <PMString> vec_notes =  oTableValue.getNotesList();
				vector <double> :: iterator itemIterator;
				vector <PMString> ::iterator notesIterator =vec_notes.begin(); 
				for(itemIterator = vec_ItemIDs.begin();itemIterator != vec_ItemIDs.end();itemIterator++,notesIterator++)
				{
					if(selectedItemID == *itemIterator)
					{
						attributeIDfromNotes = *notesIterator;
						isNotesValid = kTrue;
						break;
					}
				}

			}
		}while(kFalse);

		return isNotesValid;
	}

	bool8 getAttributeIDFromNotesNew(PublicationNode & pNode ,const double & sectionid,const  double & selectedItemID,PMString & attributeIDfromNotes)
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
			return kFalse;

		//??int32 eventPriceTableTypeID = ptrIAppFramework->getPubItemPriceTableType();
		double eventPriceTableTypeID = -1; //tagInfo.elementId;
		
        //CA_NUM("EventPriceTableTypeID : ",eventPriceTableTypeID);
		attributeIDfromNotes = "-1";
		bool8 isNotesValid = kFalse;
		int32 isProduct = pNode.getIsProduct();
		
		do
		{
			if(isProduct ==1)
			{

				VectorScreenTableInfoPtr tableInfo = NULL;
				if(pNode.getIsONEsource())
				{
					// For ONEsource
					//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(pNode.getPubId());
				}else
				{
					//For publication mode is selected
					double tempLangId = ptrIAppFramework->getLocaleId();
					tableInfo=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, pNode.getPubId(), tempLangId, kFalse);
				}
				if(!tableInfo)
				{
					ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CommonFunction::getAttributeIDFromNotesNew: !tableInfo");
					break;
				}
				if(tableInfo->size()==0)
				{ 
					break;
				}
				bool8 isEventPriceTableIDPresent =kFalse;
				CItemTableValue oTableValue;
				VectorScreenTableInfoValue::iterator it;
				for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
				{	
					if(it->getTableTypeID() == eventPriceTableTypeID)
					{
						oTableValue = *it;
						isEventPriceTableIDPresent = kTrue;
						break;
					}					
				}
				if(isEventPriceTableIDPresent == kFalse)
					break;
				

				vector <double> vec_ItemIDs = oTableValue.getItemIds();
				vector <PMString> vec_notes =  oTableValue.getNotesList();
				vector <double> :: iterator itemIterator;
				vector <PMString> ::iterator notesIterator =vec_notes.begin(); 
				for(itemIterator = vec_ItemIDs.begin();itemIterator != vec_ItemIDs.end();itemIterator++,notesIterator++)
				{
					if(selectedItemID == *itemIterator)
					{
						attributeIDfromNotes = *notesIterator;
						isNotesValid = kTrue;
						break;
					}
				}
			}
			if(isProduct ==0)
			{
				double tempLangId = ptrIAppFramework->getLocaleId();
				VectorScreenTableInfoPtr tableInfo=
						ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPubId(), sectionid, tempLangId);
				if(!tableInfo)
				{
					ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CommonFunction::getAttributeIDFromNotesNew:  tableinfo is NULL");
					break;
				}
				if(!tableInfo)
				{
					break;
				}
				if(tableInfo->size()==0)
				{	//CA("tableInfo->size()==0");
					break;
				}

				bool8 isEventPriceTableIDPresent =kFalse;
				CItemTableValue oTableValue;
				VectorScreenTableInfoValue::iterator it;
				for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
				{	
					if(it->getTableTypeID() == eventPriceTableTypeID)
					{
						oTableValue = *it;
						isEventPriceTableIDPresent = kTrue;
						break;
					}
					
				}
				if(tableInfo)
				{
					tableInfo->clear();
					delete tableInfo;
				}

				if(isEventPriceTableIDPresent == kFalse)
				{//CA("isEventPriceTableIDPresent == kFalse");
					break;
				}
		
				vector <double> vec_ItemIDs = oTableValue.getItemIds();
				vector <PMString> vec_notes =  oTableValue.getNotesList();
				vector <double> :: iterator itemIterator;
				vector <PMString> ::iterator notesIterator =vec_notes.begin(); 
				for(itemIterator = vec_ItemIDs.begin();itemIterator != vec_ItemIDs.end();itemIterator++,notesIterator++)
				{
					if(selectedItemID == *itemIterator)
					{
						attributeIDfromNotes = *notesIterator;
						isNotesValid = kTrue;
						break;
					}
				}

			}
		}while(kFalse);

		return isNotesValid;
	}

	void handleSprayingOfEventPriceRelatedAdditions(PublicationNode & pNode,TagStruct & tagInfo,PMString & itemAttributeValue)
	{
//		///added on 22Sept..EventPrice addition.
//		//CA("handleSprayingOfEventPriceRelatedAdditions");
//	    //int32 eventTabletypeId = tagInfo.elementId;	//Table type id for Event table  ////commented on 21_12	
//		int32 eventAttributeType = tagInfo.elementId;
//		int32 eventTabletypeId = -1;
//		int32 SalePriceTabletypeId = -1;
//		int32 RegularPriceTabletypeId = -1;
//
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == NULL)
//			return ;
//
//		eventTabletypeId = tagInfo.parentTypeID;
//
//		//CA_NUM("tagInfo.typeId : ",tagInfo.typeId);
//		//int32 eventTabletypeId = 1172;
//		
//		/*if((tagInfo.elementId == -701 || tagInfo.elementId == -702 || tagInfo.elementId == -703 || tagInfo.elementId == -704) && globalParentTypeId != -1)
//		{
//			eventTabletypeId = globalParentTypeId;
//			globalParentTypeId = -1;
//		}*/
//			
//		//CA_NUM("tagInfo.parentTypeID() :",tagInfo.parentTypeID);
//	    //int32 eventAttributeType = tagInfo.typeId; //hardcoded price id to distinguish whether it is EventPrice,suffics, $ off and % off. //commented on 21_12	
//		//		int32 eventAttributeType = tagInfo.elementId;
//		//CA_NUM("elementId :",elementId);
//		if(pNode.getIsProduct() == 0)
//		{
//			if(tagInfo.typeId == tagInfo.parentId && tagInfo.typeId != -1)
//				tagInfo.typeId = pNode.getPubId();
//		}
//		//tagInfo.typeId = pNode.getPubId();
//		//int32 elementId = tagInfo.elementId; //old functionality commented by Tushar to remove hardcode functinality
//		//CA_NUM("tagInfo.elementId() :",tagInfo.elementId);
////		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
////		if(ptrIAppFramework == NULL)
////			return ;
//		int32 sectionid = -1;
//		//int32 pbObjectID = -1;
//		if(pNode.getIsProduct() == 1)
//		{
//			if(global_project_level == 3)
//				sectionid = CurrentSubSectionID;
//			if(global_project_level == 2)
//				sectionid = CurrentSectionID;
//			
//		}
//
//		PMString attributeIDfromNotes("-1");
//		PMString attributeIDfromNotesSales("-1");
//		PMString attributeIDfromNotesRegular("-1");
//
//		//CA_NUM("sectionid :",sectionid);
//		//CA_NUM("tagInfo.typeId :",tagInfo.typeId);
//
//		int32 temp = tagInfo.elementId;
//
//		//bool8 isValidNotes =  getAttributeIDFromNotesNew(pNode ,sectionid,tagInfo.typeId,attributeIDfromNotes);//old functionality commented by Tushar on 19/12/06
//    	//bool8 isValidNotes =  getAttributeIDFromNotesNewByElementId(pNode ,sectionid,tagInfo.typeId,attributeIDfromNotes,tagInfo.elementId); //in this method 'tagInfo.elementId' is used as 'eventPriceTableTypeID'
//		if(eventAttributeType == -703 || eventAttributeType == -704)
//		{
//			SalePriceTabletypeId = ptrIAppFramework->ConfigCache_getPubItemSalePriceTableType();
//			RegularPriceTabletypeId = ptrIAppFramework->ConfigCache_getPubItemRegularPriceTableType();
//			//CA_NUM("SalePriceTabletypeId :",SalePriceTabletypeId);
//			//CA_NUM("RegularPriceTabletypeId :",RegularPriceTabletypeId);
//
//			bool8 isValidNotes =  getAttributeIDFromNotes(pNode ,sectionid,tagInfo.typeId,attributeIDfromNotesSales,attributeIDfromNotesRegular,SalePriceTabletypeId,RegularPriceTabletypeId);
//			//bool8 isValidNotes1 =  getAttributeIDFromNotesNewByElementId(pNode ,sectionid,tagInfo.typeId,attributeIDfromNotesRegular,RegularPriceTabletypeId);
//		}
//		else 
//			bool8 isValidNotes =  getAttributeIDFromNotesNewByElementId(pNode ,sectionid,tagInfo.typeId,attributeIDfromNotes,eventTabletypeId);
//       //CA_NUM("after tagInfo.typeId() :",tagInfo.typeId);
//       //CA_NUM("after sectionid :",sectionid);
//	   //CA_NUM("after tagInfo.parentTypeID :",tagInfo.parentTypeID);
//
//       //CA("after attributeIDfromNotes = " + attributeIDfromNotes);
//
//		//added on 22Sept..EventPrice addition.
//		PMString strPbObjectID;
//		strPbObjectID.AppendNumber(pNode.getPBObjectID());
//        //CA("strPbObjectID = " + strPbObjectID);
//		tagInfo.tagPtr->SetAttributeValue(WideString("pbObjectId"),WideString(strPbObjectID));  //Cs4
//		tagInfo.tagPtr->SetAttributeValue(WideString("rowno"),WideString("-1"/*strPbObjectID*/));  //Cs4
//		//added on 22Sept..EventPrice addition.
//		//	CA_NUM("elementId :",elementId);
//
//		if(eventAttributeType == -701)
//		{
//
//			/*??if(ptrIAppFramework->ATTRIBUTECache_isItemAttributeEventSpecific(attributeIDfromNotes.GetAsNumber()) == kTrue){
//				VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(tagInfo.typeId,attributeIDfromNotes.GetAsNumber(),tagInfo.sectionID);
//				if(vecPtr != NULL)
//					if(vecPtr->size()> 0)
//						itemAttributeValue = vecPtr->at(0).getObjectValue();
//			}
//			else if(attributeIDfromNotes.GetAsNumber() == -401 || attributeIDfromNotes.GetAsNumber() == -402 || attributeIDfromNotes.GetAsNumber() == -403)
//			{
//				itemAttributeValue = ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(tagInfo.typeId,attributeIDfromNotes.GetAsNumber(), tagInfo.languageID);
//			}
//			else*/
//			{
//				itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,attributeIDfromNotes.GetAsNumber(), tagInfo.languageID, CurrentSectionID, kFalse );
//			}
//		}
//		else if(eventAttributeType == -702)
//		{
//			itemAttributeValue = ptrIAppFramework->getPriceSuffix(attributeIDfromNotes.GetAsNumber());
//			//CA(" inside itemAttributeValue" + itemAttributeValue);
//		}
//		else if(eventAttributeType == -703 || eventAttributeType == -704)
//		{
//			if(attributeIDfromNotesSales == "" && attributeIDfromNotesRegular == "")
//			{
//				itemAttributeValue = "";
//				return;
//			}
//			else if(attributeIDfromNotesSales == "" || attributeIDfromNotesSales == "")
//			{
//				itemAttributeValue = "";
//				return;
//			}
//			double eventPrice = 0;
//			double regularPrice = 0;
////			int32 eventPriceID = attributeIDfromNotes.GetAsNumber();
//			do
//			{
//				PMString eventPriceValue("");
//				if(attributeIDfromNotesSales.GetAsNumber() == -401 || attributeIDfromNotesSales.GetAsNumber() == -402 || attributeIDfromNotesSales.GetAsNumber() == -403){
//					eventPriceValue = ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(tagInfo.typeId,attributeIDfromNotesSales.GetAsNumber(), tagInfo.languageID);
//				}
//				else{
//					eventPriceValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,attributeIDfromNotesSales.GetAsNumber(), tagInfo.languageID, CurrentSectionID, kFalse );
//				}
//				//PMString eventPriceValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,eventPriceID, tagInfo.languageID, kTrue );
//				if(eventPriceValue == "")
//				{
//					itemAttributeValue = "";
//					return;
//				}
//				
//				PMString ::ConversionError * pErr = NULL;
//				eventPrice = eventPriceValue.GetAsDouble(pErr,NULL);
//				if(( pErr != NULL) &&(*pErr == PMString ::kNoNumber || *pErr == PMString ::kNotJustNumber))
//				{//CA("11111111eventPriceValue == """);
//					itemAttributeValue = "";
//					return;
//				}
//			}while(kFalse);
//			do
//			{	
//			//	int32 regularPriceID = ptrIAppFramework->getRegularPriceAttributeId();
////					int32 regularPriceID = ptrIAppFramework->getEventPriceAttributeId(eventTabletypeId);
////					if(regularPriceID == -1 )
////					{
////						itemAttributeValue = "";
////						return;
////					}
//				PMString regularPriceValue("");
//				if(attributeIDfromNotesRegular.GetAsNumber() == -401 || attributeIDfromNotesRegular.GetAsNumber() == -402 || attributeIDfromNotesRegular.GetAsNumber() == -403){
//					regularPriceValue = ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(tagInfo.typeId,attributeIDfromNotesRegular.GetAsNumber(), tagInfo.languageID);
//				}
//				else{
//					regularPriceValue= ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,attributeIDfromNotesRegular.GetAsNumber(), tagInfo.languageID, CurrentSectionID, kFalse );
//				}
////					PMString regularPriceValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,regularPriceID, tagInfo.languageID, kTrue );
//				if(regularPriceValue == "")
//				{	
//					itemAttributeValue = "";
//					return;
//				}
//
//				PMString ::ConversionError * pErr = NULL;
//				regularPrice = regularPriceValue.GetAsDouble(pErr,NULL);
//				if(( pErr != NULL) && (*pErr == PMString ::kNoNumber || *pErr == PMString ::kNotJustNumber))
//				{	
//					itemAttributeValue = "";
//					return;
//				}
//			}while(kFalse);
//
////PMString num;
////num.Append("regularPrice -> ");
////num.AppendNumber(regularPrice);
////num.Append(" : ");
////num.Append("eventPrice -> ");
////num.AppendNumber(eventPrice);
////CA(num);
//
//			//int32 realDifference = (int32)(regularPrice - eventPrice  );
//			
//			//PMReal realDifference =Round(regularPrice - eventPrice);
//			//num.Append("\n");
//			//num.Append("realDifference : ");
//			//num.AppendNumber(realDifference);
//			//num.Append("\n");
//			////int32 intDifference = ToInt32(realDifference);
//			////num.Append("intDifference : ");
//			////num.AppendNumber(intDifference);
//			//CA(num);
//			if(eventAttributeType == -703)
//			{
//				//PMReal temp;
//				//PMReal nearestDollar = ::Ceiling(realDifference);
//				//itemAttributeValue.AppendNumber(nearestDollar);
//				//itemAttributeValue.AppendNumber(intDifference);
//				
//				int32 realDifference = ptrIAppFramework->getDollerDifference(regularPrice ,eventPrice);
//				itemAttributeValue.AppendNumber(realDifference);
//			}
//			else if(eventAttributeType == -704)
//			{
//				itemAttributeValue = "";
//				if(regularPrice != 0)
//				{
//					//int32 actualPercentage = (int32)((realDifference/regularPrice) * 100);
//					//int32 percentageDiff = ToInt32(actualPercentage);
//					//itemAttributeValue.AppendNumber(percentageDiff);
//					int32 percentageDiff = ptrIAppFramework->getPercentageDifference(regularPrice ,eventPrice);
//					itemAttributeValue.AppendNumber(percentageDiff);						
//
//				}
//			}
//			//CA("itemAttributeValue: "+ itemAttributeValue);
//
//			/*float a = 44.99f;
//			float b = 69.99f;
//
//			int32 c = (int32)(b-a);
//			CA_NUM("c : ",c);*/
//			//PMReal a = 10.99;
//			//PMReal b = 20.99;
//			//PMReal c = Round(b - a);
//			
//			//CA_NUM("c : ",c);
//
//		
//		}
//		
//		//ended on 22Sept..EventPrice addition.
//
//	//ended on 22Sept..EventPrice addition.
	}

	void convertTagStructToXMLTagAttributeValue(const TagStruct & tagInfo,XMLTagAttributeValue & tagVal)
	{
		//"ID"
		tagVal.ID.AppendNumber(PMReal(tagInfo.elementId));
		//"typeId"
		tagVal.typeId.AppendNumber(PMReal(tagInfo.typeId));
		tagVal.header.AppendNumber(tagInfo.header);
		tagVal.isEventField.AppendNumber(tagInfo.isEventField);
		tagVal.deleteIfEmpty.AppendNumber(tagInfo.deleteIfEmpty);
		tagVal.dataType.AppendNumber(tagInfo.dataType);
		//"isAutoResize"
		tagVal.isAutoResize.AppendNumber(tagInfo.isAutoResize);
		//"LanguageID"
		tagVal.LanguageID.AppendNumber(PMReal(tagInfo.languageID));
		//"index"
		tagVal.index.AppendNumber(tagInfo.whichTab);
		tagVal.pbObjectId.AppendNumber(PMReal(tagInfo.pbObjectId));
		//"parentID"
		tagVal.parentID.AppendNumber(PMReal(tagInfo.parentId));
		tagVal.childId.AppendNumber(PMReal(tagInfo.childId));
		//"sectionID"
		tagVal.sectionID.AppendNumber(PMReal(tagInfo.sectionID));
		//"parentTypeID"
		tagVal.parentTypeID.AppendNumber(PMReal(tagInfo.parentTypeID));
		tagVal.isSprayItemPerFrame.AppendNumber(tagInfo.isSprayItemPerFrame);
		tagVal.catLevel.AppendNumber(tagInfo.catLevel);
		//"imgFlag"
		tagVal.imgFlag.AppendNumber(tagInfo.imgFlag);
		tagVal.imageIndex.AppendNumber(tagInfo.imageIndex);
		tagVal.flowDir.AppendNumber(tagInfo.flowDir);
		tagVal.childTag.AppendNumber(tagInfo.childTag);
		//"tableFlag"
		tagVal.tableFlag.AppendNumber(tagInfo.isTablePresent);
		tagVal.tableType.AppendNumber(tagInfo.tableType);
		tagVal.tableId.AppendNumber(PMReal(tagInfo.tableId));
		//"rowno"
		tagVal.rowno.AppendNumber(PMReal(tagInfo.rowno));
		//"colno"
		tagVal.colno.AppendNumber(PMReal(tagInfo.colno));
		tagVal.field1.AppendNumber(PMReal(tagInfo.field1));
		tagVal.field2.AppendNumber(PMReal(tagInfo.field2));
		tagVal.field3.AppendNumber(PMReal(tagInfo.field3));
		tagVal.field4.AppendNumber(PMReal(tagInfo.field4));
		tagVal.field5.AppendNumber(PMReal(tagInfo.field5));
        tagVal.groupKey.Append(tagInfo.groupKey);
        
	}

	bool8 getAttributeIDFromNotesNewByElementId(PublicationNode & pNode ,const double & sectionid,const  double & selectedItemID,PMString & attributeIDfromNotes,double eventPriceTableTypeID)
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
			return kFalse;

		attributeIDfromNotes = "-1";
		bool8 isNotesValid = kFalse;
		int32 isProduct = pNode.getIsProduct();
		
		do
		{
			if(isProduct == 1)
			{
				VectorScreenTableInfoPtr tableInfo = NULL;
				if(pNode.getIsONEsource())
				{
					//CA("Going for ONEsource");
					// For ONEsource
					//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(pNode.getPubId());
				}else
				{
					//CA("Going For Publication");
					//For publication mode is selected
					double tempLangId = ptrIAppFramework->getLocaleId();
					tableInfo=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, pNode.getPubId(), tempLangId, kFalse);
				}
				if(!tableInfo)
				{
					ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CommonFunction::getAttributeIDFromNotesNewByElementId: !tableInfo");								
					break;
				}
				if(tableInfo->size()==0)
				{ 
					break;
				}

				bool8 isEventPriceTableIDPresent =kFalse;
				CItemTableValue oTableValue;
				VectorScreenTableInfoValue::iterator it;
				for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
				{	
					if(it->getTableTypeID() == eventPriceTableTypeID)
					{
						oTableValue = *it;
						isEventPriceTableIDPresent = kTrue;
						break;
					}
					
				}
				if(isEventPriceTableIDPresent == kFalse)
					break;

				vector <double> vec_ItemIDs = oTableValue.getItemIds();
				vector <PMString> vec_notes =  oTableValue.getNotesList();
				if(vec_notes.size() <= 0)
				{
					//CA("vec_notes.size() <= 0");
					break;
				}
				vector <double> :: iterator itemIterator;
				vector <PMString> ::iterator notesIterator =vec_notes.begin(); 
				for(itemIterator = vec_ItemIDs.begin();itemIterator != vec_ItemIDs.end();itemIterator++,notesIterator++)
				{
					if(selectedItemID == *itemIterator)
					{
						attributeIDfromNotes = *notesIterator;
						isNotesValid = kTrue;
						break;
					}
				}
			}
			if(isProduct == 0)
			{
				double langId = ptrIAppFramework->getLocaleId();
				VectorScreenTableInfoPtr tableInfo=
						ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPubId(), sectionid, langId);
				if(!tableInfo)
				{
					ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CommonFunction::getAttributeIDFromNotesNewByElementId: GETProjectProduct_getItemTablesByPubObjectId's !tableInfo");
					break;
				}
			
				if(tableInfo->size()==0)
				{	
					break;
				}
				bool8 isEventPriceTableIDPresent =kFalse;
				CItemTableValue oTableValue;
				VectorScreenTableInfoValue::iterator it;
				for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
				{	
					if(it->getTableTypeID() == eventPriceTableTypeID)
					{
						oTableValue = *it;
						isEventPriceTableIDPresent = kTrue;
						break;
					}
					
				}

				if(tableInfo)
				{
					tableInfo->clear();
					delete tableInfo;
				}
				if(isEventPriceTableIDPresent == kFalse)
				{
					break;
				}
		
				vector <double> vec_ItemIDs = oTableValue.getItemIds();
				vector <PMString> vec_notes =  oTableValue.getNotesList();
				if(vec_notes.size() <= 0)
				{
					//CA("vec_notes.size() <= 0");
					break;
				}
				vector <double> :: iterator itemIterator;
				vector <PMString> ::iterator notesIterator =vec_notes.begin(); 
				for(itemIterator = vec_ItemIDs.begin();itemIterator != vec_ItemIDs.end();itemIterator++,notesIterator++)
				{
					
					if(selectedItemID == *itemIterator)
					{
						attributeIDfromNotes = *notesIterator;
						isNotesValid = kTrue;
						break;
					}
				}

			}
		}while(kFalse);

		return isNotesValid;
	}

	//following function added by Tushar 0n 23/12/06
	void makeTagInsideCellImpotent(IIDXMLElement * tableXMLElementPtr)
	{
		//This is a special case.The tag is of product but the Item is
		//selected.Or the tag is of Item and the product is selected.
		//So make this tag unproductive for refresh.(Impotent)		
		do
		{
			for(int32 tagIndex = 0;tagIndex < tableXMLElementPtr->GetChildCount();tagIndex++)
			{
				XMLReference cellXMLElementRef = tableXMLElementPtr->GetNthChild(tagIndex);
				InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
				for(int32 tagIndex1 = 0;tagIndex1 < cellXMLElementPtr->GetChildCount();tagIndex1++)
				{
					XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(tagIndex1);
					InterfacePtr<IIDXMLElement>cellTextTagPtr(cellTextXMLRef.Instantiate());
					makeTheTagImpotent(cellTextTagPtr);
				}
			}
         }while(kFalse);
	}

//following method added by Tushar on 15/01/06
bool8 getAttributeIDFromNotes(PublicationNode & pNode ,const double & sectionid,const  double & selectedItemID,PMString & attributeIDfromNotesSales,PMString & attributeIDfromNotesRegular,double SalePriceTabletypeId,double RegularPriceTabletypeId)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
		return kFalse;
	
	attributeIDfromNotesSales = "-1";
	attributeIDfromNotesRegular = "-1";

	bool8 isNotesValid = kFalse;
	int32 isProduct = pNode.getIsProduct();
	
	do
	{
		if(isProduct == 1)
		{
			VectorScreenTableInfoPtr tableInfo = NULL;
			if(pNode.getIsONEsource())
			{
				// For ONEsource
				//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(pNode.getPubId());
			}else
			{
				//For publication mode is selected
				double tempLangId = ptrIAppFramework->getLocaleId();
				tableInfo=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, pNode.getPubId(), tempLangId, kFalse);
			}
			if(!tableInfo)
			{
				break;
			}
			if(tableInfo->size()==0)
			{ 
				break;
			}
			bool8 isSalePriceTableIDPresent =kFalse;
			bool8 isRegularPriceTableIDPresent =kFalse;
			
			CItemTableValue oTableValue;
			CItemTableValue oTableValue1;

			VectorScreenTableInfoValue::iterator it;
			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
			{	
				if(it->getTableTypeID() == SalePriceTabletypeId)
				{
					oTableValue = *it;
					isSalePriceTableIDPresent = kTrue;
				}
				if(it->getTableTypeID() == RegularPriceTabletypeId)
				{
					oTableValue1 = *it;
					isRegularPriceTableIDPresent = kTrue;
				}
				if(isSalePriceTableIDPresent == kTrue && isRegularPriceTableIDPresent == kTrue)
				{
					break;
				}
			}
			if(isSalePriceTableIDPresent == kFalse || isRegularPriceTableIDPresent == kFalse)
				break;

			vector <double> vec_ItemIDs = oTableValue.getItemIds();
			vector <PMString> vec_notes =  oTableValue.getNotesList();
			vector <double> :: iterator itemIterator;
			vector <PMString> ::iterator notesIterator =vec_notes.begin(); 
			for(itemIterator = vec_ItemIDs.begin();itemIterator != vec_ItemIDs.end();itemIterator++,notesIterator++)
			{
				if(selectedItemID == *itemIterator)
				{
					attributeIDfromNotesSales = *notesIterator;
					isNotesValid = kTrue;
					break;
				}
			}
			vec_ItemIDs.clear();
			vec_ItemIDs = oTableValue1.getItemIds();
			vec_notes.clear();
			vec_notes =  oTableValue1.getNotesList();
			notesIterator =vec_notes.begin();
			for(itemIterator = vec_ItemIDs.begin();itemIterator != vec_ItemIDs.end();itemIterator++,notesIterator++)
			{
				if(selectedItemID == *itemIterator)
				{
					attributeIDfromNotesRegular = *notesIterator;
					isNotesValid = kTrue;
					break;
				}
			}
		}
		if(isProduct == 0)
		{
			double langId = ptrIAppFramework->getLocaleId();
			VectorScreenTableInfoPtr tableInfo=
					ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPubId(), sectionid, langId);
			if(!tableInfo)
			{
				break;
			}
			if(!tableInfo)
			{
				break;
			}
			if(tableInfo->size()==0)
			{	
				break;
			}
			bool8 isSalePriceTableIDPresent =kFalse;
			bool8 isRegularPriceTableIDPresent =kFalse;	

			CItemTableValue oTableValue;
			CItemTableValue oTableValue1;

			VectorScreenTableInfoValue::iterator it;
			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
			{	
				if(it->getTableTypeID() == SalePriceTabletypeId)
				{
					oTableValue = *it;
					isSalePriceTableIDPresent = kTrue;
				}
				if(it->getTableTypeID() == RegularPriceTabletypeId)
				{
					oTableValue1 = *it;
					isRegularPriceTableIDPresent = kTrue;
				}
				if(isSalePriceTableIDPresent == kTrue && isRegularPriceTableIDPresent == kTrue)
				{
					break;
				}
			}

			if(tableInfo)
			{
				tableInfo->clear();
				delete tableInfo;
			}

			if(isSalePriceTableIDPresent == kFalse || isRegularPriceTableIDPresent == kFalse)
				break;
	
			vector <double> vec_ItemIDs = oTableValue.getItemIds();
			vector <PMString> vec_notes =  oTableValue.getNotesList();
			vector <double> :: iterator itemIterator;
			vector <PMString> ::iterator notesIterator =vec_notes.begin(); 
			for(itemIterator = vec_ItemIDs.begin();itemIterator != vec_ItemIDs.end();itemIterator++,notesIterator++)
			{
				if(selectedItemID == *itemIterator)
				{
					attributeIDfromNotesSales = *notesIterator;
					isNotesValid = kTrue;
					break;
				}
			}
			
			vec_ItemIDs.clear();
			vec_ItemIDs = oTableValue1.getItemIds();
			vec_notes.clear();
			vec_notes =  oTableValue1.getNotesList();

			notesIterator =vec_notes.begin();
			for(itemIterator = vec_ItemIDs.begin();itemIterator != vec_ItemIDs.end();itemIterator++,notesIterator++)
			{
				if(selectedItemID == *itemIterator)
				{
					attributeIDfromNotesRegular = *notesIterator;
					isNotesValid = kTrue;
					break;
				}
			}
		}
	}while(kFalse);

	return isNotesValid;
}

void handleSprayingOfEventPriceRelatedAdditionsNew(PublicationNode & pNode,TagStruct & tagInfo,PMString & itemAttributeValue)
{
	///added on 22Sept..EventPrice addition.
	//CA("handleSprayingOfEventPriceRelatedAdditions");
    //int32 eventTabletypeId = tagInfo.elementId;	//Table type id for Event table  ////commented on 21_12	
	double eventAttributeType = tagInfo.elementId;
	//int32 eventTabletypeId = -1;
	//int32 SalePriceTabletypeId = -1;
	//int32 RegularPriceTabletypeId = -1;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
		return ;

	//eventTabletypeId = tagInfo.parentTypeID;

	//CA_NUM("tagInfo.typeId : ",tagInfo.typeId);
	//int32 eventTabletypeId = 1172;
	
	/*if((tagInfo.elementId == -701 || tagInfo.elementId == -702 || tagInfo.elementId == -703 || tagInfo.elementId == -704) && globalParentTypeId != -1)
	{
		eventTabletypeId = globalParentTypeId;
		globalParentTypeId = -1;
	}*/
		
	//CA_NUM("tagInfo.parentTypeID() :",tagInfo.parentTypeID);
    //int32 eventAttributeType = tagInfo.typeId; //hardcoded price id to distinguish whether it is EventPrice,suffics, $ off and % off. //commented on 21_12	
	//		int32 eventAttributeType = tagInfo.elementId;
	//CA_NUM("elementId :",elementId);

	double itemId = -1;

	
	

	if(tagInfo.childId  != -1 && tagInfo.childTag== 1)
	{
		itemId = tagInfo.childId;
	}
	else
	{
		itemId = pNode.getPubId();
	}
			
	

	//int32 elementId = tagInfo.elementId; //old functionality commented by Tushar to remove hardcode functinality
	//CA_NUM("tagInfo.elementId() :",tagInfo.elementId);
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == NULL)
//			return ;
	double sectionid = -1;
	//int32 pbObjectID = -1;
	if(pNode.getIsProduct() == 1)
	{
		if(global_project_level == 3)
			sectionid = CurrentSubSectionID;
		if(global_project_level == 2)
			sectionid = CurrentSectionID;
		
	}

//	PMString attributeIDfromNotes("-1");
//	PMString attributeIDfromNotesSales("-1");
//	PMString attributeIDfromNotesRegular("-1");

	//CA_NUM("sectionid :",sectionid);
	//CA_NUM("tagInfo.typeId :",tagInfo.typeId);

	double temp = tagInfo.elementId;

	//bool8 isValidNotes =  getAttributeIDFromNotesNew(pNode ,sectionid,tagInfo.typeId,attributeIDfromNotes);//old functionality commented by Tushar on 19/12/06
	//bool8 isValidNotes =  getAttributeIDFromNotesNewByElementId(pNode ,sectionid,tagInfo.typeId,attributeIDfromNotes,tagInfo.elementId); //in this method 'tagInfo.elementId' is used as 'eventPriceTableTypeID'
	//if(eventAttributeType == -703 || eventAttributeType == -704)
	//{
	//	SalePriceTabletypeId = ptrIAppFramework->ConfigCache_getPubItemSalePriceTableType();
	//	RegularPriceTabletypeId = ptrIAppFramework->ConfigCache_getPubItemRegularPriceTableType();
	//	//CA_NUM("SalePriceTabletypeId :",SalePriceTabletypeId);
	//	//CA_NUM("RegularPriceTabletypeId :",RegularPriceTabletypeId);

	//	bool8 isValidNotes =  getAttributeIDFromNotes(pNode ,sectionid,tagInfo.typeId,attributeIDfromNotesSales,attributeIDfromNotesRegular,SalePriceTabletypeId,RegularPriceTabletypeId);
	//	//bool8 isValidNotes1 =  getAttributeIDFromNotesNewByElementId(pNode ,sectionid,tagInfo.typeId,attributeIDfromNotesRegular,RegularPriceTabletypeId);
	//}
	//else 
	//	bool8 isValidNotes =  getAttributeIDFromNotesNewByElementId(pNode ,sectionid,tagInfo.typeId,attributeIDfromNotes,eventTabletypeId);
//     //CA_NUM("after tagInfo.typeId() :",tagInfo.typeId);
   //CA_NUM("after sectionid :",sectionid);
   //CA_NUM("after tagInfo.parentTypeID :",tagInfo.parentTypeID);

   //CA("after attributeIDfromNotes = " + attributeIDfromNotes);

	//added on 22Sept..EventPrice addition.
	PMString strPbObjectID;
	strPbObjectID.AppendNumber(PMReal(pNode.getPBObjectID()));
    //CA("strPbObjectID = " + strPbObjectID);
	tagInfo.tagPtr->SetAttributeValue(WideString("pbObjectId"),WideString(strPbObjectID));  //Cs4
	tagInfo.tagPtr->SetAttributeValue(WideString("rowno"),WideString("-1"/*strPbObjectID*/));  //Cs4
	//added on 22Sept..EventPrice addition.
	//	CA_NUM("elementId :",elementId);

	//if(eventAttributeType == -701)
	//{
	//	//tagInfo.tagPtr->SetAttributeValue("ID",attributeIDfromNotes);//commented by Tushar on 28/12/06
	//	//tagInfo.elementId =attributeIDfromNotes.GetAsNumber();
	//	if(ptrIAppFramework->ATTRIBUTECache_isItemAttributeEventSpecific(attributeIDfromNotes.GetAsNumber()) == kTrue){
	//		VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(tagInfo.typeId,attributeIDfromNotes.GetAsNumber(),tagInfo.sectionID);
	//		if(vecPtr != NULL)
	//			if(vecPtr->size()> 0)
	//				itemAttributeValue = vecPtr->at(0).getObjectValue();
	//	}
	//	else if(attributeIDfromNotes.GetAsNumber() == -401 || attributeIDfromNotes.GetAsNumber() == -402 || attributeIDfromNotes.GetAsNumber() == -403){
	//		itemAttributeValue = ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(tagInfo.typeId,attributeIDfromNotes.GetAsNumber(), tagInfo.languageID);
	//	}
	//	else{
	//		itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,attributeIDfromNotes.GetAsNumber(), tagInfo.languageID, kFalse );
	//	}
	//}
	//else if(eventAttributeType == -702)
	//{
	//	itemAttributeValue = ptrIAppFramework->getPriceSuffix(attributeIDfromNotes.GetAsNumber());
	//	//CA(" inside itemAttributeValue" + itemAttributeValue);
	//}
	//else 
	if(eventAttributeType == -703 || eventAttributeType == -704)
	{
		/*if(attributeIDfromNotesSales == "" && attributeIDfromNotesRegular == "")
		{
			itemAttributeValue = "";
			return;
		}
		else if(attributeIDfromNotesSales == "" || attributeIDfromNotesSales == "")
		{
			itemAttributeValue = "";
			return;
		}*/
		double eventPrice = 0;
		double regularPrice = 0;
//			int32 eventPriceID = attributeIDfromNotes.GetAsNumber();

		double eventPriceId = -1;
		double regularPriceId = -1;	
		eventPriceId = ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("EVENT_SALE_PRICE");
		regularPriceId = ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("EVENT_REGULAR_PRICE");				
		if(eventPriceId == -1 || regularPriceId == -1)
		{
			itemAttributeValue = "";
			return;
		}
		
		do
		{


			PMString eventPriceValue("");
			/*if(attributeIDfromNotesSales.GetAsNumber() == -401 || attributeIDfromNotesSales.GetAsNumber() == -402 || attributeIDfromNotesSales.GetAsNumber() == -403){
				eventPriceValue = ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(tagInfo.typeId,attributeIDfromNotesSales.GetAsNumber(), tagInfo.languageID);
			}*/
			//if(ptrIAppFramework->ATTRIBUTECache_isItemAttributeEventSpecific(eventPriceId) == kTrue){
			//	VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(pNode.getPubId(),eventPriceId,CurrentSubSectionID);
			//	if(vecPtr != NULL){
			//		if(vecPtr->size()> 0){
			//			eventPriceValue = vecPtr->at(0).getObjectValue();
			//			//CA("eventPriceValue 1111111111= " + eventPriceValue);
			//		}
			//	}
			//}
			//else
			{
				eventPriceValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemId, eventPriceId, tagInfo.languageID, CurrentSectionID, kFalse );
				//CA("eventPriceValue = " + eventPriceValue);
			}
			//PMString eventPriceValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,eventPriceID, tagInfo.languageID, kTrue );
			if(eventPriceValue == "")
			{
				itemAttributeValue = "";
				return;
			}
			
			PMString ::ConversionError * pErr = NULL;
			eventPrice = eventPriceValue.GetAsDouble(pErr,NULL);
			if(( pErr != NULL) &&(*pErr == PMString ::kNoNumber || *pErr == PMString ::kNotJustNumber))
			{//CA("11111111eventPriceValue == """);
				itemAttributeValue = "";
				return;
			}
		}while(kFalse);
		do
		{	
		//	int32 regularPriceID = ptrIAppFramework->getRegularPriceAttributeId();
//					int32 regularPriceID = ptrIAppFramework->getEventPriceAttributeId(eventTabletypeId);
//					if(regularPriceID == -1 )
//					{
//						itemAttributeValue = "";
//						return;
//					}
			PMString regularPriceValue("");
			/*if(attributeIDfromNotesRegular.GetAsNumber() == -401 || attributeIDfromNotesRegular.GetAsNumber() == -402 || attributeIDfromNotesRegular.GetAsNumber() == -403){
				regularPriceValue = ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(tagInfo.typeId,attributeIDfromNotesRegular.GetAsNumber(), tagInfo.languageID);
			}*/
			//if(ptrIAppFramework->ATTRIBUTECache_isItemAttributeEventSpecific(regularPriceId) == kTrue){
			//	VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(pNode.getPubId(),regularPriceId,CurrentSubSectionID);
			//	if(vecPtr != NULL){
			//		if(vecPtr->size()> 0){
			//			regularPriceValue = vecPtr->at(0).getObjectValue();
			//			//CA("regularPriceValue 1111111111= " + regularPriceValue);
			//		}
			//	}
			//}
			//else
			{
				regularPriceValue= ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemId, regularPriceId , tagInfo.languageID, CurrentSectionID, kFalse );
				//CA("regularPriceValue = " + regularPriceValue);
			}
//					PMString regularPriceValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,regularPriceID, tagInfo.languageID, kTrue );
			if(regularPriceValue == "")
			{	
				itemAttributeValue = "";
				return;
			}

			PMString ::ConversionError * pErr = NULL;
			regularPrice = regularPriceValue.GetAsDouble(pErr,NULL);
			if(( pErr != NULL) && (*pErr == PMString ::kNoNumber || *pErr == PMString ::kNotJustNumber))
			{	
				itemAttributeValue = "";
				return;
			}
		}while(kFalse);


		if(eventAttributeType == -703)
		{
			//PMReal temp;
			//PMReal nearestDollar = ::Ceiling(realDifference);
			//itemAttributeValue.AppendNumber(nearestDollar);
			//itemAttributeValue.AppendNumber(intDifference);
			
			double realDifference = ptrIAppFramework->getDollerOff(regularPrice ,eventPrice);
			itemAttributeValue.AppendNumber(PMReal(realDifference));
		}
		else if(eventAttributeType == -704)
		{
			itemAttributeValue = "";
			if(regularPrice != 0)
			{
				//int32 actualPercentage = (int32)((realDifference/regularPrice) * 100);
				//int32 percentageDiff = ToInt32(actualPercentage);
				//itemAttributeValue.AppendNumber(percentageDiff);
				double percentageDiff = ptrIAppFramework->getPercentageOff(regularPrice ,eventPrice);
				itemAttributeValue.AppendNumber(PMReal(percentageDiff));

			}
		}

	
	}
	
	//ended on 22Sept..EventPrice addition.

//ended on 22Sept..EventPrice addition.
}
