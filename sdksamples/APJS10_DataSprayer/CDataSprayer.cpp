#include "VCPlugInHeaders.h"
#include "HelperInterface.h"
#include "CAlert.h"
#include "PMString.h"
#include "DSFID.h"
#include "IDataSprayer.h"
#include "FileUtils.h"
#include "IFrameListComposer.h"
#include "TableUtility.h"
#include "ICellContentMgr.h"	// for kTextContentType
#include "ICellContent.h"
#include "ITableModel.h"
#include "INewTableCmdData.h"
#include "PublicationNode.h"
#include "IAppFramework.h"
#include "ILayoutUtils.h"
#include "IDocument.h"
#include "Idatabase.h"
#include "SDKUtilities.h"
#include "IScrapItem.h"
#include "ICommand.h"
#include "ITextSelectionSuite.h"
#include "ISelectionUtils.h"
#include "IXMLTagSuite.h"
#include "ISelectionManager.h"
#include "ILayoutSelectionSuite.h"
#include "IFrameContentSuite.h"
#include "ITextMiscellanySuite.h"
#include "IFrameContentSuite.h"
#include "ILayoutSelectionSuite.h"
#include "IFrameContentUtils.h"
#include "ILayoutUIUtils.h"
#include "ITextAttributeSuite.h"
#include "ITableSelectionSuite.h"
#include "CommonFunctions.h"
#include "ITextStoryThread.h"
#include "IParcelList.h"
#include "VOSSavedData.h"
#include "ITextWalkerSelectionUtils.h"
#include "ITextFocusManager.h"
#include "ITextFocus.h"
#include "ISelectionManager.h"
#include "ITextSelectionSuite.h"
#include "ThreadTextFrame.h"
#include "IItemStrand.h"
#include "IInlineData.h"
#include "ITableModelList.h"
#include "XDocBkImageSizerCmd.h"
#include "ITableGeometry.h"
#include "ITextStoryThreadDict.h"
#include "ITextStoryThread.h"
#include "IXMLReferenceData.h"
#include "IDCSInfo.h"
#include "IInlineData.h"
#include "ITableCommands.h"
#include "UIDRef.h"
#include "IPageItemTypeUtils.h"
#include "ISpread.h"
#include "ILayoutUIUtils.h"
#include "ITextFrameColumn.h"
#include "VOSSavedData.h"
#include "ITextModelCmds.h"
#include "IUIDData.h"
#include "IRangeData.h"
#include "IHierarchy.h"
#include "IScrapSuite.h"
#include "IClipboardController.h"
#include "ITransformFacade.h"
#include "ITableUtils.h"
#include "ILayoutSelectionSuite.h"
#include "ITextTarget.h"
#include "IImageUtils.h"
#include "IPageItemTypeUtils.h"
#include "TextRange.h"
#include "CCursorProvider.h"
#include "IActiveContext.h"
#include "IRulerCaret.h"
#include "ITextEditSuite.h"
#include "IPageItemScriptUtils.h"
#include "TableSourceInfoValue.h"
#include "TSTableSourceHelper.h"
#include "IImportResourceCmdData.h"
#include "URI.h"
#include "IURIUtils.h"
#include "Utils.h"
#include "IPageList.h"
#include "ISpreadList.h"
#include "IMargins.h"
#include "IPageCmdData.h"
#include "INewSpreadCmdData.h"
#include "IApplyMasterCmdData.h"
#include "IMasterSpreadList.h"
#include "ITextModelCmds.h"
#include "IConcreteSelection.h"
#include "ITableTextSelection.h"
#include "ITextTarget.h"
#include "IScrapSuite.h"
#include "IClipboardController.h"
#include "ITransformFacade.h"
#include "IPageList.h"
#include "IMargins.h"
#include "IXMLAttributeCommands.h"
#include "IXMLUtils.h"
#include "ITextModelCmds.h"
#include "string.h"

inline PMString  numToPMString(int32 num){
PMString numStr;
numStr.AppendNumber(num);
return numStr;
}

#define pNode pNode1

#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CAlert::InformationAlert(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

using namespace std;
PublicationNode pNode;
double CurrentSectionID;
double CurrentPublicationID;
double CurrentObjectTypeID;
double CurrentSubSectionID;
extern bool16 AddHeaderToTable;
bool16 IsSectionSpray = kFalse;
int32 global_project_level = -1;
UIDRef fInlineUIDRef;
bool16 HorizontalFlow=kFalse;
VectorNewImageFrameUIDList NewImageFrameUIDList;
VectorNewImageFrameUIDRefList NewImageFrameUIDListForMap;
MapNewImageFrameUIDList NewImageFrameUIDListMap;
VectorNewImageFrameUIDList NewImageFrameUIDList_WhenFrameMovesToNextColumn;
VecParameter vParameters; 
VecReturnParameter vReturnParameter;
PMString zeroByteImageList;
vector<double> idList;

char AlphabetArray[] = "abcdefghijklmnopqrstuvwxyz";

bool16 isInlineImage= kFalse; //**Flag to Check Is Tmage Frame is Embedded Inside TextFrame
static vector<double> FinalItemIds;
int32  noofTimesInlineImageCopied = -1;
boost::shared_ptr<PasteData> pasteInLineData;
ITextModel *gtextModel;
static int32 localNum=0;
static int32 localLeter=0;


bool16 isReset = kFalse;  //*** Letter /Number Keys
bool16 fColumnChange = kFalse; //In case of vertical flow(flow dir = 1)for image frame.When no space available at the bootom of original frame.
bool16 getMarginBounds(const UIDRef& pageUIDRef, PMRect& marginBoxBounds);
bool16 getPageBounds(const UIDRef& pageUIDRef, PMRect& pageBounds);
bool16 addPage(Parameter& p, PMRect& PagemarginBoxBounds);

//for Add page case
vector<UID> PageUIDList;
bool16 fPageAdded = kFalse;
int32 count_PivotList = -1;
int32 pv_count = 0;

struct LineData
{	
	int32 startCharacterIndex;
	int32 endCharacterIndex;
	bool16 IsItemTagPresentInTheLine;
	TagList listOfTagInTheLine;
};

void collectItemIDsForProduct(vector<double> & FinalItemIds);

class TabbedDataStruct
{
public:
	int32 StartIndex;
	int32 EndIndex;
	SlugStruct stencileInfo;
	double ItemID;
	int32 rowno;
	int32 colno;
};

class ItemIDAttributeValueStruct
{
public:
	double itemID;
	PMString attributeValue;
};
class TabbedTagStruct
{
public:
	TagStruct tagInfo;
	PMString columnName;
	vector<ItemIDAttributeValueStruct> itemIDWithAttributeValueList;
};
vector<TabbedTagStruct> tabbedTagStructList;
typedef vector<TabbedDataStruct> VecorTabbeddataStruct;

struct elementID_tagIndex_struct
	{
		double elementID;
		vector<int32> vector_tagIndices;
	};

struct struct_elementID_index
{
	double elementID;
	int32 index;
};
//Added for delete if empty change
double attributeId = -1;
int32 delStart = -1;
int32 delEnd =-1;
bool16 shouldDelete = kFalse;
int32 deleteCount =0;
vector<RangeData> deleteTextRanges;
////
bool16 isBoxdDeleted = kFalse;

typedef struct
{
	double elementId;
	bool16 isProcessed;
	IIDXMLElement *tagPtr;
} slugReplicated;

bool16 SelectFrame(const UIDRef& frameUIDRef)
{
	bool16 result = kFalse;
	do 
	{
		//Select the frame.
		InterfacePtr<ISelectionManager> selectionManager(Utils<ISelectionUtils>()->QueryActiveSelection ());
		if (selectionManager == nil)
		{
			//CA("selectionManager == nil");
			break;
		}

		// Deselect everything.
		if (selectionManager->SelectionExists (kInvalidClass/*any CSB*/, ISelectionManager::kAnySelection)) {
			// Clear the selection
			selectionManager->DeselectAll(nil);
		}

		// Make a layout selection.
		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
		if (!layoutSelectionSuite) {
			break;
		}

		layoutSelectionSuite->/*Select*/SelectPageItems(UIDList(frameUIDRef), 
			Selection::kReplace,  Selection::kDontScrollLayoutSelection);				
		result = kTrue;

	} while(false);

	return result;
}

class BoxBounds
{
public:
	PMReal Top;
	PMReal Bottom;
	PMReal Left;
	PMReal Right;
	UIDRef BoxUIDRef;
	PMReal compressShift;
	PMReal enlargeShift;
	bool16 shiftUP;
	bool16 isUnderImage;
	bool16 isMoved;

	BoxBounds()
	{
		Top = 0.0;
		Bottom= 0.0;
		Left= 0.0;
		Right= 0.0;
		compressShift= 0.0;
		enlargeShift= 0.0;
		shiftUP = kFalse;
		isUnderImage= kFalse;
		isMoved = kFalse;
	}
};
typedef vector<BoxBounds> vectorBoxBounds;

void getBoxPosition(PMRect &BoxBounds);
bool16 getMaxLimitsOfBoxes(const UIDList& boxList, PMRect& maxBounds, vectorBoxBounds &boxboundlist);

int32 vericalCount = 0;
int32 HorizontalCount = 0;


class CDataSprayer: public CPMUnknown<IDataSprayer>
{
public:
    
	CDataSprayer(IPMUnknown* boss);
	
    
    ~CDataSprayer()
    {
        //Close the file
    }
    
    void showTagInfo(UIDRef);
    //bool16 doesExist(IIDXMLElement * ptr, UIDRef BoxRef);
    bool16 doesExist(IIDXMLElement* , UIDRef boxRef);
    void sprayForThisBox(UIDRef, TagList& );
    bool16 isFrameTagged(const UIDRef&);
    bool16 sprayForTaggedBox(const UIDRef&);
	// bool16 sprayForTaggedBox(const UIDRef& boxUIDRef, int32 OBjectID);//added by rahul
    void getAllIds(double);
    
	UIDList selectUIDList;
	static int32	ItemSprayOverflowFlag;
	PMString imagePath;
	//bool16 ItemSprayOverflowFlag ;
    
    //	void showTagInfo(UIDRef);
    bool16 GetStoryFromBox(InterfacePtr<ITextModel>, int32, int32, PMString&);
    bool16 GetTextDimensions(char*, char*, int32*, int32*);
    void ReplicateText(UIDRef, int32, TagList);
    void fillDataInBox(const UIDRef&, TagStruct&, double, bool16 isTaggedFrame=kFalse,bool16 isFlag=kFalse);
    bool16 fillImageInBox(const UIDRef&, TagStruct& , double);
    ErrorCode CreatePicturebox(UIDRef&, UIDRef, PMRect, PMReal strokeWeight=0.0);
    bool16 ImportFileInFrame(const UIDRef& imageBox, const PMString& fromPath);
	// void forceRecompose(InterfacePtr<ITextFrame>);
    void forceRecompose(InterfacePtr<ITextFrameColumn>);
	// bool16 isTextFrameOverset(const InterfacePtr<ITextFrame>, int32&);
    bool16 isTextFrameOverset(const InterfacePtr<ITextFrameColumn>, int32&);
    bool16 removeAllText(const UIDRef& boxUIDRef);
    void fitImageInBox(const UIDRef& boxUIDRef);
    void insertNewTable(const InterfacePtr<ITextModel>& textModel,
                        int numRows,
                        int numCols,
                        TextIndex index,
                        int32 len,
                        PMReal rowHeight,
                        PMReal colWidth) const;
	void Test();
	void FillPnodeStruct(PublicationNode pubNode , double SectionID=0, double PublicationID=0,double SubSectionID=0);
	void SlugReader(const UIDRef&,int32,int32,double&,double&);
	void NewSlugReader(InterfacePtr<ITableModel> &tblModel,int32 rowIndex,int32 colIndex,double& Attr_id,double& Item_id,InterfacePtr<ITableTextSelection> &tblTxtSel);
	bool8 IsAttrInTextSlug(const UIDRef& tableUIDRef,double Attr_id);
	bool8 UpdateDataInTextSlug(const UIDRef& tableUIDRef,double Attr_id,PMString Table_col);
	ErrorCode ProcessSimpleCommand(const ClassID& commandClass, const UIDList& itemsIn, UIDList& itemsOut);
	
	//following method is added by vijay for testing on 5-1-2007
	virtual void sprayImageInTable(UIDRef& , TagStruct&);
	void fillTaggedTableInBoxCS2(const UIDRef& curBox, TagStruct& slugInfo);
	
	//10Jun tabbed text
	void arrangeForSprayingProductItemWithOtherCopyAttributes(UIDRef & boxUIDRef,InterfacePtr<ITextModel> & textModel);
	//end 10Jun tabbed text
    
	//16Jun ...items from product dropdown
	void sprayProductItemTableScreenPrintInTabbedTextForm(TagStruct & tagInfo);
	void addAttributesToANewlyCreatedTag(SlugStruct & newTagToBeAdded,XMLReference & createdElement);
	//end 16Jun
    
	void sprayItemItemTableScreenPrintInTabbedTextForm(TagStruct & tagInfo);
    
	//Added on 16Aug..This will spray section level's item's item.
	//colno = -101 for the section level's item's item
	void arrangeForSprayingItemItemWithOtherCopyAttributes(UIDRef & boxUIDRef,InterfacePtr<ITextModel> & textModel);
	
	//Added on 17-10-2006 -vijay
	void sprayCMedCustomTableScreenPrintInTabbedTextForm(TagStruct & tagInfo);
    
	void sprayMPVAttributes(UIDRef & boxUIDRef,InterfacePtr<ITextModel> & tempTextModel);
	void sprayProductKitComponentTableScreenPrintInTabbedTextForm(TagStruct & tagInfo, bool16 isKitTable);
	//void ToggleBold_or_Italic(InterfacePtr<ITextModel>, VectorHtmlTrackerPtr vectHtmlValuePtr, int32 StartTextIndex);
	
	//following function is added by vijay on 9-1-2007 to fitInlineImageInBox
	void fitInlineImageInBoxInsideTableCell(UIDRef& boxUIDRef);
	void sprayTableInsideTableCell(UIDRef& boxUIDRef, TagStruct & tagInfo);
    
	void sprayItemImage(UIDRef& boxUIDRef, TagStruct & tagInfo, PublicationNode& pNode,double itemID);
	int deleteThisBox(UIDRef boxUIDRef);
    
	void sprayXRefTableScreenPrintInTabbedTextForm(TagStruct & tagInfo);
	void sprayAccessoryTableScreenPrintInTabbedTextForm(TagStruct & tagInfo);
    
	virtual bool16 isHorizontalFlow();
	virtual void setFlow(bool16 flow);
	
	void ClearNewImageFrameList();
	VectorNewImageFrameUIDList getNewImageFrameList();
	void sprayHybridTableScreenPrintInTabbedTextForm(TagStruct & tagInfo);
	bool16 fillBMSImageInBox(const UIDRef&, TagStruct& , double);
	void sprayItemImageInsideHybridTable(UIDRef& boxUIDRef, TagStruct & tagInfo, double itemID,int32 imageno = 1);
    
	bool16 fillImageInBoxForCategoryImages(const UIDRef& boxUIDRef, TagStruct& slugInfo, double objectId);
	bool16 copySelectedItems(UIDRef &newItem , PMRect pageBounds);
	void arrangeForSprayingProductForAllStandardTableStencil(UIDRef & boxUIDRef);
	void arrangeForSprayingProductForAllStandardTableStencilForTableOption(UIDRef & boxUIDRef);
	void attachAttributestoStandardTable (XMLReference* newTag,int32 selIndex,int32 selTab);
	ErrorCode ReplaceText(ITextModel* textModel, const TextIndex position, const int32 length, const /*K2*/boost::shared_ptr<WideString>& text);	//----CS5---
	
	bool16 fillPVAndMPVImageInBox(const UIDRef&, TagStruct& , double,bool16 isForTable=kFalse, bool16 isSprayText= kFalse);
	ErrorCode DeleteText(ITextModel* textModel, const TextIndex position, const int32 length);
	void fitInlineImageInBoxInsideTableCellNEW(const UIDRef& boxUIDRef,InterfacePtr<ITextModel>& textModel,IIDXMLElement*);
    
    bool16 resetLetterOrNumberkeySeqCount(int32); //******
    
    void ClearNewImageFrameUIDList();
    MapNewImageFrameUIDList getNewImageFrameUIDList();
    
    bool16 setDifferentParameters(VecParameter& vecParameter);
    void ClearDifferentParameters();
    
    bool16 getIsColumnChange();
    void resetIsColumnChange();
    
    VecReturnParameter getReturnParameter();
    void ClearReturnParameter();
    
    //added by sagar
	bool16 addNewPageforOverflowOfFrame(UIDRef boxUIDRef);
	void setItemSprayOverflowFlag(int32 oversetflag);
	int32 getItemSprayOverflowFlag();
    
	void clearZeroByteImageList();
	PMString getZeroByteImageList();
	bool16 fileExists(PMString& path, PMString& name);
    
    void getTextModelByBoxUIDRef(const UIDRef & boxUIDRef, InterfacePtr<ITextModel> & textModel );
    PMString getAlphabetValue(int32 itemIdSeqNo);
    void handleDeleIfEmptyTags(UIDRef &boxUIDRef);
    
    void  arrangeForSprayingItemAttributeGroup(UIDRef & boxUIDRef,InterfacePtr<ITextModel> & tempTextModel);
    
};

int32 CDataSprayer::ItemSprayOverflowFlag = 0;

CREATE_PMINTERFACE (CDataSprayer, kDataSprayerImpl)

CDataSprayer::CDataSprayer(IPMUnknown* boss):CPMUnknown<IDataSprayer>(boss)
{
	do 
	{
		
	}while(0);

}

void CDataSprayer::Test()
{
	//CA(" Inside DataSprayer");
}

bool16 CDataSprayer::fileExists(PMString& path, PMString& name)
{
	PMString theName("");
	theName.Append(path);
	theName.Append(name);
    
    #ifdef MACINTOSH
    {
        InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
        if(ptrIAppFramework == NULL)
        {
            //CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
            return kFalse;
        }
        
        ptrIAppFramework->getUnixPath(theName);
    }
    #endif

	std::string tempString(theName.GetPlatformString());	
	const char *file= const_cast<char *> (tempString.c_str());
	FILE *fp=NULL;

	fp=std::fopen(file, "r");
	if(fp!=NULL)
	{
		std::fclose(fp);
		return kTrue;
	}
	return kFalse;
}

bool16 CDataSprayer::doesExist(IIDXMLElement * ptr, UIDRef BoxUidref)
{
	IIDXMLElement *xmlPtr;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}
	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::doesExist::!itagReader");	
		return kFalse;
	}

    TagList Newtaglist = itagReader->getTagsFromBox(BoxUidref, &xmlPtr);
    if(ptr==xmlPtr)
    {

        for(int32 tagIndex = 0 ; tagIndex < Newtaglist.size() ; tagIndex++)
        {
            Newtaglist[tagIndex].tagPtr->Release();
        }

        return kTrue;
    }

    for(int32 tagIndex = 0 ; tagIndex < Newtaglist.size() ; tagIndex++)
    {
        Newtaglist[tagIndex].tagPtr->Release();
    }

	return kFalse;
}


void CDataSprayer::showTagInfo(UIDRef boxRef)
{
	do
	{
		TagList tList;
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			return;
		}
		InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader)
		{
			ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::showTagInfo::!itagReader");			
			return;
		}
		tList=itagReader->getTagsFromBox(boxRef);

		/*PMString allInfo;
		for(int numTags=0; numTags<tList.size(); numTags++)
		{
			allInfo.Clear();
			allInfo.AppendNumber(PMReal(tList[numTags].elementId));
			allInfo+="\n";
			allInfo.AppendNumber(PMReal(tList[numTags].parentId));
			allInfo+="\n";
			allInfo.AppendNumber(PMReal(tList[numTags].imgFlag));
			allInfo+="\n";
			allInfo.AppendNumber(PMReal(tList[numTags].sectionID));
			allInfo+="\n";
			allInfo+="Start and end index :";
			allInfo.AppendNumber(PMReal(tList[numTags].startIndex));
			allInfo+=" / ";
			allInfo.AppendNumber(PMReal(tList[numTags].endIndex));
			CAlert::InformationAlert(allInfo);
		}
        */

		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}

	}while(kFalse);
}

bool16 CDataSprayer::GetTextDimensions(char* hayStack, char* needle, int32* start, int32* end)
{
	char* pos;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}
	pos=strstr(hayStack, needle);
	if(!pos)
	{
		ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::GetTextDimensions::!pos");		
		return kFalse;
	}
	*start=pos-hayStack;
	*end=static_cast<int32> (std::strlen(needle));
	return kTrue;
}

bool16 getTextRange(IIDXMLElement *idPtr, TagList& tagList, int32* start, int32* end)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}
	InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::getTextRange::!itagReader");	
		return kFalse;
	}

	for(int i=0; i<tagList.size(); i++)
	{
		if(tagList[i].tagPtr==idPtr && tagList[i].whichTab==4)
		{
			if(!itagReader->GetUpdatedTag(tagList[i]))
			{
				ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::getTextRange::!itagReader->GetUpdatedTag(tagList[i])");			
				return kFalse;
			}
			*start=tagList[i].startIndex+1;
			*end=tagList[i].endIndex-(*start);
			return kTrue;
		}
	}
	return kFalse;
}

bool16 validBreak(TagList& tagList, int32 index)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}
	InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::validBreak::!itagReader");	
		return kFalse;
	}

	for(int i=0; i<tagList.size(); i++)
	{
		itagReader->GetUpdatedTag(tagList[i]);
		if(tagList[i].startIndex < index && tagList[i].endIndex > index)
		{
			ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::validBreak::!startIndex and !endIndex");		
			return kFalse;
		}
	}
	return kTrue;
}

bool16 inRange(int32 x, int32 y, IIDXMLElement *idPtr, TagList tList)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}
	InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::inRange::!itagReader");	
		return kFalse;
	}

	for(int i=0; i<tList.size(); i++)
	{
		itagReader->GetUpdatedTag(tList[i]);
		if(tList[i].tagPtr==idPtr)
		{
			if(tList[i].startIndex >= x && tList[i].endIndex <= y)
				return kTrue;
			else
				return kFalse;
		}
	}
	return kTrue;
}

void CDataSprayer::ReplicateText
(UIDRef boxUID, int32 numReplication, TagList tagList)
{
	vector<slugReplicated> slugReplicatedList;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return;
	}
	for(int i=0;i<tagList.size(); i++)
	{
		if(tagList[i].whichTab!=4 || tagList[i].parentId!=-1)
			continue;

		slugReplicated sRep;
		sRep.elementId=tagList[i].elementId;
		sRep.isProcessed=kFalse;
		sRep.tagPtr=tagList[i].tagPtr;
		slugReplicatedList.push_back(sRep);
	}

	InterfacePtr<IPMUnknown> unknown(boxUID, IID_IUNKNOWN);
	UID textFrameUID =  Utils<IFrameUtils>()->GetTextFrameUID(unknown);
	if (textFrameUID == kInvalidUID)
	{
		ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::ReplicateText::textFrameUID == kInvalidUID");	
		return;
	}

	InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUID, UseDefaultIID());
	if (graphicFrameHierarchy == nil) 
	{
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::ReplicateText::!graphicFrameHierarchy");
		return;
	}
					
	InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
	if (!multiColumnItemHierarchy) {
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::ReplicateText::!multiColumnItemHierarchy");
		return;
	}

	InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
	if (!multiColumnItemTextFrame) {
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::ReplicateText::!multiColumnItemTextFrame");
		return;
	}
	
	InterfacePtr<IHierarchy>
	frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
	if (!frameItemHierarchy) {
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::ReplicateText::!frameItemHierarchy");
		return;
	}

	InterfacePtr<ITextFrameColumn>
	textFrame(frameItemHierarchy, UseDefaultIID()); // used later on
	if (!textFrame) {
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::ReplicateText::!textFrame");
		return;
	}

	TextIndex startIndex = textFrame->TextStart();
	
	InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());

	if (textModel == NULL)
	{
		ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::ReplicateText::textModel == NULL");	
		return;
	}

	TextIndex finishIndex = startIndex + textModel->GetPrimaryStoryThreadSpan()-1;
    int32 tStart=0, tEnd=0;

	for(int32 i=0; i<slugReplicatedList.size(); i++)
	{
		if(slugReplicatedList[i].isProcessed)
			continue;

		slugReplicatedList[i].isProcessed=kTrue;
		PMString entireStory("");
		startIndex = textFrame->TextStart();
		finishIndex = startIndex + textModel->GetPrimaryStoryThreadSpan()-1;
				
		if(!GetStoryFromBox(textModel, startIndex, finishIndex, entireStory))
		{
			ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::ReplicateText::GetStoryFromBox == kFalse");			
			return;
		}
	
		if(!getTextRange(slugReplicatedList[i].tagPtr, tagList ,&tStart, &tEnd))
		{
			ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::ReplicateText::getTextRange == kFalse");					
			return;
		}

		PMString ReplicatedStringL("");
		PMString ReplicatedStringR("");
		const char * charEntireStory=entireStory.GetPlatformString().c_str();
		tEnd=tStart+tEnd;
	
		while(tStart>startIndex)
		{
			if(charEntireStory[tStart]=='\n' || charEntireStory[tStart]=='\r')
			{
				if(validBreak(tagList, tStart))
				{
					ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::ReplicateText::validBreak");									
					break;
				}
			}
			PMString temp("");
			temp.Append(charEntireStory[tStart]);
			temp.Append(ReplicatedStringL);
			ReplicatedStringL=temp;
			tStart--;
		}

		//Traverse right
		while(tEnd<finishIndex)
		{
			if(charEntireStory[tEnd]=='\n' || charEntireStory[tEnd]=='\r')
			{
				if(validBreak(tagList, tEnd))
					break;
			}
			ReplicatedStringR.Append(charEntireStory[tEnd]);
			tEnd++;
		}


		ReplicatedStringL.Clear();
		for(int ptr=tStart; ptr<tEnd; ptr++)
			ReplicatedStringL.Append(charEntireStory[ptr]);

		
		for(int j=i+1; j<slugReplicatedList.size(); j++)
		{
			if(inRange(tStart, tEnd, slugReplicatedList[j].tagPtr, tagList))
			{
				slugReplicatedList[j].isProcessed=kTrue;
			}
		}

		ReplicatedStringR.Clear();
		ReplicatedStringR.Append("\r");

		int32 indent=0;
		if(charEntireStory[tStart]=='\n' || charEntireStory[tStart]=='\r')
			indent=1;

		if(!std::strlen(ReplicatedStringL.GetPlatformString().c_str()) || tStart+indent<0)
			continue;

		for(int k=0; k<numReplication; k++)
		{
	/*		VOSAllSavedData* repText=NULL;

			repText=textModel->CopyRange(tStart+indent, std::strlen(ReplicatedStringL.GetPlatformString().c_str()));
			if(!repText)
				break;
			
			ICommand* pasteCmd=textModel->PasteCmd(tStart+indent, repText);
			if(!pasteCmd)
				break;

			InterfacePtr<ICommand> pInsertTextCommand(pasteCmd);
			if(pInsertTextCommand ==NULL)
				break;
	
			if (CmdUtils::ProcessCommand(pInsertTextCommand)!=kSuccess )
				break;

			if(!indent)			
			{
				WideString* myText=new WideString(ReplicatedStringR);
				if(!myText)
				{
					CAlert::ErrorAlert("Not sufficient memory to carry on this task");
					return;
				}

				InterfacePtr<ICommand> pInsertTextCommand2(textModel->InsertCmd((int32)ReplicatedStringL.Length()+tStart+indent, myText, kFalse, NULL));
				if (pInsertTextCommand2 ==NULL)
					break;
		
				if (CmdUtils::ProcessCommand(pInsertTextCommand2)!=kSuccess )
					break;
			}
			
			forceRecompose(textFrame);
			
			finishIndex+=std::strlen(ReplicatedStringL.GetPlatformString().c_str());
			if(!indent)
				finishIndex+=1;
	 */   }
	}
}

bool16 CDataSprayer::GetStoryFromBox
(InterfacePtr<ITextModel> iModel, int32 startIndex, int32 finishIndex, PMString& story)
{
	TextIterator begin(iModel, startIndex);
	TextIterator end(iModel, finishIndex);
	for (TextIterator iter = begin; iter <= end; iter++)
	{
		//const textchar characterCode = (*iter).GetTextChar();
		UTF16TextChar characterCode; 
		int32 len=1; 
		(*iter).ToUTF16(&characterCode,&len);
		char buf;
		::CTUnicodeTranslator::Instance()->TextCharToChar(&characterCode, 1, &buf, 1); 
		story.Append(buf);
	}
	return kTrue;
}

void setTagParent(TagStruct& tStruct, double id, bool16 isTagged=kFalse)// update tag info
{
	PMString attribVal;
	attribVal.AppendNumber(PMReal(id));
	XMLReference tStructXMLRef = tStruct.tagPtr->GetXMLReference();

	if(tStruct.whichTab == 4 && tStruct.isTablePresent == 1)
	{
		attribVal.Clear();
		double objectID = pNode.getPBObjectID();
		attribVal.AppendNumber(PMReal(objectID));
	}
    
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
		return ;

	if(tStruct.whichTab == 5)
	{
		if(pNode.getIsONEsource())
		{
			PMString attrVal;
			CItemModel cItemModelObj;;
			//parentID
			attrVal.AppendNumber(PMReal(pNode.getPubId()));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tStructXMLRef, WideString("parentID"),WideString(attrVal));
			
			attrVal.Clear();
			//sectionID
			attrVal.AppendNumber(PMReal(pNode.getParentId()));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tStructXMLRef, WideString("sectionID"),WideString(attrVal));
			
			attrVal.Clear();
			//parentTypeID
			attrVal.AppendNumber(PMReal(pNode.getTypeId()));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tStructXMLRef, WideString("parentTypeID"),WideString(attrVal));
			
			attrVal.Clear();
			attrVal.AppendNumber(PMReal(pNode.getPBObjectID()));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tStructXMLRef, WideString("pbObjectId"),WideString(attrVal));

			attrVal.Clear();
			attrVal.AppendNumber(PMReal(tStruct.tableId));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tStructXMLRef, WideString("tableId"),WideString(attrVal));
		}
		else
		{
			double attributeID = tStruct.elementId;
			double InputID = -1;

			InputID = ptrIAppFramework->getSectionIdByLevelAndEventId(tStruct.catLevel ,CurrentSectionID, tStruct.languageID);
			if(tStruct.tagPtr)
			{
				attribVal.Clear();
				attribVal.AppendNumber(PMReal(InputID));
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tStructXMLRef, WideString("parentID"),WideString(attribVal));//ObjectID		
		
				attribVal.Clear();
                attribVal.AppendNumber(PMReal(CurrentSectionID));
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tStructXMLRef, WideString("sectionID"),WideString(attribVal));//Publication ID
				
				attribVal.Clear();
				attribVal.AppendNumber(PMReal(CurrentObjectTypeID));			// Need to check
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tStructXMLRef, WideString("parentTypeID"),WideString(attribVal));//Parent Type ID

				attribVal.Clear();
				attribVal.AppendNumber(PMReal(pNode.getPBObjectID()));			// Need to check
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tStructXMLRef, WideString("pbObjectId"),WideString(attribVal));

				attribVal.Clear();
				attribVal.AppendNumber(PMReal(tStruct.tableId));
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tStructXMLRef, WideString("tableId"),WideString(attribVal));
			}
		}
	}
	else
	{
		CObjectValue oVal;
		oVal = ptrIAppFramework->GETProduct_getObjectElementValue(id, CurrentSectionID);
		double parentTypeID= oVal.getObject_type_id();

		if(tStruct.tagPtr)
		{
			XMLReference tagXMLRef = tStruct.tagPtr->GetXMLReference(); 
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagXMLRef, WideString("parentID"),WideString( attribVal));
			attribVal.Clear();
			
			if(isTagged)
			{					
				attribVal.AppendNumber(tStruct.imgFlag);
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tagXMLRef,WideString("imgFlag"),WideString(attribVal));
			}
			
			attribVal.Clear();
			if(global_project_level == 3)
					attribVal.AppendNumber(PMReal(CurrentSubSectionID));
			if(global_project_level == 2)
				attribVal.AppendNumber(PMReal(CurrentSectionID));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagXMLRef, WideString("sectionID"),WideString(attribVal));
			
			attribVal.Clear();
			attribVal.AppendNumber(PMReal(parentTypeID));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagXMLRef, WideString("parentTypeID"),WideString(attribVal));

			attribVal.Clear();
			attribVal.AppendNumber(PMReal(pNode.getPBObjectID()));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagXMLRef, WideString("pbObjectId"),WideString(attribVal));


			attribVal.Clear();
			attribVal.AppendNumber(PMReal(tStruct.tableId));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagXMLRef, WideString("tableId"),WideString(attribVal));
		}
	}

}


/*
//IMPORTANT REQUEST TO DEVELOPER.

Please don't spray any data in the following two functions
void CDataSprayer ::arrangeForSprayingItemItemWithOtherCopyAttributes(UIDRef & boxUIDRef,InterfacePtr<ITextModel> & tempTextModel)
void CDataSprayer ::arrangeForSprayingProductItemWithOtherCopyAttributes(UIDRef & boxUIDRef,InterfacePtr<ITextModel> & tempTextModel)

These function just do some preparation work for the actual data spraying.
Care must be taken that the actual data is not getting sprayed inside the above two 
function.(Even if it sounds very trivial and easy to spray the data inside these two function.)
The data spraying work should be handled only in the  function.
*/

//Added on 16Aug..This will spray section level's item's item.
//colno = -101 for the section level's item's item
void CDataSprayer ::arrangeForSprayingItemItemWithOtherCopyAttributes(UIDRef & boxUIDRef,InterfacePtr<ITextModel> & tempTextModel)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return;
	}

	//Get the textmodel first
	UIDRef txtModelUIDRef = UIDRef::gNull;
	UID textFrameUID = kInvalidUID;
	
    InterfacePtr<ITextModel>  textModel;
    getTextModelByBoxUIDRef(boxUIDRef , textModel);
    if (textModel == NULL)
	{
		return;
	}
    
	tempTextModel = textModel;
	
	InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{ 
		return;
	}
	TagList tList=itagReader->getTagsFromBox(boxUIDRef);
	int numTags=static_cast<int> (tList.size());

	int32 field1_val = -1;
	if(numTags<0)
	{
		return;
	}

	bool16 isAtLeastOneItemTagPresent = kFalse;
	int32 isComponentTableAttributePresent = 0;
	vector<int32> vectorItemTagIndex;
	vector<int32> vectorInLineAllItemImageTagIndex;
	double languageId = 0;
	//Iterate thourgh the tList and collect the indices of all the item tags inside the vectorItemTagIndex.

	for(int32 tagIndex=0;tagIndex<numTags;tagIndex++)
	{
		TagStruct & firstTagInfo = tList[tagIndex];
		XMLContentReference xmlCntnRef = firstTagInfo.tagPtr->GetContentReference();
		if(xmlCntnRef.IsTable())
		{
			//CA("This is the tag attached to the table");
			continue;
		}

		PMString field_1 = firstTagInfo.tagPtr->GetAttributeValue(WideString("field1"));
		field1_val = field_1.GetAsNumber();

		PMString isChild = firstTagInfo.tagPtr->GetAttributeValue(WideString("childTag"));
		PMString isSprayItemPerFrame = firstTagInfo.tagPtr->GetAttributeValue(WideString("isSprayItemPerFrame"));
		XMLReference firstTagInfoXmlRef = firstTagInfo.tagPtr->GetXMLReference();

		if
		(
			firstTagInfo.whichTab == 4 //ItemCopyAttribute identified
			&& (firstTagInfo.isTablePresent != 1)//filter TableInTabbedText
			&& (firstTagInfo.typeId !=-701) && (firstTagInfo.typeId !=-702) && (firstTagInfo.typeId !=-703) && (firstTagInfo.typeId !=-704)
		)
		{
			if(isChild == "1" && isSprayItemPerFrame == "-1")//SectionLevel Item's Item identified.
			{
				vectorItemTagIndex.push_back(tagIndex);
				isAtLeastOneItemTagPresent = kTrue;
				if((firstTagInfo.rowno == -901) )
					isComponentTableAttributePresent = 1;
				else if((firstTagInfo.rowno == -902) )
					isComponentTableAttributePresent = 2;
				else if((firstTagInfo.rowno == -903) )
					isComponentTableAttributePresent = 3;
				languageId = firstTagInfo.languageID;
			}
			else //Means its a SectionLevel Items' tag.Treat this tag as ProductCopyAttribute tag.
			{
				//Ensure that the actual ItemCopyAttribute's value will be sprayed
				//and not its header name.

				if(firstTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-803") && isSprayItemPerFrame == "-1")
				{
					PMString attributeValue("");
					int32 itemIdSeqNo = pNode.getSequence();
					
                    attributeValue = getAlphabetValue(itemIdSeqNo);
                    
					Utils<IXMLAttributeCommands>()->SetAttributeValue(firstTagInfoXmlRef, WideString("rowno"),WideString(attributeValue));

				}
				else if(firstTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-827") && isSprayItemPerFrame == "-1")
				{
					PMString attributeValue("");
					int32 itemIdSeqNo = pNode.getSequence();

					int32 local =0;
					if (itemIdSeqNo == 0)
						local =1;
					else 
						local = itemIdSeqNo +1;
					attributeValue.AppendNumber(local);
					Utils<IXMLAttributeCommands>()->SetAttributeValue(firstTagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
				}

			}
		}
		else if(firstTagInfo.whichTab == 3 )
		{//Product tags are not welcome here.We are spraying Item.
		//Make the tag impotent.

			if(firstTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-803") && isSprayItemPerFrame == "-1")
			{
				//CA("Here 2");
				PMString attributeValue("");
				int32 itemIdSeqNo = pNode.getSequence();
				
                attributeValue = getAlphabetValue(itemIdSeqNo);
                
				Utils<IXMLAttributeCommands>()->SetAttributeValue(firstTagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
			}
			else if(firstTagInfo.typeId != -1001)
				makeTheTagImpotent(firstTagInfo.tagPtr);
			else if(firstTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-827") && isSprayItemPerFrame == "-1")
			{
				PMString attributeValue("");
				int32 itemIdSeqNo = pNode.getSequence();
				attributeValue.AppendNumber(itemIdSeqNo);
				Utils<IXMLAttributeCommands>()->SetAttributeValue(firstTagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
			}
		}

	}
	
	if(isAtLeastOneItemTagPresent == kFalse)
	{
		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
		return;
	}

	vector<vector<PMString> > Kit_vec_tablerows;  //To store Kit-Component's 'tableData' so that we can use
												  //values inside it for spraying 'Quantity' & 'Availability' attributes.
	vector<vector<PMString> > Accesssory_vec_tablerows; //To store Accesssory's 'tableData'

	vector<double> vecTableID,vecTableTypeID;
	TableSourceInfoValue *tableSourceInfoValueObj = NULL;
	bool16 isCallFromTS = kFalse;

	if(isComponentTableAttributePresent == 0) //Normal Table
	{
		do
		{	
			//--------------
			VectorScreenTableInfoPtr tableInfo=NULL;
			InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
			if(ptrTableSourceHelper != nil)
			{
				tableSourceInfoValueObj=ptrTableSourceHelper->getTableSourceInfoValueObj();
				if(tableSourceInfoValueObj)
				{
					isCallFromTS = tableSourceInfoValueObj->getIsCallFromTABLEsource();
					if(isCallFromTS)
					{
						tableInfo=tableSourceInfoValueObj->getVectorScreenItemTableInfoPtr();
						if(!tableInfo)
							return;
						
					}
				}
			}
			
			CItemTableValue oTableValue;
			VectorScreenTableInfoValue::iterator it;

			bool16 typeidFound=kFalse;
			vector<double> vec_items;
			FinalItemIds.clear();
			vecTableID.clear();
			vecTableTypeID.clear();

			CItemTableValue oTableSourceValue;
			VectorScreenTableInfoValue::iterator itr;

			if(isCallFromTS == kFalse)	//-----Condition for ContentSprayer---
			{
				tableInfo=ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPubId(), CurrentSectionID, languageId  );
				if(!tableInfo)
				{
					ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingItemItemWithOtherCopyAttributes::GETProjectProduct_getItemTablesByPubObjectId's !tableInfo");
					break;
				}
				if(tableInfo->size()==0)
				{
					ptrIAppFramework->LogInfo("AP7_DataSprayerModel::CDataSprayer ::arrangeForSprayingItemItemWithOtherCopyAttributes: table size = 0");
					break;
				}

				for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
				{//for tabelInfo start				
					oTableValue = *it;				
					vec_items = oTableValue.getItemIds();

					if(field1_val == -1)	//-------
					{
						if(FinalItemIds.size() == 0)
						{
							FinalItemIds = vec_items;
						}
						else
						{
							for(int32 i=0; i<vec_items.size(); i++)
							{	bool16 Flag = kFalse;
								for(int32 j=0; j<FinalItemIds.size(); j++)
								{
									if(vec_items[i] == FinalItemIds[j])
									{
										Flag = kTrue;
										break;
									}				
								}
								if(!Flag)
									FinalItemIds.push_back(vec_items[i]);
								
							}
						}
					}
					else
					{
						if(field1_val != oTableValue.getTableTypeID())
							continue;
						if(FinalItemIds.size() == 0)
						{
							FinalItemIds = vec_items;
						}
						else
						{
					
							for(int32 i=0; i<vec_items.size(); i++)
							{	bool16 Flag = kFalse;

								for(int32 j=0; j<FinalItemIds.size(); j++)
								{
									if(vec_items[i] == FinalItemIds[j])
									{
										Flag = kTrue;
										break;
									}				
								}
								if(!Flag)
									FinalItemIds.push_back(vec_items[i]);
							}
						}
					}
				}//for tabelInfo end
			}
			else		//-----Condition For TableSource------
			{
				vector<double>::iterator itrID;
				vector<double>selectedTblTypID = tableSourceInfoValueObj->getVec_TableType_ID();
				vector<double>selectedTblID = tableSourceInfoValueObj->getVec_Table_ID();

				for(itr = tableInfo->begin(); itr!=tableInfo->end(); itr++)
				{
					oTableSourceValue = *itr;				
					vec_items = oTableSourceValue.getItemIds();
					double table_ID = oTableSourceValue.getTableID();
					double table_Type_ID = oTableSourceValue.getTableTypeID();
					int32 check=0;

					for(itrID=selectedTblID.begin(); itrID !=selectedTblID.end();itrID++,check++)
					{
						if(selectedTblTypID.at(check) != table_Type_ID || selectedTblID.at(check) != table_ID)
							 continue;
							
						if(FinalItemIds.size() == 0)
						{
							FinalItemIds = vec_items;
							for(int32 index = 0 ; index < FinalItemIds.size() ; index++)
							{
								vecTableID.push_back(table_ID);
								vecTableTypeID.push_back(table_Type_ID);
							}
						}
						else
						{
						   	for(int32 i=0; i<vec_items.size(); i++)
							{
								bool16 Flag = kFalse;
								for(int32 j=0; j<FinalItemIds.size(); j++)
								{
									if(vec_items[i] == FinalItemIds[j])
									{
										Flag = kTrue;
										break;
									}				
								}
								if(!Flag)
								{
									vecTableID.push_back(table_ID);
									vecTableTypeID.push_back(table_Type_ID);
									FinalItemIds.push_back(vec_items[i]);
								}
								
							}
						}
						
					}
					
				}

			} 
			if(!isCallFromTS)
			{
				if(tableInfo)
				{
					tableInfo->clear();
					delete tableInfo;
				}
			}

		}while(kFalse);
	}
	else // getting items from Component  table.
	{
		//CA("isComponentTableAttributePresent == kTrue"); ??
		//do
		//{
		//	CItemTableValue oTableValue;
		//	VectorScreenTableInfoValue::iterator it  ;

		//	VectorScreenTableInfoPtr KittableInfo = NULL;
		//	VectorScreenTableInfoPtr AccessoryTableInfo = NULL;
		//	
		//	if(isComponentTableAttributePresent == 1)
		//	{
		//		bool16 isKitTable = kFalse; // we are spraying component table.
		//		KittableInfo= ptrIAppFramework->GETItem_getKitOrCompoentScreenTableByItemIdLanguageId(pNode.getPubId(), languageId, isKitTable); 
		//		if(!KittableInfo)
		//		{
		//			//CA("KittableInfo is NULL");
		//			return;
		//		}

		//		if(KittableInfo->size()==0){
		//			//CA(" KittableInfo->size()==0");
		//			delete KittableInfo;
		//			break;
		//		}

		//		it = KittableInfo->begin();
		//	}
		//	if(isComponentTableAttributePresent == 2)// we are spraying X-Ref table.
		//	{					
		//		KittableInfo= ptrIAppFramework->GETItem_getXRefScreenTableByItemIdLanguageId(pNode.getPubId(), languageId ); 
		//		if(!KittableInfo)
		//		{
		//			//CA("KittableInfo is NULL");
		//			return;
		//		}

		//		if(KittableInfo->size()==0){
		//			//CA(" KittableInfo->size()==0");
		//			delete KittableInfo;
		//			break;
		//		}

		//		it = KittableInfo->begin();
		//	}
		//	else if(isComponentTableAttributePresent == 3)// we are spraying Accessory table.
		//	{
		//		AccessoryTableInfo= ptrIAppFramework->GETItem_getAccessoryScreenTableByItemIdLanguageId(pNode.getPubId(), languageId); 
		//		if(!AccessoryTableInfo)
		//		{
		//			//CA("AccessoryTableInfo is NULL");
		//			return;
		//		}

		//		if(AccessoryTableInfo->size()==0){
		//			//CA(" AccessoryTableInfo->size()==0");
		//			delete AccessoryTableInfo;
		//			break;
		//		}

		//		it = AccessoryTableInfo->begin();
		//	}

		//	bool16 typeidFound=kFalse;
		//	vector<int32> vec_items;
		//	FinalItemIds.clear();

		//	
		//	{//for tabelInfo start				
		//		oTableValue = *it;				
		//		vec_items = oTableValue.getItemIds();
		//	
		//		if(FinalItemIds.size() == 0)
		//		{
		//			FinalItemIds = vec_items;
		//		}
		//		else
		//		{
		//			for(int32 i=0; i<vec_items.size(); i++)
		//			{	bool16 Flag = kFalse;
		//				for(int32 j=0; j<FinalItemIds.size(); j++)
		//				{
		//					if(vec_items[i] == FinalItemIds[j])
		//					{
		//						Flag = kTrue;
		//						break;
		//					}				
		//				}
		//				if(!Flag)
		//					FinalItemIds.push_back(vec_items[i]);
		//			}
		//		}
		//	}//for tabelInfo end
		//	
		//	if((isComponentTableAttributePresent == 1) || (isComponentTableAttributePresent == 2))
		//		Kit_vec_tablerows = oTableValue.getTableData();

		//	if(isComponentTableAttributePresent == 3)
		//		Accesssory_vec_tablerows = oTableValue.getTableData();

		//	if(KittableInfo)
		//	{
		//		KittableInfo->clear();				
		//		delete KittableInfo;
		//	}
		//	if(AccessoryTableInfo)
		//	{
		//		AccessoryTableInfo->clear();
		//		delete AccessoryTableInfo; 
		//	}
		//}while(0);

	}
	//FinalItemIds.push_back(pNode.getPubId());  // Added for Item under section.
	//end ItemTableHandling

	if(FinalItemIds.size()<=0)
	{	
		//if there are no items present in an item then the program flow will return from here without attaching 
		//"CustomTabbedText" tag ....
		//to attach this tag following do-while is added here .........
		do
		{   
			//TagStruct & firstTagInfo = tList[0];
			int32 lineStartCharIndex = 0;
			int32 lineEndCharIndex = -1;
			for(int32 itemTagIndex = 0;itemTagIndex < vectorItemTagIndex.size();itemTagIndex++)
			{
				//Get the actual index of the itemtag inside the taglist of the box. 
				//now find the startIndex and endIndex of the line in which the item tag is present.

				int32 index = vectorItemTagIndex[itemTagIndex];
				
				TagStruct & tagInfo=tList[index];
				int32 startPos=-1;
				int32 endPos = -1;
				Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&startPos,&endPos);
                
				if(startPos <= lineEndCharIndex)
					continue;

				int32 len=2;
				UTF16TextChar utfChar[2]={kTextChar_Null,kTextChar_Null};
				//Find the starting character's index of the line
				TextIterator startIterator(textModel,startPos); 			
				do
				{
					if(startIterator.IsNull())
					{
						//ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingItemItemWithOtherCopyAttributes::startIterator.IsNull");
						break;
					}
				
					while(kTrue)
					{
						(*startIterator).ToUTF16(utfChar,&len);
						//CA_NUM("startIterator.Position() ",startIterator.Position());
						if((utfChar[0] == kTextChar_CR) || (startIterator.IsNull())  || startPos == 0)
							break;
							
						startIterator--;
					}
				}while(kFalse);
                
				if(startPos > 0)
					lineStartCharIndex = startIterator.Position() + 1;
				else
					lineStartCharIndex = 0;

                //Find the ending character's index of the line
				TextIterator endIterator(textModel,endPos);
				do
				{
					if(endIterator.IsNull())
					{	
						//ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingItemItemWithOtherCopyAttributes::endIterator.IsNull");
						break;
					}
				
					while(kTrue)
					{
						(*endIterator).ToUTF16(utfChar,&len);
						if((utfChar[0] == kTextChar_CR) || (endIterator.IsNull()))
							break;
						endIterator++;
					}
				}while(kFalse);

				lineEndCharIndex = endIterator.Position();
								
				PMString isChild = tagInfo.tagPtr->GetAttributeValue(WideString("childTag"));
				if(isChild == "1")
				{
					UIDRef textModelUIDRef =::GetUIDRef(textModel);
					
					PMString tableTagName = "CustomTabbedText";

					TextIndex startPos =lineStartCharIndex;
					TextIndex endPos =lineEndCharIndex;
					
					ErrorCode err = kFailure;
					XMLReference CreatedElement;
					const XMLReference &  parentXMLR = kInvalidXMLReference; 
					err = Utils<IXMLElementCommands>()->CreateElement(WideString(tableTagName),textModelUIDRef,startPos,
																		endPos,parentXMLR,& CreatedElement);

					PMString attribName("ID");
					PMString attribVal("-101");
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName = "typeId";
					attribVal = "-5";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName = "header";
					attribVal.AppendNumber(tagInfo.header);
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName = "isEventField";
					attribVal = "-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
					attribName.Clear();
					attribVal.Clear();
					attribName = "deleteIfEmpty";
					attribVal = "-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName = "dataType";
					attribVal = "-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="isAutoResize";
					attribVal="";
					attribVal.AppendNumber(tagInfo.isAutoResize);
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="LanguageID";
					attribVal="";
					attribVal.AppendNumber(PMReal(tagInfo.languageID));
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
										
					attribName.Clear();
					attribVal.Clear();
					attribName = "index";
					attribVal = "4";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName = "pbObjectId";
					if(isComponentTableAttributePresent == 0)
						attribVal.AppendNumber(PMReal(pNode.getPBObjectID()));
					else
						attribVal = "-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="parentID";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="childId";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="sectionID";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="parentTypeID";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="isSprayItemPerFrame";
					attribVal.AppendNumber(tagInfo.isSprayItemPerFrame);
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="catLevel";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="imgFlag";
					attribVal="0";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName="imageIndex";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="flowDir";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="childTag";
					attribVal="1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
				
					attribName.Clear();
					attribVal.Clear();
					attribName="tableFlag";
					attribVal="0";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName="tableType";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="tableId";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="rowno";
					attribVal="";
					if(isComponentTableAttributePresent == 1)
						attribVal.AppendNumber(-901);
					else if(isComponentTableAttributePresent == 2)
						attribVal.AppendNumber(-902);
					else if(isComponentTableAttributePresent == 3)
						attribVal.AppendNumber(-903);
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
	
					attribName.Clear();
					attribVal.Clear();
					attribName="colno";
					attribVal="-1";//"-101";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
				
					attribName.Clear();
					attribVal.Clear();
					attribName="field1";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="field2";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="field3";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="field4";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="field5";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="groupKey";
                    attribVal="";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();


				}
			}
		}while(kFalse);
		
		ptrIAppFramework->LogInfo("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingItemItemWithOtherCopyAttributes::FinalItemIds.size()<=0");
		return;
	}


	bool16 firstTabbedTextTag = kTrue;
	tabbedTagStructList.clear();
	
	TextIndex startPos=-1;
	TextIndex endPos = -1;
	
	txtModelUIDRef = ::GetUIDRef(textModel);

	int32 tagIndex = 0;
	int32 tagIndexOfNewlyAddedTagDueToLinePaste = 0;
	int32 tagListSize = static_cast<int32> (tList.size());

	int32 firstCharacterIndexOfTheline = 0;
	TagStruct tagInfo;	

	TextIterator startIterator(textModel,firstCharacterIndexOfTheline);
	if(startIterator.IsNull())
	{	
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingItemItemWithOtherCopyAttributes::startIterator.IsNull");
		return;
	}

	bool16 breakTheOuterLoop = kFalse;
	bool16 areItemIDsCollected = kFalse;

	int32 lineStartCharIndex = 0;
	int32 lineEndCharIndex = -1;

	int32 firstTagIndex =0;
	int32 lastTagIndex = 0;
	int32 updatedTagIndex = 0;

	//The loop below is the ThinkTank of this function.
	//what we are doing here is scanning each line in which the ItemTag is present.
	//We already collected the indices of the itemTags inside the vectorItemTagIndex vector.
	//so iterate through that vector.
	//scan each line of the item tag.
	//We are doing this because we have to copy the line for the number of times of size of FinalItemIds
	//CA("Start Arranging table");
	int32 finalItemIndex = 0;
	for(int32 itemTagIndex = 0;itemTagIndex < vectorItemTagIndex.size();itemTagIndex++)
	{
		//Get the actual index of the itemtag inside the taglist of the box. 
		//now find the startIndex and endIndex of the line in which the item tag is present.

		int32 index = vectorItemTagIndex[itemTagIndex];
        bool16 headerPresentInTheRow = kFalse;

		//following statement is to be removed.
		lastTagIndex = index;

		tagInfo=tList[index];
		PMString isHeader = tagInfo.tagPtr->GetAttributeValue(WideString("header"));
		if(isHeader == "1")
		{
			//CA("header present in the row");
			headerPresentInTheRow = kTrue;
		}

		int32 startPos=-1;
		int32 endPos = -1;
		Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&startPos,&endPos);

		if(startPos <= lineEndCharIndex)
		 continue;

		int32 len=2;
		UTF16TextChar utfChar[2]={kTextChar_Null,kTextChar_Null};
		//Find the starting character's index of the line
		TextIterator startIterator(textModel,startPos); 			
		do
		{
			if(startIterator.IsNull())
			{	
				//ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingItemItemWithOtherCopyAttributes::startIterator.IsNull");
				break;
			}
			
			while(kTrue)
			{
				(*startIterator).ToUTF16(utfChar,&len);
				if((utfChar[0] == kTextChar_CR) || (startIterator.IsNull())  || startPos == 0)
				break;
					
				startIterator--;
			}
		}while(kFalse);

		if(startPos > 0)
			lineStartCharIndex = startIterator.Position() + 1;
		else
			lineStartCharIndex = 0;
					

        //Find the ending character's index of the line
		TextIterator endIterator(textModel,endPos);
		TextIterator endIterator1(textModel,endPos);
		do
		{
			if(endIterator.IsNull())
			{	
				//ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingItemItemWithOtherCopyAttributes::endIterator.IsNull");
				break;
			}
			int32 temp=0;
			while(kTrue)
			{
				if((endIterator1.IsNull() == kTrue))
				{
					//CA("(endIterator.IsNull() == kTrue)");
					break;
				}
				(*endIterator1).ToUTF16(utfChar,&len);
                
				if((utfChar[0] == kTextChar_CR))
				{
					temp++;								
				}	
				endIterator1++;
			}

			while(kTrue)
			{
				if((endIterator.IsNull() == kTrue))
				{
					//CA("(endIterator.IsNull() == kTrue)");
					break;
				}
				(*endIterator).ToUTF16(utfChar,&len);

				if((utfChar[0] == kTextChar_CR) || (endIterator.IsNull() == kTrue))
				{	

					if((utfChar[0] == kTextChar_CR) )
					{
						if(temp > 1)
						{
							endIterator++;								
						}	
						else
						{
							WideString* enterChar= new WideString("\r");
                            textModel->Insert(endIterator.Position(),enterChar);
                            CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
							endIterator++;
							delete enterChar;
						}

					}
					break;
				}
				endIterator++;
			}
		}while(kFalse);

		lineEndCharIndex = endIterator.Position();
		int32 numberOfTagsInALine = 0;
		int32 numberOfTimesTheLineToBeCopied = 0;
		

		CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
		InterfacePtr<ISelectionManager> selectionManager(Utils<ISelectionUtils>()->QueryActiveSelection ());
		if (selectionManager == nil) 
		{
			//CA("selectionManager == nil");
			break;
		}
		// Deselect everything.
		if (selectionManager->SelectionExists (kInvalidClass/*any CSB*/, ISelectionManager::kAnySelection)) {
			// Clear the selection
			selectionManager->DeselectAll(nil);
		}
			//Find all the tags which are in the current line.
		for(int32 normalTagIndex = 0;normalTagIndex < numTags;normalTagIndex++)
		{
			TagStruct & tagInfo = tList[normalTagIndex];
			int32 startPos=-1;
			int32 endPos = -1;
			Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&startPos,&endPos);
			XMLReference tagInfoXmlRef = tagInfo.tagPtr->GetXMLReference(); 

			if(startPos >= lineStartCharIndex && endPos <= lineEndCharIndex)
			{
				numberOfTagsInALine++;
				
				if(headerPresentInTheRow)
				{
					numberOfTimesTheLineToBeCopied = static_cast<int32>(FinalItemIds.size());
					PMString attrIndex("index");
					PMString indexValue  = tagInfo.tagPtr->GetAttributeValue(WideString(attrIndex));
					PMString isChild = tagInfo.tagPtr->GetAttributeValue(WideString("childTag"));
					PMString strTableFlag = tagInfo.tagPtr->GetAttributeValue(WideString("tableFlag"));
					if(strTableFlag == "-15")
					{
						continue;
					}
					else if( indexValue == "4" && isChild == "1" && strTableFlag != "1")
					{
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("header"),WideString("1"));

						//change  the tableFlag to -12 for item_copy_attributes in text frame.
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("tableFlag"),WideString("-12"));

						if((tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-805")))
						{
							PMString attributeValue("");										
							attributeValue.Append("Quantity");
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if(tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-806"))
						{
							PMString attributeValue("");								
							attributeValue.Append("Availability");
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if(tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-807"))
						{
							PMString attributeValue("");								
							attributeValue.Append("Cross-reference Type");
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if(tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-808"))
						{
							PMString attributeValue("");								
							attributeValue.Append("Rating");
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if(tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-809"))
						{
							PMString attributeValue("");								
							attributeValue.Append("Alternate");
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if(tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-810"))
						{
							PMString attributeValue("");								
							attributeValue.Append("Comments");
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if(tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-811"))
						{
							PMString attributeValue("");								
							attributeValue.Append("");
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if(tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-812"))
						{
							PMString attributeValue("");								
							attributeValue.Append("Quantity");
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if(tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-813"))
						{
							PMString attributeValue("");								
							attributeValue.Append("Required");
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if(tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-814"))
						{
							PMString attributeValue("");								
							attributeValue.Append("Comments");
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}

					}
					else if(strTableFlag != "1")
					{
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("header"),WideString("1"));
						//Header For nonItemCopyAttributes
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("tableFlag"),WideString("-12"));
					}
					else if(strTableFlag == "1")
					{
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("ID"),WideString("-103"));
					}
				}
				else
				{
					//CA(" NO headerPresentInTheRow . ");
					numberOfTimesTheLineToBeCopied = static_cast<int32>(FinalItemIds.size() - 1);
					PMString attrIndex("index");
					PMString indexValue  = tagInfo.tagPtr->GetAttributeValue(WideString(attrIndex));
					PMString isChild = tagInfo.tagPtr->GetAttributeValue(WideString("childTag"));
					PMString strTableFlag = tagInfo.tagPtr->GetAttributeValue(WideString("tableFlag"));

					PMString strID = tagInfo.tagPtr->GetAttributeValue(WideString("ID"));
					if(strTableFlag == "-15")
					{
						continue;
					}
					else if(indexValue == "4" && isChild == "1" && strTableFlag != "1")
					{
						PMString attributeValue;
						attributeValue.AppendNumber(PMReal(FinalItemIds[0]));

						if (selectionManager->SelectionExists (kInvalidClass/*any CSB*/, ISelectionManager::kAnySelection)) {
							// Clear the selection
							selectionManager->DeselectAll(nil);
						}
                        
                        Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("childId"),WideString(attributeValue));
                        Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("tableFlag"),WideString("-12"));
							
                        if(vecTableID.size() > 0 && vecTableTypeID.size() > 0){
                            PMString idStr("");
                            idStr.AppendNumber(PMReal(vecTableID[0]));
                            Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("tableId"),WideString(idStr));
                            idStr.Clear();
                            idStr.AppendNumber(PMReal(vecTableTypeID[0]));
                            Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("typeId"),WideString(idStr));
                        }

						if(tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-803"))
						{
							attributeValue.Clear();										
							attributeValue.Append("a");
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if((isComponentTableAttributePresent == 1) && (tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-805")) )
						{
							attributeValue.Clear();
							attributeValue.Append(Kit_vec_tablerows[0][0]);
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						
						else if((isComponentTableAttributePresent == 1) && (tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-806")) )
						{										
							attributeValue.Clear();	
							//CA(Kit_vec_tablerows[0][1]);
							attributeValue.Append((Kit_vec_tablerows[0][1]));												
							//tagInfo.tagPtr->SetAttributeValue(WideString("rowno"),WideString(attributeValue));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if((isComponentTableAttributePresent == 2) && (tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-807")) )
						{										
							attributeValue.Clear();
							attributeValue.Append((Kit_vec_tablerows[0][0]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if((isComponentTableAttributePresent == 2) && (tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-808")) )
						{										
							attributeValue.Clear();
							attributeValue.Append((Kit_vec_tablerows[0][1]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if((isComponentTableAttributePresent == 2) && (tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-809")) )
						{										
							attributeValue.Clear();
							attributeValue.Append((Kit_vec_tablerows[0][2]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if((isComponentTableAttributePresent == 2) && (tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-810")) )
						{										
							attributeValue.Clear();
							attributeValue.Append((Kit_vec_tablerows[0][3]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if((isComponentTableAttributePresent == 2) && (tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-811")) )
						{										
							attributeValue.Clear();
							attributeValue.Append((Kit_vec_tablerows[0][4]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if((isComponentTableAttributePresent == 3) && (tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-812")) )
						{										
							attributeValue.Clear();
							attributeValue.Append((Accesssory_vec_tablerows[0][0]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if((isComponentTableAttributePresent == 3) && (tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-813")) )
						{										
							attributeValue.Clear();
							attributeValue.Append((Accesssory_vec_tablerows[0][1]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if((isComponentTableAttributePresent == 3) && (tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-814")) )
						{										
							attributeValue.Clear();
							attributeValue.Append((Accesssory_vec_tablerows[0][2]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if(tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-827")) //**** NUmber Keys
						{
							attributeValue.Clear();										
							attributeValue.Append("1");
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}

					}
					else if(indexValue == "4" && isChild != "1" && strTableFlag != "1" )
					{									
						PMString attributeValue;
						attributeValue.AppendNumber(PMReal(pNode.getPubId()));

						if(!isInlineImage){
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("typeId"),WideString(attributeValue));
						}
						else{

							finalItemIndex = itemTagIndex;							
							attributeValue.clear();
							attributeValue.AppendNumber(PMReal(FinalItemIds[itemTagIndex]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("parentID"),WideString(attributeValue));

							attributeValue.clear();
							attributeValue.AppendNumber(PMReal(pNode.getPBObjectID()));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("pbObjectId"),WideString(attributeValue));
						}
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("tableFlag"),WideString("-12"));
					}
					else if(strTableFlag != "1")
					{
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("typeId"),WideString("-1"));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("tableFlag"),WideString("-12"));
					}
				}
				PMString pbObjID("");
				pbObjID.AppendNumber(PMReal(pNode.getPBObjectID()));
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("pbObjectId"),WideString(pbObjID));
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("tableType"),WideString("2"));
			}
		}

		int32 textSelectionStartAt = lineEndCharIndex;
		int32 length = lineEndCharIndex -lineStartCharIndex + 1;
		int32 startPasteIndex = lineEndCharIndex + 1;
		if(lineEndCharIndex > lineStartCharIndex)
		{
			boost::shared_ptr< PasteData > pasteData;
            startPasteIndex--;
			TextIndex destEnd = startPasteIndex; 
			for(int32 lineIndex=0;lineIndex<numberOfTimesTheLineToBeCopied;lineIndex++)
			{
				do
				{
					//CA("Start Copying");
					InterfacePtr<ICommand> copyStoryRangeCmd(CmdUtils::CreateCommand(kCopyStoryRangeCmdBoss));
					if (copyStoryRangeCmd == nil) {//CA("copyStoryRangeCmd == nil");
						break;
					}

					// Refer the command to the source story and range to be copied.
					InterfacePtr<IUIDData> sourceUIDData(copyStoryRangeCmd, UseDefaultIID());
					if (sourceUIDData == nil) {//CA("sourceUIDData == nil");
						break;
					}
					
					sourceUIDData->Set(txtModelUIDRef);
					InterfacePtr<IRangeData> sourceRangeData(copyStoryRangeCmd, UseDefaultIID());
					if (sourceRangeData == nil) {//CA("sourceRangeData == nil");
						break;
					}
					
					sourceRangeData->Set(lineStartCharIndex, lineEndCharIndex);

					// Refer the command to the destination story and the range to be replaced.
					UIDList itemList(txtModelUIDRef);
					copyStoryRangeCmd->SetItemList(itemList);
					
					InterfacePtr<IRangeData> destRangeData(copyStoryRangeCmd, IID_IRANGEDATA2);

					destRangeData->Set(startPasteIndex, destEnd );

					// Process CopyStoryRangeCmd
					ErrorCode status = CmdUtils::ProcessCommand(copyStoryRangeCmd);

				}while(kFalse);

				startPasteIndex = startPasteIndex + length -1;
				destEnd = startPasteIndex;
				//CA_NUM("startPasteIndex : ",startPasteIndex);
				//CA_NUM("destEnd : ",destEnd);
			}
			int32 textSelectionEndAt = startPasteIndex-1;
			TagList newTagList = itagReader->getTagsFromBox(boxUIDRef);
			int32 pastedTagCount = 0;
			for(int32 newTagIndex = 0;newTagIndex < newTagList.size();newTagIndex++)
			{
				TagStruct & newTagInfo = newTagList[newTagIndex];

				int32 newStartPos = -1;
				int32 newEndPos = -1;
				Utils<IXMLUtils>()->GetElementIndices(newTagInfo.tagPtr,&newStartPos,&newEndPos);
				if((newStartPos >= textSelectionStartAt) && (newEndPos <= textSelectionEndAt))
				{
					PMString attrIndex("index");
					PMString indexValue  = newTagInfo.tagPtr->GetAttributeValue(WideString(attrIndex));
					PMString isChild  = newTagInfo.tagPtr->GetAttributeValue(WideString("childTag"));
					PMString strTableFlag = newTagInfo.tagPtr->GetAttributeValue(WideString("tableFlag"));
					PMString strParentTypeId = newTagInfo.tagPtr->GetAttributeValue(WideString("parentTypeID"));
					PMString strID = newTagInfo.tagPtr->GetAttributeValue(WideString("ID"));
					XMLReference newTagInfoXmlRef = newTagInfo.tagPtr->GetXMLReference();

					double itemID =-1;
					int32 itemIdSeqNo = 0;
					if(headerPresentInTheRow)
					{
						itemID = FinalItemIds[pastedTagCount / numberOfTagsInALine];
						itemIdSeqNo = (pastedTagCount / numberOfTagsInALine) ;
					}
					else
					{
						itemID = FinalItemIds[pastedTagCount / numberOfTagsInALine + 1];
						itemIdSeqNo = (pastedTagCount / numberOfTagsInALine + 1);
					}

					PMString attributeValue;
					attributeValue.AppendNumber(PMReal(itemID));
					if(strTableFlag == "-15")
					{
						//CA("Continue.strTableFlag == -15");
						continue;
					}
					else if(indexValue == "4" && isChild == "1" && strTableFlag != "1" /*&& strID == "-701"*/)
					{
						Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("childId"),WideString(attributeValue));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("tableFlag"),WideString("-12"));

						if(vecTableID.size() > 0 && vecTableTypeID.size() > 0){
							PMString idStr("");
							idStr.AppendNumber(PMReal(vecTableID[itemIdSeqNo]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("tableId"),WideString(idStr));
							idStr.Clear();
							idStr.AppendNumber(PMReal(vecTableTypeID[itemIdSeqNo]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("typeId"),WideString(idStr));
							
						}

						if(newTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-803"))
						{
							attributeValue.Clear();
                            
                            attributeValue = getAlphabetValue(itemIdSeqNo);
                            
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if((isComponentTableAttributePresent == 1) && (newTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-805")))
						{
                            // Quantity
							attributeValue.Clear();
							attributeValue.Append(Kit_vec_tablerows[itemIdSeqNo][0]);
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if((isComponentTableAttributePresent == 1) && (newTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-806")))
						{										
							attributeValue.Clear();	 // Availability
							attributeValue.Append((Kit_vec_tablerows[itemIdSeqNo][1]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if((isComponentTableAttributePresent == 2) && (newTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-807")))
						{										
							attributeValue.Clear();	 // Availability
							attributeValue.Append((Kit_vec_tablerows[itemIdSeqNo][0]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if((isComponentTableAttributePresent == 2) && (newTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-808")))
						{										
							attributeValue.Clear();	 // Availability
							attributeValue.Append((Kit_vec_tablerows[itemIdSeqNo][1]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if((isComponentTableAttributePresent == 2) && (newTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-809")))
						{										
							attributeValue.Clear();	 // Availability
							attributeValue.Append((Kit_vec_tablerows[itemIdSeqNo][2]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if((isComponentTableAttributePresent == 2) && (newTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-810")))
						{										
							attributeValue.Clear();	 // Availability
							attributeValue.Append((Kit_vec_tablerows[itemIdSeqNo][3]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if((isComponentTableAttributePresent == 2) && (newTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-811")))
						{										
							attributeValue.Clear();	 // Availability
							attributeValue.Append((Kit_vec_tablerows[itemIdSeqNo][4]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
						else if((isComponentTableAttributePresent == 3) && (newTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-812")))
						{
							attributeValue.Clear();	 // Availability
							attributeValue.Append((Accesssory_vec_tablerows[itemIdSeqNo][0]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}	
						else if((isComponentTableAttributePresent == 3) && (newTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-813")))
						{										
							attributeValue.Clear();	 // Availability
							attributeValue.Append((Accesssory_vec_tablerows[itemIdSeqNo][1]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}	
						else if((isComponentTableAttributePresent == 3) && (newTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-814")))
						{										
							attributeValue.Clear();	 // Availability
							attributeValue.Append((Accesssory_vec_tablerows[itemIdSeqNo][2]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}	
						else if((isComponentTableAttributePresent == 2))
						{
							if((Kit_vec_tablerows[itemIdSeqNo][4]) == "N")
							{
								attributeValue.Clear();												
								attributeValue.Append("-NCitem"); //"Non Catalog Item"
								Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
							}
						}
						else if(newTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-827") ) //*** Number Keys
						{
							PMString attributeValue("");
							attributeValue.AppendNumber(PMReal(itemIdSeqNo+1));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("rowno"),WideString(attributeValue));
						}
					}
					else if(indexValue == "4" && isChild != "1" && strTableFlag != "1")
					{
						PMString attributeValue;
						attributeValue.AppendNumber(PMReal(pNode.getPubId()));
						if(!isInlineImage){
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("typeId"),WideString(attributeValue));
						}
						else{						
							//CA("Is InLine Image");							
							attributeValue.clear();							
							attributeValue.AppendNumber(PMReal(FinalItemIds[++finalItemIndex]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("parentID"),WideString(attributeValue));

							attributeValue.clear();							
							attributeValue.AppendNumber(PMReal(pNode.getPBObjectID()));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("pbObjectId"),WideString(attributeValue));
						}

						Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("tableFlag"),WideString("-12"));
					}
					else if(strTableFlag != "1")
					{
						PMString attributeValue;
						attributeValue.AppendNumber(PMReal(pNode.getPubId()));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("typeId"),WideString("-1"));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("tableFlag"),WideString("-12"));
					}
					else if(strTableFlag == "1")
					{
						Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("ID"),WideString("-1"));
					}
					Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("header"),WideString("-1"));
					pastedTagCount++;	
				}
			}
			if(tagInfo.childTag == 1)
			{
				UIDRef textModelUIDRef =::GetUIDRef(textModel);
			
				PMString tableTagName = "CustomTabbedText";
				
				ErrorCode err = kFailure;
				XMLReference CreatedElement;
				const XMLReference &  parentXMLR = kInvalidXMLReference; 
				err = Utils<IXMLElementCommands>()->CreateElement(WideString(tableTagName), 
													textModelUIDRef,
													lineStartCharIndex, startPasteIndex -1,parentXMLR,& CreatedElement);



				PMString attribName("ID");
				PMString attribVal("-101");
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
				
				attribName.Clear();
				attribVal.Clear();
				attribName = "typeId";
				attribVal = "-5";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName = "header";
				attribVal.AppendNumber(PMReal(newTagList[0].header));
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName = "isEventField";
				attribVal = "-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName = "deleteIfEmpty";
				attribVal = "-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName = "dataType";
				attribVal = "-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName="isAutoResize";
				attribVal="";
				attribVal.AppendNumber(PMReal(newTagList[0].isAutoResize));
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName="LanguageID";
				attribVal="";
				attribVal.AppendNumber(PMReal(newTagList[0].languageID));
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName = "index";
				attribVal = "4";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
				
				attribName.Clear();
				attribVal.Clear();
				attribName = "pbObjectId";
				if(isComponentTableAttributePresent == 0)
					attribVal.AppendNumber(PMReal(pNode.getPBObjectID()));
				else
					attribVal = "-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName="parentID";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName="childId";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName="sectionID";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
				
				attribName.Clear();
				attribVal.Clear();
				attribName="parentTypeID";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName="isSprayItemPerFrame";
				attribVal.AppendNumber(PMReal(newTagList[0].isSprayItemPerFrame));
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName="catLevel";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName="imgFlag";
				attribVal="0";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName="imageIndex";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName="flowDir";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
				
				attribName.Clear();
				attribVal.Clear();
				attribName="childTag";
				attribVal="1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
							
				attribName.Clear();
				attribVal.Clear();
				attribName="tableFlag";
				attribVal="0";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
				
				attribName.Clear();
				attribVal.Clear();
				attribName="tableType";
				attribVal="2";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName="tableId";
				attribVal="-1";
				if(isCallFromTS == kFalse){
					attribVal="-1";
				}
				else{}

				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));				
				
				attribName.Clear();
				attribVal.Clear();
				attribName="rowno";
				attribVal="";
				
				if(isComponentTableAttributePresent == 1)
					attribVal.AppendNumber(-901);
				else if(isComponentTableAttributePresent == 2)
					attribVal.AppendNumber(-902);
				else if(isComponentTableAttributePresent == 3)
					attribVal.AppendNumber(-903);
				else
					attribVal.AppendNumber(-1);
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
				attribName.Clear();
				attribVal.Clear();
				attribName="colno";
				attribVal="-1";//"-101";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName="field1";
				attribVal.AppendNumber(PMReal(field1_val));		//="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));	
				
				attribName.Clear();
				attribVal.Clear();
				attribName="field2";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));	

				attribName.Clear();
				attribVal.Clear();
				attribName="field3";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));	

				attribName.Clear();
				attribVal.Clear();
				attribName="field4";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));	

				attribName.Clear();
				attribVal.Clear();
				attribName="field5";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                
                attribName.Clear();
                attribVal.Clear();
                attribName="groupKey";
                attribVal="";
                err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
			}
			
			for(int32 tagIndex = 0 ; tagIndex < newTagList.size() ; tagIndex++)
			{
				newTagList[tagIndex].tagPtr->Release();
			}
		}
		firstTagIndex = index;
	}

	if(tList.size() > 0)
	{
		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
	}
}

void CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes(UIDRef & boxUIDRef,InterfacePtr<ITextModel> & tempTextModel)
{
	//Get the TextModel first
	UIDRef txtModelUIDRef = UIDRef::gNull;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		return;
	}

	UID textFrameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	}
	if (textFrameUID == kInvalidUID)
	{
		return;
	}

    InterfacePtr<ITextModel>  textModel;
    getTextModelByBoxUIDRef(boxUIDRef , textModel);
	if (textModel == NULL)
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::!textModel");	
		return;
	}

	InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{ 
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::!itagReader");	
		return;
	}
	TagList tList=itagReader->getTagsFromBox(boxUIDRef);
	int numTags=static_cast<int>(tList.size());
	if(numTags<0)
	{
		ptrIAppFramework->LogInfo("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::numTags<0");	
		return;
	}
	bool16 isAtLeastOneItemTagPresent = kFalse;

	bool16 isEventItemPriceItemTagPresent = kFalse;
	vector<int32> vectorItemTagIndex;
	double langId = ptrIAppFramework->getLocaleId();
    
	int32 field_1 = -1;
	TagStruct firstTagInfo ;
	for(int32 tagIndex=0;tagIndex<numTags;tagIndex++)
	{
		firstTagInfo = tList[tagIndex];
        langId = firstTagInfo.languageID;
		if(firstTagInfo.tableType == 7 &&  tagIndex == 0)
		{
			if(tList[1].isTablePresent == 1)
				arrangeForSprayingProductForAllStandardTableStencilForTableOption(boxUIDRef);
			else
				arrangeForSprayingProductForAllStandardTableStencil(boxUIDRef);
			return;
		}
		XMLContentReference xmlCntnRef = firstTagInfo.tagPtr->GetContentReference();
		XMLReference firstTagInfoXmlRef = firstTagInfo.tagPtr->GetXMLReference();
		if(xmlCntnRef.IsTable())
		{
			//CA("This is the tag attached to the table");
			continue;
		}

		if(firstTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-803"))
		{
			PMString attributeValue("");
			int32 itemIdSeqNo = pNode.getSequence();
            
            attributeValue = getAlphabetValue(itemIdSeqNo);
            
			if(firstTagInfo.tagPtr->GetAttributeValue(WideString("isSprayItemPerFrame")) == WideString("1")
				|| firstTagInfo.tagPtr->GetAttributeValue(WideString("isSprayItemPerFrame")) == WideString("2")) //isSprayItemPerFrame with Horizontal Flow, // 2 = isSprayItemPerFrame with Verticle flow
			{
								
				attributeValue.clear();
                attributeValue = getAlphabetValue(localLeter);
                
				localLeter++;
			}
			Utils<IXMLAttributeCommands>()->SetAttributeValue( firstTagInfoXmlRef,  WideString("rowno"),WideString(attributeValue));
		}


		if(firstTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-827")) //*** Number Keys
		{
			PMString attributeValue("");
			int32 itemIdSeqNo = pNode.getSequence();

			if(firstTagInfo.tagPtr->GetAttributeValue(WideString("isSprayItemPerFrame")) == WideString("1")
				|| firstTagInfo.tagPtr->GetAttributeValue(WideString("isSprayItemPerFrame")) == WideString("2")) //isSprayItemPerFrame with Horizontal Flow, // 2 = isSprayItemPerFrame with Verticle flow
			{				
				itemIdSeqNo= ++localNum;
			}			
			attributeValue.AppendNumber(itemIdSeqNo);
			Utils<IXMLAttributeCommands>()->SetAttributeValue( firstTagInfoXmlRef,  WideString("rowno"),WideString(attributeValue));	
		}

		if(firstTagInfo.whichTab == 4 //ItemTag present
			&& (firstTagInfo.typeId == -1 || firstTagInfo.header == 1) 
			//ensure that this is not a ItemTable Tag
			&& (firstTagInfo.isTablePresent != 1)			
		)
		{
			vectorItemTagIndex.push_back(tagIndex);
			isAtLeastOneItemTagPresent = kTrue;
		}
		else if(firstTagInfo.whichTab == 4 && firstTagInfo.isTablePresent == 1)
		{//This is a totally invalid tag for product.i.e ItemTableTag.
			makeTheTagImpotent(firstTagInfo.tagPtr);
		}
		PMString fld_1	= firstTagInfo.tagPtr->GetAttributeValue(WideString("field1"));
		field_1 = fld_1.GetAsNumber();
	
		bool16 isCallTableSource = kFalse;
		int32 selectedTableTS = 0;
		InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
		if(ptrTableSourceHelper != nil)
		{
			TableSourceInfoValue * tableSourceInfoValueObj = ptrTableSourceHelper->getTableSourceInfoValueObj();
			if(tableSourceInfoValueObj)
			{
				isCallTableSource = tableSourceInfoValueObj->getIsCallFromTABLEsource();
			}
			selectedTableTS = (int32)tableSourceInfoValueObj->getVec_TableType_ID().size();
		}
	
		if((selectedTableTS > 1 && isCallTableSource == kTrue)&& ((firstTagInfo.typeId == -55) || (firstTagInfo.typeId ==-56)))
		{
			//CA("isAtLeastOneItemTagPresent = kTrue");
			vectorItemTagIndex.push_back(tagIndex);
			isAtLeastOneItemTagPresent = kTrue;	
		}
		else if(firstTagInfo.typeId == -57)
		{
			vectorItemTagIndex.push_back(tagIndex);
			isAtLeastOneItemTagPresent = kTrue;	
		}
		
	}

	if(isAtLeastOneItemTagPresent == kFalse)
	{
		if(tList.size() > 0)
		{
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
		}
		return;
	}
	vector<double> vecTableID,vecTableTypeID;
	double sectionid = -1;
	if(global_project_level == 3)
		sectionid = CurrentSubSectionID;
	if(global_project_level == 2)
		sectionid = CurrentSectionID;


	TableSourceInfoValue *tableSourceInfoValueObj = NULL;
	bool16 isCallFromTS = kFalse;

	if(pNode.getIsProduct() == 1)
	{
		bool16 ItemAbsentinProductFlag = kFalse;
		do
		{				
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == NULL)
			{
				CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
				break;
			}
				
			VectorScreenTableInfoPtr tableInfo = NULL;
			if(pNode.getIsONEsource())
			{
				// For ONEsource mode
				//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(pNode.getPubId());
			}else
			{
				//For publication mode 
				tableInfo =ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, pNode.getPubId(), langId,  kTrue); // getting only customtable data
			}

			if(!tableInfo)
			{
				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::tableInfo is NULL");	
				ItemAbsentinProductFlag = kTrue;
				break;
			}

			if(tableInfo->size()==0)
			{
                //if there are no items present in a product then to attach "CustomTabbedText" tag
				//following do-while is added here .........
				do
				{   
					int32 lineStartCharIndex = 0;
					int32 lineEndCharIndex = -1;
					for(int32 itemTagIndex = 0;itemTagIndex < vectorItemTagIndex.size();itemTagIndex++)
					{
						//Get the actual index of the itemtag inside the taglist of the box. 
						//now find the startIndex and endIndex of the line in which the item tag is present.

						int32 index = vectorItemTagIndex[itemTagIndex];
						
						TagStruct & tagInfo=tList[index];
						int32 startPos=-1;
						int32 endPos = -1;
						Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&startPos,&endPos);

						if(startPos <= lineEndCharIndex)
							continue;

						int32 len=2;
						UTF16TextChar utfChar[2]={kTextChar_Null,kTextChar_Null};
						//Find the starting character's index of the line
						TextIterator startIterator(textModel,startPos); 			
						do
						{
							if(startIterator.IsNull())
							{
								//ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingItemItemWithOtherCopyAttributes::startIterator.IsNull");
								break;
							}
						
							while(kTrue)
							{
								(*startIterator).ToUTF16(utfChar,&len);
								if((utfChar[0] == kTextChar_CR) || (startIterator.IsNull()) || startPos == 0)
								break;
									
								startIterator--;
							}
						}while(kFalse);
						if(startPos > 0)
							lineStartCharIndex = startIterator.Position() + 1;
						else
							lineStartCharIndex = 0;

                        //Find the ending character's index of the line
						TextIterator endIterator(textModel,endPos);
						do
						{   
							if(endIterator.IsNull())
							{	
								//ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingItemItemWithOtherCopyAttributes::endIterator.IsNull");
								break;
							}
						
							while(kTrue)
							{
								(*endIterator).ToUTF16(utfChar,&len);
								if((utfChar[0] == kTextChar_CR) || (endIterator.IsNull()))
									break;
								endIterator++;
							}
						}while(kFalse);

						lineEndCharIndex = endIterator.Position();
						PMString colNo = tagInfo.tagPtr->GetAttributeValue(WideString("colno"));
						UIDRef textModelUIDRef =::GetUIDRef(textModel);
						PMString tableTagName = "CustomTabbedText";
							
						startPos = lineStartCharIndex;
						endPos = lineEndCharIndex;
						ErrorCode err = kFailure;
						XMLReference CreatedElement;
						const XMLReference &  parentXMLR = kInvalidXMLReference; 
						err = Utils<IXMLElementCommands>()->CreateElement(WideString(tableTagName),textModelUIDRef,startPos,
																			endPos,parentXMLR,& CreatedElement);

						PMString attribName("ID");
						PMString attribVal("-101");
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
						
						attribName.Clear();
						attribVal.Clear();
						attribName = "typeId";
						attribVal = "-5";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName = "header";
						attribVal.AppendNumber(tagInfo.header);
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName = "isEventField";
						attribVal = "-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName = "deleteIfEmpty";
						attribVal = "-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName = "dataType";
						attribVal = "-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
						
						attribName.Clear();
						attribVal.Clear();
						attribName="isAutoResize";
						attribVal="";
						attribVal.AppendNumber(PMReal(tagInfo.isAutoResize));
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="LanguageID";
						attribVal="";
						attribVal.AppendNumber(PMReal(tagInfo.languageID));
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName = "index";
						attribVal = "3";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName = "pbObjectId";
						attribVal.AppendNumber(PMReal(pNode.getPBObjectID()));
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
						
						attribName.Clear();
						attribVal.Clear();
						attribName="parentID";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName = "childId";
						attribVal = "-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="sectionID";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="parentTypeID";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="isSprayItemPerFrame";
						attribVal.AppendNumber(tagInfo.isSprayItemPerFrame);
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="catLevel";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));						
						
						attribName.Clear();
						attribVal.Clear();
						attribName="imgFlag";
						attribVal="0";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
						
						attribName.Clear();
						attribVal.Clear();
						attribName="imageIndex";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
						
						attribName.Clear();
						attribVal.Clear();
						attribName="flowDir";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
						
						attribName.Clear();
						attribVal.Clear();
						attribName="childTag";
						attribVal="1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
						
						attribName.Clear();
						attribVal.Clear();
						attribName="tableFlag";
						attribVal="0";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
						
						attribName.Clear();
						attribVal.Clear();
						attribName="tableType";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
						
						attribName.Clear();
						attribVal.Clear();
						attribName="tableId";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
						
						attribName.Clear();
						attribVal.Clear();
						attribName="rowno";
						attribVal="-1";

						//if(isComponentTableAttributePresent == 0)
							
						/*else if(isComponentTableAttributePresent == 1)
							attribVal.AppendNumber(-901);
						else if(isComponentTableAttributePresent == 2)
							attribVal.AppendNumber(-902);
						else if(isComponentTableAttributePresent == 3)
							attribVal.AppendNumber(-903);*/

						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
						
						attribName.Clear();
						attribVal.Clear();
						attribName="colno";							
						attribVal = colNo;
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
						
						attribName.Clear();
						attribVal.Clear();
						attribName="field1";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="field2";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="field3";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="field4";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="field5";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                        
                        attribName.Clear();
                        attribVal.Clear();
                        attribName="groupKey";
                        attribVal="";
                        err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
					}
				}while(kFalse);
				ptrIAppFramework->LogInfo("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::tableInfo->size()==0");	
				ItemAbsentinProductFlag = kTrue;
				delete tableInfo;
				break;
			}
			CItemTableValue oTableValue;
			VectorScreenTableInfoValue::iterator it;

			bool16 typeidFound=kFalse;
			vector<double> vec_items;
			FinalItemIds.clear();
			vecTableID.clear();
			vecTableTypeID.clear();

			CItemTableValue oTableSourceValue;
			VectorScreenTableInfoValue::iterator itr;

			if(firstTagInfo.typeId == -57)
			{
				//CA("PV");

				InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
				if(ptrTableSourceHelper != nil)
				{
					tableSourceInfoValueObj=ptrTableSourceHelper->getTableSourceInfoValueObj();
					if(tableSourceInfoValueObj)
					{
						isCallFromTS = tableSourceInfoValueObj->getIsCallFromTABLEsource();
						if(isCallFromTS)	
						{
							tableInfo=tableSourceInfoValueObj->getVectorScreenTableInfoPtr();
							if(!tableInfo)
								return;
							
						}
					}
				}
				
				if(isCallFromTS == kFalse)
				{
					for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
					{
						oTableValue = *it;
						break;
					}
					
					vector<double> vectorOfTableHeader = oTableValue.getTableHeader();
					int32 noOfColumns =static_cast<int32>(vectorOfTableHeader.size());

					vec_items = oTableValue.getItemIds();
					int32 row = (int32)vec_items.size();

					for(int32 j = 0; j < row; j++)
					{
						for(int32 i = 0; i < noOfColumns; i++)
						{
							FinalItemIds.push_back(vectorOfTableHeader[i]);
						}
					}
				}
				else
				{
					//CA("Table source");
					vector<double>::iterator itrID;
					vector<double>selectedTblTypID = tableSourceInfoValueObj->getVec_TableType_ID();
					vector<double>selectedTblID = tableSourceInfoValueObj->getVec_Table_ID();
					
					bool16 isSelectedList = kFalse;
					for(int32 x=0 ; x< selectedTblID.size() ; x++)
					{
						
						for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
						{
							oTableValue = *it;

							if((selectedTblID.at(x) == oTableValue.getTableID()) && (selectedTblTypID.at(x) == oTableValue.getTableTypeID()) )
							{
								isSelectedList = kTrue;
								break;
							}
						}
						if(isSelectedList == kTrue)
						{
							isSelectedList = kFalse;
							break;
						}

					}

					vector<double> vectorOfTableHeader = oTableValue.getTableHeader();
					int32 noOfColumns =static_cast<int32>(vectorOfTableHeader.size());

					vec_items = oTableValue.getItemIds();
					int32 row = (int32)vec_items.size();

					for(int32 j = 0; j < row; j++)
					{
						for(int32 i = 0; i < noOfColumns; i++)
						{
							FinalItemIds.push_back(vectorOfTableHeader[i]);
						}
					}

				}

			}
			else
			{
			
				InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
				if(ptrTableSourceHelper != nil)
				{
					tableSourceInfoValueObj=ptrTableSourceHelper->getTableSourceInfoValueObj();
					if(tableSourceInfoValueObj)
					{
						isCallFromTS = tableSourceInfoValueObj->getIsCallFromTABLEsource();
						if(isCallFromTS)	
						{
							tableInfo=tableSourceInfoValueObj->getVectorScreenTableInfoPtr();
							if(!tableInfo)
								return;
							
						}
					}
				}
				
				if(isCallFromTS == kFalse)	//-----Condition for ContentSprayer---
				{
					//CA("ContentSprayer");
					for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
					{//for tabelInfo start				
						oTableValue = *it;				
						vec_items = oTableValue.getItemIds();
					
						if(field_1 == -1)	//--------
						{
							if(FinalItemIds.size() == 0)
							{
								FinalItemIds = vec_items;
							}
							else
							{
								for(int32 i=0; i<vec_items.size(); i++)
								{	
									bool16 Flag = kFalse;
									for(int32 j=0; j<FinalItemIds.size(); j++)
									{
										if(vec_items[i] == FinalItemIds[j])
										{
											Flag = kTrue;
											break;
										}				
									}
									if(!Flag )
									{
										FinalItemIds.push_back(vec_items[i]);
									}
								}
							}
						}
						else
						{
							if(field_1 != oTableValue.getTableTypeID())
								continue;
						
							if(FinalItemIds.size() == 0)
							{
								FinalItemIds = vec_items;
							}
							else
							{
								for(int32 i=0; i<vec_items.size(); i++)
								{	
									bool16 Flag = kFalse;
									for(int32 j=0; j<FinalItemIds.size(); j++)
									{
										if(vec_items[i] == FinalItemIds[j])
										{
											Flag = kTrue;
											break;
										}				
									}
									if(!Flag )
									{
										FinalItemIds.push_back(vec_items[i]);
									}
								}
							}
						}
					}//for tabelInfo end
				}
				else		//-----Condition For TableSource------
				{
					//CA("TableSource");
					vector<double>::iterator itrID;
					vector<double>selectedTblTypID = tableSourceInfoValueObj->getVec_TableType_ID();
					vector<double>selectedTblID = tableSourceInfoValueObj->getVec_Table_ID();
					
					for(itr = tableInfo->begin(); itr!=tableInfo->end(); itr++)
					{
						oTableSourceValue = *itr;				
						vec_items = oTableSourceValue.getItemIds();
						double table_ID = oTableSourceValue.getTableID();
						double table_Type_ID = oTableSourceValue.getTableTypeID();
						int32 check=0;
						
						for(itrID=selectedTblID.begin(); itrID !=selectedTblID.end();itrID++,++check/*check++*/)
						{
							if(selectedTblTypID.at(check) != table_Type_ID || selectedTblID.at(check) != table_ID)
							{
								continue;
							}
								
							if(firstTagInfo.typeId == -3 && firstTagInfo.elementId == -121 && firstTagInfo.dataType ==4 )
							{
								//CA("tabbbbbbbbb");
								FinalItemIds.push_back(vec_items[check]);
								vecTableID.push_back(table_ID);
								vecTableTypeID.push_back(table_Type_ID);
								if((check+1) == selectedTblTypID.size())
								{
									//CA("check == selectedTblTypID.size()");
									break;
								}
							}
							else if( (firstTagInfo.typeId == -55) || (firstTagInfo.typeId ==-56))
							{
								//CA("DEsccc");
								FinalItemIds.push_back(vec_items[check]);
								vecTableID.push_back(table_ID);
								vecTableTypeID.push_back(table_Type_ID);

								if((check+1) == selectedTblTypID.size())
								{
									//CA("check == selectedTblTypID.size()");
									break;
								}
							}
							else 
							{

								if(FinalItemIds.size() == 0)
								{
									FinalItemIds = vec_items;
									for(int32 index = 0 ; index < FinalItemIds.size() ; index++)
									{
										vecTableID.push_back(table_ID);
										vecTableTypeID.push_back(table_Type_ID);
									}
								}
								else
								{
									for(int32 i=0; i<vec_items.size(); i++)
									{
										bool16 Flag = kFalse;
										for(int32 j=0; j<FinalItemIds.size(); j++)
										{
											if(vec_items[i] == FinalItemIds[j])
											{
												Flag = kTrue;
												break;
											}				
										}
										if(!Flag )
										{
											FinalItemIds.push_back(vec_items[i]);
											vecTableID.push_back(table_ID);
											vecTableTypeID.push_back(table_Type_ID);
										}
										
									}
								}
							}
						}
						
					}

				} 
				if(!isCallFromTS)
				{
					if(tableInfo)
					{
						tableInfo->clear();
						delete tableInfo;
					}
				}
			}

		}while(kFalse);
		//If the product doesn't have any Items.
		//return from the function.

		if(ItemAbsentinProductFlag == kTrue)
		{
			for(int32 tagIndex=0;tagIndex<numTags;tagIndex++)
			{
				TagStruct & firstTagInfo = tList[tagIndex];
				XMLContentReference xmlCntnRef = firstTagInfo.tagPtr->GetContentReference();
				if(xmlCntnRef.IsTable())
				{
					//CA("This is the tag attached to the table");
					continue;
				}

				if(firstTagInfo.whichTab == 4 //ItemTag present
					&& (firstTagInfo.typeId == -1 || firstTagInfo.header == 1) 
					//ensure that this is not a ItemTable Tag
					&& (firstTagInfo.isTablePresent != 1)					
				)
				{						
						makeTheTagImpotent(firstTagInfo.tagPtr);
				}
				
			}

		}

	}

	if(FinalItemIds.size()<=0)
	{			
		ptrIAppFramework->LogInfo("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::FinalItemIds.size()<=0");
		if(tList.size() > 0)
		{
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
		}
		return;
	}

	bool16 firstTabbedTextTag = kTrue;
	tabbedTagStructList.clear();
	
	TextIndex startPos=-1;
	TextIndex endPos = -1;
	
	txtModelUIDRef = ::GetUIDRef(textModel);

	int32 tagIndex = 0;
	int32 tagIndexOfNewlyAddedTagDueToLinePaste = 0;
	int32 tagListSize =static_cast<int32>  (tList.size());

	int32 firstCharacterIndexOfTheline = 0;
	TagStruct tagInfo;	

	TextIterator startIterator(textModel,firstCharacterIndexOfTheline);
	if(startIterator.IsNull())
	{	
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::startIterator.IsNull");	
		return;
	}
	bool16 breakTheOuterLoop = kFalse;
	bool16 areItemIDsCollected = kFalse;

	int32 lineStartCharIndex = 0;
	int32 lineEndCharIndex = -1;

	int32 firstTagIndex =0;
	int32 lastTagIndex = 0;
	int32 updatedTagIndex = 0;

	for(int32 itemTagIndex = 0;itemTagIndex < vectorItemTagIndex.size();itemTagIndex++)
	{
		//CA("Inside vectorItemTagIndex.size()");
		//Get the actual index of the itemtag inside the taglist of the box. 
		//now find the startIndex and endIndex of the line in which the item tag is present.

		ErrorCode status;

		int32 index = vectorItemTagIndex[itemTagIndex];
        bool16 headerPresentInTheRow = kFalse;

		lastTagIndex = index;

		tagInfo=tList[index];
		PMString isHeader = tagInfo.tagPtr->GetAttributeValue(WideString("header"));
		if(isHeader == "1")
		{
			//CA("header present in the row");
			headerPresentInTheRow = kTrue;
		}
		
		int32 startPos=-1;
		int32 endPos = -1;
		Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&startPos,&endPos);

		if(startPos <= lineEndCharIndex)
			continue;

		int32 len=2;
		UTF16TextChar utfChar[2]={kTextChar_Null,kTextChar_Null};
		//Find the starting character's index of the line
		TextIterator startIterator(textModel,startPos); 			
		do
		{
			if(startIterator.IsNull())
			{	
				//ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::startIterator.IsNull");	
				break;
			}
			
			if(startPos > 0)
			{
				while(kTrue)
				{
					(*startIterator).ToUTF16(utfChar,&len);
					if((utfChar[0] == kTextChar_CR) || (startIterator.IsNull()) || startPos == 0)
					break;
					
					startIterator--;
				}
			}
		}while(kFalse);
		if(startPos > 0)
			lineStartCharIndex = startIterator.Position() + 1;
		else
			lineStartCharIndex = 0;
		
        //Find the ending character's index of the line
		TextIterator endIterator(textModel,endPos);
		TextIterator endIterator1(textModel,endPos);
		do
		{
			if(endIterator.IsNull())
			{	
				//ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::endIterator.IsNull");	
				break;
			}
			int32 temp=0;
			while(kTrue)
			{
				if((endIterator1.IsNull() == kTrue))
				{
					break;
				}
				(*endIterator1).ToUTF16(utfChar,&len);
				if((utfChar[0] == kTextChar_CR))
				{
					temp++;								
				}	
				endIterator1++;
			}

			
			while(kTrue)
			{
				if((endIterator.IsNull() == kTrue))
				{
					break;
				}
				(*endIterator).ToUTF16(utfChar,&len);
				if((utfChar[0] == kTextChar_CR) || (endIterator.IsNull()))
				{
					if((utfChar[0] == kTextChar_CR) )
					{
						if(temp > 1)
						{
							endIterator++;								
						}
						else
						{
							WideString* enterChar;
							if(firstTagInfo.typeId == -57)
							{
								enterChar= new WideString("\n");
							}
							else
							{
								enterChar= new WideString("\r");
							}
							textModel->Insert(endIterator.Position(),enterChar);

							CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);

							endIterator++;
							delete enterChar;
						}
						
					}
					break;
				}
				endIterator++;
			}

			
		}while(kFalse);

		CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
		lineEndCharIndex = endIterator.Position();

		InterfacePtr<ISelectionManager> selectionManager(Utils<ISelectionUtils>()->QueryActiveSelection ());
		if (selectionManager == nil)
		{
			//CA("selectionManager == nil");
			break;
		}

		// Deselect everything.
		if (selectionManager->SelectionExists (kInvalidClass/*any CSB*/, ISelectionManager::kAnySelection)) {
			// Clear the selection
			selectionManager->DeselectAll(nil);
		}

		int32 numberOfTagsInALine = 0;
		int32 numberOfTimesTheLineToBeCopied = 0;

		//Find all the tags which are in the current line.
		for(int32 normalTagIndex = 0;normalTagIndex < numTags;normalTagIndex++)
		{
			TagStruct & tagInfo1 = tList[normalTagIndex];
			int32 startPos=-1;
			int32 endPos = -1;
			Utils<IXMLUtils>()->GetElementIndices(tagInfo1.tagPtr,&startPos,&endPos);
			XMLReference tagInfo1XmlRef = tagInfo1.tagPtr->GetXMLReference();

			if(startPos >= lineStartCharIndex && endPos <= lineEndCharIndex)
			{
				CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
				numberOfTagsInALine++;
				if(headerPresentInTheRow)
				{
					numberOfTimesTheLineToBeCopied = static_cast<int32>(FinalItemIds.size());
					PMString attrIndex("index");
					PMString indexValue  = tagInfo1.tagPtr->GetAttributeValue(WideString(attrIndex));
			        PMString strTableFlag = tagInfo1.tagPtr->GetAttributeValue(WideString("tableFlag"));
					if(strTableFlag == "-15")
					{
						continue;
					}
					else if( indexValue == "4" && strTableFlag != "1")
					{																
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XmlRef,  WideString("header"),WideString("1"));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XmlRef,  WideString("tableFlag"),WideString("-12"));
					}
					else if(indexValue == "3" && strTableFlag != "1") 
					{
						//Header For nonItemCopyAttributes
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XmlRef,  WideString("tableFlag"),WideString("-13"));
					}
					else if(indexValue == "3" && strTableFlag == "1")
					{
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XmlRef,  WideString("ID"),WideString("-103"));								
					}
				}
				else
				{
					numberOfTimesTheLineToBeCopied =static_cast<int32>( FinalItemIds.size() - 1);
					PMString attrIndex("index");
					PMString indexValue  = tagInfo1.tagPtr->GetAttributeValue(WideString(attrIndex));
					PMString strTableFlag = tagInfo1.tagPtr->GetAttributeValue(WideString("tableFlag"));
					PMString strID = tagInfo1.tagPtr->GetAttributeValue(WideString("ID"));

					if(strTableFlag == "-15")
					{
						continue;
					}

					else if(indexValue == "4" && strTableFlag != "1" /*&& strID == "-701"*/)
					{
																												
						PMString attributeValue;
						attributeValue.AppendNumber(PMReal(FinalItemIds[0]));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XmlRef,  WideString("childId"),WideString(attributeValue));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XmlRef,  WideString("childTag"),WideString("1"));
						//change  the tableFlag to -12 for item_copy_attributes in text frame.
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XmlRef,  WideString("tableFlag"),WideString("-12"));
						if(vecTableID.size() > 0 && vecTableTypeID.size() > 0)
						{
							attributeValue.Clear();
							attributeValue.AppendNumber(PMReal(vecTableID[0]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XmlRef,  WideString("tableId"),WideString(attributeValue));
							attributeValue.Clear();
							attributeValue.AppendNumber(PMReal(vecTableTypeID[0]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XmlRef,  WideString("typeId"),WideString(attributeValue));
						}
						if(tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-803"))
						{
							attributeValue.Clear();										
							attributeValue.Append("a");												
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XmlRef,  WideString("rowno"),WideString(attributeValue));
						}
						if(tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-827")) //**** Number Keys
						{
							attributeValue.Clear();										
							attributeValue.Append("1");												
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XmlRef,  WideString("rowno"),WideString(attributeValue));
						}
					}								
				}
				PMString pbObjectIdStr("");
				pbObjectIdStr.AppendNumber(PMReal(pNode.getPBObjectID()));
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XmlRef, WideString("pbObjectId"),WideString(pbObjectIdStr));
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XmlRef,  WideString("tableType"),WideString("2"));
			}

		}

		
		int32 textSelectionStartAt = lineEndCharIndex /*+1*/;
		int32 length = lineEndCharIndex -lineStartCharIndex + 1;
		int32 startPasteIndex = lineEndCharIndex + 1;

		if(lineEndCharIndex > lineStartCharIndex)
		{
			boost::shared_ptr< PasteData > pasteData;
			startPasteIndex--;
			TextIndex destEnd = startPasteIndex;

			for(int32 lineIndex=0; lineIndex<numberOfTimesTheLineToBeCopied;lineIndex++)
			{
				do
				{
					// Create kCopyStoryRangeCmdBoss.
					InterfacePtr<ICommand> copyStoryRangeCmd(CmdUtils::CreateCommand(kCopyStoryRangeCmdBoss));
					if (copyStoryRangeCmd == nil) {//CA("copyStoryRangeCmd == nil");
						break;
					}

					// Refer the command to the source story and range to be copied.
					InterfacePtr<IUIDData> sourceUIDData(copyStoryRangeCmd, UseDefaultIID());
					if (sourceUIDData == nil) {//CA("sourceUIDData == nil");
						break;
					}
					
					sourceUIDData->Set(txtModelUIDRef);
					InterfacePtr<IRangeData> sourceRangeData(copyStoryRangeCmd, UseDefaultIID());
					if (sourceRangeData == nil) {//CA("sourceRangeData == nil");
						break;
					}
					
					sourceRangeData->Set(lineStartCharIndex, lineEndCharIndex);

					// Refer the command to the destination story and the range to be replaced.
					UIDList itemList(txtModelUIDRef);
					copyStoryRangeCmd->SetItemList(itemList);
					
					InterfacePtr<IRangeData> destRangeData(copyStoryRangeCmd, IID_IRANGEDATA2);

					destRangeData->Set(startPasteIndex, destEnd );

					// Process CopyStoryRangeCmd
					status = CmdUtils::ProcessCommand(copyStoryRangeCmd);

				}while(kFalse);

				startPasteIndex = startPasteIndex + length -1;
				destEnd = startPasteIndex;

			}

			// Deselect everything.
			if (selectionManager->SelectionExists (kInvalidClass/*any CSB*/, ISelectionManager::kAnySelection)) {
				// Clear the selection
				selectionManager->DeselectAll(nil);
			}

			int32 textSelectionEndAt = startPasteIndex-1;
			TagList newTagList = itagReader->getTagsFromBox(boxUIDRef);
			int32 pastedTagCount = 0;
			for(int32 newTagIndex = 0;newTagIndex < newTagList.size();newTagIndex++)
			{
				TagStruct & newTagInfo = newTagList[newTagIndex];
				XMLReference newTagInfoXmlRef = newTagInfo.tagPtr->GetXMLReference();

				int32 newStartPos = -1;
				int32 newEndPos = -1;
				Utils<IXMLUtils>()->GetElementIndices(newTagInfo.tagPtr,&newStartPos,&newEndPos);
				if((newStartPos >= textSelectionStartAt) && (newEndPos <= textSelectionEndAt))
				{			
					PMString attrIndex("index");
					PMString indexValue  = newTagInfo.tagPtr->GetAttributeValue(WideString(attrIndex));
					PMString strTableFlag = newTagInfo.tagPtr->GetAttributeValue(WideString("tableFlag"));
					PMString strID = newTagInfo.tagPtr->GetAttributeValue(WideString("ID"));
					double itemID =-1;
					int32 itemIdSeqNo = 0;
					
					if(headerPresentInTheRow)
					{
						itemID = FinalItemIds[pastedTagCount / numberOfTagsInALine];
						itemIdSeqNo = (pastedTagCount / numberOfTagsInALine);
					}
					else
					{
						itemID = FinalItemIds[pastedTagCount / numberOfTagsInALine + 1];
						itemIdSeqNo = ((pastedTagCount / numberOfTagsInALine) +1 );
					}
					PMString attributeValue;
					attributeValue.AppendNumber(PMReal(itemID));
					if(strTableFlag == "-15")
					{
						continue;
					}
					else if(indexValue == "4" && strTableFlag != "1")
					{

						Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef,   WideString("childId"),WideString(attributeValue));
						//change  the tableFlag to -12 for item_copy_attributes in text frame.
						Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef,   WideString("tableFlag"),WideString("-12"));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef,   WideString("childTag"),WideString("1"));
						if(vecTableID.size() > 0 && vecTableTypeID.size() > 0)
						{
							PMString tableIDStr(""),tableTypeIDStr("");
							tableIDStr.AppendNumber(PMReal(vecTableID[itemIdSeqNo]));
							tableTypeIDStr.AppendNumber(PMReal(vecTableTypeID[itemIdSeqNo]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef,   WideString("typeId"),WideString(tableTypeIDStr));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef,   WideString("tableId"),WideString(tableIDStr));
						}
						if(newTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-803"))
						{
							attributeValue.Clear();
                            attributeValue = getAlphabetValue(itemIdSeqNo);
                            
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef,   WideString("rowno"),WideString(attributeValue));
						}

						if (newTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-827")) //**** Number Keys
						{
							PMString attributeValue("");
							attributeValue.AppendNumber(itemIdSeqNo/*+1*/);
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef,   WideString("rowno"),WideString(attributeValue));	
						}
						/*
								do
								{
										int32 eventPriceTableTypeID = ptrIAppFramework->getPubItemPriceTableType();
										
										VectorScreenTableInfoPtr tableInfo = NULL;
										if(pNode.getIsONEsource())
										{
											// For ONEsource
											CA("IsONEsource");
											tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(objectId);
										}else
										tableInfo =ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, pNode.getPubId());
										if(!tableInfo)
										{
											break;
										}
										if(tableInfo->size()==0)
										{ 
											break;
										}
										bool8 isEventPriceTableIDPresent =kFalse;
										CItemTableValue oTableValue;
										VectorScreenTableInfoValue::iterator it;
										for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
										{	
											if(it->getTableTypeID() == eventPriceTableTypeID)
											{
												oTableValue = *it;
												isEventPriceTableIDPresent = kTrue;
												break;
											}
											
										}
										if(isEventPriceTableIDPresent == kFalse)
											break;

										vector <int32> vec_ItemIDs = oTableValue.getItemIds();
										vector <PMString> vec_notes =  oTableValue.getNotesList();
										vector <int32> :: iterator itemIterator;
										vector <PMString> ::iterator notesIterator =vec_notes.begin(); 
										for(itemIterator == vec_ItemIDs.begin();itemIterator != vec_ItemIDs.end();itemIterator++,notesIterator++)
										{
											if(itemID == *itemIterator)
											{
												//attach the notes to the ID..i.e it will be the attributeID
												newXMLTagPtr->SetAttributeValue("ID",*notesIterator);
												//attach the itemID to typeID as usual.
												newXMLTagPtr->SetAttributeValue("typeId",attributeValue);
												//change  the tableFlag to -12 for item_copy_attributes in text frame.
												newXMLTagPtr->SetAttributeValue("tableFlag","-12");
											}
										}
								}while(kFalse);
								*/			
					}
					else if(indexValue == "3" && strTableFlag != "1")
					{
						Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef,   WideString("tableFlag"),WideString("0"));
					}

					else if(indexValue == "3" && strTableFlag == "1")
					{
						Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef,   WideString("ID"),WideString("-1"));
					}
					Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef,   WideString("header"),WideString("-1"));
					pastedTagCount++;
				
				}
			}
		
			CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
			UIDRef textModelUIDRef =::GetUIDRef(textModel);

			PMString CustomTabbedTextTagName = "CustomTabbedText";
			
			ErrorCode err = kFailure;
			XMLReference CreatedElement;
			const XMLReference &  parentXMLR = kInvalidXMLReference; 

			err = Utils<IXMLElementCommands>()->CreateElement(WideString(CustomTabbedTextTagName),textModelUIDRef,
															lineStartCharIndex, startPasteIndex -1,
															parentXMLR,& CreatedElement);
			

			PMString attribName("ID");
			PMString attribVal("-101");
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName = "typeId";
			attribVal = "-5";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName = "header";
			attribVal.AppendNumber(newTagList[0].header);
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName = "isEventField";
			attribVal  = "-1";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName = "deleteIfEmpty";
			attribVal  = "-1";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();	
			attribName = "dataType";
			attribVal  = "-1";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();	
			attribName="isAutoResize";
			attribVal="";
			attribVal.AppendNumber(newTagList[0].isAutoResize);
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName="LanguageID";
			attribVal="";
			attribVal.AppendNumber(PMReal(newTagList[0].languageID));
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName = "index";
			attribVal = "3";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName = "pbObjectId";
			attribVal.AppendNumber(PMReal(pNode.getPBObjectID()));
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName="parentID";
			attribVal="-1";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName="childId";
			attribVal="-1";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName="sectionID";
			attribVal="-1";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName="parentTypeID";
			attribVal="-1";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName="isSprayItemPerFrame";
			attribVal="-1";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName="catLevel";
			attribVal="-1";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName="imgFlag";
			attribVal="0";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName="imageIndex";
			attribVal="-1";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName="flowDir";
			attribVal="-1";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName="childTag";
			attribVal="1";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName="tableFlag";
			attribVal="0";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName="tableType";
			attribVal= "2";//"-1";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName="tableId";
			attribVal="-1";
			if(isCallFromTS == kFalse){
				//CA("isCallFromTS == kFalse");
				attribVal="-1";
			}
			else{
				//CA("isCallFromTS == kTrue");
				if(tableSourceInfoValueObj){
					if(static_cast<int32>(tableSourceInfoValueObj->getVec_Table_ID().size()) == 1){
						attribVal.Clear();
						attribVal.AppendNumber(PMReal(tableSourceInfoValueObj->getVec_Table_ID().at(0)));
					}
				}
			}

			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();			
			attribName="rowno";
			attribVal="-1";

			//if(isComponentTableAttributePresent == 0)
			//	attribVal.AppendNumber(pNode.getPBObjectID());
			//else if(isComponentTableAttributePresent == 1)
			//	attribVal.AppendNumber(-901);
			//else if(isComponentTableAttributePresent == 2)
			//	attribVal.AppendNumber(-902);
			//else if(isComponentTableAttributePresent == 3)
			//	attribVal.AppendNumber(-903);

			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName="colno";
			//attribVal="-101";
			attribVal.AppendNumber(PMReal(tagInfo.colno));
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			attribName.Clear();
			attribVal.Clear();
			attribName="field1";
			attribVal.AppendNumber(PMReal(field_1));// = "-1";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

			attribName.Clear();
			attribVal.Clear();
			attribName="field2";
			attribVal = "-1";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

			attribName.Clear();
			attribVal.Clear();
			attribName="field3";
			attribVal = "-1";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

			attribName.Clear();
			attribVal.Clear();
			attribName="field4";
			attribVal = "-1";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

			attribName.Clear();
			attribVal.Clear();
			attribName="field5";
			attribVal = "-1";
			err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
            
            attribName.Clear();
            attribVal.Clear();
            attribName="groupKey";
            attribVal = "";
            err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
			for(int32 tagIndex = 0 ; tagIndex < newTagList.size() ; tagIndex++)
			{
				newTagList[tagIndex].tagPtr->Release();
			}			
		}
		firstTagIndex = index;

	}
	
	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}
}


void CDataSprayer::sprayForThisBox(UIDRef boxUIDRef, TagList& tList1 )
	{
	//CA("CDataSprayer::sprayForThisBox");
	int32 CopyFlag= 0;
	int numReplications=static_cast<int>(idList.size()-4);//These many times we have to replicate the text for the ITEMs text
	pv_count = 0;

	InterfacePtr<ITextModel> textModel;
	TagList duplicateTagList;
	isInlineImage = kFalse;
	
	deleteCount =0;
	deleteTextRanges.clear();
    isBoxdDeleted = kFalse;


	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return;
	}

	UID textFrameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());	
	do
	{		
		if (graphicFrameDataOne) 
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		if (textFrameUID == kInvalidUID)			
			break;

        InterfacePtr<ITextModel>  textModel1;
        getTextModelByBoxUIDRef(boxUIDRef , textModel1);
		if (textModel1 == NULL)
		{
			ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::!textModel");	
			break;
		}
		textModel = textModel1;

		IDataBase* db = ::GetDataBase(textModel1);
		OwnedItemDataList ownedList;
		Utils<ITextUtils>()->CollectOwnedItems(textModel1,
											0,
											textModel1->GetPrimaryStoryThreadSpan(), 
											&ownedList,
											kTrue);		
		isInlineImage = kFalse;
		for (int32 i = 0; i < ownedList.size(); i++)
		{
			if ((ownedList[i].fClassID == kInlineBoss))
			{
				isInlineImage = kTrue;	
				gtextModel = textModel1;//*******
				break;
			}
		}	
	}while(kFalse);

	if(pNode.getIsProduct() == 1)
	{
		arrangeForSprayingProductItemWithOtherCopyAttributes(boxUIDRef,textModel);
	}
	if(pNode.getIsProduct() == 0)
	{
        arrangeForSprayingItemAttributeGroup(boxUIDRef,textModel);
		arrangeForSprayingItemItemWithOtherCopyAttributes(boxUIDRef,textModel);
        
	}

	sprayMPVAttributes(boxUIDRef,textModel);

	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{ 
		ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::!itagReader");	
		return;
	}
	TagList tList=itagReader->getTagsFromBox(boxUIDRef);
	duplicateTagList = itagReader->getTagsFromBox(boxUIDRef);

	TagStruct tagInfo;
	int numTags=static_cast<int>(tList.size());
	tagInfo = tList[0];


	if(numTags<0)
	{
		ptrIAppFramework->LogInfo("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::numTags<0");	
		return;
	}

	bool16 IsFirstElement = kTrue;

	attributeId = -1;
	delStart = -1;
	delEnd =-1;
	shouldDelete = kFalse;

	PMString CutomerAssetTypes("");
	PMString DivisionAssetTypes("");
	if(ptrIAppFramework->CONFIGCACHE_getShowCustomerInIndesign()) 
	{
		CutomerAssetTypes = ptrIAppFramework->CONFIGCACHE_getCustomerAssetTypes(1);
		if(CutomerAssetTypes.Compare(kFalse,"")==0)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::!CutomerAssetTypesPtr");
		}

		DivisionAssetTypes = ptrIAppFramework->CONFIGCACHE_getCustomerAssetTypes(0);
		if(DivisionAssetTypes.Compare(kFalse,"")==0)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::!DivisionAssetTypes");
		}
	}


	//Spray for all  tags other than tabbedTextTag
	for(int i=0; i<numTags; i++)
	{
		tagInfo=tList[i];
		int32 tags = tagInfo.tagPtr->GetAttributeCount();
		XMLReference tagInfoXMLRef = tagInfo.tagPtr->GetXMLReference ();

		if((tagInfo.tagPtr->GetAttributeValue(WideString("tableFlag")) == WideString("-15")) ||tagInfo.isProcessed || tagInfo.parentId!=-1 || tagInfo.numValidFields < NUM_TAGS_FIELDS)
		{
			continue;
		}
		switch(tagInfo.imgFlag)//Differentiate between image and text boxes
		{
			case 0: //Text
			{
				if(tagInfo.elementId != attributeId)
				{
					shouldDelete = kFalse;
					attributeId = tagInfo.elementId;
					delStart = tagInfo.startIndex; 			
				}
				else
				{
					if(tagInfo.deleteIfEmpty != 1)
						shouldDelete = kFalse;				
				}

				if(tagInfo.elementId == -123)
				{
					if(tagInfo.isAutoResize == 1)
						CopyFlag=1;

					if(tagInfo.isAutoResize == 2)
						CopyFlag=2;

					PMString textToInsert("");				
					
					if((tagInfo.whichTab == 3 && pNode.getIsProduct() == 1)||(tagInfo.whichTab == 4 && pNode.getIsProduct() == 0)||(tagInfo.whichTab == 5 && pNode.getIsProduct() == 2))
					{
						double sectionid = -1;
						if(global_project_level == 3)
							sectionid = CurrentSubSectionID;
						if(global_project_level == 2)
							sectionid = CurrentSectionID;

						CAdvanceTableScreenValue oTableValue;
						do
						{
							VectorAdvanceTableScreenValuePtr tableInfo = NULL;
							if(tagInfo.whichTab == 5)
								tableInfo=ptrIAppFramework->getHybridTableData(sectionid, pNode.getPubId(),tagInfo.languageID,kTrue);
							else
								tableInfo=ptrIAppFramework->getHybridTableData(sectionid, pNode.getPubId(),tagInfo.languageID,kFalse);
							
							if(!tableInfo)
							{
								ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::!tableInfo");
								break;
							}

							if(tableInfo->size()==0)
							{
								ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::tableInfo->size()==0");
								delete tableInfo;
								break;
							}
							
							VectorAdvanceTableScreenValue::iterator it;
							it=tableInfo->begin();

							bool16 typeidFound = kFalse;
							oTableValue = *it;
							for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
							{
								oTableValue = *it;
								if(oTableValue.getTableTypeId() == tagInfo.typeId)
								{
									typeidFound=kTrue;
									break;
								}
							}
							if(tableInfo)
							{
								tableInfo->clear();
								delete tableInfo;
							}
	
							if(!typeidFound)
							{
								ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::sprayForThisBox::!typeidFound");
								break;
							}
		
							textToInsert = oTableValue.getTableName();
						}while(kFalse);

						TextIndex startPos=-1;
						TextIndex endPos = -1;

						int32 tagStartPos = 0;
						int32 tagEndPos =0; 			
						Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&tagStartPos,&tagEndPos);
						tagStartPos = tagStartPos + 1;
						tagEndPos = tagEndPos -1;

                        textToInsert.ParseForEmbeddedCharacters();
						boost::shared_ptr<WideString> insertText(new WideString(textToInsert));

						ReplaceText(textModel,tagStartPos,tagEndPos ,insertText);

						PMString attributeName;
						PMString attributeValue;
						
						attributeName = "parentID";
						attributeValue.AppendNumber(PMReal(pNode.getPubId()));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
						attributeName.Clear();
						attributeValue.Clear();
						
						attributeName = "parentTypeID";
						attributeValue.AppendNumber(PMReal(CurrentObjectTypeID));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
						attributeName.Clear();
						attributeValue.Clear();				

						attributeName = "sectionID";
						attributeValue.AppendNumber(PMReal(sectionid));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
						attributeName.Clear();
						attributeValue.Clear();	
						break;
					}				
				}
				if(tagInfo.dataType == 4 || tagInfo.dataType == 5)
				{
					if(tagInfo.isAutoResize == 1)
						CopyFlag=1;

					if(tagInfo.isAutoResize == 2)
						CopyFlag=2;

					PMString textToInsert("");				
						
					double sectionid = -1;
					if(global_project_level == 3)
						sectionid = CurrentSubSectionID;
					if(global_project_level == 2)
						sectionid = CurrentSectionID;
					
					VectorScreenTableInfoPtr itemTableInfo = NULL;
					VectorScreenTableInfoPtr tableInfo = NULL;
					VectorAdvanceTableScreenValuePtr advTableInfo = NULL;
					TableSourceInfoValue *tableSourceInfoValueObj;
					bool16 isCallFromTS = kFalse;
					InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
					if(ptrTableSourceHelper != nil)
					{
						tableSourceInfoValueObj=ptrTableSourceHelper->getTableSourceInfoValueObj();
						if(tableSourceInfoValueObj)
						{
							isCallFromTS = tableSourceInfoValueObj->getIsCallFromTABLEsource();
							if(isCallFromTS )	
							{	
								if(tagInfo.tableType == 3 || tagInfo.typeId == -58 )
								{
									advTableInfo = tableSourceInfoValueObj->getVectorAdvanceTableScreenValuePtr();
								}
								else
								{
									if(tagInfo.whichTab == 3)
										tableInfo = tableSourceInfoValueObj->getVectorScreenTableInfoPtr();
									else if(tagInfo.whichTab == 4)
										itemTableInfo = tableSourceInfoValueObj->getVectorScreenItemTableInfoPtr();
								}
							}
						}
					}
					
					if(tagInfo.whichTab == 3)
					{
						if(isCallFromTS == kFalse)	//this is for call from content sprayer
						{
							double objectId = pNode.getPubId();
							double tableID = -1;
							bool16 typeidFound=kFalse;

							if(tagInfo.tableType == 3 || tagInfo.typeId == -58)
                            {
								VectorAdvanceTableScreenValuePtr advTableInfo = ptrIAppFramework->getHybridTableData(sectionid,pNode.getPubId(),tagInfo.languageID,kTrue);
								if(!advTableInfo)
								{
									//CA("!advTableInfo");
									break;
								}
								VectorAdvanceTableScreenValue::iterator it;
								CAdvanceTableScreenValue advTableValue;
								
								for(it = advTableInfo->begin(); it!=advTableInfo->end(); it++)
								{
									advTableValue = *it;
									{
										typeidFound=kTrue;
										tableID = advTableValue.getTableId();
										textToInsert = advTableValue.getTableName();
										break;
									}
								}
                                
                                if(typeidFound == kTrue && tagInfo.tableType == 3) // For table Name
                                {
                                    textToInsert = advTableValue.getTableName();
                                
                                }
                                else if(typeidFound == kTrue && tagInfo.typeId == -58)//---"Adv. Table Note 1 to 5"
                                {
                                    if(tagInfo.elementId == -975)//---for Note_1
                                    {
                                        typeidFound=kTrue;
                                        tableID = advTableValue.getTableId();
                                        textToInsert = advTableValue.getNote1();
                                    }
                                    else if(tagInfo.elementId == -974)//---for Note_2
                                    {
                                        typeidFound=kTrue;
                                        tableID = advTableValue.getTableId();
                                        textToInsert = advTableValue.getNote2();
                                    }
                                    else if(tagInfo.elementId == -973)//---for Note_3
                                    {
                                        typeidFound=kTrue;
                                        tableID = advTableValue.getTableId();
                                        textToInsert = advTableValue.getNote3();
                                        
                                    }
                                    else if(tagInfo.elementId == -972)//---for Note_4
                                    {
                                        typeidFound=kTrue;			
                                        tableID = advTableValue.getTableId();
                                        textToInsert = advTableValue.getNote4();
                                        
                                    }
                                    else if(tagInfo.elementId == -971)//---for Note_5
                                    {
                                        typeidFound=kTrue;			
                                        tableID = advTableValue.getTableId();
                                        textToInsert = advTableValue.getNote5();
                                      
                                    }
                                }
                                
								if(advTableInfo)
								{
									advTableInfo->clear();
									delete advTableInfo;
				
								}
							}
							else
							{

								VectorScreenTableInfoPtr tableInfo = NULL;
								if(pNode.getIsONEsource())
								{
									// For ONEsource To get all Table related information when table stencils is selected on 9/10 by dattatray
									//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(objectId);
								}else
								{
									// For Publication to get all information about Table such as typeid,tablename,tableid,istranspose,printheader,tableheader etc..
									tableInfo= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, objectId , tagInfo.languageID, kTrue); // getting only customtable data

								}
								if(!tableInfo)
								{
									ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::!tableInfo");
									return;
								}

								if(tableInfo->size()==0){
									ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::tableInfo->size()==0");
									delete tableInfo;
									break;
								}

								CItemTableValue oTableValue;
								VectorScreenTableInfoValue::iterator it;
								
								for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
								{
									oTableValue = *it;
									if((oTableValue.getTableTypeID() == tagInfo.typeId))
									{   
										typeidFound=kTrue;
										textToInsert = oTableValue.getName();	
										tableID = oTableValue.getTableID();

										break;
									}
									else if(tagInfo.elementId == -121 && tagInfo.typeId == -3) //-Condition for Spray List/Table Name--
									{
										typeidFound=kTrue;		
										textToInsert = oTableValue.getName();	
										tableID = oTableValue.getTableID();
										break;
									}
									else if(tagInfo.typeId == -55) 
										{
											if(tagInfo.elementId == -983)//---List Description
											{
												typeidFound=kTrue;			
												tableID = oTableValue.getTableID();
												textToInsert = oTableValue.getDescription();	
												break;
											}
											else if(tagInfo.elementId == -982)//---Stencil Name
											{
												typeidFound=kTrue;			
												tableID = oTableValue.getTableID();
												textToInsert = oTableValue.getstencil_name();
												break;
											}

										}
									else if(tagInfo.typeId == -56)//---"Note 1 to 5"
										{
											
											if(tagInfo.elementId == -980)//---for Note_1 
											{
												typeidFound=kTrue;			
												tableID = oTableValue.getTableID();
												textToInsert = oTableValue.getNotesList().at(0);	
												break;
											}
											else if(tagInfo.elementId == -979)//---for Note_2 
											{
												typeidFound=kTrue;			
												tableID = oTableValue.getTableID();
												textToInsert = oTableValue.getNotesList().at(1);		
												break;
											}
											else if(tagInfo.elementId == -978)//---for Note_3
											{
												typeidFound=kTrue;			
												tableID = oTableValue.getTableID();
												textToInsert = oTableValue.getNotesList().at(2);		
												break;
											}
											else if(tagInfo.elementId == -977)//---for Note_4
											{
												typeidFound=kTrue;			
												tableID = oTableValue.getTableID();
												textToInsert = oTableValue.getNotesList().at(3);		
												break;
											}
											else if(tagInfo.elementId == -976)//---for Note_5 
											{
												typeidFound=kTrue;			
												tableID = oTableValue.getTableID();
												textToInsert = oTableValue.getNotesList().at(4);	
												break;
											}
										}
								}
								if(tableInfo)
								{
									tableInfo->clear();
									delete tableInfo;
									tableInfo = NULL;
								}

							}
							if(typeidFound)
							{
								PMString attributeName;
								PMString attributeValue;
								attributeName = "tableId";
								attributeValue.AppendNumber(PMReal(tableID));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));

								attributeName.clear();
								attributeValue.clear();
								attributeName = "pbObjectId";
								attributeValue.AppendNumber(PMReal(pNode.getPBObjectID()));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));

							}
							else
							{
								ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::!typeidFound");
								textToInsert = "";
							}
							
						}
						else
						{
							bool16 typeidFound=kFalse;
							double tableID = -1;
							if(tagInfo.tableType == 3 || tagInfo.typeId == -58)
							{
								CAdvanceTableScreenValue advTableValue;
								VectorAdvanceTableScreenValue::iterator it;
								for(it = advTableInfo->begin(); it!=advTableInfo->end(); it++)	
								{
									advTableValue = *it;
									if(advTableValue.getTableTypeId() == tableSourceInfoValueObj->getVec_TableType_ID().at(0) && advTableValue.getTableId() == tableSourceInfoValueObj->getVec_Table_ID().at(0) )
									{
										typeidFound=kTrue;	
										textToInsert = advTableValue.getTableName();
										tableID = advTableValue.getTableId();
										break;
									}
                                }
                                
                                if(typeidFound == kTrue && tagInfo.tableType == 3) // For table Name
                                {
                                    tableID = advTableValue.getTableId();
                                    textToInsert = advTableValue.getTableName();
                                }
                                else if(typeidFound == kTrue && tagInfo.typeId == -58)//---"Adv. Table Note 1 to 5"
                                {
                                    if(tagInfo.elementId == -975)//---for Note_1
                                    {
                                        typeidFound=kTrue;
                                        tableID = advTableValue.getTableId();
                                        textToInsert = advTableValue.getNote1();
                                    }
                                    else if(tagInfo.elementId == -974)//---for Note_2
                                    {
                                        typeidFound=kTrue;
                                        tableID = advTableValue.getTableId();
                                        textToInsert = advTableValue.getNote2();
                                    }
                                    else if(tagInfo.elementId == -973)//---for Note_3
                                    {
                                        typeidFound=kTrue;
                                        tableID = advTableValue.getTableId();
                                        textToInsert = advTableValue.getNote3();
                                        
                                    }
                                    else if(tagInfo.elementId == -972)//---for Note_4
                                    {
                                        typeidFound=kTrue;
                                        tableID = advTableValue.getTableId();
                                        textToInsert = advTableValue.getNote4();
                                        
                                    }
                                    else if(tagInfo.elementId == -971)//---for Note_5
                                    {
                                        typeidFound=kTrue;
                                        tableID = advTableValue.getTableId();
                                        textToInsert = advTableValue.getNote5();
                                        
                                    }
                                }

							}
							else
							{
                                if(!tableInfo)
								{
									ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::!tableInfo");
									return;
								}
                                
								if(tableInfo->size()==0){
									ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::tableInfo->size()==0");
									delete tableInfo;
									break;
								}
                                
								CItemTableValue oTableValue;
								VectorScreenTableInfoValue::iterator it;

								for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
								{
									oTableValue = *it;

									if(oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(0) && oTableValue.getTableID() == tableSourceInfoValueObj->getVec_Table_ID().at(0) && oTableValue.getTableTypeID() == tagInfo.typeId)
									{
										typeidFound=kTrue;
										textToInsert = oTableValue.getName();	
										tableID = oTableValue.getTableID();
										break;
									}
									else if((oTableValue.getTableID() == tableSourceInfoValueObj->getVec_Table_ID().at(0)) && (tagInfo.elementId == -121 && tagInfo.typeId == -3)) //-Condition for Spray List/Table Name--
									{
										typeidFound=kTrue;
										textToInsert = oTableValue.getName();	
										tableID = oTableValue.getTableID();
										break;
									}
									else if((oTableValue.getTableID() == tableSourceInfoValueObj->getVec_Table_ID().at(0))) //-Condition for Spray List/Table discription-
									{
										if(tagInfo.typeId == -55) 
										{
											if(tagInfo.elementId == -983)//---List Description
											{
												typeidFound=kTrue;			
												tableID = oTableValue.getTableID();
												textToInsert = oTableValue.getDescription();	
												break;
											}
											else if(tagInfo.elementId == -982)//---Stencil Name
											{
												typeidFound=kTrue;			
												tableID = oTableValue.getTableID();
												textToInsert = oTableValue.getstencil_name();
												break;
											}

										}
										else if(tagInfo.typeId == -56)//---"Note 1 to 5"
										{
											if(tagInfo.elementId == -980)//---for Note_1 
											{
												//CA("for Note_1 ");
												typeidFound=kTrue;			
												tableID = oTableValue.getTableID();
												textToInsert = oTableValue.getNotesList().at(0);	
												break;
											}
											else if(tagInfo.elementId == -979)//---for Note_2 
											{
												typeidFound=kTrue;			
												tableID = oTableValue.getTableID();
												textToInsert = oTableValue.getNotesList().at(1);		
												break;
											}
											else if(tagInfo.elementId == -978)//---for Note_3
											{
												typeidFound=kTrue;			
												tableID = oTableValue.getTableID();
												textToInsert = oTableValue.getNotesList().at(2);		
												break;
											}
											else if(tagInfo.elementId == -977)//---for Note_4
											{
												typeidFound=kTrue;			
												tableID = oTableValue.getTableID();
												textToInsert = oTableValue.getNotesList().at(3);		
												break;
											}
											else if(tagInfo.elementId == -976)//---for Note_5 
											{
												typeidFound=kTrue;			
												tableID = oTableValue.getTableID();
												textToInsert = oTableValue.getNotesList().at(4);	
												break;
											}
										}
									}
								}

							}
							if(typeidFound)
							{
								PMString attributeName;
								PMString attributeValue;
								attributeName = "tableId";
								attributeValue.AppendNumber(PMReal(tableID));
								tagInfo.tagPtr->SetAttributeValue(WideString(attributeName),WideString(attributeValue));

								attributeName.clear();
								attributeValue.clear();
								attributeName = "pbObjectId";
								attributeValue.AppendNumber(PMReal(pNode.getPBObjectID()));
								tagInfo.tagPtr->SetAttributeValue(WideString(attributeName),WideString(attributeValue));
							}
							else
							{
								ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::!typeidFound");
								textToInsert = "";
							}
						}
					}
					else if(tagInfo.whichTab == 4)
					{
						double objectId = pNode.getPBObjectID();
					
						if(isCallFromTS == kFalse)
						{
							bool16 typeidFound=kFalse;
							double tableID = -1;
							if(tagInfo.tableType == 3  || tagInfo.typeId == -58){ //
								VectorAdvanceTableScreenValuePtr advTableInfo = ptrIAppFramework->getHybridTableData(sectionid,pNode.getPubId(),tagInfo.languageID,kFalse);
								if(!advTableInfo)
								{
								//	CA("!advTableInfo");
									break;
								}
								VectorAdvanceTableScreenValue::iterator it;
								CAdvanceTableScreenValue advTableValue;
								
								for(it = advTableInfo->begin(); it!=advTableInfo->end(); it++)
								{
									advTableValue = *it;
									{
										typeidFound=kTrue;
										tableID = advTableValue.getTableId();
										textToInsert = advTableValue.getTableName();
										break;
									}
								}
                                
                                if(typeidFound == kTrue && tagInfo.tableType == 3) // For table Name
                                {
                                    textToInsert = advTableValue.getTableName();
                                    
                                }
                                else if(typeidFound == kTrue && tagInfo.typeId == -58)//---"Adv. Table Note 1 to 5"
                                {
                                    if(tagInfo.elementId == -975)//---for Note_1
                                    {
                                        typeidFound=kTrue;
                                        tableID = advTableValue.getTableId();
                                        textToInsert = advTableValue.getNote1();
                                    }
                                    else if(tagInfo.elementId == -974)//---for Note_2
                                    {
                                        typeidFound=kTrue;
                                        tableID = advTableValue.getTableId();
                                        textToInsert = advTableValue.getNote2();
                                    }
                                    else if(tagInfo.elementId == -973)//---for Note_3
                                    {
                                        typeidFound=kTrue;
                                        tableID = advTableValue.getTableId();
                                        textToInsert = advTableValue.getNote3();
                                        
                                    }
                                    else if(tagInfo.elementId == -972)//---for Note_4
                                    {
                                        typeidFound=kTrue;
                                        tableID = advTableValue.getTableId();
                                        textToInsert = advTableValue.getNote4();
                                        
                                    }
                                    else if(tagInfo.elementId == -971)//---for Note_5
                                    {
                                        typeidFound=kTrue;
                                        tableID = advTableValue.getTableId();
                                        textToInsert = advTableValue.getNote5();
                                        
                                    }
                                }
								if(advTableInfo)
								{
									advTableInfo->clear();
									delete advTableInfo;
				
								}
							}
							else
							{
								VectorScreenTableInfoPtr tableInfo = NULL; 
								/*if(pNode.getIsONEsource() == kTrue)
									tableInfo = ptrIAppFramework->GETONEsourceObjects_getItemTablesByItenId(pNode.getPubId());
								else*/
									tableInfo = ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPubId(), CurrentSectionID, tagInfo.languageID);	

								if(tableInfo == NULL)
								{
									//CA("tableinfo is NULL");
									break;
								}
								VectorScreenTableInfoValue::iterator it;
							
								CItemTableValue oTableValue;
								for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
								{
									oTableValue = *it;
									if(oTableValue.getTableTypeID() == tagInfo.typeId)
									{   				
										typeidFound=kTrue;			
										tableID = oTableValue.getTableID();
										textToInsert = oTableValue.getName();
										break;
									}
                                    else if(tagInfo.elementId == -121 && tagInfo.typeId == -3) //-Condition for Spray List/Table Name--
									{
										typeidFound=kTrue;
										textToInsert = oTableValue.getName();
										tableID = oTableValue.getTableID();
										break;
									}
									else if(tagInfo.typeId == -55)
                                    {
                                        if(tagInfo.elementId == -983)//---List Description
                                        {
                                            typeidFound=kTrue;
                                            tableID = oTableValue.getTableID();
                                            textToInsert = oTableValue.getDescription();
                                            break;
                                        }
                                        else if(tagInfo.elementId == -982)//---Stencil Name
                                        {
                                            typeidFound=kTrue;
                                            tableID = oTableValue.getTableID();
                                            textToInsert = oTableValue.getstencil_name();
                                            break;
                                        }
                                        
                                    }
									else if(tagInfo.typeId == -56)//---"Note 1 to 5"
                                    {
                                        if(tagInfo.elementId == -980)//---for Note_1
                                        {
                                            typeidFound=kTrue;
                                            tableID = oTableValue.getTableID();
                                            textToInsert = oTableValue.getNotesList().at(0);
                                            break;
                                        }
                                        else if(tagInfo.elementId == -979)//---for Note_2
                                        {
                                            typeidFound=kTrue;
                                            tableID = oTableValue.getTableID();
                                            textToInsert = oTableValue.getNotesList().at(1);		
                                            break;
                                        }
                                        else if(tagInfo.elementId == -978)//---for Note_3
                                        {
                                            typeidFound=kTrue;			
                                            tableID = oTableValue.getTableID();
                                            textToInsert = oTableValue.getNotesList().at(2);		
                                            break;
                                        }
                                        else if(tagInfo.elementId == -977)//---for Note_4
                                        {
                                            typeidFound=kTrue;			
                                            tableID = oTableValue.getTableID();
                                            textToInsert = oTableValue.getNotesList().at(3);		
                                            break;
                                        }
                                        else if(tagInfo.elementId == -976)//---for Note_5 
                                        {
                                            typeidFound=kTrue;			
                                            tableID = oTableValue.getTableID();
                                            textToInsert = oTableValue.getNotesList().at(4);	
                                            break;
                                        }
                                    }
                                    
								}
								if(tableInfo)
								{
									tableInfo->clear();
									delete tableInfo;
				
								}
							}
							if(typeidFound)
							{
								PMString attributeName;
								PMString attributeValue;
								attributeName = "tableId";
								attributeValue.AppendNumber(PMReal(tableID));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));

								attributeName.clear();
								attributeValue.clear();
								attributeName = "pbObjectId";
								attributeValue.AppendNumber(PMReal(pNode.getPBObjectID()));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));

							}
							else
							{
								//CA("typeidFound == kFalse");
								textToInsert = "";
							}
						}
						else
						{
							//CA("isCallFromTS == kTrue");
							bool16 typeidFound=kFalse;
							double tableID = -1;
                            if(tagInfo.tableType == 3 || tagInfo.typeId == -58){
								CAdvanceTableScreenValue advTableValue;
								VectorAdvanceTableScreenValue::iterator it1;
								
								for(it1 = advTableInfo->begin(); it1!=advTableInfo->end(); it1++)
								{
									advTableValue = *it1;
									if(advTableValue.getTableTypeId() == tableSourceInfoValueObj->getVec_TableType_ID().at(0) && advTableValue.getTableId() == tableSourceInfoValueObj->getVec_Table_ID().at(0) )
									{
										typeidFound=kTrue;
										textToInsert = advTableValue.getTableName();
										tableID = advTableValue.getTableId();
										break;
									}
								}
                                
                                if(typeidFound == kTrue && tagInfo.tableType == 3) // For table Name
                                {
                                    textToInsert = advTableValue.getTableName();
                                    
                                }
                                else if(typeidFound == kTrue && tagInfo.typeId == -58)//---"Adv. Table Note 1 to 5"
                                {
                                    if(tagInfo.elementId == -975)//---for Note_1
                                    {
                                        typeidFound=kTrue;
                                        tableID = advTableValue.getTableId();
                                        textToInsert = advTableValue.getNote1();
                                    }
                                    else if(tagInfo.elementId == -974)//---for Note_2
                                    {
                                        typeidFound=kTrue;
                                        tableID = advTableValue.getTableId();
                                        textToInsert = advTableValue.getNote2();
                                    }
                                    else if(tagInfo.elementId == -973)//---for Note_3
                                    {
                                        typeidFound=kTrue;
                                        tableID = advTableValue.getTableId();
                                        textToInsert = advTableValue.getNote3();
                                        
                                    }
                                    else if(tagInfo.elementId == -972)//---for Note_4
                                    {
                                        typeidFound=kTrue;
                                        tableID = advTableValue.getTableId();
                                        textToInsert = advTableValue.getNote4();
                                        
                                    }
                                    else if(tagInfo.elementId == -971)//---for Note_5
                                    {
                                        typeidFound=kTrue;
                                        tableID = advTableValue.getTableId();
                                        textToInsert = advTableValue.getNote5();
                                        
                                    }
                                }
							}
							else
							{
								CItemTableValue oTableValue;
								VectorScreenTableInfoValue::iterator it;
							
								for(it = itemTableInfo->begin(); it!=itemTableInfo->end(); it++)	
								{
									oTableValue = *it;
									if(oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(0) && oTableValue.getTableID() == tableSourceInfoValueObj->getVec_Table_ID().at(0) && oTableValue.getTableTypeID() == tagInfo.typeId)
									{
										typeidFound=kTrue;	
										textToInsert = oTableValue.getName();
										tableID = oTableValue.getTableID();
										break;
									}
                                    else if(tagInfo.elementId == -121 && tagInfo.typeId == -3) //-Condition for Spray List/Table Name--
									{
										//CA(" List/Table Name");
										typeidFound=kTrue;
										textToInsert = oTableValue.getName();
										tableID = oTableValue.getTableID();
										break;
									}
									else if(tagInfo.typeId == -55)
                                    {
                                        if(tagInfo.elementId == -983)//---List Description
                                        {
                                            typeidFound=kTrue;
                                            tableID = oTableValue.getTableID();
                                            textToInsert = oTableValue.getDescription();
                                            break;
                                        }
                                        else if(tagInfo.elementId == -982)//---Stencil Name
                                        {
                                            typeidFound=kTrue;
                                            tableID = oTableValue.getTableID();
                                            textToInsert = oTableValue.getstencil_name();
                                            break;
                                        }
                                        
                                    }
									else if(tagInfo.typeId == -56)//---"Note 1 to 5"
                                    {
                                        if(tagInfo.elementId == -980)//---for Note_1
                                        {
                                            typeidFound=kTrue;
                                            tableID = oTableValue.getTableID();
                                            textToInsert = oTableValue.getNotesList().at(0);
                                            break;
                                        }
                                        else if(tagInfo.elementId == -979)//---for Note_2
                                        {
                                            typeidFound=kTrue;
                                            tableID = oTableValue.getTableID();
                                            textToInsert = oTableValue.getNotesList().at(1);
                                            break;
                                        }
                                        else if(tagInfo.elementId == -978)//---for Note_3
                                        {
                                            typeidFound=kTrue;
                                            tableID = oTableValue.getTableID();
                                            textToInsert = oTableValue.getNotesList().at(2);
                                            break;
                                        }
                                        else if(tagInfo.elementId == -977)//---for Note_4
                                        {
                                            typeidFound=kTrue;
                                            tableID = oTableValue.getTableID();
                                            textToInsert = oTableValue.getNotesList().at(3);
                                            break;
                                        }
                                        else if(tagInfo.elementId == -976)//---for Note_5
                                        {
                                            typeidFound=kTrue;
                                            tableID = oTableValue.getTableID();
                                            textToInsert = oTableValue.getNotesList().at(4);
                                            break;
                                        }
                                    }
                                    
								}
							}
							if(typeidFound)
							{
								PMString attributeName;
								PMString attributeValue;
								attributeName = "tableId";
								attributeValue.AppendNumber(PMReal(tableID));
								tagInfo.tagPtr->SetAttributeValue(WideString(attributeName),WideString(attributeValue));

								attributeName.clear();
								attributeValue.clear();
								attributeName = "pbObjectId";
								attributeValue.AppendNumber(PMReal(pNode.getPBObjectID()));
								tagInfo.tagPtr->SetAttributeValue(WideString(attributeName),WideString(attributeValue));
							}
							else
							{
								//CA("typeidFound == kFalse");
								ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::!typeidFound");
								textToInsert = "";
							}
						}						
					}

					TextIndex startPos=-1;
					TextIndex endPos = -1;

					int32 tagStartPos = 0;
					int32 tagEndPos =0; 			
					Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&tagStartPos,&tagEndPos);
					
					tagStartPos = tagStartPos + 1;
					tagEndPos = tagEndPos -1;
					
                    textToInsert.ParseForEmbeddedCharacters();
					boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
					ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos+1 ,insertText);

					PMString attributeName;
					PMString attributeValue;
					
					attributeName = "parentID";
					if(tagInfo.whichTab == 3)
						attributeValue.AppendNumber(PMReal(pNode.getPubId()));
					if(tagInfo.whichTab == 4)
						attributeValue.AppendNumber(PMReal(pNode.getPubId()));

					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();
					
					attributeName = "parentTypeID";
					attributeValue.AppendNumber(PMReal(CurrentObjectTypeID));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();				

					attributeName = "sectionID";
					attributeValue.AppendNumber(PMReal(sectionid));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();	

					break;
				}
				
				if(tagInfo.tableType == 3 && (tagInfo.whichTab == 3 ||  tagInfo.whichTab == 4 ||  tagInfo.whichTab == 5))//for hybrid table
				{//if(tagInfo.elementId == -115 && (tagInfo.whichTab == 3 ||  tagInfo.whichTab == 4 ||  tagInfo.whichTab == 5))	

					if((tagInfo.whichTab == 3 && pNode.getIsProduct() == 1)||(tagInfo.whichTab == 4 && pNode.getIsProduct() == 0)||(tagInfo.whichTab == 5 && pNode.getIsProduct() == 2))
					{
						if(tagInfo.isAutoResize == 1)
							CopyFlag=1;
						if(tagInfo.isAutoResize == 2)
							CopyFlag=2;

						sprayHybridTableScreenPrintInTabbedTextForm(tagInfo);
					}
					break;
				}
				//Attention : New functionaliy added.
				//Table inside table cell.
				if((tagInfo.whichTab == 3 || tagInfo.whichTab == 4)&& tagInfo.tableType == 8)
				{
					sprayTableInsideTableCell(boxUIDRef,tagInfo);
					break;
				}//end table inside table cell addition.

				//start..mpvID spraying..
				//MPVAttirbutes are already spread.
				//Do nothing if block..
				if((tagInfo.whichTab == 3 || tagInfo.whichTab == 4))
				{
					if(tagInfo.colno == -201)
					{
						break;
					}
					else if(tagInfo.colno == -202)
					{
						break;
					}
					else if(tagInfo.colno == -203)
					{
						break;
					}
				 
				}
				//end.. mpvID spraying..
                
                if(tagInfo.whichTab == 4 && tagInfo.dataType == 6) // AttributeGroup Spray
                {
                    
                    if(tagInfo.isAutoResize == 1)
                        CopyFlag=1;
                    
                    if(tagInfo.isAutoResize == 2)
                        CopyFlag=2;
                    
                    PMString attributeValue1("");
                    PMString attributeName1 = "parentID";
                    attributeValue1.AppendNumber(PMReal(pNode.getPubId()));
                    Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName1),WideString(attributeValue1));
                    attributeName1.Clear();
                    attributeValue1.Clear();
                    
                    attributeName1 = "parentTypeID";
                    attributeValue1.AppendNumber(PMReal(CurrentObjectTypeID));
                    Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName1),WideString(attributeValue1));
                    
                    attributeName1.Clear();
                    attributeValue1.Clear();
                    attributeName1 = "sectionID";
                    attributeValue1.AppendNumber(PMReal(CurrentSectionID));
                    Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName1),WideString(attributeValue1));
                    
                    attributeName1.Clear();
                    attributeValue1.Clear();
                    attributeName1 = "pbObjectId";
                    attributeValue1.AppendNumber(PMReal(pNode.getPBObjectID()));
                    Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName1),WideString(attributeValue1));
                    
                    
                    int32 childCount  = tagInfo.tagPtr->GetChildCount();
                    PMString ChildCount("");
                    ChildCount.AppendNumber(childCount);
                    
                    for(int j = 0; j < childCount ;j++)
                    {
                        XMLReference childXMLReference = tagInfo.tagPtr->GetNthChild(j);
                        InterfacePtr<IIDXMLElement>childReference(childXMLReference.Instantiate());
                        
                        int32 header;
                        double elementId;
                        double languageID;
                        
                        PMString temp("");
                        temp = childReference->GetAttributeValue(WideString("ID"));
                        elementId = temp.GetAsDouble();
                        temp.Clear();
                        
                        temp = childReference->GetAttributeValue(WideString("header"));
                        header = temp.GetAsNumber();
                        temp.Clear();
                        
                        temp = childReference->GetAttributeValue(WideString("LanguageID"));
                        languageID = temp.GetAsDouble();
                        temp.Clear();
                        
                        attributeName1.Clear();
                        attributeValue1.Clear();
                        attributeName1 = "parentID";
                        attributeValue1.AppendNumber(PMReal(pNode.getPubId()));
                        Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference,   WideString(attributeName1),WideString(attributeValue1));
                        
                        attributeName1.Clear();
                        attributeValue1.clear();
                        attributeName1 = "parentTypeID";
                        attributeValue1.AppendNumber(PMReal(CurrentObjectTypeID));
                        Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference,   WideString(attributeName1),WideString(attributeValue1));
                        
                        attributeName1.Clear();
                        attributeValue1.Clear();
                        attributeName1 = "sectionID";
                        attributeValue1.AppendNumber(PMReal(CurrentSectionID));
                        Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference,   WideString(attributeName1),WideString(attributeValue1));
                        
                        attributeName1.Clear();
                        attributeValue1.Clear();
                        attributeName1 = "pbObjectId";
                        attributeValue1.AppendNumber(PMReal(pNode.getPBObjectID()));
                        Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference,   WideString(attributeName1),WideString(attributeValue1));
                        
                        PMString itemAttributeValue("");
                        
                        if(header == 1)
                        {
                            itemAttributeValue = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,languageID );
                        }
                        else
                        {
                            itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(pNode.getPubId(),elementId, languageID, CurrentSectionID, kFalse );
                        }
                        
                        int32 tagStartPos = -1;
                        int32 tagEndPos = -1;
                        Utils<IXMLUtils>()->GetElementIndices(childReference,&tagStartPos,&tagEndPos);
                        tagStartPos = tagStartPos + 1;
                        tagEndPos = tagEndPos -1;
                        
                        PMString textToInsert("");
                        textToInsert=itemAttributeValue;
                        
                        textToInsert.ParseForEmbeddedCharacters();
                        boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
                        ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos +1 ,insertText);
                        
                    }

                    break;
                }

				if((tagInfo.whichTab == 4 || tagInfo.whichTab == 3) && (tagInfo.typeId == -5 ||  tagInfo.typeId == -57 ))//for item tableTabbed text format
				{
					
					PMString attributeValue1("");
					PMString attributeName1 = "parentID";
					attributeValue1.AppendNumber(PMReal(pNode.getPubId()));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName1),WideString(attributeValue1));

					attributeName1.Clear();
					attributeValue1.Clear();

					int32 childCount  = tagInfo.tagPtr->GetChildCount();
					PMString ChildCount("");
					ChildCount.AppendNumber(childCount);

					double elementId; 
					double typeId;
					int32 header;
					double childId;
					double parentId; 
					int16 whichTab; 
					int32 imgFlag; 
					double sectionID; 
					double childParentTypeID; 
					double languageID;
					int32 isAutoResize;
					double rowno;
					double colno;
					double tableType;
					double tableId;
					int32 childTag;



					int32 isEventField;
					//bool16 isTablePresent;
					TagStruct childTagInfo;
					int32 itemIndex =0;

					
					//-----
					VectorScreenTableInfoPtr itemTableInfo = NULL;
					VectorScreenTableInfoPtr tableInfo = NULL;
					VectorAdvanceTableScreenValuePtr advTableInfo = NULL;
					TableSourceInfoValue *tableSourceInfoValueObj;
					bool16 isCallFromTS = kFalse;
					
					
					if(tagInfo.whichTab == 3 || tagInfo.whichTab == 4) //APS10
					{
						InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
						if(ptrTableSourceHelper != nil)
						{
							tableSourceInfoValueObj=ptrTableSourceHelper->getTableSourceInfoValueObj();
							if(tableSourceInfoValueObj)
							{
								isCallFromTS = tableSourceInfoValueObj->getIsCallFromTABLEsource();
								if(isCallFromTS )	
								{	
									if(tagInfo.tableType == 3)
									{
										advTableInfo = tableSourceInfoValueObj->getVectorAdvanceTableScreenValuePtr();
									}
									else
									{
										if(tagInfo.whichTab == 3)
											tableInfo = tableSourceInfoValueObj->getVectorScreenTableInfoPtr();
										else if(tagInfo.whichTab == 4)
                                            tableInfo = tableSourceInfoValueObj->getVectorScreenItemTableInfoPtr();
									}
								}
							}
						}
					
					}

					double prev_ElementID = 0;
					int32 tst_Count = 0;
					count_PivotList = pv_count;
					int32 newCountValue = 0;

					for(int j = 0; j < childCount ;j++,tst_Count++,newCountValue++)
					{
						XMLReference childXMLReference = tagInfo.tagPtr->GetNthChild(j);
						InterfacePtr<IIDXMLElement>childReference(childXMLReference.Instantiate());
						PMString temp("");
						temp = childReference->GetAttributeValue(WideString("ID"));
						elementId = temp.GetAsDouble();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("header"));
						header = temp.GetAsNumber();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("typeId"));
						typeId = temp.GetAsDouble();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("childId"));
						childId = temp.GetAsDouble();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("parentID")); 
						parentId = temp.GetAsDouble();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("index")); 
						whichTab = temp.GetAsNumber();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("imgFlag"));
						imgFlag = temp.GetAsNumber();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("sectionID"));
						sectionID = temp.GetAsDouble();
						temp.Clear();			
							
						temp = childReference->GetAttributeValue(WideString("parentTypeID"));
						childParentTypeID = temp.GetAsDouble();
						temp.Clear();					
						
						temp = childReference->GetAttributeValue(WideString("LanguageID"));
						languageID = temp.GetAsDouble();
						temp.Clear();			
						
						temp = childReference->GetAttributeValue(WideString("isAutoResize"));
						isAutoResize = temp.GetAsNumber();
						temp.Clear();			
						
						temp = childReference->GetAttributeValue(WideString("rowno"));
						rowno = temp.GetAsDouble();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("colno"));
						colno = temp.GetAsDouble();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("tableType"));
						tableType = temp.GetAsDouble();
						temp.Clear();


						temp = childReference->GetAttributeValue(WideString("tableId"));
						tableId = temp.GetAsDouble();
						temp.Clear();
						
						temp = childReference->GetAttributeValue(WideString("childTag"));
						childTag = temp.GetAsNumber();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("isEventField"));
						isEventField = temp.GetAsNumber();
						temp.Clear();

						if(tagInfo.isAutoResize == 1)
							CopyFlag=1;

						if(tagInfo.isAutoResize == 2)
							CopyFlag=2;

						childTagInfo.elementId = elementId;
						childTagInfo.typeId = typeId;
						childTagInfo.childId = childId;
						childTagInfo.header = header;
						childTagInfo.parentId = parentId;
						childTagInfo.whichTab = whichTab;
						childTagInfo.imgFlag = imgFlag;
						childTagInfo.sectionID = sectionID;
						childTagInfo.tableType = tableType;
						childTagInfo.tableId = tableId;
						childTagInfo.childTag = childTag;
						childTagInfo.isEventField = isEventField;
						
						if(isInlineImage &&  imgFlag ){
							childTagInfo.parentTypeID = pNode.getTypeId(); //ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
							if(FinalItemIds.size() > 0){
								childTagInfo.parentId = FinalItemIds[itemIndex];
								attributeValue1.AppendNumber(PMReal(FinalItemIds[itemIndex]));
							}
							else if(childTagInfo.typeId == -99)
							{
								childTagInfo.parentId = parentId;
								attributeValue1.AppendNumber(PMReal(parentId));
							}

							PMString attributeName = "parentID";
							Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference,   WideString(attributeName),WideString(attributeValue1));
							attributeValue1.clear();
						}
						else if((!isInlineImage) && (!imgFlag) )
							childTagInfo.parentTypeID = childParentTypeID;

						childTagInfo.isAutoResize = isAutoResize;
						childTagInfo.rowno = rowno;
						childTagInfo.colno = colno;
						childTagInfo.tagPtr = childReference; 

						
						PMString itemAttributeValue("");
						if(header == 1)
						{//if(typeId == -2)
							//For table header attribute.
							if(elementId == -701 || elementId == -702)
							{
								/*int32 parentTypeID = childParentTypeID;
								
								VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
								if(TypeInfoVectorPtr != NULL)
								{
									VectorTypeInfoValue::iterator it3;
									int32 Type_id = -1;
									PMString temp = "";
									PMString name = "";
									for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
									{
										Type_id = it3->getType_id();
										if(parentTypeID == Type_id)
										{
											temp = it3->getName();
											if(elementId == -701)
											{
												itemAttributeValue = temp;
											}
											else if(elementId == -702)
											{
												itemAttributeValue = temp + " Suffix";
											}
										}
									}
	
									TypeInfoVectorPtr->clear();
									delete TypeInfoVectorPtr;
	
								}*/
							}
							else if(elementId == -703)
							{
								itemAttributeValue = "$Off";
							}
							else if(elementId == -704)
							{
								itemAttributeValue = "%Off";
							}	
							else if(elementId == -805 || elementId == -806 || elementId == -807 || elementId == -808|| elementId == -809 || elementId == -810 || elementId == -811 || elementId == -812 || elementId == -813 || elementId == -814)
							{
								itemAttributeValue = childReference->GetAttributeValue(WideString("rowno"));
							}						
							else	
								itemAttributeValue = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,languageID );
							
						}//end table header
						else
						{
							if(elementId == -701 || elementId == -702 || elementId == -703 || elementId == -704)
							{	
								//handleSprayingOfEventPriceRelatedAdditions(pNode,childTagInfo,itemAttributeValue);
								handleSprayingOfEventPriceRelatedAdditionsNew(pNode,childTagInfo,itemAttributeValue);
							}
							else
							{
								if(elementId == -803) //Letter  key
								{
									itemAttributeValue = childReference->GetAttributeValue(WideString("rowno"));
									if(itemAttributeValue== "-1")
										itemAttributeValue="";
								}							
								else if(elementId == -804) // For SPraying New Indicator
								{
									if(pNode.getNewProduct()== 1)
										itemAttributeValue = "New";
									else
									{
										itemAttributeValue = "";
										deleteThisBox(boxUIDRef);
										return;
									}
								}
								else if(elementId == -805  || elementId == -806 || elementId == -807 || elementId == -808 || elementId == -809 || elementId == -810 || elementId == -811 || elementId == -812 || elementId == -813 || elementId == -814)
								{
									itemAttributeValue = childReference->GetAttributeValue(WideString("rowno"));
								}
								else if((childReference->GetAttributeValue(WideString("rowno"))) == WideString("-NCitem"))
								{
									//CA("Coming to get Non Catalog Item details"); //for Xref item table
									itemAttributeValue = ""; //??ptrIAppFramework->GETItem_GetItemXrefAttributeDetailsByLanguageId(/*typeId*/childId, elementId, languageID);
                                }
								else if(childReference->GetAttributeValue(WideString("isEventField")) == WideString("1"))
								{
									//CA("tagInfo.isEventField");
									/*??VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(childId,elementId,CurrentSubSectionID);
									if(vecPtr != NULL)
										if(vecPtr->size()> 0)
											itemAttributeValue = vecPtr->at(0).getObjectValue();*/	

									//CA("itemAttributeValue = " + itemAttributeValue);
								}
								else if(elementId == -401 || elementId == -402 || elementId == -403){
									itemAttributeValue = ""; //??ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(childId,elementId, languageID);
								}
								else if(elementId == -827 ){ // Number Key
									itemAttributeValue = childReference->GetAttributeValue(WideString("rowno"));
									if(itemAttributeValue== "-1")
										itemAttributeValue="";
								}
								else if(imgFlag)
								{								
									if(isInlineImage)
									{
										XMLContentReference contentRef = childReference->GetContentReference();
										UIDRef ContentRef = contentRef.GetUIDRef();																			
										this->sprayItemImage(ContentRef,childTagInfo,pNode,typeId);	
										SelectFrame(boxUIDRef);	
		
									}
								}
								else
								{
								//	CA("Spraying Normal item details");
									if((isCallFromTS == kTrue )&& (typeId == -3 || typeId == -55 || typeId == -56))
									{
										CItemTableValue oTableValue;
										VectorScreenTableInfoValue::iterator it;
										bool16 typeidFound=kFalse;
										for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
										{
											oTableValue = *it;
											
											if(oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(tst_Count) && oTableValue.getTableID() == tableSourceInfoValueObj->getVec_Table_ID().at(0) && oTableValue.getTableTypeID() == typeId)
											{
												typeidFound=kTrue;
												itemAttributeValue = oTableValue.getName();	
												tableId = oTableValue.getTableID();
												break;
											}
											else if((oTableValue.getTableID() == tableSourceInfoValueObj->getVec_Table_ID().at(0)) ) //-Condition for Spray List/Table Name--
											{
												if(elementId == -121 && typeId == -3)
												{
													typeidFound=kTrue;
													itemAttributeValue = oTableValue.getName();	
													tableId = oTableValue.getTableID();
													break;
												}
												else if(typeId == -55) 
												{
													if(elementId == -983)//---List Description
													{
														itemAttributeValue = oTableValue.getDescription();	
														typeidFound=kTrue;
													}
													else if(elementId == -982)//---Stencil Name
													{
														itemAttributeValue = oTableValue.getstencil_name();
														typeidFound=kTrue;
													}
													tableId = oTableValue.getTableID();
													break;
													
												}
												else if(typeId == -56)//---"Note 1 to 5"
												{
													if(elementId == -980)//---for Note_1 
													{
														itemAttributeValue = oTableValue.getNotesList().at(0);	
														typeidFound=kTrue;
													}
													else if(elementId == -979)//---for Note_2 
													{
														itemAttributeValue = oTableValue.getNotesList().at(1);		
														typeidFound=kTrue;
													}
													else if(elementId == -978)//---for Note_3
													{
														itemAttributeValue = oTableValue.getNotesList().at(2);		
														typeidFound=kTrue;
													}
													else if(elementId == -977)//---for Note_4
													{
														itemAttributeValue = oTableValue.getNotesList().at(3);		
														typeidFound=kTrue;
													}
													else if(elementId == -976)//---for Note_5 
													{
														itemAttributeValue = oTableValue.getNotesList().at(4);	
														typeidFound=kTrue;
													}
													tableId = oTableValue.getTableID();
													break;
												}
											}

										}
										if(typeidFound == kTrue)
										{
											PMString attributeName;
											PMString attributeValue;
											attributeName = "tableId";
											attributeValue.AppendNumber(PMReal(tableId));
											tagInfo.tagPtr->SetAttributeValue(WideString(attributeName),WideString(attributeValue));

											attributeName.clear();
											attributeValue.clear();
											attributeName = "pbObjectId";
											attributeValue.AppendNumber(PMReal(pNode.getPBObjectID()));
											tagInfo.tagPtr->SetAttributeValue(WideString(attributeName),WideString(attributeValue));
										}
										else
										{
											ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::!typeidFound");
											itemAttributeValue = "";
										}
									}
									else if(typeId == -57)
									{
									//	CA("*****Pivot List****");
										CItemTableValue oTable_Value;
										VectorScreenTableInfoValue::iterator it_List;
										
										vector<double> vectorOf_TableHeader;
										vector<double> vec_items;
										
										if(isCallFromTS == kFalse)
										{
											VectorScreenTableInfoPtr table_Info = NULL;
											table_Info=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(CurrentSectionID,pNode.getPubId() , languageID, kFalse);									
											if(table_Info == NULL)
											{
												break;
											}
											if(table_Info->size() == 0)
											{
												break;
											}
											for(it_List = table_Info->begin(); it_List!=table_Info->end(); it_List++)
											{
												oTable_Value = *it_List;
												break;
											}
											vectorOf_TableHeader.clear();
											vec_items.clear();

											vectorOf_TableHeader = oTable_Value.getTableHeader();
											vec_items = oTable_Value.getItemIds();

											if(vectorOf_TableHeader.size() == newCountValue)
											{
												newCountValue = 0;
												count_PivotList++;
											}
											
											itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(vec_items[count_PivotList],FinalItemIds[tst_Count],languageID, CurrentSectionID, kTrue);   
											
										}
										else
										{

											VectorScreenTableInfoPtr table_Info = NULL;
											bool16 isSelectedList = kFalse;
											table_Info=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(CurrentSectionID,pNode.getPubId() ,languageID,  kFalse);									
											if(table_Info == NULL)
											{
												break;
											}
											if(table_Info->size() == 0)
											{
												break;
											}

											vectorOf_TableHeader.clear();
											vec_items.clear();

											vectorOf_TableHeader = oTable_Value.getTableHeader();
											vec_items = oTable_Value.getItemIds();
											
											if(vectorOf_TableHeader.size() == newCountValue)
											{
												newCountValue = 0;
												count_PivotList++;
											}
											
											itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(vec_items[count_PivotList],FinalItemIds[tst_Count],languageID, CurrentSectionID, kTrue);
										}
										
										PMString attributeName;
										PMString attributeValue;
										attributeName = "tableId";
										attributeValue.AppendNumber(PMReal(tableId));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));

										attributeName.clear();
										attributeValue.clear();
										attributeName = "pbObjectId";
										attributeValue.AppendNumber(PMReal(pNode.getPBObjectID()));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));

										attributeName.clear();
										attributeValue.clear();
										attributeName = "index";
										attributeValue.AppendNumber(4);
										Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference,   WideString(attributeName),WideString(attributeValue));
										
										attributeName.clear();
										attributeValue.clear();
										attributeName = "childTag";
										attributeValue.AppendNumber(1);
										Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference,   WideString(attributeName),WideString(attributeValue));
										
										attributeName.clear();
										attributeValue.clear();
										attributeName = "childId";
										attributeValue.AppendNumber(PMReal(vec_items[count_PivotList]));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference,   WideString(attributeName),WideString(attributeValue));

										attributeName.clear();
										attributeValue.clear();
										attributeName = "tableId";
										attributeValue.AppendNumber(PMReal(oTable_Value.getTableID()));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference,   WideString(attributeName),WideString(attributeValue));

										attributeName.clear();
										attributeValue.clear();
										attributeName = "ID";
                                        attributeValue.AppendNumber(PMReal(FinalItemIds[tst_Count]));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference,   WideString(attributeName),WideString(attributeValue));
 	
									}
									else
									{

										itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(childId,elementId, languageID, CurrentSectionID, kFalse );
										if(elementId == -803 || elementId == -827)//**** Letter Keys and Number keys
										{	
											itemAttributeValue.clear();										
											itemAttributeValue.Append(childReference->GetAttributeValue(WideString("rowno")));	
											if(itemAttributeValue== "-1")
											itemAttributeValue="";
										}
									}
								}
							}
						}

						int32 tagStartPos = -1;
						int32 tagEndPos = -1;
						Utils<IXMLUtils>()->GetElementIndices(childReference,&tagStartPos,&tagEndPos);
						tagStartPos = tagStartPos + 1;
						tagEndPos = tagEndPos -1;

						PMString textToInsert("");
						//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
						//if(!iConverter)
						{
							textToInsert=itemAttributeValue;					
						}
						/*else						
						{
							textToInsert=iConverter->translateString(itemAttributeValue);
							vectorHtmlTrackerSPTBPtr->clear();
							textToInsert=iConverter->translateStringNew(itemAttributeValue, vectorHtmlTrackerSPTBPtr);
						}	*/

                        textToInsert.ParseForEmbeddedCharacters();
						boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
                        ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos +1 ,insertText);

						//this->ToggleBold_or_Italic(textModel , vectorHtmlTrackerSPTBPtr, tagStartPos );

						double sectionID = -1;
						if(global_project_level == 3)
							sectionID = CurrentSubSectionID;
						if(global_project_level == 2)
							sectionID = CurrentSectionID;
						
						TableSourceInfoValue *tableSourceInfoValueObj;
						InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
						if(ptrTableSourceHelper != nil)
						{
							tableSourceInfoValueObj=ptrTableSourceHelper->getTableSourceInfoValueObj();
							if(tableSourceInfoValueObj)
								isCallFromTS = tableSourceInfoValueObj->getIsCallFromTABLEsource();	
							if(isCallFromTS)
							{
								sectionID = CurrentSectionID;
							}
						}
						
						PMString attributeName;
						PMString attributeValue;
						
						attributeName = "parentID";
						if(isInlineImage &&  imgFlag )
						{
							attributeValue.AppendNumber(PMReal(FinalItemIds[itemIndex++]));
                        }
						else{
							attributeValue.AppendNumber(PMReal(pNode.getPubId()));
						}
						Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference,   WideString(attributeName),WideString(attributeValue));
						attributeName.Clear();
						attributeValue.Clear();

						if(elementId != -701 && elementId != -702 && elementId != -703 && elementId != -704)
						{
							attributeName = "parentTypeID";						
							if(isInlineImage  && imgFlag) //*******
							{
								 //attributeValue.AppendNumber(ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE"));		
							}
							else
								attributeValue.AppendNumber(PMReal(CurrentObjectTypeID));

							Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference,   WideString(attributeName),WideString(attributeValue));
							attributeName.Clear();
							attributeValue.Clear();

						}

						attributeName = "sectionID";
						attributeValue.AppendNumber(PMReal(sectionID));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference,   WideString(attributeName),WideString(attributeValue));
						attributeName.Clear();
						attributeValue.Clear();	

						if(pNode.getIsProduct() == 0)
						{
							Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference,   WideString("tableFlag"),WideString("-1001"));
						}
					}
					
					PMString attributeName;
					PMString attributeValue;

					attributeName = "parentID";
					attributeValue.AppendNumber(PMReal(pNode.getPubId()));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();

					attributeName = "parentTypeID";
					attributeValue.AppendNumber(PMReal(CurrentObjectTypeID));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();
					
					if(isCallFromTS)
						CurrentSubSectionID = CurrentSectionID;
							
					attributeName = "sectionID";
					attributeValue.AppendNumber(PMReal(CurrentSubSectionID));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();				
					
					break;
				}
				if(tagInfo.whichTab == 3 && tagInfo.tableType == 7 && tagInfo.childTag == 1)		//For All Standard Table in tabbed text form
				{//if(tagInfo.whichTab == 3 && tagInfo.typeId == -116 && tagInfo.elementId == -101)
					PMString attributeValue1("");
					PMString attributeName1 = "parentID";
					attributeValue1.AppendNumber(PMReal(pNode.getPubId()));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName1),WideString(attributeValue1));
					attributeName1.Clear();
					attributeValue1.Clear();

					int32 childCount  = tagInfo.tagPtr->GetChildCount();
					double elementId; 
					double typeId;
					int32 header;
					int32 childTag;
					int32 dataType;
					double parentId; 
					int16 whichTab; 
					int32 imgFlag; 
					double sectionID; 
					double childParentTypeID; 
					double languageID;
					int32 isAutoResize;
					double rowno;
					double colno;
					//bool16 isTablePresent;
					TagStruct childTagInfo;
					
					for(int j = 0; j < childCount ; j++)
					{
						XMLReference childXMLReference = tagInfo.tagPtr->GetNthChild(j);

						InterfacePtr<IIDXMLElement>childReference(childXMLReference.Instantiate());
						PMString temp("");
						temp = childReference->GetAttributeValue(WideString("ID"));
						elementId = temp.GetAsDouble();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("typeId"));
						typeId = temp.GetAsDouble();
						temp.Clear();
							
						temp = childReference->GetAttributeValue(WideString("header"));
						header = temp.GetAsNumber();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("dataType"));
						dataType = temp.GetAsNumber();
						temp.Clear();
	
						temp = childReference->GetAttributeValue(WideString("childTag"));
						childTag = temp.GetAsNumber();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("parentID")); 
						parentId = temp.GetAsDouble();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("index")); 
						whichTab = temp.GetAsNumber();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("imgFlag"));
						imgFlag = temp.GetAsNumber();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("sectionID"));
						sectionID = temp.GetAsDouble();
						temp.Clear();			
							
						temp = childReference->GetAttributeValue(WideString("parentTypeID"));
						childParentTypeID = temp.GetAsDouble();
						temp.Clear();					
						
						temp = childReference->GetAttributeValue(WideString("LanguageID"));
						languageID = temp.GetAsDouble();
						temp.Clear();			
						
						temp = childReference->GetAttributeValue(WideString("isAutoResize"));
						isAutoResize = temp.GetAsNumber();
						temp.Clear();			
						
						temp = childReference->GetAttributeValue(WideString("rowno"));
						rowno = temp.GetAsDouble();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("colno"));
						colno = temp.GetAsDouble();
						temp.Clear();
						
						if(tagInfo.isAutoResize == 1)
							CopyFlag=1;

						if(tagInfo.isAutoResize == 2)
							CopyFlag=2;

						childTagInfo.elementId = elementId;
						childTagInfo.typeId = typeId;
						childTagInfo.header = header;
						childTagInfo.dataType = dataType;
						childTagInfo.childTag = childTag;
						childTagInfo.parentId = parentId;
						childTagInfo.whichTab = whichTab;
						childTagInfo.imgFlag = imgFlag;
						childTagInfo.sectionID = sectionID;
						childTagInfo.parentTypeID = childParentTypeID;
						childTagInfo.isAutoResize = isAutoResize;
						childTagInfo.rowno = rowno;
						childTagInfo.colno = colno;
						childTagInfo.tagPtr = childReference; 
						
						PMString itemAttributeValue("");
						if(childTagInfo.header == 1)
						{//if(typeId == -2)
							double sectionid = -1;
							if(global_project_level == 3)
								sectionid = CurrentSubSectionID;
							if(global_project_level == 2)
								sectionid = CurrentSectionID;

							if(elementId == -701 || elementId == -702)
							{
								/*int32 parentTypeID = childParentTypeID;
								
								VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
								if(TypeInfoVectorPtr != NULL)
								{
									VectorTypeInfoValue::iterator it3;
									int32 Type_id = -1;
									PMString temp = "";
									PMString name = "";
									for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
									{
										Type_id = it3->getType_id();
										if(parentTypeID == Type_id)
										{
											temp = it3->getName();
											if(elementId == -701)
											{
												itemAttributeValue = temp;
											}
											else if(elementId == -702)
											{
												itemAttributeValue = temp + " Suffix";
											}
										}
									}
	
									TypeInfoVectorPtr->clear();
									delete TypeInfoVectorPtr;
								}*/
							}
							else if(elementId == -703)
							{
								itemAttributeValue = "$Off";
							}
							else if(elementId == -704)
							{
								itemAttributeValue = "%Off";
							}	
							else if(elementId == -805 || elementId == -806 || elementId == -807 || elementId == -808|| elementId == -809 || elementId == -810 || elementId == -811 || elementId == -812 || elementId == -813 || elementId == -814)
							{
								itemAttributeValue = childReference->GetAttributeValue(WideString("rowno"));
							}						
							else	
								itemAttributeValue = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,languageID );


							PMString attributeName;
								PMString attributeValue;
							attributeName = "parentID";
							if(childTagInfo.whichTab == 3)
								attributeValue.AppendNumber(PMReal(pNode.getPubId()));
							if(childTagInfo.whichTab == 4)
								attributeValue.AppendNumber(PMReal(pNode.getPubId()));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference, WideString(attributeName),WideString(attributeValue));
							attributeName.Clear();
							attributeValue.Clear();
							
							attributeName = "parentTypeID";
							attributeValue.AppendNumber(PMReal(CurrentObjectTypeID));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference, WideString(attributeName),WideString(attributeValue));
							attributeName.Clear();
							attributeValue.Clear();				

							attributeName = "sectionID";
							attributeValue.AppendNumber(PMReal(sectionid));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference, WideString(attributeName),WideString(attributeValue));
							attributeName.Clear();
							attributeValue.Clear();	
							
						}//end table header
						else
						{
                            double sectionid = -1;
							if(global_project_level == 3)
								sectionid = CurrentSubSectionID;
							if(global_project_level == 2)
								sectionid = CurrentSectionID;

							if(childTagInfo.dataType == 4)
							{//if(childTagInfo.elementId == -121)
								PMString textToInsert("");				
							
								if(childTagInfo.whichTab == 3 || childTagInfo.whichTab == 4)
								{
									double objectId = pNode.getPubId(); //Product object It is an objectID used for calling ApplicationFrameworks method.
									VectorScreenTableInfoPtr tableInfo = NULL;
									
									if(pNode.getIsONEsource())
									{
										// For ONEsource To get all Table related information when table stencils is selected on 9/10 by dattatray
										//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(objectId);
									}else
									{
                                        if(childTagInfo.whichTab == 3)
                                        {
                                            tableInfo= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, objectId, childTagInfo.languageID, kTrue);
                                        }
                                        else if(childTagInfo.whichTab == 4)//APS10
                                        {
                                            tableInfo = ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPubId(), sectionid, tagInfo.languageID);
                                        }
                                        
									}
									if(!tableInfo)
									{
										ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::!tableInfo");
										return;
									}

									if(tableInfo->size()==0){
										ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::tableInfo->size()==0");
										break;
									}

									CItemTableValue oTableValue;
									VectorScreenTableInfoValue::iterator it;

									bool16 typeidFound=kFalse;
									for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
									{
										oTableValue = *it;
										if(oTableValue.getTableTypeID() == childTagInfo.typeId)
										{   				
											typeidFound=kTrue;				
											break;
										}
									}

									if(typeidFound)
									{
										textToInsert = oTableValue.getName();	
									}
									else
									{
										ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::!typeidFound");
										textToInsert = "";
									}
	
									tableInfo->clear();
									delete tableInfo;
								}

								TextIndex startPos=-1;
								TextIndex endPos = -1;

								int32 tagStartPos = 0;
								int32 tagEndPos =0; 			
								Utils<IXMLUtils>()->GetElementIndices(childTagInfo.tagPtr,&tagStartPos,&tagEndPos);
								tagStartPos = tagStartPos + 1;
								tagEndPos = tagEndPos -1;

                                textToInsert.ParseForEmbeddedCharacters();
                                boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
								ReplaceText(textModel, tagStartPos, tagEndPos-tagStartPos + 1, insertText);
							}
							else
							{
								//CA("Spraying Normal item details");
								if(childTagInfo.isEventField)
								{
									/*VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(typeId,elementId,sectionid);
									if(vecPtr != NULL)
										if(vecPtr->size()> 0)
											itemAttributeValue = vecPtr->at(0).getObjectValue();	*/

									//CA("itemAttributeValue = " + itemAttributeValue);
								}
								else if(elementId == -401 || elementId == -402 || elementId == -403){
									itemAttributeValue = ""; //ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(typeId,elementId, languageID);
								}
								else{
									itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(typeId,elementId, languageID, CurrentSectionID, kFalse );
								}
								int32 tagStartPos = -1;
								int32 tagEndPos = -1;
								Utils<IXMLUtils>()->GetElementIndices(childTagInfo.tagPtr,&tagStartPos,&tagEndPos);
								tagStartPos = tagStartPos + 1;
								tagEndPos = tagEndPos -1;

								PMString textToInsert("");
								//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
								//if(!iConverter)
								{
									textToInsert=itemAttributeValue;					
								}
								//else						
								//{
								//	//textToInsert=iConverter->translateString(itemAttributeValue);
								//	vectorHtmlTrackerSPTBPtr->clear();
								//	textToInsert=iConverter->translateStringNew(itemAttributeValue, vectorHtmlTrackerSPTBPtr);
								//}	

                                textToInsert.ParseForEmbeddedCharacters();
                                boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
								ReplaceText(textModel, tagStartPos, tagEndPos-tagStartPos + 1, insertText);
								
							}		

							PMString attributeName;
								PMString attributeValue;
							attributeName = "parentID";
							if(childTagInfo.whichTab == 3)
								attributeValue.AppendNumber(PMReal(pNode.getPubId()));
							if(childTagInfo.whichTab == 4)
								attributeValue.AppendNumber(PMReal(pNode.getPubId()));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference, WideString(attributeName),WideString(attributeValue));
							attributeName.Clear();
							attributeValue.Clear();
							
							attributeName = "parentTypeID";
							attributeValue.AppendNumber(PMReal(CurrentObjectTypeID));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference, WideString(attributeName),WideString(attributeValue));
							attributeName.Clear();
							attributeValue.Clear();				

							attributeName = "sectionID";
							attributeValue.AppendNumber(PMReal(sectionid));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference, WideString(attributeName),WideString(attributeValue));
							attributeName.Clear();
							attributeValue.Clear();	
						}
					}
					PMString attributeName;
					PMString attributeValue;

					attributeName = "parentID";
					attributeValue.AppendNumber(PMReal(pNode.getPubId()));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();

					attributeName = "parentTypeID";
					attributeValue.AppendNumber(PMReal(CurrentObjectTypeID));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();
					
					attributeName = "sectionID";
					attributeValue.AppendNumber(PMReal(CurrentSubSectionID));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();	
					break;
				}
				else if(tagInfo.whichTab == 3 && tagInfo.tableType == 7 && tagInfo.childTag != 1)	// For All Standrd Table in Table form
				{//else if(tagInfo.whichTab == 3 && tagInfo.typeId == -116 && tagInfo.elementId != -101)
					//CA("Going for spray All Standard Table in Table form");
		
					InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
					if(tableList==nil)
					{
						ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::sprayForThisBox::tableList == nil");
						break;
					}
					int32	tableCount = tableList->GetModelCount();

					PMString attributeValue1("");
					PMString attributeName1 = "parentID";
					attributeValue1.AppendNumber(PMReal(pNode.getPubId()));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName1),WideString(attributeValue1));
					attributeName1.Clear();
					attributeValue1.Clear();

					int32 childCount  = tagInfo.tagPtr->GetChildCount();
					
					double elementId; 
					double typeId;
					int32 header;
					int32 childTag;	
					int32 dataType;
					double parentId; 
					int16 whichTab; 
					int32 imgFlag; 
					double sectionID; 
					double childParentTypeID; 
					double languageID;
					int32 isAutoResize;
					double rowno;
					double colno;
					TagStruct childTagInfo;
					
					for(int j = 0; j < childCount ; j++)
					{
						XMLReference childXMLReference = tagInfo.tagPtr->GetNthChild(j);
						InterfacePtr<IIDXMLElement>childReference(childXMLReference.Instantiate());	
						PMString temp("");
						temp = childReference->GetAttributeValue(WideString("ID"));
						elementId = temp.GetAsDouble();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("typeId"));
						typeId = temp.GetAsDouble();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("header"));
						header = temp.GetAsNumber();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("childTag"));
						childTag = temp.GetAsNumber();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("dataType"));
						dataType = temp.GetAsNumber();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("parentID")); 
						parentId = temp.GetAsDouble();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("index")); 
						whichTab = temp.GetAsNumber();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("imgFlag"));
						imgFlag = temp.GetAsNumber();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("sectionID"));
						sectionID = temp.GetAsDouble();
						temp.Clear();			
							
						temp = childReference->GetAttributeValue(WideString("parentTypeID"));
						childParentTypeID = temp.GetAsDouble();
						temp.Clear();					
						
						temp = childReference->GetAttributeValue(WideString("LanguageID"));
						languageID = temp.GetAsDouble();
						temp.Clear();			
						
						temp = childReference->GetAttributeValue(WideString("isAutoResize"));
						isAutoResize = temp.GetAsNumber();
						temp.Clear();			
						
						temp = childReference->GetAttributeValue(WideString("rowno"));
						rowno = temp.GetAsDouble();
						temp.Clear();

						temp = childReference->GetAttributeValue(WideString("colno"));
						colno = temp.GetAsDouble();
						temp.Clear();

						
						if(tagInfo.isAutoResize == 1)
							CopyFlag=1;

						if(tagInfo.isAutoResize == 2)
							CopyFlag=2;

						childTagInfo.elementId = elementId;
						childTagInfo.typeId = typeId;
						childTagInfo.header = header;
						childTagInfo.childTag = childTag;
						childTagInfo.dataType = dataType;
						childTagInfo.parentId = parentId;
						childTagInfo.whichTab = whichTab;
						childTagInfo.imgFlag = imgFlag;
						childTagInfo.sectionID = sectionID;
						childTagInfo.parentTypeID = childParentTypeID;
						childTagInfo.isAutoResize = isAutoResize;
						childTagInfo.rowno = rowno;
						childTagInfo.colno = colno;
						childTagInfo.tagPtr = childReference; 
						
						
						PMString itemAttributeValue("");
						
						if(childTagInfo.header == 1)
							AddHeaderToTable = kTrue;
						else
							AddHeaderToTable = kFalse;
					

						double sectionid = -1;
						if(global_project_level == 3)
							sectionid = CurrentSubSectionID;
						if(global_project_level == 2)
							sectionid = CurrentSectionID;

						if(childTagInfo.dataType == 4)
						{//if(childTagInfo.elementId == -121)
							PMString textToInsert("");
							if(childTagInfo.whichTab == 3 || childTagInfo.whichTab == 4)
							{
								double objectId = pNode.getPubId(); //Product object It is an objectID used for calling ApplicationFrameworks method.
								VectorScreenTableInfoPtr tableInfo = NULL;
								
								if(pNode.getIsONEsource())
								{
									// For ONEsource To get all Table related information when table stencils is selected on 9/10 by dattatray
									//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(objectId);
								}else
								{
									// For Publication to get all information about Table such as typeid,tablename,tableid,istranspose,printheader,tableheader etc..
                                    if(childTagInfo.whichTab == 3)
                                    {
                                        tableInfo= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, objectId, childTagInfo.languageID, kTrue); // getting custom table data for getting only table name
                                    }
                                    else if(childTagInfo.whichTab == 4)
                                    {
                                        tableInfo = ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPubId(), sectionid, tagInfo.languageID);
                                    }
                                    
								}
								if(!tableInfo)
								{
									ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::!tableInfo");
									return;
								}

								if(tableInfo->size()==0){
									ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::tableInfo->size()==0");
									break;
								}

								CItemTableValue oTableValue;
								VectorScreenTableInfoValue::iterator it;

								bool16 typeidFound=kFalse;
								for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
								{
									oTableValue = *it;
									if(oTableValue.getTableTypeID() == childTagInfo.typeId)
									{   				
										typeidFound=kTrue;				
										break;
									}
								}

								if(typeidFound)
								{
									textToInsert = oTableValue.getName();	
								}
								else
								{
									ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::!typeidFound");
									textToInsert = "";
								}									
							}

							TextIndex startPos=-1;
							TextIndex endPos = -1;

							int32 tagStartPos = 0;
							int32 tagEndPos =0; 			
							Utils<IXMLUtils>()->GetElementIndices(childTagInfo.tagPtr,&tagStartPos,&tagEndPos);
							tagStartPos = tagStartPos + 1;
							tagEndPos = tagEndPos -1;

                            textToInsert.ParseForEmbeddedCharacters();
                            boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
							ReplaceText(textModel, tagStartPos, tagEndPos-tagStartPos + 1, insertText);
							
						}
						else
						{
							//CA("Going to spray table");
							double objectId = pNode.getPubId(); //Product object It is an objectID used for calling ApplicationFrameworks method.		
															
							VectorScreenTableInfoPtr tableInfo = NULL;
							
							if(pNode.getIsONEsource())
							{
								// For ONEsource To get all Table related information when table stencils is selected on 9/10 by dattatray
								//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(objectId);
							}else
							{
								// For Publication to get all information about Table such as typeid, tablename, tableid, istranspose, printheader, tableheader etc..
								tableInfo= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, objectId, childTagInfo.languageID, kFalse);
							}
							if(!tableInfo)
							{
								ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::!tableInfo");
								return;
							}

							if(tableInfo->size()==0){
								ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::tableInfo->size()==0");
								break;
							}

							CItemTableValue oTableValue;
							VectorScreenTableInfoValue::iterator it;

							bool16 typeidFound=kFalse;
							for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
							{
								oTableValue = *it;
								if(oTableValue.getTableTypeID() == childTagInfo.typeId)
								{   				
									typeidFound=kTrue;				
									break;
								}
							}

							if(!typeidFound)
							{
								break;	
							}
							int32 numRows = static_cast<int32>(oTableValue.getTableData().size());
							if(numRows<=0)
								break;
							int32 numCols = static_cast<int32>(oTableValue.getTableHeader().size());
							if(numCols<=0)
								break;

							vector<double> vec_tableheaders = oTableValue.getTableHeader();
							vector<double> vec_items = oTableValue.getItemIds();;

							bool8 isTranspose = oTableValue.getTranspose();
							vector<vector<PMString> > vec_tablerows;
							vec_tablerows = oTableValue.getTableData();
							

							if(isTranspose)
							{
								XMLContentReference xmlCntnRef = childTagInfo.tagPtr->GetContentReference();
								if(xmlCntnRef.IsTable())
								{
									//CA("Is table");
									int32 i=0, j=0;
									UIDRef tableRef ;
									UIDRef tableUIDRef;	
									tableRef = xmlCntnRef.GetUIDRef();
									bool16 isMatch = kFalse;
									for(int32 tableIndex = 0 ; tableIndex < tableCount ; tableIndex++)
									{
										InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
										if(tableModel == nil) 
										{
											ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::sprayForThisBox::tableModel == nil");
											break;
										}
										UIDRef tableUIDRef1(::GetUIDRef(tableModel));	
										if(tableRef == tableUIDRef1)
										{
											//CA("Found");
											isMatch=kTrue;
											tableUIDRef = tableUIDRef1;
											break;
										}
									}
									
									if(isMatch == kFalse)
										break;

									TableUtility oTableUtil(this);
									oTableUtil.resizeTable(tableUIDRef, numCols, numRows,isTranspose, 0, 0);

									if(AddHeaderToTable){
										oTableUtil.putHeaderDataInTable(tableUIDRef, vec_tableheaders,isTranspose,childTagInfo,boxUIDRef);
										i=1; j=0;
									}

									vector<vector<PMString> >::iterator it1;
									for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
									{
										vector<PMString>::iterator it2;
										for(it2=(*it1).begin();it2!=(*it1).end();it2++)
										{
											if((j)>numCols)
												break;
											oTableUtil.setTableRowColData(tableUIDRef, (*it2), j, i);	
											SlugStruct stencileInfo;
											
											stencileInfo.elementId = vec_tableheaders[j];
											stencileInfo.typeId = tagInfo.typeId;
											stencileInfo.header = tagInfo.header ;
											stencileInfo.isEventField = tagInfo.isEventField;
											stencileInfo.deleteIfEmpty = tagInfo.deleteIfEmpty;
											stencileInfo.dataType = tagInfo.dataType;
											stencileInfo.isAutoResize = tagInfo.isAutoResize;
											stencileInfo.LanguageID = tagInfo.languageID;
											stencileInfo.whichTab = 4; // Item Attributes //for cell tag of product standard Table
											stencileInfo.pbObjectId = tagInfo.pbObjectId;
											stencileInfo.parentId = pNode.getPubId();
												
											if(AddHeaderToTable)
												stencileInfo.childId = vec_items[i-1];
											else
												stencileInfo.childId  = vec_items[i];

											stencileInfo.sectionID = CurrentSubSectionID; 
											stencileInfo.parentTypeId = pNode.getTypeId();
											stencileInfo.isSprayItemPerFrame = tagInfo.isSprayItemPerFrame;
											stencileInfo.catLevel = tagInfo.catLevel;
											stencileInfo.imgFlag = CurrentSubSectionID;
											stencileInfo.imageIndex = tagInfo.imageIndex;
											stencileInfo.flowDir = tagInfo.flowDir;
											stencileInfo.childTag = tagInfo.childTag;
											stencileInfo.tableFlag = tagInfo.isTablePresent;
											stencileInfo.tableTypeId = tagInfo.typeId;
											stencileInfo.tableType = tagInfo.tableType ;
											stencileInfo.tableId = tagInfo.tableId;
											stencileInfo.col_no = tagInfo.colno;


											PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j],tagInfo.languageID );// changes is required language ID req.
											stencileInfo.elementName = dispname;
											PMString TableColName("");
											
											TableColName.Append("ATTRID_");
											TableColName.AppendNumber(PMReal(vec_tableheaders[j]));
									
											TableColName = keepOnlyAlphaNumeric(TableColName);
											stencileInfo.TagName = TableColName;
							
											XMLReference txmlref;
											PMString TableTagNameNew = tagInfo.tagPtr->GetTagString();
											if(TableTagNameNew.Contains("PSTable"))
											{
												//CA("PSTable Found");
											}
											else
											{				
												TableTagNameNew =  "PSTable";
												//CA("PSTable Not Found : "+ TableTagNameNew);
											}

											oTableUtil.TagTable(tableUIDRef,boxUIDRef,tagInfo.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, j, i,0 );
											j++;
										}
										i++;
										j=0;
									}
								}
							}
							else
							{
								XMLContentReference xmlCntnRef = childTagInfo.tagPtr->GetContentReference();
								if(xmlCntnRef.IsTable())
								{
									int32 i=0, j=0;
									UIDRef tableRef ;
									UIDRef tableUIDRef;	
									tableRef = xmlCntnRef.GetUIDRef();
									bool16 isMatch = kFalse;
									for(int32 tableIndex = 0 ; tableIndex < tableCount ; tableIndex++)
									{
										InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
										if(tableModel == nil) 
										{
											ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::sprayForThisBox::tableModel == nil");
											break;
										}
										UIDRef tableUIDRef1(::GetUIDRef(tableModel));	
										if(tableRef == tableUIDRef1)
										{
											isMatch=kTrue;
											tableUIDRef = tableUIDRef1;
											break;
										}
									}
									
									if(isMatch == kFalse)
										break;

									TableUtility oTableUtil(this);
									oTableUtil.resizeTable(tableUIDRef,numRows, numCols,isTranspose, 0, 0);
									

									if(AddHeaderToTable)
									{
										oTableUtil.putHeaderDataInTable(tableUIDRef,vec_tableheaders,isTranspose,childTagInfo,boxUIDRef);
										 i=1; j=0;
									} 
									vector < vector <PMString> >::iterator it1;
									for(it1 = vec_tablerows.begin(); it1 != vec_tablerows.end(); it1++)
									{
										vector<PMString>::iterator it2;
										for(it2 = (*it1).begin() ; it2 != (*it1).end() ; it2++)
										{
											if((j)>numCols)
												break;
								
											oTableUtil.setTableRowColData(tableUIDRef, (*it2), i, j);
											SlugStruct stencileInfo;
											stencileInfo.elementId = vec_tableheaders[j];
											stencileInfo.typeId = tagInfo.typeId;
											stencileInfo.header = tagInfo.header;
											stencileInfo.isEventField = tagInfo.isEventField;
											stencileInfo.deleteIfEmpty = tagInfo.deleteIfEmpty;
											stencileInfo.dataType = tagInfo.dataType;
											stencileInfo.isAutoResize = tagInfo.isAutoResize;
											stencileInfo.LanguageID = tagInfo.languageID;
											stencileInfo.whichTab = 4; // Item Attributes //for cell tag of product standard Table
											stencileInfo.pbObjectId = tagInfo.pbObjectId;
											stencileInfo.parentId = pNode.getPubId();
											
											if(AddHeaderToTable)
												stencileInfo.childId = vec_items[i-1];
											else
												stencileInfo.childId = vec_items[i];
											
											stencileInfo.sectionID = CurrentSubSectionID; 
											stencileInfo.parentTypeId = pNode.getTypeId();
											stencileInfo.isSprayItemPerFrame = tagInfo.isSprayItemPerFrame;
											stencileInfo.catLevel = tagInfo.catLevel;
											stencileInfo.imgFlag = 0;
											stencileInfo.imageIndex = tagInfo.imageIndex;
											stencileInfo.flowDir = tagInfo.flowDir;
											stencileInfo.childTag = tagInfo.childTag;
											stencileInfo.tableFlag = tagInfo.isTablePresent;
											stencileInfo.tableTypeId = tagInfo.typeId;
											stencileInfo.tableType = tagInfo.tableType;
											stencileInfo.tableId = tagInfo.tableId;
											stencileInfo.col_no = tagInfo.colno;


											PMString dispname =ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j],tagInfo.languageID );// changes is required language ID req.
											stencileInfo.elementName = dispname;
											PMString TableColName("");
											
											TableColName.Append("ATTRID_");
											TableColName.AppendNumber(PMReal(vec_tableheaders[j]));
											TableColName = keepOnlyAlphaNumeric(TableColName);
											stencileInfo.TagName = TableColName;
											XMLReference txmlref;

											PMString TableTagNameNew = tagInfo.tagPtr->GetTagString();
											if(TableTagNameNew.Contains(" PSTable"))
											{
												//CA("PSTable Found");
											}
											else
											{				
												TableTagNameNew =  "PSTable";
												//CA("PSTable Not Found : "+ TableTagNameNew);
											}
											
											InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
											if (tableModel == NULL)
											{
												//CA("Err: invalid interface pointer ITableFrame");
												break;
											}

											oTableUtil.TagTable(tableUIDRef,boxUIDRef,tagInfo.tagPtr->GetTagString(), TableTagNameNew, txmlref, stencileInfo, i, j,0 );
											j++;
										}
										i++;
										j=0;
									}
									i=0;
									j=0;
								}
							}
						}
						PMString attributeName;
							PMString attributeValue;
						attributeName = "parentID";
						if(childTagInfo.whichTab == 3)
							attributeValue.AppendNumber(PMReal(pNode.getPubId()));
						if(childTagInfo.whichTab == 4)
							attributeValue.AppendNumber(PMReal(pNode.getPubId()));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference, WideString(attributeName),WideString(attributeValue));
						attributeName.Clear();
						attributeValue.Clear();
						
						attributeName = "parentTypeID";
						attributeValue.AppendNumber(PMReal(CurrentObjectTypeID));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference, WideString(attributeName),WideString(attributeValue));
						attributeName.Clear();
						attributeValue.Clear();				

						attributeName = "sectionID";
						attributeValue.AppendNumber(PMReal(sectionid));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(childXMLReference, WideString(attributeName),WideString(attributeValue));
						attributeName.Clear();
						attributeValue.Clear();	
						
					}
					PMString attributeName;
					PMString attributeValue;

					attributeName = "parentID";
					attributeValue.AppendNumber(PMReal(pNode.getPubId()));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();

					attributeName = "parentTypeID";
					attributeValue.AppendNumber(PMReal(CurrentObjectTypeID));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();
					
					attributeName = "sectionID";
					attributeValue.AppendNumber(PMReal(CurrentSubSectionID));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();	
					break;
				}
                
                if(tagInfo.whichTab == 4 && tagInfo.tableType == 9 )
                {
                    if(tagInfo.isAutoResize == 1)
                        CopyFlag=1;
                    
                    if(tagInfo.isAutoResize == 2)
                        CopyFlag=2;
                    
                    TableUtility tbUtil(this);
                    tbUtil.SprayItemAttributeGroupInsideTable(boxUIDRef, tagInfo, pNode);
                    break;
                }

				if((tagInfo.whichTab == 3 || tagInfo.whichTab == 4) && tagInfo.tableType == 2 )
				//This is a unique combination.Note in all other cases we used
				//whichTab == 3 for product but here the above condition in if together 
				//specifies that it for item and the item_copy_attributes lies inside the cell.
				{
					if(tagInfo.isAutoResize == 1)
						CopyFlag=1;

					if(tagInfo.isAutoResize == 2)
						CopyFlag=2;

					TableUtility tbUtil(this);
					//La-z-boy customization.
					//tbUtil.SprayUserTableAsPerCellTagsForOneItem(boxUIDRef, tagInfo, pNode);

					if(!ptrIAppFramework->ConfigCache_getSprayTableStructurePerItem()) // System config used to get Table spraying way
				
						// Eithere LAZBoy way or Normal way
					{  // Spraying Custom Table in Normal form 
						//CA("Normal Table Sprasy.");
						
						//--------------
						bool16 isCallFromTS = kFalse;
						TableSourceInfoValue *tableSourceInfoValueObj;
						InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));

						if(pNode.getIsProduct() == 1)	
						{
							//CA("pNode.getIsProduct() == 1");
							VectorScreenTableInfoPtr tableInfo = NULL;
							
							if(ptrTableSourceHelper != nil)
							{
								tableSourceInfoValueObj=ptrTableSourceHelper->getTableSourceInfoValueObj();
								if(tableSourceInfoValueObj)
								{
									isCallFromTS = tableSourceInfoValueObj->getIsCallFromTABLEsource();
									if(isCallFromTS )	
										tableInfo=tableSourceInfoValueObj->getVectorScreenTableInfoPtr();
								}
							}

							if(isCallFromTS && tableSourceInfoValueObj->getVec_Table_ID().size() > 1)
							{
								ErrorCode result123 =	
									tbUtil.SprayMultipleCustomTableFormTABLEsource(boxUIDRef, tagInfo, pNode);
							}
							else
							{
								//CA("CAll from Content Sprayer");
								if(tagInfo.rowno == -904)
								{
									//CA("MMY Table");
									ErrorCode result123 = tbUtil.SprayMMYCustomTable(boxUIDRef, tagInfo, pNode);
								}
								else
								{
									ErrorCode result123 = tbUtil.SprayIndividualItemAndNonItemCopyAttributesInsideTable(boxUIDRef, tagInfo, pNode);
								}
							}
						}
						if(pNode.getIsProduct() == 0)
						{
							VectorScreenTableInfoPtr itemTableInfo=NULL;
							if(ptrTableSourceHelper != nil)
							{
								tableSourceInfoValueObj=ptrTableSourceHelper->getTableSourceInfoValueObj();
								if(tableSourceInfoValueObj)
								{
									isCallFromTS = tableSourceInfoValueObj->getIsCallFromTABLEsource();
									if(isCallFromTS )	
										itemTableInfo=tableSourceInfoValueObj->getVectorScreenItemTableInfoPtr();
								}
							}

							if(isCallFromTS && tableSourceInfoValueObj->getVec_Table_ID().size() > 1)
							{
								ErrorCode result123 =	
									tbUtil.SprayMultipleCustomTableFormTABLEsource(boxUIDRef, tagInfo, pNode);
							}
							else
							{
								if(tagInfo.rowno == -904)
								{
									//CA("MMY Table");
									ErrorCode result123 = tbUtil.SprayMMYCustomTable(boxUIDRef, tagInfo, pNode);
								}
								else
								{
									ErrorCode result123 = tbUtil.SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable(boxUIDRef, tagInfo, pNode);
								}

							}

						}

						if(tableSourceInfoValueObj)
						{
							tableSourceInfoValueObj = NULL;
						}
					}
					else
					{	// LAZBOY Special case.			
						//CA("before SprayUserTableAsPerCellTagsForMultipleItem");
						tbUtil.SprayUserTableAsPerCellTagsForMultipleItem(boxUIDRef, tagInfo, pNode);
						//CA("after SprayUserTableAsPerCellTagsForMultipleItem");
					}

					break;
				}

				if(
					  tagInfo.whichTab == 3 //For product_copy_attributes
					&& tagInfo.parentId == -1 //Spraying is not done 
					&& tagInfo.isTablePresent == kTrue //i.e table is in TabbedTextFormat
					&& tagInfo.tableType != 2	//each table present has different typeId.
				  )
				{
					if(tagInfo.isAutoResize == 1)
						CopyFlag=1;
					if(tagInfo.isAutoResize == 2)
						CopyFlag=2;
					
					if(pNode.getIsProduct()== 1)
					{
						if(tagInfo.elementId == -103)
						{
							//CA("got it");
							PMString dispName("");
							do
							{
								double typeId = tagInfo.typeId;
						
								VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
								if(typeValObj==NULL)
								{
									ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 0:StructureCache_getListTableTypes's typeValObj is NULL");												
									break;
								}

								VectorTypeInfoValue::iterator it1;

								bool16 typeIDFound = kFalse;
								for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
								{	
									if(typeId == it1->getTypeId())
									{
										typeIDFound = kTrue;
										break;
									}			
								}
								if(typeIDFound)
									dispName = it1->getName();

								if(typeValObj)
									delete typeValObj;
							}while(kFalse);

							int32 tagStartPos = -1;
							int32 tagEndPos = -1;
							Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&tagStartPos,&tagEndPos);
							tagStartPos = tagStartPos + 1;
							tagEndPos = tagEndPos -1;

							PMString textToInsert("");
							//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
							//if(!iConverter)
							{
								textToInsert=dispName;					
							}
							//else
							//{
							//	//textToInsert=iConverter->translateString(dispName);
							//	vectorHtmlTrackerSPTBPtr->clear();
							//	textToInsert=iConverter->translateStringNew(dispName, vectorHtmlTrackerSPTBPtr);
							//}

                            textToInsert.ParseForEmbeddedCharacters();
                            boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
							ReplaceText(textModel, tagStartPos, tagEndPos-tagStartPos + 1, insertText);

							//this->ToggleBold_or_Italic(textModel , vectorHtmlTrackerSPTBPtr, tagStartPos );

							double sectionID = -1;
							if(global_project_level == 3)
								sectionID = CurrentSubSectionID;
							if(global_project_level == 2)
								sectionID = CurrentSectionID;
							PMString attributeName;
							PMString attributeValue;
							
							attributeName = "parentID";
							attributeValue.AppendNumber(PMReal(pNode.getPubId()));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
							attributeName.Clear();
							attributeValue.Clear();

							attributeName = "parentTypeID";
							attributeValue.AppendNumber(PMReal(CurrentObjectTypeID));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
							attributeName.Clear();
							attributeValue.Clear();

							attributeName = "sectionID";
							attributeValue.AppendNumber(PMReal(sectionID));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
							attributeName.Clear();
							attributeValue.Clear();										
							
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("tableFlag"),WideString("-11"));

							 break;
						}

						if(tagInfo.typeId == -111 && tagInfo.parentId == -1 && tagInfo.whichTab == 3)
						{
							sprayCMedCustomTableScreenPrintInTabbedTextForm(tagInfo);
						}
						else if(tagInfo.parentId==-1 && tagInfo.whichTab == 3 && tagInfo.typeId == -112)
						{
							//CA("Spraying for Kit Table");
							XMLContentReference tagContentRef = tagInfo.tagPtr->GetContentReference();
							bool16 isTable = kFalse;
							if(tagContentRef != kInvalidXMLContentReference)
							{
								isTable =  tagContentRef.IsTable();
								double tableId=0;
								if(isTable)
								{
									isTable = kTrue;
									UIDRef TableUIDRef = tagContentRef.GetUIDRef();
									TableUtility oTableUtil(this);
									oTableUtil.fillDataInKitComponentTable(TableUIDRef, pNode, tagInfo, tableId,boxUIDRef, kFalse);
								}
								else
									isTable = kFalse;
									

							}
							if(isTable == kFalse)
								sprayProductKitComponentTableScreenPrintInTabbedTextForm(tagInfo, kFalse); // Spraying Componenet table
						}
						else if(tagInfo.parentId==-1 && tagInfo.whichTab == 3)
						{
							bool16 isTable = kFalse;
							if(pNode.getIsProduct() == 1)
							{																
								XMLContentReference tagContentRef = tagInfo.tagPtr->GetContentReference();
								if(tagContentRef != kInvalidXMLContentReference)
								{
									isTable =  tagContentRef.IsTable();
									double tableId=0;
									if(isTable)
									{
										isTable = kTrue;
										UIDRef TableUIDRef = tagContentRef.GetUIDRef();
										TableUtility oTableUtil(this);
										oTableUtil.fillDataInTable(TableUIDRef, pNode, tagInfo, tableId,boxUIDRef);
									}
									else
									{
										isTable = kFalse;
									}									

								}
								if(isTable == kFalse){
									sprayProductItemTableScreenPrintInTabbedTextForm(tagInfo);
								}
							}
						}
						break;
					}
					if(pNode.getIsProduct()== 0)
						makeTheTagImpotent(tagInfo.tagPtr);

					break;
				}
				
				//Items directly under section
				if(
						tagInfo.whichTab == 4 //For item__attributes
						&& tagInfo.typeId != -1 //table typeId  
						&& tagInfo.isTablePresent == kTrue//its a table screen preset
				)
				{
					if(tagInfo.isAutoResize == 1)
						CopyFlag=1;

					if(tagInfo.isAutoResize == 2)
						CopyFlag=2;

					if(pNode.getIsProduct() == 0) // Item is selected from the product dropdown in the productFinderPalette pluign
					{
						if(tagInfo.elementId == -103)
						{
							PMString dispName("");
							do
							{
								double typeId = tagInfo.typeId;
							
								VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
								if(typeValObj==NULL)
								{
									ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 0:StructureCache_getListTableTypes's typeValObj is NULL");																							
									break;
								}

								VectorTypeInfoValue::iterator it1;
								bool16 typeIDFound = kFalse;
								for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
								{	
									if(typeId == it1->getTypeId())
									{
										typeIDFound = kTrue;
										break;
									}			
								}
								if(typeValObj){
									typeValObj->clear();
									delete typeValObj;
								}
								if(typeIDFound)
									dispName = it1->getName();
							}while(kFalse);
							int32 tagStartPos = -1;
							int32 tagEndPos = -1;
							Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&tagStartPos,&tagEndPos);
							tagStartPos = tagStartPos + 1;
							tagEndPos = tagEndPos -1;

							PMString textToInsert("");
							//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
							//if(!iConverter)
							{
								textToInsert=dispName;					
							}
							//else
							//{
							//	//textToInsert=iConverter->translateString(dispName);
							//	vectorHtmlTrackerSPTBPtr->clear();
							//	textToInsert=iConverter->translateStringNew(dispName, vectorHtmlTrackerSPTBPtr);
							//}
                            
                            textToInsert.ParseForEmbeddedCharacters();
                            boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
							ReplaceText(textModel, tagStartPos, tagEndPos-tagStartPos + 1, insertText);

							//this->ToggleBold_or_Italic(textModel , vectorHtmlTrackerSPTBPtr, tagStartPos );
							double sectionID = -1;
							if(global_project_level == 3)
								sectionID = CurrentSubSectionID;
							if(global_project_level == 2)
								sectionID = CurrentSectionID;
							PMString attributeName;
							PMString attributeValue;
							
							attributeName = "parentID";
							attributeValue.AppendNumber(PMReal(pNode.getPubId()));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
							
							attributeName.Clear();
							attributeValue.Clear();
							attributeName = "parentTypeID";
							attributeValue.AppendNumber(PMReal(CurrentObjectTypeID));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
							
							attributeName.Clear();
							attributeValue.Clear();
							attributeName = "sectionID";
							attributeValue.AppendNumber(PMReal(sectionID));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));

							attributeName.Clear();
							attributeValue.Clear();
							attributeName = "pbObjectId";
							attributeValue.AppendNumber(PMReal(pNode.getPBObjectID()));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
							
							attributeName.Clear();
							attributeValue.Clear();
							attributeName = "rowno";
							attributeValue="-1";//attributeValue.AppendNumber(PMReal(pNode.getPBObjectID());
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
							
							attributeName.Clear();
							attributeValue.Clear();
							Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString("tableFlag"),WideString("-11"));
							break;
						}
						if((tagInfo.typeId == -112) || (tagInfo.typeId == -113) || (tagInfo.typeId == -114))// Item's Componenet table in Tabbed text.
						{
							//CA("Spraying items Component table in Tabbed text format");
							if((tagInfo.typeId == -112))
								sprayProductKitComponentTableScreenPrintInTabbedTextForm(tagInfo, kFalse); // kFalse for spraying component table
							if((tagInfo.typeId == -113))
								sprayXRefTableScreenPrintInTabbedTextForm(tagInfo); // kFalse for spraying XRef table
							if((tagInfo.typeId == -114))
								sprayAccessoryTableScreenPrintInTabbedTextForm(tagInfo); // kFalse for spraying component table

						}
						else // Normal Item's Table in Tabbed text 
							sprayItemItemTableScreenPrintInTabbedTextForm(tagInfo);
					}
					if(pNode.getIsProduct() == 1)
					{
						makeTheTagImpotent(tagInfo.tagPtr);
					}

					break;
				}
		
				if(
					tagInfo.whichTab == 4 //For item_copy_attributes
					&& (tagInfo.header == 1 || tagInfo.typeId != -1) //for item_copy_attributes' stencil the 				
				  )
				{
					if(tagInfo.isAutoResize == 1)
						CopyFlag=1;

					if(tagInfo.isAutoResize == 2)
						CopyFlag=2;

			
					PMString itemAttributeValue("");
					if(tagInfo.header == 1)
					{//if(tagInfo.typeId == -2)	
						
					/*	if(tagInfo.elementId == -701 || tagInfo.elementId == -702 )
						{
							int32 parentTypeID = tagInfo.parentTypeID; 
							
							VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
							if(TypeInfoVectorPtr != NULL)
							{
								VectorTypeInfoValue::iterator it3;
								int32 Type_id = -1;
								PMString temp = "";
								PMString name = "";
								for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
								{
									Type_id = it3->getTypeId();
									if(parentTypeID == Type_id)
									{
										temp = it3->getName();
										if(tagInfo.elementId == -701)
										{
											itemAttributeValue = temp;
										}
										else if(tagInfo.elementId == -702)
										{
											itemAttributeValue = temp + " Suffix";
										}
									}
								}

								TypeInfoVectorPtr->clear();
								delete TypeInfoVectorPtr;
							}
						}
						else if(tagInfo.elementId == -703)
						{
							itemAttributeValue = "$Off";
						}
						else if(tagInfo.elementId == -704)
						{
							itemAttributeValue = "%Off";
						}	
						else
						*/
						if(tagInfo.elementId == -805 || tagInfo.elementId == -806 || tagInfo.elementId == -807 || tagInfo.elementId == -808|| tagInfo.elementId == -809 || tagInfo.elementId == -810 || tagInfo.elementId == -811 || tagInfo.elementId == -812 || tagInfo.elementId == -813 || tagInfo.elementId == -814)
						{
							itemAttributeValue = tagInfo.tagPtr->GetAttributeValue(WideString("rowno"));
						}						
						else	
							itemAttributeValue = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(tagInfo.elementId,tagInfo.languageID );
						
					}//end table header
					else
					{
						double elementId = tagInfo.elementId;
								
					/*	if(elementId == -701 || elementId == -702 || elementId == -703 || elementId == -704)
						{	
							handleSprayingOfEventPriceRelatedAdditions(pNode,tagInfo,itemAttributeValue);
							//CA("itemAttributeValue" + itemAttributeValue);
						}
						else	*/
						{

							if(tagInfo.elementId == -803) //Letter  key
							{
								itemAttributeValue = tagInfo.tagPtr->GetAttributeValue(WideString("rowno"));
								if(itemAttributeValue== "-1")
										itemAttributeValue="";
								//CA(itemAttributeValue);
							}							
							else if(tagInfo.elementId == -804) // For SPraying New Indicator
							{
								if(pNode.getNewProduct()== 1)
									itemAttributeValue = "New";
								else
								{
									itemAttributeValue = "";
									deleteThisBox(boxUIDRef);
									return;
								}
							}
							else if(tagInfo.elementId == -805  || tagInfo.elementId == -806 || tagInfo.elementId == -807 || tagInfo.elementId == -808 || tagInfo.elementId == -809 || tagInfo.elementId == -810 || tagInfo.elementId == -811 || tagInfo.elementId == -812 || tagInfo.elementId == -813 || tagInfo.elementId == -814)
							{
								itemAttributeValue = tagInfo.tagPtr->GetAttributeValue(WideString("rowno"));
							}
							else if( (tagInfo.tagPtr->GetAttributeValue(WideString("rowno"))) == WideString("-NCitem"))
							{
								//CA("Coming to get Non Catalog Item details"); for Xref item table
								itemAttributeValue = ""; //ptrIAppFramework->GETItem_GetItemXrefAttributeDetailsByLanguageId(tagInfo.typeId, tagInfo.elementId, tagInfo.languageID);
							}
							else if(tagInfo.isEventField)
							{
								//CA("tagInfo.isEventField");
								/*VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(tagInfo.typeId,tagInfo.elementId,CurrentSubSectionID);
								if(vecPtr != NULL)
									if(vecPtr->size()> 0)
										itemAttributeValue = vecPtr->at(0).getObjectValue();*/	

								//CA("itemAttributeValue = " + itemAttributeValue);
							}
							else if(tagInfo.elementId == -401 || tagInfo.elementId == -402 || tagInfo.elementId == -403){
								itemAttributeValue = ""; //ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(tagInfo.typeId,tagInfo.elementId, tagInfo.languageID);
							}
							else if(tagInfo.elementId == -827) //**** Number Key
							{
								itemAttributeValue = tagInfo.tagPtr->GetAttributeValue(WideString("rowno"));
								if(itemAttributeValue== "-1")
										itemAttributeValue="";
								//CA("From Spray forthisBox: "+itemAttributeValue);
							}
							else
							{
								//CA("Spraying Normal item details ");
								itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,tagInfo.elementId, tagInfo.languageID, CurrentSectionID, kFalse );
							}
						}
						
					}
					int32 tagStartPos = -1;
					int32 tagEndPos = -1;
					Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&tagStartPos,&tagEndPos);
					tagStartPos = tagStartPos + 1;
					tagEndPos = tagEndPos -1;


					double sectionID = -1;
					if(global_project_level == 3)
						sectionID = CurrentSubSectionID;
					if(global_project_level == 2)
						sectionID = CurrentSectionID;
					
					PMString attributeName;
					PMString attributeValue;
							
					attributeName = "parentID";
					attributeValue.AppendNumber(PMReal(pNode.getPubId()));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();

					if(tagInfo.elementId != -701 && tagInfo.elementId != -702 && tagInfo.elementId != -703 && tagInfo.elementId != -704)
					{
						attributeName = "parentTypeID";
						attributeValue.AppendNumber(PMReal(CurrentObjectTypeID));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
						attributeName.Clear();
						attributeValue.Clear();
					}

					attributeName = "sectionID";
					attributeValue.AppendNumber(PMReal(sectionID));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();	

					if(pNode.getIsProduct() == 0)
					{
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString("tableFlag"),WideString("-1001"));

					}

					if(tagInfo.deleteIfEmpty == 1)
					{//if(tagInfo.rowno == -117)
						if(delStart == tagInfo.startIndex)
							delStart = tagStartPos-1;

						if(itemAttributeValue == "")							
							shouldDelete = kTrue;
					}

					if(shouldDelete == kTrue )
					{
						int32 tagStartPos1 = -1;
						int32 tagEndPos1 = -1;

						
						if(deleteCount >0)
							CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 
					
						deleteCount++;

						XMLReference perentXMLRef = tagInfo.tagPtr->GetParent();
						InterfacePtr<IIDXMLElement>perentXMLElement(perentXMLRef.Instantiate());
						if(!perentXMLElement)
							break;
						Utils<IXMLUtils>()->GetElementIndices(perentXMLElement,&tagStartPos1,&tagEndPos1);

						if(tagInfo.elementId != attributeId)
						{
							TextIterator begin1(textModel , tagStartPos);
							TextIterator end1(textModel , tagStartPos1);
							int32 removethis = 0;
							bool16 prevEnterFound = kFalse;
							for(TextIterator iter = begin1 ;iter > end1 ; iter--)
							{
								
								if(*iter == kTextChar_CR)
								{
									prevEnterFound = kTrue;
									break;
								}
								removethis++;									
							}
							if(prevEnterFound)
							{
								delStart = tagStartPos - removethis + 1;
							}
							else
							{
								delStart = tagStartPos1;
							}
						}
						else
						{
							TextIterator begin1(textModel , delStart);
							TextIterator end1(textModel , tagStartPos1);
							int32 removethis = 0;
							bool16 prevEnterFound = kFalse;
							for(TextIterator iter = begin1 ;iter > end1 ; iter--)
							{
								
								if(*iter == kTextChar_CR)
								{
									prevEnterFound = kTrue;
									break;
								}
								removethis++;
									
							}
							if(prevEnterFound)
							{
								delStart = delStart - removethis + 1;
							}
							else
							{
								delStart = tagStartPos1 ;
							}
						}

						TextIterator begin(textModel , delStart);
						TextIterator end(textModel , tagEndPos1);
						int32 addthis = 0;
						bool16 enterFound = kFalse;
						for(TextIterator iter = begin ;iter < end ; iter++)
						{
							addthis++;
							if(*iter == kTextChar_CR)
							{
								enterFound = kTrue;
								break;
							}

								
						}
						if(enterFound)
						{
							delEnd = delStart + addthis;
						}
						else
						{
							//CA("enterFound == kFalse 1111");
							delEnd = tagEndPos1 ;//-1
						}
					
						///find enter char before current tag 
						bool16 needToAddInVec = kTrue;
						if(tList.size() > 1){
							if(i == tList.size()-1){
								int32 tagStartindx = -1;
								int32 tagEndindx = -1;
								Utils<IXMLUtils>()->GetElementIndices(tList[tList.size()-2].tagPtr,&tagStartindx,&tagEndindx);
								
								if(tagStartindx >= delStart && tagEndindx <= delEnd){
									needToAddInVec = kFalse;
								}
							}	
							else if(i == 0){
								int32 tagStartindx = -1;
								int32 tagEndindx = -1;
								Utils<IXMLUtils>()->GetElementIndices(tList[1].tagPtr,&tagStartindx,&tagEndindx);
								if(tagStartindx >= delStart && tagEndindx <= delEnd){
									needToAddInVec = kFalse;
								}
							}
							else{
								int32 tagStartindx = -1;
								int32 tagEndindx = -1;
								Utils<IXMLUtils>()->GetElementIndices(tList[i-1].tagPtr,&tagStartindx,&tagEndindx);
								if(tagStartindx >= delStart && tagEndindx <= delEnd){
									needToAddInVec = kFalse;
								}
								if(needToAddInVec == kFalse){
									tagStartindx = -1;
									tagEndindx = -1;
									Utils<IXMLUtils>()->GetElementIndices(tList[i+1].tagPtr,&tagStartindx,&tagEndindx);
									if(tagStartindx >= delStart && tagEndindx <= delEnd){
										needToAddInVec = kFalse;
									}
								}
							}
						}
						if(needToAddInVec == kTrue){
							//CA("needToAddInVec == kTrue");

							const XMLReference & XMLElementOfTagToBeDeleted = tagInfo.tagPtr->GetXMLReference();
							ErrorCode err = Utils<IXMLElementCommands>()->DeleteElement(XMLElementOfTagToBeDeleted,kFalse);

                            TextIterator begin(textModel, delStart);
							TextIterator end(textModel, tagEndPos1);
							int32 addthis = 0;
							bool16 enterFound = kFalse;
							for(TextIterator iter = begin ;iter < end ; iter++)
							{
								addthis++;
								if(*iter == kTextChar_CR)
								{
									enterFound = kTrue;
									break;
								}									
							}
							if(enterFound)
							{
								delEnd = delStart 	+ addthis;
							}
							else
							{
								delEnd = tagEndPos1 ;
							}

							RangeData rangeData(delStart,delEnd);
							deleteTextRanges.push_back(rangeData);
							break;
						}

					}

					PMString textToInsert("");
					//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
					//if(!iConverter)
					{
						textToInsert=itemAttributeValue;					
					}
					//else						
					//{
					//	//textToInsert=iConverter->translateString(itemAttributeValue);
					//	vectorHtmlTrackerSPTBPtr->clear();
					//	textToInsert=iConverter->translateStringNew(itemAttributeValue, vectorHtmlTrackerSPTBPtr);
					//}

                    textToInsert.ParseForEmbeddedCharacters();
					boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
					ReplaceText(textModel, tagStartPos, tagEndPos-tagStartPos + 1, insertText);
					//this->ToggleBold_or_Italic(textModel , vectorHtmlTrackerSPTBPtr, tagStartPos );

					
					break;
				}		
				
				if(tagInfo.whichTab!=4)
				{	
					//CA("Going for fillDataInBox ");
					if(tagInfo.isAutoResize == 1)
					{
						CopyFlag=1;
					}

					if(tagInfo.isAutoResize == 2)
					{
						CopyFlag=2;
					}

					if(pNode.getIsProduct()== 1 || pNode.getIsProduct()== 2)
					{
						this->fillDataInBox(boxUIDRef, tagInfo, idList[tagInfo.whichTab-1], kFalse, IsFirstElement);//PG, PR, PF
                        if(isBoxdDeleted)
                            return;
                        
						if(shouldDelete == kTrue)
						{
					/*		int32 tagStartPos1 = -1;
							int32 tagEndPos1 = -1;
					
							XMLReference perentXMLRef = tagInfo.tagPtr->GetParent();
							InterfacePtr<IIDXMLElement>perentXMLElement(perentXMLRef.Instantiate());
							if(!perentXMLElement)
							{
								//CA("!perentXMLElement");
								break;
							}
							Utils<IXMLUtils>()->GetElementIndices(perentXMLElement,&tagStartPos1,&tagEndPos1);

							TextIterator begin(textModel , delEnd);
							TextIterator end(textModel , tagEndPos1 - 1);
							int32 addthis = 0;
							//bool16 enterFound = kFalse;
							for(TextIterator iter = begin ;iter <= end ; iter++)
							{
								addthis++;
								if(*iter == kTextChar_CR)
								{
									//enterFound = kTrue;
									break;
								}								
							}
							//if(enterFound == kTrue)
							delEnd = delEnd + addthis;
							//else
							//	delEnd = tagEndPos1;

							RangeData rangeData(delStart,delEnd);
							//ErrorCode err = textModel->Delete(rangeData);
							//if(err == kFailure)
							//	CA("Fail to delete");
							InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
    						if (!textModelCmds) {
								//CA("!textModelCmds");
								break;
							}
							InterfacePtr<ICommand> deteteCmd(textModelCmds->DeleteCmd (rangeData));
							if (!deteteCmd) {
								//CA("!deteteCmd");
								break;
							}
							ErrorCode err = CmdUtils::ProcessCommand(deteteCmd);
					*/
					}
						else
							setTagParent(tagInfo, idList[tagInfo.whichTab-1]);//Re-Tag the box  // Not reqire after MPV Image Handling

					}
					else if (pNode.getIsONEsource() && pNode.getIsProduct() == 0)
					{
						this->fillDataInBox(boxUIDRef, tagInfo, idList[tagInfo.whichTab-1], kFalse, IsFirstElement);//PG, PR, PF
                        if(isBoxdDeleted)
                            return;
						setTagParent(tagInfo, idList[tagInfo.whichTab-1]);//Re-Tag the box  // Not reqire after MPV Image Handling
				
					}
					else if (!pNode.getIsONEsource() && pNode.getIsProduct() == 0)
					{
						this->fillDataInBox(boxUIDRef, tagInfo, idList[tagInfo.whichTab-1], kFalse, IsFirstElement);
						if(isBoxdDeleted)
                            return;
						
					}
					else
					{
						//This is a special case.The tag is of product but the Item is
						//selected.So make this tag unproductive for refresh.(Impotent)
						makeTheTagImpotent(tagInfo.tagPtr);

					}
					
					IsFirstElement = kFalse;				
					
				}			
				
				if(tagInfo.whichTab == 4)
				{

					if(tagInfo.isAutoResize == 1)
						CopyFlag=1;

					if(tagInfo.isAutoResize == 2)
						CopyFlag=2;

					if(tagInfo.childId == 1)
					{//This is SectionLevelItem's Item tag
						makeTheTagImpotent(tagInfo.tagPtr);
						break;
					}

					InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
					if(ptrIAppFramework == NULL)
					{
						CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
						break;
					}

					PMString itemAttributeValue("");
					double typeId = -1;
					double parentId = pNode.getPubId();;
					PMString tableFlag = "-1";
					if(pNode.getIsProduct() == 0)
					{//Items directly under section

						double elementId = tagInfo.elementId;   // elementID  contains the id for event price tables now 
                        
                        /*if(elementId == -701 || elementId == -702 || elementId == -703 || elementId == -704)
						{
							handleSprayingOfEventPriceRelatedAdditions(pNode,tagInfo,itemAttributeValue);
						}*/
						if(elementId == -703 || elementId == -704)
						{
							handleSprayingOfEventPriceRelatedAdditionsNew(pNode,tagInfo,itemAttributeValue);
						}
						else if(tagInfo.elementId == -803) //Letter  key
						{
							itemAttributeValue = tagInfo.tagPtr->GetAttributeValue(WideString("rowno"));
							if(itemAttributeValue== "-1")
								itemAttributeValue="";
						}	
						else if(tagInfo.elementId == -804) // For SPraying New Indicator
						{
							if(pNode.getNewProduct() == 1)
								itemAttributeValue = "New";
							else
							{
								itemAttributeValue = "";
								deleteThisBox(boxUIDRef);
								return;
							}
						}
						else if(tagInfo.isEventField)
						{
							//CA("tagInfo.isEventField");
							/*VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(pNode.getPubId(),tagInfo.elementId,CurrentSubSectionID);
							if(vecPtr != NULL){
								if(vecPtr->size()> 0){
									itemAttributeValue = vecPtr->at(0).getObjectValue();	
								}
							}*/
							//CA("itemAttributeValue = " + itemAttributeValue);
						}
						else if(tagInfo.typeId == -1 && (tagInfo.elementId != -804))
						{
							if(tagInfo.elementId == -401 || tagInfo.elementId == -402 || tagInfo.elementId == -403){
								itemAttributeValue = ""; //??ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(pNode.getPubId(),tagInfo.elementId, tagInfo.languageID);
							}
							//else if(ptrIAppFramework->ATTRIBUTECache_isItemAttributeEventSpecific(tagInfo.elementId) == kTrue)
							//{
								/*VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(pNode.getPubId(),tagInfo.elementId,tagInfo.sectionID);
								if(vecPtr != NULL){
									if(vecPtr->size()> 0){
										itemAttributeValue = vecPtr->at(0).getObjectValue();
									}
								}*/
							//}
							else
							{
								itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(pNode.getPubId(),tagInfo.elementId, tagInfo.languageID, CurrentSectionID, kFalse );
							}

						}
						//if(tagInfo.elementId == -701)
						//{
						//	//CA("1.2");
						//	PMString attributeIDfromNotes("-1");
						//	bool8 isNotesValid =  getAttributeIDFromNotes(kFalse,-1,pNode.getPBObjectID(),pNode.getPubId(),attributeIDfromNotes);
						//	tagInfo.tagPtr->SetAttributeValue("ID",attributeIDfromNotes);
						//	tagInfo.elementId = attributeIDfromNotes.GetAsNumber();
						//	itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(pNode.getPubId(),tagInfo.elementId, tagInfo.languageID, kFalse );
						//	
						//}
						//else if(tagInfo.elementId == -702)
						//{

						//}
						//else if(tagInfo.elementId == -703)
						//{
						//	PMString attributeIDfromNotes("-1");
						//	bool8 isNotesValid =  getAttributeIDFromNotes(kFalse,-1,pNode.getPBObjectID(),pNode.getPubId(),attributeIDfromNotes);
						//	tagInfo.tagPtr->SetAttributeValue("ID",attributeIDfromNotes);
						//	tagInfo.elementId = attributeIDfromNotes.GetAsNumber();
						//	itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(pNode.getPubId(),tagInfo.elementId, tagInfo.languageID, kFalse );

						//	PMString retularPriceID = ptrIAppFramework->GetRegularPriceID();
						//	PMString regularPriceValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(pNode.getPubId(),retularPriceID, tagInfo.languageID, kFalse );

						//
						//}
						//else if(tagInfo.elementId == -704)
						//{
						//	PMString attributeIDfromNotes("-1");
						//	bool8 isNotesValid =  getAttributeIDFromNotes(kFalse,-1,pNode.getPBObjectID(),pNode.getPubId(),attributeIDfromNotes);
						//	tagInfo.tagPtr->SetAttributeValue("ID",attributeIDfromNotes);
						//	tagInfo.elementId = attributeIDfromNotes.GetAsNumber();
						//	itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(pNode.getPubId(),tagInfo.elementId, tagInfo.languageID, kFalse );

						//	PMString retularPriceID = ptrIAppFramework->GetRegularPriceID();
						//	PMString regularPriceValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(pNode.getPubId(),retularPriceID, tagInfo.languageID, kFalse );

						//}					
						//else

                        if(tagInfo.elementId == -827){ //**** Number Key
							itemAttributeValue = tagInfo.tagPtr->GetAttributeValue(WideString("rowno"));
							if(itemAttributeValue== "-1")
								itemAttributeValue="";
						}
						
						typeId = pNode.getPubId();					
						tableFlag = "-1001";

						
					}
					if(pNode.getIsProduct() == 1)
					{
						//This condition is bacause the Item has no items.Because if it has any items then
						//the typeId field would have been properly filled in arrangeForSprayingProductItemWithOtherCopyAttributes
						//Make the tag impotent.
						if(tagInfo.elementId == -803) //Letter  key
						{
							itemAttributeValue = tagInfo.tagPtr->GetAttributeValue(WideString("rowno"));
							if(itemAttributeValue== "-1")
								itemAttributeValue="";
						}
						else
						{

							makeTheTagImpotent(tagInfo.tagPtr);
							ptrIAppFramework->LogInfo("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 0:after makeTheTagImpotent");																							
							break;
						}		

                        if(tagInfo.elementId == -827) //*** Number Key
						{
							itemAttributeValue = tagInfo.tagPtr->GetAttributeValue(WideString("rowno"));
							if(itemAttributeValue== "-1")
								itemAttributeValue="";
						}
					}						

					int32 tagStartPos = -1;
					int32 tagEndPos = -1;
					Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&tagStartPos,&tagEndPos);
					tagStartPos = tagStartPos + 1;
					tagEndPos = tagEndPos -1;
					
					if(tagInfo.deleteIfEmpty == 1)
					{//if(tagInfo.rowno == -117)
						if(delStart == tagInfo.startIndex)
							delStart = tagStartPos-1;

						if(itemAttributeValue == "")						
							shouldDelete = kTrue;
					}

					InterfacePtr<ITextStoryThread> textStoryThread((tagInfo.tagPtr)->QueryContentTextStoryThread());
                    if(textStoryThread == NULL)
					{
						ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 0:textStoryThread is NULL");																							
						break;
					}
					InterfacePtr<ITextModel> textModel5(textStoryThread,UseDefaultIID());
					if(textModel5 == NULL)
					{
						ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 0:textModel is NULL");																							
						break;
					}

					if(shouldDelete == kTrue )
					{
						//CA("Going to delete 2");
					
						int32 tagStartPos1 = -1;
						int32 tagEndPos1 = -1;

						
						if(deleteCount >0)
							CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 
					
						deleteCount++;

						XMLReference perentXMLRef = tagInfo.tagPtr->GetParent();
						InterfacePtr<IIDXMLElement>perentXMLElement(perentXMLRef.Instantiate());
						if(!perentXMLElement)
							break;
						Utils<IXMLUtils>()->GetElementIndices(perentXMLElement,&tagStartPos1,&tagEndPos1);

						if(tagInfo.elementId != attributeId)
						{
							TextIterator begin1(textModel , tagStartPos);
							TextIterator end1(textModel , tagStartPos1);
							int32 removethis = 0;
							bool16 prevEnterFound = kFalse;
							for(TextIterator iter = begin1 ;iter > end1 ; iter--)
							{
								
								if(*iter == kTextChar_CR)
								{
									prevEnterFound = kTrue;
									break;
								}
								removethis++;									
							}
							if(prevEnterFound)
							{
								delStart = tagStartPos - removethis + 1;
							}
							else
							{
								delStart = tagStartPos1;
							}
						}
						else
						{
							TextIterator begin1(textModel , delStart);
							TextIterator end1(textModel , tagStartPos1);
							int32 removethis = 0;
							bool16 prevEnterFound = kFalse;
							for(TextIterator iter = begin1 ;iter > end1 ; iter--)
							{
								
								if(*iter == kTextChar_CR)
								{
									prevEnterFound = kTrue;
									break;
								}
								removethis++;
									
							}
							if(prevEnterFound)
							{
								delStart = delStart - removethis + 1;
							}
							else
							{
								delStart = tagStartPos1 ;
							}
						}

						TextIterator begin(textModel , delStart);
						TextIterator end(textModel , tagEndPos1);
						int32 addthis = 0;
						bool16 enterFound = kFalse;
						for(TextIterator iter = begin ;iter < end ; iter++)
						{
							addthis++;
							if(*iter == kTextChar_CR)
							{
								enterFound = kTrue;
								break;
							}

								
						}
						if(enterFound)
						{
							delEnd = delStart + addthis ;
						}
						else
						{
							delEnd = tagEndPos1;//-1
						}
					
						///find enter char before current tag 
						bool16 needToAddInVec = kTrue;
						if(tList.size() > 1){
							if(i == tList.size()-1){
								int32 tagStartindx = -1;
								int32 tagEndindx = -1;
								Utils<IXMLUtils>()->GetElementIndices(tList[tList.size()-2].tagPtr,&tagStartindx,&tagEndindx);
								
								if(tagStartindx >= delStart && tagEndindx <= delEnd){
									needToAddInVec = kFalse;
								}
							}	
							else if(i == 0){
								int32 tagStartindx = -1;
								int32 tagEndindx = -1;
								Utils<IXMLUtils>()->GetElementIndices(tList[1].tagPtr,&tagStartindx,&tagEndindx);
								if(tagStartindx >= delStart && tagEndindx <= delEnd){
									needToAddInVec = kFalse;
								}
							}
							else{
								int32 tagStartindx = -1;
								int32 tagEndindx = -1;
								Utils<IXMLUtils>()->GetElementIndices(tList[i-1].tagPtr,&tagStartindx,&tagEndindx);
								if(tagStartindx >= delStart && tagEndindx <= delEnd){
									needToAddInVec = kFalse;
								}
								if(needToAddInVec == kFalse){
									tagStartindx = -1;
									tagEndindx = -1;
									Utils<IXMLUtils>()->GetElementIndices(tList[i+1].tagPtr,&tagStartindx,&tagEndindx);
									if(tagStartindx >= delStart && tagEndindx <= delEnd){
										needToAddInVec = kFalse;
									}
								}
							}
						}
						if(needToAddInVec == kTrue){
							const XMLReference & XMLElementOfTagToBeDeleted = tagInfo.tagPtr->GetXMLReference();
							ErrorCode err = Utils<IXMLElementCommands>()->DeleteElement(XMLElementOfTagToBeDeleted,kFalse);

                            TextIterator begin(textModel, delStart);
							TextIterator end(textModel, tagEndPos1);
							int32 addthis = 0;
							bool16 enterFound = kFalse;
							for(TextIterator iter = begin ;iter < end ; iter++)
							{
								addthis++;
								if(*iter == kTextChar_CR)
								{
									enterFound = kTrue;
									break;
								}									
							}
							if(enterFound)
							{
								delEnd = delStart + addthis ;
							}
							else
							{
								delEnd = tagEndPos1 ;
							}

							RangeData rangeData(delStart,delEnd);
							deleteTextRanges.push_back(rangeData);
							break;
						}

					}

					PMString textToInsert("");
					//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
					//if(!iConverter)
					{
						textToInsert=itemAttributeValue;					
					}
					//else
					//	/*textToInsert=iConverter->translateString(itemAttributeValue);*/
                    //		{
					//	//textToInsert=iConverter->translateString(itemAttributeValue);
					//	vectorHtmlTrackerSPTBPtr->clear();
					//	textToInsert=iConverter->translateStringNew(itemAttributeValue, vectorHtmlTrackerSPTBPtr);
					//}

                    textToInsert.ParseForEmbeddedCharacters();
					boost::shared_ptr<WideString> insertText(new WideString(textToInsert));

					ReplaceText(textModel, tagStartPos, tagEndPos-tagStartPos + 1, insertText);

					//this->ToggleBold_or_Italic(textModel5 , vectorHtmlTrackerSPTBPtr, tagStartPos );

					double sectionID = -1;
					if(global_project_level == 3)
						sectionID = CurrentSubSectionID;
					if(global_project_level == 2)
						sectionID = CurrentSectionID;
					PMString attributeName;
					PMString attributeValue;

					attributeName = "typeId";
					attributeValue.AppendNumber(PMReal(typeId));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();
					
					attributeName = "parentID";
					attributeValue.AppendNumber(PMReal(parentId));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();


					if(tagInfo.elementId != -701 && tagInfo.elementId != -702 && tagInfo.elementId != -703 && tagInfo.elementId != -704)
					{
						attributeName = "parentTypeID";
						attributeValue.AppendNumber(PMReal(CurrentObjectTypeID));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
						attributeName.Clear();
						attributeValue.Clear();
					}
					
					attributeName = "sectionID";
					attributeValue.AppendNumber(PMReal(sectionID));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();	
					
					attributeName = "tableFlag";
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(tableFlag));
					attributeName.Clear();
					attributeValue.Clear();

				}

				break;
			}//*** End Of Case 0:

		
            case 1:
			{
				bool16 makeImageFitProportionally = kFalse;

				if(tagInfo.typeId == -207 || tagInfo.typeId == -208 || tagInfo.typeId == -209|| tagInfo.typeId == -210 
					|| tagInfo.typeId == -211 || tagInfo.typeId == -212 || tagInfo.typeId == -213 || tagInfo.typeId == -214
					|| tagInfo.typeId == -215 || tagInfo.typeId == -216 || tagInfo.typeId == -217 || tagInfo.typeId == -218
					|| tagInfo.typeId == -219 || tagInfo.typeId == -220 || tagInfo.typeId == -221 || tagInfo.typeId == -222)
				{
					//CA("going for spraying image types Brand , Manufacture , Supplier");
					this->fillBMSImageInBox(boxUIDRef, tagInfo, pNode.getPubId());
					break;
				}
				if(tagInfo.header == 1)
				{	//CA("tagInfo.elementId == -122");
					if(tagInfo.isAutoResize == 1)
						CopyFlag=1;

					if(tagInfo.isAutoResize == 2)
						CopyFlag=2;

					PMString textToInsert("");				
						
					double sectionid = -1;
					if(global_project_level == 3)
						sectionid = CurrentSubSectionID;
					if(global_project_level == 2)
						sectionid = CurrentSectionID;

					if( tagInfo.elementId != -1  && tagInfo.typeId != -1 )
					{
						//PicklistGroup pickListGroupObj = ptrIAppFramework->StructureCache_getPickListGroups(tagInfo.typeId);
						//textToInsert = pickListGroupObj.getName();
                        bool16 isForTable = kFalse;
                        bool16 isSprayText = kTrue;
                        fillPVAndMPVImageInBox(boxUIDRef, tagInfo, pNode.getPubId(), isForTable, isSprayText);
                        break;
					}
                    else if(tagInfo.elementId != -1 && tagInfo.typeId == -1 && tagInfo.whichTab == 4)
                    {
                        textToInsert = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(tagInfo.elementId,tagInfo.languageID );;
                    }
					else if(tagInfo.whichTab == 3)
					{
						textToInsert = ptrIAppFramework->StructureCache_TYPECACHE_getTypeNameById(tagInfo.typeId);
					}
					else if(tagInfo.whichTab == 4)
					{
						textToInsert = ptrIAppFramework->StructureCache_TYPECACHE_getTypeNameById(tagInfo.typeId);
					}

					TextIndex startPos=-1;
					TextIndex endPos = -1;

					int32 tagStartPos = 0;
					int32 tagEndPos =0; 			
					Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&tagStartPos,&tagEndPos);
					tagStartPos = tagStartPos + 1;
					tagEndPos = tagEndPos -1;

                    textToInsert.ParseForEmbeddedCharacters();
                    boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
					ReplaceText(textModel, tagStartPos,tagEndPos , insertText);

					PMString attributeName;
					PMString attributeValue;
					
					attributeName = "parentID";
					if(tagInfo.whichTab == 3)
						attributeValue.AppendNumber(PMReal(pNode.getPubId()));
					if(tagInfo.whichTab == 4)
						attributeValue.AppendNumber(PMReal(pNode.getPubId()));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();
					
					attributeName = "parentTypeID";
					attributeValue.AppendNumber(PMReal(CurrentObjectTypeID));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();				

					attributeName = "sectionID";
					attributeValue.AppendNumber(PMReal(sectionid));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();	
					break;
				}

				if(tagInfo.dataType == 2)
				{
					if(tagInfo.isAutoResize == 1)
						CopyFlag=1;

					if(tagInfo.isAutoResize == 2)
						CopyFlag=2;

					PMString textToInsert("");				
						
					double sectionid = -1;
					if(global_project_level == 3)
						sectionid = CurrentSubSectionID;
					if(global_project_level == 2)
						sectionid = CurrentSectionID;

					if( tagInfo.elementId != -1 && tagInfo.typeId != -1  )
					{
						//PicklistGroup pickListGroupObj = ptrIAppFramework->StructureCache_getPickListGroups(tagInfo.typeId);
						//textToInsert = pickListGroupObj.getDescription();
                        bool16 isForTable = kFalse;
                        bool16 isSprayText = kTrue;
                        fillPVAndMPVImageInBox(boxUIDRef, tagInfo, pNode.getPubId(), isForTable, isSprayText);
                        break;
					}
                    else if( tagInfo.elementId != -1 && tagInfo.whichTab == 4 && tagInfo.typeId == -1 ) // Image Attribute ASSET
                    {
                        textToInsert = "";
                    }
					else if(tagInfo.whichTab == 3)
					{
						CObjectValue oVal;
						oVal=ptrIAppFramework->GETProduct_getObjectElementValue(pNode.getPubId(), CurrentSectionID);  // new added in appFramework
						double parentTypeID= oVal.getObject_type_id();
						VectorAssetValuePtr AssetValuePtrObj = NULL;
						int32 isProduct =1;

						if(tagInfo.typeId == -98 || tagInfo.typeId == -96 )
						{
							AssetValuePtrObj=ptrIAppFramework->GETAssets_GetAssetByParentAndType(pNode.getPubId(), CurrentSectionID, isProduct);
						}
						else
						{
							AssetValuePtrObj = ptrIAppFramework->GETAssets_GetAssetByParentAndType(pNode.getPubId(), CurrentSectionID, isProduct, tagInfo.typeId);
						}
						if(AssetValuePtrObj == NULL)
						{
							ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::AssetValuePtrObj == NULL");
							break;
						}

						if(AssetValuePtrObj->size() ==0)
						{
							ptrIAppFramework->LogInfo("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::AssetValuePtrObj->size() ==0");
							delete AssetValuePtrObj;
							break;
						}

						VectorAssetValue::iterator it; // iterator of Asset value	

						CAssetValue objCAssetvalue;
						for(it = AssetValuePtrObj->begin();it!=AssetValuePtrObj->end();it++)
						{
							objCAssetvalue = *it;
							textToInsert = objCAssetvalue.getDescription();
							break;
						}

						AssetValuePtrObj->clear();
						delete AssetValuePtrObj;
					}
					else if(tagInfo.whichTab == 4)
					{
						textToInsert = ptrIAppFramework->StructureCache_TYPECACHE_getTypeNameById(tagInfo.typeId);

						VectorAssetValuePtr AssetValuePtrObj = NULL;


						tagInfo.parentId=pNode.getPubId();
						tagInfo.parentTypeID = pNode.getTypeId(); //ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
						tagInfo.sectionID=CurrentSubSectionID;
						int32 isProduct =0;

						if(tagInfo.typeId != -99)
							AssetValuePtrObj= ptrIAppFramework->GETAssets_GetAssetByParentAndType(/*itemID*/pNode.getPubId(), CurrentSubSectionID, isProduct, tagInfo.typeId);
						else
							AssetValuePtrObj= ptrIAppFramework->GETAssets_GetAssetByParentAndType(/*itemID*/pNode.getPubId(), CurrentSubSectionID, isProduct);

							
						if(AssetValuePtrObj == NULL)
						{
							//CA("AssetValuePtrObj == NULL");
							continue ;
						}

						if(AssetValuePtrObj->size() == 0)
						{
							delete AssetValuePtrObj;
							continue;
						}
							
						VectorAssetValue::iterator it1; // iterator of Asset value

						CAssetValue objCAssetvalue;
						double tempTypeid = -1;

						for(it1 = AssetValuePtrObj->begin();it1!=AssetValuePtrObj->end();it1++)
						{
							objCAssetvalue = *it1;
							textToInsert = objCAssetvalue.getDescription();
							break;
						}

						AssetValuePtrObj->clear();
						delete AssetValuePtrObj;
					}

					TextIndex startPos=-1;
					TextIndex endPos = -1;

					int32 tagStartPos = 0;
					int32 tagEndPos =0; 			
					Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&tagStartPos,&tagEndPos);
					tagStartPos = tagStartPos + 1;
					tagEndPos = tagEndPos -1;

                    textToInsert.ParseForEmbeddedCharacters();
                    boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
					ReplaceText(textModel, tagStartPos,tagEndPos, insertText);

					PMString attributeName;
					PMString attributeValue;
					
					attributeName = "parentID";
					if(tagInfo.whichTab == 3)
						attributeValue.AppendNumber(PMReal(pNode.getPubId()));
					if(tagInfo.whichTab == 4)
						attributeValue.AppendNumber(PMReal(pNode.getPubId()));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();
					
					attributeName = "parentTypeID";
					attributeValue.AppendNumber(PMReal(CurrentObjectTypeID));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();				

					attributeName = "sectionID";
					attributeValue.AppendNumber(PMReal(sectionid));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString(attributeName),WideString(attributeValue));
					attributeName.Clear();
					attributeValue.Clear();	
					break;
				}
				else if(tagInfo.elementId != -1 && tagInfo.typeId != -1)
				{
					//CA("PV/MPV Image");
					fillPVAndMPVImageInBox(boxUIDRef, tagInfo, pNode.getPubId());
					fitInlineImageInBoxInsideTableCell(boxUIDRef);
					break;
				}
				else if(tagInfo.whichTab == 5 )
				{
					//CA("Spraying Category Images");				
					this->fillImageInBoxForCategoryImages(boxUIDRef, tagInfo, pNode.getPubId());
					break;
				}
				else if(tagInfo.whichTab!=4 &&(pNode.getIsProduct()== 1))
				{
					this->fillImageInBox(boxUIDRef, tagInfo, idList[tagInfo.whichTab-1]);

					setTagParent(tagInfo, idList[tagInfo.whichTab-1]);
					isInlineImage = kFalse;
				}			
				else
				{	
					//CA("else tagInfo.whichTab!=4 &&(pNode.getIsProduct()== kTrue)");					
					vector<double> tempvector;
					//CA("Spraying Item Images");

					double ObjectID;
					InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
					
					bool16 isCallFromTS = kFalse;
					if(ptrTableSourceHelper != nil)
					{
						TableSourceInfoValue *tableSourceInfoValueObj = NULL;
						tableSourceInfoValueObj=ptrTableSourceHelper->getTableSourceInfoValueObj();
						if(tableSourceInfoValueObj)
						{
							//CA("1");
							isCallFromTS = tableSourceInfoValueObj->getIsCallFromTABLEsource();
							if(isCallFromTS )	
							{
								ObjectID = idList[2];
							}
						}
					}

					if(!isCallFromTS )
					{
						ObjectID = idList[2];    // getting Product ID....
					}

					InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
					if(ptrIAppFramework == NULL)
					{
						//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
						break;
					}

					VectorLongIntPtr ItemIDInfo = NULL;

					if(pNode.getIsProduct() == 1)
					{
						if(tagInfo.parentTypeID != -1)
						{
							tempvector.clear();
							tempvector.push_back(tagInfo.parentTypeID);						
							ItemIDInfo = &tempvector;
						}
						else if(pNode.getIsONEsource() && (tagInfo.parentId == -1))
						{
							//CA("pNode.getIsONEsource() && (tagInfo.parentId == -1)");
							///If ONEsource mode  is selected and Table stencils for item is selected.Then to get all information of table.
							//ItemIDInfo=ptrIAppFramework->GETProduct_getAllItemIDsFromTables(ObjectID);
						}
						else if((!pNode.getIsONEsource()) &&  (tagInfo.parentId == -1))
						{
							//If publication mode is selected.
							ItemIDInfo=ptrIAppFramework->GETProjectProduct_getAllItemIDsFromTables(ObjectID, CurrentSectionID);

						}
						if(!ItemIDInfo)
						{	
							ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:ItemIDInfo is NULL");
							return;
						}
						
						if(ItemIDInfo->size()==0)
						{
							ptrIAppFramework->LogInfo("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:ItemIDInfo->size()==0");
							break;
						}
					}
					if(pNode.getIsProduct() == 0)
					{
						if(tagInfo.parentTypeID != -1)
						{
							tempvector.clear();
							tempvector.push_back(tagInfo.parentTypeID);						
							ItemIDInfo = &tempvector;
						}
						else
						{
							//CA("Normal case of Item Image spray");
							tempvector.clear();
							tempvector.push_back(pNode.getPubId());
							ItemIDInfo = &tempvector;
						}
					}
					if(pNode.getIsProduct() == 2)
					{
						return;
					}

					InterfacePtr<IHierarchy> iHier(boxUIDRef, UseDefaultIID());
					if(!iHier)
					{
						ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:!iHier");				
						break;
					}
					
					UIDRef parentUIDRef(boxUIDRef.GetDataBase(), iHier->GetParentUID());
					InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
					if(!iSelectionManager)
					{
						//CA("NULL");
					}

					if (iSelectionManager->SelectionExists (kInvalidClass, ISelectionManager::kAnySelection)) {
						// Clear the selection
						iSelectionManager->DeselectAll(nil);
					}
			
					InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
					if (!layoutSelectionSuite) 
					{	//CA("!layoutSelectionSuite");
						return;
					}

					layoutSelectionSuite->DeselectAllPageItems();
					UIDList selectUIDList(boxUIDRef);
					layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);

					InterfacePtr<IGeometry> iGeo(boxUIDRef, IID_IGEOMETRY);
					if(!iGeo)
					{
						ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:!iGeo");								
						break;
					}

					PMRect pageBounds=iGeo->GetStrokeBoundingBox(InnerToPasteboardMatrix(iGeo));
					PMRect originalPageBounds;
					PMRect FirstFrameBounds = pageBounds;
					VectorLongIntValue::iterator it;
					PMReal margin = 0;
					PMReal newMargin = 0;
					PMString TagName("");
					int32 Flag=0;
					
					bool16 ItemImageUnspreadFlag = kFalse;
					bool16 isFirstImageSprayed = kFalse;
					tagInfo.parentTypeID = pNode.getTypeId(); //ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");

					int32 Index =0;
					bool16 flagfornewpage = kFalse;

			
					for(it=ItemIDInfo->begin(); it!=ItemIDInfo->end(); it++,Index++)
					{	
LABEL:
						if(Index > ItemIDInfo->size())
						{
							break;
						}
						
						if(ItemIDInfo->size()>1 && getItemSprayOverflowFlag() == 0 && tagInfo.tableType == -1 )
						{
							this->setItemSprayOverflowFlag(1);
							break;
						}
						UIDRef newItem;
						tagInfo.parentId=(*it);
						
						tagInfo.parentTypeID = pNode.getTypeId(); //ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
						
						PMString id("parentId = ");
						id.AppendNumber(PMReal(tagInfo.parentId));
                        tagInfo.sectionID=CurrentSubSectionID;

						XMLReference tagRef = tagInfo.tagPtr->GetXMLReference();
						XMLTagAttributeValue tagVal;

						convertTagStructToXMLTagAttributeValue(tagInfo,tagVal);
						InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
						if(ptrIClientOptions==NULL)
						{
							ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:!ptrIClientOptions");
							break;
						}

						imagePath=ptrIClientOptions->getImageDownloadPath();
                        ptrIAppFramework->LogDebug("imagePath1 - " + imagePath);
						if(imagePath=="")
						{
							ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:imagePath == blank");
							break;
						}

						if(imagePath!="")
						{
							const char *imageP=imagePath.GetPlatformString().c_str();
							if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
							#ifdef MACINTOSH
								imagePath+="/";
							#else
								imagePath+="\\";
							#endif
						}
                        ptrIAppFramework->LogDebug("imagePath2 - " + imagePath);

						PMString fileName("");
						double assetID;
						double mpv_value_id = -1;

						VectorAssetValuePtr AssetValuePtrObj = NULL;
						if(tagInfo.typeId == -99 || tagInfo.typeId == -97 )
						{
							int32 isProduct =0; // for Item images
                            AssetValuePtrObj= ptrIAppFramework->GETAssets_GetAssetByParentAndType(tagInfo.parentId, tagInfo.sectionID, isProduct);
						}
                        else if((tagInfo.typeId == -1 && tagInfo.elementId != -1)) //Item Asset Attribute
                        {
                            PMString assetIdStr("");
                            assetIdStr = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.parentId,tagInfo.elementId , tagInfo.languageID, CurrentSectionID, kFalse );
                            
                            if(assetIdStr == "")
                            {
                                ItemImageUnspreadFlag = kTrue;
                                continue;
                            }
                            
                            double assetId = assetIdStr.GetAsDouble();
                            int32 isProduct =0;// for Item images
                            AssetValuePtrObj= ptrIAppFramework->GETAssets_GetAssetByParentAndAssetId(tagInfo.parentId, tagInfo.sectionID, isProduct, assetId);
                        }
						else
						{
							int32 isProduct =0; // for Item images
							AssetValuePtrObj= ptrIAppFramework->GETAssets_GetAssetByParentAndType(tagInfo.parentId, tagInfo.sectionID, isProduct, tagInfo.typeId);												
						}
				
		
						if(AssetValuePtrObj == NULL || AssetValuePtrObj->size() == 0)
						{
							ItemImageUnspreadFlag = kTrue;
							PMString attribValue;
							attribValue.AppendNumber(PMReal( tagInfo.parentId));
							
							if(tagInfo.tagPtr)
							{	
								TagName = tagInfo.tagPtr->GetTagString();
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString("parentID"),WideString(attribValue));//ObjectID
								
								attribValue.Clear();
								attribValue.AppendNumber(PMReal(CurrentSubSectionID));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString("sectionID"),WideString(attribValue));//Publication ID								
								
								attribValue.Clear();
								attribValue.AppendNumber(PMReal(tagInfo.parentTypeID));

								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString("parentTypeID"),WideString(attribValue));//Parent Type ID
								
								attribValue.Clear();
								attribValue.AppendNumber(-1);
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString("rowno"),WideString(attribValue));//Parent Type ID
								
								attribValue.Clear();
								attribValue.AppendNumber(PMReal(pNode.getPBObjectID()));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString("pbObjectId"),WideString(attribValue));
							}
							continue ;
						}
                        
						if(AssetValuePtrObj->size() == 0)
						{
							ItemImageUnspreadFlag = kTrue;
							delete AssetValuePtrObj;
							continue;
						}
		
						VectorAssetValue::iterator it1;
						VectorAssetValue::iterator it2ForInline;

						CAssetValue objCAssetvalue;
						double tempTypeid = -1;

						UIDList imageFramUidList  ;
						vector <UIDRef> vecAssetUidRef;
						
						CAssetValue objCAssetvalueForInline;
						it2ForInline = AssetValuePtrObj->begin();
						bool16 isBreakFromMultipleImagespray=kFalse;

					int32 checkIfDivArray = 0;
					int32 divImageIndex = 0;

					for(;divImageIndex < 2 ;)
					{
						for(it1 = AssetValuePtrObj->begin();it1!=AssetValuePtrObj->end();it1++)
						{
							objCAssetvalue = *it1;
							fileName = objCAssetvalue.geturl();
							assetID = objCAssetvalue.getAsset_id();
											
							double typeId = -1;
							if(tagInfo.typeId == -97)
                            {
								typeId = objCAssetvalue.getType_id();
								tempTypeid = typeId;
							}
							else
							{
								typeId = tagInfo.typeId;
								tempTypeid = typeId;
							}

							if(ptrIAppFramework->getSelectedCustPVID(1) != -1)
							{
								if(CutomerAssetTypes.Compare(kFalse,"") != 0)
								{
									PMString temp("");
									temp.AppendNumber(PMReal(typeId));

									if(CutomerAssetTypes.Contains(temp)==kTrue)	
									{
										if(mpv_value_id != ptrIAppFramework->getSelectedCustPVID(1))
										{
											//CA("mpv_value_id != ptrIAppFramework->getSelectedCustPVID(1)");
											continue;													
										}
									}
								}
							}
							if(ptrIAppFramework->getSelectedCustPVID(0) != -1)
								if(DivisionAssetTypes.Compare(kFalse,"") != 0)
								{
									PMString temp("");
									temp.AppendNumber(PMReal(typeId));
									if(DivisionAssetTypes.Contains(temp)==kTrue)			
										if(mpv_value_id != ptrIAppFramework->getSelectedCustPVID(0))
										{
											//CA("mpv_value_id != ptrIAppFramework->getSelectedCustPVID(0)");
											continue;													
										}
								}

							if(fileName == "")
								continue;

							UIDRef newItem;	
							do
							{
								SDKUtilities::Replace(fileName,"%20"," ");
							}while(fileName.IndexOfString("%20") != -1);

							//CA("fileName : "+ fileName);
						
							PMString imagePathWithSubdir = imagePath;
							PMString typeCodeString("");
						
							PMString total;
                            
                            total=imagePathWithSubdir+fileName;
                            
                            #ifdef MACINTOSH
                                SDKUtilities::convertToMacPath(total);
                            #endif

                            if(!fileExists(imagePathWithSubdir,fileName))
                            {
                                ptrIAppFramework->LogError("CDataSprayer:: fillImageInBox: Image Not Found on- " + total );
                                // continue;
                            }
                            

							ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1: Image Path = " + total);
					
							InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
							if (layoutData == nil)
								break;

							layoutData->SetFit(ILayoutControlData::kFitPage);

							IDocument* document = layoutData->GetDocument();
							if (document == NULL)
								break;

							IDataBase* database = ::GetDataBase(document);
							if(!database)
								break;

							UID pageUID = layoutData->GetPage();
							if(pageUID == kInvalidUID)
								break;

							bool16 result1 = kFalse;
							ThreadTextFrame ThreadObj;
							PMRect marginBoxBounds;
							UIDRef pageRef, originalSpreadUIDRef;
							result1 = ThreadObj.getCurrentPage(pageRef, originalSpreadUIDRef);
							if(result1 == kFalse){
								ptrIAppFramework->LogError("!getCurrentPage");
								return;
							}

							result1 = getMarginBounds(pageRef, marginBoxBounds);
							if(result1 == kFalse)
							{
								result1 = getPageBounds(pageRef, marginBoxBounds);
								if(result1 == kFalse)
								{
									return;
								}
							}
							
							if(FirstFrameBounds.Left() > marginBoxBounds.Right() )
							{
								////CA("Got wrong Page UID");
								UIDRef ref = layoutData->GetSpreadRef();
								InterfacePtr<ISpread> iSpread(ref, UseDefaultIID());
								if (iSpread == nil)
									break;

								int numPages=iSpread->GetNumPages();
								
								pageUID= iSpread->GetNthPageUID(numPages-1);

								UIDRef pageRef(database, pageUID);
								ThreadObj .getMarginBounds (pageRef,marginBoxBounds);
							}						
			
							if(margin != 0)
							{
								//CA("margin != 0 ");
								if(isInlineImage)
								{	

										it2ForInline = AssetValuePtrObj->begin();
										it2ForInline++;
										
										int32 index=1;
										vector<UIDRef>::iterator imgUIDRefItr;
										imgUIDRefItr = vecAssetUidRef.begin();
																										
										for(int32 noOfTimes=static_cast<int32>(vecAssetUidRef.size() -1) ;noOfTimes > 0;noOfTimes--)
										{
											//CA("In Import Looop");
											UIDRef  assetItem= vecAssetUidRef[index++];	
												
											//*****To Get the New Asset PAth *********										
											if( noOfTimes != vecAssetUidRef.size() -1)
											{
												if(it1!=AssetValuePtrObj->end()){
													it2ForInline++;
													objCAssetvalueForInline = (*it2ForInline);
													fileName = objCAssetvalueForInline.geturl();
													double nxtassetID = objCAssetvalueForInline.getAsset_id();

													if(fileName=="")
														continue;

													if(!fileExists(imagePathWithSubdir,fileName))
													{	
														//CA("!fileExists(imagePathWithSubdir,fileName)");
														continue;
													}
													PMString newtotal=imagePathWithSubdir+fileName;	
													total = newtotal;
                                                    
                                                    #ifdef MACINTOSH
                                                        SDKUtilities::convertToMacPath(total);
                                                    #endif
												}																				
											}
											ImportFileInFrame(assetItem,total);
                                                
										}
										//CA("Breaking Asset isInlineImage");
										isInlineImage = kFalse;
										break;
								}																	
								else
								{	
									if(tagInfo.flowDir == 0)
									{
										if(pageBounds.Bottom() > marginBoxBounds.Bottom())
										{
											//if(0)
											{
												int32 numPages = 1;
												
												UIDRef ref = layoutData->GetSpreadRef();
												InterfacePtr<ISpread> iSpread(ref, UseDefaultIID());
												if (iSpread == nil)
													break;

												int numPages1=iSpread->GetNumPages();
												
												pageUID= iSpread->GetNthPageUID(numPages1-1);

												UIDRef pageRef(database, pageUID);

												InterfacePtr<ICommand> newPageCmd(CmdUtils::CreateCommand(kNewPageCmdBoss)); 
												if(newPageCmd == nil)
												{
													return ;
												}
												InterfacePtr<IPageCmdData> pageCmdData(newPageCmd, UseDefaultIID());
												if(pageCmdData == nil)
												{
												//	CA("pageCmdData == nil");
													return;
												}
											
												pageCmdData->SetNewPageCmdData
												( 
													ref, 
													1, 
													1 
												);
											
												InterfacePtr<IMasterSpreadList> iMasterSpreadList(document,UseDefaultIID());
												if(iMasterSpreadList == nil)
												{
													//CA("iMasterSpreadList == nil");
													return;
												}
												UID masterSpreadUID = iMasterSpreadList->GetNthMasterSpreadUID(0);

												UIDRef masterSpreadUIDRef(database,masterSpreadUID);

												InterfacePtr<ICommand> command(CmdUtils::CreateCommand( kApplyMasterSpreadCmdBoss)); 
												if(command == nil)
												{
													//CA("command == nil");
													return;
												}
												
												UIDList pageUIDList(database);
												pageUIDList.Insert(pageUID);
												command->SetItemList(pageUIDList);

												InterfacePtr<IApplyMasterCmdData>applyMasterCmd(command,UseDefaultIID());
												if(applyMasterCmd == nil)
												{
													//CA("applyMasterCmd == nil");
													return;
												}
												applyMasterCmd->SetApplyMasterCmdData(masterSpreadUIDRef , IApplyMasterCmdData::kKeepCurrentPageSize);
												ErrorCode status = CmdUtils::ProcessCommand(newPageCmd); 
											
												if(status == kFailure)
												{
													//CA("Page can not be created!!!");
													return;
												}
											}
											flagfornewpage = kTrue;
                                            pageBounds.Top(/*pageBounds.Top());*/2);
                                            pageBounds.Bottom(10);//+2+15);
                                            pageBounds.Left(2);//15);//(pageBounds.Left()+2+margin)
                                            pageBounds.Right(10);//15);//(pageBounds.Right()+2+margin)
											FirstFrameBounds = pageBounds;
											margin = 0;
											newMargin = 0;
											
											break;
											goto LABEL;
											
										}
										pageBounds.Top(pageBounds.Top());
										pageBounds.Bottom(pageBounds.Bottom());
										pageBounds.Left(pageBounds.Left()+margin);
										pageBounds.Right(pageBounds.Right()+margin);
																		
										if(pageBounds.Right() > marginBoxBounds.Right())
										{
											pageBounds = originalPageBounds ;
											pageBounds.Top(pageBounds.Top()+ newMargin);
											pageBounds.Bottom(pageBounds.Bottom()+newMargin);
											pageBounds.Left(pageBounds.Left());
											pageBounds.Right(pageBounds.Right());
											originalPageBounds = pageBounds;
										}
										
									}				
									else
									{
										//CA("isHorizontalFlow()==kFalse");
										pageBounds.Top(pageBounds.Top()+margin);
										pageBounds.Bottom(pageBounds.Bottom()+margin);
										pageBounds.Left(pageBounds.Left());
										pageBounds.Right(pageBounds.Right());
										if(pageBounds.Bottom () > marginBoxBounds.Bottom ())
										{
                                            //CA("Adjust box position Now");
											pageBounds = originalPageBounds ;
											getBoxPosition(pageBounds);
											originalPageBounds = pageBounds;
											fColumnChange = kTrue;
										}
									}

									bool16 stat = copySelectedItems(newItem,pageBounds);
									if(!stat)
										break;
		
									UIDList tempUIDLIst(newItem.GetDataBase(), newItem.GetUID());
									
									if(!fColumnChange)
									{
										NewImageFrameUIDList.push_back(newItem.GetUID());
										NewImageFrameUIDListForMap.push_back(newItem);
									}else
									{
										
										NewImageFrameUIDList_WhenFrameMovesToNextColumn.push_back(newItem.GetUID());

										vectorBoxBounds ImageFrameBoxBoundsVector;
										PMRect ImageFrameMaxBounds = kZeroRect;
										bool16 result = getMaxLimitsOfBoxes(tempUIDLIst, ImageFrameMaxBounds, ImageFrameBoxBoundsVector);
										if(result == kFalse)
											break;
										
										ReturnParameter rp;		
										rp.idxHorizontalCount = HorizontalCount;
										rp.idxVerticalCount = vericalCount;

										DynFrameStruct CurrentFameStruct;
										CurrentFameStruct.HorzCnt = HorizontalCount;
										CurrentFameStruct.VertCnt = vericalCount;
										CurrentFameStruct.BoxBounds = ImageFrameMaxBounds;
										CurrentFameStruct.isLastHorzFrame = kFalse;

										rp.CurrentDynFrameStruct= CurrentFameStruct;
										rp.NewImageFrameUIDList_WhenFrameMovesToNextColumn = NewImageFrameUIDList_WhenFrameMovesToNextColumn;

										NewImageFrameUIDList_WhenFrameMovesToNextColumn.clear();
										if(fPageAdded)
										{
											rp.fPageAdded = kTrue;

                                            for(int32 pageIndex = 0; pageIndex < PageUIDList.size(); pageIndex++)
											{
												rp.PageUIDList.push_back(PageUIDList[pageIndex]);
											}											 
										}
										else
										{
											rp.fPageAdded = kFalse;
											rp.PageUIDList.clear();
										}

										vReturnParameter.push_back(rp);
									}
									if(ImportFileInFrame(newItem, total))
									{	
										Flag=1;	
										//fitImageInBox(newItem);	
										ItemImageUnspreadFlag = kFalse;
									}
                                    /// UpDating the Copied Tag
									TagList tList1 = itagReader->getTagsFromBox(newItem);
									if(tList1.size() != 0)
									{
										TagStruct tagInfo1 = tList1[0];
										PMString attribVal;
										attribVal.AppendNumber(PMReal( tagInfo.parentId));
										if(tagInfo1.tagPtr)
										{	
											XMLReference tagInfo1XMLRef = tagInfo1.tagPtr->GetXMLReference();

											TagName = tagInfo1.tagPtr->GetTagString();
											Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XMLRef,  WideString("parentID"), WideString(attribVal));//ObjectID
											
											attribVal.Clear();
											attribVal.AppendNumber(PMReal(CurrentSubSectionID));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XMLRef,  WideString("sectionID"),WideString( attribVal));//Publication ID								
											
											attribVal.Clear();
											attribVal.AppendNumber(PMReal(tagInfo.parentTypeID));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XMLRef,  WideString("parentTypeID"), WideString(attribVal));//Parent Type ID
											
											attribVal.Clear();
											attribVal.AppendNumber(PMReal(typeId));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XMLRef,  WideString("typeId"),WideString( attribVal));//Parent Type ID
											
											attribVal.Clear();
											attribVal.AppendNumber(PMReal(mpv_value_id));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XMLRef,  WideString("rowno"),WideString( attribVal));//Parent Type ID
											
											attribVal.Clear();
											attribVal.AppendNumber(PMReal(pNode.getPBObjectID()));
											Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XMLRef,  WideString("pbObjectId"),WideString( attribVal));								
										}
										
										for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
										{
											tList1[tagIndex].tagPtr->Release();
										}							
									}
								}
							}
							else
							{
							//	CA("margin == 0 ");
								originalPageBounds = pageBounds;
							
								if((!isInlineImage) && margin == 0)
								{
								//	CA("!isInlineImage) && margin == 0");
									
									InterfacePtr<IScrapSuite> scrapSuite(static_cast<IScrapSuite*>(Utils<ISelectionUtils>()->QuerySuite(IID_ISCRAPSUITE)));
									if (scrapSuite == nil)
									{
										ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::CopySelectedItems:: scrapSuite invalid");
										continue;
									}

									InterfacePtr<IClipboardController> clipController(GetExecutionContextSession(), UseDefaultIID());
                                    if (clipController == nil)
									{
										ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::CopySelectedItems::BscCltCore:: clipController invalid");
										continue;
									}

									InterfacePtr<IControlView> controlView(Utils<ILayoutUIUtils>()->QueryFrontView());
									if (controlView == nil)
									{
										ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::CopySelectedItems:: controlView invalid");
										continue;
									}

									// Copy and paste the selection:
									if(scrapSuite->CanCopy(clipController) != kTrue)
										continue;
									if(scrapSuite->Copy(clipController) != kSuccess)
										continue;

									newItem = boxUIDRef;
								}
								if( (!isInlineImage) && ImportFileInFrame(newItem,total))
								{
								//	CA("ImportFileInFrame");
									Flag=1;									
									ItemImageUnspreadFlag = kFalse;								
									isFirstImageSprayed = kTrue;
								}
								else if(isInlineImage)
								{
									XMLContentReference contentRef = tagInfo.tagPtr->GetContentReference();																									
									UIDRef ContentRef = contentRef.GetUIDRef();

									TextIndex startPos=-1;
									TextIndex endPos = -1;									
									bool16 pos = Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&startPos,&endPos);
									textModel->CopyRange(startPos,2,pasteInLineData); 

									if(AssetValuePtrObj->size() > 1)
									{
										// Now Pest Selected
										vecAssetUidRef.push_back(ContentRef);  //Adding First Image UIdRef
																	
										int32 posi = 0;
										
										PMString cr;
										cr.Append(kTextChar_Space);
										WideString wstr(cr);
										int32 imgIndex =1;	
										VectorAssetValue::iterator it3ForInline;
										it3ForInline = AssetValuePtrObj->begin();
										for(int32 NumofAssetTimes=static_cast<int32>(AssetValuePtrObj->size()-1);NumofAssetTimes >0; NumofAssetTimes--)
										{
                                            //CA("In Loop... Of Pesting ");
											
											UIDRef  newItemUIdRef = kInvalidUIDRef;
											posi = posi + 2;
											
                                            TextIndex newPastePos = startPos + posi;
											if(NumofAssetTimes != AssetValuePtrObj->size()-1)
											{
                                                InterfacePtr<ITextModel>  tmptextModel;
                                                getTextModelByBoxUIDRef(boxUIDRef , tmptextModel);
												if (tmptextModel == NULL)
												{
													ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::sprayForThisBox::!tmptextModel");
													return;
												}

												textModel = tmptextModel;
												bool16 pos = Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&startPos,&endPos);
												textModel->CopyRange(startPos,2,pasteInLineData); 											
											}

											boost::shared_ptr<WideString> insertText(&wstr);
											InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
    										ASSERT(textModelCmds);
    										if (!textModelCmds) {
												break;
											}
											InterfacePtr<ICommand> insertCmd(textModelCmds->InsertCmd(newPastePos, insertText));
											ASSERT(insertCmd);
											if (!insertCmd) {
												break;
											}
											ErrorCode status = CmdUtils::ProcessCommand(insertCmd);
											CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
											fileName = it3ForInline[imgIndex++].geturl();
											//CA("befor  fileName =  "+fileName );
											if(fileName!= ""){
												textModel->Paste(newPastePos,pasteInLineData);
												fileName ="";
											}
											else{											
												fileName ="";
												continue;
											}
																													
											TagList newList=itagReader->getTagsFromBox(boxUIDRef);

											TagStruct newTagInfo;
											bool16 tagFound = kFalse;
											for(int32 i = 0; i < newList.size(); i++)
											{											
												tagFound = kFalse;
												for(int32 j = 0;j < duplicateTagList.size() ; j++ )
                                                {
													if(newList[i].tagPtr == duplicateTagList[j].tagPtr)
													{
														tagFound = kTrue;
														break;
													}
												}
												if(tagFound == kFalse)
												{												
													newTagInfo = newList[i];
													XMLReference newTagInfoXmlRef = newTagInfo.tagPtr->GetXMLReference();

													it2ForInline++;

													objCAssetvalueForInline = *it2ForInline;
													
													double typeId = -1;
													if(tagInfo.typeId == -97)
													{							
														typeId = objCAssetvalueForInline.getType_id();																										
													}
													else
													{
														typeId = tagInfo.typeId;																									
													}

													PMString attributeName = "typeId";
													PMString attributeValue("");

													attributeValue.AppendNumber(PMReal(typeId));
													Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef,   WideString(attributeName),WideString(attributeValue));
													break;
												}
											}
											if(newList.size() == tList.size()){																				
												continue;
											}
											XMLContentReference newContentRef = newTagInfo.tagPtr->GetContentReference();																									
											UIDRef NewFrameUIDRef = newContentRef.GetUIDRef();			
											
											vecAssetUidRef.push_back(NewFrameUIDRef);

											for(int32 tagIndex = 0 ; tagIndex < duplicateTagList.size() ; tagIndex++)
											{
												duplicateTagList[tagIndex].tagPtr->Release();
											}

											duplicateTagList=itagReader->getTagsFromBox(boxUIDRef);

											for(int32 tagIndex = 0 ; tagIndex < newList.size() ; tagIndex++)
											{
												newList[tagIndex].tagPtr->Release();
											}
										}																									
									}

									if(ImportFileInFrame(ContentRef,total))
									{
										Flag=1;																		
										ItemImageUnspreadFlag = kFalse;
										isFirstImageSprayed = kTrue;
										
										//*** if Single Inline Image for Inline is Available
										if(AssetValuePtrObj->size() == 1)  
											makeImageFitProportionally = kTrue; // For Fitting InLine Image Proportionally 
									
									}

								}

								PMString attribVal;
								attribVal.AppendNumber(PMReal( tagInfo.parentId));
								
								if(tagInfo.tagPtr)
								{	
									XMLReference tagInfoXmlRef = tagInfo.tagPtr->GetXMLReference();

									TagName = tagInfo.tagPtr->GetTagString();
									Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("parentID"),WideString(attribVal));//ObjectID
									
									attribVal.Clear();
									attribVal.AppendNumber(PMReal(CurrentSubSectionID));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("sectionID"),WideString(attribVal));//Publication ID								
									
									attribVal.Clear();
									attribVal.AppendNumber(PMReal(tagInfo.parentTypeID));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("parentTypeID"),WideString(attribVal));//Parent Type ID
									
									attribVal.Clear();
									attribVal.AppendNumber(PMReal(typeId));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("typeId"),WideString(attribVal));//Parent Type ID
									
									attribVal.Clear();
									attribVal.AppendNumber(PMReal(mpv_value_id));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attribVal));//Parent Type ID
									
									attribVal.Clear();
									attribVal.AppendNumber(PMReal(pNode.getPBObjectID()));
									Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("pbObjectId"),WideString(attribVal));								
								}
							}
											
							if(tagInfo.flowDir == 0)
							{//if(tagInfo.isAutoResize == -111)
								margin = pageBounds.Right() -pageBounds.Left() +2;
								newMargin = pageBounds.Bottom() - pageBounds.Top() +2;
								if(tagInfo.typeId == -99){
									//CA(" tagInfo.typeId == -99 ");
									break;
								}
							}
							else
							{
								margin = pageBounds.Bottom () -pageBounds.Top () +2;
								newMargin = pageBounds.Right () - pageBounds.Left() +2;
								if(tagInfo.typeId == -99){
									//CA(" tagInfo.typeId == -99 ");
									break;
								}
							}				
						}
						divImageIndex++;
						if(checkIfDivArray != 1)
							break;

					}	

						if(flagfornewpage == kTrue)
						{
							flagfornewpage = kFalse;
							goto LABEL;
						}
		
						if((ItemImageUnspreadFlag == kTrue) && (isFirstImageSprayed == kFalse))
						{
							//attach tags
							PMString attribValue;
							attribValue.SetTranslatable(kFalse);
							attribValue.AppendNumber(PMReal( tagInfo.parentId));
							
							if(tagInfo.tagPtr)
							{	
								XMLReference tagInfoXmlRef = tagInfo.tagPtr->GetXMLReference();
								TagName = tagInfo.tagPtr->GetTagString();

								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("parentID"),WideString(attribValue));//ObjectID
								
								attribValue.Clear();
								attribValue.AppendNumber(PMReal(CurrentSubSectionID));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("sectionID"),WideString(attribValue));//Publication ID								
								
								attribValue.Clear();
								attribValue.AppendNumber(PMReal(tagInfo.parentTypeID));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("parentTypeID"),WideString(attribValue));//Parent Type ID
								
								attribValue.Clear();
								attribValue.AppendNumber(PMReal(tempTypeid));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("typeId"),WideString(attribValue));//Parent Type ID
								
								attribValue.Clear();
								attribValue.AppendNumber(PMReal(mpv_value_id));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("rowno"),WideString(attribValue));//Parent Type ID
								
								attribValue.Clear();
								attribValue.AppendNumber(PMReal(pNode.getPBObjectID()));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("pbObjectId"),WideString(attribValue));
							}
							ItemImageUnspreadFlag = kFalse;
						}
														
						//CA("at the end of outer for");
						if(AssetValuePtrObj)
							delete AssetValuePtrObj;

						isInlineImage = kFalse;
					}//end of ItemID for

				}
                
                if(NewImageFrameUIDList.size() > 0)
                {
                    NewImageFrameUIDListMap.insert(map<UID, VectorNewImageFrameUIDRefList>::value_type(boxUIDRef.GetUID(), NewImageFrameUIDListForMap));
                }
				//***** Making Fitting Of Inline Image
				if(makeImageFitProportionally)
				{
					XMLContentReference contentRef = tagInfo.tagPtr->GetContentReference();																									
					UIDRef ContentRef = contentRef.GetUIDRef();
					InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
					if(!iSelectionManager)
					{	
						 break;
					}

					if (iSelectionManager->SelectionExists (kInvalidClass, ISelectionManager::kAnySelection)) {
						// Clear the selection
						iSelectionManager->DeselectAll(nil);
					}
					InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
					( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID ,iSelectionManager))); 
					if(!txtMisSuite)
					{	
						return ; 
					}
					SelectFrame(ContentRef);
					txtMisSuite->setFrameUser();

					SelectFrame(boxUIDRef);	
					makeImageFitProportionally = kFalse;
				}		
				
				break;
			}//****End of Case 1:	

		}		
	}
        
    //handleDeleIfEmptyTags(boxUIDRef);

	int32 vec_size =static_cast<int32>(deleteTextRanges.size());
	//CA("---end loopp--");

	if(vec_size > 0)
	{
		for(int32 i = 0 ; i < vec_size; i++)
		{
            //CA_NUM("i", i);
			if(textModel != NULL)
			{
                InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
				if(textModelCmds != NULL) {

					if(i == vec_size -1 || i == 0)
					{
						//CAlert::InformationAlert("First elemet to Delete");
						RangeData::Lean l = RangeData::kLeanForward;
						int32 FirstStartIndex = (deleteTextRanges[vec_size - i - 1].Start(&l));
                        //CA_NUM("FirstStartIndex", FirstStartIndex);
						int32 FirstEndIndex = (deleteTextRanges[vec_size - i - 1].End() ) - 1;
                        //CA_NUM("FirstEndIndex", FirstEndIndex);
						RangeData FirstTagRangeData(FirstStartIndex, FirstEndIndex);
						if(FirstStartIndex == 0 && vec_size == numTags)
							deleteTextRanges[vec_size - i - 1] = FirstTagRangeData;
					}

                    RangeData::Lean l = RangeData::kLeanForward;
                    //CAlert::InformationAlert("Before deleting");
                    //CA_NUM("deleteTextRanges[vec_size - i - 1] start : ", deleteTextRanges[vec_size - i - 1].Start(&l));
                    //CA_NUM("deleteTextRanges[vec_size - i - 1].End() : ", deleteTextRanges[vec_size - i - 1].End());
					InterfacePtr<ICommand> deteteCmd(textModelCmds->DeleteCmd(deleteTextRanges[vec_size - i - 1], kFalse));
					if (deteteCmd != NULL)
                    {
						ErrorCode err = CmdUtils::ProcessCommand(deteteCmd);
						if(err == kFailure)
						{
							//CAlert::InformationAlert("not deleted ................................");
						}
						else
						{
							//CAlert::InformationAlert("deleted");
						}

					}
				}
			}
		}
        CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
	}

	if(CopyFlag == 1)
	{	
		// Fit the frame to the text content we placed.
		InterfacePtr<ICommand> fitFrameToContentCmd(CmdUtils::CreateCommand(kFitFrameToContentCmdBoss));
		ASSERT(fitFrameToContentCmd != nil);
		if (fitFrameToContentCmd == nil) {
		return;
		}
		fitFrameToContentCmd->SetItemList(UIDList(boxUIDRef));
		if (CmdUtils::ProcessCommand(fitFrameToContentCmd) != kSuccess) {
		ASSERT_FAIL("kFitFrameToContentCmdBoss failed");
		return;
		}

	}

	if(CopyFlag == 2)
	{
		//CA("Inside overflow");
		ThreadTextFrame ThreadObj;
		ThreadObj.DoCreateAndThreadTextFrame(boxUIDRef);
	}

	isInlineImage = kFalse;
	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}
	for(int32 tagIndex = 0 ; tagIndex < duplicateTagList.size() ; tagIndex++) //*******
	{
		duplicateTagList[tagIndex].tagPtr->Release();
	}
	return;
}



void CDataSprayer::fillDataInBox(const UIDRef& curBox, TagStruct& slugInfo, double objectId, bool16 isTaggedFrame,bool16 isFlag)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
		return;
	UID textFrameUID = kInvalidUID;
	XMLReference slugInfoXMLRef = slugInfo.tagPtr->GetXMLReference();

	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(curBox, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	}
	if (textFrameUID == kInvalidUID)
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillDataInBox::textFrameUID == kInvalidUID");		
		return;
	}
    
	InterfacePtr<IHierarchy> graphicFrameHierarchy(curBox, UseDefaultIID());
	if (graphicFrameHierarchy == nil) 
	{
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::fillDataInBox::!graphicFrameHierarchy");
		return;
	}
					
	InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
	if (!multiColumnItemHierarchy)
    {
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::fillDataInBox::!multiColumnItemHierarchy");
		return;
	}

	InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
	if (!multiColumnItemTextFrame)
    {
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::fillDataInBox::!multiColumnItemTextFrame");
		//CA("Its Not MultiColumn");
		return;
	}
	
	InterfacePtr<IHierarchy>
	frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
	if (!frameItemHierarchy)
    {
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::fillDataInBox::!frameItemHierarchy");
		return;
	}

	InterfacePtr<ITextFrameColumn>
	textFrame(frameItemHierarchy, UseDefaultIID()); //used later on down
	if (!textFrame)
    {
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::fillDataInBox::!textFrame");
		return;
	}

	
	InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
	if (textModel == NULL)
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillDataInBox::textModel == NULL");				
		return;
	}
		
	int32 tStart=0, tEnd=0;
	
	if(!isTaggedFrame)
	{
		InterfacePtr<ITagReader> itagReader
			((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader)
		{
			ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillDataInBox::itagReader == NULL");						
			return ;
		}
		bool16 changeDelStart= kFalse;
		if(delStart == slugInfo.startIndex )
			changeDelStart = kTrue;
			
		
		if(!itagReader->GetUpdatedTag(slugInfo))
		{
			ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillDataInBox::!itagReader->GetUpdatedTag");								
			return;
		}
		
		tStart=slugInfo.startIndex+1;
		tEnd=slugInfo.endIndex-tStart;
		
		if(changeDelStart)
			delStart = slugInfo.startIndex;
		
	}
	
	

	PMString textToInsert("");
	char* tempText=NULL;
	WString Buf = NULL;
	
	do
	{
		// new add bcoz structure has been changed 
		if((slugInfo.whichTab == 1 || slugInfo.whichTab == 2 || slugInfo.whichTab == 3 || slugInfo.whichTab == 5) && slugInfo.header == 1)
		{
			CElementModel cElementModelObj;
			bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(slugInfo.elementId,cElementModelObj);
			if(result)
				textToInsert = cElementModelObj.getDisplayNameByLanguageId(slugInfo.languageID);
		}
        
		
		else if(slugInfo.whichTab == 5)
		{
			PMString tempBuffer("");
			double attributeID = slugInfo.elementId;
			double languageID = slugInfo.languageID;
			double TagtypeId = slugInfo.typeId;
			double InputID = -1;
				
			if(slugInfo.catLevel > 0)
			{//if(slugInfo.colno != -1)
     			tempBuffer = ""; //ptrIAppFramework->getCategoryCopyAttributeValue(pNode.getPubId(),slugInfo.catLevel/*slugInfo.colno*/, attributeID,languageID, pNode.getIsProduct() );
			}
			else
			{
				//CA("Else Part slugInfo.colno != -1 ");
				if(attributeID < 0) // for static attribute Spray
				{
					InputID = ptrIAppFramework->getSectionIdByLevelAndEventId(slugInfo.catLevel ,CurrentSectionID, languageID);
					if(InputID == -1)
						break;
					
					tempBuffer = ptrIAppFramework->PUBModel_getAttributeValueByLanguageID(InputID,attributeID,languageID,kTrue);
				}
				else  // for spraying Publication, Section & Subsection level copy attributes.
				{
					InputID = ptrIAppFramework->getSectionIdByLevelAndEventId(slugInfo.catLevel ,CurrentSectionID, languageID);
					tempBuffer = ptrIAppFramework->PUBModel_getAttributeValueByLanguageID(InputID, attributeID,languageID, kFalse);
				}
			}


			//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			//if(!iConverter)
			{
				textToInsert=tempBuffer;
				//break;
			}
			//textToInsert=iConverter->translateString(tempBuffer);
			//vectorHtmlTrackerPtr->clear();
			//textToInsert=iConverter->translateStringNew(tempBuffer, vectorHtmlTrackerPtr);
			if(isTaggedFrame)
				textToInsert.Append("\r");
			
			PMString attributeName;
			PMString attributeValue;
			
			attributeName = "parentID";
			attributeValue.AppendNumber(PMReal(InputID));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString(attributeName),WideString(attributeValue));
			attributeName.Clear();
			attributeValue.Clear();

			attributeName = "parentTypeID";
			attributeValue.AppendNumber(PMReal(CurrentObjectTypeID));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString(attributeName),WideString(attributeValue));
			attributeName.Clear();
			attributeValue.Clear();

			attributeName = "sectionID";
			attributeValue.AppendNumber(PMReal(CurrentSectionID));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString(attributeName),WideString(attributeValue));
			attributeName.Clear();
			attributeValue.Clear();
			
			break;
		} // End Project

		else if(slugInfo.whichTab == 1 || slugInfo.whichTab == 2 || slugInfo.whichTab == 3)
		{ // PF , PG, PR ..... Object Value
			//CA("Spraying for PF, PG, PR in FIll DataIn box");
			PMString tempBuffer("");

			double objectID = -1;
			double elementID = slugInfo.elementId;
			double languageID = slugInfo.languageID;

			if(slugInfo.whichTab == 1)
			{
				objectID =idList[0];
			}
			else if(slugInfo.whichTab == 2)
			{
				objectID =idList[1];
			}
			else if (slugInfo.whichTab == 3)
			{
				objectID =pNode.getPubId();
			}			

			if((slugInfo.tagPtr->GetAttributeValue(WideString("tableFlag"))) == WideString("-13"))
			{
				CElementModel cElementModelObj;
				bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(elementID,cElementModelObj);
				if(result)
					tempBuffer = cElementModelObj.getDisplayName();
			}
			else
			{
				if(elementID == -802) // For SPraying New Indicator
				{
					if(pNode.getNewProduct() == 1)
					{
						tempBuffer = "New";
					}
					else
					{
                        tempBuffer = "";
						deleteThisBox(curBox);
						return;
					}
				}
				else if(elementID == -803) //Letter  key
				{
					tempBuffer = slugInfo.tagPtr->GetAttributeValue(WideString("rowno"));
					if(tempBuffer== "-1")
						tempBuffer="";
				}
				else if(slugInfo.isEventField)
				{
					/*VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(objectID,elementID,CurrentSubSectionID);
					if(vecPtr != NULL)
						if(vecPtr->size()> 0)
							tempBuffer = vecPtr->at(0).getObjectValue();*/			
				}
				else if(slugInfo.dataType == 5)//----List Table Info
				{
					VectorScreenTableInfoPtr itemTableInfo = NULL;
					VectorScreenTableInfoPtr tableInfo = NULL;
					TableSourceInfoValue *tableSourceInfoValueObj;
					bool16 isCallFromTS = kFalse;
								
					if(slugInfo.whichTab == 3 || slugInfo.whichTab == 4)
					{
															
						InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
						if(ptrTableSourceHelper != nil)
						{
							tableSourceInfoValueObj=ptrTableSourceHelper->getTableSourceInfoValueObj();
							if(tableSourceInfoValueObj)
							{
								isCallFromTS = tableSourceInfoValueObj->getIsCallFromTABLEsource();
								if(isCallFromTS )	
								{	
									if(slugInfo.tableType == 3)
									{
									}
									else
									{
										if(slugInfo.whichTab == 3)
											tableInfo = tableSourceInfoValueObj->getVectorScreenTableInfoPtr();
										else if(slugInfo.whichTab == 4)//APS10
                                            tableInfo = tableSourceInfoValueObj->getVectorScreenItemTableInfoPtr();
									}
								}
							}
						}
					
					}
					if(isCallFromTS == kTrue)
					{

						for (int32 x=0; x< tableSourceInfoValueObj->getVec_Table_ID().size(); x++)
						{
							if(tableInfo == NULL)
							{
								ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::fillDataInBox::tableInfo == NULL");
								break;
							}

							if(tableInfo->size() == 0)
							{
								ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::fillDataInBox::table_InfoVector->size() == 0");
								break;
							}

							CItemTableValue oTableValue;
							VectorScreenTableInfoValue::iterator it;

							for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
							{
								oTableValue = *it;
								if((oTableValue.getTableID() == tableSourceInfoValueObj->getVec_Table_ID().at(x))) 
								{
									if(slugInfo.typeId == -55) 
									{
										if(slugInfo.elementId == -983)//---List Description
										{
											tempBuffer = oTableValue.getDescription();	
										}
										else if(slugInfo.elementId == -982)//---Stencil Name
										{
											tempBuffer = oTableValue.getstencil_name();
										}

									}
									else if(slugInfo.typeId == -56)//---"Note 1 to 5"
									{
										if(slugInfo.elementId == -980)//---for Note_1 
										{
											tempBuffer = oTableValue.getNotesList().at(0);	
										}
										else if(slugInfo.elementId == -979)//---for Note_2 
										{
											tempBuffer = oTableValue.getNotesList().at(1);		
										}
										else if(slugInfo.elementId == -978)//---for Note_3
										{
											tempBuffer = oTableValue.getNotesList().at(2);		
										}
										else if(slugInfo.elementId == -977)//---for Note_4
										{
											tempBuffer = oTableValue.getNotesList().at(3);		
										}
										else if(slugInfo.elementId == -976)//---for Note_5 
										{
											tempBuffer = oTableValue.getNotesList().at(4);	
										}
									}
								}

							}
							
						}

					}
					else
					{
						VectorScreenTableInfoPtr table_InfoVector = NULL;
                        if(slugInfo.whichTab == 3 )
                        {
                            table_InfoVector= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(CurrentSectionID, pNode.getPubId(), slugInfo.languageID ,kTrue);
                        }
                        else if(slugInfo.whichTab == 4)//APS10
                        {
                           table_InfoVector = ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNode.getPubId(), CurrentSectionID, slugInfo.languageID);
                        
                        }
						if(table_InfoVector == NULL)
						{
							ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::fillDataInBox::table_InfoVector == NULL");
							break;
						}

						if(table_InfoVector->size() == 0)
						{
							ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::fillDataInBox::table_InfoVector->size() == 0");
							break;
						}

						CItemTableValue oTableValue;
						VectorScreenTableInfoValue::iterator it;
						int32 isCheckForCScount = 0;

						for(it = table_InfoVector->begin(); it!=table_InfoVector->end(); it++,isCheckForCScount++)
						{
							oTableValue = *it;
							if(isCheckForCScount >= 1)
							{
								//CA("isCheckForCScount > 1");
								break;
							}

							if(slugInfo.typeId == -55) 
							{
								if(slugInfo.elementId == -983)//---List Description
								{
									tempBuffer = oTableValue.getDescription();	
								}
								else if(slugInfo.elementId == -982)//---Stencil Name
								{
									tempBuffer = oTableValue.getstencil_name();
								}

							}
							else if(slugInfo.typeId == -56)//---"Note 1 to 5"
							{
								if(slugInfo.elementId == -980)//---for Note_1 
								{
									//CA("for Note_1 ");
									tempBuffer = oTableValue.getNotesList().at(0);	
								}
								else if(slugInfo.elementId == -979)//---for Note_2 
								{
									tempBuffer = oTableValue.getNotesList().at(1);		
								}
								else if(slugInfo.elementId == -978)//---for Note_3
								{
									tempBuffer = oTableValue.getNotesList().at(2);		
								}
								else if(slugInfo.elementId == -977)//---for Note_4
								{
									tempBuffer = oTableValue.getNotesList().at(3);		
								}
								else if(slugInfo.elementId == -976)//---for Note_5 
								{
									tempBuffer = oTableValue.getNotesList().at(4);	
								}
							}
						}
						if(table_InfoVector)
						{
							table_InfoVector->clear();
							delete table_InfoVector;
							table_InfoVector = NULL;
						}
					}
				}
				else
				{
					tempBuffer = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(objectID,elementID,languageID, CurrentSectionID,  isFlag);
				}
				if(elementID == -827) //Number  key
				{
					tempBuffer = slugInfo.tagPtr->GetAttributeValue(WideString("rowno"));
					if(tempBuffer== "-1")
						tempBuffer="";
				}
			}
            textToInsert=tempBuffer;

			if(isTaggedFrame)
				textToInsert.Append("\r");

			break;
		}  // end PR,PF,PG
		
	}while(0);

    textToInsert.ParseForEmbeddedCharacters();
	WideString* myText=new WideString(textToInsert);

	if(!isTaggedFrame)
	{
		//CA("!isTaggedFrame");
		//iConverter->ChangeQutationMarkONOFFState(kFalse);
        textToInsert.ParseForEmbeddedCharacters();
		boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
		ErrorCode Err=ReplaceText(textModel, tStart, tEnd, insertText);
		//iConverter->ChangeQutationMarkONOFFState(kTrue);

		//this->ToggleBold_or_Italic(textModel , vectorHtmlTrackerPtr, tStart );
		
		int32 tagStartPos;
		if(slugInfo.deleteIfEmpty == 1)
		{//if(slugInfo.rowno == -117)
			if(textToInsert == "")						
				shouldDelete = kTrue;
	
			int32 tagStartPos1 = -1;
			int32 tagEndPos1 = -1;
			if(slugInfo.tagPtr)
				Utils<IXMLUtils>()->GetElementIndices(slugInfo.tagPtr,&tagStartPos1,&tagEndPos1);			
			delEnd = tagEndPos1;
			tagStartPos = tagStartPos1+1;
		}
        
        if(shouldDelete == kTrue )
        {
            PMString attributeName;
            PMString attributeValue;
            
            attributeName = "deleteIfEmpty";
            attributeValue.AppendNumber(2);
            Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString(attributeName),WideString(attributeValue));
            
        }
		
		if(shouldDelete == kTrue )
		{
			//CA("Going to delete 2");
		
			int32 tagStartPos1 = -1;
			int32 tagEndPos1 = -1;

			
			if(deleteCount >0)
				CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 
		
			deleteCount++;

			XMLReference perentXMLRef = slugInfo.tagPtr->GetParent();
			InterfacePtr<IIDXMLElement>perentXMLElement(perentXMLRef.Instantiate());
			if(!perentXMLElement)
				return;

			Utils<IXMLUtils>()->GetElementIndices(perentXMLElement,&tagStartPos1,&tagEndPos1);

			if(slugInfo.elementId != attributeId)
			{
				TextIterator begin1(textModel , tagStartPos);
				TextIterator end1(textModel , tagStartPos1);
				int32 removethis = 0;
				bool16 prevEnterFound = kFalse;
				for(TextIterator iter = begin1 ;iter > end1 ; iter--)
				{
					
					if(*iter == kTextChar_CR)
					{
						prevEnterFound = kTrue;
						break;
					}
					removethis++;
						
				}
				if(prevEnterFound)
				{
					delStart = tagStartPos - removethis + 1;
				}
				else
				{
					delStart = tagStartPos1;
				}
			}
			else
			{
				TextIterator begin1(textModel , delStart);
				TextIterator end1(textModel , tagStartPos1);
				int32 removethis = 0;
				bool16 prevEnterFound = kFalse;
				for(TextIterator iter = begin1 ;iter > end1 ; iter--)
				{
					
					if(*iter == kTextChar_CR)
					{
						prevEnterFound = kTrue;
						break;
					}
					removethis++;
						
				}
				if(prevEnterFound)
				{
					delStart = delStart - removethis + 1;
				}
				else
				{
					delStart = tagStartPos1 ;
				}
			}
	
			const XMLReference & XMLElementOfTagToBeDeleted = slugInfo.tagPtr->GetXMLReference();
			ErrorCode err = Utils<IXMLElementCommands>()->DeleteElement(XMLElementOfTagToBeDeleted,kFalse);
            
			TextIterator begin(textModel , delStart);
			TextIterator end(textModel , tagEndPos1);
			int32 addthis = 0;
			bool16 enterFound = kFalse;
			for(TextIterator iter = begin ;iter < end ; iter++)
			{
				addthis++;
				if(*iter == kTextChar_CR)
				{
					enterFound = kTrue;
					break;
				}
			}
			if(enterFound)
			{
				delEnd = delStart + addthis;
			}
			else
			{
				delEnd = tagEndPos1;
			}
			RangeData rangeData(delStart,delEnd);
			deleteTextRanges.push_back(rangeData);
		}
	}
	else
	{	
		//CA("Spraying Copy Attr");
		TextIndex startIndex = textFrame->TextStart();
		TextIndex finishIndex = startIndex + textModel->GetPrimaryStoryThreadSpan()-1;
		if(finishIndex<0)
			finishIndex=0;

		InterfacePtr<ITextFocusManager> textFocusManager(textModel, UseDefaultIID());
		if (textFocusManager == NULL)
			return;

		InterfacePtr<ITextFocus> frameTextFocus(textFocusManager->NewFocus(RangeData(0, 0, RangeData::kLeanForward)));
		if (frameTextFocus == NULL)
		{
			ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillDataInBox::frameTextFocus == NULL");																	
			return;
		}
        
		boost::shared_ptr<WideString> insertText(myText);
		InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
    	ASSERT(textModelCmds);
    	if (!textModelCmds) {
			return;
		}
		InterfacePtr<ICommand> insertCmd(textModelCmds->InsertCmd(finishIndex, insertText));
		ASSERT(insertCmd);
		if (!insertCmd) {
			return;
		}
		ErrorCode status = CmdUtils::ProcessCommand(insertCmd);
		CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
	}	
	
	this->forceRecompose(textFrame);
	
	delete myText;
	
}

bool16 CDataSprayer::ImportFileInFrame
(const UIDRef& imageBox, const PMString& fromPath)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		return kFalse;
	}
	bool16 fileExists = SDKUtilities::FileExistsForRead(fromPath) == kSuccess;
	if(!fileExists)
    {
        ptrIAppFramework->LogError("CDataSprayer::ImportFileInFrame: Image not found on- " + fromPath );
		return kFalse;
    }

	IDocument* doc =Utils<ILayoutUIUtils>()->GetFrontDocument();
	IDataBase* db = ::GetDataBase(doc); 
	if (db == NULL)
	{
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::ImportFileInFrame::db == NULL");																	
		return kFalse;
	}

	IDFile sysFile = SDKUtilities::PMStringToSysFile(const_cast<PMString* >(&fromPath));	

	if(  FileUtils::GetFileSize (sysFile) <= 0 )
	{
		ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::ImportFileInFrame::Got a Zero size file , aborting Import");		
		PMString zeroByteImageStr("Unable to Spray Image with 0kb size: ");
		zeroByteImageStr.Append(fromPath);
		zeroByteImageStr.Append("\n");
		zeroByteImageList.Append(zeroByteImageStr);
			
		return kFalse;
	}

	InterfacePtr<IPlaceGun> placeGun(db, db->GetRootUID(), UseDefaultIID());
	if(!placeGun)
	{
		ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::ImportFileInFrame::!placeGun");
		return kFalse;
	}

	if (placeGun->IsLoaded())
	{
		InterfacePtr<ICommand> abortPlaceGunCmd( CmdUtils::CreateCommand(kAbortPlaceGunCmdBoss));
		ASSERT(abortPlaceGunCmd);
		if (!abortPlaceGunCmd)
		{
			ErrorUtils::PMSetGlobalErrorCode(kFailure);
			return kFalse;
		}
		InterfacePtr<IUIDData> uidData(abortPlaceGunCmd, UseDefaultIID());
		ASSERT(uidData);
		if (!uidData) 
		{
			ErrorUtils::PMSetGlobalErrorCode(kFailure);
			return kFalse;
		}
		uidData->Set(placeGun);
		if (CmdUtils::ProcessCommand(abortPlaceGunCmd) != kSuccess) 
		{
			ASSERT_FAIL("kAbortPlaceGunCmdBoss failed");
			ErrorUtils::PMSetGlobalErrorCode(kFailure);
			return kFalse;
		}
	}

	InterfacePtr<ICommand> importCmd(CmdUtils::CreateCommand(kImportAndLoadPlaceGunCmdBoss));
	if(!importCmd) 
	{
		ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::ImportFileInFrame::importCmd == NULL");
		return kFalse;
	}

	URI tmpURI;
	Utils<IURIUtils>()->IDFileToURI(sysFile, tmpURI);
	InterfacePtr<IImportResourceCmdData> importFileCmdData(importCmd, IID_IIMPORTRESOURCECMDDATA); // no kDefaultIID	
	if (importFileCmdData == nil){
		return kFalse;
	}	
	importFileCmdData->Set(db,tmpURI,kMinimalUI);

	ErrorCode err = CmdUtils::ProcessCommand(importCmd);
	if(err != kSuccess) 
		return kFalse;

	UIDRef placedItem(db, placeGun->GetFirstPlaceGunItemUID());
	InterfacePtr<ICommand> replaceCmd(CmdUtils::CreateCommand(kReplaceCmdBoss));
	if (replaceCmd == NULL)
	{
		ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::ImportFileInFrame::!replaceCmd");
		return kFalse;
	}
	
	InterfacePtr<IReplaceCmdData>iRepData(replaceCmd, IID_IREPLACECMDDATA);
	if(!iRepData)
	{
		ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::ImportFileInFrame::!iRepData");
		return kFalse;
	}

	iRepData->Set(db, imageBox.GetUID(), placedItem.GetUID(), kFalse);
	
	ErrorCode status = CmdUtils::ProcessCommand(replaceCmd);
	if(status==kFailure)
	{
		ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::ImportFileInFrame::!status");
		return kFalse;
	}
	return kTrue;
}


bool16 CDataSprayer::fillImageInBox
(const UIDRef& boxUIDRef, TagStruct& slugInfo, double objectId)
{
	static PMString SectionSpryImagePath("");
	XMLReference slugInfoXMLRef = slugInfo.tagPtr->GetXMLReference();

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}
	InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	if(ptrIClientOptions==NULL)
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::!ptrIClientOptions");
		return kFalse;
	}

	imagePath=ptrIClientOptions->getImageDownloadPath();
	if(imagePath!="")
	{
		const char *imageP=imagePath.GetPlatformString().c_str();
		if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
			#ifdef MACINTOSH
				imagePath+="/";
			#else
				imagePath+="\\";
			#endif
	}
	if(imagePath=="")
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::Asset Server path not set");
		return 1;
	}
	
	//PMString CutomerAssetTypes("");
	//PMString DivisionAssetTypes("");
	//if(ptrIAppFramework->CONFIGCACHE_getShowCustomerInIndesign())	
	//{
	//	if(ptrIAppFramework->getSelectedCustPVID(1) != -1)
	//	{
	//		//CA("shown customer selection");
	//		CutomerAssetTypes = ptrIAppFramework->CONFIGCACHE_getCustomerAssetTypes(1);
	//		if(CutomerAssetTypes.Compare(kFalse,"")==0)
	//		{
	//			//CA("CutomerAssetTypes==nil");
	//			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::!CutomerAssetTypes");	
	//			//break;
	//		}
	//		//CA("CutomerAssetTypes : "+CutomerAssetTypes);
	//	}
	//	if(ptrIAppFramework->getSelectedCustPVID(0) != -1)
	//	{
	//		//CA("shown customer selection");
	//		DivisionAssetTypes = ptrIAppFramework->CONFIGCACHE_getCustomerAssetTypes(0);
	//		if(DivisionAssetTypes.Compare(kFalse,"")==0)
	//		{
	//			//CA("DivisionAssetTypes==nil");
	//			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::!DivisionAssetTypes");	
	//			//break;
	//		}
	//		//CA("DivisionAssetTypes : "+DivisionAssetTypes);
	//	}
	//}	

	PMString fileName("");
	double assetID;
	double mpv_value_id = -1;
	CObjectValue oVal;

	oVal=ptrIAppFramework->GETProduct_getObjectElementValue(objectId, CurrentSectionID);  // new added in appFramework
	double parentTypeID= oVal.getObject_type_id();

	PMReal maxPageWidth,maxPageHeight;
	int numPages1=1;
	PMReal page1height,page1width;

	do{
		
		InterfacePtr<ILayoutControlData> layoutData1(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutData1 == nil)
		{
			//CA("layoutData == nil");
			break;
		}
        layoutData1->SetFit(ILayoutControlData::kFitPage);
        
		UIDRef sprdref = layoutData1->GetSpreadRef();
		InterfacePtr<ISpread> iSpread1(sprdref, UseDefaultIID());
		if (iSpread1 == nil)
		{
			//CA("iSpread == nil");
			break;
		}
		numPages1=iSpread1->GetNumPages();
		UID page1UID1= iSpread1->GetNthPageUID(0);
		if(page1UID1 == kInvalidUID)
		{
			//CA("page1UID1 == kInvalidUID");		
			break;
		}	

		IDocument* document = layoutData1->GetDocument();
		if (document == nil)
		{
			//CA("No document");		
			break;
		}

		IDataBase* database = ::GetDataBase(document);
		if(!database)
		{
			//CA("No database");		
			break;
		}
		UIDRef current1PageUIDRef(database, page1UID1);
		
		InterfacePtr<IGeometry> pageGeometry(current1PageUIDRef, UseDefaultIID());
		if (pageGeometry == nil)
		{
			//CA("pageGeometry is invalid");
			break;
		}
		
		PMRect pageBoxBounds = pageGeometry->GetStrokeBoundingBox();

		page1width = pageBoxBounds.Right() - pageBoxBounds.Left();
		page1height = pageBoxBounds.Bottom() - pageBoxBounds.Top();
		if(numPages1 == 2)
		{
			//CA("Changing Margins");
			maxPageWidth = 3 * page1width;						
		}
		else
		{
			maxPageWidth = 2 * page1width;		
		}
		maxPageHeight = page1height + 72;
	}while(kFalse);

	if(slugInfo.whichTab!=4)
	{
		VectorAssetValuePtr AssetValuePtrObj = NULL;
		if(slugInfo.typeId == -98 || slugInfo.typeId == -96 )
		{
			int32 isProduct =1;
			AssetValuePtrObj=ptrIAppFramework->GETAssets_GetAssetByParentAndType(objectId, CurrentSectionID, isProduct);
		}
		else
		{
			int32 isProduct =1;
			AssetValuePtrObj = ptrIAppFramework->GETAssets_GetAssetByParentAndType(objectId, CurrentSectionID, isProduct, slugInfo.typeId);
		}
		if(AssetValuePtrObj == NULL)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::AssetValuePtrObj == NULL");
			return kFalse;
		}

		if(AssetValuePtrObj->size() ==0)
		{
			ptrIAppFramework->LogInfo("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::AssetValuePtrObj->size() ==0");
			return kFalse;
		}

		slugInfo.parentId = objectId;
		slugInfo.parentTypeID = parentTypeID;

		if(global_project_level == 3)
			slugInfo.sectionID = CurrentSubSectionID;
		if(global_project_level == 2)
			slugInfo.sectionID = CurrentSectionID;

		InterfacePtr<IHierarchy> iHier(boxUIDRef, UseDefaultIID());
		if(!iHier)
		{
			ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::!iHier");		
			return kFalse;
		}
		
		UIDRef parentUIDRef(boxUIDRef.GetDataBase(), iHier->GetParentUID());
		InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{
			return kFalse;
		}
		if (iSelectionManager->SelectionExists (kInvalidClass, ISelectionManager::kAnySelection)) {
			// Clear the selection
			iSelectionManager->DeselectAll(nil);
		}

		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
		if (!layoutSelectionSuite) 
		{	//CA("!layoutSelectionSuite");
			return kFalse;
		}

		layoutSelectionSuite->DeselectAllPageItems();
		UIDList selectUIDList(boxUIDRef);
		layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);

		InterfacePtr<IGeometry> iGeo(boxUIDRef, IID_IGEOMETRY);
		if(!iGeo)
		{
			ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::!iGeo");		
			return kFalse;
		}

		PMRect pageBounds=iGeo->GetStrokeBoundingBox(InnerToPasteboardMatrix(iGeo));
		PMRect originalPageBounds;
		PMRect FirstFrameBounds = pageBounds;

		VectorAssetValue::iterator it; // iterator of Asset value	
		vector <UIDRef> vecAssetUidRef;
		VectorAssetValue::iterator it2ForInline;
		CAssetValue objCAssetvalueForInline;
		it2ForInline = AssetValuePtrObj->begin();

		PMReal margin = 0;
		PMReal newMargin = 0;
		PMString TagName("");
		CAssetValue objCAssetvalue;
		for(it = AssetValuePtrObj->begin();it!=AssetValuePtrObj->end();it++)
		{
			objCAssetvalue = *it;
			fileName = objCAssetvalue.geturl();
			assetID = objCAssetvalue.getAsset_id();

			double typeId = -1;
			if(slugInfo.typeId == -96 )
				typeId = objCAssetvalue.getType_id();
			else
				typeId = slugInfo.typeId;

			if(fileName=="")
				continue;
			
			//if(ptrIAppFramework->getSelectedCustPVID(1) != -1)
			//	if(CutomerAssetTypes.Compare(kFalse,"") != 0)
			//	{
			//		PMString temp("");
			//		temp.AppendNumber(PMReal(typeId);
			//		//CA("typeId : "+temp);
			//		if(CutomerAssetTypes.Contains(temp)==kTrue)			
			//			if(mpv_value_id != ptrIAppFramework->getSelectedCustPVID(1))
			//				continue;													
			//	}
			//if(ptrIAppFramework->getSelectedCustPVID(0) != -1)
			//	if(DivisionAssetTypes.Compare(kFalse,"") != 0)
			//	{
			//		PMString temp("");
			//		temp.AppendNumber(PMReal(typeId);
			//		//CA("typeId : "+temp);
			//		if(DivisionAssetTypes.Contains(temp)==kTrue)			
			//			if(mpv_value_id != ptrIAppFramework->getSelectedCustPVID(0))
			//				continue;													
			//	}
			
			UIDRef newItem;	
			do
			{
				SDKUtilities::Replace(fileName,"%20"," ");
			}while(fileName.IndexOfString("%20") != -1);

			PMString imagePathWithSubdir = imagePath;
			PMString typeCodeString("");
			
			if(typeCodeString.NumUTF16TextChars()!=0)
			{ 
				//CA("typeCodeString = " + typeCodeString);
				PMString TempString = typeCodeString;		
				CharCounter ABC = 0;
				bool16 Flag = kTrue;
				bool16 isFirst = kTrue;
				do
				{					
					if(TempString.Contains("/", ABC))
					{
		 				ABC = TempString.IndexOfString("/");
						if(ABC == 0)
						{
							//CA("Removing");
							TempString.Remove(0,1);
							continue;
						}
				 	
	 					PMString* FirstString = TempString.Substring(0, ABC);
						PMString newsubString = *FirstString;
						
						//CA("NEW SUBSTRING = " + newsubString);
	 					if(!isFirst)
	 						imagePathWithSubdir.Append("\\");
	 										 		
	 					PMString * SecondString = TempString.Substring(ABC+1);
	 					PMString SSSTring =  *SecondString;		 		
	 					TempString = SSSTring;

						if(FirstString)
							delete FirstString;

						if(SecondString)
							delete SecondString;
						//CA("FINAL TEMPSTRING = "+TempString);

					}
					else
					{				
						if(!isFirst)
	 						imagePathWithSubdir.Append("\\");
	 					isFirst = kFalse;				
						Flag= kFalse;

					}				
				}while(Flag);
			}
            
            #ifdef WINDOWS
			SDKUtilities ::AppendPathSeparator(imagePathWithSubdir);
            #endif

			PMString total;
            total=imagePathWithSubdir+fileName;

            #ifdef MACINTOSH
                SDKUtilities::convertToMacPath(total);
            #endif
            
			InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
			if (layoutData == nil)
				break;
		
            layoutData->SetFit(ILayoutControlData::kFitPage);
            
			IDocument* document = layoutData->GetDocument();
			if (document == NULL)
				break;

			IDataBase* database = ::GetDataBase(document);
			if(!database)
				break;

			InterfacePtr<IPageList> iPageList(document,UseDefaultIID());
			if(iPageList == nil)
			{
				ptrIAppFramework->LogDebug("ProductFinderPalette:SubSectionSprayer::getCurrentPage:iPageList == nil");
				break;
			}
			
			UID  pageUID = layoutData->GetPage();
			if(pageUID == kInvalidUID)
				break;

			UIDRef pageRef(database, pageUID);

			ThreadTextFrame ThreadObj;
			PMRect marginBoxBounds;
			ThreadObj .getMarginBounds (pageRef,marginBoxBounds);

			InterfacePtr<ITagReader> itagReader
			((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
			if(!itagReader)
			{
				ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::fillImageInBox::!itagReader");	
				return kFalse;
			}

			TagList duplicateTagList;
			duplicateTagList = itagReader->getTagsFromBox(boxUIDRef);

			if(FirstFrameBounds.Left() > marginBoxBounds.Right() )
			{
				//CA("Got wrong Page UID");
				UIDRef ref = layoutData->GetSpreadRef();
				InterfacePtr<ISpread> iSpread(ref, UseDefaultIID());
				if (iSpread == nil)
					break;

				int numPages=iSpread->GetNumPages();
				
				pageUID= iSpread->GetNthPageUID(numPages-1);

				UIDRef pageRef(database, pageUID);
				ThreadObj .getMarginBounds (pageRef,marginBoxBounds);
			}

			if(margin != 0 && (slugInfo.typeId != -98))
			{
				if(!isInlineImage)
				{
					if(slugInfo.flowDir == 0)	
					{//if(slugInfo.isAutoResize == -111)	
						//CA("isHorizontalFlow()==kTrue");
						pageBounds.Top(pageBounds.Top());
						pageBounds.Bottom(pageBounds.Bottom());
						pageBounds.Left(pageBounds.Left()+margin);
						pageBounds.Right(pageBounds.Right()+margin);
						if(pageBounds.Right() > marginBoxBounds.Right())
						{
							pageBounds = originalPageBounds ;
							pageBounds.Top(pageBounds.Top()+ newMargin);
							pageBounds.Bottom(pageBounds.Bottom()+newMargin);
							pageBounds.Left(pageBounds.Left());
							pageBounds.Right(pageBounds.Right());
							originalPageBounds = pageBounds;
						}
					}
					else
					{	
						//CA("isHorizontalFlow()==kFalse");
						pageBounds.Top(pageBounds.Top()+margin);
						pageBounds.Bottom(pageBounds.Bottom()+margin);
						pageBounds.Left(pageBounds.Left());
						pageBounds.Right(pageBounds.Right());
						if(pageBounds.Bottom() > marginBoxBounds.Bottom())
						{
							pageBounds = originalPageBounds ;
							pageBounds.Top(pageBounds.Top());
							pageBounds.Bottom(pageBounds.Bottom());
							pageBounds.Left(pageBounds.Left()+ newMargin);
							pageBounds.Right(pageBounds.Right()+ newMargin);
							originalPageBounds = pageBounds;
						}				
					}
					if(numPages1 == 1){
						if(pageBounds.Right() > maxPageWidth)
						{
							//CA("going out of pasteboard co-ordinate 1");
							break;
						}
					}
					else{
						if(pageBounds.Right() > maxPageWidth)
						{
							//CA("going out of pasteboard co-ordinate 2");
							break;
						}						
					}
					
					bool16 stat = copySelectedItems(newItem,pageBounds);
					if(!stat)
					{
						ptrIAppFramework->LogDebug("CDataSprayer:: fillImageInBox: copySelectedItems return kFalse");
						return kFalse;
					}

					UIDList tempUIDLIst(newItem.GetDataBase(), newItem.GetUID());
					
					NewImageFrameUIDList.push_back(newItem.GetUID());
                    NewImageFrameUIDListForMap.push_back(newItem);
				
					ImportFileInFrame(newItem, total);

					/// UpDating the Copied Tag					
					TagList tList1 = itagReader->getTagsFromBox(newItem);
					TagStruct tagInfo1 = tList1[0];
					XMLReference tagInfo1XMLRef = tagInfo1.tagPtr->GetXMLReference();

					PMString attribVal;
					attribVal.AppendNumber(PMReal(slugInfo.parentId));
					if(tagInfo1.tagPtr)
					{	
						TagName = tagInfo1.tagPtr->GetTagString();
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XMLRef, WideString("parentID"),WideString(attribVal));//ObjectID
						
						attribVal.Clear();							
						double pubId=slugInfo.sectionID;
						attribVal.AppendNumber(PMReal(pubId));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XMLRef, WideString("sectionID"),WideString(attribVal));//Publication ID
						
						attribVal.Clear();
						attribVal.AppendNumber(PMReal(slugInfo.parentTypeID));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XMLRef, WideString("parentTypeID"),WideString(attribVal));//Parent Type ID
						
						attribVal.Clear();
						attribVal.AppendNumber(PMReal(typeId));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XMLRef, WideString("typeId"),WideString(attribVal));
						
						attribVal.Clear();
						attribVal.AppendNumber(PMReal(mpv_value_id));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XMLRef, WideString("rowno"),WideString(attribVal));
						
						attribVal.Clear();
						attribVal.AppendNumber(PMReal(pNode.getPBObjectID()));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XMLRef, WideString("pbObjectId"),WideString(attribVal));
					}

					for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
					{
						tList1[tagIndex].tagPtr->Release();
					}
				}				
				else //if isInLineImage == kTrue
				{
					//CA("IsInLine Image == kTrue");				
				}
			}
			else
			{
				originalPageBounds = pageBounds;
				if(!isInlineImage){//Added if isInlineImage == kFalse

					InterfacePtr<IScrapSuite> scrapSuite(static_cast<IScrapSuite*>(Utils<ISelectionUtils>()->QuerySuite(IID_ISCRAPSUITE)));
					if (scrapSuite == nil)
					{
						//CA("CopySelectedItems: scrapSuite invalid");
						ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::CopySelectedItems:: scrapSuite invalid");
						continue;
					}
					InterfacePtr<IClipboardController> clipController(/*gSession*/GetExecutionContextSession(), UseDefaultIID());
					if (clipController == nil)
					{
						ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::CopySelectedItems::BscCltCore:: clipController invalid");
						continue;
					}

					InterfacePtr<IControlView> controlView(Utils<ILayoutUIUtils>()->QueryFrontView());
					if (controlView == nil)
					{
						ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::CopySelectedItems:: controlView invalid");
						continue;
					}
					//Copy and paste the selection:
					if(scrapSuite->CanCopy(clipController) != kTrue)
						continue;
					if(scrapSuite->Copy(clipController) != kSuccess)
						continue;

					newItem = boxUIDRef;
					ImportFileInFrame(newItem, total);
				}
				else //Means InLine Case :: Mumbai Terror's Day Night 26/11/08 2:23 Am
				{
					XMLContentReference contentRef = slugInfo.tagPtr->GetContentReference();
					UIDRef ContentRef = contentRef.GetUIDRef();
					
					//****** Now From Here Add Support for the Multiple Image Spray
					//*** Copying and pasting First Image Befor Spray of First Image

					//****Copying Selected Range Data 
					//**********
					TextIndex startPos=-1;
					TextIndex endPos = -1;									
					bool16 pos = Utils<IXMLUtils>()->GetElementIndices(slugInfo.tagPtr,&startPos,&endPos);														
																													
					gtextModel->CopyRange(startPos,2,pasteInLineData);
					
					if(AssetValuePtrObj->size() > 1)
					{
						vecAssetUidRef.push_back(ContentRef);  //Adding First Image UIdRef
						int32 posi = 0;
						PMString cr;
						cr.Append(kTextChar_Space);
						WideString wstr(cr);
						int32 imgIndex =1;	

						VectorAssetValue::iterator it3ForInline;
						it3ForInline = AssetValuePtrObj->begin();
						for(int32 NumofAssetTimes=static_cast<int32>(AssetValuePtrObj->size()-1);NumofAssetTimes >0; NumofAssetTimes--)
						{
							UIDRef  newItemUIdRef = kInvalidUIDRef;
							posi = posi + 2;
							
							TextIndex newPastePos = startPos + posi;
							if(NumofAssetTimes != AssetValuePtrObj->size()-1)
							{
                                InterfacePtr<ITextModel>  tmptextModel;
                                getTextModelByBoxUIDRef(boxUIDRef , tmptextModel);
								if (tmptextModel == NULL)
								{
									ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::sprayForThisBox::!tmptextModel");
									return kFalse;
								}

								gtextModel = tmptextModel;
								bool16 pos = Utils<IXMLUtils>()->GetElementIndices(slugInfo.tagPtr,&startPos,&endPos);
								gtextModel->CopyRange(startPos,2,pasteInLineData); 	
								
							}	

							boost::shared_ptr<WideString> insertText(&wstr);
							InterfacePtr<ITextModelCmds> textModelCmds(gtextModel, UseDefaultIID());
    						ASSERT(textModelCmds);
    						if (!textModelCmds) {
								continue;
							}
							InterfacePtr<ICommand> insertCmd(textModelCmds->InsertCmd(newPastePos, insertText));
							ASSERT(insertCmd);
							if (!insertCmd) {
								continue;
							}
							ErrorCode status = CmdUtils::ProcessCommand(insertCmd);

							CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
							fileName = it3ForInline[imgIndex++].geturl();
							if(fileName!= ""){
								gtextModel->Paste(newPastePos,pasteInLineData);
								fileName ="";
							}
							else{											
								fileName ="";
								continue;
							}
							TagList newList=itagReader->getTagsFromBox(boxUIDRef);

							TagStruct newTagInfo;
							bool16 tagFound = kFalse;

							for(int32 i = 0; i < newList.size(); i++)
							{

								tagFound = kFalse;
								for(int32 j = 0;j < duplicateTagList.size();j++ )
								{												
									if(newList[i].tagPtr == duplicateTagList[j].tagPtr)
									{
										//CA("newList[i].tagPtr == duplicateTagList[j].tagPtr");
										tagFound = kTrue;
										newTagInfo = newList[i];
										break;
									}

									if(tagFound == kFalse)
									{
										newTagInfo = newList[i];
										double typeId = -1;
										PMString attributeName = "typeId";
										PMString attributeValue("");
										break;
									}
								}
								if(newList.size() == duplicateTagList.size()){
									//CA("continued");
									continue;
								}
								XMLContentReference newContentRef = newTagInfo.tagPtr->GetContentReference();
								UIDRef NewFrameUIDRef = newContentRef.GetUIDRef();
								vecAssetUidRef.push_back(NewFrameUIDRef);

							}

							for(int32 tagIndex = 0 ; tagIndex < newList.size() ; tagIndex++)
							{
								newList[tagIndex].tagPtr->Release();
							}
						}						
					}

					//***** Here Importing the First Image
					if(ImportFileInFrame(ContentRef,total))
					{									
						//fitImageInBox(newItem);						
					}
				}
				PMString attribVal;
				attribVal.AppendNumber(PMReal(slugInfo.parentId));
				
				if(slugInfo.tagPtr)
				{	
					TagName = slugInfo.tagPtr->GetTagString();
					XMLReference slugInfoXMLRef = slugInfo.tagPtr->GetXMLReference();

					Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("parentID"),WideString(attribVal));//ObjectID
					
					attribVal.Clear();							
					double pubId=slugInfo.sectionID;
					attribVal.AppendNumber(PMReal(pubId));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("sectionID"),WideString(attribVal));//Publication ID
					
					attribVal.Clear();
					//pCache.isExist(id, pNode);
					attribVal.AppendNumber(PMReal(slugInfo.parentTypeID));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("parentTypeID"),WideString(attribVal));//Parent Type ID
					
					attribVal.Clear();
					attribVal.AppendNumber(PMReal(typeId));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("typeId"),WideString(attribVal));
					
					attribVal.Clear();
					attribVal.AppendNumber(PMReal(mpv_value_id));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("rowno"),WideString(attribVal));
					
					attribVal.Clear();
					attribVal.AppendNumber(PMReal(pNode.getPBObjectID()));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(slugInfoXMLRef, WideString("pbObjectId"),WideString(attribVal));
				}
			}
			if(slugInfo.flowDir == 0)
			{//if(slugInfo.isAutoResize == -111)
				margin = pageBounds.Right() -pageBounds.Left() +2;
				newMargin = pageBounds.Bottom() - pageBounds.Top() +2;
			}
			else
			{
				margin = pageBounds.Bottom() -pageBounds.Top() +2;
				newMargin = pageBounds.Right() - pageBounds.Left() +2;
			}

			for(int32 tagIndex = 0 ; tagIndex < duplicateTagList.size() ; tagIndex++)
			{
				duplicateTagList[tagIndex].tagPtr->Release();
			}

			if(slugInfo.typeId == -98)
				break;
		}

		if(AssetValuePtrObj){

			AssetValuePtrObj->clear();
			delete AssetValuePtrObj;
		}
	}//end of if slugInfo.whichTab != 4
	else
	{
		//CA("Else");
		do
		{
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == NULL)
			{
				CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
				break;
			}

			InterfacePtr<IHierarchy> iHier(boxUIDRef, UseDefaultIID());
			if(!iHier)
			{
				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:!iHier");				
				break;
			}

			UIDRef parentUIDRef(boxUIDRef.GetDataBase(), iHier->GetParentUID());
			InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
			if(!iSelectionManager)
			{
				break;
			}

			if (iSelectionManager->SelectionExists (kInvalidClass, ISelectionManager::kAnySelection)) {
				// Clear the selection
				iSelectionManager->DeselectAll(nil);
			}
	
			InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
			if (!layoutSelectionSuite) 
			{
				break;
			}
			layoutSelectionSuite->DeselectAllPageItems();
			UIDList selectUIDList(boxUIDRef);
			layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);
			InterfacePtr<IGeometry> iGeo(boxUIDRef, IID_IGEOMETRY);
			if(!iGeo)
			{
				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:!iGeo");								
				break;
			}

			PMRect pageBounds=iGeo->GetStrokeBoundingBox(InnerToPasteboardMatrix(iGeo));
			PMRect FirstFrameBounds = pageBounds;
			VectorLongIntValue::iterator it;
			PMReal margin = 0;
			PMString TagName("");
			int32 Flag=0;

			bool16 ItemImageUnspreadFlag = kFalse;
			UIDRef newItem;	

			XMLReference tagRef = slugInfo.tagPtr->GetXMLReference();
			XMLTagAttributeValue tagVal;
			convertTagStructToXMLTagAttributeValue(slugInfo,tagVal);

			InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
			if(ptrIClientOptions==NULL)
			{
				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:!ptrIClientOptions");
				break;
			}

			PMString imagePath=ptrIClientOptions->getImageDownloadPath();

			if(imagePath=="")
			{
				ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::Asset server path not set.");
				break;
			}

			if(imagePath!="")
			{
				const char *imageP=imagePath.GetPlatformString().c_str();
				if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
				#ifdef MACINTOSH
					imagePath+="/";
				#else
					imagePath+="\\";
				#endif
			}


			PMString fileName("");
			double assetID;
			double mpv_value_id = -1;
			VectorAssetValuePtr AssetValuePtrObj = NULL;


			slugInfo.parentId=pNode.getPubId();
			slugInfo.parentTypeID = pNode.getTypeId(); //ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
			slugInfo.sectionID=CurrentSubSectionID;
			if(slugInfo.typeId != -99)
			{
				int32 isProduct = 0;
				AssetValuePtrObj= ptrIAppFramework->GETAssets_GetAssetByParentAndType(/*itemID*/pNode.getPubId(), CurrentSubSectionID, isProduct, slugInfo.typeId);

			}
			else
			{
				int32 isProduct = 0;
				AssetValuePtrObj= ptrIAppFramework->GETAssets_GetAssetByParentAndType(/*itemID*/pNode.getPubId(), CurrentSubSectionID, isProduct);

			}
			if(AssetValuePtrObj == NULL){
				ItemImageUnspreadFlag = kTrue;
				continue ;
			}

			if(AssetValuePtrObj->size() == 0)
			{
				//CA("AssetValuePtrObj->size() == 0");
				ItemImageUnspreadFlag = kTrue;
				continue;
			}

			VectorAssetValue::iterator it1; // iterator of Asset value

			CAssetValue objCAssetvalue;
			double tempTypeid = -1;

			for(it1 = AssetValuePtrObj->begin();it1!=AssetValuePtrObj->end();it1++)
			{
				objCAssetvalue = *it1;
				fileName = objCAssetvalue.geturl();
				assetID = objCAssetvalue.getAsset_id();

				double typeId = -1;
				if(slugInfo.typeId == -99)
				{
					typeId = objCAssetvalue.getType_id();
					tempTypeid = typeId;
				}
				else
				{
					typeId = slugInfo.typeId;
					tempTypeid = typeId;
				}

				//CA("fileName : "+ fileName);
				if(fileName=="")
					continue;


				//if(ptrIAppFramework->getSelectedCustPVID(1) != -1)
				//	if(CutomerAssetTypes.Compare(kFalse,"") != 0)
				//	{
				//		PMString temp("");
				//		temp.AppendNumber(typeId);
				//		//CA("typeId : "+temp);
				//		if(CutomerAssetTypes.Contains(temp)==kTrue)			
				//			if(mpv_value_id != ptrIAppFramework->getSelectedCustPVID(1))
				//				continue;													
				//	}
				//if(ptrIAppFramework->getSelectedCustPVID(0) != -1)
				//	if(DivisionAssetTypes.Compare(kFalse,"") != 0)
				//	{
				//		PMString temp("");
				//		temp.AppendNumber(typeId);
				//		//CA("typeId : "+temp);
				//		if(DivisionAssetTypes.Contains(temp)==kTrue)			
				//			if(mpv_value_id != ptrIAppFramework->getSelectedCustPVID(0))
				//				continue;													
				//	}

				UIDRef newItem;
				do
				{
					SDKUtilities::Replace(fileName,"%20"," ");
				}while(fileName.IndexOfString("%20") != -1);

				PMString imagePathWithSubdir = imagePath;
				PMString typeCodeString("");

				#ifdef WINDOWS
					imagePathWithSubdir.Append("\\");
				#endif

				if(!fileExists(imagePathWithSubdir,fileName))
				{
					//CA("Image Not Found on Local Drive");
                    ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::Image Not Found on- " + imagePathWithSubdir + fileName);
					continue;
				}

				PMString total=imagePathWithSubdir+fileName;
                
                #ifdef MACINTOSH
                    SDKUtilities::convertToMacPath(total);
                #endif

				if(margin != 0 )
				{
					if(slugInfo.flowDir == 0)
					{//if(slugInfo.isAutoResize == -111)
						pageBounds.Top(pageBounds.Top());
						pageBounds.Bottom(pageBounds.Bottom());
						pageBounds.Left(pageBounds.Left()+margin);
						pageBounds.Right(pageBounds.Right()+margin);
					}
					else
					{
						pageBounds.Top(pageBounds.Top()+margin);
						pageBounds.Bottom(pageBounds.Bottom()+margin);
						pageBounds.Left(pageBounds.Left());
						pageBounds.Right(pageBounds.Right());
					}
					
					bool16 stat = copySelectedItems(newItem,pageBounds);
					if(!stat)
						break;

					UIDList tempUIDLIst(newItem.GetDataBase(), newItem.GetUID());
					NewImageFrameUIDList.push_back( newItem.GetUID());
                    NewImageFrameUIDListForMap.push_back(newItem);
                    
					if(ImportFileInFrame(newItem, total))
					{
						Flag=1;
						fitImageInBox(newItem);
					}
				}
				else
				{
					newItem = boxUIDRef;
					if(ImportFileInFrame(boxUIDRef, total))
					{		
						Flag=1;	
						fitImageInBox(boxUIDRef);
					}
				}

				margin = pageBounds.Right() -pageBounds.Left() +2;

				if((slugInfo.typeId == -99) )
					break;
			}
		}while(false);
	}

	return 1;
}

ErrorCode CDataSprayer::CreatePicturebox(UIDRef& newPageItem, UIDRef parent, PMRect rect, PMReal strokeWeight)
{
	ErrorCode returnValue = kFailure;
	do 
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			return returnValue;
		}
		ICommandSequence *sequence = CmdUtils::BeginCommandSequence();
		if (sequence == NULL)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::CreatePicturebox::sequence == NULL");				
			break;
		}

		UIDRef newSplineItem = Utils<IPathUtils>()->CreateRectangleSpline
		(parent, rect, INewPageItemCmdData::kGraphicFrameAttributes);

		const UIDList splineItemList(newSplineItem);

		InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->
		CreateStrokeWeightCommand(strokeWeight,&splineItemList,kTrue,kTrue));
		if(strokeSplineCmd==NULL)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::CreatePicturebox::Cannot create the command to stroke the spline?");
			break;
		}				
				
		if(CmdUtils::ProcessCommand(strokeSplineCmd)==kFailure)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::CreatePicturebox::Failed to stroke the spline?");
			break;
		}

		InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QueryActiveSwatchList());
		if(iSwatchList==NULL)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::CreatePicturebox::Cannot get swatch list?");
			break;
		}
		UID blackUID = iSwatchList->GetNoneSwatchUID();
		
		InterfacePtr<ICommand> applyStrokeCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeRenderingCommand(blackUID,&splineItemList,kTrue,kTrue));
		if(applyStrokeCmd==NULL)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::CreatePicturebox::Cannot create the command to render the stroke?");
			break;
		}

		if(CmdUtils::ProcessCommand(applyStrokeCmd)!=kSuccess)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::CreatePicturebox::Failed to render the stroke?");
			break;
		}
		
		CmdUtils::EndCommandSequence(sequence);
		newPageItem = newSplineItem;
		returnValue=kSuccess;
	}
	while(false);
	
	return returnValue;
}

void CDataSprayer::forceRecompose(InterfacePtr<ITextFrameColumn> textFrame)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return;
	}
	InterfacePtr<IFrameList> frameList(textFrame->QueryFrameList());
	if (frameList == NULL) 
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::forceRecompose::!frameList");	
		return;
	}
	
	int32 firstDamagedFrameIndex = frameList->GetFirstDamagedFrameIndex();
	if (firstDamagedFrameIndex >= 0)
	{
		UID frameUID = ::GetUID(textFrame);
		int32 frameIndex = frameList->GetFrameIndex(frameUID);
		if (frameIndex < 0)
			return;
		
		InterfacePtr<IFrameListComposer> frameListComposer(frameList, UseDefaultIID());
		if (frameListComposer == NULL) 
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::forceRecompose::frameListComposer == NULL");				
			return;
		}
		frameListComposer->RecomposeThru(frameIndex);
	}
}

bool16 CDataSprayer::isTextFrameOverset(const InterfacePtr<ITextFrameColumn> textFrame, int32& maxLimit)
{
	bool16 overset = kFalse;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}
	do {
		forceRecompose(textFrame);

		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
		if (textModel == NULL) 
			break;
		
		TextIndex storyFinalTextIndex = textModel->GetPrimaryStoryThreadSpan() - 1;
		
		TextIndex frameFinalTextIndex = 
			textFrame->TextStart() + textFrame->TextSpan() - 1;

		if (frameFinalTextIndex == storyFinalTextIndex) 
			break;
		
		if (frameFinalTextIndex == storyFinalTextIndex - 1) 
			break;
		
		overset = kTrue;
		maxLimit=frameFinalTextIndex;

	} while (false);
	return overset;
}

bool16 CDataSprayer::isFrameTagged(const UIDRef& boxRef)
{
	
	TagList tagList;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}

	InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::isFrameTagged::!itagReader");		
		return kFalse;
	}

	
	tagList=itagReader->getFrameTags(boxRef);

	if(tagList.size()==0)//Ordinary box
	{
		return kFalse;
	}

	if( tagList[0].elementId==-1 &&	tagList[0].parentId==-1 &&	tagList[0].parentTypeID==-1 &&	tagList[0].imgFlag==-1 && tagList[0].isTablePresent==kFalse &&
		tagList[0].sectionID==-1 &&	tagList[0].typeId==-1 && tagList[0].whichTab==-1)
	{	
		if(tagList.size() > 0)
		{
			for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
			{
				tagList[tagIndex].tagPtr->Release();
			}
		}
		return kFalse;//Our box, but not tagged
	}

	if(tagList.size() > 0)
	{
		for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
		{
			tagList[tagIndex].tagPtr->Release();
		}
	}
	return kTrue;
}

void CDataSprayer::getAllIds(double selectedID)
{
	idList.clear();
	if (pNode.getIsProduct() == 0)
	{
		idList.push_back(pNode.getParentId());	
		idList.push_back(pNode.getParentId());	
		idList.push_back(selectedID);	
		return;
	}
	double tempId=pNode.getParentId();

	double rootid = CurrentPublicationID;//oVal1.getParent_id();
	
	idList.push_back(rootid);		//Level 1 PF
	idList.push_back(tempId);					//Level 2 PG
	idList.push_back(selectedID);				//Level 3 PR

}

bool16 CDataSprayer::sprayForTaggedBox(const UIDRef& boxUIDRef)
{
	int32 CopyFlag= 0;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}
	InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogError("CDataSprayer :: sprayForTaggedBox: itagReader == NULL");
		return kFalse;
	}

	TagList tList=itagReader->getFrameTags(boxUIDRef);	
	TagStruct tStruct=tList[0];

	if(!tStruct.isTablePresent)
	{
		if(!this->removeAllText(boxUIDRef))
		{ 
			ptrIAppFramework->LogDebug("Breaks at removeAllText");
			if(tList.size() > 0)
			{
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					tList[tagIndex].tagPtr->Release();
				}
			}
			
			return kFalse;
		}
	}

	int numReplications=static_cast<int>(idList.size()-4);
	do
	{		
		if(tStruct.isTablePresent)
		{
			if(tStruct.isAutoResize == 1)
				CopyFlag=1;

			if(tStruct.isAutoResize == 2)
				CopyFlag=2;

			
			TableUtility oTableUtil(this);
			UIDRef tableUIDRef;
			if(oTableUtil.isTablePresent(boxUIDRef, tableUIDRef))
			{
				double tableId=tStruct.tableId;
				if(tStruct.typeId == -111 && tStruct.parentId == -1 && tStruct.whichTab == 3)
				{
					setTagParent(tStruct, idList[tStruct.whichTab-1], kTrue);//Re-Tag the box

				}
				else if((tStruct.typeId == -112 || tStruct.typeId == -113 || tStruct.typeId == -114) && tStruct.parentId == -1 && tStruct.whichTab == 4/*3*/) // Change For Kit spray
				{
					//CA("Before calling fillDataInKitComponentTable");
					if(tStruct.typeId == -112)  // Spraying Component Table
						oTableUtil.fillDataInKitComponentTable
						(tableUIDRef, pNode, tStruct, tableId,boxUIDRef, kFalse);
					else if(tStruct.typeId == -113) // Spraying XRef Table
						oTableUtil.fillDataInXRefTable
						(tableUIDRef, pNode, tStruct, tableId,boxUIDRef);
					else if(tStruct.typeId == -114) // Spraying Accessory Table
						oTableUtil.fillDataInAccessoryTable
						(tableUIDRef, pNode, tStruct, tableId,boxUIDRef);

					XMLReference boxXMLRef = tStruct.tagPtr->GetXMLReference();
					XMLTagAttributeValue xmlTagAttrVal;
					//collect all the attributes from the tag.
					//typeId,LanguageID are important
					getAllXMLTagAttributeValues(boxXMLRef,xmlTagAttrVal);					
					PMString parentIDstr("");
					parentIDstr.AppendNumber(PMReal(pNode.getPubId()));
					xmlTagAttrVal.parentID = parentIDstr;

					PMString parentTypeIDstr("");
					parentTypeIDstr.AppendNumber(PMReal(pNode.getTypeId()));
					xmlTagAttrVal.parentTypeID = parentTypeIDstr;

					PMString pbObjectId("");
					pbObjectId.AppendNumber(PMReal(pNode.getPBObjectID()));
					xmlTagAttrVal.pbObjectId =pbObjectId;

					PMString rowno("-1");
					xmlTagAttrVal.rowno =rowno;

					PMString sectionID("");
					sectionID.AppendNumber(PMReal(CurrentSubSectionID));
					xmlTagAttrVal.sectionID = sectionID;
					setAllXMLTagAttributeValues(boxXMLRef ,xmlTagAttrVal);
				}

				else if((tStruct.whichTab == 3 || tStruct.whichTab == 4 || tStruct.whichTab == 5) && tStruct.tableType == 3)
				{//else if((tStruct.whichTab == 3 || tStruct.whichTab == 4 || tStruct.whichTab == 5) && tStruct.elementId == -115)
					if((tStruct.whichTab == 3 && pNode.getIsProduct() == 1)||(tStruct.whichTab == 4 && pNode.getIsProduct() == 0) ||(tStruct.whichTab == 5 && pNode.getIsProduct() == 2))
						oTableUtil.fillDataInHybridTable(tableUIDRef, pNode, tStruct, tableId,boxUIDRef);
				}
				//else if(tStruct.whichTab == 3 && tStruct.typeId == -116)
				//{
				//	//CA("Spray For All Standard Table Stencil");
				//	UIDRef boxUIDRef1 = boxUIDRef;
				//	oTableUtil.fillDataInTableForAllStandardTable(pNode,/* tStruct,*/ tableId,boxUIDRef1);
				//}
				else if(tStruct.parentId==-1 && tStruct.whichTab == 3)
				{
					if(pNode.getIsProduct() == 1)
					{
						oTableUtil.fillDataInTable(tableUIDRef, pNode, tStruct, tableId,boxUIDRef);
						setTagParent(tStruct, idList[tStruct.whichTab-1], kTrue);//Re-Tag the box						
					}
					if(pNode.getIsProduct() == 0)
					{
						//CA("Item is selected to spray the Product Table");
					}
				}
				else if(tStruct.parentId==-1 && tStruct.whichTab == 4)
				{
					if(pNode.getIsProduct() == 0)
					{
						XMLReference boxXMLRef = tStruct.tagPtr->GetXMLReference();
						
						//CA("Custom Table Spray...");
						oTableUtil.FillDataInsideItemTableOfItem
						(
							boxXMLRef,
							tableUIDRef

						);
						InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
						if(!iSelectionManager)
						{
							return 0;
						}
						if (iSelectionManager->SelectionExists (kInvalidClass, ISelectionManager::kAnySelection)) {
							// Clear the selection
							iSelectionManager->DeselectAll(nil);
						}
						InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
						if (!layoutSelectionSuite) 
						{
							return 0;
						}
						layoutSelectionSuite->DeselectAllPageItems();
					}
					if(pNode.getIsProduct() == 1)
					{
						//CA("Product is selected to spray the Item Table");
					}
					
				 }
				
			}
			else if(tStruct.typeId!=-1 && tStruct.parentId==-1)
			{
				//CA("Tabbed table box");

				PMString textToInsert;
				double tableId=0;
				fillTaggedTableInBoxCS2(boxUIDRef,tStruct);
			}
			break;
		}
	
		if(tStruct.whichTab!=4)
		{
			this->fillDataInBox(boxUIDRef, tStruct, idList[tStruct.whichTab-1], kTrue, kTrue);//PG, PR, PF
			setTagParent(tStruct, idList[tStruct.whichTab-1], kTrue);//Re-Tag the box
			break;
		}
		
		for(int times=0; times<numReplications+1; times++)
		{
			this->fillDataInBox(boxUIDRef, tStruct, idList[3+times], kTrue);			
			
		}
		setTagParent(tStruct, idList[2], kTrue);//Retag with the PR id	
		
		
		
	}while(kFalse);

	//remove the selection inside the table.
	do
	{
		InterfacePtr<ISelectionManager> iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{ 
			break;
		}		

		if (iSelectionManager->SelectionExists (kInvalidClass, ISelectionManager::kAnySelection)) {
			// Clear the selection
			iSelectionManager->DeselectAll(nil);
		}

		InterfacePtr<ITableSelectionSuite> tableSelectionSuite(iSelectionManager, UseDefaultIID());
		if (!tableSelectionSuite) 
		{	
			break;				
		}			
		tableSelectionSuite->DeselectAll();
		
	}while(kFalse);

	if(CopyFlag == 1)
	{
		InterfacePtr<ICommand> fitFrameToContentCmd(CmdUtils::CreateCommand(kFitFrameToContentCmdBoss));
		ASSERT(fitFrameToContentCmd != nil);
		if (fitFrameToContentCmd == nil) {
		return kFalse;
		}
		fitFrameToContentCmd->SetItemList(UIDList(boxUIDRef));
		if (CmdUtils::ProcessCommand(fitFrameToContentCmd) != kSuccess) {
		ASSERT_FAIL("kFitFrameToContentCmdBoss failed");
		return kFalse;
		}
	}

	if(CopyFlag == 2)
	{
		//CA("Inside overflow");
		ThreadTextFrame ThreadObj;
		UIDRef CloneUIDRef(boxUIDRef);
		ThreadObj.DoCreateAndThreadTextFrame(CloneUIDRef);
	}

	if(tList.size() > 0)
	{
		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
	}
	return kTrue;
}


bool16 CDataSprayer::removeAllText(const UIDRef& boxUIDRef)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}

	UID textFrameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	}
	if (textFrameUID == kInvalidUID)
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::removeAllText::textFrameUID == kInvalidUID");	
		return kFalse;
	}

	InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
	if (graphicFrameHierarchy == nil) 
	{
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::removeAllText::!graphicFrameHierarchy");
		return kFalse;
	}
					
	InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
	if (!multiColumnItemHierarchy)
    {
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::removeAllText::!multiColumnItemHierarchy");
		return kFalse;
	}

	InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
	if (!multiColumnItemTextFrame)
    {
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::removeAllText::!multiColumnItemTextFrame");
		//CA("Its Not MultiColumn");
		return kFalse;
	}
	
	InterfacePtr<IHierarchy>
	frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
	if (!frameItemHierarchy)
    {
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::removeAllText::!frameItemHierarchy");
		return kFalse;
	}

	InterfacePtr<ITextFrameColumn>
	textFrame(frameItemHierarchy, UseDefaultIID());
	if (!textFrame)
    {
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::removeAllText::!textFrame");
		return kFalse;
	}

	InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
	if (textModel == NULL)
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::removeAllText::textModel == NULL");			
		return 0;
	}

	if((textModel->GetPrimaryStoryThreadSpan()-1)==0)//Box is already empty
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::removeAllText::Box is already empty");				
		return kTrue;
	}
	textModel->Delete(0, textModel->GetPrimaryStoryThreadSpan()-1);
	this->forceRecompose(textFrame);
	return kTrue;
}


void CDataSprayer::fitImageInBox(const UIDRef& boxUIDRef)
{
	do 
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			return;
		}
		InterfacePtr<ICommand> iAlignCmd(CmdUtils::CreateCommand(kFitContentPropCmdBoss));
		if(!iAlignCmd)
		{
			ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fitImageInBox::!iAlignCmd");		
			return;
		}

		InterfacePtr<IHierarchy> iHier(boxUIDRef, IID_IHIERARCHY);
		if(!iHier)
		{
			ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fitImageInBox::!iHier");				
			return;
		}

		if(iHier->GetChildCount()==0)//There may not be any image at all????
		{
			ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fitImageInBox::There may not be any image");						
			return;
		}
		UID childUID=iHier->GetChildUID(0);

		UIDRef newChildUIDRef(boxUIDRef.GetDataBase(), childUID);


		iAlignCmd->SetItemList(UIDList(newChildUIDRef));

		CmdUtils::ProcessCommand(iAlignCmd);
	} while (false);
}

void CDataSprayer::insertNewTable(const InterfacePtr<ITextModel>& textModel, 
												int numRows, 
												int numCols,
												TextIndex index,
												int32 len, 
												PMReal rowHeight,
												PMReal colWidth) const
{
	const CellType cellType = kTextContentType;
	// We use a command sequence so the effect appears to be a single thing.
	ErrorCode	eCode = kSuccess;
	IAbortableCmdSeq* cmdSeq = NULL;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return;
	}

	do {
		ASSERT(textModel != NULL);								// Pre: textModel != NULL
		if(textModel==NULL) {
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::insertNewTable::!textModel");
			break;
		}

		cmdSeq = CmdUtils::BeginAbortableCmdSeq();
		 
		ASSERT(cmdSeq);
		cmdSeq->SetName(PMString("Insert Table"));

		// Delete the old selection.
		if (len > 0)
		{	
			textModel->Delete(index, len);
			eCode = ErrorUtils::PMGetGlobalErrorCode();
		}
		ASSERT(eCode==kSuccess);
		if (eCode != kSuccess) {
			
			break;
		}
	
		// Create the new table.
		InterfacePtr<ICommand> command(CmdUtils::CreateCommand(kNewTableCmdBoss));
		ASSERT(command);
		if(command==NULL) {
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::insertNewTable::!command");
			break;
		}
		InterfacePtr<INewTableCmdData> tableData(command, UseDefaultIID());
		ASSERT(tableData);
		if(tableData==NULL) {
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::insertNewTable::!tableData");
			break;
		}
		tableData->Set(::GetDataBase(textModel), 
						::GetUID(textModel), 
						index,
						numRows, numCols, 
						rowHeight, 
						colWidth, 
						cellType);

		CmdUtils::ProcessCommand(command);
		eCode = ErrorUtils::PMGetGlobalErrorCode();
		ASSERT(eCode== kSuccess);
		if (eCode != kSuccess) {
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::insertNewTable::!eCode");
			break;
		}
	} while(kFalse);

	if(cmdSeq != NULL) {
		if (eCode == kSuccess) {
			CmdUtils::EndCommandSequence(cmdSeq);
		}
		else {
			CmdUtils::AbortCommandSequence(cmdSeq);
		}
	}
}


void CDataSprayer::FillPnodeStruct(PublicationNode pubNode , double SectionID, double PublicationID,double SubSectionID )
{
	PMString  dummyPubName = pubNode.getName();
	pNode.setAll(dummyPubName,pubNode.getPubId(),pubNode.getPBObjectID(),pubNode.getParentId(), pubNode.getLevel(),pubNode.getSequence(),pubNode.getChildCount(),pubNode.getHitCount(), pubNode.getTypeId(),pubNode.getReferenceId(),pubNode.getIsProduct(),pubNode.getIsONEsource());
    
	pNode.setNewProduct(pubNode.getNewProduct());
	pNode.setIsStarred(pubNode.getIsStarred());
	CurrentSectionID=SectionID;
	CurrentPublicationID=PublicationID;	
	CurrentSubSectionID= SubSectionID;
	if(SubSectionID == 0)
		CurrentSubSectionID=SectionID; 
	CurrentObjectTypeID=pubNode.getTypeId();

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CA(" ptrIAppFramework NULL ");
		return;
	}
	int32 level = ptrIAppFramework->getPM_Project_Levels();
	global_project_level = level ;
}

void CDataSprayer::SlugReader(const UIDRef& boxUIDRef,int32 rowIndex,int32 colIndex,double& Attr_id,double& Item_id)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return;
	}
	InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::SlugReader::!itagReader");	
		return;
	}
	TagList tList=itagReader->getFrameTags(boxUIDRef);
	TagStruct tStruct=tList[0];
	if(!tStruct.isTablePresent)
	{
		if(!this->removeAllText(boxUIDRef)){// CA("Breaks at removeAllText");
			
			if(tList.size() > 0)
			{
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					tList[tagIndex].tagPtr->Release();
				}
			}
			return;
		}

	}
	do
	{
        TableUtility oTableUtil(this);
        UIDRef tableUIDRef;

        double tableId=0;
        oTableUtil.TextSlugReader(boxUIDRef);
        
	}while(kFalse);

	if(tList.size() > 0)
	{
		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
	}

}

void CDataSprayer::NewSlugReader(InterfacePtr<ITableModel> &tblModel,int32 rowIndex,int32 colIndex,double& Attr_id,double& Item_id,InterfacePtr<ITableTextSelection> &tblTxtSel)
{
	//CA("CDataSprayer::NewSlugReader");
	
	//do
	//{
	//	//if(tStruct.isTablePresent)
	//	{	
	//		//TableUtility oTableUtil;
	//		//UIDRef tableUIDRef;

	//		//if(oTableUtil.isTablePresent(boxUIDRef, tableUIDRef))
	//		//{	
	//		//	PFTreeDataCache pCache;
	//		//	PublicationNode pNode;
	//		//	int32 tableId=0;
	//
	//		//	pCache.isExist(idList[2], pNode);
	//			//oTableUtil.fillDataInTable(tableUIDRef, pNode, tStruct.typeId, tableId);
	//			
	//			//CA("yanhantak");

	//			//oTableUtil.NewSlugreader(tblModel,rowIndex,colIndex,Attr_id,Item_id,tblTxtSel);
	//			//oTableUtil.TextSlugReader(tableUIDRef);
	//			//PMString as("Attr id is..");
	//			//as.AppendNumber(Attr_id);
	//			//CA(as);
	//		//}

	//		/*InterfacePtr<ITagWriter> tWrite
	//		((static_cast<ITagWriter*> (CreateObject(kTGWTagWriterBoss,IID_ITAGWRITER))));
	//		if(!tWrite)
	//			break;

			/*InterfacePtr<ITagWriter> tWrite
			((static_cast<ITagWriter*> (CreateObject(kTGWTagWriterBoss,IID_ITAGWRITER))));
			if(!tWrite)
				break;

			tWrite->NewSlugreader(tblModel,rowIndex,colIndex,Attr_id,Item_id,tblTxtSel);*/

//		}
//	}while(kFalse);

}
bool8  CDataSprayer::IsAttrInTextSlug(const UIDRef& tableUIDRef,double Attr_id)
{

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}
	InterfacePtr<ITagReader> itagReader
	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::IsAttrInTextSlug::!itagReader");	
		return kFalse;
	}

	TagList tList=itagReader->getFrameTags(tableUIDRef);

	TagStruct tStruct=tList[0];

	if(!tStruct.isTablePresent)
	{
        if(tList.size() > 0)
        {
            for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
            {
                tList[tagIndex].tagPtr->Release();
            }
        }
        return kFalse;

	}
	if(tList.size() > 0)
	{
		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
	}
	do
	{
		TableUtility oTableUtil(this);
		return oTableUtil.IsAttrInTextSlug(tableUIDRef,Attr_id);
	}while(kFalse);

	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}

return kFalse;
}

bool8 CDataSprayer::UpdateDataInTextSlug(const UIDRef& tableUIDRef,double Attr_id,PMString Table_col)
{

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}
	InterfacePtr<ITagReader> itagReader
	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::UpdateDataInTextSlug::!itagReader");	
		return kFalse;
	}

	TagList tList=itagReader->getFrameTags(tableUIDRef);

	TagStruct tStruct=tList[0];

	if(!tStruct.isTablePresent)
	{
		if(!this->removeAllText(tableUIDRef))
		{
			if(tList.size() > 0)
			{
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					tList[tagIndex].tagPtr->Release();
				}
			}
			return kFalse;
		}

	}
	if(tList.size() > 0)
	{
		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
	}
	do
	{
        TableUtility oTableUtil(this);
        return oTableUtil.UpdateDataInTextSlug(tableUIDRef,Attr_id,Table_col);
	}while(kFalse);

	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}
return kFalse;
}
ErrorCode CDataSprayer::ProcessSimpleCommand(const ClassID& commandClass, const UIDList& itemsIn, UIDList& itemsOut)
{
	ErrorCode status = kFailure;
	do
	{
		if (commandClass == kInvalidClass)
		{
			ASSERT_FAIL("ProcessSimpleCommand: commandClass is invalid"); break;
		}
		InterfacePtr<ICommand> cmd(CmdUtils::CreateCommand(commandClass));
		if (cmd == NULL)
		{
			ASSERT(cmd); 
			break;
		}
		if (status == kSuccess)
		{
			const UIDList& local_itemsOut = cmd->GetItemListReference();
			itemsOut = local_itemsOut;
		}
		else
		{
			//CA("status != kSuccess");
		//	ASSERT_FAIL("ProcessSimpleCommand: The command failed");
		}
	} while (false);
	return status;
}


void CDataSprayer::fillTaggedTableInBoxCS2(const UIDRef& curBox, TagStruct& slugInfo)
{
	AddHeaderToTable = kTrue;
 do
 {
	PMString result("");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return;
	}	
	double tableTypeId = slugInfo.typeId;
	UID textFrameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(curBox, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	}
	if (textFrameUID == kInvalidUID)
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillTaggedTableInBoxCS2::!textFrameUID");	
		return;
	}

     InterfacePtr<ITextModel>  textModel;
     getTextModelByBoxUIDRef(curBox , textModel);
	if (textModel == NULL)
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillTaggedTableInBoxCS2::!textModel");			
		return;
	}
	
	int32 tStart=0, tEnd=0;
	InterfacePtr<ITagReader> itagReader
			((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillTaggedTableInBoxCS2::!itagReader");	
		return ;
	}

	if(!itagReader->GetUpdatedTag(slugInfo))
		return;
	tStart=0;
	tEnd=slugInfo.endIndex-tStart;
	UIDRef txtMdlUIDRef =::GetUIDRef(textModel);
	PMString textToInsert("");
	PMString FinalString("");
	char* tempText=NULL;
	
	double objectId = pNode.getPubId(); //Product object
	double sectionid = -1;

	if(global_project_level == 3)
		sectionid = CurrentSubSectionID;
	if(global_project_level == 2)
		sectionid = CurrentSectionID;

	VectorScreenTableInfoPtr tableInfo = NULL;
	if(pNode.getIsONEsource())
	{
		// For ONEsource
		//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(objectId);
	}else
	{
		//publication mode is selected
		tableInfo=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, objectId, slugInfo.languageID, kFalse);
	}

	if(!tableInfo)
	{	ptrIAppFramework->LogDebug("CDataSprayer::fillTaggedTableInBoxCS2:  tableinfo is NULL");
		break;
	}

	if(tableInfo->size()==0){ //CA(" tableInfo->size()==0");
		break;
	}
	
	int32 numRows = -1;
	int32 numCols = -1;

	CItemTableValue oTableValue;
	VectorScreenTableInfoValue::iterator it;
	bool16 typeidFound=kFalse;
	for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
	{
		oTableValue = *it;

		if(oTableValue.getTableTypeID() == tableTypeId)
		{
			typeidFound=kTrue;				
			break;
		}
	}
	
	if(tableInfo)
	{
		tableInfo->clear();
		delete tableInfo;
	}

	if(!typeidFound){ //CA("!typeidFound");
		break;
	}
	numRows = static_cast<int32>(oTableValue.getTableData().size());
	if(numRows<=0)
		break;
	numCols =static_cast<int32>(oTableValue.getTableHeader().size());
	if(numCols<=0)
		break;
	bool8 isTranspose = oTableValue.getTranspose();

	vector<double> vec_tableheaders;
	vector<double> vec_items;

	vec_tableheaders = oTableValue.getTableHeader();
	vec_items = oTableValue.getItemIds();
	ErrorCode Err = kFailure;
	int32 CurrTextIndex =-1; 
	CurrTextIndex = tStart;
	VecorTabbeddataStruct vectorTabbedDataList;
	vectorTabbedDataList.clear();	

	if(isTranspose)
	{
		int32 i=1, j=0;
	
		bool8 FLAG=kFalse;
		vector<vector<PMString> > vec_tablerows;
		vec_tablerows = oTableValue.getTableData();		
		
		for(i=0;i<vec_tableheaders.size();i++)
		{
			if(i!=0){
				result.Append("\r");
				CurrTextIndex = CurrTextIndex+3;
			}
			int32 k=0;
			if(AddHeaderToTable)
			{
				PMString tempBuffer("");
				textToInsert.Clear();

				PMString dispname = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],slugInfo.languageID);
				tempBuffer.Append(dispname);
                
                textToInsert=tempBuffer;

				int32 NoofChar = textToInsert.NumUTF16TextChars();			
			
				TabbedDataStruct TbbDataStruct;
				TbbDataStruct.StartIndex = CurrTextIndex;
				TbbDataStruct.EndIndex = CurrTextIndex + NoofChar;
				TbbDataStruct.ItemID = -2;
				TbbDataStruct.rowno = i;
				TbbDataStruct.colno = k;
				CurrTextIndex = CurrTextIndex + NoofChar;
							
				TbbDataStruct.stencileInfo.elementId = vec_tableheaders[i];
				TbbDataStruct.stencileInfo.elementName = dispname;

				PMString TableColName("");
				TableColName.Append("ATTRID_");
				TableColName.AppendNumber(PMReal(vec_tableheaders[i]));
				
				TbbDataStruct.stencileInfo.TagName = TableColName;
				TbbDataStruct.stencileInfo.LanguageID = slugInfo.languageID;
				TbbDataStruct.stencileInfo.header = 1;
				TbbDataStruct.stencileInfo.parentId = pNode.getPubId();
				TbbDataStruct.stencileInfo.parentTypeId = pNode.getTypeId();
				TbbDataStruct.stencileInfo.whichTab = slugInfo.whichTab;
				TbbDataStruct.stencileInfo.imgFlag = 0; 

				if(global_project_level == 3)
					TbbDataStruct.stencileInfo.sectionID = CurrentSubSectionID;
				if(global_project_level == 2)
					TbbDataStruct.stencileInfo.sectionID = CurrentSectionID;

				vectorTabbedDataList.push_back(TbbDataStruct);
				result.Append(textToInsert);
				result.Append("\t");
				CurrTextIndex = CurrTextIndex+3;
				k++;
			}

			vector<vector<PMString> >::iterator it1;
			
			for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
			{
				PMString tempBuffer("");
				textToInsert.Clear();
				vector<PMString>::iterator it2;
				it2=(*it1).begin();
				it2 = it2+i+1;
				tempBuffer.Append((*it2));

                textToInsert=tempBuffer;
				int32 NoofChar1 = textToInsert.NumUTF16TextChars();			
			
				TabbedDataStruct TbbDataStruct;
				TbbDataStruct.StartIndex = CurrTextIndex;
				TbbDataStruct.EndIndex = CurrTextIndex + NoofChar1;
				
				if(AddHeaderToTable)
					TbbDataStruct.ItemID = vec_items[k-1];
				else
					TbbDataStruct.ItemID = vec_items[k];

				TbbDataStruct.rowno = i;
				TbbDataStruct.colno = k;
				CurrTextIndex = CurrTextIndex + NoofChar1;

				TbbDataStruct.stencileInfo.elementId = vec_tableheaders[i];

				PMString dispname = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],slugInfo.languageID);
				TbbDataStruct.stencileInfo.elementName = dispname;

				PMString TableColName("");
				TableColName.Append("ATTRID_");
				TableColName.AppendNumber(PMReal(vec_tableheaders[i]));
				
				TbbDataStruct.stencileInfo.TagName = TableColName;
				TbbDataStruct.stencileInfo.LanguageID = slugInfo.languageID;

				if(AddHeaderToTable)
					TbbDataStruct.stencileInfo.typeId  = vec_items[k-1];
				else
					TbbDataStruct.stencileInfo.typeId  = vec_items[k];
				TbbDataStruct.stencileInfo.parentId = pNode.getPubId();
				TbbDataStruct.stencileInfo.parentTypeId = pNode.getTypeId();

				TbbDataStruct.stencileInfo.whichTab = slugInfo.whichTab;
				TbbDataStruct.stencileInfo.imgFlag = 0;
				
				if(global_project_level == 3)
					TbbDataStruct.stencileInfo.sectionID = CurrentSubSectionID;
				if(global_project_level == 2)
					TbbDataStruct.stencileInfo.sectionID = CurrentSectionID;

				vectorTabbedDataList.push_back(TbbDataStruct);
				result.Append(textToInsert);

				if(it1!=vec_tablerows.end()-1){
					result.Append("\t");
					CurrTextIndex = CurrTextIndex+3;
				}
				k++;
			}
		}

		WideString* myText=new WideString(result);
		Err = kFailure;

		boost::shared_ptr<WideString> insertText(myText);
		InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
    	ASSERT(textModelCmds);
    	if (!textModelCmds) {
			break;
		}
		InterfacePtr<ICommand> insertCmd(textModelCmds->InsertCmd(tStart, insertText));
		ASSERT(insertCmd);
		if (!insertCmd) {
			break;
		}
		ErrorCode status = CmdUtils::ProcessCommand(insertCmd);
		CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);

		XMLReference ParentXmlRef = slugInfo.tagPtr->GetXMLReference();
		for(int j=0; j<vectorTabbedDataList.size(); j++)
		{
			XMLReference newTag;
			Utils<IXMLElementCommands>()->CreateElement(WideString(vectorTabbedDataList[j].stencileInfo.TagName), txtMdlUIDRef, vectorTabbedDataList[j].StartIndex,  vectorTabbedDataList[j].EndIndex,ParentXmlRef, &newTag);
			TableUtility OTUtil(this);
			OTUtil.attachAttributes(&newTag, kFalse, vectorTabbedDataList[j].stencileInfo, vectorTabbedDataList[j].rowno, vectorTabbedDataList[j].colno);
		}
		delete myText;
	}
	else
	{
		int32 i=0, j=0;
		bool8 FLAG=kFalse;
		int32 k =0;
		if(AddHeaderToTable)
		{
			for(int32 i=0;i<vec_tableheaders.size();i++)
			{	
				PMString tempBuffer("");
				textToInsert.Clear();
				PMString dispname = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],slugInfo.languageID); // new add by vaibhav
			
				tempBuffer.Append(dispname);
                textToInsert=tempBuffer;

				int32 NoofChar = textToInsert.NumUTF16TextChars();			
				TabbedDataStruct TbbDataStruct;
				TbbDataStruct.StartIndex = CurrTextIndex;
				TbbDataStruct.EndIndex = CurrTextIndex + NoofChar;
				TbbDataStruct.ItemID = -2;
				TbbDataStruct.rowno = i;
				TbbDataStruct.colno = k;
				CurrTextIndex = CurrTextIndex + NoofChar;
							
				TbbDataStruct.stencileInfo.elementId = vec_tableheaders[i];
				TbbDataStruct.stencileInfo.elementName = dispname;
				PMString TableColName("");
                TableColName.Append("ATTRID_");
                TableColName.AppendNumber(PMReal(vec_tableheaders[i]));

				TbbDataStruct.stencileInfo.TagName = TableColName;
				TbbDataStruct.stencileInfo.LanguageID = slugInfo.languageID;
				TbbDataStruct.stencileInfo.header = 1;
				TbbDataStruct.stencileInfo.parentId = pNode.getPubId();
				TbbDataStruct.stencileInfo.parentTypeId = pNode.getTypeId();
				TbbDataStruct.stencileInfo.whichTab = slugInfo.whichTab;

				TbbDataStruct.stencileInfo.imgFlag = 0;

				if(global_project_level == 3)
					TbbDataStruct.stencileInfo.sectionID = CurrentSubSectionID;
				if(global_project_level == 2)
					TbbDataStruct.stencileInfo.sectionID = CurrentSectionID;

				vectorTabbedDataList.push_back(TbbDataStruct);
				result.Append(textToInsert);			
				k++;
				
				if(i < vec_tableheaders.size()-1){
					result.Append("\t");
					CurrTextIndex = CurrTextIndex+3;
				}
				else{
					result.Append("\r");
					CurrTextIndex = CurrTextIndex+3;
				}
				FLAG=kTrue;
			}
		}
		if(AddHeaderToTable)
			i=1, j=0;
		else
			i=0, j=0;
		vector<vector<PMString> > vec_tablerows;
		vec_tablerows = oTableValue.getTableData();
		vector<vector<PMString> >::iterator it1;
		
		for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
		{
			
			vector<PMString>::iterator it2;
			k=0;
			for(it2=(*it1).begin();it2!=(*it1).end();it2++)
			{
				PMString tempBuffer("");
				textToInsert.Clear();
				if((j)>numCols)
					break;

				tempBuffer.Append((*it2));

                textToInsert=tempBuffer;
			
				int32 NoofChar1 = textToInsert.NumUTF16TextChars();			
				TabbedDataStruct TbbDataStruct;
				TbbDataStruct.StartIndex = CurrTextIndex;
				TbbDataStruct.EndIndex = CurrTextIndex + NoofChar1;
				
				if(AddHeaderToTable)
					TbbDataStruct.ItemID = vec_items[i-1];
				else
					TbbDataStruct.ItemID = vec_items[i];

				TbbDataStruct.rowno = i;
				TbbDataStruct.colno = j;
				CurrTextIndex = CurrTextIndex + NoofChar1;

				TbbDataStruct.stencileInfo.elementId = vec_tableheaders[j];
				PMString TableColName("");
				TableColName.Append("ATTRID_");
				TableColName.AppendNumber(PMReal(vec_tableheaders[j]));
                
				TbbDataStruct.stencileInfo.TagName = TableColName;
				
				if(AddHeaderToTable)
					TbbDataStruct.stencileInfo.typeId  = vec_items[i-1];
				else
					TbbDataStruct.stencileInfo.typeId  = vec_items[i];
				
				TbbDataStruct.stencileInfo.LanguageID = slugInfo.languageID;
				TbbDataStruct.stencileInfo.parentId = pNode.getPubId();
				TbbDataStruct.stencileInfo.parentTypeId = pNode.getTypeId();
				TbbDataStruct.stencileInfo.whichTab = slugInfo.whichTab;
				TbbDataStruct.stencileInfo.imgFlag = 0; 
				if(global_project_level == 3)
					TbbDataStruct.stencileInfo.sectionID = CurrentSubSectionID;
				if(global_project_level == 2)
					TbbDataStruct.stencileInfo.sectionID = CurrentSectionID;

				vectorTabbedDataList.push_back(TbbDataStruct);
				result.Append(textToInsert);
				k++;

				if(it2!=(*it1).end()-1){
					result.Append("\t");
					CurrTextIndex = CurrTextIndex+3;
				}
			j++;
			}
			if(it1+1!=vec_tablerows.end())
			{
				result.Append("\r");
				CurrTextIndex = CurrTextIndex+3;
			}

			i++;
			j=0;
		}

		WideString* myText=new WideString(result);
		//if(iConverter)
		//	iConverter->ChangeQutationMarkONOFFState(kFalse);
		Err = kFailure;

		boost::shared_ptr<WideString> insertText(myText);
		InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
    	ASSERT(textModelCmds);
    	if (!textModelCmds) {
			break;
		}
		InterfacePtr<ICommand> insertCmd(textModelCmds->InsertCmd(tStart, insertText));
		ASSERT(insertCmd);
		if (!insertCmd) {
			break;
		}
		ErrorCode status = CmdUtils::ProcessCommand(insertCmd);

		CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);

		//if(iConverter)
		//	iConverter->ChangeQutationMarkONOFFState(kTrue);

		XMLReference ParentXmlRef = slugInfo.tagPtr->GetXMLReference();
		for(int j=0; j<vectorTabbedDataList.size(); j++)
		{
			XMLReference newTag;
			Utils<IXMLElementCommands>()->CreateElement(WideString(vectorTabbedDataList[j].stencileInfo.TagName), txtMdlUIDRef, vectorTabbedDataList[j].StartIndex,  vectorTabbedDataList[j].EndIndex,ParentXmlRef, &newTag);
			TableUtility OTUtil(this);
			OTUtil.attachAttributes(&newTag, kFalse, vectorTabbedDataList[j].stencileInfo, vectorTabbedDataList[j].rowno, vectorTabbedDataList[j].colno);

		}
		delete myText;
	}	
	
 }while(0);
}

//another approach 15Jun
void collectItemIDsForProduct(vector<double> & FinalItemIds)
{
	do
	{
		double sectionid = -1;
		if(global_project_level == 3)
			sectionid = CurrentSubSectionID;
		if(global_project_level == 2)
			sectionid = CurrentSectionID;
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}
		VectorScreenTableInfoPtr tableInfo = NULL;
		if(pNode.getIsONEsource())
		{
			// For ONEsource
			//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(pNode.getPubId());
		}else
		{
			double tempLangId = ptrIAppFramework->getLocaleId(); // Added by Apsiva need to rectify later
			//For publication mode is selected
			tableInfo= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, pNode.getPubId(), tempLangId,  kTrue);
		}
		if(!tableInfo)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::collectItemIDsForProduct::tableInfo is NULL");	
			break;
		}
		if(tableInfo->size()==0)
		{ 
			break;
		}
		CItemTableValue oTableValue;
		VectorScreenTableInfoValue::iterator it;

		bool16 typeidFound=kFalse;
		vector<double> vec_items;
		FinalItemIds.clear();

		for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
		{//for tabelInfo start				
			oTableValue = *it;				
			vec_items = oTableValue.getItemIds();
		
			if(FinalItemIds.size() == 0)
			{
				FinalItemIds = vec_items;
			}
			else
			{
				for(int32 i=0; i<vec_items.size(); i++)
				{	bool16 Flag = kFalse;
					for(int32 j=0; j<FinalItemIds.size(); j++)
					{
						if(vec_items[i] == FinalItemIds[j])
						{
							Flag = kTrue;
							break;
						}				
					}
					if(!Flag)
						FinalItemIds.push_back(vec_items[i]);
				}
			}
		}//for tabelInfo end
		if(tableInfo)
		{
			tableInfo->clear();
			delete tableInfo;
		}
	}while(kFalse);

}//end another approach 15Jun


//16Jun..items from product dropdown.
void CDataSprayer::sprayProductItemTableScreenPrintInTabbedTextForm(TagStruct & tagInfo)
{
	int32 CopyFlag = 0;
	XMLReference tagInfoXMLReference = tagInfo.tagPtr->GetXMLReference();
	int numReplications=static_cast<int>(idList.size()-4);//These many times we have to replicate the text for the ITEMs text

	PMString elementName,colName;
	vector<double> FinalItemIds;
	bool16 firstTabbedTextTag = kTrue;
	tabbedTagStructList.clear();

	UIDRef txtModelUIDRef = UIDRef::gNull;
	InterfacePtr<ITextModel> textModel;
	
	TextIndex startPos=-1;
	TextIndex endPos = -1;

	int32 replaceItemWithTabbedTextFrom = -1;
	int32 replaceItemWithTabbedTextTo = -1;
	PMString tabbedTextData;
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return;
	}
	do{
		if(tagInfo.whichTab == 3 && tagInfo.parentId == -1 && tagInfo.isTablePresent == kTrue)
		{//start if1

			UID textFrameUID = kInvalidUID;
			InterfacePtr<IGraphicFrameData> graphicFrameDataOne(tagInfo.curBoxUIDRef, UseDefaultIID());
			if (graphicFrameDataOne) 
			{
				textFrameUID = graphicFrameDataOne->GetTextContentUID();
			}
			if (textFrameUID == kInvalidUID)
			{
				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayProductItemTableScreenPrintInTabbedTextForm::!textFrameUID");	
				break;
			}

            InterfacePtr<ITextModel>  temptextModel;
            getTextModelByBoxUIDRef(tagInfo.curBoxUIDRef , temptextModel);
			if (temptextModel == NULL)
			{
				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayProductItemTableScreenPrintInTabbedTextForm::!temptextModel");	
				break;
			}
			textModel = temptextModel;

			txtModelUIDRef = ::GetUIDRef(textModel);

			double sectionid = -1;
			if(global_project_level == 3)
				sectionid = CurrentSubSectionID;
			if(global_project_level == 2)
				sectionid = CurrentSectionID;
			
			
			PMString attrVal;
			attrVal.AppendNumber(PMReal(pNode.getPubId()));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLReference, WideString("parentID"),WideString(attrVal));
			
			attrVal.Clear();
			attrVal.AppendNumber(PMReal(CurrentObjectTypeID));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLReference, WideString("parentTypeID"),WideString(attrVal));
			
			attrVal.Clear();
			attrVal.AppendNumber(PMReal(sectionid));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLReference, WideString("sectionID"),WideString(attrVal));
			
			attrVal.Clear();
			attrVal.AppendNumber(PMReal(pNode.getPBObjectID()));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLReference, WideString("pbObjectId"),WideString(attrVal));

			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLReference, WideString("tableFlag"),WideString("-11"));
			//change the ID from -1 to -101, as -1 is elementId for productNumber.
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLReference, WideString("ID"),WideString("-101"));

			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLReference, WideString("childTag"),WideString("1"));

			int32 tagStartPos = 0;
			int32 tagEndPos =0; 			
			Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&tagStartPos,&tagEndPos);
			tagStartPos = tagStartPos + 1;
			tagEndPos = tagEndPos -1;

			tabbedTextData.Clear();
			replaceItemWithTabbedTextFrom = tagStartPos;
			replaceItemWithTabbedTextTo	= tagEndPos;
			bool16 isTableDataPresent = kTrue;
			CItemTableValue oTableValue;

			do
			{
				VectorScreenTableInfoPtr tableInfo = NULL;
				TableSourceInfoValue *tableSourceInfoValueObj;
				bool16 isCallFromTS = kFalse;
				InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
				if(ptrTableSourceHelper != nil)
				{
					tableSourceInfoValueObj=ptrTableSourceHelper->getTableSourceInfoValueObj();
					if(tableSourceInfoValueObj)
					{
						isCallFromTS = tableSourceInfoValueObj->getIsCallFromTABLEsource();
						if(isCallFromTS)
						{
							tableInfo=tableSourceInfoValueObj->getVectorScreenTableInfoPtr();
							if(!tableInfo)
								break;
						}
					}
				}
				
				bool16 typeidFound=kFalse;
				if(isCallFromTS)	/// This is called from TABLESource
				{
					VectorScreenTableInfoValue::iterator it;
					
					for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
					{
						oTableValue = *it;
						if(oTableValue.getTableTypeID() == tagInfo.typeId && tableSourceInfoValueObj->getVec_TableType_ID().at(0) == tagInfo.typeId && oTableValue.getTableID() == tableSourceInfoValueObj->getVec_Table_ID().at(0))
						{   				
							typeidFound=kTrue;				
							break;
						}
					}
					if(!typeidFound)
					{
						isTableDataPresent = kFalse;
						break;
					}
				}
				else
				{	/// This is called from Conten Sprayer
					VectorScreenTableInfoPtr tableInfo = NULL;
					if(pNode.getIsONEsource())
					{
						// If ONEsource mode is selected  
						//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(pNode.getPubId());
					}else
					{
						//CA("pNode.getIsONEsource() == kFalse");
						//For publication mode is selected
						tableInfo=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, pNode.getPubId(), tagInfo.languageID, kFalse);
					}
					if(!tableInfo)
					{
						isTableDataPresent = kFalse;
						ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayProductItemTableScreenPrintInTabbedTextForm::!tableInfo");	
						break;
					}

					if(tableInfo->size()==0)
					{
						isTableDataPresent = kFalse;
						break;
					}
					
					VectorScreenTableInfoValue::iterator it;
					
					for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
					{
						oTableValue = *it;
						if(oTableValue.getTableTypeID() == tagInfo.typeId)
						{   				
							typeidFound=kTrue;				
							break;
						}
					}
					
					if(tableInfo)
					{
						tableInfo->clear();
						delete tableInfo;
					}

					if(!typeidFound)
					{
						isTableDataPresent = kFalse;
						break;
					}
				}
			}while(kFalse);
			if(isTableDataPresent == kFalse)
			{				
				PMString textToInsert("");

                textToInsert=tabbedTextData;

                textToInsert.ParseForEmbeddedCharacters();
				boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
				ReplaceText(textModel,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);
				break;
			}

			attrVal.Clear();
			attrVal.AppendNumber(PMReal(oTableValue.getTableID()));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLReference, WideString("tableId"),WideString(attrVal));

			vector<double> vectorOfTableHeader = oTableValue.getTableHeader();
			vector<vector<PMString> >  vectorOfRowByRowTableData = oTableValue.getTableData();
			vector<double> vec_items = oTableValue.getItemIds();
			bool8 isTranspose = oTableValue.getTranspose();
			vector<vector<SlugStruct> > RowList;
			vector<SlugStruct> listOfNewTagsAdded;

			int32 noOfColumns =static_cast<int32>(vectorOfTableHeader.size());
			int32 noOfRows = static_cast<int32>(vec_items.size());

			//Check for the tableHeaderFlag at colno
			int32 headerFlag = tagInfo.header;
			//update the attributes of the parent tag
			
			if(!isTranspose)
			{				
				/*  
				In case of Transpose == kFalse..the table gets sprayed like 
					Header11 Header12  Header13 ...
					Data11	 Data12	   Data13
					Data21	 Data22	   Data23
					....
					....
					So first row is Header Row and subsequent rows are DataRows.
				*/
				//The for loop below will spray the Table Header in left to right fashion.
				if(headerFlag == 1)//Spray table with the headers.
				{
					for(int32 headerIndex = 0;headerIndex < noOfColumns;headerIndex++)
					{
						
						double elementId = vectorOfTableHeader[headerIndex];

						SlugStruct newTagToBeAdded;
						newTagToBeAdded.elementId = elementId;
						newTagToBeAdded.typeId = -1;
						newTagToBeAdded.header = 1;
						newTagToBeAdded.parentId = pNode.getPubId();
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.parentTypeId = pNode.getTypeId();
						newTagToBeAdded.elementName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );
						newTagToBeAdded.TagName = prepareTagName(newTagToBeAdded.elementName);;
						newTagToBeAdded.LanguageID = tagInfo.languageID;
						newTagToBeAdded.whichTab =4 ;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.sectionID = sectionid ;
						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
						newTagToBeAdded.tableFlag = -12;
						newTagToBeAdded.row_no=-1;
						newTagToBeAdded.col_no= -1;
						newTagToBeAdded.tableId = oTableValue.getTableID();
						newTagToBeAdded.pbObjectId = pNode.getPBObjectID();
						newTagToBeAdded.tableType = tagInfo.tableType;

						
						PMString insertPMStringText;
						insertPMStringText.Append(newTagToBeAdded.elementName);
                        insertPMStringText=insertPMStringText;
                        
                        if(headerIndex != noOfColumns - 1)
						{
							insertPMStringText.Append("\t");						
						}
						else
						{
							insertPMStringText.Append("\r");
						}


						tabbedTextData.Append(insertPMStringText);
						
						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
				
                        tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
						
                        newTagToBeAdded.tagStartPos=tagStartPos;
                        newTagToBeAdded.tagEndPos=tagEndPos;
                        listOfNewTagsAdded.push_back(newTagToBeAdded);
                        tagStartPos = tagEndPos+ 1 + 2;
			
					}
				}

				//This for loop will spray the Item_copy_attributes value
				for(int32 itemIndex = 0;itemIndex <noOfRows;itemIndex++)
				{
					
					double itemId = vec_items[itemIndex];
					for(int32 headerIndex = 0;headerIndex <noOfColumns;headerIndex++)
					{
						double elementId = vectorOfTableHeader[headerIndex];
						PMString tagName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );

						SlugStruct newTagToBeAdded;
						newTagToBeAdded.elementId = elementId;
						newTagToBeAdded.typeId = -1;
						newTagToBeAdded.header = -1;
						newTagToBeAdded.childId = itemId;
						newTagToBeAdded.childTag =1;
						newTagToBeAdded.parentId = pNode.getPubId();
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.parentTypeId = pNode.getTypeId();
						
	
						/*if(ptrIAppFramework->ATTRIBUTECache_isItemAttributeEventSpecific(elementId) == kTrue)
						{
							VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(itemId,elementId,tagInfo.sectionID);
							if(vecPtr != NULL)
								if(vecPtr->size()> 0)
									newTagToBeAdded.elementName = vecPtr->at(0).getObjectValue();
						}
						else */if(elementId == -401 || elementId == -402 || elementId == -403){
							newTagToBeAdded.elementName = ""; // ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(itemId,elementId, tagInfo.languageID);
						}
						else
						{
							newTagToBeAdded.elementName =ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemId,elementId,tagInfo.languageID, CurrentSectionID, kFalse);
						}
						newTagToBeAdded.TagName = prepareTagName(tagName);
						newTagToBeAdded.LanguageID = tagInfo.languageID;
						newTagToBeAdded.whichTab =4;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.sectionID = sectionid;
						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
						newTagToBeAdded.tableFlag = -12;
						newTagToBeAdded.row_no=-1;
						newTagToBeAdded.col_no=-1;
						newTagToBeAdded.tableId = oTableValue.getTableID();
						newTagToBeAdded.pbObjectId = pNode.getPBObjectID();
						newTagToBeAdded.tableType = tagInfo.tableType;

						PMString insertPMStringText;
						insertPMStringText.Append(newTagToBeAdded.elementName);
                        insertPMStringText=insertPMStringText;

						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
						if(headerIndex != noOfColumns - 1)
						{
							insertPMStringText.Append("\t");
							
						}
						else if(itemIndex != noOfRows-1)
						{
							insertPMStringText.Append("\r");
						}
						tabbedTextData.Append(insertPMStringText);
										
                        tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
                        newTagToBeAdded.tagStartPos=tagStartPos;
                        newTagToBeAdded.tagEndPos=tagEndPos;
                        listOfNewTagsAdded.push_back(newTagToBeAdded);

                        tagStartPos = tagEndPos+ 1 + 2;
			
					}

				}				
			}//end of if IsTranspose == kFalse
			else
			{//else of if IsTranspose ==kFalse..i.e IsTranspose == kTrue. Headers are in first column
				/*  
				In case of Transpose == kTrue..the table gets sprayed like 

					Header11 Data11 Data12 Data13..
					Header22 Data21 Data22 Data23..
					Header33 Data31 Data32 Data33..
					....
					....
				So First Column is HeaderColumn and subsequent columns are Data columns.
				*/
				
				for(int32 headerIndex = 0;headerIndex <noOfColumns;headerIndex++)
				{
					
					double elementId = vectorOfTableHeader[headerIndex];
					//noOfRows in the for loop below is noOfRows + 1.
					//As we have to add first column which is of table headers.
					//In the earlier part(i.e in the if block of IsTranspose == kFalse)
					//we have added the header row in a separate loop.
					for(int32 itemIndex = 0;itemIndex <noOfRows + 1;itemIndex++)
					{
                        double itemId =-1;
						PMString tagName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );
						PMString elementName;
						SlugStruct newTagToBeAdded;
						if(itemIndex == 0 && headerFlag == 1)//Spray the table with the headers
						{
							elementName = tagName;
							newTagToBeAdded.header = headerFlag;
						}
						else
						{
							if(itemIndex == 0 && headerFlag != 1)
							{
								continue;
							}
							itemId = vec_items[itemIndex-1];
							if(elementId == -401 || elementId == -402 || elementId == -403){
								elementName = ""; //ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(itemId,elementId, tagInfo.languageID);
							}
							/*else if(ptrIAppFramework->ATTRIBUTECache_isItemAttributeEventSpecific(elementId) == kTrue){
								VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(itemId,elementId,tagInfo.sectionID);
								if(vecPtr != NULL)
									if(vecPtr->size()> 0)
										elementName = vecPtr->at(0).getObjectValue();
							}*/
							else
							{
								elementName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemId,elementId,tagInfo.languageID, CurrentSectionID, kFalse);
							}
							newTagToBeAdded.header = -1;
						}			
						newTagToBeAdded.elementId = elementId;
						newTagToBeAdded.typeId = -1;
						newTagToBeAdded.childId = itemId;
						newTagToBeAdded.childTag =1;
						newTagToBeAdded.parentId = pNode.getPubId();
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.parentTypeId = pNode.getTypeId();
						newTagToBeAdded.elementName = elementName;
						newTagToBeAdded.TagName = prepareTagName(tagName);
						newTagToBeAdded.LanguageID = tagInfo.languageID;
						newTagToBeAdded.whichTab =4;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.sectionID = sectionid;
						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
						newTagToBeAdded.tableFlag = -12;
						newTagToBeAdded.row_no=-1;
						newTagToBeAdded.col_no= -1;
						newTagToBeAdded.tableId = oTableValue.getTableID();
						newTagToBeAdded.pbObjectId = pNode.getPBObjectID();
						newTagToBeAdded.tableType = tagInfo.tableType;

						PMString insertPMStringText;
						insertPMStringText.Append(newTagToBeAdded.elementName);
                        insertPMStringText=insertPMStringText;

						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
						if(itemIndex != noOfRows)
						{
							insertPMStringText.Append("\t");
						}
						else
						{
							insertPMStringText.Append("\r");
						}
						tabbedTextData.Append(insertPMStringText);
									
						tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
						newTagToBeAdded.tagStartPos=tagStartPos;
						newTagToBeAdded.tagEndPos=tagEndPos;
						listOfNewTagsAdded.push_back(newTagToBeAdded);
						tagStartPos = tagEndPos+ 1 + 2;
					}
				}
			}

			PMString textToInsert("");
            textToInsert=tabbedTextData;

            textToInsert.ParseForEmbeddedCharacters();
			boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
			ReplaceText(textModel,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);

			for(int32 indexOfTagToBeAdded = 0;indexOfTagToBeAdded <listOfNewTagsAdded.size();indexOfTagToBeAdded++)
			{					
				SlugStruct & newTagToBeAdded = listOfNewTagsAdded[indexOfTagToBeAdded];
				XMLReference createdElement;
				PMString NewTagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
				WideString tagNamewstr = WideString(NewTagName);
				Utils<IXMLElementCommands>()->CreateElement(tagNamewstr , txtModelUIDRef,newTagToBeAdded.tagStartPos,newTagToBeAdded.tagEndPos,kInvalidXMLReference, &createdElement);
				addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
			}
			
		}//end if1
	}while(kFalse);

//			}//end for1
}//end of function


/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////3Aug Item Table Preset Start ////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

void CDataSprayer::sprayItemItemTableScreenPrintInTabbedTextForm(TagStruct & tagInfo)
{
	int32 CopyFlag=0;
	XMLReference tagInfoXMLRef = tagInfo.tagPtr->GetXMLReference();
	int numReplications=static_cast<int>(idList.size()-4);//These many times we have to replicate the text for the ITEMs text

	PMString elementName,colName;
	//7Jun
	vector<double> FinalItemIds;
	bool16 firstTabbedTextTag = kTrue;
	tabbedTagStructList.clear();

	UIDRef txtModelUIDRef = UIDRef::gNull;
	InterfacePtr<ITextModel> textModel;
	
	TextIndex startPos=-1;
	TextIndex endPos = -1;

	int32 replaceItemWithTabbedTextFrom = -1;
	int32 replaceItemWithTabbedTextTo = -1;
	PMString tabbedTextData;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CA("Pointer to IAppFramework is NULL.");
		return;
	}
	do{
		if(tagInfo.whichTab == 4 && tagInfo.parentId == -1 && tagInfo.isTablePresent == kTrue)
		{//start if1
			
			UID textFrameUID = kInvalidUID;
			InterfacePtr<IGraphicFrameData> graphicFrameDataOne(tagInfo.curBoxUIDRef, UseDefaultIID());
			if (graphicFrameDataOne) 
			{
				textFrameUID = graphicFrameDataOne->GetTextContentUID();
			}
			if (textFrameUID == kInvalidUID)
			{
				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayItemItemTableScreenPrintInTabbedTextForm::!textFrameUID");	
				break;
			}

            InterfacePtr<ITextModel>  temptextModel;
            getTextModelByBoxUIDRef(tagInfo.curBoxUIDRef , temptextModel);
			if (temptextModel == NULL)
			{
				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayItemItemTableScreenPrintInTabbedTextForm::!temptextModel");	
				break;
			}
			textModel = temptextModel;

			txtModelUIDRef = ::GetUIDRef(textModel);
			
			double objectId = pNode.getPBObjectID();
			double pubId = pNode.getPubId();
			double sectionid = -1;
			if(global_project_level == 3)
				sectionid = CurrentSubSectionID;
			if(global_project_level == 2)
				sectionid = CurrentSectionID;

			//SectionLevelItem has two important IDs.PubId and PBObjectID.
			//PBObjectID is used while spraying its item table.
			PMString attrVal;
			attrVal.AppendNumber(PMReal(pNode.getPubId()));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("parentID"),WideString(attrVal));
			attrVal.Clear();
			//
			attrVal.AppendNumber(PMReal(CurrentObjectTypeID));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("parentTypeID"),WideString(attrVal));
			attrVal.Clear();
			//
			attrVal.AppendNumber(PMReal(sectionid));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("sectionID"),WideString(attrVal));
			attrVal.Clear();
			//IMPORTANT: rowno stored the PBObjectID i.e the itemID.
			attrVal.AppendNumber(PMReal(objectId));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("pbObjectId"),WideString(attrVal));
			//tagInfo.tagPtr->SetAttributeValue(WideString("rowno"),WideString(attrVal));
			attrVal.Clear();
			//
			//tableFlag =-11 to identify that this is the "item" table tag in the 
			//tabbed text format.
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("tableFlag"),WideString("-11"));
			//change the ID from -1 to -101, as -1 is elementId for productNumber.
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("ID"),WideString("-101"));

			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("childTag"),WideString("1"));
			int32 tagStartPos = 0;
			int32 tagEndPos =0; 
			Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&tagStartPos,&tagEndPos);	

			tagStartPos = tagStartPos + 1;
			tagEndPos = tagEndPos -1;

			tabbedTextData.Clear();
			replaceItemWithTabbedTextFrom = tagStartPos;
			replaceItemWithTabbedTextTo	= tagEndPos;

			bool16 isItemTableDataAvailableForTheItem = kTrue;
			CItemTableValue oTableValue;
			do
			{
				VectorScreenTableInfoPtr tableInfo=NULL;
				TableSourceInfoValue *tableSourceInfoValueObj;
				bool16 isCallFromTS = kFalse;
				InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
				if(ptrTableSourceHelper != nil)
				{
					tableSourceInfoValueObj=ptrTableSourceHelper->getTableSourceInfoValueObj();
					if(tableSourceInfoValueObj)
					{
						isCallFromTS = tableSourceInfoValueObj->getIsCallFromTABLEsource();
						if(isCallFromTS == kTrue)
						{
							//CA("isCallFromTS");
							tableInfo=tableSourceInfoValueObj->getVectorScreenItemTableInfoPtr();
							if(!tableInfo)
								return ;
							if(tableInfo->size()==0)
							{
								break;
							}							
						}				
					}
				}
				VectorScreenTableInfoValue::iterator it;
				bool16 typeidFound=kFalse;

				if(isCallFromTS == kFalse)	//-------Conten sprayer----
				{
					tableInfo=ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pubId, CurrentSectionID, tagInfo.languageID);
					if(!tableInfo)
					{
						isItemTableDataAvailableForTheItem = kFalse;
						ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayItemItemTableScreenPrintInTabbedTextForm::!tableInfo");	
						break;
					}

					if(tableInfo->size()==0)
					{ 
						isItemTableDataAvailableForTheItem = kFalse;
						break;
					}

					for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
					{
						oTableValue = *it;
						if(oTableValue.getTableTypeID() == tagInfo.typeId)
						{   				
							typeidFound=kTrue;				
							break;
						}
					}
					if(tableInfo)
					{
						tableInfo->clear();
						delete tableInfo;
					}

				}
				else	//---------TableSource------
				{
					for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
					{
						oTableValue = *it;
						if(oTableValue.getTableTypeID() == tableSourceInfoValueObj->getVec_TableType_ID().at(0) && tagInfo.typeId == tableSourceInfoValueObj->getVec_TableType_ID().at(0) && oTableValue.getTableID() == tableSourceInfoValueObj->getVec_Table_ID().at(0))
						{   				
							typeidFound=kTrue;				
							break;
						}
					}
				}
				if(!typeidFound)
				{
					isItemTableDataAvailableForTheItem = kFalse;
					break;
				}
					
			}while(kFalse);

			if(isItemTableDataAvailableForTheItem == kFalse)
			{
				PMString textToInsert("");
                textToInsert=tabbedTextData;

                textToInsert.ParseForEmbeddedCharacters();
				boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
				ReplaceText(textModel,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);
				break;
			}
	
			PMString tableIDStr("");
			tableIDStr.AppendNumber(PMReal(oTableValue.getTableID()));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("tableId"),WideString(tableIDStr));


			vector<double>  vectorOfTableHeader = oTableValue.getTableHeader();
			vector<vector<PMString> >  vectorOfRowByRowTableData = oTableValue.getTableData();
			vector<double>  vec_items = oTableValue.getItemIds();
			bool8 isTranspose = oTableValue.getTranspose();
			
			vector<vector<SlugStruct> > RowList;
			vector<SlugStruct> listOfNewTagsAdded;

			int32 noOfColumns =static_cast<int32>(vectorOfTableHeader.size());
			int32 noOfRows =static_cast<int32>(vec_items.size());
			
			//check for the header flag
			int32 headerFlag = tagInfo.header;//int32 colnoAsHeaderFlag = tagInfo.colno;
			//update the attributes of the parent tag

			if(!isTranspose)
			{
				/*  
				In case of Transpose == kFalse..the table gets sprayed like 
					Header11 Header12  Header13 ...
					Data11	 Data12	   Data13
					Data21	 Data22	   Data23
					....
					....
					So first row is Header Row and subsequent rows are DataRows.
				*/
				//The for loop below will spray the Table Header in left to right fashion.
				if(headerFlag == 1)
				{
					for(int32 headerIndex = 0;headerIndex < noOfColumns;headerIndex++)
					{
						double elementId = vectorOfTableHeader[headerIndex];

						SlugStruct newTagToBeAdded;
						newTagToBeAdded.elementId = elementId;
						newTagToBeAdded.typeId = -1;
						newTagToBeAdded.header = headerFlag;
						newTagToBeAdded.parentId = pubId;
						newTagToBeAdded.pbObjectId = objectId; 
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.parentTypeId = pNode.getTypeId();
						newTagToBeAdded.elementName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );
						newTagToBeAdded.TagName = prepareTagName(newTagToBeAdded.elementName);;
						newTagToBeAdded.LanguageID = tagInfo.languageID;
						newTagToBeAdded.whichTab =4 ;
						newTagToBeAdded.sectionID = sectionid ;
						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
						newTagToBeAdded.tableFlag = -1001;
						newTagToBeAdded.row_no=-1;
						newTagToBeAdded.col_no= -1;
						newTagToBeAdded.tableId = oTableValue.getTableID();	
						newTagToBeAdded.childTag = 1;
						newTagToBeAdded.tableType = tagInfo.tableType;
						
						PMString insertPMStringText;
						insertPMStringText.Append(newTagToBeAdded.elementName);
                        
                        insertPMStringText=insertPMStringText;
						
						if(headerIndex != noOfColumns - 1)
						{
							insertPMStringText.Append("\t");						
						}
						else
						{
							insertPMStringText.Append("\r");
						}
						tabbedTextData.Append(insertPMStringText);

						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
			
                        tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
						
                        newTagToBeAdded.tagStartPos=tagStartPos;
                        newTagToBeAdded.tagEndPos=tagEndPos;
                        listOfNewTagsAdded.push_back(newTagToBeAdded);
                        tagStartPos = tagEndPos+ 1 + 2;
			
					}
				}

				//This for loop will spray the Item_copy_attributes value
				for(int32 itemIndex = 0;itemIndex <noOfRows;itemIndex++)
				{
					double itemId = vec_items[itemIndex];
					for(int32 headerIndex = 0;headerIndex <noOfColumns;headerIndex++)
					{
						double elementId = vectorOfTableHeader[headerIndex];
						PMString tagName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );

						SlugStruct newTagToBeAdded;
						newTagToBeAdded.elementId = elementId;
						newTagToBeAdded.typeId = -1;
						newTagToBeAdded.header = -1;
						newTagToBeAdded.childTag =1;
						newTagToBeAdded.childId = itemId;
						newTagToBeAdded.pbObjectId = objectId;
						newTagToBeAdded.parentId = pubId;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.parentTypeId = pNode.getTypeId();
						/*if(ptrIAppFramework->ATTRIBUTECache_isItemAttributeEventSpecific(elementId) == kTrue)
						{
							VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(itemId,elementId,tagInfo.sectionID);
							if(vecPtr != NULL)
								if(vecPtr->size()> 0)
									newTagToBeAdded.elementName = vecPtr->at(0).getObjectValue();
						}
						else*/ if(elementId == -401 || elementId == -402 || elementId == -403){
							newTagToBeAdded.elementName = ""; //ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(itemId,elementId, tagInfo.languageID);
						}
						else
						{
							newTagToBeAdded.elementName =ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemId,elementId,tagInfo.languageID,CurrentSectionID, kFalse);
						}
						newTagToBeAdded.TagName = prepareTagName(tagName);
						newTagToBeAdded.LanguageID = tagInfo.languageID;
						newTagToBeAdded.whichTab =4 ;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.sectionID = sectionid;
						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
						newTagToBeAdded.tableFlag = -1001;
						newTagToBeAdded.row_no=-1;
						newTagToBeAdded.col_no = -1;
						newTagToBeAdded.tableId = oTableValue.getTableID();
						newTagToBeAdded.tableType = tagInfo.tableType;

						PMString insertPMStringText;
						insertPMStringText.Append(newTagToBeAdded.elementName);
                        
                        insertPMStringText=insertPMStringText;
						
						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
						if(headerIndex != noOfColumns - 1)
						{
							insertPMStringText.Append("\t");
							
						}
						else if(itemIndex != noOfRows-1)
						{
							insertPMStringText.Append("\r");
						}
						tabbedTextData.Append(insertPMStringText);
						tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
						newTagToBeAdded.tagStartPos=tagStartPos;
						newTagToBeAdded.tagEndPos=tagEndPos;
						listOfNewTagsAdded.push_back(newTagToBeAdded);

						tagStartPos = tagEndPos+ 1 + 2;
					}
				}				
			}//end of if IsTranspose == kFalse
			else
			{//else of if IsTranspose ==kFalse..i.e IsTranspose == kTrue. Headers are in first column
				/*  
				In case of Transpose == kTrue..the table gets sprayed like 

					Header11 Data11 Data12 Data13..
					Header22 Data21 Data22 Data23..
					Header33 Data31 Data32 Data33..
					....
					....
				So First Column is HeaderColumn and subsequent columns are Data columns.
				*/
				
				for(int32 headerIndex = 0;headerIndex <noOfColumns;headerIndex++)
				{
					double elementId = vectorOfTableHeader[headerIndex];
					//noOfRows in the for loop below is noOfRows + 1.
					//As we have to add first column which is of table headers.
					//In the earlier part(i.e in the if block of IsTranspose == kFalse)
					//we have added the header row in a separate loop.
					for(int32 itemIndex = 0;itemIndex <noOfRows + 1;itemIndex++)
					{
						double itemId =-1;
						PMString tagName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );
						PMString elementName;
						SlugStruct newTagToBeAdded;
						if(itemIndex == 0 && headerFlag == 1)
						{
							elementName = tagName;
							newTagToBeAdded.header = headerFlag;
						}
						else
						{
							itemId = vec_items[itemIndex-1];
							/*if(ptrIAppFramework->ATTRIBUTECache_isItemAttributeEventSpecific(elementId) == kTrue){
								VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(itemId,elementId,tagInfo.sectionID);
								if(vecPtr != NULL)
									if(vecPtr->size()> 0)
										elementName = vecPtr->at(0).getObjectValue();
							}
							else*/ if(elementId == -401 || elementId == -402 || elementId == -403){
								elementName = ""; //ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(itemId,elementId, tagInfo.languageID);
							}
							else
							{
								elementName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemId,elementId,tagInfo.languageID, CurrentSectionID, kFalse);
							}
							newTagToBeAdded.header = -1;
						}			
						newTagToBeAdded.elementId = elementId;
						newTagToBeAdded.typeId = -1;
						
						newTagToBeAdded.childId = itemId;
						newTagToBeAdded.childTag =1;
						newTagToBeAdded.pbObjectId = objectId;
						newTagToBeAdded.parentId = pubId;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.parentTypeId = pNode.getTypeId();
						newTagToBeAdded.elementName = elementName;
						newTagToBeAdded.TagName = prepareTagName(tagName);
						newTagToBeAdded.LanguageID = tagInfo.languageID;
						newTagToBeAdded.whichTab =4 ;
						newTagToBeAdded.imgFlag = 0;
						newTagToBeAdded.sectionID = sectionid;
						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
						newTagToBeAdded.tableFlag = -1001;
						newTagToBeAdded.row_no=-1;
						newTagToBeAdded.col_no=-1;
						newTagToBeAdded.childId = 1;
						newTagToBeAdded.tableId = oTableValue.getTableID();
						newTagToBeAdded.tableType = tagInfo.tableType;
						
						PMString insertPMStringText;
						insertPMStringText.Append(newTagToBeAdded.elementName);
                        insertPMStringText=insertPMStringText;
						
						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
						if(itemIndex != noOfRows)
						{
							insertPMStringText.Append("\t");
						}
						else
						{
							insertPMStringText.Append("\r");
						}
						tabbedTextData.Append(insertPMStringText);
									
						tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
						newTagToBeAdded.tagStartPos=tagStartPos;
						newTagToBeAdded.tagEndPos=tagEndPos;

						listOfNewTagsAdded.push_back(newTagToBeAdded);
						 
						tagStartPos = tagEndPos+ 1 + 2;		 
			
					}
				}
			}
			PMString textToInsert("");
            textToInsert=tabbedTextData;

            textToInsert.ParseForEmbeddedCharacters();
			boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
			ReplaceText(textModel,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);

			for(int32 indexOfTagToBeAdded = 0;indexOfTagToBeAdded <listOfNewTagsAdded.size();indexOfTagToBeAdded++)
			{

				SlugStruct & newTagToBeAdded = listOfNewTagsAdded[indexOfTagToBeAdded];
				XMLReference createdElement;
				Utils<IXMLElementCommands>()->CreateElement(WideString(newTagToBeAdded.TagName) , txtModelUIDRef,newTagToBeAdded.tagStartPos,newTagToBeAdded.tagEndPos,kInvalidXMLReference, &createdElement);
				addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);

			}
	
		}//end if1
	}while(kFalse);
	
	
//			}//end for1
}//end of function

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////3Aug Item Table Preset End /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

//start AddAttributes
void CDataSprayer :: addAttributesToANewlyCreatedTag(SlugStruct & newTagToBeAdded ,XMLReference & createdElement)
{
	InterfacePtr<IIDXMLElement>createdTagElement(createdElement.Instantiate());
	if(createdTagElement == NULL)
		return;

	Utils<IXMLAttributeCommands> attributeFacade;
	PMString attributeName;
	PMString attributeValue;
	ErrorCode err = kFailure;

	attributeName = "ID";
	attributeValue.AppendNumber(PMReal(newTagToBeAdded.elementId));
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);
	
	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "typeId";
	attributeValue.AppendNumber(PMReal(newTagToBeAdded.typeId));
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "header";
	attributeValue.AppendNumber(newTagToBeAdded.header);
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "isEventField";
	attributeValue.AppendNumber(newTagToBeAdded.isEventField);
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "deleteIfEmpty";
	attributeValue.AppendNumber(newTagToBeAdded.deleteIfEmpty);
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "dataType";
	attributeValue.AppendNumber(newTagToBeAdded.dataType);
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "isAutoResize";
	attributeValue.AppendNumber(newTagToBeAdded.isAutoResize);
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "LanguageID";
	attributeValue.AppendNumber(PMReal(newTagToBeAdded.LanguageID));
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "index";
	attributeValue.AppendNumber(newTagToBeAdded.whichTab);
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "pbObjectId";
	attributeValue.AppendNumber(PMReal(newTagToBeAdded.pbObjectId));
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "parentID";
	attributeValue.AppendNumber(PMReal(newTagToBeAdded.parentId));
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "childId";
	attributeValue.AppendNumber(PMReal(newTagToBeAdded.childId));
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "sectionID";
	attributeValue.AppendNumber(PMReal(newTagToBeAdded.sectionID));
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);
	
	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "parentTypeID";
	attributeValue.AppendNumber(PMReal(newTagToBeAdded.parentTypeId));
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "isSprayItemPerFrame";
	attributeValue.AppendNumber(newTagToBeAdded.isSprayItemPerFrame);
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "catLevel";
	attributeValue.AppendNumber(newTagToBeAdded.catLevel);
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "imgFlag";
	attributeValue.AppendNumber(newTagToBeAdded.imgFlag);
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "imageIndex";
	attributeValue.AppendNumber(newTagToBeAdded.imageIndex);
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "flowDir";
	attributeValue.AppendNumber(newTagToBeAdded.flowDir);
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "childTag";
	attributeValue.AppendNumber(newTagToBeAdded.childTag);
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);
	
	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "tableFlag";
	attributeValue.AppendNumber(newTagToBeAdded.tableFlag);
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);
	
	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "tableType";
	attributeValue.AppendNumber(newTagToBeAdded.tableType);
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "tableId";
	attributeValue.AppendNumber(PMReal(newTagToBeAdded.tableId));
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);
	
	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "rowno";
	attributeValue.AppendNumber(PMReal(newTagToBeAdded.row_no));
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);
	
	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "colno";
	attributeValue.AppendNumber(PMReal(newTagToBeAdded.col_no));
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);
	
	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "field1";
	attributeValue.AppendNumber(PMReal(newTagToBeAdded.field1));
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "field2";
	attributeValue.AppendNumber(PMReal(newTagToBeAdded.field2));
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "field3";
	attributeValue.AppendNumber(PMReal(newTagToBeAdded.field3));
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "field4";
	attributeValue.AppendNumber(PMReal(newTagToBeAdded.field4));
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

	attributeName.Clear();
	attributeValue.Clear();
	attributeName = "field5";
	attributeValue.AppendNumber(PMReal(newTagToBeAdded.field5));
	attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);
    
    attributeName.Clear();
    attributeValue.Clear();
    attributeName = "groupKey";
    attributeValue.Append(newTagToBeAdded.groupKey);
    attributeFacade->CreateAttribute(createdElement,(WideString)attributeName, (WideString)attributeValue);

}

/////////////////////////////////////////////////////////////////////////////////////////////
//Following function is added by vijay on 17-10-2006////////////////////////////////FROM HERE
void CDataSprayer::sprayCMedCustomTableScreenPrintInTabbedTextForm(TagStruct & tagInfo)
{
	//CA("inside sprayForThisBox function");
			
	//UIDRef txtModelUIDRef = UIDRef::gNull;
	//InterfacePtr<ITextModel> textModel;
	//
	//int32 replaceItemWithTabbedTextFrom = -1;
	//int32 replaceItemWithTabbedTextTo = -1;
	//PMString tabbedTextData("");
	//do{
	//	if(tagInfo.whichTab == 3 && tagInfo.parentId == -1 && tagInfo.isTablePresent == kTrue && tagInfo.typeId == -111)
	//	{//start if1
	//		
	//		
	//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//		if(ptrIAppFramework == NULL)
	//		{
	//			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
	//			break;
	//		}		
	//		//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));

	//		//InterfacePtr<IPMUnknown> unknown(tagInfo.curBoxUIDRef, IID_IUNKNOWN);
	//		//if(unknown == NULL)
	//		//{
	//		//	//CA("unknown == NULL");
	//		//	break;
	//		//}
	//		//UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
	//		UID textFrameUID = kInvalidUID;
	//		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(tagInfo.curBoxUIDRef, UseDefaultIID());
	//		if (graphicFrameDataOne) 
	//		{
	//			textFrameUID = graphicFrameDataOne->GetTextContentUID();
	//		}
	//		if (textFrameUID == kInvalidUID)
	//		{
	//			//CA("textFrameUID == kInvalidUID");
	//			break;
	//		}
	//		
	//		
	//			
	//			/*InterfacePtr<ITextFrame> textFrame(tagInfo.curBoxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
	//			if (textFrame == NULL)
	//			{
	//				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayCMedCustomTableScreenPrintInTabbedTextForm::!textFrame");	
	//				break;
	//			}*/

	//			InterfacePtr<IHierarchy> graphicFrameHierarchy(tagInfo.curBoxUIDRef, UseDefaultIID());
	//			if (graphicFrameHierarchy == nil) 
	//			{
	//				//CA(graphicFrameHierarchy);
	//				ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::sprayCMedCustomTableScreenPrintInTabbedTextForm::!graphicFrameHierarchy");
	//				break;
	//			}
	//							
	//			InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
	//			if (!multiColumnItemHierarchy) {
	//				//CA(multiColumnItemHierarchy);
	//				ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::sprayCMedCustomTableScreenPrintInTabbedTextForm::!multiColumnItemHierarchy");
	//				break;
	//			}

	//			InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
	//			if (!multiColumnItemTextFrame) {
	//				//CA(!multiColumnItemTextFrame);
	//				ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::sprayCMedCustomTableScreenPrintInTabbedTextForm::!multiColumnItemTextFrame");
	//				//CA("Its Not MultiColumn");
	//				break;
	//			}
	//			
	//			InterfacePtr<IHierarchy>
	//			frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
	//			if (!frameItemHierarchy) {
	//				//CA(“!frameItemHierarchy”);
	//				ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::sprayCMedCustomTableScreenPrintInTabbedTextForm::!frameItemHierarchy");
	//				break;
	//			}

	//			InterfacePtr<ITextFrameColumn>
	//			textFrame(frameItemHierarchy, UseDefaultIID());
	//			if (!textFrame) {
	//				//CA("!!!ITextFrameColumn");
	//				ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::sprayCMedCustomTableScreenPrintInTabbedTextForm::!textFrame");
	//				break;
	//			}
	//			
	//			InterfacePtr<ITextModel> temptextModel(textFrame->QueryTextModel());
	//			if (temptextModel == NULL)
	//			{
	//				ptrIAppFramework->LogDebug("CDataSprayer::sprayCMedCustomTableScreenPrintInTabbedTextForm: temptextModel == NULL");
	//				break;
	//			}
	//			textModel = temptextModel;
	//			txtModelUIDRef = ::GetUIDRef(textModel);
	//					
	//		
	//		/*InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//		if(ptrIAppFramework == NULL)
	//		{
	//			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
	//			break;
	//		}*/

	//					
	//		int32 sectionid = -1;
	//		if(global_project_level == 3)
	//			sectionid = CurrentSubSectionID;
	//		if(global_project_level == 2)
	//			sectionid = CurrentSectionID;
	//		
	//		PMString attrVal;
	//		attrVal.AppendNumber(pNode.getPubId());
	//		tagInfo.tagPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
	//		attrVal.Clear();
	//		//
	//		attrVal.AppendNumber(CurrentObjectTypeID);
	//		tagInfo.tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attrVal));
	//		attrVal.Clear();
	//		
	//		attrVal.AppendNumber(sectionid);
	//		tagInfo.tagPtr->SetAttributeValue(WideString("sectionID"),WideString(attrVal));
	//		attrVal.Clear();
	//		
	//		//tableFlag =-11 to identify that this is the "item" table tag in the 
	//		//tabbed text format.
	//		tagInfo.tagPtr->SetAttributeValue(WideString("tableFlag"),WideString("-11"));
	//		//change the ID from -1 to -101, as -1 is elementId for productNumber.
	//		tagInfo.tagPtr->SetAttributeValue(WideString("ID"),WideString("-101"));

	//		tagInfo.tagPtr->SetAttributeValue(WideString("childTag"),WideString("1"));
	//		//end update the attributes of the parent tag
	//		//IIDXMLElement * itemTagPtr = tagInfo.tagPtr;
	//		int32 tagStartPos = 0;
	//		int32 tagEndPos =0; 			
	//		Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&tagStartPos,&tagEndPos);
	//		tagStartPos = tagStartPos + 1;
	//		tagEndPos = tagEndPos -1;

	//		tabbedTextData.Clear();
	//		replaceItemWithTabbedTextFrom = tagStartPos;
	//		replaceItemWithTabbedTextTo	= tagEndPos;
	//		bool16 isTableDataPresent = kTrue;
	//		
	//		CMedCustomTableScreenValue* customtableInfoptr = ptrIAppFramework->GetONEsourceObjects_getMed_CustomTableScreenValue(pNode.getPubId());
	//		if(customtableInfoptr == NULL)
	//		{	
	//			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayCMedCustomTableScreenPrintInTabbedTextForm::!customtableInfoptr");	
	//			break;
	//		}
	//		CMedCustomTableScreenValue customtableInfo = *customtableInfoptr;

	//		if(!customtableInfoptr)
	//		{
	//			isTableDataPresent = kFalse;
	//			/*break;*/
	//		}

	//		if(isTableDataPresent == kFalse)
	//		{
	//			PMString textToInsert("");
	//			textToInsert.Clear();
	//			//if(!iConverter)
	//			{
	//				textToInsert=tabbedTextData;					
	//			}
	//			//else
	//			//{
	//			//	textToInsert=iConverter->translateString(tabbedTextData);				
	//			//}

	//			//WideString insertText(textToInsert);
	//			//textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
	//			/*K2*/boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
	//			ReplaceText(textModel,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);

	//			break;
	//		}
	//		
	//		int32 numRows=0;
	//		int32 numCols=0;

	//		numRows =static_cast<int32>(customtableInfo.Tabledata.size());
	//		if(numRows<=0)
	//			break;		

	//		numCols =static_cast<int32>(customtableInfo.HeaderList.size());
	//		if(numCols<=0)
	//			break;
	//		
	//		vector<vector<SlugStruct> > RowList;
	//		vector<SlugStruct> listOfNewTagsAdded;

	//		//Check for the tableHeaderFlag at colno
	//		int32 headerFlag = tagInfo.header;//int32 colnoAsHeaderFlag = tagInfo.colno;
	//		//update the attributes of the parent tag
	//		bool8 isTranspose = kFalse;//Hardcoded as false for Medtronics
	//		if(!isTranspose)
	//		{
	//			/*  
	//			In case of Transpose == kFalse..the table gets sprayed like 
	//				Header11 Header12  Header13 ...
	//				Data11	 Data12	   Data13
	//				Data21	 Data22	   Data23
	//				....
	//				....
	//				So first row is Header Row and subsequent rows are DataRows.
	//			*/
	//			//The for loop below will spray the Table Header in left to right fashion.
	//			//Note typeId == -2 for TableHeaders.
	//			if(headerFlag == 1)//Spray table with the headers.
	//			{//if(colnoAsHeaderFlag != -555)
	//				
	//				for(int k=0; k<customtableInfo.HeaderList.size(); k++)
	//				{	
	//					
	//					SlugStruct newTagToBeAdded;
	//					newTagToBeAdded.elementId = customtableInfo.HeaderList[k].getvalueAttributeId();;
	//					newTagToBeAdded.typeId = -1;//-2;
	//					newTagToBeAdded.header = headerFlag;
	//					newTagToBeAdded.parentId = pNode.getPubId();
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//					
	//					PMString TableColName("");
	//					TableColName.Append("ATTRID_");
	//					TableColName.AppendNumber(customtableInfo.HeaderList[k].getvalueAttributeId());

	//					newTagToBeAdded.elementName = customtableInfo.HeaderList[k].getdisplayString();
	//					newTagToBeAdded.TagName = TableColName;
	//					newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//					newTagToBeAdded.whichTab =tagInfo.whichTab;
	//					newTagToBeAdded.imgFlag = 0;;
	//					newTagToBeAdded.sectionID = sectionid ;
	//					newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//					newTagToBeAdded.tableFlag = -12;
	//					newTagToBeAdded.row_no=-1;
	//					newTagToBeAdded.col_no=tagInfo.colno;				 

	//					PMString insertPMStringText("");
	//					insertPMStringText.Append(newTagToBeAdded.elementName);
	//					if(k != numCols - 1)
	//					{
	//						insertPMStringText.Append("\t");						
	//					}
	//					else
	//					{
	//						insertPMStringText.Append("\r");
	//					}

	//					tabbedTextData.Append(insertPMStringText);
	//					

	//					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
	//						
	//					 tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//					
	//					 newTagToBeAdded.tagStartPos=tagStartPos;
	//					 newTagToBeAdded.tagEndPos=tagEndPos;
	//					 listOfNewTagsAdded.push_back(newTagToBeAdded);
	//					
	//					 tagStartPos = tagEndPos+ 1 + 2;
	//		
	//				}
	//				
	//			}
	//			//////////////This for loop will spray the Item_copy_attributes value
	//			
	//			for(int32 count2 = 0;count2 <numRows;count2++)
	//			{
	//				
	//				VectorCustomItemTableRowInfo rowVector = customtableInfo.Tabledata[count2];
	//				int32 rowVectorSize =static_cast<int32>(rowVector.size());
	//				for(int32 count1 =0 ; count1 < rowVectorSize ;count1++)
	//				{
	//					
	//					PMString TableColName("");
	//					TableColName.Append("ATTRID_");
	//					TableColName.AppendNumber(rowVector[count1].getvalueAttributeId());
	//					SlugStruct newTagToBeAdded;
	//					newTagToBeAdded.elementId = rowVector[count1].getvalueAttributeId();
	//					newTagToBeAdded.typeId =-1;//rowVector[count1].getItemId();
	//					newTagToBeAdded.header = headerFlag;
	//					newTagToBeAdded.childTag =1;	
	//					newTagToBeAdded.childId = rowVector[count1].getItemId();
	//					newTagToBeAdded.parentId = pNode.getPubId();
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//					newTagToBeAdded.elementName =rowVector[count1].getdisplayString()/*TableColName*/;
	//					newTagToBeAdded.TagName = TableColName;
	//					newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//					newTagToBeAdded.whichTab =tagInfo.whichTab;
	//					newTagToBeAdded.imgFlag = 0; 
	//					newTagToBeAdded.sectionID = sectionid;
	//					newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//					newTagToBeAdded.tableFlag = -12;
	//					newTagToBeAdded.row_no=-1;
	//					newTagToBeAdded.col_no=-1;//colnoAsHeaderFlag;
	//					PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
	//					PMString insertPMStringText("");
	//					insertPMStringText.Append(newTagToBeAdded.elementName);
	//					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
	//					if(count1 != numCols - 1)
	//					{
	//						insertPMStringText.Append("\t");
	//					}
	//					else if(count2 != numRows-1)
	//					{
	//						insertPMStringText.Append("\r");
	//					}
	//					tabbedTextData.Append(insertPMStringText);
	//					/*WideString insertText(insertPMStringText);
	//					CA("insertText::"+insertText);
	//					textModel->Insert(kTrue,tagStartPos,&insertText);
	//					*/
	//					tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
	//					newTagToBeAdded.tagStartPos=tagStartPos;
	//					newTagToBeAdded.tagEndPos=tagEndPos;
	//					listOfNewTagsAdded.push_back(newTagToBeAdded);
	//					tagStartPos = tagEndPos+ 1 + 2;
	//				}
	//					
	//			}				
	//		}
	//			
	//			PMString textToInsert("");
	//			
	//			//if(!iConverter)
	//			{
	//				textToInsert=tabbedTextData;					
	//			}
	//			//else
	//			//{
	//			//	textToInsert=iConverter->translateString(tabbedTextData);
	//			//}

	//			//WideString insertText(textToInsert);
	//			//textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
	//			/*K2*/boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
	//			ReplaceText(textModel,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);
	//			for(int32 indexOfTagToBeAdded = 0;indexOfTagToBeAdded <listOfNewTagsAdded.size();indexOfTagToBeAdded++)
	//			{
	//				
	//				SlugStruct & newTagToBeAdded = listOfNewTagsAdded[indexOfTagToBeAdded];
	//				XMLReference createdElement;
	//				Utils<IXMLElementCommands>()->CreateElement(WideString(newTagToBeAdded.TagName) , txtModelUIDRef,newTagToBeAdded.tagStartPos,newTagToBeAdded.tagEndPos,kInvalidXMLReference, &createdElement);
	//				addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);

	//			}
	//	 }
	//}while(kFalse);
	return;
}



//New tagAdded to the selection for mpvId tags.
//Note this function should be actually arrangeMPVAttributes function.
//But due to some constraint we are also spraying in this function.
//The code for this code is copied from the earlier arrangeForSprayingItemItemWithOtherCopyAttributes
//with some modifications.(view the code..I have put a commented where this code differs)
void CDataSprayer ::sprayMPVAttributes(UIDRef & boxUIDRef,InterfacePtr<ITextModel> & tempTextModel)
{

//	//ALGORITHM
//	/*
//	The tag for this MPV is like below
//	|
//	|__PossibleValueBox
//			|
//			|_Name
//			|
//			|_Abbrevation
//			|
//			|_Description
//	i.e like
//
//	[ PossibleValueBox //This is a parentTag
//		[Name] //childTag1
//		[Abbrevation]//childTag2
//		[Description]//childTag3
//	]
//
//	The reason for keeping this structure is the user may write some non tagged text inside the PossibleValueBox
//	And we want to retain that text.
//
//	The typeId of the PossibleValueBox is -1001.
//	So first search for all the PossibleValueBox in tList.
//	*/
//	
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == NULL)
//	{
//		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
//		return;
//	}
//	
//	//Get the textmodel first
//	UIDRef txtModelUIDRef = UIDRef::gNull;
//	/*InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);
//	if(unknown == NULL)
//	{
//		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::sprayMPVAttributes::!unknown");
//		return;
//	}
//	UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
//	UID textFrameUID = kInvalidUID;
//	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
//	if (graphicFrameDataOne) 
//	{
//		textFrameUID = graphicFrameDataOne->GetTextContentUID();
//	}
//	if (textFrameUID == kInvalidUID)
//	{
//		//ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::sprayMPVAttributes::!textFrameUID");
//		return;
//	}	
//	/*InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
//	if (textFrame == NULL)
//	{
//		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::sprayMPVAttributes::!textFrame");
//		return;
//	}	*/	
//
//	InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
//	if (graphicFrameHierarchy == nil) 
//	{
//		//CA(graphicFrameHierarchy);
//		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::sprayMPVAttributes::!graphicFrameHierarchy");
//		return;
//	}
//					
//	InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
//	if (!multiColumnItemHierarchy) {
//		//CA(multiColumnItemHierarchy);
//		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::sprayMPVAttributes::!multiColumnItemHierarchy");
//		return;
//	}
//
//	InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
//	if (!multiColumnItemTextFrame) {
//		//CA(!multiColumnItemTextFrame);
//		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::sprayMPVAttributes::!multiColumnItemTextFrame");
//		//CA("Its Not MultiColumn");
//		return;
//	}
//	
//	InterfacePtr<IHierarchy>
//	frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
//	if (!frameItemHierarchy) {
//		//CA(“!frameItemHierarchy”);
//		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::sprayMPVAttributes::!frameItemHierarchy");
//		return;
//	}
//
//	InterfacePtr<ITextFrameColumn>
//	textFrame(frameItemHierarchy, UseDefaultIID());
//	if (!textFrame) {
//		//CA("!!!ITextFrameColumn");
//		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::sprayMPVAttributes::!textFrame");
//		return;
//	}
//
//	InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
//	if (textModel == NULL)
//	{
//		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::sprayMPVAttributes::!textModel");
//		return;
//	}
//	//tempTextModel = textModel;
//	//
//
//	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*>(CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
//	if(!itagReader)
//	{ 
//		ptrIAppFramework->LogError("AP7_DataSprayer:CDataSprayer::sprayMPVAttributes::!itagReader");
//		return;
//	}
//	TagList tList=itagReader->getTagsFromBox(boxUIDRef);
//	int numTags=static_cast<int>(tList.size());
//	if(numTags<0)
//	{
//		return;
//	}
//	
//	VectorHtmlTrackerPtr vectorHtmlTrackerMPVPtr = NULL;
//	VectorHtmlTrackerValue vectorObj ;
//	vectorHtmlTrackerMPVPtr = &vectorObj;
//	vector<MPVToggleData> vectorMPVToggleData;
//	vectorMPVToggleData.clear();
//
//	bool16 isAtLeastOneMPVTagPresent = kFalse;
//	vector<struct_elementID_index> vector_mpvTagIndex;
//
//	//Collect the Possible_value_box tag index in the vector_mpvTagIndex.
//	for(int32 tagIndex=0;tagIndex<numTags;tagIndex++)
//	{//start for tagIndex
//		TagStruct & firstTagInfo = tList[tagIndex];
//		XMLContentReference xmlCntnRef = firstTagInfo.tagPtr->GetContentReference();
//		if(xmlCntnRef.IsTable())
//		{
//			continue;
//		}
//
//		int32 colNo = firstTagInfo.colno;
//		int32 elementID = firstTagInfo.elementId;
//		int32 typeID = firstTagInfo.typeId;					
//		if(elementID == -1 && typeID == -1001 )
//		{
//			//Collect the actual elementID from the first child of the tag.
//			int32 childCount = firstTagInfo.tagPtr->GetChildCount();					
//			XMLReference childRef = firstTagInfo.tagPtr->GetNthChild(0);
//			//IIDXMLElement * childPtr = childRef.Instantiate();
//			InterfacePtr<IIDXMLElement>childPtr(childRef.Instantiate());
//
//			PMString strElementID = childPtr->GetAttributeValue(WideString("ID"));
//
//			int32 elementID = strElementID.GetAsNumber();
//			struct_elementID_index elmntIndx;
//			elmntIndx.elementID = elementID;
//			elmntIndx.index = tagIndex;
//			isAtLeastOneMPVTagPresent = kTrue;
//			vector_mpvTagIndex.push_back(elmntIndx);
//		}
//		//}//end if col
//	}//end for tagIndex		
//	
//	if(isAtLeastOneMPVTagPresent == kFalse)
//	{
//		//added by avinash.. since the tlsit is loaded..so before returning it sholud be release
//		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
//		{
//			tList[tagIndex].tagPtr->Release();
//		}
//		//till here
//		return; //og
//	
//	}
//	/*	
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == NULL)
//	{
//		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
//		return;
//	}*/
//			
//	txtModelUIDRef = ::GetUIDRef(textModel);
//	int32 tagIndex = 0;
//	int32 tagIndexOfNewlyAddedTagDueToLinePaste = 0;
//	int32 tagListSize =static_cast<int32>(tList.size());
//
//	int32 firstCharacterIndexOfTheline = 0;
//	TagStruct tagInfo;	
//	
//	
//
//	int32 lineStartCharIndex = 0;
//	int32 lineEndCharIndex = -1;
//
//	int32 firstTagIndex =0;
//	int32 lastTagIndex = 0;
//	int32 updatedTagIndex = 0;
//
//	int32 numberOfTagsInALine = 0;
//
//	int32 numberOfTimesTheLineToBeCopied = -1;// = FinalMPVIds.size() -1;			
//
//	for(int32 itemTagIndex = 0;itemTagIndex < vector_mpvTagIndex.size();itemTagIndex++)
//	{//for xxx
//		numberOfTagsInALine = 0;
//		//Get the actual index of the PossibleValueBox inside the taglist of the box. 
//		//now find the startIndex and endIndex of the PossibleValueBox tag.
//		
//		struct_elementID_index elmntIndx = vector_mpvTagIndex[itemTagIndex];
//
//		int32 elementID = elmntIndx.elementID;
//		int32 index = elmntIndx.index;
//
//		//CA_NUM("elementID :",elementID);
//		VectorPossibeValueModelPtr vectorPV = ptrIAppFramework->GETProduct_getMPVAttributeValueByLanguageID(pNode.getPubId(),elementID,1,kTrue);
//        if(vectorPV == NULL)
//		{
//			//We have to update the tag attributes with default value.
//			TagStruct tagInfo = tList[index];
//			//IIDXMLElement * tagPtr = tagInfo.tagPtr;
//			int32 childCount = tagInfo.tagPtr->GetChildCount();
//			for(int32 childIndex = 0;childIndex < childCount;childIndex++)
//			{
//				XMLReference childXMLRef = tagInfo.tagPtr->GetNthChild(childIndex);
//				//IIDXMLElement * childTagPtr = childXMLRef.Instantiate();
//				InterfacePtr<IIDXMLElement>childTagPtr(childXMLRef.Instantiate());
//
//				int32 startIndex = -1;
//				int32 endIndex = -1;
//				Utils<IXMLUtils>()->GetElementIndices(childTagPtr,&startIndex,&endIndex);
//				startIndex = startIndex+1;
//				endIndex = endIndex -1;
//				PMString colNo = childTagPtr->GetAttributeValue(WideString("colno"));
//				PMString data("");
//				//WideString insertText(data);			
//				//textModel->Replace(startIndex,endIndex-startIndex + 1,&insertText);			
//				/*K2*/boost::shared_ptr<WideString> insertText(new WideString(data));
//				ReplaceText(textModel,startIndex,endIndex-startIndex + 1,insertText);
//
//				PMString attrVal;
//				attrVal.AppendNumber(pNode.getPubId());
//				childTagPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
//				//parentTypeID
//				attrVal.Clear();
//				attrVal.AppendNumber(pNode.getTypeId());
//				childTagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attrVal));
//				//sectionID
//				attrVal.Clear();
//				attrVal.AppendNumber(CurrentSubSectionID);
//
//				//CA("Inside sprayMPVAttributes " + attrVal);
//				childTagPtr->SetAttributeValue(WideString("SectionID"),WideString(attrVal));
//				//mpvID..stored in rowno
//				PMString attrValMPVID;
//				attrValMPVID.AppendNumber(-1);
//				childTagPtr->SetAttributeValue(WideString("rowno"),WideString(attrValMPVID));
//			}
//			continue;
//		}
//		numberOfTimesTheLineToBeCopied =static_cast<int32> (vectorPV->size()-1);
//		
//		tagInfo=tList[index];	
//		XMLReference mpvTagXMLRef = tagInfo.tagPtr->GetXMLReference();
//
//		//Get the start and end indices of the PossibleValueBox tag.
//		int32 startPos=-1;
//		int32 endPos = -1;
//		Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&startPos,&endPos);		
//		//CA_NUM("startPos ",startPos);
//		//CA_NUM("endPos ",endPos);
//		//CA_NUM("lineEndCharIndex : ",lineEndCharIndex);
//		if(startPos <= lineEndCharIndex)
//			continue;
//		
//		lineStartCharIndex = startPos ;//+ 1 ;			
//		lineEndCharIndex = endPos;
//		int32 replaceStartIndex = 0;
////check which tags are in the tag ragne of PossibleValueBox tag
//		for(int32 normalTagIndex = 0;normalTagIndex < numTags;normalTagIndex++)
//		{
//
//			TagStruct & tagInfo = tList[normalTagIndex];
//			//IIDXMLElement * tagPtr = tagInfo.tagPtr;
//			int32 startPos=-1;
//			int32 endPos = -1;
//			Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&startPos,&endPos);
//
//			if(startPos >= lineStartCharIndex && endPos <= lineEndCharIndex)
//			{						
//				int32 childCount = tagInfo.tagPtr->GetChildCount();
//				for(int32 childIndex = 0;childIndex < childCount;childIndex++)
//				{
//					XMLReference childXMLRef = tagInfo.tagPtr->GetNthChild(childIndex);
//					//IIDXMLElement * childTagPtr = childXMLRef.Instantiate();	
//					InterfacePtr<IIDXMLElement>childTagPtr(childXMLRef.Instantiate());
//
//					int32 startIndex = -1;
//					int32 endIndex = -1;
//
//					Utils<IXMLUtils>()->GetElementIndices(childTagPtr,&startIndex,&endIndex);
//
//					startIndex = startIndex+1;
//					endIndex = endIndex -1;
//
//					PMString colNo = childTagPtr->GetAttributeValue(WideString("colno"));
//					//CA("colNo :" + colNo);
//					int32 mpvID =-1;
//					PMString data("");
//					CPossibleValueModel pvmdl;							
//					pvmdl = (*vectorPV)[0];			
//					
//					mpvID = pvmdl.getPv_id();//FinalMPVIds[0];							
//					if(colNo == "-201")
//						data = pvmdl.getName();
//					else if(colNo == "-202")
//						data = pvmdl.getAbbrevation();
//					else if(colNo == "-203")
//						data = pvmdl.getDescription();
//					
//					//data = "1234";
//					InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//					if(iConverter)								
//					{
//						//data=iConverter->translateString(data);
//						vectorHtmlTrackerMPVPtr->clear();
//						data=iConverter->translateStringNew(data, vectorHtmlTrackerMPVPtr);
//					}
//					//WideString insertText(data);
//					//CA("before replace");
//					//textModel->Replace(startIndex,endIndex-startIndex + 1,&insertText);							
//					/*K2*/boost::shared_ptr<WideString> insertText(new WideString(data));
//					ReplaceText(textModel,startIndex,endIndex-startIndex + 1,insertText);
//
//					//CA("after replace");						
//					MPVToggleData mpvToggledataOnj;
//					mpvToggledataOnj.ptr = *vectorHtmlTrackerMPVPtr;
//					mpvToggledataOnj.StartReplaceIndex = startIndex;
//					vectorMPVToggleData.push_back(mpvToggledataOnj);
//					//parentID
//					PMString attrVal;
//					attrVal.AppendNumber(pNode.getPubId());
//					childTagPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
//					//parentTypeID
//					attrVal.Clear();
//					attrVal.AppendNumber(pNode.getTypeId());
//					childTagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attrVal));
//					//sectionID
//					attrVal.Clear();
//					attrVal.AppendNumber(CurrentSubSectionID);
//					//CA("Inside sprayMPVAttributes 2 " + attrVal);
//					childTagPtr->SetAttributeValue(WideString("SectionID"),WideString(attrVal));
//					//mpvID..stored in rowno
//					PMString attrValMPVID;
//					attrValMPVID.AppendNumber(mpvID);
//					childTagPtr->SetAttributeValue(WideString("rowno"),WideString(attrValMPVID));
//
//					Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&lineStartCharIndex,&lineEndCharIndex);
//				}
//				numberOfTagsInALine++;
//			}
//		}
////CA_NUM("numberOfTagsInALine :",numberOfTagsInALine);
//		//int32 numberOfTagsInALine = 0;
//		//int32 numberOfTimesTheLineToBeCopied = 0;
//		
//		
//		int32 textSelectionStartAt = lineEndCharIndex +1;
//		int32 length = lineEndCharIndex -lineStartCharIndex + 1;
//		int32 startPasteIndex = lineEndCharIndex + 1;
//		if(lineEndCharIndex > lineStartCharIndex)
//		{//if aaa
//		
//			//CA_NUM("lineStartCharIndex " ,lineStartCharIndex);
//			//CA_NUM("lineEnd ",lineEndCharIndex);
//			/*K2*/boost::shared_ptr< PasteData > pasteData;
//			//Copy the entire line.
//			for(int32 lineIndex=0;lineIndex<numberOfTimesTheLineToBeCopied;lineIndex++)
//			{
////CA_NUM("for lineIndex : ",lineIndex);
//				textModel->CopyRange(lineStartCharIndex,length,pasteData);
//				//
//				PMString cr;
//				cr.Append(kTextChar_CR);
//				WideString wstr(cr);
//				textModel->Insert(startPasteIndex,&wstr);
//				startPasteIndex++;
//				//
//				textModel->Paste(startPasteIndex,pasteData);
//				startPasteIndex =   startPasteIndex + length;
//				//CA_NUM("startPasteIndex : ",startPasteIndex);
//				//break;
//			}
//
//				//continue;
//
//			//this->ToggleBold_or_Italic(textModel , vectorHtmlTrackerMPVPtr, startIndex );	
//			int32 textSelectionEndAt = startPasteIndex-1;
//			//CA_NUM("textSelectionEndAt : ",textSelectionEndAt);
//			//start delete the parentMPVTag.
//				/*ErrorCode errCode = Utils<IXMLElementCommands>()->DeleteElement
//					(mpvTagXMLRef, kFalse);
//				// Verify the results: no errors, valid XMLRef returned, we can instantiate it.
//				if (errCode != kSuccess)
//				{
//					//CA("ExpXMLActionComponent::TagFrameElement - CreateElement failed");
//					break;
//				}*/	
//			//end delete the parentMPVTag.
//
//			TagList newTagList = itagReader->getTagsFromBox(boxUIDRef);
//			int32 pastedTagCount = 0;
//			for(int32 newTagIndex = 0;newTagIndex < newTagList.size();newTagIndex++)
//			{//for yyy
//				TagStruct & newTagInfo = newTagList[newTagIndex];
//				//IIDXMLElement * newXMLTagPtr = newTagInfo.tagPtr;
//
//				int32 elementID = newTagInfo.elementId;
//				int32 typeID = newTagInfo.typeId;
//				
//
//				int32 newStartPos = -1;
//				int32 newEndPos = -1;
//				Utils<IXMLUtils>()->GetElementIndices(newTagInfo.tagPtr,&newStartPos,&newEndPos);
//				if((newStartPos >= textSelectionStartAt) && (newEndPos <= textSelectionEndAt))
//				{//if yyy
//					
//					if(elementID == -1 && typeID == -1001)
//					{//start if zzz
//						int32 childCount = newTagInfo.tagPtr->GetChildCount();
//						for(int32 childIndex = 0;childIndex < childCount;childIndex++)
//						{//for zzz
//							int32 startIndex= -1;
//							int32 endIndex = -1;
//
//							XMLReference childXMLRef = newTagInfo.tagPtr->GetNthChild(childIndex);
//							//IIDXMLElement * childTagPtr = childXMLRef.Instantiate();	
//							InterfacePtr<IIDXMLElement>childTagPtr(childXMLRef.Instantiate());
//
//							int32 mpvID =-1;
//							//mpvID = ((*vectorPV)[pastedTagCount / numberOfTagsInALine + 1]).getPv_id();//FinalMPVIds[pastedTagCount / numberOfTagsInALine + 1];
//							
//
//							Utils<IXMLUtils>()->GetElementIndices(childTagPtr,&startIndex,&endIndex);
//
//							startIndex = startIndex + 1;
//							endIndex = endIndex - 1;
//
//							PMString colNo = childTagPtr->GetAttributeValue(WideString("colno"));
//							
//							PMString data("");
//							CPossibleValueModel pvmdl;
//
//							//CA_NUM(" [pastedTagCount / numberOfTagsInALine + 1] : " , ((pastedTagCount / numberOfTagsInALine) + 1));
//							pvmdl = ((*vectorPV)[pastedTagCount / numberOfTagsInALine + 1]);
//
//							mpvID = pvmdl.getPv_id();//FinalMPVIds[0];
//
//							if(colNo == "-201")
//								data = pvmdl.getName();
//							else if(colNo == "-202")
//								data = pvmdl.getAbbrevation();
//							else if(colNo == "-203")
//								data = pvmdl.getDescription();
//							//data = "1234";
//							InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//							if(iConverter)
//							{
//								//data=iConverter->translateString(data);
//								vectorHtmlTrackerMPVPtr->clear();
//								data=iConverter->translateStringNew(data, vectorHtmlTrackerMPVPtr);
//							}
//
//							//CA("data2 : " + data);
//
//							textSelectionEndAt = textSelectionEndAt + data.NumUTF16TextChars() - (endIndex-startIndex);
//
//							//CA("before replace 2");
//							//WideString insertText(data);
//							//textModel->Replace(startIndex,endIndex-startIndex + 1,&insertText);
//							/*K2*/boost::shared_ptr<WideString> insertText(new WideString(data));
//							ReplaceText(textModel,startIndex,endIndex-startIndex + 1,insertText);
//							//CA("after replace 2");
//							MPVToggleData mpvToggledataOnj;
//							mpvToggledataOnj.ptr = *vectorHtmlTrackerMPVPtr;
//							mpvToggledataOnj.StartReplaceIndex = startIndex;
//							vectorMPVToggleData.push_back(mpvToggledataOnj);
//
//							//update element
//							
//							PMString attrValMPVID;
//							attrValMPVID.AppendNumber(mpvID);
//							childTagPtr->SetAttributeValue(WideString("rowno"),WideString(attrValMPVID));
//							
//						}//end for zzz	
//					}//end if zzz
//					pastedTagCount++;
//					
//				}//end if yyy
//			}//end for yyy
//			
//			for(int32 tagIndex = 0 ; tagIndex < newTagList.size() ; tagIndex++)
//			{
//				newTagList[tagIndex].tagPtr->Release();
//			}
//		}//end if aaa
//		//firstTagIndex = index;
//	}//end for xxx
//		
//	if(vectorMPVToggleData.size()> 0)
//	{
//		for(int32 p=0; p<vectorMPVToggleData.size(); p++ )
//		{
//			this->ToggleBold_or_Italic(textModel , &vectorMPVToggleData[p].ptr, vectorMPVToggleData[p].StartReplaceIndex );
//		}
//	}
//
//	TagList newTagList = itagReader->getTagsFromBox(boxUIDRef);
//	for(int32 index =0;index<newTagList.size();index++)
//	{
//		TagStruct & tagInfo = newTagList[index];
//		//IIDXMLElement * tagPtr = tagInfo.tagPtr;
//		XMLReference tagRef = tagInfo.tagPtr->GetXMLReference();
//		
//		int32 typeID = tagInfo.typeId;
//		int32 elementID = tagInfo.elementId;
//
//		if(elementID == -1 && typeID == -1001)
//		{
//
//			ErrorCode errCode = Utils<IXMLElementCommands>()->DeleteElement(tagRef, kFalse,kFalse,kTrue,kTrue);
//			if (errCode != kSuccess)
//			{
//				ptrIAppFramework->LogError("CDataSprayer ::sprayMPVAttributes: ExpXMLActionComponent::TagFrameElement - CreateElement failed");
//				break;
//			}				
//		}
//	}
//	//CA("00000000");
//
//	//added by avinash to clear the newTagList..since it loaded
//	for(int32 tagIndex = 0 ; tagIndex < newTagList.size() ; tagIndex++)
//	{
//		newTagList[tagIndex].tagPtr->Release();
//	}	
//	//till here
//	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
//	{
//		tList[tagIndex].tagPtr->Release();
//	}

}
//ended on 20Nov..


//21Nov..items from product dropdown KitComponent Table.. ???/// This Function is now modified to spray Item Componet table
// Modified By Rahul. to spray Item's Componet table. in tabbed text format.
void CDataSprayer::sprayProductKitComponentTableScreenPrintInTabbedTextForm(TagStruct & tagInfo, bool16 isKitTable)
{
	//CA("inside sprayProductKitComponentTableScreenPrintInTabbedTextForm function");
	//int32 CopyFlag=0;

	//int numReplications=static_cast<int>(idList.size()-4);//These many times we have to replicate the text for the ITEMs text

	//PMString elementName,colName;

	//vector<int32> FinalItemIds;
	//bool16 firstTabbedTextTag = kTrue;
	//tabbedTagStructList.clear();

	//UIDRef txtModelUIDRef = UIDRef::gNull;
	//InterfacePtr<ITextModel> textModel;
	//
	//TextIndex startPos=-1;
	//TextIndex endPos = -1;
	////First collect all the tabbedTextTag in a vector tabbedTagStructList.
	//		
	//	//check for the tabbedTextTag
	//int32 replaceItemWithTabbedTextFrom = -1;
	//int32 replaceItemWithTabbedTextTo = -1;
	//PMString tabbedTextData;
	//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//if(ptrIAppFramework == NULL)
	//{
	//	CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
	//	return;
	//}
	////InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	//do{
	//	//CA("Before do while");
	//	if(tagInfo.whichTab == 4/*3*/ && tagInfo.parentId == -1 && tagInfo.isTablePresent == kTrue)
	//	{//start if1
	//		//CA("Inside Do while");
	//			/*InterfacePtr<IPMUnknown> unknown(tagInfo.curBoxUIDRef, IID_IUNKNOWN);
	//			if(unknown == NULL)
	//			{
	//				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!unknown");
	//				break;
	//			}
	//			UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
	//			UID textFrameUID = kInvalidUID;
	//			InterfacePtr<IGraphicFrameData> graphicFrameDataOne(tagInfo.curBoxUIDRef, UseDefaultIID());
	//			if (graphicFrameDataOne) 
	//			{
	//				textFrameUID = graphicFrameDataOne->GetTextContentUID();
	//			}
	//			if (textFrameUID == kInvalidUID)
	//			{
	//				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!textFrameUID");
	//				break;
	//			}
	//			
	//			/*InterfacePtr<ITextFrame> textFrame(tagInfo.curBoxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
	//			if (textFrame == NULL)
	//			{
	//				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!textFrame");
	//				break;
	//			}*/

	//			InterfacePtr<IHierarchy> graphicFrameHierarchy(tagInfo.curBoxUIDRef, UseDefaultIID());
	//			if (graphicFrameHierarchy == nil) 
	//			{
	//				//CA(graphicFrameHierarchy);
	//				ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!graphicFrameHierarchy");
	//				break;
	//			}
	//							
	//			InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
	//			if (!multiColumnItemHierarchy) {
	//				//CA(multiColumnItemHierarchy);
	//				ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!multiColumnItemHierarchy");
	//				break;
	//			}

	//			InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
	//			if (!multiColumnItemTextFrame) {
	//				//CA(!multiColumnItemTextFrame);
	//				ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!multiColumnItemTextFrame");
	//				//CA("Its Not MultiColumn");
	//				break;
	//			}
	//			
	//			InterfacePtr<IHierarchy>
	//			frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
	//			if (!frameItemHierarchy) {
	//				//CA(“!frameItemHierarchy”);
	//				ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!frameItemHierarchy");
	//				break;
	//			}

	//			InterfacePtr<ITextFrameColumn>
	//			textFrame(frameItemHierarchy, UseDefaultIID());
	//			if (!textFrame) {
	//				//CA("!!!ITextFrameColumn");
	//				ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!textFrame");
	//				break;
	//			}
	//			
	//			InterfacePtr<ITextModel> temptextModel(textFrame->QueryTextModel());
	//			if (temptextModel == NULL)
	//			{
	//				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!temptextModel");
	//				break;
	//			}
	//			textModel = temptextModel;

	//			txtModelUIDRef = ::GetUIDRef(textModel);
	//		

	//		/////end   8Jun///////
	//				
	//		//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//		//if(ptrIAppFramework == NULL)
	//		//{
	//		//	//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
	//		//	break;
	//		//}
	//		int32 sectionid = -1;
	//		if(global_project_level == 3)
	//			sectionid = CurrentSubSectionID;
	//		if(global_project_level == 2)
	//			sectionid = CurrentSectionID;

	//		
	//		PMString attrVal;
	//		attrVal.AppendNumber(pNode.getPubId());	
	//		tagInfo.tagPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
	//		attrVal.Clear();
	//		
	//		attrVal.AppendNumber(CurrentObjectTypeID);
	//		tagInfo.tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attrVal));
	//		attrVal.Clear();
	//		
	//		attrVal.AppendNumber(sectionid);
	//		tagInfo.tagPtr->SetAttributeValue(WideString("sectionID"),WideString(attrVal));
	//		attrVal.Clear();
	//		//tableFlag =-11 to identify that this is the "item" table tag in the 
	//		//tabbed text format.
	//		tagInfo.tagPtr->SetAttributeValue(WideString("tableFlag"),WideString("-11"));
	//		//change the ID from -1 to -101, as -1 is elementId for productNumber.
	//		tagInfo.tagPtr->SetAttributeValue(WideString("ID"),WideString("-101"));

	//		tagInfo.tagPtr->SetAttributeValue(WideString("childTag"),WideString("1"));

	//		//end update the attributes of the parent tag
	//		//IIDXMLElement * itemTagPtr = tagInfo.tagPtr;
	//		int32 tagStartPos = 0;
	//		int32 tagEndPos =0; 			
	//		Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&tagStartPos,&tagEndPos);
	//		tagStartPos = tagStartPos + 1;
	//		tagEndPos = tagEndPos -1;

	//		tabbedTextData.Clear();
	//		replaceItemWithTabbedTextFrom = tagStartPos;
	//		replaceItemWithTabbedTextTo	= tagEndPos;

	//					
	//		bool16 isTableDataPresent = kTrue;
	//		//CItemTableValue oTableValue;
	//		//VectorScreenTableInfoPtr tableInfo = NULL;
	//		////Added By Dattatray on 9/10/06 When Table is selected and to get all the parameters of table TableID,TableTypeID,TableName,TableHeader,TableData etc. which is required fo spraying.
	//		//	if(pNode.getIsONEsource())
	//		//	{
	//		//		// If ONEsource mode is selected  
	//		//		tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(pNode.getPubId());
	//		//	}else
	//		//	{
	//		//		//For publication mode is selected
	//		//		tableInfo=ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, pNode.getPubId());
	//		//	}
	//		//	if(!tableInfo)
	//		//	{
	//		//		isTableDataPresent = kFalse;
	//		//		ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!tableInfo");
	//		//		break;
	//		//	}

	//		//	if(tableInfo->size()==0)
	//		//	{ 
	//		//		//CA(" tableInfo->size()==0");
	//		//		isTableDataPresent = kFalse;
	//		//		ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayProductKitComponentTableScreenPrintInTabbedTextForm::tableInfo->size ==0");
	//		//		break;
	//		//	}

	//		//	CItemTableValue OriginaloTableValue;
	//		//	VectorScreenTableInfoValue::iterator it1;
	//		//	it1 = tableInfo->begin();
	//		//	OriginaloTableValue = *it1;
	//		//	
	//		//	vector<int32> vec_item_new;
	//		//	vec_item_new = OriginaloTableValue.getItemIds();
	//		//	if(vec_item_new.size() == 0)
	//		//	{
	//		//		ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayProductKitComponentTableScreenPrintInTabbedTextForm::vec_item_new->size ==0");
	//		//		break;
	//		//	}
	//			int32 itemId = pNode.getPubId(); //vec_item_new[0]/*30017240*/;
	//			
	//			/*PMString ASD("itemid: ");
	//			ASD.AppendNumber(itemId);
	//			CA(ASD);*/

	//			VectorScreenTableInfoPtr KittableInfo = NULL;

	//			KittableInfo= ptrIAppFramework->GETItem_getKitOrCompoentScreenTableByItemIdLanguageId(itemId, tagInfo.languageID, isKitTable); 
	//			if(!KittableInfo)
	//			{
	//				ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::sprayProductKitComponentTableScreenPrintInTabbedTextForm::!KittableInfo");
	//				return;
	//			}

	//			if(KittableInfo->size()==0){
	//				//CA(" KittableInfo->size()==0");
	//				isTableDataPresent = kFalse;

	//				if(isTableDataPresent == kFalse)
	//				{
	//					PMString textToInsert("");
	//					//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	//					//if(!iConverter)
	//					{
	//						textToInsert=tabbedTextData;					
	//					}
	//					//else
	//					//	textToInsert=iConverter->translateString(tabbedTextData);				
	//					
	//					//WideString insertText(textToInsert);	
	//					//textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
	//					/*K2*/boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
	//					ReplaceText(textModel,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);
	//					break;
	//				}
	//				break;
	//			}

	//			/*VectorScreenTableInfoValue::iterator it;
	//			bool16 typeidFound=kFalse;
	//				for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
	//				{
	//					oTableValue = *it;
	//					if(oTableValue.getTableTypeID() == tagInfo.typeId)
	//					{   				
	//						typeidFound=kTrue;				
	//						break;
	//					}
	//				}
	//				if(!typeidFound)
	//				{
	//					isTableDataPresent = kFalse;
	//					break;
	//				}*/
	//		CItemTableValue oTableValue;
	//		VectorScreenTableInfoValue::iterator it;
	//		it = KittableInfo->begin();
	//		oTableValue = *it;
	//		isTableDataPresent = kTrue;
	//		
	//		vector<int32> vectorOfTableHeader = oTableValue.getTableHeader();
	//		vector<vector<PMString> >  vectorOfRowByRowTableData = oTableValue.getTableData();
	//		vector<int32> vec_items = oTableValue.getItemIds();
	//		bool8 isTranspose = /*oTableValue.getTranspose()*/kFalse;
	//		vector<vector<SlugStruct> > RowList;
	//		vector<SlugStruct> listOfNewTagsAdded;

	//		int32 noOfColumns =static_cast<int32>(vectorOfTableHeader.size());
	//		int32 noOfRows = static_cast<int32>(vec_items.size());			

	//	/*	PMString numRow("numRows::");
	//		numRow.AppendNumber(noOfRows);
	//		CA(numRow);
	//		PMString numCol("numcols::");
	//		numCol.AppendNumber(noOfColumns);
	//		CA(numCol);*/
	//		//Check for the tableHeaderFlag at colno
	//		int32 headerFlag = tagInfo.header;//int32 colnoAsHeaderFlag = tagInfo.colno;
	//		//update the attributes of the parent tag
	//		
	//		if(!isTranspose)
	//		{
	//			/*  
	//			In case of Transpose == kFalse..the table gets sprayed like 
	//				Header11 Header12  Header13 ...
	//				Data11	 Data12	   Data13
	//				Data21	 Data22	   Data23
	//				....
	//				....
	//				So first row is Header Row and subsequent rows are DataRows.
	//			*/
	//			//The for loop below will spray the Table Header in left to right fashion.
	//			//Note typeId == -2 for TableHeaders.
	//			int32 rowCount =0, colCount=0;
	//			
	//			if(headerFlag == 1)//Spray table with the headers.
	//			{//if(colnoAsHeaderFlag != -555)
	//				int32 headerIndex = 0;
	//				
	//				{ // For Quantity First Attribute in Header
	//					int32 elementId = -805;

	//					SlugStruct newTagToBeAdded;
	//					newTagToBeAdded.elementId = elementId;
	//					newTagToBeAdded.typeId = -1;//-2;
	//					newTagToBeAdded.header = headerFlag;
	//					newTagToBeAdded.parentId = pNode.getPubId();
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//					newTagToBeAdded.elementName = "Quantity";
	//					newTagToBeAdded.TagName = prepareTagName(newTagToBeAdded.elementName);;
	//					newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//					newTagToBeAdded.whichTab =4 ;
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.sectionID = sectionid ;
	//					newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//					newTagToBeAdded.tableFlag = -12;
	//					newTagToBeAdded.row_no=-1;
	//					newTagToBeAdded.col_no=-1;//colnoAsHeaderFlag;					 

	//					PMString insertPMStringText;
	//					insertPMStringText.Append(newTagToBeAdded.elementName);
	//					//if(!iConverter)
	//					{
	//						insertPMStringText=insertPMStringText;					
	//					}
	//					//else
	//					//	insertPMStringText=iConverter->translateString(insertPMStringText);
	//					
	//					if(headerIndex != noOfColumns - 1)
	//					{
	//						insertPMStringText.Append("\t");						
	//					}
	//					else
	//					{
	//						insertPMStringText.Append("\r");
	//					}

	//					tabbedTextData.Append(insertPMStringText);					

	//					//CA("tabbedTextData 2: "+ tabbedTextData);

	//					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
	//					tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//					
	//					newTagToBeAdded.tagStartPos=tagStartPos;
	//					newTagToBeAdded.tagEndPos=tagEndPos;
	//					listOfNewTagsAdded.push_back(newTagToBeAdded);
	//					tagStartPos = tagEndPos+ 1 + 2;	
	//				}

	//				for(headerIndex = 1;headerIndex < noOfColumns;headerIndex++)
	//				{
	//					//CA("Insdie For looop");
	//					int32 elementId = vectorOfTableHeader[headerIndex];

	//					 SlugStruct newTagToBeAdded;
	//					 newTagToBeAdded.elementId = elementId;
	//					 newTagToBeAdded.typeId = -1;//-2;
	//					 newTagToBeAdded.header = headerFlag;
	//					 newTagToBeAdded.parentId = pNode.getPubId();
	//					 newTagToBeAdded.imgFlag = 0;
	//					 newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//					 newTagToBeAdded.elementName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );
	//					 newTagToBeAdded.TagName = prepareTagName(newTagToBeAdded.elementName);;
	//					 newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//					 newTagToBeAdded.whichTab =4 ;
	//					 newTagToBeAdded.imgFlag = 0;
	//					 newTagToBeAdded.sectionID = sectionid ;
	//					 newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//					 newTagToBeAdded.tableFlag = -12;
	//					 newTagToBeAdded.row_no=-1;
	//					 newTagToBeAdded.col_no=-1;//colnoAsHeaderFlag;					 

	//					 //listOfNewTagsAdded.push_back(newTagToBeAdded);
	//					
	//					//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
	//					
	//					PMString insertPMStringText;
	//					insertPMStringText.Append(newTagToBeAdded.elementName);
	//					//if(!iConverter)
	//					{
	//						insertPMStringText=insertPMStringText;					
	//					}
	//					//else
	//					//	insertPMStringText=iConverter->translateString(insertPMStringText);
	//					
	//					if(headerIndex != noOfColumns - 1)
	//					{
	//						insertPMStringText.Append("\t");						
	//					}
	//					else
	//					{
	//						insertPMStringText.Append("\r");
	//					}

	//					tabbedTextData.Append(insertPMStringText);
	//					//CA("tabbedTextData 1: "+ tabbedTextData);

	//					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
	//					/*WideString insertText(insertPMStringText);
	//					if(headerIndex == 0)
	//					{
	//						//CA("headerIndex == 0");
	//						textModel->Replace(kTrue,tagStartPos,tagEndPos-tagStartPos + 1 ,&insertText);
	//						//textModel->Replace(kTrue,1 ,7,&insertText);
	//						//return;
	//					}
	//					else
	//					{
	//						textModel->Insert(kTrue,tagStartPos,&insertText);
	//					}*/				
	//					 tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//					
	//					 newTagToBeAdded.tagStartPos=tagStartPos;
	//					 newTagToBeAdded.tagEndPos=tagEndPos;
	//					 listOfNewTagsAdded.push_back(newTagToBeAdded);
	//					 // XMLReference createdElement;
	//					// Utils<IXMLElementCommands>()->CreateElement(modifiedTagName , txtModelUIDRef,tagStartPos,tagEndPos,kInvalidXMLReference, &createdElement);
	//					// addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
	//					 tagStartPos = tagEndPos+ 1 + 2;
	//		
	//				}					

	//			//	{ // For Availablity
	//			//		headerIndex++;
	//			//		int32 elementId = --806;

	//			//		SlugStruct newTagToBeAdded;
	//			//		newTagToBeAdded.elementId = elementId;
	//			//		newTagToBeAdded.typeId = -2;
	//			//		newTagToBeAdded.parentId = pNode.getPubId();
	//			//		newTagToBeAdded.imgflag = 0;
	//			//		newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//			//		newTagToBeAdded.elementName = "Availablity";
	//			//		newTagToBeAdded.TagName = prepareTagName(newTagToBeAdded.elementName);;
	//			//		newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//			//		newTagToBeAdded.whichTab =3 ;
	//			//		newTagToBeAdded.imgFlag = 0;
	//			//		newTagToBeAdded.sectionID = sectionid ;
	//			//		newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//			//		newTagToBeAdded.tableFlag = -12;
	//			//		newTagToBeAdded.row_no=-1;
	//			//		newTagToBeAdded.col_no=colnoAsHeaderFlag;					 

	//			//		PMString insertPMStringText;
	//			//		insertPMStringText.Append(newTagToBeAdded.elementName);
	//			//		if(headerIndex != noOfColumns - 1)
	//			//		{
	//			//			insertPMStringText.Append("\t");						
	//			//		}
	//			//		else
	//			//		{
	//			//			insertPMStringText.Append("\r");
	//			//		}

	//			//		tabbedTextData.Append(insertPMStringText);	

	//			//		//CA("tabbedTextData 3: "+ tabbedTextData);

	//			//		int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
	//			//		tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//			//		
	//			//		newTagToBeAdded.tagStartPos=tagStartPos;
	//			//		newTagToBeAdded.tagEndPos=tagEndPos;
	//			//		listOfNewTagsAdded.push_back(newTagToBeAdded);
	//			//		tagStartPos = tagEndPos+ 1 + 2;					
	//			//	}
	//			//	

	//				rowCount++;
	//			}
	//			//This for loop will spray the Item_copy_attributes value]
	//			vector<vector<PMString> >::iterator it1;
	//			for(it1=vectorOfRowByRowTableData.begin(); it1!=vectorOfRowByRowTableData.end(); it1++/*int32 itemIndex = 0;itemIndex <noOfRows;itemIndex++*/)
	//			{
	//				
	//				vector<PMString>::iterator it2;
	//				int32 itemId =-1;
	//				if(headerFlag == 1)//if(colnoAsHeaderFlag != -555)
	//					itemId = vec_items[rowCount-1];
	//				else
	//					itemId = vec_items[rowCount];
	//				
	//				for(it2=(*it1).begin();it2!=(*it1).end();it2++/*int32 headerIndex = 0;headerIndex <noOfColumns;headerIndex++*/)
	//				{
	//					int32 elementId = vectorOfTableHeader[colCount];
	//					
	//					PMString tagName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );

	//					SlugStruct newTagToBeAdded;
	//					newTagToBeAdded.elementId = elementId;
	//					newTagToBeAdded.typeId = -1;//itemId;
	//					newTagToBeAdded.childId = itemId;
	//					newTagToBeAdded.childTag =1;
	//					newTagToBeAdded.header = headerFlag;
	//					newTagToBeAdded.parentId = pNode.getPubId();
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//					PMString TableColName("");  
	//					TableColName.Append("ATTRID_");
	//					TableColName.AppendNumber(elementId);

	//					newTagToBeAdded.elementName = TableColName;
	//					newTagToBeAdded.TagName = prepareTagName(TableColName);
	//					newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//					newTagToBeAdded.whichTab =4 ;
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.sectionID = sectionid;
	//					newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//					newTagToBeAdded.tableFlag = -12;
	//					newTagToBeAdded.row_no=-1;
	//					newTagToBeAdded.col_no=-1;//colnoAsHeaderFlag;

	//					 //start
	//					//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
	//					PMString insertPMStringText;
	//					insertPMStringText.Append((*it2));
	//					//if(!iConverter)
	//					{
	//						insertPMStringText=insertPMStringText;					
	//					}
	//					//else
	//					//	insertPMStringText=iConverter->translateString(insertPMStringText);
	//					
	//					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
	//					if(colCount != noOfColumns - 1)
	//					{
	//						insertPMStringText.Append("\t");							
	//					}
	//					else if(rowCount != noOfRows-1)
	//					{
	//						insertPMStringText.Append("\r");
	//					}
	//					tabbedTextData.Append(insertPMStringText);
	//					//WideString insertText(insertPMStringText);						
	//					//textModel->Insert(kTrue,tagStartPos,&insertText);
	//									
	//					 tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
	//					 newTagToBeAdded.tagStartPos=tagStartPos;
	//					 newTagToBeAdded.tagEndPos=tagEndPos;
	//					 listOfNewTagsAdded.push_back(newTagToBeAdded);
	//					 //XMLReference createdElement;
	//					 //Utils<IXMLElementCommands>()->CreateElement(modifiedTagName , txtModelUIDRef,tagStartPos,tagEndPos,kInvalidXMLReference, &createdElement);
	//					// addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
	//					 tagStartPos = tagEndPos+ 1 + 2;
	//					 //end
	//					 colCount ++;

	//					 if(it2 == (*it1).begin())
	//						 it2++;
	//					//listOfNewTagsAdded.push_back(newTagToBeAdded);		
	//				}
	//				colCount =0;
	//				rowCount++;
	//			}				
	//		}//end of if IsTranspose == kFalse
	//		else
	//		{//else of if IsTranspose ==kFalse..i.e IsTranspose == kTrue. Headers are in first column
	//			/*  
	//			In case of Transpose == kTrue..the table gets sprayed like 

	//				Header11 Data11 Data12 Data13..
	//				Header22 Data21 Data22 Data23..
	//				Header33 Data31 Data32 Data33..
	//				....
	//				....
	//			So First Column is HeaderColumn and subsequent columns are Data columns.
	//			*/
	//			
	//			//for(int32 headerIndex = 0;headerIndex <noOfColumns;headerIndex++)
	//			//{
	//			//	int32 elementId = vectorOfTableHeader[headerIndex];
	//			//	//noOfRows in the for loop below is noOfRows + 1.
	//			//	//As we have to add first column which is of table headers.
	//			//	//In the earlier part(i.e in the if block of IsTranspose == kFalse)
	//			//	//we have added the header row in a separate loop.
	//			//	for(int32 itemIndex = 0;itemIndex <noOfRows + 1;itemIndex++)
	//			//	{
	//			//		int32 itemId =-1;
	//			//		PMString tagName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );
	//			//		PMString elementName;
	//			//		SlugStruct newTagToBeAdded;
	//			//		if( itemIndex == 0 && 
	//			//			colnoAsHeaderFlag != -555//Spray the table with the headers
	//			//		   )
	//			//			{
	//			//				//itemId == -2 for header.
	//			//				itemId = -2;
	//			//				elementName = tagName;
	//			//			}
	//			//		else
	//			//			{
	//			//				itemId = vec_items[itemIndex-1];
	//			//				elementName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemId,elementId,tagInfo.languageID,kTrue);
	//			//			}			
	//			//		 newTagToBeAdded.elementId = elementId;
	//			//		 newTagToBeAdded.typeId = itemId;
	//			//		 newTagToBeAdded.parentId = pNode.getPubId();
	//			//		 newTagToBeAdded.imgflag = 0;
	//			//		 newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//			//		 newTagToBeAdded.elementName = elementName;
	//			//		 newTagToBeAdded.TagName = prepareTagName(tagName);
	//			//		 newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//			//		 newTagToBeAdded.whichTab =3 ;
	//			//		 newTagToBeAdded.imgFlag = 0;
	//			//		 newTagToBeAdded.sectionID = sectionid;
	//			//		 newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//			//		 newTagToBeAdded.tableFlag = -12;
	//			//		 newTagToBeAdded.row_no=-1;
	//			//		 newTagToBeAdded.col_no=colnoAsHeaderFlag;
	//			//		
	//			//		PMString insertPMStringText;
	//			//		insertPMStringText.Append(newTagToBeAdded.elementName);
	//			//		int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
	//			//		if(itemIndex != noOfRows)
	//			//		{
	//			//			insertPMStringText.Append("\t");
	//			//			
	//			//		}
	//			//		else
	//			//		{
	//			//			insertPMStringText.Append("\r");
	//			//		}
	//			//		tabbedTextData.Append(insertPMStringText);
	//			//					
	//			//		 tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
	//			//		 newTagToBeAdded.tagStartPos=tagStartPos;
	//			//		 newTagToBeAdded.tagEndPos=tagEndPos;

	//			//		 listOfNewTagsAdded.push_back(newTagToBeAdded);
	//			//		 
	//			//		 tagStartPos = tagEndPos+ 1 + 2;
	//			//		 
	//		
	//			//	}

	//			//	

	//			//}

	//		}
	//			//CA(tabbedTextData);
	//		PMString textToInsert("");
	//		//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	//		//if(!iConverter)
	//		{
	//			textToInsert=tabbedTextData;					
	//		}
	//		//else
	//		//	textToInsert=iConverter->translateString(tabbedTextData);
	//		
	//		//WideString insertText(textToInsert);	
	//		//textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
	//		/*K2*/boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
	//		ReplaceText(textModel,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);
	//	
	//		/*PMString ASD("listOfNewTagsAdded.size() : ");
	//		ASD.AppendNumber(listOfNewTagsAdded.size());
	//		CA(ASD);*/

	//		for(int32 indexOfTagToBeAdded = 0;indexOfTagToBeAdded <listOfNewTagsAdded.size();indexOfTagToBeAdded++)
	//		{
	//			SlugStruct & newTagToBeAdded = listOfNewTagsAdded[indexOfTagToBeAdded];
	//			//CA(newTagToBeAdded.TagName);
	//		
	//			XMLReference createdElement;
	//			Utils<IXMLElementCommands>()->CreateElement(WideString(newTagToBeAdded.TagName), txtModelUIDRef,newTagToBeAdded.tagStartPos,newTagToBeAdded.tagEndPos,kInvalidXMLReference, &createdElement);
	//			addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);

	//		}
	//
	//	}//end if1
	//}while(kFalse);

	

}//end of function


//void CDataSprayer::ToggleBold_or_Italic(InterfacePtr<ITextModel> textModel, VectorHtmlTrackerPtr vectHtmlValuePtr, int32 StartTextIndex)
//{
//
//	if(vectHtmlValuePtr->size() == 0)
//		return;
//
//	/*PMString QWE("vectHtmlValuePtr->size()");
//	QWE.AppendNumber(vectHtmlValuePtr->size());
//	CA(QWE);*/
//
//	InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
//	if(!iSelectionManager)
//	{
//		//CA("NULL");
//	}
//
//	InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
//	if (!layoutSelectionSuite) 
//	{	//CA("!layoutSelectionSuite");
//		return;
//	}
//	
//	//layoutSelectionSuite->DeselectAll(); //CS3 change
//	layoutSelectionSuite->DeselectAllPageItems();
//	 
//
//	VectorHtmlTrackerValue::iterator it1;
//	for(it1 = vectHtmlValuePtr->begin(); it1!=vectHtmlValuePtr->end(); it1++)
//	{
//		//CA(" QWWW ");
//		/*PMString ASD(" StartIndex:  ");
//		ASD.AppendNumber(StartTextIndex);
//		ASD.Append("  Start : " );
//		ASD.AppendNumber(StartTextIndex + it1->startIndex);
//		ASD.Append("  End : ");
//		ASD.AppendNumber(StartTextIndex + it1->EndIndex);
//		CA(ASD);*/
//
//		RangeData rangeData(StartTextIndex + it1->startIndex ,StartTextIndex + it1->EndIndex);
//
//		UIDRef storyUIDRef(::GetUIDRef(textModel));
//		//CA("TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT");
//		ITextSelectionSuite * txtSelectionSuite = Utils<ISelectionUtils>()->QueryTextSelectionSuite(iSelectionManager);
//		txtSelectionSuite->SetTextSelection(storyUIDRef,rangeData,Selection::kDontScrollSelection,NULL);
//			
//		InterfacePtr<ITextMiscellanySuite> txtMisSuite2(static_cast<ITextMiscellanySuite* >
//		( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
//		if(!txtMisSuite2)
//		{		
//			//CA("!txtMisSuite2");
//			break;
//		}						
//		
//		InterfacePtr<ITextAttributeSuite> textAttr(txtMisSuite2,UseDefaultIID());
//		if(textAttr == NULL)
//		{
//			//CA("textAttr == NULL");
//			break;
//		}	
//		if(it1->style ==  1)
//			textAttr->ToggleBold();
//		else if(it1->style ==  2)
//			textAttr->ToggleItalic();	
//
//	}
//}

/////////////////////////////////////////////////////////////////////////////
//added temp for testing of image spray inside table cell-vijay 5-1-2007
//==================================================================from here
///////////////////////////////////////////////////////////////////////////////////
void CDataSprayer::sprayImageInTable(UIDRef& boxUIDRef, TagStruct & tagInfo)
{	

	ErrorCode errcode = kFailure;
	ErrorCode result = kFailure;
	vector<double> FinalItemIds;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return;
	}

	UID textFrameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	}

	if (textFrameUID == kInvalidUID)
	{
		return;//breaks the do{}while(kFalse)
	}

    InterfacePtr<ITextModel>  textModel;
    getTextModelByBoxUIDRef(boxUIDRef , textModel);
	if (textModel == NULL)
	{
		return;
	}

	InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
	if(tableList==NULL)
	{
		return;
	}
	int32 totalNumberOfTablesInsideTextFrame = tableList->GetModelCount();
		
	if(totalNumberOfTablesInsideTextFrame < 0) //This check is very important...  this ascertains if the table is in box or not.
	{
		return;
	}
				
	for(int tableIndex = 0; tableIndex<totalNumberOfTablesInsideTextFrame; tableIndex++)
	{
		for(int32 tagIndex = 0;tagIndex < tagInfo.tagPtr->GetChildCount();tagIndex++)
		{				
			XMLReference cellXMLElementRef =   tagInfo.tagPtr->GetNthChild(tagIndex);
			InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
			if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
			{	//CA("inside for->if");
				continue;
			}
			XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(0);
			InterfacePtr<IIDXMLElement>cellTextTagPtr(cellTextXMLRef.Instantiate());
			//The cell may be blank. i.e doesn't have any text tag inside it.
			if(cellTextTagPtr == NULL)
			{	//CA("cellTextTagPtr == NULL");
				continue;
			}

			XMLContentReference cntentREF = cellTextTagPtr->GetContentReference();
			UIDRef ref = cntentREF.GetUIDRef();
			if(ref == UIDRef::gNull)
			{
				//CA("ref == UIDRef::gNull");
				continue;
			}

			InterfacePtr<ITagReader> itagReader
			((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
			if(!itagReader)
				return ;
			TagList tList=itagReader->getTagsFromBox(ref);
			TagStruct tagInfoo;
			int numTags=static_cast<int>(tList.size());

			if(numTags<0)
			{
				return;
			}

			tagInfoo=tList[0];
			if(cellTextTagPtr->GetAttributeValue(WideString("imgFlag")) == WideString("1"))
			{
				this->fillImageInBox (ref ,tagInfoo, idList[tagInfoo.whichTab-1]);
				setTagParent(tagInfoo, idList[tagInfoo.whichTab-1]);
			}	
			
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
		}
	}
	fitInlineImageInBoxInsideTableCell(boxUIDRef);
	return;
}

void CDataSprayer::fitInlineImageInBoxInsideTableCell(UIDRef& boxUIDRef)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return;
	}

	IDataBase * database = boxUIDRef.GetDataBase();

	UID textFrameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	}
	UIDRef textFrameUIDref(database,textFrameUID);

    InterfacePtr<ITextModel>  txtModel;
    getTextModelByBoxUIDRef(boxUIDRef , txtModel);
	if(!txtModel)
	{
		//CA("Text Model is null");
		return ;
	}

	K2Vector<UIDRef> inlinesList;
				
	IDataBase* db = ::GetDataBase(txtModel);
	OwnedItemDataList ownedList;
	Utils<ITextUtils>()->CollectOwnedItems(txtModel,
										0,
										txtModel->GetPrimaryStoryThreadSpan(), 
										&ownedList,
										kTrue);


	for (int32 i = 0; i < ownedList.size(); i++)
	{
		if ((ownedList[i].fClassID == kInlineBoss))
		{ 
			inlinesList.push_back(UIDRef(db, ownedList[i].fUID));
		}
	}

	K2Vector<UIDRef>::const_iterator iter;
	for (iter = inlinesList.begin(); iter < inlinesList.end(); ++iter)
	{
		UIDRef inlineBossUIDRef = *iter;
		InterfacePtr<IHierarchy> hier(inlineBossUIDRef, UseDefaultIID());
		ASSERT(hier);
		if (!hier) {// CA("!hier");
			break;
		}
		if (hier->GetChildCount() < 1) {
			//CA("hier->GetChildCount() < 1");
			break;
		}
		UID uid = hier->GetChildUID(0);
		if (uid == kInvalidUID) { 
			//CA("uid == kInvalidUID");
			break;
		}
		UIDRef placedThingUIDRef(inlineBossUIDRef.GetDataBase(),uid);
					
		InterfacePtr<ICommand> iAlignCmd(CmdUtils::CreateCommand(kFitContentPropCmdBoss));
		if(!iAlignCmd)
			return;

		InterfacePtr<IHierarchy> iHier(placedThingUIDRef, IID_IHIERARCHY);
		if(!iHier)
			return;

		if(iHier->GetChildCount()==0)//There may not be any image at all????
			return;
        
		UID childUID=iHier->GetChildUID(0);
		UIDRef newChildUIDRef(placedThingUIDRef.GetDataBase(), childUID);
		iAlignCmd->SetItemList(UIDList(newChildUIDRef));
		CmdUtils::ProcessCommand(iAlignCmd);
	}//end of inner for loop
}


void CDataSprayer:: sprayTableInsideTableCell(UIDRef& boxUIDRef, TagStruct & tagInfo)
{
	XMLReference tagInfoXMLRef = tagInfo.tagPtr->GetXMLReference();
		//This is a special case where inside a single table the TableData of multiple products is sprayed.
		//Here in the cell of a table there is ScreenTable of the product or item.and when
		//multiple product list or item list is sprayed in each row's cell,corressponding item's or 
		//product's ScreenTable is sprayed.
		do
		{
				InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
				if(ptrIAppFramework == NULL)
				{
					//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
					break;
				}

				UIDRef tableUIDRef(UIDRef::gNull);

				UID textFrameUID = kInvalidUID;
				InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
				if (graphicFrameDataOne) 
				{
					textFrameUID = graphicFrameDataOne->GetTextContentUID();
				}
				if (textFrameUID == kInvalidUID)
				{
					break;
				}

                InterfacePtr<ITextModel>  textModel;
                getTextModelByBoxUIDRef(boxUIDRef , textModel);
				if (textModel == NULL)
				{
					break;
				}
				UIDRef StoryUIDRef(::GetUIDRef(textModel));

				InterfacePtr<ITagReader> itagReader
				((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
				if(!itagReader)
					break;
				

				TableUtility tutil(NULL);
				XMLContentReference xmlCntnRef = tagInfo.tagPtr->GetContentReference();
				InterfacePtr<IXMLReferenceData> xmlRefData(xmlCntnRef.Instantiate());
				if(xmlRefData == NULL)
				{
					//CA("xmlRefData == NULL");
					break;
				}

				InterfacePtr<ITableModel> tableModel(xmlRefData,UseDefaultIID());
				if(tableModel == NULL)
				{
					//CA("tableModel == NULL");
					break;
				}

				int32 rowPatternRepeatCount = 1;//pNodeDataList_ForTableSprayInsideTableCell.size()-1;//pNodeDataList.size();

				int32 totalNumberOfColumns = tableModel->GetTotalCols().count;
				int32 totalNumberOfRowsBeforeRowInsertion =  tableModel->GetTotalRows().count;

				int32 rowsToBeStillAdded = rowPatternRepeatCount * totalNumberOfRowsBeforeRowInsertion - totalNumberOfRowsBeforeRowInsertion;
   
				GridArea gridAreaForEntireTable(0,0,totalNumberOfRowsBeforeRowInsertion,totalNumberOfColumns);
				InterfacePtr<ITableGeometry> tableGeometry(tableModel,UseDefaultIID());
				if(tableGeometry == NULL)
				{
					//CA("tableGeometry == NULL");
					break;
				}
				PMReal rowHeight = tableGeometry->GetRowHeights(totalNumberOfRowsBeforeRowInsertion-1); 
				
				InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
				if(tableCommands==NULL)
				{
					//CA("Err: invalid interface pointer ITableCommands");
					break;//continues the for..tableIndex
				}

				for(int32 rowIndex = 0;rowIndex < rowsToBeStillAdded ;rowIndex++)
				{
					tableCommands->InsertRows(RowRange(totalNumberOfRowsBeforeRowInsertion+ rowIndex-1,1),Tables::eAfter,rowHeight);
				}

				bool16 canCopy = tableModel->CanCopy(gridAreaForEntireTable);
				if(canCopy == kFalse)
				{
					//CA("canCopy == kFalse");
					break;
				}			
	
				TableMemento * tempPtr = tableModel->Copy(gridAreaForEntireTable);
				if(tempPtr == NULL)
				{
					//CA("tempPtr == NULL");
					break;
				}
		
				for(int32 pasteIndex = 1;pasteIndex < rowPatternRepeatCount;pasteIndex++)
				{
					tableModel->Paste(GridAddress(pasteIndex * totalNumberOfRowsBeforeRowInsertion,0),ITableModel::eAll,tempPtr);
					tempPtr = tableModel->Copy(gridAreaForEntireTable);
				}

				Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString("ID"),WideString("-103"));
				//parentID
				PMString attributeVal("");
				attributeVal.AppendNumber(PMReal(pNode.getPubId()));
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString("parentID"),WideString(attributeVal));
				attributeVal.Clear();
				//parentTypeID
				attributeVal.AppendNumber(PMReal(pNode.getTypeId()));
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString("parentTypeID"),WideString(attributeVal));
				attributeVal.Clear();
				//sectionID
				attributeVal.AppendNumber(PMReal(pNode.getParentId()));
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef,   WideString("sectionID"),WideString(attributeVal));
				attributeVal.Clear();

				if(pNode.getIsProduct() == 1)
				{
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("index"),WideString("3"));
				}
				if(pNode.getIsProduct() == 0)
				{
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("index"),WideString("4"));
				}

				for(int32 rowPatternIndex = 0;rowPatternIndex < rowPatternRepeatCount;rowPatternIndex++)
				{//start for rowPatternIndex

					for(int32 indexOfRowInRowPattern = 0;indexOfRowInRowPattern <totalNumberOfRowsBeforeRowInsertion;indexOfRowInRowPattern++)
					{//start for indexOfRowInRowPattern
						for(int32 columnIndex = 0;columnIndex < totalNumberOfColumns;columnIndex++)
						{//start for..iterate through each column.
							int32 tagIndex = 
							rowPatternIndex * totalNumberOfRowsBeforeRowInsertion * totalNumberOfColumns +
							indexOfRowInRowPattern * totalNumberOfColumns + columnIndex;				
							XMLReference cellXMLElementRef = tagInfo.tagPtr->GetNthChild(tagIndex);
							
							//This is a tag attached to the cell.
							InterfacePtr<IIDXMLElement>cellXMLElementPtr(cellXMLElementRef.Instantiate());
							//Get the text tag attached to the text inside the cell.
							//We are providing only one texttag inside cell.
							if(cellXMLElementPtr==NULL || cellXMLElementPtr->GetChildCount()<= 0)
							{
								continue;
							}
							
							XMLContentReference cellXMLCntnRef = cellXMLElementPtr->GetContentReference();
							InterfacePtr<IXMLReferenceData>cellXMLRefData(cellXMLCntnRef.Instantiate());

							XMLReference cellTextXMLRef = cellXMLElementPtr->GetNthChild(0);
							InterfacePtr<IIDXMLElement>cellTextTagPtr(cellTextXMLRef.Instantiate());
							//The cell may be blank. i.e doesn't have any text tag inside it.
							if(cellTextTagPtr == NULL)
							{
								continue;
							}
							XMLContentReference xmlCntnRef = cellTextTagPtr->GetContentReference();
							InterfacePtr<IXMLReferenceData>xmlRefData(xmlCntnRef.Instantiate());
							
							if(xmlCntnRef.IsTable())
							{
								//CA("xmlCntnRef.IsTable inline Table");
								InterfacePtr<ITableModel> innerTableModel(xmlRefData,UseDefaultIID());
								if(innerTableModel == NULL)
								{
									//CA("innerTableModel == NULL");
									continue;
								}
								UIDRef innerTableModelUIDRef = ::GetUIDRef(innerTableModel);
				
								XMLTagAttributeValue xmlTagAttrVal;
								//collect all the attributes from the tag.
								//typeId,LanguageID are important
								getAllXMLTagAttributeValues(cellTextXMLRef,xmlTagAttrVal);	

								TableUtility tUtils(NULL);

								if(xmlTagAttrVal.typeId.GetAsDouble() == -111)
								{
									tUtils.fillDataInCMedCustomTableInsideTable
									(
										cellTextXMLRef,
										innerTableModelUIDRef,
										pNode
									);
								}
								else
								{
									tUtils.FillDataTableForItemOrProduct
									(
										cellTextXMLRef,
										innerTableModelUIDRef,
										pNode.getIsProduct()
									);
								}

								InterfacePtr<ICommand> fitFrameToContentCmd(CmdUtils::CreateCommand(kFitFrameToContentCmdBoss));
								ASSERT(fitFrameToContentCmd != nil);
								if (fitFrameToContentCmd == nil) {
								return ;
								}
								fitFrameToContentCmd->SetItemList(UIDList(tagInfo.curBoxUIDRef));
								if (CmdUtils::ProcessCommand(fitFrameToContentCmd) != kSuccess) {
								ASSERT_FAIL("kFitFrameToContentCmdBoss failed");
								return ;
								}
								
							}
							else if(xmlCntnRef.IsInline())
							{	
								//CA("is Inline Image true");
								UIDRef ref = xmlCntnRef.GetUIDRef();
								if(ref == UIDRef::gNull)
								{
									//CA("ref == UIDRef::gNull");
									continue;
								}
								
								TagList tList=itagReader->getTagsFromBox(ref);
								TagStruct tagInfoo;
								int numTags=static_cast<int>(tList.size());

								if(numTags<0)
								{
									return;
								}

								tagInfoo=tList[0];
								if(cellTextTagPtr->GetAttributeValue(WideString("imgFlag")) == WideString("1"))
								{
									this->fillImageInBox (ref ,tagInfoo, idList[tagInfoo.whichTab-1]);
									setTagParent(tagInfoo, idList[tagInfoo.whichTab-1]);
								}
								for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
								{
									tList[tagIndex].tagPtr->Release();
								}
							}
							else 
							{
								//CA("Final Else");
								
								PMString strElementID = cellTextTagPtr->GetAttributeValue(WideString("ID"));
								PMString strLanguageID = cellTextTagPtr->GetAttributeValue(WideString("LanguageID"));
								//check whether the tag specifies ProductCopyAttributes.
								PMString tableFlag =   cellTextTagPtr->GetAttributeValue(WideString("tableFlag"));
								PMString strParentTypeID =   cellTextTagPtr->GetAttributeValue(WideString("parentTypeID"));//added by Tushar on 27/12/06

                                double elementID = strElementID.GetAsDouble();
								double languageID = strLanguageID.GetAsDouble();
								double parentTypeID = strParentTypeID.GetAsDouble();//added by Tushar on 27/12/06										

								int32 tagStartPos = -1;
								int32 tagEndPos = -1;
								Utils<IXMLUtils>()->GetElementIndices(cellTextTagPtr,&tagStartPos,&tagEndPos);
								XMLReference cellTextTagXMLRef = cellTextTagPtr->GetXMLReference();
								tagStartPos = tagStartPos + 1;
								tagEndPos = tagEndPos -1;
								
								PMString dispName("");							
								if(tableFlag == "1")
								{										
									//The following code will spray the table preset inside the table cell.
									//for copy ID = -104
									PMString index = cellTextTagPtr->GetAttributeValue(WideString("index"));
									if(index == "3")// || index == "4")
									{//Product ItemTable
										PMString typeID = cellTextTagPtr->GetAttributeValue(WideString("typeId"));
										TagStruct tagInfo;												
										tagInfo.whichTab = index.GetAsNumber();
										if(index == "3")
											tagInfo.parentId = pNode.getPubId();
										/*else if(index == "4")
											tagInfo.parentId = pNode.getPBObjectID();*/

										tagInfo.isTablePresent = tableFlag.GetAsNumber();
										tagInfo.typeId = typeID.GetAsDouble();
										tagInfo.sectionID = pNode.getParentId();
										
											
										tutil.GetItemTableInTabbedTextForm(tagInfo,dispName);
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextTagXMLRef, WideString("ID"),WideString("-104"));
										//CA(dispName);
									}
									else if(index == "4")
									{//This is the Section level ItemTableTag and we have selected the product
									//for spraying
										makeTheTagImpotent(cellTextTagPtr);										
									}
									
								}
								else if(tableFlag == "0")
								{
									PMString index = cellTextTagPtr->GetAttributeValue(WideString("index"));
									if( index == "3")
									{//Product
										

										dispName = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(pNode.getPubId(),elementID,languageID, CurrentSectionID, kTrue);
										PMString attrVal;
										attrVal.AppendNumber(PMReal(pNode.getTypeId()));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextTagXMLRef, WideString("typeId"),WideString(attrVal));
										attrVal.Clear();

										attrVal.AppendNumber(PMReal(pNode.getPubId()));
										Utils<IXMLAttributeCommands>()->SetAttributeValue(cellTextTagXMLRef, WideString("parentID"),WideString(attrVal));								
									}
								}
							
								PMString textToInsert("");
								textToInsert=dispName;
                                
                                textToInsert.ParseForEmbeddedCharacters();
								boost::shared_ptr<WideString> insertData(new WideString(textToInsert));
								ReplaceText(textModel,tagStartPos,tagEndPos-tagStartPos + 1,insertData);
							}
						}//end for ...interate through each column
					}//end for indexOfRowInRowPattern 
				}//end for rowPatternIndex

				fitInlineImageInBoxInsideTableCell(boxUIDRef);
		}while(kFalse);
}//end function sprayTableInsideTableCell

//
void CDataSprayer:: sprayItemImage(UIDRef& boxUIDRef, TagStruct & tagInfo, PublicationNode& pNode,double itemID)
{
	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}
		
		InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader)
		{
			ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::doesExist::!itagReader");	
			break;
		}

		InterfacePtr<IHierarchy> iHier(boxUIDRef, UseDefaultIID());
		if(!iHier)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:!iHier");				
			break;
		}

		
		UIDRef parentUIDRef(boxUIDRef.GetDataBase(), iHier->GetParentUID());
		InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{
			break;
		}

		if (iSelectionManager->SelectionExists (kInvalidClass/*any CSB*/, ISelectionManager::kAnySelection)) {
			// Clear the selection
			iSelectionManager->DeselectAll(nil);
		}

		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
		if (!layoutSelectionSuite) 
		{	//CA("!layoutSelectionSuite");
			break;
		}

		//layoutSelectionSuite->DeselectAll(); //CS3 change
		layoutSelectionSuite->DeselectAllPageItems();
		UIDList selectUIDList(boxUIDRef);
		layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);
		
		InterfacePtr<IGeometry> iGeo(boxUIDRef, IID_IGEOMETRY);
		if(!iGeo)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:!iGeo");								
			break;
		}

		PMRect pageBounds=iGeo->GetStrokeBoundingBox(InnerToPasteboardMatrix(iGeo));
		PMRect FirstFrameBounds = pageBounds;
		VectorLongIntValue::iterator it;
		PMReal margin = 0;
		PMString TagName("");
		int32 Flag=0;
		
		bool16 ItemImageUnspreadFlag = kFalse;
		UIDRef newItem;	

		XMLReference tagRef = tagInfo.tagPtr->GetXMLReference();
		XMLTagAttributeValue tagVal;
		convertTagStructToXMLTagAttributeValue(tagInfo,tagVal);

		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==NULL)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:!ptrIClientOptions");
			break;
		}

		PMString imagePath=ptrIClientOptions->getImageDownloadPath();
			
		if(imagePath=="")
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:imagePath == blank");					
			break;
		}

		if(imagePath!="")
		{
			const char *imageP=imagePath.GetPlatformString().c_str();
			if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
			#ifdef MACINTOSH
				imagePath+="/";
			#else
				imagePath+="\\";
			#endif
		}
		
		PMString fileName("");
		double assetID;
		double mpv_value_id = -1;

		VectorAssetValuePtr AssetValuePtrObj = NULL;

		int32 isProduct =0;
		if(tagInfo.typeId != -99)
		{
			if(!isInlineImage)
				AssetValuePtrObj= ptrIAppFramework->GETAssets_GetAssetByParentAndType(itemID, CurrentSectionID, isProduct, tagInfo.typeId);
			else				
				AssetValuePtrObj= ptrIAppFramework->GETAssets_GetAssetByParentAndType(itemID, CurrentSectionID,isProduct,  tagInfo.typeId);			
		}
		else
		{				
			AssetValuePtrObj= ptrIAppFramework->GETAssets_GetAssetByParentAndType(itemID, CurrentSectionID, isProduct);
		}

		if(AssetValuePtrObj == NULL){
			ItemImageUnspreadFlag = kTrue;
			continue ;
		}

		if(AssetValuePtrObj->size() == 0)
		{
			ItemImageUnspreadFlag = kTrue;
			continue;
		}
		
		PMString CutomerAssetTypes("");
		PMString DivisionAssetTypes("");
		if(ptrIAppFramework->CONFIGCACHE_getShowCustomerInIndesign())
		{
			if(ptrIAppFramework->getSelectedCustPVID(1) != -1)
				CutomerAssetTypes = ptrIAppFramework->CONFIGCACHE_getCustomerAssetTypes(1);
			if(ptrIAppFramework->getSelectedCustPVID(0) != -1)
				DivisionAssetTypes = ptrIAppFramework->CONFIGCACHE_getCustomerAssetTypes(0);
		
		}
		VectorAssetValue::iterator it1; // iterator of Asset value

		CAssetValue objCAssetvalue;
		double tempTypeid = -1;

		for(it1 = AssetValuePtrObj->begin();it1!=AssetValuePtrObj->end();it1++)
		{
			//CA("Inside Asset For.....");
			objCAssetvalue = *it1;
			fileName = objCAssetvalue.geturl();
			assetID = objCAssetvalue.getAsset_id();
			mpv_value_id = objCAssetvalue.getMpv_value_id();
			
		
			double typeId = -1;
			if(tagInfo.typeId == -99)
			{
				typeId = objCAssetvalue.getType_id();
				tempTypeid = typeId;
			}
			else
			{
				typeId = tagInfo.typeId;
				tempTypeid = typeId;
			}

			
			if(ptrIAppFramework->getSelectedCustPVID(1) != -1)
				if(CutomerAssetTypes.Compare(kFalse,"") != 0)
				{
					PMString temp("");
					temp.AppendNumber(PMReal(typeId));
					//CA("typeId : "+temp);
					if(CutomerAssetTypes.Contains(temp)==kTrue)			
						if(mpv_value_id != ptrIAppFramework->getSelectedCustPVID(1))
							continue;													
				}
			if(ptrIAppFramework->getSelectedCustPVID(0) != -1)
				if(DivisionAssetTypes.Compare(kFalse,"") != 0)
				{
					PMString temp("");
					temp.AppendNumber(PMReal(typeId));
					//CA("typeId : "+temp);
					if(DivisionAssetTypes.Contains(temp)==kTrue)			
						if(mpv_value_id != ptrIAppFramework->getSelectedCustPVID(0))
							continue;													
				}
											
			if(fileName=="")
				continue;

			UIDRef newItem;	
			do
			{
				SDKUtilities::Replace(fileName,"%20"," ");
			}while(fileName.IndexOfString("%20") != -1);
		
			PMString imagePathWithSubdir = imagePath;
			PMString typeCodeString;

			#ifdef WINDOWS
				imagePathWithSubdir.Append("\\");
			#endif
			
			if(!fileExists(imagePathWithSubdir,fileName))
			{	
				//CA("Image Not Found on Local Drive");
				continue;
			}
		
			PMString total=imagePathWithSubdir+fileName;
            
            #ifdef MACINTOSH
                SDKUtilities::convertToMacPath(total);
            #endif

			if(margin != 0 )
			{
				if(isHorizontalFlow()==kTrue)
				{
					pageBounds.Top(pageBounds.Top());
					pageBounds.Bottom(pageBounds.Bottom());
					pageBounds.Left(pageBounds.Left()+margin);
					pageBounds.Right(pageBounds.Right()+margin);
				}
				else
				{
					pageBounds.Top(pageBounds.Top()+margin);
					pageBounds.Bottom(pageBounds.Bottom()+margin);
					pageBounds.Left(pageBounds.Left());
					pageBounds.Right(pageBounds.Right());
				}
				
				bool16 stat = copySelectedItems(newItem,pageBounds);
				if(!stat)
					break;
	
				UIDList tempUIDLIst(newItem.GetDataBase(), newItem.GetUID());
				
				NewImageFrameUIDList.push_back( newItem.GetUID());
                NewImageFrameUIDListForMap.push_back(newItem);

				if(ImportFileInFrame(newItem, total))
				{
					Flag=1;	
					//fitImageInBox(newItem);						
				}
                /// Creating and Adding tag to newly created Item image box
			
				/// UpDating the Copied Tag
				TagList tList1 = itagReader->getTagsFromBox(newItem);
				TagStruct tagInfo1 = tList1[0];

				XMLReference tagInfo1XMLRef = tagInfo1.tagPtr->GetXMLReference();
				
				PMString attributeName = "typeId";
				PMString attributeValue("");
				attributeValue.AppendNumber(PMReal(typeId));
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfo1XMLRef, WideString(attributeName),WideString(attributeValue));

				for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
				{
					tList1[tagIndex].tagPtr->Release();
				}

			}
			else
			{
				newItem = boxUIDRef;
				if(ImportFileInFrame(boxUIDRef, total))
				{		
					Flag=1;	
					//fitImageInBox(boxUIDRef);						
				}
				PMString attribVal;
				attribVal.AppendNumber(PMReal( tagInfo.parentId));

				XMLReference tagInfoXMLRef = tagInfo.tagPtr->GetXMLReference();
				
				if(tagInfo.tagPtr)
				{	
					TagName = tagInfo.tagPtr->GetTagString();
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("parentID"),WideString(attribVal));//ObjectID
					attribVal.Clear();							
					
					attribVal.AppendNumber(PMReal(CurrentSubSectionID));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("sectionID"),WideString(attribVal));//Publication ID								
					attribVal.Clear();

					attribVal.AppendNumber(PMReal(tagInfo.parentTypeID));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("parentTypeID"),WideString(attribVal));//Parent Type ID
					attribVal.Clear();

					attribVal.AppendNumber(PMReal(typeId));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("typeId"),WideString(attribVal));//Parent Type ID
					attribVal.Clear();

					attribVal.AppendNumber(PMReal(mpv_value_id));
					Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("rowno"),WideString(attribVal));//Parent Type ID
					attribVal.Clear();							
					
				}
			}
					
			margin = pageBounds.Right() -pageBounds.Left() +2;

			if((tagInfo.typeId == -99))
				break;
		}

		if(ItemImageUnspreadFlag == kTrue)
		{
			//attach tags
			PMString attribValue;
			attribValue.AppendNumber(PMReal( tagInfo.parentId));
			XMLReference tagInfoXMLRef = tagInfo.tagPtr->GetXMLReference();
			
			if(tagInfo.tagPtr)
			{	
				TagName = tagInfo.tagPtr->GetTagString();
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("parentID"),WideString(attribValue));//ObjectID
				attribValue.Clear();							
				
				attribValue.AppendNumber(PMReal(CurrentSubSectionID));
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("sectionID"),WideString(attribValue));//Publication ID								
				attribValue.Clear();

				attribValue.AppendNumber(PMReal(tagInfo.parentTypeID));
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("parentTypeID"),WideString(attribValue));//Parent Type ID
				attribValue.Clear();

				attribValue.AppendNumber(PMReal(tempTypeid));
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("typeId"),WideString(attribValue));//Parent Type ID
				attribValue.Clear();

				attribValue.AppendNumber(PMReal(mpv_value_id));
				Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("rowno"),WideString(attribValue));//Parent Type ID
				attribValue.Clear();
			}
		}

		if(isInlineImage)
		{
			XMLContentReference contentRef = tagInfo.tagPtr->GetContentReference();																									
			UIDRef ContentRef = contentRef.GetUIDRef();

			SelectFrame(ContentRef);	
			InterfacePtr<ISelectionManager>	iSelectionManager1 (Utils<ISelectionUtils> ()->QueryActiveSelection ());
			if(!iSelectionManager1)
			{				
				 break;
			}

			if (iSelectionManager->SelectionExists (kInvalidClass/*any CSB*/, ISelectionManager::kAnySelection)) {
				// Clear the selection
				iSelectionManager->DeselectAll(nil);
			}
			InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
			( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID ,iSelectionManager1))); 
			if(!txtMisSuite)
			{
				//CA("txtMisSuite ==  NULL ");
				break ; 
			}			
			txtMisSuite->setFrameUser();				
		}
	}while(false);	
	return;
}


int CDataSprayer::deleteThisBox(UIDRef boxUIDRef)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFalse;
	InterfacePtr<IHierarchy> ptrHierarchy (boxUIDRef, UseDefaultIID());
	if(ptrHierarchy == NULL)
	{
		//CA("ptrHierarchy == NULL");
	}
	UIDList DleteBoxList(boxUIDRef.GetDataBase());
	DleteBoxList.Append(boxUIDRef.GetUID());

	UID parentUID = ptrHierarchy->GetParentUID();
	if(parentUID == kInvalidUID)
	{
		//CA("parentUID == kInvalidUID");
	}
	else
	{
		UIDRef ParentUIDRef(boxUIDRef.GetDataBase(), parentUID);
		bool16 isGroupFrame = kFalse;
		isGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(ParentUIDRef);
		if(isGroupFrame == kTrue)
		{
			//CA("Is Parent Grouped Frame");
			DleteBoxList.Append(ParentUIDRef.GetUID());
			InterfacePtr<IScrapItem> scrap(ParentUIDRef, UseDefaultIID());
			if(scrap==nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::deleteThisBox::scrap is nil");	
				return 0;
			}
			InterfacePtr<ICommand> command (scrap->GetDeleteCmd());
			if(!command)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::deleteThisBox::!command");	
				return 0;
			}

			command->SetItemList(UIDList(ParentUIDRef));
			if(CmdUtils::ProcessCommand(command)!=kSuccess)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::deleteThisBox::!kSuccess");	
				return 0;
			}

		}else
		{
			//CA("Not Parent a Group Frame");
		}
	}

	InterfacePtr<IScrapItem> scrap(boxUIDRef, UseDefaultIID());
	if(scrap==nil)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::deleteThisBox::scrap is nil");	
		return 0;
	}
	InterfacePtr<ICommand> command (scrap->GetDeleteCmd());
	if(!command)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::deleteThisBox::!command");	
		return 0;
	}

	command->SetItemList(UIDList(boxUIDRef));
	if(CmdUtils::ProcessCommand(command)!=kSuccess)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::deleteThisBox::!kSuccess");	
		return 0;
	}
    isBoxdDeleted = kTrue;
	return 1;
}


// Added to spray Item's XRef table. in tabbed text format.
void CDataSprayer::sprayXRefTableScreenPrintInTabbedTextForm(TagStruct & tagInfo)
{
	//CA("inside sprayXRefTableScreenPrintInTabbedTextForm function");
	//int32 CopyFlag=0;

	//PMString elementName,colName;

	//vector<int32> FinalItemIds;
	//bool16 firstTabbedTextTag = kTrue;
	//tabbedTagStructList.clear();

	//UIDRef txtModelUIDRef = UIDRef::gNull;
	//InterfacePtr<ITextModel> textModel;
	//
	//TextIndex startPos=-1;
	//TextIndex endPos = -1;
	////First collect all the tabbedTextTag in a vector tabbedTagStructList.
	//		
	//	//check for the tabbedTextTag
	//int32 replaceItemWithTabbedTextFrom = -1;
	//int32 replaceItemWithTabbedTextTo = -1;
	//PMString tabbedTextData;
	//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//if(ptrIAppFramework == NULL)
	//{
	//	CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
	//	return;
	//}

	//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));

	//do{
	//	//CA("Before do while");
	//	if(tagInfo.whichTab == 4 && tagInfo.parentId == -1 && tagInfo.isTablePresent == kTrue)
	//	{//start if1
	//		//CA("Inside Do while");
	//		/*InterfacePtr<IPMUnknown> unknown(tagInfo.curBoxUIDRef, IID_IUNKNOWN);
	//		if(unknown == NULL)
	//		{
	//			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayXRefTableScreenPrintInTabbedTextForm::!unknown");
	//			break;
	//		}
	//		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
	//		UID textFrameUID = kInvalidUID;
	//		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(tagInfo.curBoxUIDRef, UseDefaultIID());
	//		if (graphicFrameDataOne) 
	//		{
	//			textFrameUID = graphicFrameDataOne->GetTextContentUID();
	//		}
	//		if (textFrameUID == kInvalidUID)
	//		{
	//			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayXRefTableScreenPrintInTabbedTextForm::!textFrameUID");
	//			break;
	//		}
	//			
	//		/*InterfacePtr<ITextFrame> textFrame(tagInfo.curBoxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
	//		if (textFrame == NULL)
	//		{
	//			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayXRefTableScreenPrintInTabbedTextForm::!textFrame");
	//			break;
	//		}*/

	//		InterfacePtr<IHierarchy> graphicFrameHierarchy(tagInfo.curBoxUIDRef, UseDefaultIID());
	//		if (graphicFrameHierarchy == nil) 
	//		{
	//			//CA("graphicFrameHierarchy is NULL");
	//			return;
	//		}

	//		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
	//		if (!multiColumnItemHierarchy) {
	//		//CA("multiColumnItemHierarchy is NULL");
	//		}
	//		InterfacePtr<IMultiColumnTextFrame>
	//		frameItemTFC(multiColumnItemHierarchy, UseDefaultIID());
	//		if (!frameItemTFC) {
	//			//CA("!!!IMultiColumnTextFrame");
	//			return;
	//		}
	//			
	//		InterfacePtr<ITextModel> temptextModel(frameItemTFC->QueryTextModel());
	//		if (temptextModel == NULL)
	//		{
	//			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayXRefTableScreenPrintInTabbedTextForm::!temptextModel");
	//			break;
	//		}
	//		textModel = temptextModel;

	//		txtModelUIDRef = ::GetUIDRef(textModel);
	//		
	//		int32 sectionid = -1;
	//		if(global_project_level == 3)
	//			sectionid = CurrentSubSectionID;
	//		if(global_project_level == 2)
	//			sectionid = CurrentSectionID;
	//		
	//		PMString attrVal;
	//		attrVal.AppendNumber(pNode.getPubId());	
	//		tagInfo.tagPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
	//		attrVal.Clear();
	//		
	//		attrVal.AppendNumber(CurrentObjectTypeID);
	//		tagInfo.tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attrVal));
	//		attrVal.Clear();
	//		
	//		attrVal.AppendNumber(sectionid);
	//		tagInfo.tagPtr->SetAttributeValue(WideString("sectionID"),WideString(attrVal));
	//		attrVal.Clear();
	//		//tableFlag =-11 to identify that this is the "item" table tag in the 
	//		//tabbed text format.
	//		tagInfo.tagPtr->SetAttributeValue(WideString("tableFlag"),WideString("-11"));
	//		//change the ID from -1 to -101, as -1 is elementId for productNumber.
	//		tagInfo.tagPtr->SetAttributeValue(WideString("ID"),WideString("-101"));

	//		tagInfo.tagPtr->SetAttributeValue(WideString("childTag"),WideString("1"));

	//		//end update the attributes of the parent tag
	//		//IIDXMLElement * itemTagPtr = tagInfo.tagPtr;
	//		int32 tagStartPos = 0;
	//		int32 tagEndPos =0; 			
	//		Utils<IXMLUtils>()->GetElementIndices( tagInfo.tagPtr,&tagStartPos,&tagEndPos);
	//		tagStartPos = tagStartPos + 1;
	//		tagEndPos = tagEndPos -1;

	//		tabbedTextData.Clear();
	//		replaceItemWithTabbedTextFrom = tagStartPos;
	//		replaceItemWithTabbedTextTo	= tagEndPos;
	//					
	//		bool16 isTableDataPresent = kTrue;
	//		
	//		int32 itemId = pNode.getPubId(); //vec_item_new[0]/*30017240*/;
	//		
	//		/*PMString ASD("itemid: ");
	//		ASD.AppendNumber(itemId);
	//		CA(ASD);*/

	//		VectorScreenTableInfoPtr XreftableInfo = NULL;

	//		XreftableInfo= ptrIAppFramework->GETItem_getXRefScreenTableByItemIdLanguageId(itemId, tagInfo.languageID); 
	//		if(!XreftableInfo)
	//		{
	//			ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::sprayXRefTableScreenPrintInTabbedTextForm::!XreftableInfo");
	//				isTableDataPresent = kFalse;

	//			if(isTableDataPresent == kFalse)
	//			{
	//				PMString textToInsert("");					
	//				if(!iConverter)
	//				{
	//					textToInsert=tabbedTextData;					
	//				}
	//				else
	//					textToInsert=iConverter->translateString(tabbedTextData);				
	//				//WideString insertText(textToInsert);	
	//				//textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
	//				/*K2*/boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
	//				ReplaceText(textModel,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);

	//				break;
	//			}
	//			break;
	//			return;
	//		}

	//		if(XreftableInfo->size()==0)
	//		{
	//			//CA(" XreftableInfo->size()==0");
	//			isTableDataPresent = kFalse;

	//			if(isTableDataPresent == kFalse)
	//			{
	//				PMString textToInsert("");					
	//				if(!iConverter)
	//				{
	//					textToInsert=tabbedTextData;					
	//				}
	//				else
	//					textToInsert=iConverter->translateString(tabbedTextData);				
	//				//WideString insertText(textToInsert);	
	//				//textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
	//				/*K2*/boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
	//				ReplaceText(textModel,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);

	//				break;
	//			}
	//			break;
	//		}
	//			
	//		CItemTableValue oTableValue;
	//		VectorScreenTableInfoValue::iterator it;
	//		it = XreftableInfo->begin();
	//		oTableValue = *it;
	//		isTableDataPresent = kTrue;
	//		
	//		vector<int32> vectorOfTableHeader = oTableValue.getTableHeader();
	//		vector<vector<PMString> >  vectorOfRowByRowTableData = oTableValue.getTableData();
	//		vector<int32> vec_items = oTableValue.getItemIds();
	//		bool8 isTranspose = kFalse;
	//		vector<vector<SlugStruct> > RowList;
	//		vector<SlugStruct> listOfNewTagsAdded;

	//		int32 noOfColumns =static_cast<int32>(vectorOfTableHeader.size());
	//		int32 noOfRows = static_cast<int32>(vec_items.size());			

	//	/*	PMString numRow("numRows::");
	//		numRow.AppendNumber(noOfRows);
	//		CA(numRow);
	//		PMString numCol("numcols::");
	//		numCol.AppendNumber(noOfColumns);
	//		CA(numCol);*/

	//		//Check for the tableHeaderFlag at colno
	//		int32 headerFlag = tagInfo.header;//int32 colnoAsHeaderFlag = tagInfo.colno;
	//		//update the attributes of the parent tag
	//		
	//		if(!isTranspose)
	//		{
	//			/*  
	//			In case of Transpose == kFalse..the table gets sprayed like 
	//				Header11 Header12  Header13 ...
	//				Data11	 Data12	   Data13
	//				Data21	 Data22	   Data23
	//				....
	//				....
	//				So first row is Header Row and subsequent rows are DataRows.
	//			*/
	//			//The for loop below will spray the Table Header in left to right fashion.
	//			//Note typeId == -2 for TableHeaders.
	//			int32 rowCount =0, colCount=0;
	//			
	//			//tableHeader = [-807, -808, -809, -810, -811,  11000001, 11000005, 11000012, 11000020, 11000033, 11000043]
	//			if(headerFlag == 1)//Spray table with the headers.
	//			{//f(colnoAsHeaderFlag != -555)
	//				int32 headerIndex = 5;			
	//				
	//				for(;headerIndex < noOfColumns;headerIndex++)
	//				{
	//					//CA("Insdie For looop");					

	//					int32 elementId = vectorOfTableHeader[headerIndex];
	//					 SlugStruct newTagToBeAdded;
	//					 newTagToBeAdded.elementId = elementId;
	//					 newTagToBeAdded.typeId = -1;//-2;
	//					 newTagToBeAdded.header = headerFlag;
	//					 newTagToBeAdded.parentId = pNode.getPubId();
	//					 newTagToBeAdded.imgFlag = 0;
	//					 newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//					 newTagToBeAdded.elementName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );
	//					 newTagToBeAdded.TagName = prepareTagName(newTagToBeAdded.elementName);
	//					 newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//					 newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//					 newTagToBeAdded.whichTab =4 ;
	//					 newTagToBeAdded.imgFlag = 0;
	//					 newTagToBeAdded.sectionID = sectionid ;
	//					 newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//					 newTagToBeAdded.tableFlag = -12;
	//					 newTagToBeAdded.row_no=-1;
	//					 newTagToBeAdded.col_no=-1;//colnoAsHeaderFlag;					 

	//					 //listOfNewTagsAdded.push_back(newTagToBeAdded);						
	//					//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
	//					
	//					PMString insertPMStringText;
	//					insertPMStringText.Append(newTagToBeAdded.elementName);
	//					if(!iConverter)
	//					{
	//						insertPMStringText=insertPMStringText;					
	//					}
	//					else
	//						insertPMStringText=iConverter->translateString(insertPMStringText);
	//					
	//					insertPMStringText.Append("\t");						
	//					
	//					tabbedTextData.Append(insertPMStringText);
	//					//CA("tabbedTextData 1: "+ tabbedTextData);

	//					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
	//								
	//					tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//					
	//					newTagToBeAdded.tagStartPos=tagStartPos;
	//					newTagToBeAdded.tagEndPos=tagEndPos;
	//					listOfNewTagsAdded.push_back(newTagToBeAdded);
	//					 // XMLReference createdElement;
	//					// Utils<IXMLElementCommands>()->CreateElement(modifiedTagName , txtModelUIDRef,tagStartPos,tagEndPos,kInvalidXMLReference, &createdElement);
	//					// addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
	//					tagStartPos = tagEndPos+ 1 + 2;			
	//				}	

	//				for(int32 p=0; p<5; p++ )
	//				{ 
	//					SlugStruct newTagToBeAdded;
	//					int32 elementId = -1;
	//					PMString ElementName = "";
	//					switch (p)
	//					{
	//						case 0:							
	//								elementId = -807;
	//								ElementName = "Cross-reference Type";
	//								break;							
	//						case 1:							
	//								elementId = -808;
	//								ElementName = "Rating";
	//								break;							
	//						case 2:							
	//								elementId = -809;
	//								ElementName = "Alternate";
	//								break;
	//						case 3:							
	//								elementId = -810;
	//								ElementName = "Comments";
	//								break;							
	//						case 4:							
	//								elementId = -811;
	//								ElementName = "";
	//								break;
	//						default:
	//								elementId = -1;
	//								ElementName = "";
	//								break;
	//					}
	//					
	//					newTagToBeAdded.elementId = elementId;
	//					newTagToBeAdded.typeId = -1;//-2;
	//					newTagToBeAdded.header = headerFlag;
	//					newTagToBeAdded.parentId = pNode.getPubId();
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//					newTagToBeAdded.elementName = ElementName;

	//					PMString TableColName("");  
	//					TableColName.Append("ATTRID_");
	//					TableColName.AppendNumber(elementId);

	//					//newTagToBeAdded.elementName = TableColName;

	//					newTagToBeAdded.TagName = prepareTagName(TableColName);
	//					newTagToBeAdded.TagName = keepOnlyAlphaNumeric(TableColName);
	//					newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//					newTagToBeAdded.whichTab =4 ;
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.sectionID = sectionid ;
	//					newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//					newTagToBeAdded.tableFlag = -12;
	//					newTagToBeAdded.row_no=-1;
	//					newTagToBeAdded.col_no=-1;//colnoAsHeaderFlag;					 

	//					PMString insertPMStringText;
	//					insertPMStringText.Append(newTagToBeAdded.elementName);
	//					if(!iConverter)
	//					{
	//						insertPMStringText=insertPMStringText;					
	//					}
	//					else
	//						insertPMStringText=iConverter->translateString(insertPMStringText);
	//					if(p < 4)
	//					{
	//						insertPMStringText.Append("\t");						
	//					}
	//					else if(p == 4 )
	//					{
	//						insertPMStringText.Append("\r");
	//					}

	//					tabbedTextData.Append(insertPMStringText);					

	//					//CA("tabbedTextData 2: "+ tabbedTextData);

	//					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
	//					tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//					
	//					newTagToBeAdded.tagStartPos=tagStartPos;
	//					newTagToBeAdded.tagEndPos=tagEndPos;
	//					listOfNewTagsAdded.push_back(newTagToBeAdded);
	//					tagStartPos = tagEndPos+ 1 + 2;	
	//				}				
	//				rowCount++;
	//			}
	//			//This for loop will spray the Item_copy_attributes value]

	//			//tableData =[ Manufacturer Reference, 5, false, , true, New Manufacturer, 289, Parquetry  Super Set, , 26.00, 18.95],	
	//			vector<vector<PMString> >::iterator it1;
	//				
	//			for(it1=vectorOfRowByRowTableData.begin(); it1!=vectorOfRowByRowTableData.end(); it1++/*int32 itemIndex = 0;itemIndex <noOfRows;itemIndex++*/)
	//			{					
	//				vector<PMString>::iterator it2;
	//				int32 itemId =-1;
	//				if(headerFlag == 1)//if(colnoAsHeaderFlag != -555)
	//					itemId = vec_items[rowCount-1];
	//				else
	//					itemId = vec_items[rowCount];

	//				//CA(" Spraying attributes first");
	//				colCount=5;
	//				for(it2=(*it1).begin();it2!=(*it1).end();it2++/*int32 headerIndex = 0;headerIndex <noOfColumns;headerIndex++*/)
	//				{
	//					if(it2==(*it1).begin())
	//					{
	//						it2++; // This is to avoid Hardcoded data spray which is up to 5th position of it2
	//						it2++;
	//						it2++;
	//						it2++;
	//						it2++;					
	//					}

	//					int32 elementId = vectorOfTableHeader[colCount];
	//					
	//					PMString tagName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );

	//					SlugStruct newTagToBeAdded;
	//					newTagToBeAdded.elementId = elementId;
	//					newTagToBeAdded.typeId = -1;//itemId;
	//					newTagToBeAdded.childId = itemId;
	//					newTagToBeAdded.childTag = 1;
	//					newTagToBeAdded.header = headerFlag;
	//					newTagToBeAdded.parentId = pNode.getPubId();
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//					PMString TableColName("");  
	//					TableColName.Append("ATTRID_");
	//					TableColName.AppendNumber(elementId);

	//					newTagToBeAdded.elementName = TableColName;
	//					newTagToBeAdded.TagName = prepareTagName(TableColName);
	//					newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//					newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//					newTagToBeAdded.whichTab =4 ;
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.sectionID = sectionid;
	//					newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//					newTagToBeAdded.tableFlag = -12;
	//					newTagToBeAdded.row_no=-1;
	//					newTagToBeAdded.col_no=-1;//colnoAsHeaderFlag;

	//					//start
	//					//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
	//					PMString insertPMStringText;
	//					insertPMStringText.Append((*it2));
	//					if(!iConverter)
	//					{
	//						insertPMStringText=insertPMStringText;					
	//					}
	//					else
	//						insertPMStringText=iConverter->translateString(insertPMStringText);

	//					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
	//					/*if(colCount != noOfColumns - 1)
	//					{*/
	//						insertPMStringText.Append("\t");							
	//					/*}*/
	//					/*else if(rowCount != noOfRows-1)
	//					{
	//						insertPMStringText.Append("\r");
	//					}*/
	//					//CA("insertPMStringText : " + insertPMStringText);

	//					tabbedTextData.Append(insertPMStringText);
	//					//WideString insertText(insertPMStringText);						
	//					//textModel->Insert(kTrue,tagStartPos,&insertText);
	//									
	//					tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
	//					newTagToBeAdded.tagStartPos=tagStartPos;
	//					newTagToBeAdded.tagEndPos=tagEndPos;
	//					listOfNewTagsAdded.push_back(newTagToBeAdded);
	//					//XMLReference createdElement;
	//					//Utils<IXMLElementCommands>()->CreateElement(modifiedTagName , txtModelUIDRef,tagStartPos,tagEndPos,kInvalidXMLReference, &createdElement);
	//					// addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
	//					tagStartPos = tagEndPos+ 1 + 2;
	//					//end
	//					colCount ++;						
	//						
	//				}
	//				
	//				//CA(" Spraying Hardcoded part now");
	//				colCount =0;
	//				for(it2=(*it1).begin();it2!=(*it1).end();it2++)
	//				{
	//					if(colCount >=5)
	//						break;
	//	
	//					int32 elementId = vectorOfTableHeader[colCount];						
	//					
	//					SlugStruct newTagToBeAdded;
	//					newTagToBeAdded.elementId = elementId;
	//					newTagToBeAdded.typeId = -1;//itemId;
	//					newTagToBeAdded.header = headerFlag;
	//					newTagToBeAdded.childId = itemId;
	//					newTagToBeAdded.childTag =1;
	//					newTagToBeAdded.parentId = pNode.getPubId();
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.parentTypeId = pNode.getTypeId();

	//					PMString TableColName("");  
	//					TableColName.Append("ATTRID_");
	//					TableColName.AppendNumber(elementId);

	//					newTagToBeAdded.elementName = TableColName;
	//					newTagToBeAdded.TagName = prepareTagName(TableColName);
	//					newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//					newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//					newTagToBeAdded.whichTab =4 ;
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.sectionID = sectionid;
	//					newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//					newTagToBeAdded.tableFlag = -12;
	//					newTagToBeAdded.row_no=-1;
	//					newTagToBeAdded.col_no=-1;//colnoAsHeaderFlag;

	//					//start
	//					//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
	//					PMString insertPMStringText;
	//					insertPMStringText.Append((*it2));
	//					if(!iConverter)
	//					{
	//						insertPMStringText=insertPMStringText;					
	//					}
	//					else
	//						insertPMStringText=iConverter->translateString(insertPMStringText);

	//					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
	//					if(colCount < 4)
	//					{
	//						//CA(" colCount < 4 ");
	//						insertPMStringText.Append("\t");							
	//					}
	//					else if(colCount == 4)
	//					{
	//						//CA(" colCount == 4 ");
	//						insertPMStringText.Append("\r");
	//					}

	//					tabbedTextData.Append(insertPMStringText);
	//					//WideString insertText(insertPMStringText);						
	//					//textModel->Insert(kTrue,tagStartPos,&insertText);
	//									
	//					tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
	//					newTagToBeAdded.tagStartPos=tagStartPos;
	//					newTagToBeAdded.tagEndPos=tagEndPos;
	//					listOfNewTagsAdded.push_back(newTagToBeAdded);
	//					//XMLReference createdElement;
	//					//Utils<IXMLElementCommands>()->CreateElement(modifiedTagName , txtModelUIDRef,tagStartPos,tagEndPos,kInvalidXMLReference, &createdElement);
	//					// addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
	//					tagStartPos = tagEndPos+ 1 + 2;
	//					//end
	//					colCount ++;
	//			
	//					//listOfNewTagsAdded.push_back(newTagToBeAdded);		
	//				}					

	//				colCount =0;
	//				rowCount++;	
	//			}				
	//		}//end of if IsTranspose == kFalse
	//		else
	//		{//else of if IsTranspose ==kFalse..i.e IsTranspose == kTrue. Headers are in first column
	//			/*  
	//			In case of Transpose == kTrue..the table gets sprayed like 

	//				Header11 Data11 Data12 Data13..
	//				Header22 Data21 Data22 Data23..
	//				Header33 Data31 Data32 Data33..
	//				....
	//				....
	//			So First Column is HeaderColumn and subsequent columns are Data columns.
	//			*/
	//			
	//			//for(int32 headerIndex = 0;headerIndex <noOfColumns;headerIndex++)
	//			//{
	//			//	int32 elementId = vectorOfTableHeader[headerIndex];
	//			//	//noOfRows in the for loop below is noOfRows + 1.
	//			//	//As we have to add first column which is of table headers.
	//			//	//In the earlier part(i.e in the if block of IsTranspose == kFalse)
	//			//	//we have added the header row in a separate loop.
	//			//	for(int32 itemIndex = 0;itemIndex <noOfRows + 1;itemIndex++)
	//			//	{
	//			//		int32 itemId =-1;
	//			//		PMString tagName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );
	//			//		PMString elementName;
	//			//		SlugStruct newTagToBeAdded;
	//			//		if( itemIndex == 0 && 
	//			//			colnoAsHeaderFlag != -555//Spray the table with the headers
	//			//		   )
	//			//			{
	//			//				//itemId == -2 for header.
	//			//				itemId = -2;
	//			//				elementName = tagName;
	//			//			}
	//			//		else
	//			//			{
	//			//				itemId = vec_items[itemIndex-1];
	//			//				elementName = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(itemId,elementId,tagInfo.languageID,kTrue);
	//			//			}			
	//			//		 newTagToBeAdded.elementId = elementId;
	//			//		 newTagToBeAdded.typeId = itemId;
	//			//		 newTagToBeAdded.parentId = pNode.getPubId();
	//			//		 newTagToBeAdded.imgflag = 0;
	//			//		 newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//			//		 newTagToBeAdded.elementName = elementName;
	//			//		 newTagToBeAdded.TagName = prepareTagName(tagName);
	//			//		 newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//			//		 newTagToBeAdded.whichTab =3 ;
	//			//		 newTagToBeAdded.imgFlag = 0;
	//			//		 newTagToBeAdded.sectionID = sectionid;
	//			//		 newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//			//		 newTagToBeAdded.tableFlag = -12;
	//			//		 newTagToBeAdded.row_no=-1;
	//			//		 newTagToBeAdded.col_no=colnoAsHeaderFlag;
	//			//		
	//			//		PMString insertPMStringText;
	//			//		insertPMStringText.Append(newTagToBeAdded.elementName);
	//			//		int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
	//			//		if(itemIndex != noOfRows)
	//			//		{
	//			//			insertPMStringText.Append("\t");
	//			//			
	//			//		}
	//			//		else
	//			//		{
	//			//			insertPMStringText.Append("\r");
	//			//		}
	//			//		tabbedTextData.Append(insertPMStringText);
	//			//					
	//			//		 tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
	//			//		 newTagToBeAdded.tagStartPos=tagStartPos;
	//			//		 newTagToBeAdded.tagEndPos=tagEndPos;

	//			//		 listOfNewTagsAdded.push_back(newTagToBeAdded);
	//			//		 
	//			//		 tagStartPos = tagEndPos+ 1 + 2;
	//			//		 
	//		
	//			//	}

	//			//	

	//			//}

	//		}
	//			//CA(tabbedTextData);
	//			PMString textToInsert("");
	//			//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	//			if(!iConverter)
	//			{
	//				textToInsert=tabbedTextData;					
	//			}
	//			else
	//				textToInsert=iConverter->translateString(tabbedTextData);
	//			
	//			//WideString insertText(textToInsert);	
	//			//textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
	//			/*K2*/boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
	//			ReplaceText(textModel,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);
	//		
	//		/*	PMString ASD1("listOfNewTagsAdded.size() : ");
	//			ASD1.AppendNumber(listOfNewTagsAdded.size());
	//			CA(ASD1);*/

	//			for(int32 indexOfTagToBeAdded = 0;indexOfTagToBeAdded <listOfNewTagsAdded.size();indexOfTagToBeAdded++)
	//			{
	//				SlugStruct & newTagToBeAdded = listOfNewTagsAdded[indexOfTagToBeAdded];
	//				//CA(newTagToBeAdded.TagName);
	//			
	//				XMLReference createdElement;
	//				Utils<IXMLElementCommands>()->CreateElement(WideString(newTagToBeAdded.TagName), txtModelUIDRef,newTagToBeAdded.tagStartPos,newTagToBeAdded.tagEndPos,kInvalidXMLReference, &createdElement);
	//				addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);

	//			}
	//	
	//		}//end if1
	//}while(kFalse);


}//end of function

void CDataSprayer::sprayAccessoryTableScreenPrintInTabbedTextForm(TagStruct & tagInfo)
{
	//CA("CDataSprayer::sprayAccessoryTableScreenPrintInTabbedTextForm");
	//int32 CopyFlag=0;

	//PMString elementName,colName;

	//vector<int32> FinalItemIds;
	//bool16 firstTabbedTextTag = kTrue;
	//tabbedTagStructList.clear();

	//UIDRef txtModelUIDRef = UIDRef::gNull;
	//InterfacePtr<ITextModel> textModel;
	//
	//TextIndex startPos=-1;
	//TextIndex endPos = -1;
	////First collect all the tabbedTextTag in a vector tabbedTagStructList.
	//		
	//	//check for the tabbedTextTag
	//int32 replaceItemWithTabbedTextFrom = -1;
	//int32 replaceItemWithTabbedTextTo = -1;
	//PMString tabbedTextData;
	//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//if(ptrIAppFramework == NULL)
	//{
	//	CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
	//	return;
	//}

	//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));

	//do
	//{
	//	if(tagInfo.whichTab == 4 && tagInfo.parentId == -1 && tagInfo.isTablePresent == kTrue)
	//	{//start if1
	//		//CA("Inside Do while");
	//		/*InterfacePtr<IPMUnknown> unknown(tagInfo.curBoxUIDRef, IID_IUNKNOWN);
	//		if(unknown == NULL)
	//		{
	//			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayXRefTableScreenPrintInTabbedTextForm::!unknown");
	//			break;
	//		}
	//		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
	//		UID textFrameUID = kInvalidUID;
	//		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(tagInfo.curBoxUIDRef, UseDefaultIID());
	//		if (graphicFrameDataOne) 
	//		{
	//			textFrameUID = graphicFrameDataOne->GetTextContentUID();
	//		}
	//		if (textFrameUID == kInvalidUID)
	//		{
	//			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayXRefTableScreenPrintInTabbedTextForm::!textFrameUID");
	//			break;
	//		}
	//			
	//		/*InterfacePtr<ITextFrame> textFrame(tagInfo.curBoxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
	//		if (textFrame == NULL)
	//		{
	//			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayXRefTableScreenPrintInTabbedTextForm::!textFrame");
	//			break;
	//		}*/
	//		InterfacePtr<IHierarchy> graphicFrameHierarchy(tagInfo.curBoxUIDRef, UseDefaultIID());
	//		if (graphicFrameHierarchy == nil) 
	//		{
	//			//CA("graphicFrameHierarchy is NULL");
	//			return;
	//		}

	//		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
	//		if (!multiColumnItemHierarchy) {
	//		//CA("multiColumnItemHierarchy is NULL");
	//		}
	//		InterfacePtr<IMultiColumnTextFrame>
	//		frameItemTFC(multiColumnItemHierarchy, UseDefaultIID());
	//		if (!frameItemTFC) {
	//		//CA("!!!IMultiColumnTextFrame");
	//		return;
	//		}
	//		InterfacePtr<ITextModel> temptextModel(frameItemTFC->QueryTextModel());
	//		if (temptextModel == NULL)
	//		{
	//			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayXRefTableScreenPrintInTabbedTextForm::!temptextModel");
	//			break;
	//		}
	//		textModel = temptextModel;

	//		txtModelUIDRef = ::GetUIDRef(textModel);
	//		
	//		int32 sectionid = -1;
	//		if(global_project_level == 3)
	//			sectionid = CurrentSubSectionID;
	//		if(global_project_level == 2)
	//			sectionid = CurrentSectionID;
	//		
	//		PMString attrVal;
	//		attrVal.AppendNumber(pNode.getPubId());	
	//		tagInfo.tagPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
	//		attrVal.Clear();
	//		
	//		attrVal.AppendNumber(CurrentObjectTypeID);
	//		tagInfo.tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attrVal));
	//		attrVal.Clear();
	//		
	//		attrVal.AppendNumber(sectionid);
	//		tagInfo.tagPtr->SetAttributeValue(WideString("sectionID"),WideString(attrVal));
	//		attrVal.Clear();
	//		//tableFlag =-11 to identify that this is the "item" table tag in the 
	//		//tabbed text format.
	//		tagInfo.tagPtr->SetAttributeValue(WideString("tableFlag"),WideString("-11"));
	//		//change the ID from -1 to -101, as -1 is elementId for productNumber.
	//		tagInfo.tagPtr->SetAttributeValue(WideString("ID"),WideString("-101"));

	//		tagInfo.tagPtr->SetAttributeValue(WideString("childTag"),WideString("1"));

	//		//end update the attributes of the parent tag
	//		//IIDXMLElement * itemTagPtr = tagInfo.tagPtr;
	//		int32 tagStartPos = 0;
	//		int32 tagEndPos =0; 			
	//		Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&tagStartPos,&tagEndPos);
	//		tagStartPos = tagStartPos + 1;
	//		tagEndPos = tagEndPos -1;

	//		tabbedTextData.Clear();
	//		replaceItemWithTabbedTextFrom = tagStartPos;
	//		replaceItemWithTabbedTextTo	= tagEndPos;
	//					
	//		bool16 isTableDataPresent = kTrue;
	//		
	//		int32 itemId = pNode.getPubId(); //vec_item_new[0]/*30017240*/;
	//		
	//		/*PMString ASD("itemid: ");
	//		ASD.AppendNumber(itemId);
	//		CA(ASD);*/

	//		VectorScreenTableInfoPtr AccessorytableInfo = NULL;

	//		AccessorytableInfo= ptrIAppFramework->GETItem_getAccessoryScreenTableByItemIdLanguageId(itemId, tagInfo.languageID); 
	//		if(!AccessorytableInfo)
	//		{
	//			ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::sprayXRefTableScreenPrintInTabbedTextForm::!AccessorytableInfo");
	//			return;
	//		}

	//		if(AccessorytableInfo->size()==0)
	//		{
	//			//CA(" XreftableInfo->size()==0");
	//			isTableDataPresent = kFalse;

	//			if(isTableDataPresent == kFalse)
	//			{
	//				PMString textToInsert("");					
	//				if(!iConverter)
	//				{
	//					textToInsert=tabbedTextData;					
	//				}
	//				else
	//					textToInsert=iConverter->translateString(tabbedTextData);				
	//				//WideString insertText(textToInsert);	
	//				//textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
	//				/*K2*/boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
	//				ReplaceText(textModel,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);

	//				break;
	//			}
	//			break;
	//		}
	//			
	//		CItemTableValue oTableValue;
	//		VectorScreenTableInfoValue::iterator it;
	//		it = AccessorytableInfo->begin();
	//		oTableValue = *it;
	//		isTableDataPresent = kTrue;
	//		
	//		vector<int32> vectorOfTableHeader = oTableValue.getTableHeader();
	//		vector<vector<PMString> >  vectorOfRowByRowTableData = oTableValue.getTableData();
	//		vector<int32> vec_items = oTableValue.getItemIds();
	//		bool8 isTranspose = kFalse;
	//		vector<vector<SlugStruct> > RowList;
	//		vector<SlugStruct> listOfNewTagsAdded;

	//		int32 noOfColumns =static_cast<int32>(vectorOfTableHeader.size());
	//		int32 noOfRows = static_cast<int32>(vec_items.size());			

	//	/*	PMString numRow("numRows::");
	//		numRow.AppendNumber(noOfRows);
	//		CA(numRow);
	//		PMString numCol("numcols::");
	//		numCol.AppendNumber(noOfColumns);
	//		CA(numCol);*/

	//		//Check for the tableHeaderFlag at colno
	//		int32 headerFlag = tagInfo.header;//int32 colnoAsHeaderFlag = tagInfo.colno;
	//		//update the attributes of the parent tag

	//		if(!isTranspose)
	//		{
	//			/*  
	//			In case of Transpose == kFalse..the table gets sprayed like 
	//				Header11 Header12  Header13 ...
	//				Data11	 Data12	   Data13
	//				Data21	 Data22	   Data23
	//				....
	//				....	
	//				So first row is Header Row and subsequent rows are DataRows.
	//			*/
	//			//The for loop below will spray the Table Header in left to right fashion.
	//			//Note typeId == -2 for TableHeaders.
	//			int32 rowCount =0, colCount=0;
	//			
	//			//tableHeader = [-812, -813, -814,  11000001, 11000005, 11000012, 11000020, 11000033, 11000043]
	//			if(headerFlag == 1)//Spray table with the headers.
	//			{//if(colnoAsHeaderFlag != -555)
	//				for(int32 p=0; p<3; p++ )
	//				{ 
	//					SlugStruct newTagToBeAdded;
	//					int32 elementId = -1;
	//					PMString ElementName = "";
	//					switch (p)
	//					{
	//						case 0:							
	//								elementId = -812;
	//								ElementName = "Quantity";
	//								break;							
	//						case 1:							
	//								elementId = -813;
	//								ElementName = "Required";
	//								break;							
	//						case 2:							
	//								elementId = -814;
	//								ElementName = "Comments";
	//								break;
	//						default:
	//								elementId = -1;
	//								ElementName = "";
	//								break;
	//					}
	//					
	//					newTagToBeAdded.elementId = elementId;
	//					newTagToBeAdded.typeId = -1;//-2;
	//					newTagToBeAdded.header = headerFlag;
	//					newTagToBeAdded.parentId = pNode.getPubId();
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//					newTagToBeAdded.elementName = ElementName;

	//					PMString TableColName("");  
	//					TableColName.Append("ATTRID_");
	//					TableColName.AppendNumber(elementId);

	//					//newTagToBeAdded.elementName = TableColName;

	//					newTagToBeAdded.TagName = prepareTagName(TableColName);
	//					newTagToBeAdded.TagName = keepOnlyAlphaNumeric(TableColName);
	//					newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//					newTagToBeAdded.whichTab =4 ;
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.sectionID = sectionid ;
	//					newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//					newTagToBeAdded.tableFlag = -12;
	//					newTagToBeAdded.row_no=-1;
	//					newTagToBeAdded.col_no=-1;//colnoAsHeaderFlag;					 

	//					PMString insertPMStringText;
	//					insertPMStringText.Append(newTagToBeAdded.elementName);
	//					if(!iConverter)
	//					{
	//						insertPMStringText=insertPMStringText;					
	//					}
	//					else
	//						insertPMStringText=iConverter->translateString(insertPMStringText);
	//					
	//					insertPMStringText.Append("\t");						
	//					
	//					
	//					tabbedTextData.Append(insertPMStringText);					

	//					//CA("tabbedTextData 2: "+ tabbedTextData);

	//					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
	//					tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//					
	//					newTagToBeAdded.tagStartPos=tagStartPos;
	//					newTagToBeAdded.tagEndPos=tagEndPos;
	//					listOfNewTagsAdded.push_back(newTagToBeAdded);
	//					tagStartPos = tagEndPos+ 1 + 2;	
	//				}

	//				int32 headerIndex = 3;

	//				for(;headerIndex < noOfColumns;headerIndex++)
	//				{
	//					//CA("Insdie For looop");					

	//					int32 elementId = vectorOfTableHeader[headerIndex];
	//					 SlugStruct newTagToBeAdded;
	//					 newTagToBeAdded.elementId = elementId;
	//					 newTagToBeAdded.typeId = -1;//-2;
	//					 newTagToBeAdded.header = headerFlag;
	//					 newTagToBeAdded.parentId = pNode.getPubId();
	//					 newTagToBeAdded.imgFlag = 0;
	//					 newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//					 newTagToBeAdded.elementName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );
	//					 newTagToBeAdded.TagName = prepareTagName(newTagToBeAdded.elementName);
	//					 newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//					 newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//					 newTagToBeAdded.whichTab =4 ;
	//					 newTagToBeAdded.imgFlag = 0;
	//					 newTagToBeAdded.sectionID = sectionid ;
	//					 newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//					 newTagToBeAdded.tableFlag = -12;
	//					 newTagToBeAdded.row_no=-1;
	//					 newTagToBeAdded.col_no=-1;//colnoAsHeaderFlag;					 

	//					 //listOfNewTagsAdded.push_back(newTagToBeAdded);						
	//					//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
	//					
	//					PMString insertPMStringText;
	//					insertPMStringText.Append(newTagToBeAdded.elementName);
	//					if(!iConverter)
	//					{
	//						insertPMStringText=insertPMStringText;					
	//					}
	//					else
	//						insertPMStringText=iConverter->translateString(insertPMStringText);
	//					
	//					if(headerIndex < noOfColumns -1)
	//						insertPMStringText.Append("\t");
	//					else
	//						insertPMStringText.Append("\r");
	//					
	//					tabbedTextData.Append(insertPMStringText);
	//					//CA("tabbedTextData 1: "+ tabbedTextData);

	//					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
	//								
	//					tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//					
	//					newTagToBeAdded.tagStartPos=tagStartPos;
	//					newTagToBeAdded.tagEndPos=tagEndPos;
	//					listOfNewTagsAdded.push_back(newTagToBeAdded);
	//					 // XMLReference createdElement;
	//					// Utils<IXMLElementCommands>()->CreateElement(modifiedTagName , txtModelUIDRef,tagStartPos,tagEndPos,kInvalidXMLReference, &createdElement);
	//					// addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
	//					tagStartPos = tagEndPos+ 1 + 2;			
	//				}	
	//								
	//				rowCount++;
	//			}
	//			//This for loop will spray the Item_copy_attributes value]

	//			//tableData =[1, false, , CGO60610P, Wall Poster, 24" x 36", 37.88, 31.99],	
	//			vector<vector<PMString> >::iterator it1;
	//				
	//			for(it1=vectorOfRowByRowTableData.begin(); it1!=vectorOfRowByRowTableData.end(); it1++/*int32 itemIndex = 0;itemIndex <noOfRows;itemIndex++*/)
	//			{					
	//				vector<PMString>::iterator it2;
	//				int32 itemId =-1;
	//				if(headerFlag == 1)//if(colnoAsHeaderFlag != -555)
	//					itemId = vec_items[rowCount-1];
	//				else
	//					itemId = vec_items[rowCount];


	//				//CA(" Spraying Hardcoded part first");
	//				colCount =0;
	//				for(it2=(*it1).begin();it2!=(*it1).end();it2++)
	//				{
	//					if(colCount == 3)
	//						break;
	//					int32 elementId = vectorOfTableHeader[colCount];						
	//					
	//					SlugStruct newTagToBeAdded;
	//					newTagToBeAdded.elementId = elementId;
	//					newTagToBeAdded.typeId = -1;//itemId;
	//					newTagToBeAdded.header = headerFlag;
	//					newTagToBeAdded.childId = itemId;
	//					newTagToBeAdded.childTag =1;
	//					newTagToBeAdded.parentId = pNode.getPubId();
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.parentTypeId = pNode.getTypeId();

	//					PMString TableColName("");  
	//					TableColName.Append("ATTRID_");
	//					TableColName.AppendNumber(elementId);

	//					newTagToBeAdded.elementName = TableColName;
	//					newTagToBeAdded.TagName = prepareTagName(TableColName);
	//					newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//					newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//					newTagToBeAdded.whichTab =4 ;
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.sectionID = sectionid;
	//					newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//					newTagToBeAdded.tableFlag = -12;
	//					newTagToBeAdded.row_no=-1;
	//					newTagToBeAdded.col_no=-1;//colnoAsHeaderFlag;

	//					//start
	//					//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
	//					PMString insertPMStringText;
	//					insertPMStringText.Append((*it2));
	//					if(!iConverter)
	//					{
	//						insertPMStringText=insertPMStringText;					
	//					}
	//					else
	//						insertPMStringText=iConverter->translateString(insertPMStringText);

	//					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
	//					
	//					insertPMStringText.Append("\t");							
	//					
	//					tabbedTextData.Append(insertPMStringText);
	//					//WideString insertText(insertPMStringText);						
	//					//textModel->Insert(kTrue,tagStartPos,&insertText);
	//									
	//					tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
	//					newTagToBeAdded.tagStartPos=tagStartPos;
	//					newTagToBeAdded.tagEndPos=tagEndPos;
	//					listOfNewTagsAdded.push_back(newTagToBeAdded);
	//					//XMLReference createdElement;
	//					//Utils<IXMLElementCommands>()->CreateElement(modifiedTagName , txtModelUIDRef,tagStartPos,tagEndPos,kInvalidXMLReference, &createdElement);
	//					// addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
	//					tagStartPos = tagEndPos+ 1 + 2;
	//					//end
	//					colCount ++;
	//			
	//					//listOfNewTagsAdded.push_back(newTagToBeAdded);		
	//				}	

	//				//CA(" Spraying attributes Now");
	//				colCount=3;
	//				for(it2=(*it1).begin();it2!=(*it1).end();it2++/*int32 headerIndex = 0;headerIndex <noOfColumns;headerIndex++*/)
	//				{

	//					if(it2==(*it1).begin())
	//					{
	//						it2++; // This is to avoid Hardcoded data spray which is up to 5th position of it2
	//						it2++;
	//						it2++;
	//					}

	//					int32 elementId = vectorOfTableHeader[colCount];
	//					
	//					PMString tagName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(elementId,tagInfo.languageID );

	//					SlugStruct newTagToBeAdded;
	//					newTagToBeAdded.elementId = elementId;
	//					newTagToBeAdded.typeId = -1;//itemId;
	//					newTagToBeAdded.header = headerFlag;
	//					newTagToBeAdded.childId = itemId;
	//					newTagToBeAdded.childTag =1;
	//					newTagToBeAdded.parentId = pNode.getPubId();
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//					PMString TableColName("");  
	//					TableColName.Append("ATTRID_");
	//					TableColName.AppendNumber(elementId);

	//					newTagToBeAdded.elementName = TableColName;
	//					newTagToBeAdded.TagName = prepareTagName(TableColName);
	//					newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//					newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//					newTagToBeAdded.whichTab =4 ;
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.sectionID = sectionid;
	//					newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//					newTagToBeAdded.tableFlag = -12;
	//					newTagToBeAdded.row_no=-1;
	//					newTagToBeAdded.col_no=-1;//colnoAsHeaderFlag;

	//					//start
	//					//PMString modifiedTagName = prepareTagName(newTagToBeAdded.TagName);
	//					PMString insertPMStringText;
	//					insertPMStringText.Append((*it2));
	//					if(!iConverter)
	//					{
	//						insertPMStringText=insertPMStringText;					
	//					}
	//					else
	//						insertPMStringText=iConverter->translateString(insertPMStringText);

	//					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
	//					/*if(colCount != noOfColumns - 1)
	//					{*/
	//					if(colCount < noOfColumns-1)
	//						insertPMStringText.Append("\t");
	//					else
	//						insertPMStringText.Append("\r");
	//					/*}*/
	//					/*else if(rowCount != noOfRows-1)
	//					{
	//						insertPMStringText.Append("\r");
	//					}*/
	//					//CA("insertPMStringText : " + insertPMStringText);

	//					tabbedTextData.Append(insertPMStringText);
	//					//WideString insertText(insertPMStringText);						
	//					//textModel->Insert(kTrue,tagStartPos,&insertText);
	//									
	//					tagEndPos = tagStartPos + lengthOfTheString;//don't add tab or newline character between the tag.
	//					newTagToBeAdded.tagStartPos=tagStartPos;
	//					newTagToBeAdded.tagEndPos=tagEndPos;
	//					listOfNewTagsAdded.push_back(newTagToBeAdded);
	//					//XMLReference createdElement;
	//					//Utils<IXMLElementCommands>()->CreateElement(modifiedTagName , txtModelUIDRef,tagStartPos,tagEndPos,kInvalidXMLReference, &createdElement);
	//					// addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
	//					tagStartPos = tagEndPos+ 1 + 2;
	//					//end
	//					colCount ++;						
	//						
	//				}
	//						

	//				colCount =0;
	//				rowCount++;	
	//			}				
	//		}//en

	//		
	//		//CA(tabbedTextData);	
	//		PMString textToInsert("");
	//		//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	//		if(!iConverter)
	//		{
	//			textToInsert=tabbedTextData;					
	//		}
	//		else
	//			textToInsert=iConverter->translateString(tabbedTextData);
	//		
	//		//WideString insertText(textToInsert);	
	//		//textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
	//		/*K2*/boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
	//		ReplaceText(textModel,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);
	//		
	//		/*	PMString ASD1("listOfNewTagsAdded.size() : ");
	//			ASD1.AppendNumber(listOfNewTagsAdded.size());
	//			CA(ASD1);*/

	//		for(int32 indexOfTagToBeAdded = 0;indexOfTagToBeAdded <listOfNewTagsAdded.size();indexOfTagToBeAdded++)
	//		{
	//			SlugStruct & newTagToBeAdded = listOfNewTagsAdded[indexOfTagToBeAdded];
	//			//CA(newTagToBeAdded.TagName);
	//		
	//			XMLReference createdElement;
	//			Utils<IXMLElementCommands>()->CreateElement(WideString(newTagToBeAdded.TagName), txtModelUIDRef,newTagToBeAdded.tagStartPos,newTagToBeAdded.tagEndPos,kInvalidXMLReference, &createdElement);
	//			addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);

	//		}

	//	}

	//}while( kFalse );
}



bool16 CDataSprayer ::isHorizontalFlow()
{
	if(HorizontalFlow==kTrue)
	{
		//CA("HorizontalFlow==kTrue")	;
		return kTrue;
	}
	else
	{
		//CA("HorizontalFlow==kFalse")	;
		return kFalse;
	}

}


void CDataSprayer ::setFlow(bool16 flow)
{
	//CA("HorizontalFlow = flow 1234");
	HorizontalFlow = flow;
}

void CDataSprayer::ClearNewImageFrameList()
{
	NewImageFrameUIDList.clear();
}

VectorNewImageFrameUIDList CDataSprayer::getNewImageFrameList()
{
	return NewImageFrameUIDList;
}


//////////	Added on 22 Aug.

void CDataSprayer::sprayHybridTableScreenPrintInTabbedTextForm(TagStruct & tagInfo)
{
	//int32 CopyFlag = 0;
	//	
	//PMString elementName,colName;

	//bool16 firstTabbedTextTag = kTrue;
	//tabbedTagStructList.clear();

	//UIDRef txtModelUIDRef = UIDRef::gNull;
	//InterfacePtr<ITextModel> textModel;
	//
	//TextIndex startPos=-1;
	//TextIndex endPos = -1;
	//
	//int32 replaceItemWithTabbedTextFrom = -1;
	//int32 replaceItemWithTabbedTextTo = -1;
	//PMString tabbedTextData;
	//
	//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//if(ptrIAppFramework == NULL)
	//{
	//	CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
	//	return;
	//}
	//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	//do{
	//	InterfacePtr<IPMUnknown> unknown(tagInfo.curBoxUIDRef, IID_IUNKNOWN);
	//	if(unknown == NULL)
	//	{
	//		ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayHybridTableScreenPrintInTabbedTextForm::!unknown");	
	//		break;
	//	}
	//	UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
	//	if (textFrameUID == kInvalidUID)
	//	{
	//		ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayHybridTableScreenPrintInTabbedTextForm::!textFrameUID");	
	//		break;
	//	}
	//	
	//	/*InterfacePtr<ITextFrame> textFrame(tagInfo.curBoxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
	//	if (textFrame == NULL)
	//	{
	//		ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayHybridTableScreenPrintInTabbedTextForm::!textFrame");	
	//		break;
	//	}


	//	
	//	InterfacePtr<ITextModel> temptextModel(textFrame->QueryTextModel());
	//	if (temptextModel == NULL)
	//	{
	//		ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayHybridTableScreenPrintInTabbedTextForm::!temptextModel");	
	//		break;
	//	}*/

	//	InterfacePtr<IHierarchy> graphicFrameHierarchy(tagInfo.curBoxUIDRef, UseDefaultIID());
	//	if (graphicFrameHierarchy == nil) 
	//	{
	//		//CA("graphicFrameHierarchy is NULL");
	//		break;
	//	}
	//		
	//	int32 chldCount = graphicFrameHierarchy->GetChildCount();
	//	PMString Count("ChildCount::");
	//	Count.AppendNumber(chldCount);
	//	//CA(Count);
	//	/*if(chldCount==0)
	//		IHierarchy* multiColumnItemHierarchy = graphicFrameHierarchy->QueryParent();*/
	//	InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
	//	if (!multiColumnItemHierarchy) {
	//		//CA("multiColumnItemHierarchy is NULL");
	//	}
	//	
	//	InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
	//	if (!multiColumnItemTextFrame) {
	//		//CA("Its Not MultiColumn");
	//		break;
	//	}	

	//	InterfacePtr<ITextModel> temptextModel(multiColumnItemTextFrame->QueryTextModel());
	//	if(!temptextModel)
	//	{
	//	//	CA("!txtModel" );
	//			break;
	//	}

	//	textModel = temptextModel;

	//	txtModelUIDRef = ::GetUIDRef(textModel);
	//		

	//	int32 sectionid = -1;
	//	if(global_project_level == 3)
	//		sectionid = CurrentSubSectionID;
	//	if(global_project_level == 2)
	//		sectionid = CurrentSectionID;
	//	
	//	
	//	PMString attrVal;
	//	attrVal.AppendNumber(pNode.getPubId());	
	//	tagInfo.tagPtr->SetAttributeValue(WideString("parentID"),WideString(attrVal));
	//	attrVal.Clear();
	//	//
	//	attrVal.AppendNumber(CurrentObjectTypeID);
	//	tagInfo.tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attrVal));
	//	attrVal.Clear();
	//	//
	//	attrVal.AppendNumber(sectionid);
	//	tagInfo.tagPtr->SetAttributeValue(WideString("sectionID"),WideString(attrVal));
	//	attrVal.Clear();

	//	attrVal.AppendNumber(pNode.getPBObjectID());
	//	tagInfo.tagPtr->SetAttributeValue(WideString("rowno"),WideString(attrVal));
	//	attrVal.Clear();
	//	
	//	tagInfo.tagPtr->SetAttributeValue(WideString("tableFlag"),WideString("0"));
	//	//change the ID from -1 to -101, as -1 is elementId for productNumber.
	////	tagInfo.tagPtr->SetAttributeValue("ID","-115");
	//	
	//	//end update the attributes of the parent tag
	//	//IIDXMLElement * itemTagPtr = tagInfo.tagPtr;
	//	int32 tagStartPos = 0;
	//	int32 tagEndPos =0; 			
	//	Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&tagStartPos,&tagEndPos);
	//	tagStartPos = tagStartPos + 1;
	//	tagEndPos = tagEndPos -1;

	//	tabbedTextData.Clear();
	//	replaceItemWithTabbedTextFrom = tagStartPos;
	//	replaceItemWithTabbedTextTo	= tagEndPos;

	//	
	//	bool16 isTableDataPresent = kTrue;
	//	bool16 typeidFound = kFalse;	

	//	CAdvanceTableScreenValue oTableValue;
	//	VectorAdvanceTableScreenValue::iterator it;
	//	do
	//	{
	//		VectorAdvanceTableScreenValuePtr tableInfo = NULL;
	//		bool16 callFromTS = kFalse;
	//		//TableSourceInfoValue *TableSourceInfoValueObjPtr = NULL;
	//		//InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
	//		//if(ptrTableSourceHelper != nil)
	//		//{
	//		//	//CA("ptrTableSourceHelper != nil");
	//		//	TableSourceInfoValueObjPtr = ptrTableSourceHelper->getTableSourceInfoValueObj();
	//		//	if(TableSourceInfoValueObjPtr)
	//		//		callFromTS = TableSourceInfoValueObjPtr->getIsCallFromTABLEsource();
	//		//}

	//		if(callFromTS == kTrue)
	//		{
	//			////CA("callFromTS == kTrue");
	//			//if(TableSourceInfoValueObjPtr)
	//			//	if(TableSourceInfoValueObjPtr->getIsTable())
	//			//		tableInfo = TableSourceInfoValueObjPtr->getVectorAdvanceTableScreenValuePtr(); 
	//			//
	//			//if(!tableInfo)
	//			//{//CA("!tableInfo");
	//			//	isTableDataPresent = kFalse;
	//			//	ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayHybridTableScreenPrintInTabbedTextForm::!tableInfo");	
	//			//	break;
	//			//}

	//			//if(tableInfo->size()==0)
	//			//{ 
	//			//	//CA(" tableInfo->size()==0");
	//			//	isTableDataPresent = kFalse;
	//			//	ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayHybridTableScreenPrintInTabbedTextForm::tableInfo->size()==0");
	//			//	break;
	//			//}
	//			//			
	//			//for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
	//			//{
	//			//	oTableValue = *it;
	//			//	if( TableSourceInfoValueObjPtr->getVec_TableType_ID().at(0) == tagInfo.typeId && oTableValue.getTableTypeId() == tagInfo.typeId )
	//			//	{   				
	//			//		typeidFound=kTrue;
	//			//		break;
	//			//	}
	//			//}
	//							
	//		}
	//		else
	//		{
	//			//CA("callFromTS == kFalse");
	//			if(tagInfo.whichTab == 5)
	//				tableInfo=ptrIAppFramework->getHybridTableData(sectionid, pNode.getPubId(),tagInfo.languageID,kTrue);
	//			else
	//				tableInfo=ptrIAppFramework->getHybridTableData(sectionid, pNode.getPubId(),tagInfo.languageID,kFalse);
	//			
	//			if(!tableInfo)
	//			{//CA("!tableInfo");
	//				isTableDataPresent = kFalse;
	//				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayHybridTableScreenPrintInTabbedTextForm::!tableInfo");	
	//				break;
	//			}

	//			if(tableInfo->size()==0)
	//			{ 
	//				//CA(" tableInfo->size()==0");
	//				isTableDataPresent = kFalse;
	//				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayHybridTableScreenPrintInTabbedTextForm::tableInfo->size()==0");
	//				break;
	//			}
	//			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
	//			{
	//				oTableValue = *it;
	//				if(oTableValue.getTableTypeId() == tagInfo.typeId)
	//				{   				
	//					typeidFound=kTrue;
	//					break;
	//				}
	//			}
	//			if(tableInfo)
	//			{
	//				tableInfo->clear();
	//				delete tableInfo;
	//			}
	//		}
	//	

	//	}while(kFalse);
	//	
	///*int32 tableTypeID = oTableValue.getTableId(); 
	//attrVal.AppendNumber(tableTypeID);
	//tagInfo.tagPtr->SetAttributeValue("typeId",attrVal);
	//attrVal.Clear();*/
	//	
	//	if(isTableDataPresent == kFalse)
	//	{				
	//		PMString textToInsert("");
	//		
	//		if(!iConverter)
	//		{
	//			textToInsert=tabbedTextData;					
	//		}
	//		else
	//			textToInsert=iConverter->translateString(tabbedTextData);				
	//		//WideString insertText(textToInsert);	
	//		//textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
	//		/*K2*/boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
	//		ReplaceText(textModel,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);
	//		break;
	//	}
	//	
	//	if(typeidFound == kFalse)
	//	{
	//		ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayHybridTableScreenPrintInTabbedTextForm::typeidFound == kFalse");
	//		break;
	//	}

	//	bool8 isTranspose = oTableValue.getTranspose();
	//	
	//	int32 bodyRows= oTableValue.getBodyRows();
	//	int32 bodyColumns= oTableValue.getBodyColumns();
	//	int32 headerRows= oTableValue.getHeaderRows();
	//	//int32 headerColumns= oTableValue.getHeaderColumns();

	//	/*PMString Str("bodyRows = ");
	//	Str.AppendNumber(bodyRows);
	//	Str.Append("  , bodyColumns = ");
	//	Str.AppendNumber(bodyColumns);
	//	Str.Append("  , headerRows = ");
	//	Str.AppendNumber(headerRows);
	//	CA(Str);*/
	//
	//	vector<SlugStruct> listOfNewTagsAdded;

	//	////Check for the tableHeaderFlag at colno
	//	int32 headerFlag = tagInfo.header;//int32 colnoAsHeaderFlag = tagInfo.colno;
	//	
	//	//isTranspose=kTrue;
	//	if(!isTranspose)
	//	{				
	//		//	CA("!isTranspose");
	//		//	In case of Transpose == kFalse..the table gets sprayed like 
	//		//	Header11 Header12  Header13 ...
	//		//	Data11	 Data12	   Data13
	//		//	Data21	 Data22	   Data23
	//		//	....
	//		//	....
	//		//	So first row is Header Row and subsequent rows are DataRows.
	//		
	//		//The for loop below will spray the Table Header in left to right fashion.
	//		//Note typeId == -2 for TableHeaders.
	//		if(headerFlag == 1)//if(colnoAsHeaderFlag != -555)//Spray table with the headers.
	//		{
	//			vector<vector<PMString> >vec_tableheaders=oTableValue.getTableHeader();
	//			vector<vector<PMString> >::iterator it1;
	//			
	//			for(it1 = vec_tableheaders.begin(); it1 != vec_tableheaders.end(); it1++)
	//			{
	//				
	//				vector<PMString>vec_tableHeaderRow;
	//				vec_tableHeaderRow = (*it1);
	//				vector<PMString>::iterator it2;
	//				for(it2 = vec_tableHeaderRow.begin();it2 != vec_tableHeaderRow.end(); it2++)
	//				{			
	//					PMString dispname = (*it2);
	//					WideString* wStr=new WideString(*it2);
	//					
	//					SlugStruct newTagToBeAdded;
	//					newTagToBeAdded.elementId =-1;
	//					newTagToBeAdded.typeId = -1;//-2;
	//					newTagToBeAdded.header = headerFlag;
	//					newTagToBeAdded.parentId = pNode.getPubId();
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//					newTagToBeAdded.elementName = dispname ;
	//					newTagToBeAdded.TagName = prepareTagName("Header");
	//					newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//					newTagToBeAdded.LanguageID = tagInfo.languageID;			//PMString colName;
	//					newTagToBeAdded.whichTab =4 ;
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.sectionID = sectionid ;
	//					newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//					newTagToBeAdded.tableFlag = 0;
	//					newTagToBeAdded.row_no=-1;
	//					newTagToBeAdded.col_no=-1;//colnoAsHeaderFlag;					 
	//					PMString insertPMStringText;
	//					insertPMStringText.Append(newTagToBeAdded.elementName);
	//					if(!iConverter)
	//					{
	//						insertPMStringText=insertPMStringText;					
	//					}
	//					else
	//						insertPMStringText=iConverter->translateString(insertPMStringText);	

	//					if(it2 != vec_tableHeaderRow.end() - 1)
	//					{
	//						
	//						insertPMStringText.Append("\t");						
	//					}
	//					else
	//					{
	//						insertPMStringText.Append("\r");
	//					}

	//					tabbedTextData.Append(insertPMStringText);
	//					
	//					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
	//					tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//					newTagToBeAdded.tagStartPos=tagStartPos;
	//					newTagToBeAdded.tagEndPos=tagEndPos;
	//					
	//					/*PMString temp;
	//					temp.AppendNumber(newTagToBeAdded.tagStartPos);
	//					temp.Append("	,	");
	//					temp.AppendNumber(newTagToBeAdded.tagEndPos);
	//					CA("insertPMStringText	"+insertPMStringText);
	//					CA("tagStartPos &	tagendpos	 "+temp);*/ 
	//					  
	//					listOfNewTagsAdded.push_back(newTagToBeAdded);
	//					tagStartPos = tagEndPos+ 1 + 2;
	//					if(wStr)
	//						delete wStr;
	//		
	//				}
	//			}
	//		}
	//		//CA(tabbedTextData);
	//		
	//		vector<TableRow> vec_tableBodyData=oTableValue.getTableBodyData();
	//		vector<TableRow>::iterator it3;
	//		int j=0;
	//			
	//		for(it3 = vec_tableBodyData.begin(); it3 != vec_tableBodyData.end(); it3++)
	//		{
	//			//CA("vec_tableBodyData");
	//			vector<CellData> vec_CellData=(*it3).tableRowContent;
	//			PMString textToInsert("");
	//			InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	//			if(!iConverter)
	//			{
	//				break;					
	//			}
	//			vector<CellData>::iterator it4;
	//			for(it4 = vec_CellData.begin(); it4 != vec_CellData.end(); it4++)
	//			{
	//			//	CA("Cell Data");
	//				CellData tempCellData=(*it4);
	//				if(j>=bodyColumns)
	//					break;

	//				TagList tagList;
	//				textToInsert.Clear();					
	//						
	//				tagList = iConverter-> translateStringForAdvanceTableCell(tempCellData,textToInsert);

	//				int32 tagListSize =static_cast<int32>(tagList.size());
	//				
	//				PMString insertPMStringText;
	//				insertPMStringText.Append(textToInsert);
	//						
	//				if(it4 == vec_CellData.end()-1)
	//				{
	//					insertPMStringText.Append("\r");
	//				}
	//				else
	//				{
	//					insertPMStringText.Append("\t");
	//				}
	//				
	//				tabbedTextData.Append(insertPMStringText);
	//				int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();

	//				if(tagList.size() == 0)
	//				{						
	//					SlugStruct newTagToBeAdded;
	//					newTagToBeAdded.elementId = -1;
	//					newTagToBeAdded.typeId = -1;
	//					newTagToBeAdded.header = headerFlag;
	//					newTagToBeAdded.parentId = pNode.getPubId();
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//					PMString tagName("Static_Text");
	//					newTagToBeAdded.TagName = prepareTagName(tagName);
	//					newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//					newTagToBeAdded.LanguageID = tagInfo.languageID;			
	//					newTagToBeAdded.whichTab =tagInfo.whichTab;
	//					newTagToBeAdded.imgFlag = 0;
	//					newTagToBeAdded.sectionID = sectionid;
	//					newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//					newTagToBeAdded.tableFlag = 0;
	//					newTagToBeAdded.row_no= -1;
	//					newTagToBeAdded.col_no=-1;//colnoAsHeaderFlag;
	//						
	//					tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//					newTagToBeAdded.tagStartPos=tagStartPos;
	//					newTagToBeAdded.tagEndPos=tagEndPos;
	//					listOfNewTagsAdded.push_back(newTagToBeAdded);
	//					tagStartPos = tagEndPos+ 1 + 2;			
	//		
	//					j++;
	//					continue;
	//				}

	//				for(int32 k = 0; k < tagListSize; k++)
	//				{						
	//					TagStruct tInfo = tagList[k];
	//					SlugStruct newTagToBeAdded;
	//					newTagToBeAdded.whichTab   = tInfo.whichTab;
	//					newTagToBeAdded.typeId = tInfo.typeId;
	//					newTagToBeAdded.header = headerFlag;
	//					newTagToBeAdded.parentId = pNode.getPubId();					
	//					//newTagToBeAdded.tagStartPos= tInfo.startIndex;						
	//					//newTagToBeAdded.tagEndPos  = tInfo.endIndex;
	//					newTagToBeAdded.elementId  = tInfo.elementId;						
	//					PMString TagName("");
	//					TagName.Append("ATTRID_");
	//					TagName.AppendNumber(newTagToBeAdded.elementId);	
	//					newTagToBeAdded.TagName = prepareTagName(TagName);
	//					newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//					newTagToBeAdded.LanguageID =tagInfo.languageID;
	//					newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//					newTagToBeAdded.imgFlag = tagInfo.imgFlag;
	//					newTagToBeAdded.sectionID = CurrentSubSectionID; 
	//					newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//					newTagToBeAdded.col_no = tagInfo.colno;
	//					
	//					/*PMString temp("tagStartPos = ");
	//					temp.AppendNumber(tagStartPos);
	//					temp.Append(" ,	tInfo.startIndex = ");
	//					temp.AppendNumber(tInfo.startIndex);
	//					temp.Append(" ,	tInfo.endIndex = ");
	//					temp.AppendNumber(tInfo.endIndex);
	//					temp.Append(" ,	lengthOfTheString = ");
	//					temp.AppendNumber(lengthOfTheString);
	//					CA(temp);*/

	//					if(tagListSize == 1)///case : only one tag is present inside each cell 
	//					{
	//						tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//						newTagToBeAdded.tagStartPos=tagStartPos;
	//						newTagToBeAdded.tagEndPos=tagEndPos;
	//						listOfNewTagsAdded.push_back(newTagToBeAdded);
	//						tagStartPos = tagEndPos+ 1 + 2;
	//					}
	//					else //case : multiple tags are present inside each cell
	//					{
	//	
	//						/*int32 temp = tagStartPos;
	//						tagStartPos = tagStartPos + tInfo.startIndex;
	//						tagEndPos = temp + tInfo.endIndex;
	//						newTagToBeAdded.tagStartPos=tagStartPos;
	//						newTagToBeAdded.tagEndPos=tagEndPos;*/

	//						/*PMString temp("tagStartPos = ");
	//						temp.AppendNumber(tagStartPos);
	//						temp.Append(" ,	tInfo.startIndex = ");
	//						temp.AppendNumber(tInfo.startIndex);
	//						temp.Append(" ,	tInfo.endIndex = ");
	//						temp.AppendNumber(tInfo.endIndex);
	//						CA(temp);*/

	//						/*listOfNewTagsAdded.push_back(newTagToBeAdded);
	//						
	//						tagStartPos = temp + 2;*/
	//					}
	//											
	//				}
	//				//tagStartPos = tagStartPos + lengthOfTheString;
	//			}
	//			j=0;
	//		}
	//	}	//end of if IsTranspose == kFalse	
	//	else
	//	{
	//		//else of if IsTranspose ==kFalse..i.e IsTranspose == kTrue. Headers are in first column
	//			
	//		//In case of Transpose == kTrue..the table gets sprayed like 

	//		//	Header11 Data11 Data12 Data13..
	//		//	Header22 Data21 Data22 Data23..
	//		//	Header33 Data31 Data32 Data33..
	//		//	....
	//		//	....
	//		//So First Column is HeaderColumn and subsequent columns are Data columns.
	//		vector<TableRow> vec_tableBodyData=oTableValue.getTableBodyData();
	//		if(headerFlag == 1)//if(colnoAsHeaderFlag != -555)
	//		{
	//			vector<vector<PMString> >vec_tableheaders=oTableValue.getTableHeader();
	//			
	//			int32 Rows = bodyRows + headerRows;
	//			for(int32 rowIndex = 0; rowIndex < bodyColumns ; rowIndex++)
	//			{
	//				for(int32 colIndex = 0 ; colIndex < Rows ; colIndex++)
	//				{
	//					if(colIndex < headerRows)
	//					{
	//						vector<PMString> headerRowObj = vec_tableheaders[colIndex];
	//						PMString TextToInsert = headerRowObj[rowIndex];
	//						
	//						SlugStruct newTagToBeAdded;
	//						newTagToBeAdded.elementId =-1;
	//						newTagToBeAdded.typeId = -1;//-2;
	//						newTagToBeAdded.header = headerFlag;
	//						newTagToBeAdded.parentId = pNode.getPubId();
	//						newTagToBeAdded.imgFlag = 0;
	//						newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//						newTagToBeAdded.elementName = TextToInsert ;
	//						newTagToBeAdded.TagName = prepareTagName("Header");
	//						newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//						newTagToBeAdded.LanguageID = tagInfo.languageID;			
	//						newTagToBeAdded.whichTab =4 ;
	//						newTagToBeAdded.imgFlag = 0;
	//						newTagToBeAdded.sectionID = sectionid ;
	//						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//						newTagToBeAdded.tableFlag = 0;
	//						newTagToBeAdded.row_no=-1;
	//						newTagToBeAdded.col_no=-1;//colnoAsHeaderFlag;					 
	//						
	//						PMString insertPMStringText;
	//						insertPMStringText.Append(newTagToBeAdded.elementName);
	//						if(!iConverter)
	//						{
	//							insertPMStringText=insertPMStringText;					
	//						}
	//						else
	//							insertPMStringText=iConverter->translateString(insertPMStringText);	

	//						insertPMStringText.Append("\t");						
	//						
	//						tabbedTextData.Append(insertPMStringText);
	//						
	//						int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();
	//						tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//						newTagToBeAdded.tagStartPos=tagStartPos;
	//						newTagToBeAdded.tagEndPos=tagEndPos;
	//						
	//						/*PMString temp;
	//						temp.AppendNumber(newTagToBeAdded.tagStartPos);
	//						temp.Append("	,	");
	//						temp.AppendNumber(newTagToBeAdded.tagEndPos);
	//						CA("insertPMStringText	"+insertPMStringText);
	//						CA("tagStartPos &	tagendpos	 "+temp);
	//						*/   
	//						
	//						listOfNewTagsAdded.push_back(newTagToBeAdded);
	//						tagStartPos = tagEndPos+ 1 + 2;
	//						continue;
	//					}	
	//					PMString textToInsert("");
	//					CellData cellDataObj =  vec_tableBodyData[colIndex - headerRows].tableRowContent[rowIndex];
	//					TagList tagList;
	//					tagList = iConverter-> translateStringForAdvanceTableCell(cellDataObj,textToInsert);
	//					int32 tagListSize =static_cast<int32>(tagList.size());
	//				
	//					PMString insertPMStringText;
	//					insertPMStringText.Append(textToInsert);//stencileInfo.elementName);
	//				
	//					if(colIndex	== Rows-1)
	//					{
	//						if(rowIndex != bodyColumns - 1)
	//							insertPMStringText.Append("\r");
	//					}
	//					else
	//					{
	//						insertPMStringText.Append("\t");
	//					}
	//					
	//					tabbedTextData.Append(insertPMStringText);
	//					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();

	//					if(tagList.size() == 0)
	//					{
	//						
	//						SlugStruct newTagToBeAdded;
	//						newTagToBeAdded.elementId = -1;
	//						newTagToBeAdded.typeId = -1;
	//						newTagToBeAdded.header = headerFlag;
	//						newTagToBeAdded.parentId = pNode.getPubId();//objectId;
	//						newTagToBeAdded.imgFlag = 0;
	//						newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//						PMString tagName("Static_Text");
	//						newTagToBeAdded.TagName = prepareTagName(tagName);
	//						newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//						newTagToBeAdded.LanguageID = tagInfo.languageID;			
	//						newTagToBeAdded.whichTab =tagInfo.whichTab;
	//						newTagToBeAdded.imgFlag = 0;
	//						newTagToBeAdded.sectionID = sectionid;
	//						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//						newTagToBeAdded.tableFlag = 0;
	//						newTagToBeAdded.row_no=-1;
	//						newTagToBeAdded.col_no=-1;//colnoAsHeaderFlag;
	//						
	//						if(colIndex	== Rows-1)
	//						{
	//							if(rowIndex == bodyColumns - 1)
	//								tagEndPos = tagStartPos + lengthOfTheString;
	//							else
	//								tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//						}
	//						else
	//						{
	//							tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//						}
	//						
	//						newTagToBeAdded.tagStartPos=tagStartPos;
	//						newTagToBeAdded.tagEndPos=tagEndPos;
	//						listOfNewTagsAdded.push_back(newTagToBeAdded);
	//						tagStartPos = tagEndPos+ 1 + 2;			
	//			
	//						continue;
	//					}

	//					for(int32 k = 0; k < tagListSize; k++)
	//					{
	//						
	//						TagStruct tInfo = tagList[k];
	//						SlugStruct newTagToBeAdded;
	//						newTagToBeAdded.whichTab   = tInfo.whichTab;						
	//						newTagToBeAdded.typeId = tInfo.typeId;
	//						newTagToBeAdded.parentId = pNode.getPubId();						
	//						//newTagToBeAdded.tagStartPos= tInfo.startIndex;						
	//						//newTagToBeAdded.tagEndPos  = tInfo.endIndex;
	//						newTagToBeAdded.elementId  = tInfo.elementId;						
	//						PMString TagName("");
	//						TagName.Append("ATTRID_");
	//						TagName.AppendNumber(newTagToBeAdded.elementId);	
	//						newTagToBeAdded.TagName = prepareTagName(TagName);
	//						newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//						newTagToBeAdded.LanguageID =tagInfo.languageID;
	//						newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//						newTagToBeAdded.imgFlag = tagInfo.imgFlag;
	//						newTagToBeAdded.sectionID = CurrentSubSectionID; 
	//						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//						newTagToBeAdded.col_no = tagInfo.colno;
	//						
	//						/*PMString temp("tagStartPos = ");
	//						temp.AppendNumber(tagStartPos);
	//						temp.Append(" ,	tInfo.startIndex = ");
	//						temp.AppendNumber(tInfo.startIndex);
	//						temp.Append(" ,	tInfo.endIndex = ");
	//						temp.AppendNumber(tInfo.endIndex);
	//						temp.Append(" ,	lengthOfTheString = ");
	//						temp.AppendNumber(lengthOfTheString);
	//						CA(temp);*/

	//						if(tagListSize == 1)///case : only one tag is present inside each cell 
	//						{
	//							
	//							if(colIndex	== Rows-1)
	//							{
	//								if(rowIndex == bodyColumns - 1)
	//									tagEndPos = tagStartPos + lengthOfTheString;
	//								else
	//									tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//							}
	//							else
	//							{
	//								tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//							}
	//				
	//							newTagToBeAdded.tagStartPos=tagStartPos;
	//							newTagToBeAdded.tagEndPos=tagEndPos;
	//							listOfNewTagsAdded.push_back(newTagToBeAdded);
	//							tagStartPos = tagEndPos+ 1 + 2;
	//						}
	//						else //case : multiple tags are present inside each cell
	//						{
	//		
	//							/*int32 temp = tagStartPos;
	//							tagStartPos = tagStartPos + tInfo.startIndex;
	//							tagEndPos = temp + tInfo.endIndex;
	//							newTagToBeAdded.tagStartPos=tagStartPos;
	//							newTagToBeAdded.tagEndPos=tagEndPos;*/

	//							/*PMString temp("tagStartPos = ");
	//							temp.AppendNumber(tagStartPos);
	//							temp.Append(" ,	tInfo.startIndex = ");
	//							temp.AppendNumber(tInfo.startIndex);
	//							temp.Append(" ,	tInfo.endIndex = ");
	//							temp.AppendNumber(tInfo.endIndex);
	//							CA(temp);*/

	//							/*listOfNewTagsAdded.push_back(newTagToBeAdded);
	//							
	//							tagStartPos = temp + 2;*/
	//						}
	//												
	//					}						
	//					for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
	//					{
	//						tagList[tagIndex].tagPtr->Release();
	//					}
	//				}
	//			}
	//		}
	//		else
	//		{
	//			for(int32 rowIndex = 0; rowIndex < bodyColumns ; rowIndex++)
	//			{
	//				for(int32 colIndex = 0 ; colIndex < bodyRows ; colIndex++)
	//				{
	//					CellData cellDataObj =  vec_tableBodyData[colIndex].tableRowContent[rowIndex];
	//					TagList tagList;
	//					PMString textToInsert("");
	//					tagList = iConverter-> translateStringForAdvanceTableCell(cellDataObj,textToInsert);
	//					int32 tagListSize =static_cast<int32> (tagList.size());
	//				
	//					PMString insertPMStringText;
	//					insertPMStringText.Append(textToInsert);//stencileInfo.elementName);
	//				
	//					if(colIndex	== bodyRows - 1)
	//					{
	//						if(rowIndex != bodyColumns - 1)
	//							insertPMStringText.Append("\r");
	//					}
	//					else
	//					{
	//						insertPMStringText.Append("\t");
	//					}
	//					
	//					tabbedTextData.Append(insertPMStringText);
	//					int32 lengthOfTheString = insertPMStringText.NumUTF16TextChars();

	//					if(tagList.size() == 0)
	//					{							
	//						SlugStruct newTagToBeAdded;
	//						newTagToBeAdded.elementId = -1;
	//						newTagToBeAdded.typeId = -1;
	//						newTagToBeAdded.header = headerFlag;
	//						newTagToBeAdded.parentId = pNode.getPubId();
	//						newTagToBeAdded.imgFlag = 0;
	//						newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//						PMString tagName("Static_Text");
	//						newTagToBeAdded.TagName = prepareTagName(tagName);
	//						newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//						newTagToBeAdded.LanguageID = tagInfo.languageID;			
	//						newTagToBeAdded.whichTab =tagInfo.whichTab;
	//						newTagToBeAdded.imgFlag = 0;
	//						newTagToBeAdded.sectionID = sectionid;
	//						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//						newTagToBeAdded.tableFlag = 0;
	//						newTagToBeAdded.row_no=-1;
	//						newTagToBeAdded.col_no=-1;//colnoAsHeaderFlag;
	//						
	//						if(colIndex	== bodyRows-1)
	//						{
	//							if(rowIndex == bodyColumns - 1)
	//								tagEndPos = tagStartPos + lengthOfTheString;
	//							else
	//								tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//						}
	//						else
	//						{
	//							tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//						}
	//						
	//						newTagToBeAdded.tagStartPos=tagStartPos;
	//						newTagToBeAdded.tagEndPos=tagEndPos;
	//						listOfNewTagsAdded.push_back(newTagToBeAdded);
	//						tagStartPos = tagEndPos+ 1 + 2;			
	//			
	//						continue;
	//					}

	//					for(int32 k = 0; k < tagListSize; k++)
	//					{
	//						
	//						TagStruct tInfo = tagList[k];
	//						SlugStruct newTagToBeAdded;
	//						newTagToBeAdded.whichTab   = tInfo.whichTab;						
	//						newTagToBeAdded.typeId = tInfo.typeId;
	//						newTagToBeAdded.parentId = pNode.getPubId();					
	//						//newTagToBeAdded.tagStartPos= tInfo.startIndex;						
	//						//newTagToBeAdded.tagEndPos  = tInfo.endIndex;
	//						newTagToBeAdded.elementId  = tInfo.elementId;						
	//						PMString TagName("");
	//						TagName.Append("ATTRID_");
	//						TagName.AppendNumber(newTagToBeAdded.elementId);	
	//						newTagToBeAdded.TagName = prepareTagName(TagName);
	//						newTagToBeAdded.TagName = keepOnlyAlphaNumeric(newTagToBeAdded.TagName);
	//						newTagToBeAdded.LanguageID =tagInfo.languageID;
	//						newTagToBeAdded.parentTypeId = pNode.getTypeId();
	//						newTagToBeAdded.imgFlag = tagInfo.imgFlag;
	//						newTagToBeAdded.sectionID = CurrentSubSectionID; 
	//						newTagToBeAdded.isAutoResize = tagInfo.isAutoResize;
	//						newTagToBeAdded.col_no = tagInfo.colno;
	//						
	//						/*PMString temp("tagStartPos = ");
	//						temp.AppendNumber(tagStartPos);
	//						temp.Append(" ,	tInfo.startIndex = ");
	//						temp.AppendNumber(tInfo.startIndex);
	//						temp.Append(" ,	tInfo.endIndex = ");
	//						temp.AppendNumber(tInfo.endIndex);
	//						temp.Append(" ,	lengthOfTheString = ");
	//						temp.AppendNumber(lengthOfTheString);
	//						CA(temp);*/

	//						if(tagListSize == 1)///case : only one tag is present inside each cell 
	//						{
	//							if(colIndex	== bodyRows-1)
	//							{
	//								if(rowIndex == bodyColumns - 1)
	//									tagEndPos = tagStartPos + lengthOfTheString;
	//								else
	//									tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//							}
	//							else
	//							{
	//								tagEndPos = tagStartPos + lengthOfTheString - 1;//don't add tab or newline character between the tag.
	//							}
	//							newTagToBeAdded.tagStartPos=tagStartPos;
	//							newTagToBeAdded.tagEndPos=tagEndPos;
	//							listOfNewTagsAdded.push_back(newTagToBeAdded);
	//							tagStartPos = tagEndPos+ 1 + 2;
	//						}
	//						else //case : multiple tags are present inside each cell
	//						{
	//		
	//							/*int32 temp = tagStartPos;
	//							tagStartPos = tagStartPos + tInfo.startIndex;
	//							tagEndPos = temp + tInfo.endIndex;
	//							newTagToBeAdded.tagStartPos=tagStartPos;
	//							newTagToBeAdded.tagEndPos=tagEndPos;*/

	//							/*PMString temp("tagStartPos = ");
	//							temp.AppendNumber(tagStartPos);
	//							temp.Append(" ,	tInfo.startIndex = ");
	//							temp.AppendNumber(tInfo.startIndex);
	//							temp.Append(" ,	tInfo.endIndex = ");
	//							temp.AppendNumber(tInfo.endIndex);
	//							CA(temp);*/

	//							/*listOfNewTagsAdded.push_back(newTagToBeAdded);
	//							
	//							tagStartPos = temp + 2;*/
	//						}													
	//					}
	//				}
	//			}
	//		}
	//	}
	//					
	//	//WideString insertText(tabbedTextData);	
	//	//textModel->Replace(replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,&insertText);
	//	/*K2*/boost::shared_ptr<WideString> insertText(new WideString(tabbedTextData));
	//	ReplaceText(textModel,replaceItemWithTabbedTextFrom,replaceItemWithTabbedTextTo-replaceItemWithTabbedTextFrom + 1 ,insertText);
	//				
	//	for(int32 indexOfTagToBeAdded = 0;indexOfTagToBeAdded <listOfNewTagsAdded.size();indexOfTagToBeAdded++)
	//	{					
	//		SlugStruct & newTagToBeAdded = listOfNewTagsAdded[indexOfTagToBeAdded];
	//		XMLReference createdElement;
	//		Utils<IXMLElementCommands>()->CreateElement(WideString(newTagToBeAdded.TagName), txtModelUIDRef,newTagToBeAdded.tagStartPos,newTagToBeAdded.tagEndPos,kInvalidXMLReference, &createdElement);
	//		addAttributesToANewlyCreatedTag(newTagToBeAdded,createdElement);
	//	}			
	//}while(kFalse);
}

bool16 CDataSprayer::fillBMSImageInBox
(const UIDRef& boxUIDRef, TagStruct& slugInfo, double objectId)
{	

	static PMString SectionSpryImagePath("");

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}
	InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	if(ptrIClientOptions==NULL)
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::!ptrIClientOptions");
		return kFalse;
	}

	imagePath=ptrIClientOptions->getImageDownloadPath();
	if(imagePath!="")
	{
		const char *imageP=imagePath.GetPlatformString().c_str();
		if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
			#ifdef MACINTOSH
				imagePath+="/";
			#else
				imagePath+="\\";
			#endif
	}

	if(imagePath=="")
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::imagePath is blank");
		return 1;
	}

	PMString fileName("");

	double mpv_value_id = -1;
	CObjectValue oVal;

	slugInfo.parentId = objectId;

	if(global_project_level == 3)
		slugInfo.sectionID = CurrentSubSectionID;
	if(global_project_level == 2)
		slugInfo.sectionID = CurrentSectionID;

	InterfacePtr<IHierarchy> iHier(boxUIDRef, UseDefaultIID());
	if(!iHier)
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::!iHier");		
		return kFalse;
	}
	
	UIDRef parentUIDRef(boxUIDRef.GetDataBase(), iHier->GetParentUID());
	InterfacePtr<IGeometry> iGeo(boxUIDRef, IID_IGEOMETRY);
	if(!iGeo)
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::!iGeo");		
		return kFalse;
	}

	PMRect pageBounds=iGeo->GetStrokeBoundingBox(InnerToPasteboardMatrix(iGeo));
	PMRect originalPageBounds;
	PMRect FirstFrameBounds = pageBounds;

	CAssetValue partnerAssetValueObj = ptrIAppFramework->getPartnerAssetValueByPartnerIdImageTypeId(objectId, slugInfo.sectionID, slugInfo.languageID,  slugInfo.typeId, slugInfo.whichTab);

	fileName = partnerAssetValueObj.geturl();
			
	if(fileName == "")
		return kFalse;

	UIDRef newItem;	
	do
	{
		SDKUtilities::Replace(fileName,"%20"," ");
	}while(fileName.IndexOfString("%20") != -1);

	//CA(fileName);
	PMString imagePathWithSubdir = imagePath;
	PMString typeCodeString("");

	if(typeCodeString.NumUTF16TextChars()!=0)
	{ 
	
		PMString TempString = typeCodeString;		
		CharCounter ABC = 0;
		bool16 Flag = kTrue;
		bool16 isFirst = kTrue;
		do
		{					
			if(TempString.Contains("/", ABC))
			{
			 	ABC = TempString.IndexOfString("/"); 				
				
		 		PMString * FirstString = TempString.Substring(0, ABC);		 		
		 		PMString newsubString = *FirstString;		 	
					 			
		 		if(!isFirst)
		 		imagePathWithSubdir.Append("\\");
				
		 		imagePathWithSubdir.Append(newsubString);	
				if(ptrIAppFramework->getAssetServerOption() == 0)
				 		FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir), kTrue);					 		
		 		isFirst = kFalse;					 		
		 		PMString * SecondString = TempString.Substring(ABC+1);
		 		PMString SSSTring =  *SecondString;		 		
		 		TempString = SSSTring;	
				if(FirstString)
					delete FirstString;

				if(SecondString)
					delete SecondString;

			}
			else
			{				
				if(!isFirst)
		 		imagePathWithSubdir.Append("\\");

				imagePathWithSubdir.Append(TempString); //-------
				if(ptrIAppFramework->getAssetServerOption() == 0)
			 		FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir), kTrue);		 		
		 		isFirst = kFalse;				
				Flag= kFalse;
			}				
				
		}while(Flag);

	}

	if(ptrIAppFramework->getAssetServerOption() == 0)
		FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir));

    #ifdef WINDOWS
        SDKUtilities ::AppendPathSeparator(imagePathWithSubdir);
    #endif
	
	PMString total=imagePathWithSubdir+fileName;	
	
	//CA("total = " + total);
    #ifdef MACINTOSH
        SDKUtilities::convertToMacPath(total);
    #endif
		
	originalPageBounds = pageBounds;
	newItem = boxUIDRef;
	if(ImportFileInFrame(newItem, total))
	{									
		//fitImageInBox(newItem);						
	}
	
	PMString attribVal;
	attribVal.AppendNumber(PMReal(slugInfo.parentId));
	if(slugInfo.tagPtr)
	{
		slugInfo.tagPtr->SetAttributeValue(WideString("parentID"),WideString(attribVal));//ObjectID
		attribVal.Clear();							
		
		double pubId=slugInfo.sectionID;
		attribVal.AppendNumber(PMReal(pubId));
		slugInfo.tagPtr->SetAttributeValue(WideString("sectionID"),WideString(attribVal));//Publication ID
		attribVal.Clear();

		attribVal.AppendNumber(PMReal(pNode.getTypeId()));
		slugInfo.tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attribVal));//Parent Type ID
		attribVal.Clear();

		attribVal.AppendNumber(PMReal(slugInfo.typeId));
		slugInfo.tagPtr->SetAttributeValue(WideString("typeId"),WideString(attribVal));
		attribVal.Clear();

		attribVal.AppendNumber(PMReal(mpv_value_id));
		slugInfo.tagPtr->SetAttributeValue(WideString("rowno"),WideString(attribVal));
		attribVal.Clear();		
	}			
	return 1;
}
void CDataSprayer:: sprayItemImageInsideHybridTable(UIDRef& boxUIDRef, TagStruct & tagInfo, double itemID,int32 imageno)
{
	//do
	//{
	//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//	if(ptrIAppFramework == NULL)
	//	{
	//		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
	//		break;
	//	}

	//	InterfacePtr<IHierarchy> iHier(boxUIDRef, UseDefaultIID());
	//	if(!iHier)
	//	{
	//		ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:!iHier");				
	//		break;
	//	}
	//	
	//	UIDRef parentUIDRef(boxUIDRef.GetDataBase(), iHier->GetParentUID());
	//	InterfacePtr<IGeometry> iGeo(boxUIDRef, IID_IGEOMETRY);
	//	if(!iGeo)
	//	{
	//		ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:!iGeo");								
	//		break;
	//	}

	//	PMRect pageBounds=iGeo->GetStrokeBoundingBox(InnerToPasteboardMatrix(iGeo));
	//	PMRect FirstFrameBounds = pageBounds;
	//	VectorLongIntValue::iterator it;
	//	PMReal margin = 0;
	//	PMString TagName("");
	//	int32 Flag=0;
	//	
	//	bool16 ItemImageUnspreadFlag = kFalse;
	//	UIDRef newItem;	

	//	XMLReference tagRef = tagInfo.tagPtr->GetXMLReference();
	//	XMLTagAttributeValue tagVal;
	//	convertTagStructToXMLTagAttributeValue(tagInfo,tagVal);

	//	InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	//	if(ptrIClientOptions==NULL)
	//	{//CA("ptrIClientOptions==NULL");
	//		ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:!ptrIClientOptions");
	//		break;
	//	}

	//	PMString imagePath=ptrIClientOptions->getImageDownloadPath();
	//		
	//	if(imagePath=="")
	//	{	//CA("imagePath");
	//		ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:imagePath == blank");					
	//		break;
	//	}

	//	if(imagePath!="")
	//	{
	//		const char *imageP=imagePath.GetPlatformString().c_str();
	//		if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
	//		#ifdef MACINTOSH
	//			imagePath+="/";
	//		#else
	//			imagePath+="\\";
	//		#endif
	//	}
	//					

	//	PMString fileName("");
	//	int32 assetID;
	//	int32 mpv_value_id = -1;

	//	//	fileName=ptrIAppFramework->getObjectAssetName(tagInfo.parentId, tagInfo.parentTypeID, tagInfo.typeId); commented @vaibhav
	//	VectorAssetValuePtr AssetValuePtrObj = NULL;

	//	/*int32 temptypeid = tagInfo.typeId;
	//	int32 tempparentid = tagInfo.parentId;
	//	int32 tempparenttypeid = tagInfo.parentTypeID;
	//	PMString tempstri = "";
	//	tempstri.Append(" parentId = ");
	//	tempstri.AppendNumber(tempparentid);
	//	tempstri.Append(" , parentTypeID = ");
	//	tempstri.AppendNumber(tempparenttypeid);
	//	tempstri.Append(" , typeId = ");
	//	tempstri.AppendNumber(temptypeid);
	//	CA(tempstri);*/
	//	if(tagInfo.elementId > 0)
	//	{
	//		//CA("tagInfo.elementId > 0");
	//		PMString tempTypeID("");
	//		tempTypeID.AppendNumber(tagInfo.typeId);

	//		ptrIAppFramework->clearAllStaticObjects();
	//		AssetValuePtrObj = ptrIAppFramework->GETAssets_GetPVAssetsList(tempTypeID,tagInfo.parentId,tagInfo.imageIndex);
	//	}
	//	else if(tagInfo.typeId != -99)
	//	{
	//		//CA("tagInfo.typeId != -99");
	//		AssetValuePtrObj= ptrIAppFramework->GETAssets_GetAssetByParentAndType(itemID, CurrentSectionID, 0, tagInfo.typeId);
	//	}
	//	else
	//	{
	//		//CA("tagInfo.typeId == -99");
	//		AssetValuePtrObj= ptrIAppFramework->GETAssets_GetAssetByParentAndType(itemID, CurrentSectionID, 0);
	//	}
	//		
	//	if(AssetValuePtrObj == NULL){
	//		//CA("AssetValuePtrObj == NULL");
	//		ItemImageUnspreadFlag = kTrue;
	//		continue ;
	//	}

	//	int32 sizeOfAssetValuePtrObj = static_cast<int32>(AssetValuePtrObj->size());
	//	PMString tempstr = "";
	//	tempstr.AppendNumber(sizeOfAssetValuePtrObj);
	//	//CA("sizeOfAssetValuePtrObj = " + tempstr);

	//	if(AssetValuePtrObj->size() == 0)
	//	{
	//		//CA("AssetValuePtrObj->size() == 0");
	//		ItemImageUnspreadFlag = kTrue;
	//		continue;
	//	}
	//		
	//	VectorAssetValue::iterator it1; // iterator of Asset value

	//	CAssetValue objCAssetvalue;
	//	int32 tempTypeid = -1;
	//	int32 imageIndex = 0;
	//	for(it1 = AssetValuePtrObj->begin();it1!=AssetValuePtrObj->end();it1++)
	//	{
	//		objCAssetvalue = *it1;
	//		fileName = objCAssetvalue.geturl();
	//		assetID = objCAssetvalue.getAsset_id();

	//		//
	//		mpv_value_id = objCAssetvalue.getMpv_value_id();
	//		//
	//	
	//		int32 typeId = -1;
	//		if(tagInfo.typeId == -99)
	//		{
	//			typeId = objCAssetvalue.getType_id();
	//			tempTypeid = typeId;
	//		}
	//		else
	//		{
	//			typeId = tagInfo.typeId;
	//			tempTypeid = typeId;
	//		}

	//		//CA("fileName : "+ fileName);											
	//		if(fileName=="")
	//			continue;

	//		UIDRef newItem;	
	//		do
	//		{
	//			SDKUtilities::Replace(fileName,"%20"," ");
	//		}while(fileName.IndexOfString("%20") != -1);

	//		//CA(" 5  " + fileName);
	//	
	//		PMString imagePathWithSubdir = imagePath;
	//		
	//		if(tagInfo.elementId > 0)	//For PVMPV image
	//		{

	//			imagePathWithSubdir.Append(objCAssetvalue.getDescription());
	//			//CA("imagePathWithSubdir : "+imagePathWithSubdir);
	//			if(ptrIAppFramework->getAssetServerOption() == 0)
	//			{
	//				//CA("Going to DownLoad");
	//				FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir), kTrue);
	//				if(!ptrIAppFramework->GETAsset_downLoadPVMPVAsset(objCAssetvalue.getDescription(),fileName,imagePathWithSubdir))
	//				{	
	//					//CA("GETAsset_downLoadItemAsset Failed");
	//					ItemImageUnspreadFlag = kTrue;
	//					continue;
	//				}
	//				
	//			}
	//			
	//			if(!fileExists(imagePathWithSubdir,fileName))
	//			{	
	//				//CA("Image Not Found on Local Drive");
	//				continue;
	//			}

	//		}
	//		else
	//		{
	//			PMString typeCodeString;
	//			//////////////////////////////////////////////////=========================from here
	//			typeCodeString=objCAssetvalue.getDescription();
	//			if(typeCodeString.NumUTF16TextChars()==0)
	//			{
	//				typeCodeString=ptrIAppFramework->ClientActionGetAssets_getItemAssetFolder(assetID);
	//			}
	//			if(typeCodeString.NumUTF16TextChars()!=0)
	//			{ 
	//			
	//				PMString TempString = typeCodeString;		
	//				CharCounter ABC = 0;
	//				bool16 Flag = kTrue;
	//				bool16 isFirst = kTrue;
	//				do
	//				{
	//					
	//					if(TempString.Contains("/", ABC))
	//					{
	//		 				ABC = TempString.IndexOfString("/"); 				
	//					
	//	 					PMString * FirstString = TempString.Substring(0, ABC);		 		
	//	 					PMString newsubString = *FirstString;		 	
	//										
	//	 					if(!isFirst)
	//	 					imagePathWithSubdir.Append("\\");
	//						
	//	 					imagePathWithSubdir.Append(newsubString);		 	
	//	 					FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir), kTrue);
	//						
	//	 					isFirst = kFalse;
	//						
	//	 					PMString * SecondString = TempString.Substring(ABC+1);
	//	 					PMString SSSTring =  *SecondString;		 		
	//	 					TempString = SSSTring;
	//						if(FirstString)
	//							delete FirstString;

	//						if(SecondString)
	//							delete SecondString;
	//					}
	//					else
	//					{				
	//						if(!isFirst)
	//	 					imagePathWithSubdir.Append("\\");
	//						
	//	 					imagePathWithSubdir.Append(TempString);		 		
	//	 					FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir), kTrue);		 		
	//	 					isFirst = kFalse;				
	//						Flag= kFalse;
	//						
	//						//CA(" 2 : "+ imagePathWithSubdir);
	//					}				
	//					
	//				}while(Flag);
	//		
	//			}
	//			
	//			#ifdef WINDOWS
	//				imagePathWithSubdir.Append("\\");
	//			#endif

	//			if(ptrIAppFramework->getAssetServerOption() == 0)
	//			{						
	//				if(!ptrIAppFramework->GETAsset_downLoadItemAsset(assetID,imagePathWithSubdir.GetPlatformString().c_str()))
	//				{	//CA("GETAsset_downLoadItemAsset Failed");
	//					ItemImageUnspreadFlag = kTrue;
	//					continue;
	//				}
	//				
	//			}

	//			if(!fileExists(imagePathWithSubdir,fileName))
	//			{	
	//				//CA("Image Not Found on Local Drive");
	//				continue;
	//			}

	//			imageIndex++;

	//			if(imageno != imageIndex)
	//				continue;
	//		}
	//	
	//			

	//		PMString total=imagePathWithSubdir+fileName;
	//	
	//		if(margin != 0 /*&& (tagInfo.typeId != -99)*/ )
	//		{
	//			if(isHorizontalFlow()==kTrue)
	//			{
	//				pageBounds.Top(pageBounds.Top());//+2+15);
	//				pageBounds.Bottom(pageBounds.Bottom());//+2+15);
	//				pageBounds.Left(pageBounds.Left()+margin);//15);//(pageBounds.Left()+2+margin)
	//				pageBounds.Right(pageBounds.Right()+margin);//15);//(pageBounds.Right()+2+margin)
	//			}
	//			else
	//			{
	//				pageBounds.Top(pageBounds.Top()+margin);//+2+15);
	//				pageBounds.Bottom(pageBounds.Bottom()+margin);//+2+15);
	//				pageBounds.Left(pageBounds.Left());//15);//(pageBounds.Left()+2+margin)
	//				pageBounds.Right(pageBounds.Right());//15);//(pageBounds.Right()+2+margin)
	//			
	//			}
	//			CreatePicturebox(newItem, parentUIDRef,pageBounds);
	//			UIDList tempUIDLIst(newItem.GetDataBase(), newItem.GetUID());
	//			
	//			NewImageFrameUIDList.push_back( newItem.GetUID());

	//			if(ImportFileInFrame(newItem, total))
	//			{					
	//				Flag=1;	
	//				//fitImageInBox(newItem);						
	//			}


	//		}
	//		else
	//		{
	//			newItem = boxUIDRef;
	//			if(ImportFileInFrame(boxUIDRef, total))
	//			{		
	//				Flag=1;	
	//				fitImageInBox(boxUIDRef);						
	//			}
	//		}
	//	
	//		
	//		//setTagParent(tagInfo, tagInfo.parentId);
	//		margin = pageBounds.Right() -pageBounds.Left() +2;

	//		if((tagInfo.typeId == -99) /*&& (pNode.getIsProduct() == kFalse)*/ )
	//			break;
	//	}
	//}while(false);
}


bool16 CDataSprayer::fillImageInBoxForCategoryImages
(const UIDRef& boxUIDRef, TagStruct& slugInfo, double objectId)
{	
	//CA("fillImageInBoxForCategoryImages1");
//	static PMString SectionSpryImagePath("");
//
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == NULL)
//	{
//		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
//		return kFalse;
//	}
//	InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
//	if(ptrIClientOptions==NULL)
//	{
//		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::!ptrIClientOptions");
//		return kFalse;
//	}
//	
//	PMString imagePath=ptrIClientOptions->getImageDownloadPath();
//	if(imagePath!="")
//	{
//		const char *imageP=imagePath.GetPlatformString().c_str();
//		if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
//			#ifdef MACINTOSH
//				imagePath+="/";
//			#else
//				imagePath+="\\";
//			#endif
//	}	
//	if(imagePath=="")
//	{
//		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillImageInBoxForCategoryImages::imagePath is blank");
//		return 1;
//	}
//		
//	//CA("ptrIAppFramework");
//	PMString fileName("");
//	int32 assetID;
//	int32 mpv_value_id = -1;
//	CObjectValue oVal;
//	
//	int32 parentTypeID = -1;
//	int32 LevelClassId = 0;
//	int32 imageType =-1;
//	if(slugInfo.catLevel > 0)
//	{
//		parentTypeID= -1; //ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_CLASS_TYPE");
//		LevelClassId = ptrIAppFramework->GetONEsourceObjects_getCategoryClassIDByObjectIdLevelNo(objectId, slugInfo.catLevel/*.colno*/, pNode.getIsProduct());
//		imageType = 1;
//	}
//	else if(slugInfo.catLevel == -1)
//	{
//		//CA("slugInfo.catLevel == -1"); 
//		//CAI(CurrentPublicationID);
//		parentTypeID= ptrIAppFramework->TYPECACHE_getTypeByCode("PUB_PUBLICATION_TYPE");
//		LevelClassId = CurrentPublicationID;
//		imageType =2;
//	}
//	else if(slugInfo.catLevel < -1)
//	{
//		//CA("slugInfo.catLevel < -1"); 
//		//CAI(CurrentSubSectionID);
//		parentTypeID= ptrIAppFramework->TYPECACHE_getTypeByCode("PUB_SUBSECTION_TYPE");
//		LevelClassId = CurrentSubSectionID;
//		imageType = 3;
//	}
//
//
//	if(slugInfo.whichTab == 5)
//	{
//		//CA("Inside If");		
//		VectorAssetValuePtr AssetValuePtrObj = NULL;
//		//CAI(LevelClassId);
//		//CAI(parentTypeID);
//		//CAI(slugInfo.typeId);
//		isProduct = 5; // for Section level Assets
//
//		AssetValuePtrObj = ptrIAppFramework->GETAssets_GetAssetByParentAndType(LevelClassId,LevelClassId,isProduct, slugInfo.typeId);
//		if(AssetValuePtrObj == NULL)
//		{
//			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::fillImageInBoxForCategoryImages::AssetValuePtrObj == NULL");
//			return kFalse;
//		}
//
//		//int32 size = AssetValuePtrObj->size();
//		//PMString sizeofobj("Size ");
//		//sizeofobj.AppendNumber(size);
//		//CA(sizeofobj );
//
//		if(AssetValuePtrObj->size() ==0)
//		{
//			ptrIAppFramework->LogInfo("AP7_DataSprayerModel::CDataSprayer::fillImageInBoxForCategoryImages::AssetValuePtrObj->size() ==0");
//			delete AssetValuePtrObj;
//			return kFalse;
//		}
//
//		slugInfo.parentId = LevelClassId;
//		slugInfo.parentTypeID = parentTypeID;
//
//		if(global_project_level == 3)
//			slugInfo.sectionID = LevelClassId;
//		if(global_project_level == 2)
//			slugInfo.sectionID = LevelClassId;
//
//		InterfacePtr<IHierarchy> iHier(boxUIDRef, UseDefaultIID());
//		if(!iHier)
//		{
//			ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillImageInBoxForCategoryImages::!iHier");		
//			return kFalse;
//		}
//		
//		UIDRef parentUIDRef(boxUIDRef.GetDataBase(), iHier->GetParentUID());
//////////////	A
//		InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
//		if(!iSelectionManager)
//		{
//			return kFalse;;
//		}
//
//		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
//		if (!layoutSelectionSuite) 
//		{	//CA("!layoutSelectionSuite");
//			return kFalse;
//		}
//		//layoutSelectionSuite->DeselectAll(); //CS3 change
//		layoutSelectionSuite->DeselectAllPageItems();
//		UIDList selectUIDList(boxUIDRef);
//		layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);
//
//////////////
//		InterfacePtr<IGeometry> iGeo(boxUIDRef, IID_IGEOMETRY);
//		if(!iGeo)
//		{
//			ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillImageInBoxForCategoryImages::!iGeo");		
//			return kFalse;
//		}
//
//		PMRect pageBounds=iGeo->GetStrokeBoundingBox(InnerToPasteboardMatrix(iGeo));
//		PMRect originalPageBounds;
//		PMRect FirstFrameBounds = pageBounds;
//
//		VectorAssetValue::iterator it; // iterator of Asset value	
//
//		PMReal margin = 0;
//		PMReal newMargin = 0;
//		PMString TagName("");
//		CAssetValue objCAssetvalue;
//		for(it = AssetValuePtrObj->begin();it!=AssetValuePtrObj->end();it++)
//		{
//			objCAssetvalue = *it;
//			fileName = objCAssetvalue.geturl();
//			assetID = objCAssetvalue.getAsset_id();
////
//			mpv_value_id = objCAssetvalue.getMpv_value_id();
//				//
//
//			int32 typeId = -1;
//			typeId = slugInfo.typeId;
//
//			//CA(fileName);		
//			// } // Commented By Rahul for MPV Image Implementation. Added in the last  
//			//else{	
//			//	// fileName=ptrIAppFramework->getObjectAssetName(referanceid, parentTypeID, slugInfo.typeId);
//			//}
//			//PMString ASD("Asset ID : ");
//			//ASD.AppendNumber(assetID);
//			//CA(ASD);
//			//CA(fileName);
//
//			if(fileName=="")
//				continue;
//			
//			UIDRef newItem;	
//			do
//			{
//				SDKUtilities::Replace(fileName,"%20"," ");
//			}while(fileName.IndexOfString("%20") != -1);
//
//			//CA(fileName);
//			PMString imagePathWithSubdir = imagePath;
//			PMString typeCodeString;
//
//			typeCodeString = objCAssetvalue.getDescription();
//			//CA("typeCodeString =  " + typeCodeString);
//
//			if(typeCodeString.NumUTF16TextChars()==0)
//				typeCodeString=ptrIAppFramework->ClientActionGetAssets_getCategoryAssetFolder(assetID, imageType);
//
////following code is commented by vijay on 17/8/2006 & instead of that following code is added			
//			/*if(typeCodeString.NumUTF16TextChars()!=0)
//				imagePathWithSubdir.Append(typeCodeString);	*/
////////////////////////////////////////////////////////////////////////////////////////////////
//			//////////////////////////////////////////////////=========================from here
//
//			if(typeCodeString.NumUTF16TextChars()!=0)
//			{ 
//				PMString TempString = typeCodeString;		
//				CharCounter ABC = 0;
//				bool16 Flag = kTrue;
//				bool16 isFirst = kTrue;
//				do
//				{					
//					if(TempString.Contains("/", ABC))
//					{
//		 				ABC = TempString.IndexOfString("/"); 				
//				 	
//	 					PMString * FirstString = TempString.Substring(0, ABC);		 		
//	 					PMString newsubString = *FirstString;		 	
//				 				 		
//	 					if(!isFirst)
//	 					imagePathWithSubdir.Append("\\");
//				 		
//	 					imagePathWithSubdir.Append(newsubString);	
//						if(ptrIAppFramework->getAssetServerOption() == 0)
//		 					FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir), kTrue);					 		
//	 					isFirst = kFalse;					 		
//	 					PMString * SecondString = TempString.Substring(ABC+1);
//	 					PMString SSSTring =  *SecondString;		 		
//	 					TempString = SSSTring;
//						if(FirstString)
//							delete FirstString;
//
//						if(SecondString)
//							delete SecondString;
//					}
//					else
//					{				
//						if(!isFirst)
//	 					imagePathWithSubdir.Append("\\");
//				 		
//	 					imagePathWithSubdir.Append(TempString);		 	
//						if(ptrIAppFramework->getAssetServerOption() == 0)
//		 					FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir), kTrue);		 		
//	 					isFirst = kFalse;				
//						Flag= kFalse;
//						
//						//CA(" 2 : "+ imagePathWithSubdir);
//					}					
//				}while(Flag);
//			}
//
//			//////////////////////////////////////////////////==========================upto here
//			if(ptrIAppFramework->getAssetServerOption() == 0)
//				FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir));
//			SDKUtilities ::AppendPathSeparator(imagePathWithSubdir);
//			
//			//CA("Image Path = " + imagePathWithSubdir + " FileName " + fileName);
//			//if(!fileExists(imagePathWithSubdir,fileName))
//			//{	
//				//if(!ptrIAppFramework->downloadServer(fileName.GetPlatformString().c_str(), imagePathWithSubdir.GetPlatformString().c_str(), slugInfo.typeId))
//
//			if(ptrIAppFramework->getAssetServerOption() == 0)
//			{
//				if(!ptrIAppFramework->GETAsset_downLoadCatagoryAsset(assetID,imagePathWithSubdir, imageType))
//				{
//					ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::fillImageInBoxForCategoryImages::!GETAsset_downLoadProductAsset");				
//					continue;
//				}
//				
//			}
//
//			if(!fileExists(imagePathWithSubdir,fileName))	
//			{
//				ptrIAppFramework->LogDebug("CDataSprayer:: fillImageInBoxForCategoryImages: Image Not Found on Local Drive");
//				continue; 
//			}
//			PMString total=imagePathWithSubdir+fileName;	
//			//CA(total);			
//
////10-april
//		//InterfacePtr<ILayoutControlData> layoutData(::QueryFrontLayoutData());
//		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
//		if (layoutData == nil)
//			break;
//
//		IDocument* document = layoutData->GetDocument();
//		if (document == NULL)
//			break;
//
//		IDataBase* database = ::GetDataBase(document);
//		if(!database)
//			break;
//		
//		UID pageUID = layoutData->GetPage();
//		if(pageUID == kInvalidUID)
//			break;
//
//		UIDRef pageRef(database, pageUID);
//
//		ThreadTextFrame ThreadObj;
//		PMRect marginBoxBounds;
//		ThreadObj .getMarginBounds (pageRef,marginBoxBounds);
//
//		
//		if(FirstFrameBounds.Left() > marginBoxBounds.Right() )
//		{
//			//CA("Got wrong Page UID");
//			/*IGeometry* spreadItem = layoutData->GetSpread();
//			if(spreadItem == nil)
//				break;*/
//			UIDRef ref = layoutData->GetSpreadRef();
//			InterfacePtr<ISpread> iSpread(ref, UseDefaultIID());
//			if (iSpread == nil)
//				break;
//
//			int numPages=iSpread->GetNumPages();
//			
//			pageUID= iSpread->GetNthPageUID(numPages-1);
//
//			UIDRef pageRef(database, pageUID);
//			ThreadObj .getMarginBounds (pageRef,marginBoxBounds);
//		}
////10-april
//
//			if(margin != 0 && (slugInfo.typeId != -98))
//			{
//					//////////////pageBounds.Top(pageBounds.Top());//+2+15);
//					//////////////pageBounds.Bottom(pageBounds.Bottom());//+2+15);
//					//////////////pageBounds.Left(pageBounds.Left()+margin);//15);//(pageBounds.Left()+2+margin)
//					//////////////pageBounds.Right(pageBounds.Right()+margin);//15);//(pageBounds.Right()+2+margin)
//					//////////////CreatePicturebox(newItem, parentUIDRef,pageBounds);
////11-april		
//				if(slugInfo.flowDir == 0)	
//				{//if(slugInfo.isAutoResize==-111)		
//					//CA("isHorizontalFlow()==kTrue");
//					pageBounds.Top(pageBounds.Top());//+2+15);
//					pageBounds.Bottom(pageBounds.Bottom());//+2+15);
//					pageBounds.Left(pageBounds.Left()+margin);//15);//(pageBounds.Left()+2+margin)	
//					pageBounds.Right(pageBounds.Right()+margin);//15);//(pageBounds.Right()+2+margin)
//					if(pageBounds.Right() > marginBoxBounds.Right())
//					{
//						pageBounds = originalPageBounds ;
//						pageBounds.Top(pageBounds.Top()+ newMargin);//+2+15);
//						pageBounds.Bottom(pageBounds.Bottom()+newMargin);//+2+15);
//						pageBounds.Left(pageBounds.Left());//15);//(pageBounds.Left()+2+margin)
//						pageBounds.Right(pageBounds.Right());//15);//(pageBounds.Right()+2+margin)	
//						originalPageBounds = pageBounds;
//					}
//				}
//				else
//				{	
//					//CA("isHorizontalFlow()==kFalse");
//					pageBounds.Top(pageBounds.Top()+margin);//+2+15);
//					pageBounds.Bottom(pageBounds.Bottom()+margin);//+2+15);
//					pageBounds.Left(pageBounds.Left());//15);//(pageBounds.Left()+2+margin)
//					pageBounds.Right(pageBounds.Right());//15);//(pageBounds.Right()+2+margin)
//					if(pageBounds.Bottom() > marginBoxBounds.Bottom())
//					{
//						pageBounds = originalPageBounds ;
//						pageBounds.Top(pageBounds.Top());//+2+15);
//						pageBounds.Bottom(pageBounds.Bottom());//+2+15);
//						pageBounds.Left(pageBounds.Left()+ newMargin);//15);//(pageBounds.Left()+2+margin)
//						pageBounds.Right(pageBounds.Right()+ newMargin);//15);//(pageBounds.Right()+2+margin)	
//						originalPageBounds = pageBounds;
//					}				
//				}
//                
//				bool16 stat = copySelectedItems(newItem,pageBounds);
//				if(!stat)
//					break;
//				
//				//CreatePicturebox(newItem, parentUIDRef,pageBounds);
//				UIDList tempUIDLIst(newItem.GetDataBase(), newItem.GetUID());
//				
//				NewImageFrameUIDList.push_back(newItem.GetUID());
////11-april
//				if(ImportFileInFrame(newItem, total))
//				{
//					//fitImageInBox(newItem);
//				}
//				/// Creating and Adding tag to newly created Item image box
//			
//				InterfacePtr<IPMUnknown> unknown(newItem, IID_IUNKNOWN);
//				if(unknown == NULL){
//					ptrIAppFramework->LogDebug("CDataSprayer:: fillImageInBoxForCategoryImages: unknown NULL");
//					return kFalse;
//				}
//				InterfacePtr<IDocument> doc(newItem.GetDataBase(), newItem.GetDataBase()->GetRootUID(), UseDefaultIID());
//				if(doc == NULL){
//					ptrIAppFramework->LogDebug("CDataSprayer:: fillImageInBoxForCategoryImages: Document not found...");
//					return kFalse;
//				}
//				InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(doc));
//				if (rootElement == NULL){			
//					ptrIAppFramework->LogDebug("CDataSprayer:: fillImageInBoxForCategoryImages: IIDXMLElement NIL...");
//					return kFalse;
//				}
//				
//				XMLReference parent = rootElement->GetXMLReference();
//				PMString storyTagName =	TagName;
//
//				XMLReference resultXMLRef=kInvalidXMLReference;
//				do {
//					// Acquire the IXLMElementCommands interface on the Utils boss, and use its method
//					// to tag the frame.  Arbitrarily ask for the element to be the 0th child of it's parent.
//					ErrorCode errCode = Utils<IXMLElementCommands>()->CreateElement(WideString(storyTagName), newItem.GetUID(), parent, 0, &resultXMLRef);
//					// Verify the results: no errors, valid XMLRef returned, we can instantiate it.
//					if (errCode != kSuccess)
//					{
//						//CA("ExpXMLActionComponent::TagFrameElement - CreateElement failed");
//						continue;
//					}		
//					if (resultXMLRef == kInvalidXMLReference)
//					{
//						//CA("ExpXMLActionComponent::TagFrameElement - Can't create new XMLReference");
//						continue;
//					}		
//					InterfacePtr<IIDXMLElement> newXMLElement (resultXMLRef.Instantiate());
//					if (newXMLElement==NULL) 
//					{
//						ptrIAppFramework->LogInfo("CDataSprayer:: fillImageInBoxForCategoryImages: ExpXMLActionComponent::TagFrameElement - Can't instantiate new XML element");
//					}		
//					
//					PMString attribName("ID");
//					PMString attribVal("");
//					attribVal.AppendNumber(-1);							
//					ErrorCode err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "typeId";
//					attribVal.AppendNumber(typeId);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "header";
//					attribVal.AppendNumber(-1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "isEventField";
//					attribVal.AppendNumber(-1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "deleteIfEmpty";
//					attribVal.AppendNumber(-1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "dataType";
//					attribVal.AppendNumber(-1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "isAutoResize";							
//					attribVal.AppendNumber(slugInfo.isAutoResize);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "LanguageID";								
//					attribVal.AppendNumber(slugInfo.languageID);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "index";
//					attribVal.AppendNumber(slugInfo.whichTab);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "pbObjectId";
//					attribVal.AppendNumber(-1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "parentID";
//					attribVal.AppendNumber(slugInfo.parentId);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "childId";
//					attribVal.AppendNumber(-1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "sectionID";
//					attribVal.AppendNumber(slugInfo.sectionID);						
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "parentTypeID";
//					attribVal.AppendNumber(slugInfo.parentTypeID);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "isSprayItemPerFrame";
//					attribVal.AppendNumber(-1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "catLevel";
//					attribVal.AppendNumber(slugInfo.catLevel);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//					
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "imgFlag";
//					attribVal.AppendNumber(1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//				
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "imageIndex";
//					attribVal.AppendNumber(-1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "flowDir";
//					attribVal.AppendNumber(slugInfo.flowDir);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//					
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "childTag";
//					attribVal.AppendNumber(-1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//					
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "tableFlag";
//					attribVal.AppendNumber(0);						
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "tableType";
//					attribVal.AppendNumber(-1);						
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));	
//					
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "tableId";
//					attribVal.AppendNumber(-1);						
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//					
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "rowno";
//					attribVal.AppendNumber(mpv_value_id);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "colno";
//					attribVal.AppendNumber(slugInfo.colno);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "field1";
//					attribVal.AppendNumber(-1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "field2";
//					attribVal.AppendNumber(-1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "field3";
//					attribVal.AppendNumber(-1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "field4";
//					attribVal.AppendNumber(-1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//					attribName.Clear();
//					attribVal.Clear();
//					attribName = "field5";
//					attribVal.AppendNumber(-1);							
//					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
//
//				}while (false);
//
//			}
//			else
//			{
//				originalPageBounds = pageBounds;
//
//				InterfacePtr<IScrapSuite> scrapSuite(static_cast<IScrapSuite*>(Utils<ISelectionUtils>()->QuerySuite(IID_ISCRAPSUITE)));
//				if (scrapSuite == nil)
//				{
//					//CA("CopySelectedItems: scrapSuite invalid");
//					ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::CopySelectedItems:: scrapSuite invalid");
//					continue;
//				}
//				InterfacePtr<IClipboardController> clipController(/*gSession*/GetExecutionContextSession(), UseDefaultIID());
//				if (clipController == nil)
//				{
//					ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::CopySelectedItems::BscCltCore:: clipController invalid");
//					continue;
//				}
//
//				//InterfacePtr<IControlView> controlView(::QueryFrontView());
//				IControlView* controlView = Utils<ILayoutUIUtils>()->QueryFrontView();
//				if (controlView == nil)
//				{
//					ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::CopySelectedItems:: controlView invalid");
//					continue;
//				}
//
//
//				// Copy and paste the selection:
//				if(scrapSuite->CanCopy(clipController) != kTrue)
//					continue;
//				if(scrapSuite->Copy(clipController) != kSuccess)
//					continue;
//
//				newItem = boxUIDRef;
//				if(ImportFileInFrame(newItem, total))
//				{									
//					//fitImageInBox(newItem);						
//				}
//				PMString attribVal;
//				attribVal.AppendNumber( slugInfo.parentId);
//				
//				if(slugInfo.tagPtr)
//				{	
//					TagName = slugInfo.tagPtr->GetTagString();
//					slugInfo.tagPtr->SetAttributeValue(WideString("parentID"),WideString(attribVal));//ObjectID
//					
//					attribVal.Clear();							
//					int32 pubId=slugInfo.sectionID;
//					attribVal.AppendNumber(pubId);
//					slugInfo.tagPtr->SetAttributeValue(WideString("sectionID"),WideString(attribVal));//Publication ID
//					
//					attribVal.Clear();
//					//pCache.isExist(id, pNode);
//					attribVal.AppendNumber(slugInfo.parentTypeID);					
//					slugInfo.tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attribVal));//Parent Type ID
//					
//					attribVal.Clear();
//					attribVal.AppendNumber(typeId);
//					slugInfo.tagPtr->SetAttributeValue(WideString("typeId"),WideString(attribVal));
//					
//					attribVal.Clear();
//					attribVal.AppendNumber(mpv_value_id);
//					slugInfo.tagPtr->SetAttributeValue(WideString("rowno"),WideString(attribVal));
//					
//					attribVal.Clear();
//					//
//				}
//			}
//			
//			//setTagParent(tagInfo, tagInfo.parentId);
//			if(slugInfo.flowDir == 0)
//			{//if(slugInfo.isAutoResize == -111)
//				margin = pageBounds.Right() -pageBounds.Left() +2;
//				newMargin = pageBounds.Bottom() - pageBounds.Top() +2;
//			}
//			else
//			{
//				margin = pageBounds.Bottom() -pageBounds.Top() +2;
//				newMargin = pageBounds.Right() - pageBounds.Left() +2;
//			}
//			
//		}
//
//		if(AssetValuePtrObj)
//		{
//			AssetValuePtrObj->clear();
//			delete AssetValuePtrObj;
//		}
//	}//end of if slugInfo.whichTab != 4
	return 1;
}

bool16 CDataSprayer::copySelectedItems(UIDRef &newItem , PMRect pageBounds)
{
	ErrorCode status = kFailure;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return kFalse;
	}
	
	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
	if(!iSelectionManager)
	{	
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::copySelectedItems::iSelectionManager is NULL");
		return kFalse;
	}

	do
	{
		// Acquire the interfaces we need to use IScrapSuite:
		InterfacePtr<IScrapSuite> scrapSuite(static_cast<IScrapSuite*>(Utils<ISelectionUtils>()->QuerySuite(IID_ISCRAPSUITE)));
		if (scrapSuite == nil)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::CopySelectedItems:: scrapSuite invalid");
			return kFalse;;
		}
		InterfacePtr<IClipboardController> clipController(/*gSession*/GetExecutionContextSession(), UseDefaultIID());
		if (clipController == nil)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::CopySelectedItems::BscCltCore:: clipController invalid");
			return kFalse;;
		}

		//InterfacePtr<IControlView> controlView(::QueryFrontView());
		InterfacePtr<IControlView> controlView (Utils<ILayoutUIUtils>()->QueryFrontView());
		if (controlView == nil)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::CopySelectedItems:: controlView invalid");
			return kFalse;;
		}


		// Copy and paste the selection:
		//if(scrapSuite->CanCopy(clipController) != kTrue)
		//	break;
		//if(scrapSuite->Copy(clipController) != kSuccess)
		//	break;
		if(scrapSuite->CanPaste(clipController) != kTrue)
			return kFalse;;
		if(scrapSuite->Paste(clipController, controlView) != kSuccess)
			return kFalse;;

		status = kSuccess;
		if (status != kSuccess)
			return kFalse;
		
		

		UIDList copiedBoxUIDList;
		
		InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
		( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
		if(!txtMisSuite)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::copySelectedItems::!txtMisSuite");
			return kFalse; 
		}
		txtMisSuite->GetUidList(copiedBoxUIDList);
		newItem = copiedBoxUIDList.GetRef(0);

		PBPMPoint moveToPoints(pageBounds.Left(),pageBounds.Top());	

		InterfacePtr<IGeometry> geometryPtr(newItem, IID_IGEOMETRY);
		if(!geometryPtr)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::copySelectedItems::case 1:!geometryPtr");								
			return kFalse;
		}
		PMRect boxBounds=geometryPtr->GetStrokeBoundingBox(InnerToPasteboardMatrix(geometryPtr));
		
		PMReal top = boxBounds.Top();
		PMReal left = boxBounds.Left();


		PMReal left1 = moveToPoints.X() - left;
		PMReal top1 = moveToPoints.Y()  - top;
	
		const PBPMPoint moveByPoints(left1, top1);
		
		Transform::CoordinateSpace coordinateSpace = Transform::PasteboardCoordinates() ;
		PBPMPoint referencePoint(PMPoint(0,0));
		ErrorCode errorCode =  Utils<Facade::ITransformFacade>()->TransformItems( copiedBoxUIDList, coordinateSpace, referencePoint, Transform::TranslateBy(moveByPoints.X(),moveByPoints.Y()));
		if(errorCode !=  kSuccess)
			return kFalse;
	} while (kFalse); // Only do once.
//CA("frame pasted");
	return kTrue;
}


void CDataSprayer::arrangeForSprayingProductForAllStandardTableStencil(UIDRef & boxUIDRef)
{
	UIDRef txtModelUIDRef = UIDRef::gNull;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return;
	}

	UID textFrameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	}
	if (textFrameUID == kInvalidUID)
	{
		//ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::!textFrameUID");	
		return;
	}	

    InterfacePtr<ITextModel>  textModel;
    getTextModelByBoxUIDRef(boxUIDRef , textModel);
	if (textModel == NULL)
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::!textModel");	
		return;
	}

	InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{ 
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::!itagReader");	
		return;
	}
	TagList tList=itagReader->getTagsFromBox(boxUIDRef);
	int numTags=static_cast<int>(tList.size());
	if(numTags<0)
	{
		ptrIAppFramework->LogInfo("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::numTags<0");	
		return;
	}

	bool16 isAtLeastOneItemTagPresent = kFalse;

	//added on 22Sept..EventPrice addition.
	bool16 isEventItemPriceItemTagPresent = kFalse;
	//ended on 22Sept..EventPrice addition.
	vector<int32> vectorItemTagIndex;
    //Collect the indices of all Item tags inside a vector.

	double langId ;

	for(int32 tagIndex=0;tagIndex<numTags;tagIndex++)
	{
		TagStruct & firstTagInfo = tList[tagIndex];
		langId = firstTagInfo.languageID;
		XMLContentReference xmlCntnRef = firstTagInfo.tagPtr->GetContentReference();
		if(xmlCntnRef.IsTable())
		{
			//CA("This is the tag attached to the table");
			continue;
		}

		if(firstTagInfo.whichTab == 4 //ItemTag present
			&& (firstTagInfo.typeId == -1 || firstTagInfo.dataType == 4)
			&& (firstTagInfo.isTablePresent != 1)	//ensure that this is not a ItemTable Tag
		)
		{			
			vectorItemTagIndex.push_back(tagIndex);
			isAtLeastOneItemTagPresent = kTrue;
		}
		else if(firstTagInfo.whichTab == 4 && firstTagInfo.isTablePresent == 1)
		{//This is a totally invalid tag for product.i.e ItemTableTag.
			makeTheTagImpotent(firstTagInfo.tagPtr);
		}
	}
	
	double sectionid = -1;
	if(global_project_level == 3)
		sectionid = CurrentSubSectionID;
	if(global_project_level == 2)
		sectionid = CurrentSectionID;
	if(pNode.getIsProduct() == 1)
	{
		bool16 ItemAbsentinProductFlag = kFalse;
		do
		{				
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == NULL)
			{
				CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
				break;
			}
				
			VectorScreenTableInfoPtr tableInfo = NULL;
			if(pNode.getIsONEsource())
			{
				// For ONEsource mode
				//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(pNode.getPubId());
			}else
			{
				//For publication mode 
				tableInfo =ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, pNode.getPubId(), langId, kTrue); //for custom table  
			}

			if(!tableInfo)
			{
				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::tableInfo is NULL");	
				ItemAbsentinProductFlag = kTrue;
				break;
			}
			if(tableInfo->size()==0)
			{
				//if there are no items present in a product then to attach "CustomTabbedText" tag 
				//following do-while is added here .........
				do
				{   
					int32 lineStartCharIndex = 0;
					int32 lineEndCharIndex = -1;
					for(int32 itemTagIndex = 0;itemTagIndex < vectorItemTagIndex.size();itemTagIndex++)
					{
						//Get the actual index of the itemtag inside the taglist of the box. 
						//now find the startIndex and endIndex of the line in which the item tag is present.

						int32 index = vectorItemTagIndex[itemTagIndex];
						
						TagStruct & tagInfo=tList[index];
						int32 startPos=-1;
						int32 endPos = -1;
						Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&startPos,&endPos);
						if(startPos <= lineEndCharIndex)
							continue;

						int32 len=2;
						UTF16TextChar utfChar[2]={kTextChar_Null,kTextChar_Null};
						//Find the starting character's index of the line
						TextIterator startIterator(textModel,startPos); 			
						do
						{
							if(startIterator.IsNull())
							{	
								//ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingItemItemWithOtherCopyAttributes::startIterator.IsNull");
								break;
							}
						
							while(kTrue)
							{
								(*startIterator).ToUTF16(utfChar,&len);
								if((utfChar[0] == kTextChar_CR) || (startIterator.IsNull()))
								break;
									
								startIterator--;
							}
						}while(kFalse);
						lineStartCharIndex = startIterator.Position() + 1;			

                        //Find the ending character's index of the line
						TextIterator endIterator(textModel,endPos);
						do
						{   
							if(endIterator.IsNull())
							{	
								//ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingItemItemWithOtherCopyAttributes::endIterator.IsNull");
								break;
							}
						
							while(kTrue)
							{
								(*endIterator).ToUTF16(utfChar,&len);
								if((utfChar[0] == kTextChar_CR) || (endIterator.IsNull()))
									break;
								endIterator++;
							}
						}while(kFalse);

						lineEndCharIndex = endIterator.Position();
						PMString colNo = tagInfo.tagPtr->GetAttributeValue(WideString("colno"));
					
						UIDRef textModelUIDRef =::GetUIDRef(textModel);
							
						PMString tableTagName = "CustomTabbedText";
							
                        startPos = lineStartCharIndex;
                        endPos = lineEndCharIndex;
						ErrorCode err = kFailure;
						XMLReference CreatedElement;
						const XMLReference &  parentXMLR = kInvalidXMLReference; 
						err = Utils<IXMLElementCommands>()->CreateElement(WideString(tableTagName),textModelUIDRef,startPos,
																			endPos,parentXMLR,& CreatedElement);

						PMString attribName("ID");
						PMString attribVal("-101");
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
						
						attribName.Clear();
						attribVal.Clear();
						attribName = "typeId";
						attribVal = "-5";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
						
						attribName.Clear();
						attribVal.Clear();
						attribName = "header";
						attribVal = "-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
						
						attribName.Clear();
						attribVal.Clear();
						attribName = "isEventField";
						attribVal = "-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
						
						attribName.Clear();
						attribVal.Clear();
						attribName = "deleteIfEmpty";
						attribVal = "-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
						
						attribName.Clear();
						attribVal.Clear();
						attribName = "dataType";
						attribVal = "-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
						
						attribName.Clear();
						attribVal.Clear();
						attribName="isAutoResize";
						attribVal="";
						attribVal.AppendNumber(tagInfo.isAutoResize);
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="LanguageID";
						attribVal="";
						attribVal.AppendNumber(PMReal(tagInfo.languageID));
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName = "index";
						attribVal = "3";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
						
						attribName.Clear();
						attribVal.Clear();
						attribName = "pbObjectId";
						attribVal.AppendNumber(PMReal(pNode.getPBObjectID()));
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="parentID";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName = "childId";
						attribVal = "-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
	
						attribName.Clear();
						attribVal.Clear();
						attribName="sectionID";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="parentTypeID";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="isSprayItemPerFrame";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="catLevel";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
						attribName.Clear();
						attribVal.Clear();
						attribName="imgFlag";
						attribVal="0";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
						
						attribName.Clear();
						attribVal.Clear();
						attribName="imageIndex";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
		
						attribName.Clear();
						attribVal.Clear();
						attribName="flowDir";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
	
						attribName.Clear();
						attribVal.Clear();
						attribName="childTag";
						attribVal="1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="tableFlag";
						attribVal="0";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="tableType";
						attribVal="7";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="tableId";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
						
						attribName.Clear();
						attribVal.Clear();
						attribName="rowno";
						attribVal="";

						//if(isComponentTableAttributePresent == 0)
						attribVal.AppendNumber(-1);
						/*else if(isComponentTableAttributePresent == 1)
							attribVal.AppendNumber(-901);
						else if(isComponentTableAttributePresent == 2)
							attribVal.AppendNumber(-902);
						else if(isComponentTableAttributePresent == 3)
							attribVal.AppendNumber(-903);*/

						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="colno";
						attribVal = colNo;
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="field1";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
	
						attribName.Clear();
						attribVal.Clear();
						attribName="field2";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="field3";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="field4";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

						attribName.Clear();
						attribVal.Clear();
						attribName="field5";
						attribVal="-1";
						err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                        
                        attribName.Clear();
                        attribVal.Clear();
                        attribName="groupKey";
                        attribVal="";
                        err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					}
				}while(kFalse);
				ptrIAppFramework->LogInfo("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::tableInfo->size()==0");	
				ItemAbsentinProductFlag = kTrue;
				break;
			}

			bool16 typeidFound=kFalse;
			if(ItemAbsentinProductFlag == kTrue)
			{
				for(int32 tagIndex=0;tagIndex<numTags;tagIndex++)
				{
					TagStruct & firstTagInfo = tList[tagIndex];
					XMLContentReference xmlCntnRef = firstTagInfo.tagPtr->GetContentReference();
					if(xmlCntnRef.IsTable())
					{
						//CA("This is the tag attached to the table");
						continue;
					}

					if(firstTagInfo.whichTab == 4 //ItemTag present
						&& (firstTagInfo.typeId == -1 || firstTagInfo.header == 1) 
						//ensure that this is not a ItemTable Tag
						&& (firstTagInfo.isTablePresent != 1)					
					)
					{
							makeTheTagImpotent(firstTagInfo.tagPtr);
					}
				}
			}
	
			tabbedTagStructList.clear();
			
			TextIndex startPos=-1;
			TextIndex endPos = -1;
			
			txtModelUIDRef = ::GetUIDRef(textModel);

			int32 tagIndex = 0;
			int32 tagIndexOfNewlyAddedTagDueToLinePaste = 0;
			int32 tagListSize =static_cast<int32>(tList.size());

			int32 firstCharacterIndexOfTheline = 0;
			TagStruct tagInfo;	

			TextIterator startIterator(textModel,firstCharacterIndexOfTheline);
			if(startIterator.IsNull())
			{	
				ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::startIterator.IsNull");	
				return;
			}
			bool16 breakTheOuterLoop = kFalse;
			bool16 areItemIDsCollected = kFalse;

			int32 lineStartCharIndex = 0;
			int32 lineEndCharIndex = -1;

			int32 firstTagIndex =0;
			int32 lastTagIndex = 0;
			int32 updatedTagIndex = 0;
			vector<double> FinalItemIds;

			CItemTableValue oTableValue1;
			vector<double> vec_items1;
			vector<double> vec_tableTypeID;
			VectorScreenTableInfoValue::iterator it12;
			for(it12 = tableInfo->begin(); it12!=tableInfo->end(); it12++)
			{	
				oTableValue1 = *it12;
				vec_tableTypeID.push_back(oTableValue1.getTableTypeID());

				if(it12 == tableInfo->begin())
					vec_items1 = oTableValue1.getItemIds();
			}
					
			int32 lenOfTableNameTag = 0;
			if(vectorItemTagIndex.size() == 0)
			{
				if(tList[0].tableType == 7)
				{//if(tList[0].typeId == -116)
					int32 startPos1=-1;
					int32 endPos1 = -1;
					Utils<IXMLUtils>()->GetElementIndices(tList[0].tagPtr,&startPos1,&endPos1);

					lenOfTableNameTag = (endPos1-startPos1)+ 1;
					int32 tableNameStartIndex = 0;
					int32 tableNameEndIndex = lenOfTableNameTag;
					int32 startPasteIndex = lenOfTableNameTag;
					TextIndex destEnd = lenOfTableNameTag;
					CItemTableValue oTableValue;
					vector<double> vec_items;
					VectorScreenTableInfoValue::iterator it;
					FinalItemIds.clear();
					int32 i = 0;
					for(it = tableInfo->begin(); it!=tableInfo->end(); it++,i++)
					{//for tabelInfo start	
						oTableValue = *it;				
						vec_items = oTableValue.getItemIds();
                        boost::shared_ptr< PasteData > pasteData;
						
						if(tList[0].tableType == 7)
						{//if(tList[0].typeId == -116)							
							if(i != 0)
							{
								do{
									InterfacePtr<ICommand> copyStoryRangeCmd(CmdUtils::CreateCommand(kCopyStoryRangeCmdBoss));
									if (copyStoryRangeCmd == nil) 
									{
										//CA("copyStoryRangeCmd == nil");
										break;
									}
			
									// Refer the command to the source story and range to be copied.
									InterfacePtr<IUIDData> sourceUIDData(copyStoryRangeCmd, UseDefaultIID());
									if (sourceUIDData == nil) 
									{
										//CA("sourceUIDData == nil");
										break;
									}
								
									sourceUIDData->Set(txtModelUIDRef);
									InterfacePtr<IRangeData> sourceRangeData(copyStoryRangeCmd, UseDefaultIID());
									if (sourceRangeData == nil) 
									{
										//CA("sourceRangeData == nil");
										break;
									}

									sourceRangeData->Set(tableNameStartIndex, tableNameEndIndex);

									// Refer the command to the destination story and the range to be replaced.
									UIDList itemList(txtModelUIDRef);
									copyStoryRangeCmd->SetItemList(itemList);
								
									WideString* newLineCharacter = new WideString("\r");
									textModel->Insert(startPasteIndex ,newLineCharacter);
									/*boost::shared_ptr<WideString> insertText(newLineCharacter);

									InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
    								ASSERT(textModelCmds);
    								if (!textModelCmds) {
										break;
									}
									InterfacePtr<ICommand> insertCmd(textModelCmds->InsertCmd(startPasteIndex, insertText));
									ASSERT(insertCmd);
									if (!insertCmd) {
										break;
									}
									ErrorCode status = CmdUtils::ProcessCommand(insertCmd);*/

									CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
									startPasteIndex++;
									destEnd++;

									InterfacePtr<IRangeData> destRangeData(copyStoryRangeCmd, IID_IRANGEDATA2);

									destRangeData->Set(startPasteIndex, destEnd );
									// Process CopyStoryRangeCmd
									ErrorCode status1 = CmdUtils::ProcessCommand(copyStoryRangeCmd);

									if(newLineCharacter)
										delete newLineCharacter;
								
								}while(kFalse);
				
								startPasteIndex = startPasteIndex + lenOfTableNameTag;
								destEnd = startPasteIndex;	
							
							}
						}
					}

					TagList newTagList = itagReader->getTagsFromBox(boxUIDRef);

					int32 pastedTagCount = 0;
					int32 tableNameIndex = 0;
					for(int32 newTagIndex = 0;newTagIndex < newTagList.size();newTagIndex++)
					{
						TagStruct & newTagInfo = newTagList[newTagIndex];
						XMLReference newTagInfoXmlRef = newTagInfo.tagPtr->GetXMLReference();
						if(newTagInfo.tableType == 7)
						{//if(newTagInfo.typeId == -116)
							//CA("Table name.....");
							PMString attributeValue;
							attributeValue.AppendNumber(PMReal(vec_tableTypeID[tableNameIndex]));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef,   WideString("typeId"),WideString(attributeValue));
							tableNameIndex++;						
						}	

					}

					UIDRef textModelUIDRef =::GetUIDRef(textModel);
					PMString CustomTabbedTextTagName = "All_Standard_Tables";
		
					ErrorCode err = kFailure;
					XMLReference CreatedElement;
					const XMLReference &  parentXMLR = kInvalidXMLReference; 
					err = Utils<IXMLElementCommands>()->CreateElement(WideString(CustomTabbedTextTagName),textModelUIDRef,
																	0, startPasteIndex,
																	parentXMLR,& CreatedElement);
					

					PMString attribName("ID");
					PMString attribVal("-101");
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName = "typeId";
					attribVal = "-1";//"-116";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName = "header";
					attribVal.AppendNumber(tagInfo.header);
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName = "isEventField";
					attribVal = "-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName = "deleteIfEmpty";
					attribVal = "-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName = "dataType";
					attribVal = "-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName="isAutoResize";
					attribVal="";
					attribVal.AppendNumber(newTagList[0].isAutoResize);
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName="LanguageID";
					attribVal="";
					attribVal.AppendNumber(PMReal(newTagList[0].languageID));
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName = "index";
					attribVal = "3";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName = "pbObjectId";
					attribVal.AppendNumber(PMReal(pNode.getPBObjectID()));
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="parentID";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName = "childId";
					attribVal = "-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="sectionID";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="parentTypeID";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="isSprayItemPerFrame";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName="catLevel";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName="imgFlag";
					attribVal="0";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName="imageIndex";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName="flowDir";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
		
					attribName.Clear();
					attribVal.Clear();
					attribName="childTag";
					attribVal="1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
			
					attribName.Clear();
					attribVal.Clear();
					attribName="tableFlag";
					attribVal="0";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName="tableType";
					attribVal="7";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="tableId";
					attribVal="-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
				
					attribName.Clear();
					attribVal.Clear();
					attribName="rowno";
					attribVal= "-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName="colno";
					//attribVal="-101";
					attribVal.AppendNumber(PMReal(tagInfo.colno));
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName="field1";
					attribVal = "-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="field2";
					attribVal = "-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="field3";
					attribVal = "-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="field4";
					attribVal = "-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName="field5";
					attribVal = "-1";
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="groupKey";
                    attribVal = "";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));


					for(int32 tagIndex = 0 ; tagIndex < newTagList.size() ; tagIndex++)
					{
						newTagList[tagIndex].tagPtr->Release();
					}
				}

			}

			for(int32 itemTagIndex = 0;itemTagIndex < vectorItemTagIndex.size();itemTagIndex++)
			{
				//Get the actual index of the itemtag inside the taglist of the box. 
				//now find the startIndex and endIndex of the line in which the item tag is present.

				int32 index = vectorItemTagIndex[itemTagIndex];
				bool16 headerPresentInTheRow = kFalse;

				//following statement is to be removed.
				lastTagIndex = index;

				tagInfo=tList[index];
				XMLReference tagInfoXMLRef = tagInfo.tagPtr->GetXMLReference();
				PMString header = tagInfo.tagPtr->GetAttributeValue(WideString("header"));
				if(header == "1")
				{
					headerPresentInTheRow = kTrue;
				}

				int32 startPos=-1;
				int32 endPos = -1;
				Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&startPos,&endPos);
			
				if(startPos <= lineEndCharIndex)
					continue;

				int32 len=2;
				UTF16TextChar utfChar[2]={kTextChar_Null,kTextChar_Null};
				//Find the starting character's index of the line
				TextIterator startIterator(textModel,startPos); 			
				do
				{
					if(startIterator.IsNull())
					{	
						//ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::startIterator.IsNull");	
						break;
					}
					
					while(kTrue)
					{
						(*startIterator).ToUTF16(utfChar,&len);
						if((utfChar[0] == kTextChar_CR) || (startIterator.IsNull()))
						break;
							
						startIterator--;
					}
				}while(kFalse);
				lineStartCharIndex = startIterator.Position() + 1;
					
				//Find the ending character's index of the line
				TextIterator endIterator(textModel,endPos);
				TextIterator endIterator1(textModel,endPos);
				do
				{
					if(endIterator.IsNull())
					{	
						//ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::endIterator.IsNull");	
						break;
					}
					int32 temp=0;
					while(kTrue)
					{
						if((endIterator1.IsNull() == kTrue))
						{
							//CA("(endIterator.IsNull() == kTrue)");
							break;
						}
						(*endIterator1).ToUTF16(utfChar,&len);
						//CA_NUM("endIterator1.Position() ",endIterator1.Position());
						if((utfChar[0] == kTextChar_CR))
						{	
							//CA("temp ++ ");
							temp++;								
						}	
						endIterator1++;
					}

					while(kTrue)
					{
						if((endIterator.IsNull() == kTrue))
						{
							//CA("(endIterator.IsNull() == kTrue)");
							break;
						}
						(*endIterator).ToUTF16(utfChar,&len);
						//CA_NUM("endIterator.Position() ",endIterator.Position());
						if((utfChar[0] == kTextChar_CR) || (endIterator.IsNull()))
						{
							if((utfChar[0] == kTextChar_CR) )
							{
								if(temp > 1)
								{
									endIterator++;								
								}
								else
								{
									//CA("Inserting new line character");
									WideString* enterChar= new WideString("\r");
									textModel->Insert(endIterator.Position(),enterChar);
									/*boost::shared_ptr<WideString> insertText(enterChar);

									InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
    								ASSERT(textModelCmds);
    								if (!textModelCmds) {
										break;
									}
									InterfacePtr<ICommand> insertCmd(textModelCmds->InsertCmd(endIterator.Position(), insertText));
									ASSERT(insertCmd);
									if (!insertCmd) {
										break;
									}
									ErrorCode status = CmdUtils::ProcessCommand(insertCmd);
									CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);*/
									endIterator++;
									delete enterChar;
								}
								
							}
							break;
						}
						endIterator++;
					}
				}while(kFalse);

				lineEndCharIndex = endIterator.Position();
				int32 numberOfTagsInALine = 0;

                //Find all the tags which are in the current line.
				for(int32 normalTagIndex = 0;normalTagIndex < numTags;normalTagIndex++)
				{
					TagStruct & tagInfo = tList[normalTagIndex];
					int32 startPos=-1;
					int32 endPos = -1;
					Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&startPos,&endPos);
					if(startPos >= lineStartCharIndex && endPos <= lineEndCharIndex)
					{
						numberOfTagsInALine++;
						if(headerPresentInTheRow)
						{
							//CA("headerPresentInTheRow");
							PMString attrIndex("index");
							PMString indexValue  = tagInfo.tagPtr->GetAttributeValue(WideString(attrIndex));
							PMString strTableFlag = tagInfo.tagPtr->GetAttributeValue(WideString("tableFlag"));
							if(strTableFlag == "-15")
							{
								continue;
							}
							else if(indexValue == "4" && strTableFlag != "1")
							{
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("header"),WideString("1"));
								//change  the tableFlag to -12 for item_copy_attributes in text frame.
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("tableFlag"),WideString("-12"));
							}
							else if(indexValue == "3" && strTableFlag != "1") 
							{
								//Header For nonItemCopyAttributes
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("tableFlag"),WideString("-13"));
							}
							else if(indexValue == "3" && strTableFlag == "1")
							{
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("ID"),WideString("-103"));								
							}
						}
						else
						{
						//	numberOfTimesTheLineToBeCopied = FinalItemIds.size() - 1;
							PMString attrIndex("index");
							PMString indexValue  = tagInfo.tagPtr->GetAttributeValue(WideString(attrIndex));
							PMString strTableFlag = tagInfo.tagPtr->GetAttributeValue(WideString("tableFlag"));	

							PMString strID = tagInfo.tagPtr->GetAttributeValue(WideString("ID"));
							if(strTableFlag == "-15")
							{
								continue;
							}
							else if(indexValue == "4" && strTableFlag != "1" /*&& strID == "-701"*/)
							{
								PMString attributeValue;
								attributeValue.AppendNumber(PMReal(vec_items1[0]));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("childId"),WideString(attributeValue));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("childTag"),WideString("1"));
								//change  the tableFlag to -12 for item_copy_attributes in text frame.
								Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("tableFlag"),WideString("-12"));

								if(tagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-803"))
								{
									attributeValue.Clear();										
									attributeValue.Append("a");												
									Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXMLRef, WideString("rowno"),WideString(attributeValue));
								}
							}								
						}						
					}
				}

                // For Copy paste Attributes
				int32 textSelectionStartAt = lineEndCharIndex;
				int32 length = lineEndCharIndex -lineStartCharIndex+ 1;
				int32 startPasteIndex = lineEndCharIndex;//+1
				TextIndex destEnd = lineEndCharIndex;
				int32 tableNameStartIndex,tableNameEndIndex,itemAttributeStartIndex,itemAttributsEndIndex;

				if(tList[0].tableType == 7)
				{//if(tList[0].typeId == -116)
					int32 startPos1=-1;
					int32 endPos1 = -1;
					Utils<IXMLUtils>()->GetElementIndices(tList[0].tagPtr,&startPos1,&endPos1);
					
					lenOfTableNameTag = (endPos1-startPos1) + 2;
					tableNameStartIndex = 0;
					tableNameEndIndex = lenOfTableNameTag;//28;
					itemAttributeStartIndex = lenOfTableNameTag;//28;
					itemAttributsEndIndex = lineEndCharIndex;
				}
				else
				{
					itemAttributeStartIndex=0;
					itemAttributsEndIndex = lineEndCharIndex;
				}

				CItemTableValue oTableValue;
				vector<double> vec_items;
				VectorScreenTableInfoValue::iterator it;
				FinalItemIds.clear();
				int32 i = 0;
				for(it = tableInfo->begin(); it!=tableInfo->end(); it++,i++)
				{//for tabelInfo start
					oTableValue = *it;				
					vec_items = oTableValue.getItemIds();
                    boost::shared_ptr< PasteData > pasteData;
						
						if(tList[0].tableType == 7)
						{//if(tList[0].typeId == -116)
                            //CA("isTableNamePresent");
							if(i != 0)
							{
								do{
									InterfacePtr<ICommand> copyStoryRangeCmd(CmdUtils::CreateCommand(kCopyStoryRangeCmdBoss));
									if (copyStoryRangeCmd == nil) 
									{
										//CA("copyStoryRangeCmd == nil");
										break;
									}
			
									// Refer the command to the source story and range to be copied.
									InterfacePtr<IUIDData> sourceUIDData(copyStoryRangeCmd, UseDefaultIID());
									if (sourceUIDData == nil) 
									{
										//CA("sourceUIDData == nil");
										break;
									}
								
									sourceUIDData->Set(txtModelUIDRef);
									InterfacePtr<IRangeData> sourceRangeData(copyStoryRangeCmd, UseDefaultIID());
									if (sourceRangeData == nil) 
									{
										//CA("sourceRangeData == nil");
										break;
									}
									sourceRangeData->Set(tableNameStartIndex, tableNameEndIndex);

									// Refer the command to the destination story and the range to be replaced.
									UIDList itemList(txtModelUIDRef);
									copyStoryRangeCmd->SetItemList(itemList);
									
									InterfacePtr<IRangeData> destRangeData(copyStoryRangeCmd, IID_IRANGEDATA2);

									destRangeData->Set(startPasteIndex, destEnd );
			
									// Process CopyStoryRangeCmd
									ErrorCode status = CmdUtils::ProcessCommand(copyStoryRangeCmd);
								}while(kFalse);
				
								startPasteIndex = startPasteIndex + lenOfTableNameTag;//28;
								destEnd = startPasteIndex;	

							}
						}
						
							for(int k1 = 0 ; k1 < vec_items.size() ; k1++)
							{
								
								if(headerPresentInTheRow && k1 == 0 && i == 0 )
								{
									do{
										InterfacePtr<ICommand> copyStoryRangeCmd(CmdUtils::CreateCommand(kCopyStoryRangeCmdBoss));
										if (copyStoryRangeCmd == nil) 
										{
											//CA("copyStoryRangeCmd == nil");
											break;
										}
				
										// Refer the command to the source story and range to be copied.
										InterfacePtr<IUIDData> sourceUIDData(copyStoryRangeCmd, UseDefaultIID());
										if (sourceUIDData == nil) 
										{
											//CA("sourceUIDData == nil");
											break;
										}
									
										sourceUIDData->Set(txtModelUIDRef);
										InterfacePtr<IRangeData> sourceRangeData(copyStoryRangeCmd, UseDefaultIID());
										if (sourceRangeData == nil) 
										{
											//CA("sourceRangeData == nil");
											break;
										}
										sourceRangeData->Set(itemAttributeStartIndex, itemAttributsEndIndex);

										// Refer the command to the destination story and the range to be replaced.
										UIDList itemList(txtModelUIDRef);
										copyStoryRangeCmd->SetItemList(itemList);
										
										InterfacePtr<IRangeData> destRangeData(copyStoryRangeCmd, IID_IRANGEDATA2);

										destRangeData->Set(startPasteIndex, destEnd );
				
										// Process CopyStoryRangeCmd
										ErrorCode status = CmdUtils::ProcessCommand(copyStoryRangeCmd);

									}while(kFalse);
					
									startPasteIndex = startPasteIndex + length -1;
									destEnd = startPasteIndex;

								}


								if(i == 0 && k1 == 0 )
									FinalItemIds.push_back(vec_items[k1]);
								else
								{
									do{
										InterfacePtr<ICommand> copyStoryRangeCmd(CmdUtils::CreateCommand(kCopyStoryRangeCmdBoss));
										if (copyStoryRangeCmd == nil) 
										{
											//CA("copyStoryRangeCmd == nil");
											break;
										}
				
										// Refer the command to the source story and range to be copied.
										InterfacePtr<IUIDData> sourceUIDData(copyStoryRangeCmd, UseDefaultIID());
										if (sourceUIDData == nil) 
										{
											//CA("sourceUIDData == nil");
											break;
										}
									
										sourceUIDData->Set(txtModelUIDRef);
										InterfacePtr<IRangeData> sourceRangeData(copyStoryRangeCmd, UseDefaultIID());
										if (sourceRangeData == nil) 
										{
											//CA("sourceRangeData == nil");
											break;
										}
										sourceRangeData->Set(itemAttributeStartIndex, itemAttributsEndIndex);

										// Refer the command to the destination story and the range to be replaced.
										UIDList itemList(txtModelUIDRef);
										copyStoryRangeCmd->SetItemList(itemList);
										
										InterfacePtr<IRangeData> destRangeData(copyStoryRangeCmd, IID_IRANGEDATA2);
										destRangeData->Set(startPasteIndex, destEnd );
				
										// Process CopyStoryRangeCmd
										ErrorCode status = CmdUtils::ProcessCommand(copyStoryRangeCmd);
										FinalItemIds.push_back(vec_items[k1]);

									}while(kFalse);
					
									startPasteIndex = startPasteIndex + length -1;
									destEnd = startPasteIndex;
								}
							}
				}
				
				int32 textSelectionEndAt = startPasteIndex-1;
				TagList newTagList = itagReader->getTagsFromBox(boxUIDRef);

				int32 pastedTagCount = 0;
				int32 tableNameIndex = 0;
				
				for(int32 newTagIndex = 0;newTagIndex < newTagList.size();newTagIndex++)
				{
					TagStruct & newTagInfo = newTagList[newTagIndex];
					XMLReference newTagInfoXmlRef = newTagInfo.tagPtr->GetXMLReference();
					if(newTagInfo.tableType == 7)
					{//if(newTagInfo.typeId == -116)
						PMString attributeValue;
						attributeValue.AppendNumber(PMReal(vec_tableTypeID[tableNameIndex]));
						Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef,   WideString("typeId"),WideString(attributeValue));

						tableNameIndex++;
						continue;
					}
					int32 newStartPos = -1;
					int32 newEndPos = -1;
					Utils<IXMLUtils>()->GetElementIndices(newTagInfo.tagPtr,&newStartPos,&newEndPos);

					if((newStartPos >= textSelectionStartAt) && (newEndPos <= textSelectionEndAt) &&  newTagInfo.tableType != 1)
					{//if((newStartPos >= textSelectionStartAt) && (newEndPos <= textSelectionEndAt) &&  newTagInfo.typeId != -116 )				

						PMString attrIndex("index");
						PMString indexValue  = newTagInfo.tagPtr->GetAttributeValue(WideString(attrIndex));
						PMString strTableFlag = newTagInfo.tagPtr->GetAttributeValue(WideString("tableFlag"));
						PMString strID = newTagInfo.tagPtr->GetAttributeValue(WideString("ID"));
						double itemID =-1;
				
						int32 itemIdSeqNo = 0;
						if(headerPresentInTheRow)
						{
							itemID = FinalItemIds[pastedTagCount / numberOfTagsInALine];
							itemIdSeqNo = (pastedTagCount / numberOfTagsInALine);
						}
						else
						{
							itemID = FinalItemIds[pastedTagCount / numberOfTagsInALine + 1];
							itemIdSeqNo = ((pastedTagCount / numberOfTagsInALine) +1 );
						}
						PMString attributeValue;
						attributeValue.AppendNumber(PMReal(itemID));
						if(strTableFlag == "-15")
						{
							continue;
						}
						else if(indexValue == "4" && strTableFlag != "1" /*&& strID == "-701"*/)
						{

							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef,   WideString("childId"),WideString(attributeValue));
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef,   WideString("childTag"),WideString("1"));
							//change  the tableFlag to -12 for item_copy_attributes in text frame.
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef,   WideString("tableFlag"),WideString("-12"));
		
							if(newTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-803"))
							{
								attributeValue.Clear();
                                attributeValue = getAlphabetValue(itemIdSeqNo);
                                
								Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef,   WideString("rowno"),WideString(attributeValue));
							}

							if(newTagInfo.tagPtr->GetAttributeValue(WideString("ID")) == WideString("-827")) //***** Number Keys
							{
								attributeValue.Clear();
								attributeValue.AppendNumber(PMReal(itemIdSeqNo+1));
								Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef,   WideString("rowno"),WideString(attributeValue));
							}
						}
						else if(indexValue == "3" && strTableFlag != "1")
						{
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef,   WideString("tableFlag"),WideString("0"));
						}
						else if(indexValue == "3" && strTableFlag == "1")
						{
							Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef,   WideString("ID"),WideString("-1"));
						}
						pastedTagCount++;
					}
				}
				
                ////To delete enter character
				textModel->Delete(startPasteIndex-1 ,1);
			
				UIDRef textModelUIDRef =::GetUIDRef(textModel);
				PMString CustomTabbedTextTagName = "All_Standard_Tables";
	
				ErrorCode err = kFailure;
				XMLReference CreatedElement;
				const XMLReference &  parentXMLR = kInvalidXMLReference; 
				err = Utils<IXMLElementCommands>()->CreateElement(WideString(CustomTabbedTextTagName),textModelUIDRef,
																0, startPasteIndex -1,
																parentXMLR,& CreatedElement);
				

				PMString attribName("ID");
				PMString attribVal("-101");
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
				
				attribName.Clear();
				attribVal.Clear();
				attribName = "typeId";
				attribVal = "-1";//"-116";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
				
				attribName.Clear();
				attribVal.Clear();
				attribName = "header";
				attribVal = "-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName = "isEventField";
				attribVal = "-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName = "deleteIfEmpty";
				attribVal = "-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName = "dataType";
				attribVal = "-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
				
				attribName.Clear();
				attribVal.Clear();
				attribName="isAutoResize";
				attribVal="";
				attribVal.AppendNumber(newTagList[0].isAutoResize);
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName="LanguageID";
				attribVal="";
				attribVal.AppendNumber(PMReal(newTagList[0].languageID));
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName = "index";
				attribVal = "3";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
				
				attribName.Clear();
				attribVal.Clear();
				attribName = "pbObjectId";
				attribVal.AppendNumber(PMReal(pNode.getPBObjectID()));
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName="parentID";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
	
				attribName.Clear();
				attribVal.Clear();
				attribName="childId";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName="sectionID";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName="parentTypeID";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName="isSprayItemPerFrame";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName="catLevel";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
				
				attribName.Clear();
				attribVal.Clear();
				attribName="imgFlag";
				attribVal="0";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
				
				attribName.Clear();
				attribVal.Clear();
				attribName="imageIndex";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName="flowDir";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName="childTag";
				attribVal="1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));				
				
				attribName.Clear();
				attribVal.Clear();
				attribName="tableFlag";
				attribVal="0";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
				
				attribName.Clear();
				attribVal.Clear();
				attribName="tableType";
				attribVal="7";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));	
				
				attribName.Clear();
				attribVal.Clear();
				attribName="tableId";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));	
				
				attribName.Clear();
				attribVal.Clear();
				attribName="rowno";
				attribVal="";

				//if(isComponentTableAttributePresent == 0)
				attribVal.AppendNumber(-1);
				//else if(isComponentTableAttributePresent == 1)
				//	attribVal.AppendNumber(-901);
				//else if(isComponentTableAttributePresent == 2)
				//	attribVal.AppendNumber(-902);
				//else if(isComponentTableAttributePresent == 3)
				//	attribVal.AppendNumber(-903);

				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
				
				attribName.Clear();
				attribVal.Clear();
				attribName="colno";
				//attribVal="-101";
				attribVal.AppendNumber(PMReal(tagInfo.colno));
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
				
				attribName.Clear();
				attribVal.Clear();
				attribName="field1";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

				attribName.Clear();
				attribVal.Clear();
				attribName="field2";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));	

				attribName.Clear();
				attribVal.Clear();
				attribName="field3";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));	

				attribName.Clear();
				attribVal.Clear();
				attribName="field4";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));	

				attribName.Clear();
				attribVal.Clear();
				attribName="field5";
				attribVal="-1";
				err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                
                attribName.Clear();
                attribVal.Clear();
                attribName="groupKey";
                attribVal="";
                err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
				
				for(int32 tagIndex = 0 ; tagIndex < newTagList.size() ; tagIndex++)
				{
					newTagList[tagIndex].tagPtr->Release();
				}
			}

		if(tableInfo)
			delete tableInfo;


		}while(kFalse);
			
	}
	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}

}
void CDataSprayer::arrangeForSprayingProductForAllStandardTableStencilForTableOption(UIDRef & boxUIDRef)
{
	do{
		double sectionid = -1;
		if(global_project_level == 3)
			sectionid = CurrentSubSectionID;
		if(global_project_level == 2)
			sectionid = CurrentSectionID;

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}
		
		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection ());
		if(!iSelectionManager)
		{	//CA("Slection NULL");
			break;
		}

        InterfacePtr<ITextModel>  textModel;
        getTextModelByBoxUIDRef(boxUIDRef , textModel);
		if (textModel == NULL)
		{
			ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::!textModel");	
			return;
		}
		UIDRef txtModelUIDRef = ::GetUIDRef(textModel);	
		VectorScreenTableInfoPtr tableInfo = NULL;
		if(pNode.getIsONEsource())
		{
			// For ONEsource mode
			//tableInfo =ptrIAppFramework->GetProduct_getObjectTableByObjectId(pNode.getPubId());
		}else
		{
			//For publication mode 
			double tempLangId = ptrIAppFramework->getLocaleId();
			tableInfo =ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(sectionid, pNode.getPubId(),tempLangId,  kTrue); // getting only reqd data
		}

		if(!tableInfo)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::arrangeForSprayingProductForAllStandardTableStencilForTableOption::tableInfo is NULL");	
			break;
		}

		InterfacePtr<ITagReader> itagReader
			((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader)
		{ 
			ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::arrangeForSprayingProductForAllStandardTableStencilForTableOption::!itagReader");	
			break;
		}
		TagList tList=itagReader->getTagsFromBox(boxUIDRef);
		int numTags=static_cast<int>(tList.size());
		if(numTags<0)
		{
			ptrIAppFramework->LogInfo("AP7_DataSprayer::CDataSprayer::arrangeForSprayingProductForAllStandardTableStencilForTableOption::numTags<0");	
			break;
		}

		int32 tableNameStartIndex = -1;
		int32 tableNameEndIndex = -1;
		int32 startPasteIndex = -1;

		SlugStruct tableStruct;

		for(int32 index = static_cast<int32> (tList.size()-1) ; index >=0 ;index--)
		{
			TagStruct & tagInfo=tList[index];
			PMString isTablePresent = tagInfo.tagPtr->GetAttributeValue(WideString("tableFlag"));
			if(isTablePresent == "0")
			{
				int32 startPos=-1;
				int32 endPos = -1;
				Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&startPos,&endPos);
				tableNameStartIndex = startPos;
				tableNameEndIndex = endPos + 1;
				startPasteIndex = tableNameEndIndex + 3;	//one for enter char and one for table
			}
			else
			{
				tableStruct.elementId = tagInfo.elementId ;
				tableStruct.typeId = tagInfo.typeId ;
				tableStruct.header = tagInfo.header;
				tableStruct.isEventField = tagInfo.isEventField ;
				tableStruct.deleteIfEmpty = tagInfo.deleteIfEmpty ;
				tableStruct.dataType = tagInfo.dataType ;
				tableStruct.isAutoResize = tagInfo.isAutoResize ;
				tableStruct.LanguageID = tagInfo.languageID ;
				tableStruct.whichTab = tagInfo.whichTab ;
				tableStruct.pbObjectId = tagInfo.pbObjectId ;
				tableStruct.parentId  = tagInfo.parentId ;
				tableStruct.childId = tagInfo.childId ;
				tableStruct.sectionID = tagInfo.sectionID ;
				tableStruct.parentTypeId = tagInfo.parentTypeID ;
				tableStruct.isSprayItemPerFrame = tagInfo.isSprayItemPerFrame ;
				tableStruct.catLevel = tagInfo.catLevel ;
				tableStruct.imgFlag = tagInfo.imgFlag ;
				tableStruct.imageIndex = tagInfo.imageIndex ;
				tableStruct.flowDir = tagInfo.flowDir ;
				tableStruct.childTag = tagInfo.childTag ;		
				tableStruct.tableFlag = tagInfo.isTablePresent ;
				tableStruct.tableType = tagInfo.tableType ;
				tableStruct.tableId = tagInfo.tableId ;
				tableStruct.row_no = tagInfo.rowno ;
				tableStruct.col_no = tagInfo.colno ;		
			}
		}

		CItemTableValue oTableValue;
		vector<double> vec_items;
		vector<double> vec_tableTypeID;
		VectorScreenTableInfoValue::iterator it;
		int32 i = 0,j=2;
		int32 destEnd = startPasteIndex;
		for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
		{	
			//CA("Table For");
			oTableValue = *it;
			vec_tableTypeID.push_back(oTableValue.getTableTypeID());

			if(it == tableInfo->begin())
				vec_items = oTableValue.getItemIds();
				
			if(vec_items.size() < 1)
				continue;

			if(i == 0)		//as we are not copying for first table
			{
				i++;
				continue;
			}
            //	Copy Paste Table Name tag
			InterfacePtr<ICommand> copyStoryRangeCmd(CmdUtils::CreateCommand(kCopyStoryRangeCmdBoss));
			if (copyStoryRangeCmd == nil) 
			{
				//CA("copyStoryRangeCmd == nil");
				break;
			}

			// Refer the command to the source story and range to be copied.
			InterfacePtr<IUIDData> sourceUIDData(copyStoryRangeCmd, UseDefaultIID());
			if (sourceUIDData == nil) 
			{
				//CA("sourceUIDData == nil");
				break;
			}
		
			sourceUIDData->Set(txtModelUIDRef);
			InterfacePtr<IRangeData> sourceRangeData(copyStoryRangeCmd, UseDefaultIID());
			if (sourceRangeData == nil) 
			{
				//CA("sourceRangeData == nil");
				break;
			}

			sourceRangeData->Set(tableNameStartIndex, tableNameEndIndex);

			// Refer the command to the destination story and the range to be replaced.
			UIDList itemList(txtModelUIDRef);
			copyStoryRangeCmd->SetItemList(itemList);

			WideString* newLineCharacter = new WideString("\r");
			textModel->Insert(startPasteIndex ,newLineCharacter);

			startPasteIndex++;
			destEnd++;

			InterfacePtr<IRangeData> destRangeData(copyStoryRangeCmd, IID_IRANGEDATA2);

			destRangeData->Set(startPasteIndex, destEnd );

			// Process CopyStoryRangeCmd
			ErrorCode status2 = CmdUtils::ProcessCommand(copyStoryRangeCmd);
			startPasteIndex = startPasteIndex + (tableNameEndIndex-tableNameStartIndex);
			destEnd = startPasteIndex;	
			j++;

            //going 4 copy paste table
			textModel->Insert(startPasteIndex ,newLineCharacter);

			startPasteIndex++;
			destEnd++;

			
			Utils<ITableUtils> tableUtils;
			if (!tableUtils) 
			{
				ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::arrangeForSprayingProductForAllStandardTableStencilForTableOption::!tableUtils");
				break;
			}
			tableUtils->InsertTable (textModel,
										startPasteIndex,
										0,
										2,
										2,
										17,//20,
										47,//50,
										kTextContentType,
										ITableUtils::eSetSelectionInFirstCell);

			UIDRef tableRef=( Utils<ITableUtils>()->GetTableModel(textModel, startPasteIndex));

			startPasteIndex = startPasteIndex+2;
			startPasteIndex = destEnd+2;

			XMLReference xmlRef;
			PMString tableName = "PSTable";
			tableName.AppendNumber(i);
			TableUtility objUtils(this);
			objUtils.AddTableAndCellElements(tableRef,boxUIDRef,tableName,"PSCell",xmlRef,0,tableStruct);

			j++;
			i++;

			if(newLineCharacter)
				delete newLineCharacter;
		}
        //Assigning currect typeID according to table to which it belongs
		TagList newTagList = itagReader->getTagsFromBox(boxUIDRef);

		int32 tableNameIndex = 0;
		int32 k = 0;
		for(int32 newTagIndex = 0;newTagIndex < newTagList.size();newTagIndex++)
		{
			TagStruct & newTagInfo = newTagList[newTagIndex];
			XMLReference newTagInfoXmlRef = newTagInfo.tagPtr->GetXMLReference();

			PMString attributeValue;
			attributeValue.AppendNumber(PMReal(vec_tableTypeID[tableNameIndex]));
			Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef,   WideString("typeId"),WideString(attributeValue));
			k++;
		
			if(k==2)
			{
				k=0;
				tableNameIndex++;
			}

		}
//////	Changing tags end

//////	Now going to atach AllStandard Table Tag
		PMString TagName = "All_Standard_Tables";
	
		ErrorCode err = kFailure;
		XMLReference CreatedElement;
		const XMLReference &  parentXMLR = kInvalidXMLReference; 
		err = Utils<IXMLElementCommands>()->CreateElement(WideString(TagName),txtModelUIDRef,
														0, startPasteIndex,
														parentXMLR,& CreatedElement);
		

		PMString attribName("ID");
		PMString attribVal("-1");
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
		
		attribName.Clear();
		attribVal.Clear();
		attribName = "typeId";
		attribVal = "-1";//"-116";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
		
		attribName.Clear();
		attribVal.Clear();
		attribName="header";
		attribVal="-1";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
		
		attribName.Clear();
		attribVal.Clear();
		attribName="isEventField";
		attribVal="-1";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

		attribName.Clear();
		attribVal.Clear();
		attribName="deleteIfEmpty";
		attribVal="-1";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
		
		attribName="dataType";
		attribVal="-1";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
		attribName.Clear();
		attribVal.Clear();

		attribName.Clear();
		attribVal.Clear();
		attribName="isAutoResize";
		attribVal="";
		attribVal.AppendNumber(tableStruct.isAutoResize);
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

		attribName.Clear();
		attribVal.Clear();
		attribName="LanguageID";
		attribVal="";
		attribVal.AppendNumber(PMReal(tableStruct.LanguageID));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

		attribName.Clear();
		attribVal.Clear();
		attribName = "index";
		attribVal = "3";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
		
		attribName.Clear();
		attribVal.Clear();
		attribName="pbObjectId";
		attribVal="-1";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
	
		attribName.Clear();
		attribVal.Clear();
		attribName="parentID";
		attribVal="-1";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

		attribName.Clear();
		attribVal.Clear();
		attribName="childId";
		attribVal="-1";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
	
		
		attribName.Clear();
		attribVal.Clear();
		attribName="sectionID";
		attribVal="-1";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
		
		attribName.Clear();
		attribVal.Clear();
		attribName="parentTypeID";
		attribVal="-1";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));


		attribName.Clear();
		attribVal.Clear();
		attribName="isSprayItemPerFrame";
		attribVal="-1";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

		attribName.Clear();
		attribVal.Clear();
		attribName="catLevel";
		attribVal="-1";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

		attribName.Clear();
		attribVal.Clear();
		attribName="imgFlag";
		attribVal="0";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

		attribName.Clear();
		attribVal.Clear();
		attribName="imageIndex";
		attribVal="-1";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

		attribName.Clear();
		attribVal.Clear();
		attribName="flowDir";
		attribVal="-1";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

		attribName.Clear();
		attribVal.Clear();
		attribName="childTag";
		attribVal="-1";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
		
		attribName.Clear();
		attribVal.Clear();
		attribName="tableFlag";
		attribVal="1";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
		
		attribName.Clear();
		attribVal.Clear();
		attribName="tableType";
		attribVal="7";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
		
		attribName.Clear();
		attribVal.Clear();
		attribName="tableId";
		attribVal="-1";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
		
		attribName.Clear();
		attribVal.Clear();
		attribName="rowno";
		attribVal="";
		attribVal.AppendNumber(PMReal(-1));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
		
		attribName.Clear();
		attribVal.Clear();
		attribName="colno";
		//attribVal="-101";
		attribVal.AppendNumber(PMReal(-1));
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
		
		attribName.Clear();
		attribVal.Clear();
		attribName="field1";
		attribVal="-1";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));	

		attribName.Clear();
		attribVal.Clear();
		attribName="field2";
		attribVal="-1";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));	

		attribName.Clear();
		attribVal.Clear();
		attribName="field3";
		attribVal="-1";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));	

		attribName.Clear();
		attribVal.Clear();
		attribName="field4";
		attribVal="-1";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));	

		attribName.Clear();
		attribVal.Clear();
		attribName="field5";
		attribVal="-1";
		err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));	
	
        attribName.Clear();
        attribVal.Clear();
        attribName="groupKey";
        attribVal="";
        err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));

		if(tableInfo)
			delete tableInfo;

//////////	End
		
		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
		for(int32 tagIndex1 = 0 ; tagIndex1 < newTagList.size() ; tagIndex1++)
		{
			newTagList[tagIndex1].tagPtr->Release();
		}
		
	}while(kFalse);
}
ErrorCode CDataSprayer::ReplaceText(ITextModel* textModel, const TextIndex position, const int32 length, const /*K2*/boost::shared_ptr<WideString>& text)
{
	//CA("CDataSprayer::ReplaceText");
	ErrorCode status = kFailure;
	do {

		ASSERT(textModel);
		if (!textModel) {
			//CA("!textModel");
			break;
		}
		if (position < 0 || position >= textModel->TotalLength()) {
			//CA("position invalid");
			break;
		}
		if (length < 0 || length >= textModel->TotalLength()) {
			//CA("length invalid");
			break;
		}
    	InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
    	ASSERT(textModelCmds);
    	if (!textModelCmds) {
			//CA("!textModelCmds");
			break;
		}


		InterfacePtr<ICommand> replaceCmd(textModelCmds->ReplaceCmd(position, length, text));
		ASSERT(replaceCmd);
		if (!replaceCmd) {
			//CA("!replaceCmd");
			break;
		}
		status = CmdUtils::ProcessCommand(replaceCmd);
		if(status != kSuccess)
		{
			//CA("status != kSuccess");
			break;
		}

		CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);

	} while(false);
	return status;
}
bool16 CDataSprayer::fillPVAndMPVImageInBox(const UIDRef& boxUIDRef, TagStruct& slugInfo, double parentId,bool16 isForTable, bool16 isSprayText)
{
	
	static PMString SectionSpryImagePath("");
	bool16 makeImageFitProportionally = kFalse;
	InterfacePtr<ITextModel> textModel;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}

	if(isInlineImage && isForTable==kFalse)
	{

        InterfacePtr<ITextModel>  tmptextModel;
        getTextModelByBoxUIDRef(boxUIDRef , tmptextModel);
		if (tmptextModel == NULL)
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::fillPVAndMPVImageInBox::!textModel");
			return kFalse;
		}
	    textModel = tmptextModel;

	}
	
	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{ 
		ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::!itagReader");	
		return kFalse;
	}

	InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	if(ptrIClientOptions==NULL)
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::!ptrIClientOptions");
		return kFalse;
	}

	imagePath=ptrIClientOptions->getImageDownloadPath();
    ptrIAppFramework->LogDebug("imagePath1 - " + imagePath);
	if(imagePath!="")
	{
		const char *imageP=imagePath.GetPlatformString().c_str();
		if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
			#ifdef MACINTOSH
				imagePath+="/";
			#else
				imagePath+="\\";
			#endif
	}

	if(imagePath=="")
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::Asset server not set");
		return 1;
	}
    ptrIAppFramework->LogDebug("imagePath2 - " + imagePath);

	PMString fileName("");
	double assetID;
	double mpv_value_id = -1;

	InterfacePtr<IScrapSuite> scrapSuite(static_cast<IScrapSuite*>(Utils<ISelectionUtils>()->QuerySuite(IID_ISCRAPSUITE)));
	if (scrapSuite == nil)
	{
		ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::CopySelectedItems:: scrapSuite invalid");
		return 1;
	}
	bool16 canClear = scrapSuite->CanDeselectAll();
	if(canClear == kTrue){
		scrapSuite->DeselectAll();	
	}	

	PMReal maxPageWidth,maxPageHeight;
	int numPages1=1;
	PMReal page1height,page1width;
	if(isInlineImage == kFalse)
	{
		do{
			
			InterfacePtr<ILayoutControlData> layoutData1(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
			if (layoutData1 == nil)
			{
				//CA("layoutData == nil");
				break;
			}
            
            layoutData1->SetFit(ILayoutControlData::kFitPage);
			UIDRef sprdref = layoutData1->GetSpreadRef();
			InterfacePtr<ISpread> iSpread1(sprdref, UseDefaultIID());
			if (iSpread1 == nil)
			{
				//CA("iSpread == nil");
				break;
			}
			numPages1=iSpread1->GetNumPages();
			UID page1UID1= iSpread1->GetNthPageUID(0);
			if(page1UID1 == kInvalidUID)
			{
				//CA("page1UID1 == kInvalidUID");		
				break;
			}	

			IDocument* document = layoutData1->GetDocument();
			if (document == nil)
			{
				//CA("No document");		
				break;
			}

			IDataBase* database = ::GetDataBase(document);
			if(!database)
			{
				//CA("No database");		
				break;
			}
			UIDRef current1PageUIDRef(database, page1UID1);
			
			InterfacePtr<IGeometry> pageGeometry(current1PageUIDRef, UseDefaultIID());
			if (pageGeometry == nil)
			{
				//CA("pageGeometry is invalid");
				break;
			}
			
			PMRect pageBoxBounds = pageGeometry->GetStrokeBoundingBox();

			page1width = pageBoxBounds.Right() - pageBoxBounds.Left();
			page1height = pageBoxBounds.Bottom() - pageBoxBounds.Top();
			if(numPages1 == 2)
			{
				//CA("Changing Margins");
				maxPageWidth = 3 * page1width;						
			}
			else
			{
				maxPageWidth = 2 * page1width;		
			}
			maxPageHeight = page1height + 72;
		}while(kFalse);
	}

	if(slugInfo.whichTab!=4)
	{
		
		VectorAssetValuePtr AssetValuePtrObj = NULL;
		if(slugInfo.whichTab == 5 && slugInfo.catLevel < 0)
		{
			double InputID = -1;
			double TagtypeId = -slugInfo.colno;

			InputID =	 ptrIAppFramework->getSectionIdByLevelAndEventId(slugInfo.catLevel ,CurrentSectionID, slugInfo.languageID);

			AssetValuePtrObj = ptrIAppFramework->GETAssets_GetPVMPVAssetByParentIdAndAttributeId(InputID,slugInfo.elementId, CurrentSectionID, slugInfo.languageID,3, slugInfo.typeId , slugInfo.imageIndex);
			if(AssetValuePtrObj == NULL)
			{
				return kFalse;				
			}
		}
		else if(slugInfo.whichTab == 5)
		{
			//int32 parentTypeID= ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_CLASS_TYPE");
			//int32 LevelClassId = ptrIAppFramework->GetONEsourceObjects_getCategoryClassIDByObjectIdLevelNo(parentId, slugInfo.catLevel/*colno*/, pNode.getIsProduct());
			//AssetValuePtrObj = ptrIAppFramework->GETAssets_GetPVMPVAssetByParentIdAndAttributeId(LevelClassId,slugInfo.elementId, CurrentSectionID, slugInfo.languageID,2,  slugInfo.typeId , slugInfo.imageIndex/*PVImageIndex*/);
			//if(AssetValuePtrObj == NULL)
			//{
			//	//CA("AssetValuePtrObj == NULL");
			//	
			//	return kFalse;
			//	
			//}
		}
		else if(slugInfo.whichTab == 3)
		{
			AssetValuePtrObj = ptrIAppFramework->GETAssets_GetPVMPVAssetByParentIdAndAttributeId(parentId,slugInfo.elementId, CurrentSectionID, slugInfo.languageID,1, slugInfo.typeId , slugInfo.imageIndex/*PVImageIndex*/);
		}
		if(AssetValuePtrObj == NULL)
		{			
			return kFalse;			
		}

		int32 sizeOfAssetValuePtrObj =static_cast<int32> (AssetValuePtrObj->size());

		if(AssetValuePtrObj->size() ==0)
		{
			ptrIAppFramework->LogInfo("AP7_DataSprayerModel::CDataSprayer::fillPVAndMPVImageInBox::AssetValuePtrObj->size() ==0");
			return kFalse;
		}
        VectorAssetValue::iterator it; // iterator of Asset value

        // Handle OutputAsSwatch - Here mpv_id is set in parentTypeId from ProductFinder.
        // Use mpv_id to populate only one image record in AssetValuePtrObj so that it will spray only one record at time.
        
        VectorAssetValuePtr OutputAsSwatchAssetValuePtrObj = NULL;
        
        if(slugInfo.parentTypeID != -1 && slugInfo.isSprayItemPerFrame == 3)
        {
            for(it = AssetValuePtrObj->begin();it!=AssetValuePtrObj->end();it++)
            {
                CAssetValue objCAssetvalue = *it;
                if(objCAssetvalue.getMpv_value_id() == slugInfo.parentTypeID )
                {
                    OutputAsSwatchAssetValuePtrObj = new VectorAssetValue;
                    OutputAsSwatchAssetValuePtrObj->push_back(objCAssetvalue);
                    break;
                }
            }
            if(AssetValuePtrObj)
                delete AssetValuePtrObj;
            AssetValuePtrObj = OutputAsSwatchAssetValuePtrObj;
        }
        
        if(AssetValuePtrObj->size() ==0)
		{
			ptrIAppFramework->LogInfo("AP7_DataSprayerModel::CDataSprayer::fillPVAndMPVImageInBox::OutputAsSwatchAssetValuePtrObj/AssetValuePtrObj->size() ==0");
			return kFalse;
		}
        
		slugInfo.parentId = parentId;
		
		if(global_project_level == 3)
			slugInfo.sectionID = CurrentSubSectionID;
		if(global_project_level == 2)
			slugInfo.sectionID = CurrentSectionID;

		InterfacePtr<IHierarchy> iHier(boxUIDRef, UseDefaultIID());
		if(!iHier)
		{
			ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::!iHier");		
			return kFalse;
		}
		
		UIDRef parentUIDRef(boxUIDRef.GetDataBase(), iHier->GetParentUID());
		InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{
			return kFalse;
		}
		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
		if (!layoutSelectionSuite) 
		{	//CA("!layoutSelectionSuite");
			return kFalse;
		}
		layoutSelectionSuite->DeselectAllPageItems();
		UIDList selectUIDList(boxUIDRef);
		layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);

		InterfacePtr<IGeometry> iGeo(boxUIDRef, IID_IGEOMETRY);
		if(!iGeo)
		{
			ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillImageInBox::!iGeo");		
			return kFalse;
		}

		PMRect pageBounds=iGeo->GetStrokeBoundingBox(InnerToPasteboardMatrix(iGeo));
		PMRect originalPageBounds;
		PMRect FirstFrameBounds = pageBounds;

		

		PMReal margin = 0;
		PMReal newMargin = 0;
		PMString TagName("");
		CAssetValue objCAssetvalue;
		PMString pickListValue("");
        PMString description("");
		
		vector <UIDRef> vecAssetUidRef;					
		VectorAssetValue::iterator it2ForInline;

		CAssetValue objCAssetvalueForInline;
		it2ForInline = AssetValuePtrObj->begin();
        
		for(it = AssetValuePtrObj->begin();it!=AssetValuePtrObj->end();it++)
		{
			objCAssetvalue = *it;
			fileName = objCAssetvalue.geturl();
			assetID = objCAssetvalue.getAsset_id();
			mpv_value_id = objCAssetvalue.getMpv_value_id();
            
            pickListValue = objCAssetvalue.getPickListValue();
            description = objCAssetvalue.getDescription();

			double typeId = slugInfo.typeId;
			
			slugInfo.parentTypeID = objCAssetvalue.getParent_type_id();
			//CA("fileName = "+fileName);			
			if(fileName=="")
				continue;
			
			UIDRef newItem;	
			do
			{
				SDKUtilities::Replace(fileName,"%20"," ");
			}while(fileName.IndexOfString("%20") != -1);

			//CA(fileName);
			PMString imagePathWithSubdir = imagePath;
			PMString typeCodeString;

            ptrIAppFramework->LogDebug("Image Path:  " + imagePathWithSubdir + fileName + "");
			if(!fileExists(imagePathWithSubdir,fileName))	
			{
				//CA("File is not exist");
				ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillPVAndMPVImageInBox::Image Not Found on- " + imagePathWithSubdir + fileName);
				continue; 
			}
			PMString total=imagePathWithSubdir+fileName;	
            #ifdef MACINTOSH
                SDKUtilities::convertToMacPath(total);
            #endif

			InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
			if (layoutData == nil)
				continue;
            
            layoutData->SetFit(ILayoutControlData::kFitPage);
		
			IDocument* document = layoutData->GetDocument();
			if (document == NULL)
				continue;

			IDataBase* database = ::GetDataBase(document);
			if(!database)
				continue;
			
			UID pageUID = layoutData->GetPage();
			if(pageUID == kInvalidUID)
				continue;

			UIDRef pageRef(database, pageUID);

			ThreadTextFrame ThreadObj;
			PMRect marginBoxBounds;
			ThreadObj .getMarginBounds (pageRef,marginBoxBounds);

			InterfacePtr<ITagReader> itagReader
			((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
			if(!itagReader)
			{
				ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::fillImageInBox::!itagReader");	
				continue;
			}

			if(FirstFrameBounds.Left() > marginBoxBounds.Right() )
			{
				UIDRef ref = layoutData->GetSpreadRef();
				InterfacePtr<ISpread> iSpread(ref, UseDefaultIID());
				if (iSpread == nil)
					continue;

				int numPages=iSpread->GetNumPages();
				
				pageUID= iSpread->GetNthPageUID(numPages-1);

				UIDRef pageRef(database, pageUID);
				ThreadObj .getMarginBounds (pageRef,marginBoxBounds);
			}

			if(margin != 0)
			{
				ptrIAppFramework->LogDebug("margin != 0 ");
				if(isInlineImage)
                {
					it2ForInline = it;

					int32 index=1;
					vector<UIDRef>::iterator imgUIDRefItr;
					imgUIDRefItr = vecAssetUidRef.begin();
																					
					for(int32 noOfTimes=static_cast<int32>(vecAssetUidRef.size() -1 );noOfTimes > 0;noOfTimes--)
					{
						//CA("In Import Looop");
						UIDRef  assetItem= vecAssetUidRef[index++];	
							
						//*****To Get the New Asset PAth *********
						if( noOfTimes != vecAssetUidRef.size() -1)
						{
							if(it != AssetValuePtrObj->end() )
							{
								bool16 found = kFalse;
								do{
									it2ForInline++;
									if(it2ForInline == AssetValuePtrObj->end())
										break;

									objCAssetvalueForInline = (*it2ForInline);
									fileName = objCAssetvalueForInline.geturl();
									//CA("fileName : "+fileName);
									if(fileName=="")
										continue;

									if(fileExists(imagePathWithSubdir,fileName))
										found = kTrue;			
										
								}while(found==kFalse);

								double nxtassetID = objCAssetvalueForInline.getAsset_id();

								if(fileName=="")
									continue;

								if(!fileExists(imagePathWithSubdir,fileName))
								{	
									ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::fillPVAndMPVImageInBox::Image Not Found on- " + imagePathWithSubdir + fileName);
									continue;
								}							
								
								PMString newtotal=imagePathWithSubdir+fileName;	
								total = newtotal;
                                
                                #ifdef MACINTOSH
                                    SDKUtilities::convertToMacPath(total);
                                #endif
						
								mpv_value_id = 	objCAssetvalueForInline.getMpv_value_id();

							}																				
						}
						
						if(ImportFileInFrame(assetItem,total))
						{	
							//CA("ImportFileInFrame");
						}

						if(isForTable == kTrue)
						{
							XMLReference cellXMLRef = slugInfo.tagPtr->GetParent();
							InterfacePtr<IIDXMLElement> cellXMLElement (cellXMLRef.Instantiate());
							if (cellXMLElement==NULL) 
							{
								//CA("cellXMLElement==NULL");	
								return kFalse;
							}
							
							XMLReference frameXMLRef = cellXMLElement->GetNthChild(index-1);
							InterfacePtr<IIDXMLElement> frameXMLElement (frameXMLRef.Instantiate());
							if (frameXMLElement==NULL) 
							{
								//CA("frameXMLElement==NULL");	
								return kFalse;
							}
							PMString attribVal;
							attribVal.AppendNumber(PMReal( slugInfo.parentId));
							
							if(frameXMLElement)
							{	
								TagName = frameXMLElement->GetTagString();
								frameXMLElement->SetAttributeValue(WideString("parentID"), WideString(attribVal));//ObjectID
								
								attribVal.Clear();							
								double pubId=slugInfo.sectionID;
								attribVal.AppendNumber(PMReal(pubId));
								frameXMLElement->SetAttributeValue(WideString("sectionID"),WideString(attribVal));//Publication ID
								
								attribVal.Clear();
								attribVal.AppendNumber(PMReal(slugInfo.parentTypeID));
								frameXMLElement->SetAttributeValue(WideString("parentTypeID"),WideString(attribVal));//Parent Type ID
								
								attribVal.Clear();
								attribVal.AppendNumber(PMReal(typeId));
								frameXMLElement->SetAttributeValue(WideString("typeId"),WideString(attribVal));
								
								attribVal.Clear();
								attribVal.AppendNumber(PMReal(mpv_value_id));
								frameXMLElement->SetAttributeValue(WideString("rowno"),WideString(attribVal));							
							}
						}
						else
						{
							TagList tList1 = itagReader->getTagsFromBox(assetItem);
							if(tList1.size() < 1)
							{
								//CA("tList1.size() < 1");
								continue;

							}
							TagStruct tagInfo1 = tList1[0];
							PMString attribVal;
							attribVal.AppendNumber(PMReal( slugInfo.parentId));
							
							if(tagInfo1.tagPtr )
							{	
								TagName = tagInfo1.tagPtr->GetTagString();
								tagInfo1.tagPtr->SetAttributeValue(WideString("parentID"), WideString(attribVal));//ObjectID
								
								attribVal.Clear();							
								double pubId=slugInfo.sectionID;
								attribVal.AppendNumber(PMReal(pubId));
								tagInfo1.tagPtr->SetAttributeValue(WideString("sectionID"), WideString(attribVal));//Publication ID
								
								attribVal.Clear();
								attribVal.AppendNumber(PMReal(slugInfo.parentTypeID));
								tagInfo1.tagPtr->SetAttributeValue(WideString("parentTypeID"), WideString(attribVal));//Parent Type ID
								
								attribVal.Clear();
								attribVal.AppendNumber(PMReal(typeId));
								tagInfo1.tagPtr->SetAttributeValue(WideString("typeId"),WideString(attribVal));
								
								attribVal.Clear();
								attribVal.AppendNumber(PMReal(mpv_value_id));
								tagInfo1.tagPtr->SetAttributeValue(WideString("rowno"),WideString(attribVal));								
							}

							for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
							{
								tList1[tagIndex].tagPtr->Release();
							}

						}					

					}
					//CA("Breaking Asset isInlineImage");
					//fitInlineImageInBoxInsideTableCell(boxUIDRef);
					isInlineImage = kFalse;
					break;
				}
				else
				{
					//CA("Not IsInLineImage ");
					if(slugInfo.flowDir == 0)	
					{//if(slugInfo.isAutoResize == -111)	
						//CA("isHorizontalFlow()==kTrue");
						pageBounds.Top(pageBounds.Top());
						pageBounds.Bottom(pageBounds.Bottom());
						pageBounds.Left(pageBounds.Left()+margin);
						pageBounds.Right(pageBounds.Right()+margin);
						if(pageBounds.Right() > marginBoxBounds.Right())
						{
							pageBounds = originalPageBounds ;
							pageBounds.Top(pageBounds.Top()+ newMargin);
							pageBounds.Bottom(pageBounds.Bottom()+newMargin);
							pageBounds.Left(pageBounds.Left());
							pageBounds.Right(pageBounds.Right());
							originalPageBounds = pageBounds;
						}
					}
					else
					{	
						//CA("isHorizontalFlow()==kFalse");
						pageBounds.Top(pageBounds.Top()+margin);
						pageBounds.Bottom(pageBounds.Bottom()+margin);
						pageBounds.Left(pageBounds.Left());
						pageBounds.Right(pageBounds.Right());
						if(pageBounds.Bottom() > marginBoxBounds.Bottom())
						{
							pageBounds = originalPageBounds ;
							pageBounds.Top(pageBounds.Top());
							pageBounds.Bottom(pageBounds.Bottom());
							pageBounds.Left(pageBounds.Left()+ newMargin);
							pageBounds.Right(pageBounds.Right()+ newMargin);
							originalPageBounds = pageBounds;
						}				
					}
	               if(numPages1 == 1){
						if(pageBounds.Right() > maxPageWidth)
						{
							ptrIAppFramework->LogDebug("Fill PV 2 going out of pasteboard co-ordinate 1");
							break;
						}
					}
					else{
						if( pageBounds.Right() > maxPageWidth)
						{
							ptrIAppFramework->LogDebug("going out of pasteboard co-ordinate 2");
							break;
						}						
					}					

					bool16 stat = copySelectedItems(newItem,pageBounds);
					if(!stat)
					{
						ptrIAppFramework->LogDebug("CDataSprayer:: fillImageInBox: copySelectedItems return kFalse");
						continue;
					}

					UIDList tempUIDLIst(newItem.GetDataBase(), newItem.GetUID());
					
					NewImageFrameUIDList.push_back(newItem.GetUID());
                    NewImageFrameUIDListForMap.push_back(newItem);
                    
					ptrIAppFramework->LogDebug("total : " + total);
					
                    TagList tList1 = itagReader->getTagsFromBox(newItem);
					if(tList1.size() < 1)
					{
						//CA("tList1.size() < 1");
						break;
                        
					}
					TagStruct tagInfo1 = tList1[0];
                    
                    if(isSprayText == kFalse)
                    {
                        if(ImportFileInFrame(newItem, total))
                        {
                            ptrIAppFramework->LogDebug("Imported");
                            //fitImageInBox(newItem);
                        }
                    }
                    else if (isSprayText)
                    {
                        InterfacePtr<ITextModel>  textModel2;
                        getTextModelByBoxUIDRef(newItem , textModel2);
                        if (textModel2 == NULL)
                        {
                            ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::!textModel2");
                            break;
                        }
                        
                        PMString textToInsert("");
                        if(tagInfo1.header == 1 && tagInfo1.dataType == 1)
                        {
                            textToInsert = pickListValue;
                        }
                        else if(tagInfo1.header == -1 && tagInfo1.dataType == 2)
                        {
                            textToInsert = description;
                        }
                        TextIndex startPos=-1;
                        TextIndex endPos = -1;
                        
                        int32 tagStartPos = 0;
                        int32 tagEndPos =0;
                        Utils<IXMLUtils>()->GetElementIndices(tagInfo1.tagPtr,&tagStartPos,&tagEndPos);
                        tagStartPos = tagStartPos + 1;
                        tagEndPos = tagEndPos -1;
                        
                        textToInsert.ParseForEmbeddedCharacters();
                        boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
                        ReplaceText(textModel2, tagStartPos,tagEndPos , insertText);
                        
                        if(tagInfo1.isAutoResize == 1)
                        {
                            // Fit the frame to the text content we placed.
                            InterfacePtr<ICommand> fitFrameToContentCmd(CmdUtils::CreateCommand(kFitFrameToContentCmdBoss));
                            ASSERT(fitFrameToContentCmd != nil);
                            if (fitFrameToContentCmd == nil) {
                                continue;
                            }
                            fitFrameToContentCmd->SetItemList(UIDList(boxUIDRef));
                            if (CmdUtils::ProcessCommand(fitFrameToContentCmd) != kSuccess) {
                                ASSERT_FAIL("kFitFrameToContentCmdBoss failed");
                                continue;
                            }
                        }
                    }
					
                    /// UpDating the Copied Tag
					
					PMString attribVal;
					attribVal.AppendNumber(PMReal( slugInfo.parentId));
					
					if(tagInfo1.tagPtr)
					{	
						TagName = tagInfo1.tagPtr->GetTagString();
						tagInfo1.tagPtr->SetAttributeValue(WideString("parentID"),WideString(attribVal));//ObjectID
						
						attribVal.Clear();							
						double pubId=slugInfo.sectionID;
						attribVal.AppendNumber(PMReal(pubId));
						tagInfo1.tagPtr->SetAttributeValue(WideString("sectionID"), WideString(attribVal));//Publication ID
						
						attribVal.Clear();
						attribVal.AppendNumber(PMReal(slugInfo.parentTypeID));
						tagInfo1.tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString( attribVal));//Parent Type ID
						
						attribVal.Clear();
						attribVal.AppendNumber(PMReal(typeId));
						tagInfo1.tagPtr->SetAttributeValue(WideString("typeId"),WideString(attribVal));
						
						attribVal.Clear();
						attribVal.AppendNumber(PMReal(mpv_value_id));
						tagInfo1.tagPtr->SetAttributeValue(WideString("rowno"),WideString(attribVal));				
					}

					for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
					{
						tList1[tagIndex].tagPtr->Release();
					}
				}
			}
			else
			{
				//CA("margin == 0 ");
				originalPageBounds = pageBounds;
                newItem = boxUIDRef;
				if(!isInlineImage && sizeOfAssetValuePtrObj > 1)
				{					
					InterfacePtr<IScrapSuite> scrapSuite(static_cast<IScrapSuite*>(Utils<ISelectionUtils>()->QuerySuite(IID_ISCRAPSUITE)));
					if (scrapSuite == nil)
					{
						ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::CopySelectedItems:: scrapSuite invalid");
						continue;
					}
					InterfacePtr<IClipboardController> clipController(GetExecutionContextSession(), UseDefaultIID());
					if (clipController == nil)
					{
						ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::CopySelectedItems::BscCltCore:: clipController invalid");
						continue;
					}

					InterfacePtr<IControlView> controlView ( Utils<ILayoutUIUtils>()->QueryFrontView());
					if (controlView == nil)
					{
						ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::CopySelectedItems:: controlView invalid");
						continue;
					}
	
					// Copy and paste the selection:
					if(scrapSuite->CanCopy(clipController) != kTrue){
						ptrIAppFramework->LogDebug("can't copy");
						continue;
					}
					if(scrapSuite->Copy(clipController) != kSuccess){
						ptrIAppFramework->LogDebug("Unable to copy");
						continue;
					}
					newItem = boxUIDRef;
				}
				if(!isInlineImage)
				{
                    if(isSprayText == kFalse)
                    {
                        if(ImportFileInFrame(newItem, total))
                        {
                            ptrIAppFramework->LogDebug("Imported");
                            //fitImageInBox(newItem);
                        }
                    }
                    else if (isSprayText)
                    {
                        InterfacePtr<ITextModel>  textModel2;
                        getTextModelByBoxUIDRef(newItem , textModel2);
                        if (textModel2 == NULL)
                        {
                            ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::!textModel2");
                            break;
                        }
                        
                        PMString textToInsert("");
                        if(slugInfo.header == 1 && slugInfo.dataType == 1)
                        {
                            textToInsert = pickListValue;
                        }
                        else if(slugInfo.header == -1 && slugInfo.dataType == 2)
                        {
                            textToInsert = description;
                        }
                        TextIndex startPos=-1;
                        TextIndex endPos = -1;
                        
                        int32 tagStartPos = 0;
                        int32 tagEndPos =0;
                        Utils<IXMLUtils>()->GetElementIndices(slugInfo.tagPtr,&tagStartPos,&tagEndPos);
                        tagStartPos = tagStartPos + 1;
                        tagEndPos = tagEndPos -1;
                        
                        textToInsert.ParseForEmbeddedCharacters();
                        boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
                        ReplaceText(textModel2, tagStartPos,tagEndPos , insertText);
                        
                        if(slugInfo.isAutoResize == 1)
                        {
                            // Fit the frame to the text content we placed.
                            InterfacePtr<ICommand> fitFrameToContentCmd(CmdUtils::CreateCommand(kFitFrameToContentCmdBoss));
                            ASSERT(fitFrameToContentCmd != nil);
                            if (fitFrameToContentCmd == nil) {
                                continue;
                            }
                            fitFrameToContentCmd->SetItemList(UIDList(boxUIDRef));
                            if (CmdUtils::ProcessCommand(fitFrameToContentCmd) != kSuccess) {
                                ASSERT_FAIL("kFitFrameToContentCmdBoss failed");
                                continue;
                            }
                        }
                    }
											
				}
				else if(isInlineImage)
				{
					//CA("isInlineImage ");
					XMLContentReference contentRef;
					UIDRef ContentRef;
					TextIndex startPos=-1;
					TextIndex endPos = -1;
					if(isForTable == kTrue)
					{
						//CA("isForTable == kTrue");
						//***** Here Befor Importing  First Image we Need To Do The Multiple Image Copy Paste 
						//** No of Times the total  Asset Image - 1 .
						XMLReference cellXMLRef = slugInfo.tagPtr->GetParent();
						InterfacePtr<IIDXMLElement> cellXMLElement (cellXMLRef.Instantiate());
						if (cellXMLElement==NULL) 
						{
							//CA("cellXMLElement==NULL");	
							return kFalse;
						}	
						InterfacePtr<ITextStoryThread> textStoryThread(cellXMLElement->QueryContentTextStoryThread());
						if(textStoryThread == NULL)
						{
							//CA("textStoryThread == NULL");
							return kFalse;
						}
						InterfacePtr<ITextModel> textModel1(textStoryThread->QueryTextModel());
						if(!textModel1)
						{
							//CA("!textModel1");
							return kFalse;
						}
						textModel = textModel1;
						XMLContentReference contentRef = cellXMLElement->GetContentReference();
						ContentRef = contentRef.GetUIDRef();

						startPos = textStoryThread->GetTextStart();													
						ErrorCode err = textModel1->CopyRange(startPos,1,pasteInLineData); 
						if(err ==  kFailure)
						{
							//CA("Fail To Copy");
							return kFalse;
						}
					}
					else
					{
						//***** Here Befor Importing  First Image we Need To Do The Multiple Image Copy Paste 
						//** No of Times the total  Asset Image - 1 .
						//CA("For tabbed text");
						contentRef = slugInfo.tagPtr->GetContentReference();
						ContentRef = contentRef.GetUIDRef();
						bool16 pos = Utils<IXMLUtils>()->GetElementIndices(slugInfo.tagPtr,&startPos,&endPos);														
						ErrorCode err = textModel->CopyRange(startPos,1,pasteInLineData); 
						if(err ==  kFailure)
						{
							//CA("Fail To Copy");
							return kFalse;
						}
					}					

					if(AssetValuePtrObj->size() > 1)
					{
						//**** Now Pest Selected																
						vecAssetUidRef.push_back(ContentRef);  //Adding First Image UIdRef
													
						int32 posi = 0;
						
						PMString cr;
						if(slugInfo.flowDir == 1 )
							cr.Append("\r");	
						else
							cr.Append(kTextChar_Space);

						WideString wstr(cr);
						int32 imgIndex =1;	
						VectorAssetValue::iterator it3ForInline;

						it3ForInline = it;
						
                        TagList tList;
						tList = itagReader->getTagsFromBox(boxUIDRef);

						for(int32 NumofAssetTimes=static_cast<int32>(AssetValuePtrObj->size()-1);NumofAssetTimes >0; NumofAssetTimes--)
						{
							//CA("In Loop... Of Pesting ");
							UIDRef  newItemUIdRef = kInvalidUIDRef;
							
							posi = posi + 2;
							it3ForInline++;

							if(it3ForInline == AssetValuePtrObj->end())
								break;
							
							TextIndex newPastePos = startPos + posi;
							
							if(NumofAssetTimes != AssetValuePtrObj->size()-1)
							{
								if(isForTable == kFalse)
								{
									textModel->CopyRange(startPos,1,pasteInLineData); 	
								}
								else
								{
									//CA("For Table");
									ErrorCode err1  = textModel->CopyRange(startPos,1,pasteInLineData); 	
									if(err1 ==  kFailure)
									{
										//CA("Fail To CopyRange	11");
										return kFalse;
									}
								}

							}
							
						
							//CA("Befor Inserting space");
							ErrorCode err1 = textModel->Insert(newPastePos-1,&wstr);
							if(err1 ==  kFailure)
							{
								//CA("Fail To Insert space");
								return kFalse;
							}
							//CA("After Insert space");

							fileName = it3ForInline->geturl();
							if(fileName!= ""){
								if(!fileExists(imagePathWithSubdir,fileName))
								{
									//CA("File is not exist");
									posi = posi -2;
									ptrIAppFramework->LogDebug("CDataSprayer:: fillImageInBox: Image Not Found on Local Drive");
									continue; 
								}
								//CA("Before Paste");
							
								InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
								InterfacePtr<ICommand> pasteCmd(textModelCmds->PasteCmd(newPastePos, pasteInLineData));
								if(!pasteCmd) 
								{
									//CA("AP7_DataSprayer::TableUtility::ChangeToInline::pasteCmd== nil");		
									return kFalse;;
								}
								ErrorCode status = CmdUtils::ProcessCommand(pasteCmd);
								if(status != kSuccess) 
								{
									//CA("AP7_DataSprayer::TableUtility::ChangeToInline::status != kSuccess");		
									return kFalse;;
								}
								fileName ="";
							}
							else{											
								fileName ="";
								posi=posi-2;
								continue;
							}
							
							if(isForTable == kFalse)
							{
								TagList newList=itagReader->getTagsFromBox(boxUIDRef);
								TagStruct newTagInfo;
								bool16 tagFound = kFalse;
								for(int32 i = 0; i < newList.size(); i++)
								{											
									tagFound = kFalse;
									for(int32 j = 0;j < tList.size() ; j++ )
									{												
										if(newList[i].tagPtr == tList[j].tagPtr)
										{
											tagFound = kTrue;
											break;
										}
									}
									if(tagFound == kFalse)
									{												
										newTagInfo = newList[i];																							
										it2ForInline++;

										objCAssetvalueForInline = *it2ForInline;
										
										double typeId = -1;
										if(slugInfo.typeId == -97)
										{							
											typeId = objCAssetvalueForInline.getType_id();																										
										}
										else
										{
											typeId = slugInfo.typeId;																									
										}

										PMString attributeName = "typeId";
										PMString attributeValue("");

										attributeValue.AppendNumber(PMReal(typeId));
										newTagInfo.tagPtr->SetAttributeValue(WideString(attributeName),WideString(attributeValue));
										break;
									}
								}
								if(newList.size() == tList.size())
								{	

									for(int32 tagIndex = 0 ; tagIndex < newList.size() ; tagIndex++)
									{
										newList[tagIndex].tagPtr->Release();
									}
									continue;
								}
								XMLContentReference newContentRef = newTagInfo.tagPtr->GetContentReference();																									
								UIDRef NewFrameUIDRef = newContentRef.GetUIDRef();			
								
								vecAssetUidRef.push_back(NewFrameUIDRef);
								tList=itagReader->getTagsFromBox(boxUIDRef); // Apsiva9

								for(int32 tagIndex = 0 ; tagIndex < newList.size() ; tagIndex++)
								{
									newList[tagIndex].tagPtr->Release();
								}
							}							
						}
						if(isForTable == kTrue)
						{
							vecAssetUidRef.clear();
							XMLReference cellXMLRef = slugInfo.tagPtr->GetParent();
							InterfacePtr<IIDXMLElement> cellXMLElement (cellXMLRef.Instantiate());
							if (cellXMLElement==NULL) 
							{
								//CA("cellXMLElement==NULL");	
								return kFalse;
							}
							int32 framecount = cellXMLElement->GetChildCount();
							for(int32 index = 0 ;index < framecount ; index++)
							{
								XMLReference freameXMLRef = cellXMLElement->GetNthChild(index);
								InterfacePtr<IIDXMLElement>frameXMLElementPtr(freameXMLRef.Instantiate());
								XMLContentReference cntentREF = frameXMLElementPtr->GetContentReference();
								UIDRef ref = cntentREF.GetUIDRef();
								vecAssetUidRef.push_back(ref);
							}
							
						}

						//
						for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
						{
							tList[tagIndex].tagPtr->Release();
						}
					}
					else if(AssetValuePtrObj->size() == 1)
					{
						if(isForTable == kTrue)
						{
							vecAssetUidRef.clear();
							XMLReference cellXMLRef = slugInfo.tagPtr->GetParent();
							InterfacePtr<IIDXMLElement> cellXMLElement (cellXMLRef.Instantiate());
							if (cellXMLElement==NULL) 
							{
								//CA("cellXMLElement==NULL");	
								return kFalse;
							}
							int32 framecount = cellXMLElement->GetChildCount();
							for(int32 index = 0 ;index < framecount ; index++)
							{
								XMLReference freameXMLRef = cellXMLElement->GetNthChild(index);
								InterfacePtr<IIDXMLElement>frameXMLElementPtr(freameXMLRef.Instantiate());
								XMLContentReference cntentREF = frameXMLElementPtr->GetContentReference();
								UIDRef ref = cntentREF.GetUIDRef();
								vecAssetUidRef.push_back(ref);
							}
							
						}
						else
							vecAssetUidRef.push_back(ContentRef);
					}

					//Here we are Importing the First Image
					//CA("total	:	"+total);
					if(ImportFileInFrame(vecAssetUidRef[0],total))
					{		
						//CA("Imported File In Frame");
						makeImageFitProportionally = kTrue; // For Fitting InLine Image Proportionally 
					}
					else
					{
						//CA("Fail to import");
						return kFalse;
					}
				}
				
				PMString attribVal;
				attribVal.AppendNumber(PMReal( slugInfo.parentId));
				
				if(slugInfo.tagPtr)
				{	
					TagName = slugInfo.tagPtr->GetTagString();
					slugInfo.tagPtr->SetAttributeValue(WideString("parentID"),WideString(attribVal));//ObjectID
					
					attribVal.Clear();							
					double pubId=slugInfo.sectionID;
					attribVal.AppendNumber(PMReal(pubId));
					slugInfo.tagPtr->SetAttributeValue(WideString("sectionID"),WideString(attribVal));//Publication ID
					
					attribVal.Clear();
					attribVal.AppendNumber(PMReal(slugInfo.parentTypeID));
					slugInfo.tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attribVal));//Parent Type ID

					attribVal.Clear();
					attribVal.AppendNumber(PMReal(typeId));
					slugInfo.tagPtr->SetAttributeValue(WideString("typeId"),WideString(attribVal));
					
					attribVal.Clear();
					attribVal.AppendNumber(PMReal(mpv_value_id));
					slugInfo.tagPtr->SetAttributeValue(WideString("rowno"),WideString(attribVal));
				}
			}

			if(slugInfo.flowDir == 0)
			{//if(slugInfo.isAutoResize == -111)
				margin = pageBounds.Right() -pageBounds.Left() +2;
				newMargin = pageBounds.Bottom() - pageBounds.Top() +2;
			}
			else
			{
				margin = pageBounds.Bottom() -pageBounds.Top() +2;
				newMargin = pageBounds.Right() - pageBounds.Left() +2;
			}			
		}

		if(AssetValuePtrObj)
			delete AssetValuePtrObj;
	}//end of if slugInfo.whichTab != 4
	else
	{
		//CA("slugInfo.whichTab == 4");
		do
		{
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == NULL)
			{
				CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
				break;
			}

			InterfacePtr<IHierarchy> iHier(boxUIDRef, UseDefaultIID());
			if(!iHier)
			{
				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:!iHier");				
				break;
			}

			UIDRef parentUIDRef(boxUIDRef.GetDataBase(), iHier->GetParentUID());
			InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
			if(!iSelectionManager)
			{
				break;
			}
	
			InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
			if (!layoutSelectionSuite) 
			{	//CA("!layoutSelectionSuite");
				break;
			}

			layoutSelectionSuite->DeselectAllPageItems();
			UIDList selectUIDList(boxUIDRef);
			layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);

			InterfacePtr<IGeometry> iGeo(boxUIDRef, IID_IGEOMETRY);
			if(!iGeo)
			{
				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:!iGeo");								
				break;
			}

			PMRect pageBounds=iGeo->GetStrokeBoundingBox(InnerToPasteboardMatrix(iGeo));
			PMRect FirstFrameBounds = pageBounds;
			VectorLongIntValue::iterator it;
			PMRect originalPageBounds;
			PMReal margin = 0;
			PMReal newMargin = 0;
			PMString TagName("");
			int32 Flag=0;

			bool16 ItemImageUnspreadFlag = kFalse;
		
			UIDRef newItem;	

			XMLReference tagRef = slugInfo.tagPtr->GetXMLReference();
			XMLTagAttributeValue tagVal;
			convertTagStructToXMLTagAttributeValue(slugInfo,tagVal);

			InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
			if(ptrIClientOptions==NULL)
			{
				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:!ptrIClientOptions");
				break;
			}

			PMString imagePath=ptrIClientOptions->getImageDownloadPath();

			if(imagePath=="")
			{	
				//CA("imagePath");
				ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:imagePath == blank");					
				break;
			}

			if(imagePath!="")
			{
				const char *imageP=imagePath.GetPlatformString().c_str();
				if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
				#ifdef MACINTOSH
					imagePath+="/";
				#else
					imagePath+="\\";
				#endif
			}


			PMString fileName("");
			double mpv_value_id = -1;

            if(slugInfo.parentTypeID != -1  &&  slugInfo.isSprayItemPerFrame != 3)
            {
                //CA("This was set for SprayItemPerFrame  ***************");
                slugInfo.parentId = (slugInfo.parentTypeID);
                parentId = (slugInfo.parentTypeID);
            }
            else
            {
              slugInfo.parentId= parentId;
            }
            
            //ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
			slugInfo.sectionID=CurrentSubSectionID;

			VectorAssetValuePtr AssetValuePtrObj = NULL;
			if(slugInfo.elementId == -95)
			{
				//AssetValuePtrObj = ptrIAppFramework->GetMPVAssetByBaseModelNumberForWP_EventItemSwatchImage(CurrentSectionID , pNode.getPubId());
				//if(AssetValuePtrObj == NULL){
				//	//CA("AssetValuePtrObj == NULL");
				//	ItemImageUnspreadFlag = kTrue;
				//	continue ;
				//}

			}
			else
			{
				AssetValuePtrObj = ptrIAppFramework->GETAssets_GetPVMPVAssetByParentIdAndAttributeId(parentId,slugInfo.elementId, CurrentSectionID, slugInfo.languageID,0, slugInfo.typeId, slugInfo.imageIndex);
				if(AssetValuePtrObj == NULL){
					ItemImageUnspreadFlag = kTrue;
					continue ;
				}
			}

			int32 sizeOfAssetValuePtrObj =static_cast<int32> (AssetValuePtrObj->size());
			
			PMString tempstr = "";
			tempstr.AppendNumber(sizeOfAssetValuePtrObj);
            
            VectorAssetValue::iterator it1; // iterator of Asset value
            // Handle OutputAsSwatch - Here mpv_id is set in parentTypeId from ProductFinder.
            // Use mpv_id to populate only one image record in AssetValuePtrObj so that it will spray only one record at time.
            
            VectorAssetValuePtr OutputAsSwatchAssetValuePtrObj = NULL;
            
            if(slugInfo.parentTypeID != -1 && slugInfo.isSprayItemPerFrame == 3)
            {
                for(it1 = AssetValuePtrObj->begin();it1!=AssetValuePtrObj->end();it1++)
                {
                    CAssetValue objCAssetvalue = *it1;
                    if(objCAssetvalue.getMpv_value_id() == slugInfo.parentTypeID )
                    {
                        OutputAsSwatchAssetValuePtrObj = new VectorAssetValue;
                        OutputAsSwatchAssetValuePtrObj->push_back(objCAssetvalue);
                        break;
                    }
                }
                if(AssetValuePtrObj)
                {
                    delete AssetValuePtrObj;
                    AssetValuePtrObj= NULL;
                    
                }
                AssetValuePtrObj = OutputAsSwatchAssetValuePtrObj;
            }

            
            if( AssetValuePtrObj != NULL && AssetValuePtrObj->size() == 0)
			{
				ItemImageUnspreadFlag = kTrue;
				continue;
			}
            
            sizeOfAssetValuePtrObj =static_cast<int32> (AssetValuePtrObj->size());

            //set it after selecting image for use case outputAsSwatch
            slugInfo.parentTypeID = pNode.getTypeId();
			
			vector <UIDRef> vecAssetUidRef;	

			CAssetValue objCAssetvalue;
			double tempTypeid = -1;
            PMString pickListValue("");
            PMString description("");

			VectorAssetValue::iterator it2ForInline;

			CAssetValue objCAssetvalueForInline;
			it2ForInline = AssetValuePtrObj->begin();

			for(it1 = AssetValuePtrObj->begin();it1 != AssetValuePtrObj->end();it1++)
			{
				//CA("For Loop");
				objCAssetvalue = *it1;
				fileName = objCAssetvalue.geturl();
				mpv_value_id = objCAssetvalue.getMpv_value_id();
				
                pickListValue = objCAssetvalue.getPickListValue();
                description = objCAssetvalue.getDescription();
                
				double typeId = -1;
				typeId = slugInfo.typeId;
				tempTypeid = typeId;
				
				if(fileName=="")
					continue;

				UIDRef newItem;
				do
				{
					SDKUtilities::Replace(fileName,"%20"," ");
				}while(fileName.IndexOfString("%20") != -1);
				PMString imagePathWithSubdir = imagePath;

				if(!fileExists(imagePathWithSubdir,fileName))
				{
					//CA("Image Not Found on Local Drive");
					continue;
				}

				PMString total=imagePathWithSubdir+fileName;
                #ifdef MACINTOSH
                    SDKUtilities::convertToMacPath(total);
                #endif

				InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
				if (layoutData == nil)
					break;

				layoutData->SetFit(ILayoutControlData::kFitPage);

				IDocument* document = layoutData->GetDocument();
				if (document == NULL)
					break;

				IDataBase* database = ::GetDataBase(document);
				if(!database)
					break;

				UID pageUID = layoutData->GetPage();
				if(pageUID == kInvalidUID)
					break;

				UIDRef pageRef(database, pageUID);

				ThreadTextFrame ThreadObj;
				PMRect marginBoxBounds;
				ThreadObj .getMarginBounds (pageRef,marginBoxBounds);

				if(FirstFrameBounds.Left() > marginBoxBounds.Right() )
				{
					UIDRef ref = layoutData->GetSpreadRef();
					InterfacePtr<ISpread> iSpread(ref, UseDefaultIID());
					if (iSpread == nil)
						break;

					int numPages=iSpread->GetNumPages();
					
					pageUID= iSpread->GetNthPageUID(numPages-1);

					UIDRef pageRef(database, pageUID);
					ThreadObj .getMarginBounds (pageRef,marginBoxBounds);
				}

				if(margin != 0 )
				{
					if(isInlineImage)
					{
						it2ForInline = it1;
						int32 index=1;
						vector<UIDRef>::iterator imgUIDRefItr;
						imgUIDRefItr = vecAssetUidRef.begin();
																						
						for(int32 noOfTimes=static_cast<int32>(vecAssetUidRef.size() -1) ;noOfTimes > 0;noOfTimes--)
						{
							//CA("In Import Looop");
							UIDRef  assetItem= vecAssetUidRef[index++];	
								
							//*****To Get the New Asset PAth *********
							if( noOfTimes != vecAssetUidRef.size() -1)
							{
								if(it1 != AssetValuePtrObj->end() ){
									bool16 found = kFalse;
									do{
										it2ForInline++;
										if(it2ForInline == AssetValuePtrObj->end())
											break;

										objCAssetvalueForInline = (*it2ForInline);
										fileName = objCAssetvalueForInline.geturl();

										if(fileName=="")
											continue;

										if(fileExists(imagePathWithSubdir,fileName))
											found = kTrue;			

									}while(found==kFalse);

									double nxtassetID = objCAssetvalueForInline.getAsset_id();
									
									mpv_value_id = objCAssetvalueForInline.getMpv_value_id();
									PMString axd("fileName = ");
									
									if(fileName=="")
										continue;

									if(!fileExists(imagePathWithSubdir,fileName))
									{	
										//CA("File not exist for margin != 0");
										continue;
									}

									PMString newtotal=imagePathWithSubdir+fileName;	
									total = newtotal;
                                    
                                    #ifdef MACINTOSH
                                        SDKUtilities::convertToMacPath(total);
                                    #endif
                                    
								}																				
							}
						
							if(ImportFileInFrame(assetItem,/*new*/total))
							{	
								//CA(" Imported File In Frame ");
								//Flag=1;										
								//ItemImageUnspreadFlag = kFalse;								
							}
							
							if(isForTable == kTrue)
							{
								XMLReference cellXMLRef = slugInfo.tagPtr->GetParent();
								InterfacePtr<IIDXMLElement> cellXMLElement (cellXMLRef.Instantiate());
								if (cellXMLElement==NULL) 
								{
									//CA("cellXMLElement==NULL");	
									return kFalse;
								}
								
								XMLReference frameXMLRef = cellXMLElement->GetNthChild(index-1);
								InterfacePtr<IIDXMLElement> frameXMLElement (frameXMLRef.Instantiate());
								if (frameXMLElement==NULL) 
								{
									//CA("frameXMLElement==NULL");	
									return kFalse;
								}
								PMString attribVal;
								attribVal.AppendNumber(PMReal( slugInfo.parentId));
								
								if(frameXMLElement)
								{	
									TagName = frameXMLElement->GetTagString();
									frameXMLElement->SetAttributeValue(WideString("parentID"), WideString(attribVal));//ObjectID
									
									attribVal.Clear();							
									double pubId=slugInfo.sectionID;
									attribVal.AppendNumber(PMReal(pubId));
									frameXMLElement->SetAttributeValue(WideString("sectionID"), WideString(attribVal));//Publication ID
									
									attribVal.Clear();
									attribVal.AppendNumber(PMReal(slugInfo.parentTypeID));
									frameXMLElement->SetAttributeValue(WideString("parentTypeID"), WideString(attribVal));//Parent Type ID
									
									attribVal.Clear();
									attribVal.AppendNumber(PMReal(typeId));
									frameXMLElement->SetAttributeValue(WideString("typeId"),WideString(attribVal));
									
									attribVal.Clear();
									attribVal.AppendNumber(PMReal(mpv_value_id));
									frameXMLElement->SetAttributeValue(WideString("rowno"),WideString(attribVal));								
																
								}
							}
							else
							{
								TagList tList1 = itagReader->getTagsFromBox(assetItem);
								if(tList1.size() < 1)
								{
									continue;
								}
								TagStruct tagInfo1 = tList1[0];
								PMString attribVal;
								attribVal.AppendNumber(PMReal( slugInfo.parentId));
								
								if(tagInfo1.tagPtr )
								{	
									TagName = tagInfo1.tagPtr->GetTagString();
									tagInfo1.tagPtr->SetAttributeValue(WideString("parentID"), WideString(attribVal));//ObjectID
									
									attribVal.Clear();							
									double pubId=slugInfo.sectionID;
									attribVal.AppendNumber(PMReal(pubId));
									tagInfo1.tagPtr->SetAttributeValue(WideString("sectionID"), WideString(attribVal));//Publication ID
									
									attribVal.Clear();
									attribVal.AppendNumber(PMReal(slugInfo.parentTypeID));
									tagInfo1.tagPtr->SetAttributeValue(WideString("parentTypeID"), WideString(attribVal));//Parent Type ID
									
									attribVal.Clear();
									attribVal.AppendNumber(PMReal(typeId));
									tagInfo1.tagPtr->SetAttributeValue(WideString("typeId"),WideString(attribVal));
									
									attribVal.Clear();
									attribVal.AppendNumber(PMReal(mpv_value_id));
									tagInfo1.tagPtr->SetAttributeValue(WideString("rowno"),WideString(attribVal));
									
								}

								for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
								{
									tList1[tagIndex].tagPtr->Release();
								}

							}
						}
						//CA("Breaking Asset isInlineImage");
						//fitInlineImageInBoxInsideTableCell(boxUIDRef);
						isInlineImage = kFalse;
						break;
					}
					else
					{
						//CA("!isInlineImage");
						if(slugInfo.flowDir == 0)
						{//if(slugInfo.isAutoResize == -111)
							pageBounds.Top(pageBounds.Top());
							pageBounds.Bottom(pageBounds.Bottom());
							pageBounds.Left(pageBounds.Left()+margin);
							pageBounds.Right(pageBounds.Right()+margin);
							if(pageBounds.Right() > marginBoxBounds.Right())
							{
								pageBounds = originalPageBounds ;
								pageBounds.Top(pageBounds.Top()+ newMargin);
								pageBounds.Bottom(pageBounds.Bottom()+newMargin);
								pageBounds.Left(pageBounds.Left());
								pageBounds.Right(pageBounds.Right());
								originalPageBounds = pageBounds;
							}
						}
						else
						{
							pageBounds.Top(pageBounds.Top()+margin);
							pageBounds.Bottom(pageBounds.Bottom()+margin);
							pageBounds.Left(pageBounds.Left());
							pageBounds.Right(pageBounds.Right());
							if(pageBounds.Bottom() > marginBoxBounds.Bottom())
							{
								pageBounds = originalPageBounds ;
								pageBounds.Top(pageBounds.Top());
								pageBounds.Bottom(pageBounds.Bottom());
								pageBounds.Left(pageBounds.Left()+ newMargin);
								pageBounds.Right(pageBounds.Right()+ newMargin);
								originalPageBounds = pageBounds;
							}

						}
						
						if(numPages1 == 1){
							if(pageBounds.Right()  > maxPageWidth)
							{
								ptrIAppFramework->LogDebug("FillPV1 going out of pasteboard co-ordinate 1");
								break;
							}
						}
						else{
							if(pageBounds.Right() > maxPageWidth)
							{
								ptrIAppFramework->LogDebug("going out of pasteboard co-ordinate 2");
								break;
							}						
						}

						bool16 stat = copySelectedItems(newItem,pageBounds);
						if(!stat)
						{
							//CA("Unable To Copy");
							break;
						}	

						UIDList tempUIDLIst(newItem.GetDataBase(),newItem.GetUID());

						NewImageFrameUIDList.push_back( newItem.GetUID());
                        NewImageFrameUIDListForMap.push_back(newItem);
                        
                        TagList tList1 = itagReader->getTagsFromBox(newItem);
						TagStruct tagInfo1 = tList1[0];
						
                        if(isSprayText == kFalse)
                        {
                            if(ImportFileInFrame(newItem, total))
                            {
                                ptrIAppFramework->LogDebug("Imported");
                                Flag=1;
                            }
                        }
                        else if (isSprayText)
                        {
                            InterfacePtr<ITextModel>  textModel2;
                            getTextModelByBoxUIDRef(newItem , textModel2);
                            if (textModel2 == NULL)
                            {
                                ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::!textModel2");
                                break;
                            }
                            
                            PMString textToInsert("");
                            if(tagInfo1.header == 1 && tagInfo1.dataType == 1)
                            {
                                textToInsert = pickListValue;
                            }
                            else if(tagInfo1.header == -1 && tagInfo1.dataType == 2)
                            {
                                textToInsert = description;
                            }
                            TextIndex startPos=-1;
                            TextIndex endPos = -1;
                            
                            int32 tagStartPos = 0;
                            int32 tagEndPos =0;
                            Utils<IXMLUtils>()->GetElementIndices(tagInfo1.tagPtr,&tagStartPos,&tagEndPos);
                            tagStartPos = tagStartPos + 1;
                            tagEndPos = tagEndPos -1;
                            
                            textToInsert.ParseForEmbeddedCharacters();
                            boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
                            ReplaceText(textModel2, tagStartPos,tagEndPos , insertText);
                            
                            if(tagInfo1.isAutoResize == 1)
                            {
                                // Fit the frame to the text content we placed.
                                InterfacePtr<ICommand> fitFrameToContentCmd(CmdUtils::CreateCommand(kFitFrameToContentCmdBoss));
                                ASSERT(fitFrameToContentCmd != nil);
                                if (fitFrameToContentCmd == nil) {
                                    continue;
                                }
                                fitFrameToContentCmd->SetItemList(UIDList(boxUIDRef));
                                if (CmdUtils::ProcessCommand(fitFrameToContentCmd) != kSuccess) {
                                    ASSERT_FAIL("kFitFrameToContentCmdBoss failed");
                                    continue;
                                }
                            }
                        }

						/// UpDating the Copied Tag					
						

						PMString attribVal;
						attribVal.AppendNumber(PMReal(slugInfo.parentId));
				
						if(tagInfo1.tagPtr)
						{	
							TagName = tagInfo1.tagPtr->GetTagString();
							tagInfo1.tagPtr->SetAttributeValue(WideString("parentID"),WideString(attribVal));//ObjectID
							
							attribVal.Clear();							
							double pubId=slugInfo.sectionID;
							attribVal.AppendNumber(PMReal(pubId));
							tagInfo1.tagPtr->SetAttributeValue(WideString("sectionID"),WideString(attribVal));//Publication ID
							
							attribVal.Clear();
							attribVal.AppendNumber(PMReal(slugInfo.parentTypeID));
							tagInfo1.tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attribVal));//Parent Type ID
							
							attribVal.Clear();
							attribVal.AppendNumber(PMReal(typeId));
							tagInfo1.tagPtr->SetAttributeValue(WideString("typeId"),WideString(attribVal));
							
							attribVal.Clear();
							attribVal.AppendNumber(PMReal(mpv_value_id));
							tagInfo1.tagPtr->SetAttributeValue(WideString("rowno"),WideString(attribVal));
							
						}

						for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
						{
							tList1[tagIndex].tagPtr->Release();
						}
					}


				}
				else
				{
					//CA("margin == 0");
					originalPageBounds = pageBounds;
                    newItem = boxUIDRef;
					if(!isInlineImage && sizeOfAssetValuePtrObj > 1)
					{
						InterfacePtr<IScrapSuite> scrapSuite(static_cast<IScrapSuite*>(Utils<ISelectionUtils>()->QuerySuite(IID_ISCRAPSUITE)));
						if (scrapSuite == NULL)
						{
							ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::CopySelectedItems:: scrapSuite invalid");
							continue;
						}
						InterfacePtr<IClipboardController> clipController(GetExecutionContextSession(), UseDefaultIID());
						if (clipController == NULL)
						{
							ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::CopySelectedItems::BscCltCore:: clipController invalid");
							continue;
						}

						InterfacePtr<IDataExchangeHandler> dataExchangeHandlerPtr ( clipController->QueryActiveScrapHandler());
						if(dataExchangeHandlerPtr != NULL)
							dataExchangeHandlerPtr->Clear();

						InterfacePtr<IControlView> controlView(Utils<ILayoutUIUtils>()->QueryFrontView());
						if (controlView == nil)
						{
							ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::CopySelectedItems:: controlView invalid");
							continue;
						}
							
						// Copy  the selection:
						if(scrapSuite->CanCopy(clipController) != kTrue)
							continue;
						if(scrapSuite->Copy(clipController) != kSuccess)
							continue;

						newItem = boxUIDRef;
					}
					//if((!isInlineImage) && ImportFileInFrame(newItem, total))
					//{
					//	//CA("Image Imported");
					//	Flag=1;
					//	//fitImageInBox(boxUIDRef);
					//}
                    
                    if(!isInlineImage)
                    {
                        if(isSprayText == kFalse)
                        {
                            if(ImportFileInFrame(newItem, total))
                            {
                                ptrIAppFramework->LogDebug("Imported");
                                //fitImageInBox(newItem);
                            }
                        }
                        else if (isSprayText)
                        {
                            InterfacePtr<ITextModel>  textModel2;
                            getTextModelByBoxUIDRef(newItem , textModel2);
                            if (textModel2 == NULL)
                            {
                                ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::!textModel2");
                                break;
                            }
                            
                            PMString textToInsert("");
                            if(slugInfo.header == 1 && slugInfo.dataType == 1)
                            {
                                textToInsert = pickListValue;
                            }
                            else if(slugInfo.header == -1 && slugInfo.dataType == 2)
                            {
                                textToInsert = description;
                            }
                            TextIndex startPos=-1;
                            TextIndex endPos = -1;
                            
                            int32 tagStartPos = 0;
                            int32 tagEndPos =0;
                            Utils<IXMLUtils>()->GetElementIndices(slugInfo.tagPtr,&tagStartPos,&tagEndPos);
                            tagStartPos = tagStartPos + 1;
                            tagEndPos = tagEndPos -1;
                            
                            textToInsert.ParseForEmbeddedCharacters();
                            boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
                            ReplaceText(textModel2, tagStartPos,tagEndPos , insertText);
                            
                            if(slugInfo.isAutoResize == 1)
                            {
                                // Fit the frame to the text content we placed.
                                InterfacePtr<ICommand> fitFrameToContentCmd(CmdUtils::CreateCommand(kFitFrameToContentCmdBoss));
                                ASSERT(fitFrameToContentCmd != nil);
                                if (fitFrameToContentCmd == nil) {
                                    continue;
                                }
                                fitFrameToContentCmd->SetItemList(UIDList(boxUIDRef));
                                if (CmdUtils::ProcessCommand(fitFrameToContentCmd) != kSuccess) {
                                    ASSERT_FAIL("kFitFrameToContentCmdBoss failed");
                                    continue;
                                }
                            }
                        }
                        
                    }
					else if(isInlineImage)
					{
						XMLContentReference contentRef;
						UIDRef ContentRef;
						//Here We Are Doing Copy Paste of Image Box
						TextIndex startPos=-1;
						TextIndex endPos = -1;	
						
						if(isForTable == kTrue)
						{
							// Here Befor Importing  First Image we Need To Do The Multiple Image Copy Paste
							XMLReference cellXMLRef = slugInfo.tagPtr->GetParent();
							InterfacePtr<IIDXMLElement> cellXMLElement (cellXMLRef.Instantiate());
							if (cellXMLElement==NULL) 
							{
								//CA("cellXMLElement==NULL");	
								continue;
							}	
							InterfacePtr<ITextStoryThread> textStoryThread(cellXMLElement->QueryContentTextStoryThread());
							if(textStoryThread == NULL)
							{
								//CA("textStoryThread == NULL");
								continue;
							}
							InterfacePtr<ITextModel> textModel1(textStoryThread->QueryTextModel());
							if(!textModel1)
							{
								//CA("!textModel1");
								continue;
							}
							textModel = textModel1;
							XMLContentReference contentRef = cellXMLElement->GetContentReference();
							ContentRef = contentRef.GetUIDRef();

							startPos = textStoryThread->GetTextStart();													
							ErrorCode err = textModel1->CopyRange(startPos,1,pasteInLineData); 
							if(err ==  kFailure)
							{
								//CA("Fail To Copy");
								continue;
							}
						}
						else
						{
							// Here Befor Importing  First Image we Need To Do The Multiple Image Copy Paste
							contentRef = slugInfo.tagPtr->GetContentReference();
							ContentRef = contentRef.GetUIDRef();
							bool16 pos = Utils<IXMLUtils>()->GetElementIndices(slugInfo.tagPtr,&startPos,&endPos);														
							ErrorCode err = textModel->CopyRange(startPos,1,pasteInLineData); 
							if(err ==  kFailure)
							{
								return kFalse;
							}
						}

					
						if(AssetValuePtrObj->size() > 1)
						{
							// Now Pest Selected
							vecAssetUidRef.push_back(ContentRef);  //Adding First Image UIdRef
														
							int32 posi = 0;
							
							PMString cr;
							if(slugInfo.flowDir == 1 )
								cr.Append("\r");	
							else
								cr.Append(kTextChar_Space);

							WideString wstr(cr);
							int32 imgIndex =1;	
							VectorAssetValue::iterator it3ForInline;
							it3ForInline = it1;
	
							TagList tList;
							tList = itagReader->getTagsFromBox(boxUIDRef);

							for(int32 NumofAssetTimes=static_cast<int32>(AssetValuePtrObj->size()-1);NumofAssetTimes >0; NumofAssetTimes--)
							{
								UIDRef  newItemUIdRef = kInvalidUIDRef;
								posi = posi + 2;
							
								it3ForInline++;

								if(it3ForInline == AssetValuePtrObj->end())
									break;
								
								TextIndex newPastePos = startPos + posi;
								
								if(NumofAssetTimes != AssetValuePtrObj->size()-1)
								{
									if(isForTable == kFalse)
									{
										textModel->CopyRange(startPos,1,pasteInLineData); 
									}
									else
									{
										ErrorCode err1  = textModel->CopyRange(startPos,1,pasteInLineData); 	
										if(err1 ==  kFailure)
										{
											return kFalse;
										}
									}

								}

								textModel->Insert(newPastePos-1,&wstr);

								fileName = it3ForInline->geturl();
								if(fileName!= ""){
									
									if(!fileExists(imagePathWithSubdir,fileName))
									{
										posi = posi -2;
										continue;
									}
									
									InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
									InterfacePtr<ICommand> pasteCmd(textModelCmds->PasteCmd(newPastePos, pasteInLineData));
									if(!pasteCmd) 
									{
										return kFalse;;
									}
									ErrorCode status = CmdUtils::ProcessCommand(pasteCmd);
									if(status != kSuccess) 
									{
										return kFalse;;
									}
									fileName ="";
								}
								else{											
									fileName ="";
									posi=posi-2;
									continue;
								}

								if(isForTable == kFalse)
								{
									TagList newList=itagReader->getTagsFromBox(boxUIDRef);

									TagStruct newTagInfo;
									bool16 tagFound = kFalse;
									for(int32 i = 0; i < newList.size(); i++)
									{											
										tagFound = kFalse;
										for(int32 j = 0;j < tList.size() ; j++ )
										{												
											if(newList[i].tagPtr == tList[j].tagPtr)
											{
												tagFound = kTrue;
												break;
											}
										}
										if(tagFound == kFalse)
										{												
											newTagInfo = newList[i];																							
											it2ForInline++;

											objCAssetvalueForInline = *it2ForInline;
											
											double typeId = -1;
											if(slugInfo.typeId == -97)
											{							
												typeId = objCAssetvalueForInline.getType_id();																										
											}
											else
											{
												typeId = slugInfo.typeId;																									
											}

											PMString attributeName = "typeId";
											PMString attributeValue("");
											attributeValue.AppendNumber(PMReal(typeId));

											newTagInfo.tagPtr->SetAttributeValue(WideString(attributeName),WideString(attributeValue));
											break;
										}
									}
									if(newList.size() == tList.size()){	

										for(int32 tagIndex = 0 ; tagIndex < newList.size() ; tagIndex++)
										{
											newList[tagIndex].tagPtr->Release();
										}
										continue;
									}
									XMLContentReference newContentRef = newTagInfo.tagPtr->GetContentReference();																									
									UIDRef NewFrameUIDRef = newContentRef.GetUIDRef();			
									
									vecAssetUidRef.push_back(NewFrameUIDRef);
									tList=itagReader->getTagsFromBox(boxUIDRef);

									for(int32 tagIndex = 0 ; tagIndex < newList.size() ; tagIndex++)
									{
										newList[tagIndex].tagPtr->Release();
									}
								}								
							}
							if(isForTable == kTrue)
							{
								vecAssetUidRef.clear();
								XMLReference cellXMLRef = slugInfo.tagPtr->GetParent();
								InterfacePtr<IIDXMLElement> cellXMLElement (cellXMLRef.Instantiate());
								if (cellXMLElement==NULL) 
								{
									return kFalse;
								}
								int32 framecount = cellXMLElement->GetChildCount();
								for(int32 index = 0 ;index < framecount ; index++)
								{
									XMLReference freameXMLRef = cellXMLElement->GetNthChild(index);
									InterfacePtr<IIDXMLElement>frameXMLElementPtr(freameXMLRef.Instantiate());
									XMLContentReference cntentREF = frameXMLElementPtr->GetContentReference();
									UIDRef ref = cntentREF.GetUIDRef();
									vecAssetUidRef.push_back(ref);
								}
								
							}
							for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
							{
								tList[tagIndex].tagPtr->Release();
							}
							
						}
						else if(AssetValuePtrObj->size() == 1)
						{
							if(isForTable == kTrue)
							{
								vecAssetUidRef.clear();
								XMLReference cellXMLRef = slugInfo.tagPtr->GetParent();
								InterfacePtr<IIDXMLElement> cellXMLElement (cellXMLRef.Instantiate());
								if (cellXMLElement==NULL) 
								{
									return kFalse;
								}
								int32 framecount = cellXMLElement->GetChildCount();
								for(int32 index = 0 ;index < framecount ; index++)
								{
									XMLReference freameXMLRef = cellXMLElement->GetNthChild(index);
									InterfacePtr<IIDXMLElement>frameXMLElementPtr(freameXMLRef.Instantiate());
									XMLContentReference cntentREF = frameXMLElementPtr->GetContentReference();
									UIDRef ref = cntentREF.GetUIDRef();
									vecAssetUidRef.push_back(ref);
								}
								
							}
							else
								vecAssetUidRef.push_back(ContentRef);
						}
						
						if(ImportFileInFrame(vecAssetUidRef[0],total))
						{
							if(isInlineImage)	
								makeImageFitProportionally = kTrue; //added to fit image for inline image also
							if(!isInlineImage)	
								makeImageFitProportionally = kTrue; // For Fitting InLine Image Proportionally for single image 
							
						}						
						
					}
                    
                    CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
					PMString attribVal;
					attribVal.AppendNumber(PMReal(slugInfo.parentId));
				
					if(slugInfo.tagPtr)
					{	
						TagName = slugInfo.tagPtr->GetTagString();
						slugInfo.tagPtr->SetAttributeValue(WideString("parentID"),WideString(attribVal));//ObjectID
						
						attribVal.Clear();							
						double pubId=slugInfo.sectionID;
						attribVal.AppendNumber(PMReal(pubId));
						slugInfo.tagPtr->SetAttributeValue(WideString("sectionID"),WideString(attribVal));//Publication ID
						
						attribVal.Clear();
						attribVal.AppendNumber(PMReal(slugInfo.parentTypeID));
						slugInfo.tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attribVal));//Parent Type ID
						
						attribVal.Clear();
						attribVal.AppendNumber(PMReal(typeId));
						slugInfo.tagPtr->SetAttributeValue(WideString("typeId"),WideString(attribVal));
						
						attribVal.Clear();
						attribVal.AppendNumber(PMReal(mpv_value_id));
						slugInfo.tagPtr->SetAttributeValue(WideString("rowno"),WideString(attribVal));
					}
				}

				if(slugInfo.flowDir == 0)
				{//if(slugInfo.isAutoResize == -111)
					margin = pageBounds.Right() -pageBounds.Left() +2;
					newMargin = pageBounds.Bottom() - pageBounds.Top() +2;
				}
				else
				{
					margin = pageBounds.Bottom() -pageBounds.Top() +2;
					newMargin = pageBounds.Right() - pageBounds.Left() +2;
				}	
			}
			if(ItemImageUnspreadFlag == kTrue)
			{
				PMString attribValue;
				attribValue.AppendNumber(PMReal( slugInfo.parentId));
				
				if(slugInfo.tagPtr)
				{	
					TagName = slugInfo.tagPtr->GetTagString();
					slugInfo.tagPtr->SetAttributeValue(WideString("parentID"),WideString(attribValue));//ObjectID
					
					attribValue.Clear();							
					attribValue.AppendNumber(PMReal(CurrentSubSectionID));
					slugInfo.tagPtr->SetAttributeValue(WideString("sectionID"),WideString(attribValue));//Publication ID								
					
					attribValue.Clear();
					attribValue.AppendNumber(PMReal(slugInfo.parentTypeID));
					slugInfo.tagPtr->SetAttributeValue(WideString("parentTypeID"),WideString(attribValue));//Parent Type ID
					
					attribValue.Clear();
					attribValue.AppendNumber(PMReal(tempTypeid));
					slugInfo.tagPtr->SetAttributeValue(WideString("typeId"),WideString(attribValue));//Parent Type ID
					
				}

				ItemImageUnspreadFlag = kFalse;
			}
													
			if(AssetValuePtrObj)
			{
				AssetValuePtrObj->clear();
				delete AssetValuePtrObj;
			}
		}while(false);
	}
	if(makeImageFitProportionally && !isSprayText )
	{
		XMLContentReference contentRef = slugInfo.tagPtr->GetContentReference();																									
		UIDRef ContentRef = contentRef.GetUIDRef();

		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{	
			 return 0;
		}

		InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
		( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID ,iSelectionManager))); 
		if(!txtMisSuite)
		{	
			return 0; 
		}
		SelectFrame(ContentRef);
		txtMisSuite->setFrameUser();
		SelectFrame(boxUIDRef);
	}		
	return 1;
}


ErrorCode CDataSprayer::DeleteText(ITextModel* textModel, const TextIndex position, const int32 length)
{
 	ErrorCode status = kFailure;
	do {
		ASSERT(textModel);
		if (!textModel) {
			break;
		}
		if (position < 0 || position >= textModel->TotalLength()) {
			ASSERT_FAIL("position invalid");
			break;
		}
		if (length < 1 || length >= textModel->TotalLength()) {
			ASSERT_FAIL("length invalid");
			break;
		}
   		InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
    	ASSERT(textModelCmds);
    	if (!textModelCmds) {
			break;
		}
		InterfacePtr<ICommand> deleteCmd(textModelCmds->DeleteCmd(position, length));
		ASSERT(deleteCmd);
		if (!deleteCmd) {
			break;
		}
		status = CmdUtils::ProcessCommand(deleteCmd);
	} while(false);
	return status;
}


void CDataSprayer::fitInlineImageInBoxInsideTableCellNEW(const UIDRef& boxUIDRef,InterfacePtr<ITextModel>& textModel,IIDXMLElement* cellXMLElementPtr)
{
	do{
		int32 StartPos1 = -1;
		int32 EndPos1 = -1;
		
		InterfacePtr<ITextStoryThread> textStoryThread(cellXMLElementPtr->QueryContentTextStoryThread());
		if(textStoryThread == NULL)
		{
			break;
		}
		StartPos1=textStoryThread->GetTextStart ();
		EndPos1 = textStoryThread->GetTextSpan();

		InterfacePtr<IItemStrand> itemStrand (((IItemStrand*)
			textModel->QueryStrand(kOwnedItemStrandBoss,IItemStrand::kDefaultIID)));
		if (itemStrand == nil) {
			break;
		}

		OwnedItemDataList ownedList;
		itemStrand->CollectOwnedItems(StartPos1, EndPos1 , &ownedList);
		int32 count = ownedList.size();
		
		if(count > 0)
		{
			for (int32 z = 0; z < count; z++)
			{
				UIDRef inlineBossUIDRef = UIDRef(boxUIDRef.GetDataBase(), ownedList[z].fUID);
				InterfacePtr<IHierarchy> hier(inlineBossUIDRef, UseDefaultIID());
				if (!hier) {
					break;
				}
				if (hier->GetChildCount() < 1) {
					break;
				}
				UID uid = hier->GetChildUID(0);
				if (uid == kInvalidUID) {
					break;
				}
				UIDRef placedThingUIDRef(inlineBossUIDRef.GetDataBase(),uid);
							
				InterfacePtr<ICommand> iAlignCmd(CmdUtils::CreateCommand(kFitContentPropCmdBoss));
				if(!iAlignCmd)
				{
					break;
				}		
				InterfacePtr<IHierarchy> iHier(placedThingUIDRef, IID_IHIERARCHY);
				if(!iHier)
				{
					break;
				}
				if(iHier->GetChildCount()==0)//There may not be any image at all????
				{
					break;
				}
				UID childUID=iHier->GetChildUID(0);
				UIDRef newChildUIDRef(placedThingUIDRef.GetDataBase(), childUID);
				iAlignCmd->SetItemList(UIDList(newChildUIDRef));
				CmdUtils::ProcessCommand(iAlignCmd);
			}											
		}
	}while(kFalse);
}
bool16 CDataSprayer::resetLetterOrNumberkeySeqCount(int32 val)
{
	if(val== 1){
		isReset = kTrue;
		localLeter =0;
		localNum =0;
		return kTrue;	
	}
	else{
		isReset = kFalse;
		return kFalse;
	}
}

void CDataSprayer::ClearNewImageFrameUIDList()
{
	NewImageFrameUIDListMap.clear();
	NewImageFrameUIDListForMap.clear();
}

MapNewImageFrameUIDList CDataSprayer::getNewImageFrameUIDList()
{
	return NewImageFrameUIDListMap;
}

bool16 CDataSprayer::setDifferentParameters(VecParameter& vecParameter)
{
	vParameters = vecParameter;
	return kTrue;
}

void CDataSprayer::ClearDifferentParameters()
{
	vParameters.clear();
	
}

bool16 CDataSprayer::getIsColumnChange()
{
	return fColumnChange;
}
void CDataSprayer::resetIsColumnChange()
{
	fColumnChange = kFalse;
}


VecReturnParameter CDataSprayer::getReturnParameter()
{
	return vReturnParameter;
}

void CDataSprayer::ClearReturnParameter()
{
	vReturnParameter.clear();
	PageUIDList.clear();
	fPageAdded = kFalse;
}


void getBoxPosition(PMRect &BoxBounds)
{

	if(vParameters.size() <= 0)
	{
		return;
	}

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return;
	}

	Parameter p  = vParameters[0];
	PMReal box_Width = BoxBounds.Right() - BoxBounds.Left();
	PMReal box_Hight = BoxBounds.Bottom() - BoxBounds.Top();

	if(box_Width < p.WidthOfSprayItemPerFrames)
		box_Width = p.WidthOfSprayItemPerFrames;

	PMReal Template_Width = p.ItemStencilMaxBounds.Right() - p.ItemStencilMaxBounds.Left();
	PMReal Template_Hight = p.ItemStencilMaxBounds.Bottom() - p.ItemStencilMaxBounds.Top();

	PMReal BottomMarkAfterSpray;
	PMReal LeftMarkAfterSpray;
	PMReal TopMarkAfterSpray;
	PMReal RightMarkAfterSpray;

	PMReal NewLeft;
	PMReal NewTop;
	
	{
		p.idxVerticalCount = 0;
		p.idxHorizontalCount++; 

		vericalCount = 0;
		HorizontalCount = p.idxHorizontalCount;

		p.fFaltuCase = kTrue;
		
		if((BoxBounds.Right() + 5 + box_Width) < p.RightMarkAfterSprayForLeadingItem)
		{
			//CA("still under leading frame");
			BottomMarkAfterSpray = p.BottomMarkAfterSprayForLeadingItem  + 5.0;
			LeftMarkAfterSpray = BoxBounds.Right() + 5;

			if(BottomMarkAfterSpray + box_Hight > p.PagemarginBoxBounds.Bottom())
			{
				LeftMarkAfterSpray = p.RightMarkAfterSprayForLeadingItem + 5.0;

				if(LeftMarkAfterSpray + 5.0 + box_Width > p.PagemarginBoxBounds.Right())
				{
					//CA("Add new page case and then set let and top again");
					PMRect PagemarginBoxBounds;
					fPageAdded = addPage(p ,PagemarginBoxBounds);					
					
					LeftMarkAfterSpray = PagemarginBoxBounds.Left();
					BottomMarkAfterSpray = PagemarginBoxBounds.Top();
					p.idxVerticalCount = 0;
					p.idxHorizontalCount = 0;
					p.ProdBlockBoundList.clear();

					vericalCount = p.idxVerticalCount;
					HorizontalCount = p.idxHorizontalCount;

				}
				else
				{
					if(p.PagemarginBoxBounds.RectIn(p.ItemStencilMaxBounds)) //**** if Stencil is with in Page
					{
						BottomMarkAfterSpray = p.ItemStencilMaxBounds.Top();						
					}
					else
					{
						BottomMarkAfterSpray = p.PagemarginBoxBounds.Top();
					}
				}				
			}
		}
		else if(p.PagemarginBoxBounds.RectIn(p.ItemStencilMaxBounds)) //**** if Stencil is with in Page
		{
			LeftMarkAfterSpray = p.RightMarkAfterSprayForLeadingItem + 5.0;
			if(LeftMarkAfterSpray + Template_Width < p.PagemarginBoxBounds.Right())
			{
				//we have template width in next column
				BottomMarkAfterSpray = p.ItemStencilMaxBounds.Top();				
			}
			else
			{
				PMRect PagemarginBoxBounds;
				fPageAdded = addPage(p ,PagemarginBoxBounds);
				LeftMarkAfterSpray = PagemarginBoxBounds.Left();
				BottomMarkAfterSpray = PagemarginBoxBounds.Top();
				p.idxVerticalCount = 0;
				p.idxHorizontalCount = 0;
				p.ProdBlockBoundList.clear();
				vericalCount = p.idxVerticalCount;
				HorizontalCount = p.idxHorizontalCount;
			}
		}
		else
		{

			LeftMarkAfterSpray = p.RightMarkAfterSprayForLeadingItem + 5.0;
			if(LeftMarkAfterSpray + Template_Width < p.PagemarginBoxBounds.Right())
			{
				//we have template width in next column
				BottomMarkAfterSpray = p.PagemarginBoxBounds.Top();

			}
			else
			{
				PMRect PagemarginBoxBounds;
				fPageAdded = addPage(p ,PagemarginBoxBounds);
				LeftMarkAfterSpray = PagemarginBoxBounds.Left();
				BottomMarkAfterSpray = PagemarginBoxBounds.Top();

				p.idxVerticalCount = 0;
				p.idxHorizontalCount = 0;
				p.ProdBlockBoundList.clear();

				vericalCount = p.idxVerticalCount;
				HorizontalCount = p.idxHorizontalCount;
			}			
		}

		NewLeft = LeftMarkAfterSpray;
		NewTop = BottomMarkAfterSpray;

		BoxBounds.Left(NewLeft);
		BoxBounds.Top(NewTop);
		BoxBounds.Right(NewLeft + box_Width);
		BoxBounds.Bottom(NewTop + box_Hight);
	}
}

bool16 getMaxLimitsOfBoxes(const UIDList& boxList, PMRect& maxBounds, vectorBoxBounds &boxboundlist)
{
	boxboundlist.clear();
	PMReal minTop=0.0, minLeft=0.0 , maxBottom=0.0 , maxRight=0.0;
	UIDRef boxUIDRef = boxList.GetRef(0);
	int index =0;

	
	InterfacePtr<IGeometry> geometryPtr(boxUIDRef, UseDefaultIID());
	if(!geometryPtr)
	{
		if(boxList.Length() > 1)  // This is if first frame is deleted after spray // (Ref: New Indicator Spray Functionality)
		{
			boxUIDRef = boxList.GetRef(1);
			InterfacePtr<IGeometry> geometryPtr1(boxUIDRef, UseDefaultIID());
			geometryPtr = geometryPtr1;
			index = index+2;
		}
		else
		{
			return kFalse;
		}
	}
	else
	{
		index = index + 1;
	}

	if(!geometryPtr)
		return kFalse;

	PMRect boxBounds=geometryPtr->GetStrokeBoundingBox(InnerToPasteboardMatrix(geometryPtr));
	
	PMReal top = boxBounds.Top();
	PMReal left = boxBounds.Left();
	PMReal bottom = boxBounds.Bottom();
	PMReal right = boxBounds.Right();

	BoxBounds orgBoxBound;
	orgBoxBound.Top = top;
	orgBoxBound.Left = left;
	orgBoxBound.Right = right;
	orgBoxBound.Bottom = bottom;
	orgBoxBound.BoxUIDRef = boxUIDRef;
	orgBoxBound.compressShift =0;
	orgBoxBound.enlargeShift =0;
	orgBoxBound.shiftUP = kTrue;
	boxboundlist.push_back(orgBoxBound);

	minTop = top;
	minLeft = left;
	maxBottom = bottom;
	maxRight = right;

	for(index=1;index<boxList.Length();index++)
	{
		boxUIDRef = boxList.GetRef(index);

		InterfacePtr<IGeometry> geometryPtr(boxUIDRef, UseDefaultIID());
		if(!geometryPtr)
		{
			//CA("!geometryPtr");  // if any frame is missing then continue to other.
			continue;
		}

		PMRect boxBounds=geometryPtr->GetStrokeBoundingBox(InnerToPasteboardMatrix(geometryPtr));
				
		top = boxBounds.Top();
		left = boxBounds.Left();
		bottom = boxBounds.Bottom();
		right = boxBounds.Right();

		BoxBounds orgBoxBound1;
		orgBoxBound1.Top = top;
		orgBoxBound1.Left = left;
		orgBoxBound1.Right = right;
		orgBoxBound1.Bottom = bottom;
		orgBoxBound1.BoxUIDRef = boxUIDRef;
		orgBoxBound1.compressShift =0;
		orgBoxBound1.enlargeShift =0;
		orgBoxBound1.shiftUP = kTrue;
		boxboundlist.push_back(orgBoxBound1);

		if(top < minTop)
			minTop = top;
		if(left < minLeft)
			minLeft = left;
		if(bottom > maxBottom)
			maxBottom = bottom;
		if(right > maxRight)
			maxRight = right;
	}

	maxBounds.Top(minTop);
	maxBounds.Left(minLeft);
	maxBounds.Bottom(maxBottom);
	maxBounds.Right(maxRight);

	return kTrue;
}


bool16 getMarginBounds(const UIDRef& pageUIDRef, PMRect& marginBoxBounds)
{
	bool16 result = kFalse;

	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CA(" ptrIAppFramework nil ");
			return result;
		}

		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutData == nil)
			break;

		IDocument* doc = layoutData->GetDocument();
		if (doc == nil)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No document");		
			break;
		}
		InterfacePtr<IPageList> pageList(doc, UseDefaultIID());
		if (pageList == nil)
		{
			ASSERT_FAIL("pageList is invalid");
			break;
		}
		InterfacePtr<IGeometry> pageGeometry(pageUIDRef, UseDefaultIID());
		if (pageGeometry == nil)
		{
			ASSERT_FAIL("pageGeometry is invalid");
			break;
		}
		PMRect pageBounds = pageGeometry->GetStrokeBoundingBox();

		PMReal leftMargin =0.0,topMargin =0.0,rightMargin = 0.0,bottomMargin =0.0;
        
		InterfacePtr<ITransform> transform(pageUIDRef, UseDefaultIID());
		ASSERT(transform);
		if (!transform) {
			break;
		}
		PMRect marginBBox;
		InterfacePtr<IMargins> margins(transform, IID_IMARGINS);
		// Note it's OK if the page does not have margins.
		if (margins) {			
			margins->GetMargins(&leftMargin,&topMargin,&rightMargin,&bottomMargin);			
		}

		PageType pageType = pageList->GetPageType(pageUIDRef.GetUID()) ;

		PMPoint leftTop;
		PMPoint rightBottom;

		if(pageType == kLeftPage)
		{
			leftTop.X(pageBounds.Left()+rightMargin);
			leftTop.Y(pageBounds.Top()+topMargin);
			rightBottom.X(pageBounds.Right() - leftMargin);
			rightBottom.Y(pageBounds.Bottom() - bottomMargin);
	
		}
		else if(pageType == kRightPage)
		{
			leftTop.X(pageBounds.Left()+leftMargin);
			leftTop.Y(pageBounds.Top()+topMargin);
			rightBottom.X(pageBounds.Right() - rightMargin);
			rightBottom.Y(pageBounds.Bottom() - bottomMargin);
		}
		else if(pageType == kUnisexPage)
		{
			leftTop.X(pageBounds.Left()+leftMargin);
			leftTop.Y(pageBounds.Top()+topMargin);
			rightBottom.X(pageBounds.Right() - rightMargin);
			rightBottom.Y(pageBounds.Bottom() - bottomMargin);

		}

		// Place the item into a frame the size of the page margins
		// with origin at the top left margin. Note that the frame
		// is automatically resized to fit the content if the 
		// content is a graphic. Convert the points into the
		// pasteboard co-ordinate space.

		::TransformInnerPointToPasteboard(pageGeometry,&leftTop);
		::TransformInnerPointToPasteboard(pageGeometry,&rightBottom);

		marginBoxBounds.Left() = leftTop.X();
		marginBoxBounds.Top() = leftTop.Y();
		marginBoxBounds.Right() = rightBottom.X();
		marginBoxBounds.Bottom() = rightBottom.Y();

		result = kTrue;
	}
	while(kFalse);

	return result;
}

bool16 getPageBounds(const UIDRef& pageUIDRef, PMRect& pageBounds)
{
	bool16 result = kFalse;

	do
	{
		InterfacePtr<IGeometry> pageGeometry(pageUIDRef, UseDefaultIID());
		if (pageGeometry == nil)
		{
			ASSERT_FAIL("pageGeometry is invalid");
			break;
		}
		
		pageBounds = pageGeometry->GetStrokeBoundingBox();
		result = kTrue;
	}
	while(kFalse);

	return result;
}


bool16 addPage(Parameter& p ,PMRect& PagemarginBoxBounds)
{
	UID pageUID;
	UIDRef pageRef = UIDRef::gNull;
	UIDRef spreadUIDRef = UIDRef::gNull;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}

	//if(addPageSplCase)
	//{
	//	//InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); //Cs4
	//	//if (layoutData == nil)
	//	//{
	//	//	ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No layoutData");		
	//	//	break;
	//	//}

	//	IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
	//	if (document == nil)
	//	{
	//		ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No document");		
	//		break;
	//	}
	//	IDataBase* database = ::GetDataBase(document);
	//	if(!database)
	//	{
	//		ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No database");		
	//		break;
	//	}


	//	bool16 result = this->getCurrentPage(pageRef, spreadUIDRef);
	//	if(result == kFalse)
	//		break;

	//	InterfacePtr<IPageList> iPageList(document,UseDefaultIID());
	//	if(iPageList == nil)
	//	{
	//		//CA("iPageList == nil");
	//		break;
	//	}

	//	int32 bringSpreadToFrontIndex = 0;

	//	int32 pageToInsertAt = 0;
	//	PageType pageType = iPageList->GetPageType(pageRef.GetUID()) ;						
	//	if(pageType == kLeftPage)
	//	{				
	//		//CA("pageType == kLeftPage");
	//		pageToInsertAt = 1;
	//	}
	//	else if(pageType == kRightPage)
	//	{
	//		/*UID pageUID = iPageList->GetNthPageUID(addPageSplCase_pageIndex);
	//		if(pageUID == kInvalidUID)
	//		{
	//			ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No pageUID");		
	//			break;
	//		}							
	//		
	//		InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)document,UseDefaultIID());
	//		if (iSpreadList==nil)
	//		{
	//			ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::iSpreadList==nil");				
	//			break;
	//		}

	//		int32 pageCount = 0;
	//		for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
	//		{
	//			UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));
	//			spreadUIDRef = temp_spreadUIDRef;

	//			InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
	//			if(!spread)
	//			{
	//				ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::!spread");						
	//				break;
	//			}
	//			int numPages=spread->GetNumPages();
	//			pageCount +=  numPages;
	//			if(pageCount > addPageSplCase_pageIndex)
	//			{
	//				++numSp;
	//				UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));
	//				spreadUIDRef = temp_spreadUIDRef;

	//				bringSpreadToFrontIndex = numSp;
	//				break;
	//			}
	//		}*/

	//		//CA("pageType == kRightPage");
	//		InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)document,UseDefaultIID());
	//		if (iSpreadList==nil)
	//		{
	//			ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::iSpreadList==nil");				
	//			break;
	//		}

	//		UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(++addPageSplCase_SpreadIndex));
	//		spreadUIDRef = temp_spreadUIDRef;

	//		bringSpreadToFrontIndex = addPageSplCase_SpreadIndex;
	//		pageToInsertAt = 0;
	//	}
	//	else if(pageType == kUnisexPage)
	//	{	
	//		//CA("pageType == kUnisexPage");
	//		InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)document,UseDefaultIID());
	//		if (iSpreadList==nil)
	//		{
	//			ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::iSpreadList==nil");				
	//			break;
	//		}

	//		UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(++addPageSplCase_SpreadIndex));
	//		spreadUIDRef = temp_spreadUIDRef;

	//		bringSpreadToFrontIndex = addPageSplCase_SpreadIndex;
	//		pageToInsertAt = 0;
	//	}
	//	
	//	ProductSpray ps;
	//	
	//	/*PMString temp("addPageSplCase_SpreadIndex = ");
	//	temp.AppendNumber(addPageSplCase_SpreadIndex);
	//	temp.Append(", pageToInsertAt");
	//	temp.AppendNumber(pageToInsertAt);
	//	CA(temp);*/

	//	ErrorCode err = ps.CreatePages(document,spreadUIDRef,1,pageToInsertAt,kTrue);
	//	if(err == kFailure)
	//	{
	//		//CA("err == kFailure");
	//		return;
	//	}

	//	CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
	//	//CA("added succesfully");
	//	/*++addPageSplCase_pageIndex;*/
	//	if(pageType == kLeftPage)
	//	{	
	//		++addPageSplCase_pageIndexPerSpread;					
	//	}
	//	else if(pageType == kRightPage)
	//	{		
	//		addPageSplCase_pageIndexPerSpread = 0;
	//	}
	//	else if(pageType == kUnisexPage)
	//	{	
	//		addPageSplCase_pageIndexPerSpread = 0;
	//	}
	//	
	//	pageRef = UIDRef::gNull;
	//	spreadUIDRef = UIDRef::gNull;

	//	bool16 result2 = this->getCurrentPage(pageRef, spreadUIDRef);
	//	if(result2 == kFalse)
	//		break;

	//	
	//	InterfacePtr<IMasterPage> ptrIMasterPage(pageRef, UseDefaultIID());
	//	if(ptrIMasterPage != NULL)
	//	{
	//		ptrIMasterPage->SetMasterPageUID(addPageSplCase_MasterSpreadUIDOfTemplate);
	//	}
	//	

	//	InterfacePtr<ITransform> transform(pageRef, UseDefaultIID());					
	//	if (!transform) {//CA("!transform ");
	//		break;
	//	}
	//	
	//	InterfacePtr<IMargins> margins(transform, IID_IMARGINS);
	//	// Note it's OK if the page does not have margins.
	//	if (margins) {
	//		//CA(" before margins->SetMargins");
	//		margins->SetMargins(addPageSplCase_marginBBox.Left(), addPageSplCase_marginBBox.Top(), addPageSplCase_marginBBox.Right(), addPageSplCase_marginBBox.Bottom());
	//			//CA(" After margins->SetMargins");
	//	}

	//	InterfacePtr<IColumns> ptrIColumns(transform, IID_ICOLUMNS);
	//	// Note it's OK if the page does not have margins.
	//	if (ptrIColumns) {//CA("before SetColumns");
	//		ptrIColumns->SetColumns(addPageSplCase_columns);
	//		//CA("After SetColumns");
	//	}
	//	


	//	
	//

	//	IControlView* fntView = Utils<ILayoutUIUtils>()->QueryFrontView();
	//	if (fntView == nil)
	//	{
	//		//CA("The front view is nil.");
	//		break;
	//	}

	//	InterfacePtr<IGeometry> spreadGeo(spreadUIDRef, UseDefaultIID());
	//	if (!spreadGeo) {
	//		//CA("!spreadGeo");
	//		break;
	//	}
	//	InterfacePtr<ICommand> showSprdCmd(Utils<ILayoutUIUtils>()->MakeScrollToSpreadCmd(fntView, spreadGeo, kTrue));
	//	CmdUtils::ProcessCommand(showSprdCmd);

	//	if (CmdUtils::ProcessCommand(showSprdCmd) != kSuccess) {
	//		//CA("MakeScrollToSpreadCmd failed");
	//		break;
	//	}

	//	//InterfacePtr<ILayoutControlData> layoutData1(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); //Cs4
	//	//if (layoutData1 == nil)
	//	//{
	//	//	ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No layoutData1");		
	//	//	break;
	//	//}

	//	//IDocument* document1 = layoutData1->GetDocument();
	//	//if (document1 == nil)
	//	//{
	//	//	ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No document1");		
	//	//	break;
	//	//}

	//	//InterfacePtr<IPageList> iPageList1(document1,UseDefaultIID());
	//	//if(iPageList1 == nil)
	//	//{
	//	//	CA("iPageList1 == nil");
	//	//	break;
	//	//}

	//	///*UID pageUID = iPageList1->GetNthPageUID(addPageSplCase_pageIndex);
	//	//if(pageUID == kInvalidUID)
	//	//{
	//	//	ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No pageUID");		
	//	//	break;
	//	//}	*/

	//	//int32 InDESC = iPageList1->GetPageIndex(pageRef.GetUID());
	//	//PMString ASD("InDESC : ");
	//	//ASD.AppendNumber(InDESC);
	//	//CA(ASD);

	//
	//	/*InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
	//	if(!spread)
	//	{
	//		ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::!spread");						
	//		return;
	//	}
	//	
	//	UID pageUID = spread->GetNthPageUID(addPageSplCase_pageIndexPerSpread);*/
	//	pageUidList.push_back(/*pageUID*/pageRef.GetUID());
	//}
	//else
	{
		Utils<ILayoutUIUtils>()->AddNewPage();
		p.PageCount++;			
		
	
		IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if(fntDoc==nil)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::fntDoc==nil");	
			return kFalse;
		}
		IDataBase* database = ::GetDataBase(fntDoc);
		if(database==nil)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::database==nil");			
			return kFalse;
		}
		InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
		if (iSpreadList==nil)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::iSpreadList==nil");				
			return kFalse;
		}	

		for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
		{
			if( (iSpreadList->GetSpreadCount()-1) > numSp )
			{
				continue;
			}
			UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));
			spreadUIDRef = temp_spreadUIDRef;

			InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
			if(!spread)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::!spread");						
				return kFalse;
			}
			int numPages=spread->GetNumPages();
			pageUID = spread->GetNthPageUID(numPages-1);
			UIDRef temp_pageRef(database, pageUID);
			pageRef = temp_pageRef;

		}
		PageUIDList.push_back(pageUID);
	}

	bool16 result = getMarginBounds(pageRef, PagemarginBoxBounds);
	if(result == kFalse)
	{
		result = getPageBounds(pageRef, PagemarginBoxBounds);
		if(result == kFalse)
			return kFalse;
		
	}

	return kTrue;
}


bool16 CDataSprayer::addNewPageforOverflowOfFrame(UIDRef boxUIDRef)
{
//	do
//		{
//
//			PMRect marginBoxBounds;
//			bool16 result = kFalse;
//			bool16 overSetted  = kFalse;
//			
//			
//			UIDRef pageUIDRef;
//
//			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//			if(ptrIAppFramework == NULL)
//			{
//				CA("ptrIAppFramework == NULL");
//				return kFalse;
//			}
//
//			IDocument* doc = layoutData->GetDocument();
//			if (document == nil)
//			{
//				ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No document");		
//				break;
//			}
//			
//			IDataBase* database = ::GetDataBase(doc);
//			if(!database)
//			{
//				//////CA("database == nil");
//				return kFalse;
//			}
//			
//			InterfacePtr<ISpreadList> spreadList(doc, IID_ISPREADLIST); 
//			if(spreadList == nil)
//			{
//				//////CA("spreadeList == nil");
//				return kFalse;
//			}
//
//			/*InterfacePtr<ISelectionManager>	iSelectionManager(Utils<ISelectionUtils> ()->QueryActiveSelection ());
//			if(!iSelectionManager)
//			{
//				break;
//			}*/
//
//			int32 spreadCount = spreadList->GetSpreadCount();
//			//IDataBase *db = iSelectionManager->GetDataBase();
//			IDataBase* db = ::GetDataBase(doc);
//			UIDRef spreadUIDRef(db, spreadList->GetNthSpreadUID(spreadCount-1));
//
//			ScriptingVersionOfGetLastPage(pageUIDRef,spreadUIDRef);
//			UID pageUID = pageUIDRef.GetUID();
//
//				
//				InterfacePtr<ISubSectionSprayer> iSSSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
//				if(iSSSprayer==nil)
//				{
//					CA("Plugin Ap_SubSectionSprayer.pln was not found in the plugins directory of adobe.");
//					return kFalse;
//				}
//
//				result = this->getMarginBounds(pageUIDRef, marginBoxBounds);
//				if(result == kFalse)
//				{
//					result = this->getPageBounds(pageUIDRef, marginBoxBounds);
//					if(result == kFalse)
//					{
//						ptrIAppFramework->LogDebug("ProductFinderPalette:SubSectionSprayer::sprayPageWithResizableFrame:result == kFalse");
//						//break;
//					}
//				}
//
//
//				/*PMString temp;
//				temp.AppendNumber(iSSSprayer->getTopMargin());
//				CA(temp);*/
//
//				marginBoxBounds.Left() += iSSSprayer->getLeftMargin();
//				marginBoxBounds.Right() -= iSSSprayer->getRightMargin();
//				marginBoxBounds.Top() += iSSSprayer->getTopMargin();
//				marginBoxBounds.Bottom() -= iSSSprayer->getBottomMargin();
//
//			
//				PMReal OrgBoxMaxWidth = 0.0, OrgBoxMaxHeight=0.0, maxPageWidth=0.0, maxPageHeight=0.0;
//				PMRect origMaxBoxBounds;
//
//				maxPageWidth = abs(marginBoxBounds.Right() - marginBoxBounds.Left());
//				maxPageHeight = abs(marginBoxBounds.Bottom() - marginBoxBounds.Top());
//				PMString ZXC2("maxPageHeight : ");
//				ZXC2.AppendNumber(maxPageHeight);
//				ZXC2.Append("  maxPageWidth : ");
//				ZXC2.AppendNumber(maxPageWidth);
//				CA(ZXC2);
//				 
//				PMReal VerticalBoxSpacing = iSSSprayer->getVerticalBoxSpacing();
//				UIDList productSelUIDList;
////								PMRect origMaxBoxBounds;
//				vector<BoxBounds> ProdStencilBoxBoundVector;
//				bool16 result1 = getMaxLimitsOfBoxes(productSelUIDList, origMaxBoxBounds, ProdStencilBoxBoundVector);
//				if(result1 == kFalse)
//					return kFalse;
//
//				OrgBoxMaxWidth = abs(origMaxBoxBounds.Right() - origMaxBoxBounds.Left());
//				OrgBoxMaxHeight = abs(origMaxBoxBounds.Bottom() - origMaxBoxBounds.Top());
//
//				PMString ZXC("OrgBoxMaxHeight : ");
//				ZXC.AppendNumber(OrgBoxMaxHeight);
//				ZXC.Append("  OrgBoxMaxWidth : ");
//				ZXC.AppendNumber(OrgBoxMaxWidth);
//				CA(ZXC);
//
//				int16 maxVertCnt=0, maxHorizCnt=0;
//				/*if(iSSSprayer->getHorizFlowType()==kFalse)
//				{
//					maxVertCnt = ToInt16(maxPageSprayCount.X());
//					maxHorizCnt = ToInt16(maxPageSprayCount.Y());
//				}
//				//else*/
//				//{
//				//	maxVertCnt = ToInt16(maxPageSprayCount.Y());
//				//	maxHorizCnt = ToInt16(maxPageSprayCount.X());
//				//}
//
//				int16 CurrVertCnt=0, CurrHorizCnt=0;
//												
//				int16 horizCnt = 0; 
//				int16 vertCnt = 0;
//				FrameBoundsList ProdBlockBoundList;
//				ProdBlockBoundList.clear();
//		
//				bool16 GoToNewPage = kFalse;
//
//				int16 tempHorizCnt=0, tempVertCnt=0;	
//
//				//if(vertCnt > 0 && ProdBlockBoundList.size()!= 0 )
//				//{
//				//	bool16 result1 = kFalse;
//				//	//PMRect ProductBlockMaxBoxBounds;
//				//	PMReal TotalHeight=0.0;		
//
//				//	for(int p=0; p<ProdBlockBoundList.size(); p++)
//				//	{
//				//		if(ProdBlockBoundList[p].HorzCnt ==horizCnt)
//				//		{						
//				//			TotalHeight += abs(ProdBlockBoundList[p].BoxBounds.Bottom() - ProdBlockBoundList[p].BoxBounds.Top() + VerticalBoxSpacing);					
//				//		}
//				//	}
//					PMString ZXC1("(maxPageHeight - TotalHeight ) : ");
//					ZXC1.AppendNumber((maxPageHeight /*- TotalHeight*/ ));
//					CA(ZXC1);
//					if((origMaxBoxBounds.Bottom()) > (marginBoxBounds.Bottom() /*- TotalHeight*/ ) )
//					{
//						//CA("Vertical Area insuffisient please move to next Page");
//						//if(horizCnt == maxHorizCnt-1)
//						//{
//						//	//CA("Condition = 0");
//						//	Condition = 0;
//						//}
//						//continue;
//
//						overSetted=kTrue;
//					}
//				//}
//
//		    // InterfacePtr<IFrameList> flist(textModel->QueryFrameList());
//				//Utils<ITextUtils> tUtil;
//
//
//				//bool16 overSetted = tUtil->IsOverset(flist);
//               
//				PMString state = "yes";
//				if(overSetted)
//				{
//					CAlert::InformationAlert(state);
//                   
//					
//					//InterfacePtr<ISpreadList> spreadList(doc, UseDefaultIID());
//					
//					int32 numPages = 1;
//                   
//
//					InterfacePtr<ICommand> newPageCmd(CmdUtils::CreateCommand(kNewPageCmdBoss)); 
//					if(newPageCmd == nil)
//					{
//						CA("newPageCmd == nil");
//						return kFalse;
//					}
//					InterfacePtr<IPageCmdData> pageCmdData(newPageCmd, UseDefaultIID());
//					if(pageCmdData == nil)
//					{
//						CA("pageCmdData == nil");
//						return kFalse;
//					}
//					//CA(" @@@@@@@@@@@@@");
//				
//					pageCmdData->Set 
//					( 
//						spreadUIDRef, 
//						1, 
//						1 
//					); 
//					//CA(" ################");
//				
//					InterfacePtr<IMasterSpreadList> iMasterSpreadList(doc,UseDefaultIID());
//					if(iMasterSpreadList == nil)
//					{
//						CA("iMasterSpreadList == nil");
//						return kFalse;
//					}
//					UID masterSpreadUID = iMasterSpreadList->GetNthMasterSpreadUID(0);
//
//					UIDRef masterSpreadUIDRef(database,masterSpreadUID);
//
//					InterfacePtr<ICommand> command(CmdUtils::CreateCommand( kApplyMasterSpreadCmdBoss)); 
//					if(command == nil)
//					{
//						CA("command == nil");
//						return kFalse;
//					}
//
//					//CA("Scripting version od apply master page!!!!!!!!");
//					
//					UIDList pageUIDList(database);
//					pageUIDList.Insert(pageUID);
//					command->SetItemList(pageUIDList);
//
//					InterfacePtr<IApplyMasterCmdData>applyMasterCmd(command,UseDefaultIID());
//					if(applyMasterCmd == nil)
//					{
//						//CA("applyMasterCmd == nil");
//						return kFalse;
//					}
//					//CA("applyMasterCmd == nil@");
//
//					/*applyMasterCmd->Set 
//						( 
//							masterSpreadUIDRef, 
//							masterSpreadUIDRef, 
//							masterSpreadUIDRef 
//						); */ //CS5 change
//					
//					applyMasterCmd->SetApplyMasterCmdData(masterSpreadUIDRef , IApplyMasterCmdData::kKeepCurrentPageSize);//-now use in CS5---
//					
//
//					ErrorCode status = CmdUtils::ProcessCommand(newPageCmd); 
//				
//					if(status == kFailure)
//					{
//						CA("Page can not be created!!!");
//						return kFalse;
//					}
//
//
//				}
//		}
//		while(kFalse);
		
		return kTrue;
}

void CDataSprayer::setItemSprayOverflowFlag(int32 oversetflag)
{
		this->ItemSprayOverflowFlag = oversetflag;
}
int32 CDataSprayer::getItemSprayOverflowFlag()
{
	return this->ItemSprayOverflowFlag;
}

void CDataSprayer::clearZeroByteImageList()
{
	zeroByteImageList = "";
}
	
PMString CDataSprayer::getZeroByteImageList()
{
	return zeroByteImageList;
}

void CDataSprayer::getTextModelByBoxUIDRef(const UIDRef & boxUIDRef, InterfacePtr<ITextModel> & textModel)
{
    UID textFrameUID = kInvalidUID;
    
    InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return ;
	}
    
    InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
	if (graphicFrameDataOne)
	{
		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	}
	if (textFrameUID == kInvalidUID)
	{
		return ;
	}
    
	InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
	if (graphicFrameHierarchy == nil)
	{
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::getTextModelByBoxUIDRef::!graphicFrameHierarchy");
		return ;
	}
    
	InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
	if (!multiColumnItemHierarchy) {
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::getTextModelByBoxUIDRef::!multiColumnItemHierarchy");
		return ;
	}
    
	InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
	if (!multiColumnItemTextFrame) {
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::getTextModelByBoxUIDRef::!multiColumnItemTextFrame");
		return ;
	}
	
	InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
	if (!frameItemHierarchy) {
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::getTextModelByBoxUIDRef::!frameItemHierarchy");
		return ;
	}
    
	InterfacePtr<ITextFrameColumn>
	textFrame(frameItemHierarchy, UseDefaultIID());
	if (!textFrame) {
		ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::getTextModelByBoxUIDRef::!textFrame");
		return ;
	}
    
    InterfacePtr<ITextModel> textModel1(textFrame->QueryTextModel());
	if (textModel1 == NULL )
	{
		return ;
	}
    textModel = textModel1;
    return ;
}


PMString CDataSprayer::getAlphabetValue(int32 itemIdSeqNo)
{
    PMString attributeValue("");

    int wholeNo =  itemIdSeqNo / 26;
    int remainder = itemIdSeqNo % 26;

    if(wholeNo == 0)
    {// will print 'a' or 'b'... 'z'  upto 26 item ids
        attributeValue.Append(AlphabetArray[remainder]);
    }
    else  if(wholeNo <= 26)
    {// will print 'aa' or 'ab'.. 'ba'...'zz'  upto 676 item ids
        attributeValue.Append(AlphabetArray[wholeNo-1]);
        attributeValue.Append(AlphabetArray[remainder]);
    }
    else if(wholeNo <= 52)
    {// will print 'aaa' or 'aab'.. 'aba'.. 'azz' upto 1352 item ids
        attributeValue.Append(AlphabetArray[0]);
        attributeValue.Append(AlphabetArray[wholeNo -26 -1]);
        attributeValue.Append(AlphabetArray[remainder]);
    }
    return attributeValue;
}

void CDataSprayer::handleDeleIfEmptyTags(UIDRef &boxUIDRef)
{
/*
    InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
    if(ptrIAppFramework == NULL)
    {
        CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
        return ;
    }
    
    InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
    if(!itagReader)
    {
        ptrIAppFramework->LogError("AP7_DataSprayer::CDataSprayer::sprayForThisBox::!itagReader");
        return;
    }
    TagList tList=itagReader->getTagsFromBox(boxUIDRef);
    
    TagStruct tagInfo;
    int numTags=static_cast<int>(tList.size());
    tagInfo = tList[0];
    
    
    if(numTags<0)
    {
        ptrIAppFramework->LogInfo("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::numTags<0");
        return;
    }
    
    //Spray for all  tags other than tabbedTextTag
    for(int i=numTags-1; i<0; i--)
    {
        TagStruct tagInfo=tList[i];
        int32 tags = tagInfo.tagPtr->GetAttributeCount();
        XMLReference tagInfoXMLRef = tagInfo.tagPtr->GetXMLReference ();
        
        if(tagInfo.deleteIfEmpty != 2)
        {
            continue;
        }
        
        {
            //CA("Going to delete 2");
            
        int32 tagStartPos1 = -1;
        int32 tagEndPos1 = -1;
            
            
        if(deleteCount >0)
            CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority);
            
        deleteCount++;
            
        InterfacePtr<ITextStoryThread> textStoryThread((tagInfo.tagPtr)->QueryContentTextStoryThread());
        if(textStoryThread == NULL)
        {
            ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 0:textStoryThread is NULL");
            break;
        }
        InterfacePtr<ITextModel> textModel(textStoryThread,UseDefaultIID());
        if(textModel == NULL)
        {
            ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 0:textModel is NULL");
            break;
        }
            
            
        XMLReference perentXMLRef = tagInfo.tagPtr->GetParent();
        InterfacePtr<IIDXMLElement>perentXMLElement(perentXMLRef.Instantiate());
        if(!perentXMLElement)
            continue;
            
        Utils<IXMLUtils>()->GetElementIndices(perentXMLElement,&tagStartPos1,&tagEndPos1);
            
            if(tagInfo.elementId != attributeId)
            {
                TextIterator begin1(textModel , tagStartPos);
                TextIterator end1(textModel , tagStartPos1);
                int32 removethis = 0;
                bool16 prevEnterFound = kFalse;
                for(TextIterator iter = begin1 ;iter > end1 ; iter--)
                {
                    
                    if(*iter == kTextChar_CR)
                    {
                        prevEnterFound = kTrue;
                        break;
                    }
                    removethis++;
                    
                }
                if(prevEnterFound)
                {
                    delStart = tagStartPos - removethis + 1;
                }
                else
                {
                    delStart = tagStartPos1;
                }
            }
            else
            {
                TextIterator begin1(textModel , delStart);
                TextIterator end1(textModel , tagStartPos1);
                int32 removethis = 0;
                bool16 prevEnterFound = kFalse;
                for(TextIterator iter = begin1 ;iter > end1 ; iter--)
                {
                    
                    if(*iter == kTextChar_CR)
                    {
                        prevEnterFound = kTrue;
                        break;
                    }
                    removethis++;
                    
                }
                if(prevEnterFound)
                {
                    delStart = delStart - removethis + 1;
                }
                else
                {
                    delStart = tagStartPos1 ;
                }
            }
            
            const XMLReference & XMLElementOfTagToBeDeleted = slugInfo.tagPtr->GetXMLReference();
            //ErrorCode err = Utils<IXMLElementCommands>()->DeleteElement(XMLElementOfTagToBeDeleted,kFalse);
            
            TextIterator begin(textModel , delStart);
            TextIterator end(textModel , tagEndPos1);
            int32 addthis = 0;
            bool16 enterFound = kFalse;
            for(TextIterator iter = begin ;iter < end ; iter++)
            {
                addthis++;
                if(*iter == kTextChar_CR)
                {
                    enterFound = kTrue;
                    break;
                }
            }
            if(enterFound)
            {
                delEnd = delStart + addthis;
            }
            else
            {
                delEnd = tagEndPos1;
            }
            RangeData rangeData(delStart,delEnd);
            //deleteTextRanges.push_back(rangeData);
        }
    }
*/

}


void CDataSprayer::arrangeForSprayingItemAttributeGroup(UIDRef & boxUIDRef,InterfacePtr<ITextModel> & tempTextModel)
{
    InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
    if(ptrIAppFramework == NULL)
    {
        //CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
        return;
    }
    
    //Get the textmodel first
    UIDRef txtModelUIDRef = UIDRef::gNull;
    UID textFrameUID = kInvalidUID;
    
    InterfacePtr<ITextModel>  textModel;
    getTextModelByBoxUIDRef(boxUIDRef , textModel);
    if (textModel == NULL)
    {
        return;
    }
    
    tempTextModel = textModel;
    
    InterfacePtr<ITagReader> itagReader
    ((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
    if(!itagReader)
    {
        return;
    }
    TagList tList=itagReader->getTagsFromBox(boxUIDRef);
    int numTags=static_cast<int> (tList.size());
    
    int32 field1_val = -1;
    if(numTags<0)
    {
        return;
    }
    
    bool16 isAtLeastOneItemAttributeGroupPresent = kFalse;
    
    map<double, vector<double>> mapOfAttributeGroup;
    vector<PMString> listOfKeyForMapelements;
    vector<double> listOfAttributeGrroupIdForMapelements; // need to make sure that we process tags from bottom to up in Text frrame

    double languageId = 0;
    int32 isDeleteIfEmpty =0;
    //Iterate thourgh the tList and collect the indices of all the item tags inside the vectorItemTagIndex.
    
    for(int32 tagIndex=0 ;tagIndex <numTags ;tagIndex++)
    {
        TagStruct & firstTagInfo = tList[tagIndex];
        XMLContentReference xmlCntnRef = firstTagInfo.tagPtr->GetContentReference();
        if(xmlCntnRef.IsTable())
        {
            //CA("This is the tag attached to the table");
            continue;
        }
        
        XMLReference firstTagInfoXmlRef = firstTagInfo.tagPtr->GetXMLReference();
        
        if( firstTagInfo.whichTab == 4 //ItemCopyAttribute identified
             && (firstTagInfo.dataType == 6)//filter TableInTabbedText
            && ((firstTagInfo.elementId) != -1)
        )
        {
            if( mapOfAttributeGroup.count(firstTagInfo.elementId) > 0)
            {
                mapOfAttributeGroup.find(firstTagInfo.elementId)->second.push_back(tagIndex);
                
            }
            else
            {
                vector<double> vectorItemTagIndex1;
                vectorItemTagIndex1.push_back(tagIndex);
                mapOfAttributeGroup.insert(map<double, vector<double>>::value_type(firstTagInfo.elementId , vectorItemTagIndex1));
                if(firstTagInfo.groupKey == "")
                    listOfKeyForMapelements.push_back("");
                else
                    listOfKeyForMapelements.push_back(firstTagInfo.groupKey);
                
                listOfAttributeGrroupIdForMapelements.push_back(firstTagInfo.elementId);

            }

            isAtLeastOneItemAttributeGroupPresent = kTrue;
            languageId = firstTagInfo.languageID;
            isDeleteIfEmpty = firstTagInfo.deleteIfEmpty;
        }
        else if(firstTagInfo.whichTab == 3 )
        {//Product tags are not welcome here.We are spraying Item.
            //Make the tag impotent.
                makeTheTagImpotent(firstTagInfo.tagPtr);
        }
        
    }
    
    if(isAtLeastOneItemAttributeGroupPresent == kFalse)
    {
        for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
        {
            tList[tagIndex].tagPtr->Release();
        }
        return;
    }
    
    bool16 firstTabbedTextTag = kTrue;
    tabbedTagStructList.clear();
    
    TextIndex startPos=-1;
    TextIndex endPos = -1;
    
    txtModelUIDRef = ::GetUIDRef(textModel);
    
    int32 tagIndex = 0;
    int32 tagIndexOfNewlyAddedTagDueToLinePaste = 0;
    int32 tagListSize = static_cast<int32> (tList.size());
    
    int32 firstCharacterIndexOfTheline = 0;
    TagStruct tagInfo;
    
    TextIterator startIterator(textModel,firstCharacterIndexOfTheline);
    if(startIterator.IsNull())
    {
        ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingItemAttributeGroup::startIterator.IsNull");
        return;
    }
    
    bool16 breakTheOuterLoop = kFalse;
    bool16 areItemIDsCollected = kFalse;
    
    int32 lineStartCharIndex = 0;
    int32 lineEndCharIndex = -1;
    
    int32 firstTagIndex =0;
    int32 lastTagIndex = 0;
    int32 updatedTagIndex = 0;
    
    //The loop below is the ThinkTank of this function.
    //what we are doing here is scanning each line in which the ItemTag is present.
    //We already collected the indices of the itemTags inside the vectorItemTagIndex vector.
    //so iterate through that vector.
    //scan each line of the item tag.
    //We are doing this because we have to copy the line for the number of times of size of FinalItemIds
    //CA("Start Arranging table");
    int32 finalItemIndex = 0;

    for (int32 mapIterCount=0; mapIterCount < listOfAttributeGrroupIdForMapelements.size(); mapIterCount++)
    {
        std::map<double,vector<double>>::iterator it;
        
        it = mapOfAttributeGroup.find(listOfAttributeGrroupIdForMapelements.at(mapIterCount));
        
        double keyAttributeGroupId = it->first;
        PMString key = listOfKeyForMapelements.at(mapIterCount);
        //CAlert::InformationAlert(key);
        PMString AttributeGroupId("");
        
        vector<double> itemAttrributeList;
        vector<double> FullitemAttrributeList;
 //       if(key != "")
 //           FullitemAttrributeList = ptrIAppFramework->StructureCache_getItemAttributeListforAttributeGroupKeyAndItemId(key, pNode.getPubId());
 //       else
            FullitemAttrributeList = ptrIAppFramework->getItemAttributeListforAttributeGroupIdAndItemId( CurrentSectionID, pNode.getPubId(), keyAttributeGroupId, languageId);
        
        if(FullitemAttrributeList.size() == 0 )
        {
            ptrIAppFramework->LogError("No Attributes found for AttributeGroupKey :" + key);
            continue;
        }
        
        if(isDeleteIfEmpty == 1)
        {
            for(int32 index=0; index < FullitemAttrributeList.size(); index++)
            {
                PMString itemAttributeValue = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(pNode.getPubId(),FullitemAttrributeList.at(index), languageId, CurrentSectionID, kFalse );
                
                if(itemAttributeValue == "")
                    continue;
                else
                    itemAttrributeList.push_back(FullitemAttrributeList.at(index));
            }
        }
        else
        {
            itemAttrributeList = FullitemAttrributeList;
        }

        
        
        
        for(int32 itemTagIndex = 0;itemTagIndex < it->second.size();itemTagIndex++)
        {
            //Get the actual index of the itemtag inside the taglist of the box.
            //now find the startIndex and endIndex of the line in which the item tag is present.
            
            vector<double> vectorItemTagIndex =it->second;
            int32 index = vectorItemTagIndex[itemTagIndex];

            lastTagIndex = index;            
            tagInfo=tList[index];
            
            TagList currentTagList = itagReader->getTagsFromBox(boxUIDRef);
            
            int32 startPos=-1;
            int32 endPos = -1;
            for(int32 currTLCt= 0; currTLCt < currentTagList.size(); currTLCt++ )
            {
                if(tagInfo.elementId == currentTagList.at(currTLCt).elementId
                   && tagInfo.dataType == currentTagList.at(currTLCt).dataType
                   && tagInfo.groupKey == currentTagList.at(currTLCt).groupKey
                )
                {
            
                    Utils<IXMLUtils>()->GetElementIndices(currentTagList.at(currTLCt).tagPtr,&startPos,&endPos);
                }
            }
            
            if(startPos <= lineEndCharIndex)
                continue;
            
            AttributeGroupId = tagInfo.tagPtr->GetAttributeValue(WideString("ID"));
            
            int32 len=2;
            UTF16TextChar utfChar[2]={kTextChar_Null,kTextChar_Null};
            //Find the starting character's index of the line
            TextIterator startIterator(textModel,startPos);
            do
            {
                if(startIterator.IsNull())
                {
                    //ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingItemAttributeGroup::startIterator.IsNull");
                    break;
                }
                
                while(kTrue)
                {
                    (*startIterator).ToUTF16(utfChar,&len);
                    if((utfChar[0] == kTextChar_CR) || (startIterator.IsNull())  || startPos == 0)
                        break;
                    
                    startIterator--;
                }
            }while(kFalse);
            
            if(startPos > 0)
                lineStartCharIndex = startIterator.Position() + 1;
            else
                lineStartCharIndex = 0;
            
            
            //Find the ending character's index of the line
            TextIterator endIterator(textModel,endPos);
            TextIterator endIterator1(textModel,endPos);
            do
            {
                if(endIterator.IsNull())
                {
                    //ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingItemAttributeGroup::endIterator.IsNull");
                    break;
                }
                int32 temp=0;
                while(kTrue)
                {
                    if((endIterator1.IsNull() == kTrue))
                    {
                        //CA("(endIterator.IsNull() == kTrue)");
                        break;
                    }
                    (*endIterator1).ToUTF16(utfChar,&len);
                    
                    if((utfChar[0] == kTextChar_CR))
                    {
                        temp++;
                    }
                    endIterator1++;
                }
                
                while(kTrue)
                {
                    if((endIterator.IsNull() == kTrue))
                    {
                        //CA("(endIterator.IsNull() == kTrue)");
                        break;
                    }
                    (*endIterator).ToUTF16(utfChar,&len);
                    
                    if((utfChar[0] == kTextChar_CR) || (endIterator.IsNull() == kTrue))
                    {
                        
                        if((utfChar[0] == kTextChar_CR) )
                        {
                            if(temp > 1)
                            {
                                endIterator++;
                            }
                            else
                            {
                                WideString* enterChar= new WideString("\r");
                                textModel->Insert(endIterator.Position(),enterChar);
                                CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
                                endIterator++;
                                delete enterChar;
                            }
                            
                        }
                        break;
                    }
                    endIterator++;
                }
            }while(kFalse);
            
            lineEndCharIndex = endIterator.Position();
            int32 numberOfTagsInALine = 0;
            int32 numberOfTimesTheLineToBeCopied = 0;
            
            
            CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
            InterfacePtr<ISelectionManager> selectionManager(Utils<ISelectionUtils>()->QueryActiveSelection ());
            if (selectionManager == nil)
            {
                //CA("selectionManager == nil");
                break;
            }
            // Deselect everything.
            if (selectionManager->SelectionExists (kInvalidClass/*any CSB*/, ISelectionManager::kAnySelection)) {
                // Clear the selection
                selectionManager->DeselectAll(nil);
            }
            //Find all the tags which are in the current line.
            for(int32 normalTagIndex = 0;normalTagIndex < numTags;normalTagIndex++)
            {
                TagStruct & tagInfo = tList[normalTagIndex];
                int32 startPos=-1;
                int32 endPos = -1;
                Utils<IXMLUtils>()->GetElementIndices(tagInfo.tagPtr,&startPos,&endPos);
                XMLReference tagInfoXmlRef = tagInfo.tagPtr->GetXMLReference();
                
                if(startPos >= lineStartCharIndex && endPos <= lineEndCharIndex)
                {
                    numberOfTagsInALine++;
                
                    numberOfTimesTheLineToBeCopied = static_cast<int32>(itemAttrributeList.size() - 1);
                    PMString attrIndex("index");
                    PMString indexValue  = tagInfo.tagPtr->GetAttributeValue(WideString(attrIndex));
                    PMString dataType = tagInfo.tagPtr->GetAttributeValue(WideString("dataType"));
                    PMString strTableFlag = tagInfo.tagPtr->GetAttributeValue(WideString("tableFlag"));
                    
                    PMString AttributeGroupId = tagInfo.tagPtr->GetAttributeValue(WideString("ID"));
                    if(strTableFlag == "-15")
                    {
                        continue;
                    }
                    else if(indexValue == "4" && dataType == "6")
                    {
                        PMString attributeValue;
                        attributeValue.AppendNumber(PMReal(itemAttrributeList[0]));
                        
                        if (selectionManager->SelectionExists (kInvalidClass, ISelectionManager::kAnySelection)) {
                            // Clear the selection
                            selectionManager->DeselectAll(nil);
                        }
                        
                        Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("ID"),WideString(attributeValue));
                        Utils<IXMLAttributeCommands>()->SetAttributeValue(tagInfoXmlRef, WideString("dataType"),WideString("-1"));
                        
                    }
                }
            }
            
            int32 textSelectionStartAt = lineEndCharIndex;
            int32 length = lineEndCharIndex -lineStartCharIndex + 1;
            int32 startPasteIndex = lineEndCharIndex + 1;
            if(lineEndCharIndex > lineStartCharIndex)
            {
                boost::shared_ptr< PasteData > pasteData;
                startPasteIndex--;
                TextIndex destEnd = startPasteIndex;
                for(int32 lineIndex=0;lineIndex<numberOfTimesTheLineToBeCopied;lineIndex++)
                {
                    do
                    {
                        //CA("Start Copying");
                        InterfacePtr<ICommand> copyStoryRangeCmd(CmdUtils::CreateCommand(kCopyStoryRangeCmdBoss));
                        if (copyStoryRangeCmd == nil) {//CA("copyStoryRangeCmd == nil");
                            break;
                        }
                        
                        // Refer the command to the source story and range to be copied.
                        InterfacePtr<IUIDData> sourceUIDData(copyStoryRangeCmd, UseDefaultIID());
                        if (sourceUIDData == nil) {//CA("sourceUIDData == nil");
                            break;
                        }
                        
                        sourceUIDData->Set(txtModelUIDRef);
                        InterfacePtr<IRangeData> sourceRangeData(copyStoryRangeCmd, UseDefaultIID());
                        if (sourceRangeData == nil) {//CA("sourceRangeData == nil");
                            break;
                        }
                        
                        sourceRangeData->Set(lineStartCharIndex, lineEndCharIndex);
                        
                        // Refer the command to the destination story and the range to be replaced.
                        UIDList itemList(txtModelUIDRef);
                        copyStoryRangeCmd->SetItemList(itemList);
                        
                        InterfacePtr<IRangeData> destRangeData(copyStoryRangeCmd, IID_IRANGEDATA2);
                        
                        destRangeData->Set(startPasteIndex, destEnd );
                        
                        // Process CopyStoryRangeCmd
                        ErrorCode status = CmdUtils::ProcessCommand(copyStoryRangeCmd);
                        
                    }while(kFalse);
                    
                    startPasteIndex = startPasteIndex + length -1;
                    destEnd = startPasteIndex;
                    //CA_NUM("startPasteIndex : ",startPasteIndex);
                    //CA_NUM("destEnd : ",destEnd);
                }
                int32 textSelectionEndAt = startPasteIndex-1;
                TagList newTagList = itagReader->getTagsFromBox(boxUIDRef);
                int32 pastedTagCount = 0;
                for(int32 newTagIndex = 0;newTagIndex < newTagList.size();newTagIndex++)
                {
                    TagStruct & newTagInfo = newTagList[newTagIndex];
                    
                    int32 newStartPos = -1;
                    int32 newEndPos = -1;
                    Utils<IXMLUtils>()->GetElementIndices(newTagInfo.tagPtr,&newStartPos,&newEndPos);
                    if((newStartPos >= textSelectionStartAt) && (newEndPos <= textSelectionEndAt))
                    {
                        PMString attrIndex("index");
                        PMString indexValue  = newTagInfo.tagPtr->GetAttributeValue(WideString(attrIndex));
                        PMString groupKey  = newTagInfo.tagPtr->GetAttributeValue(WideString("groupKey"));
                        PMString strID = newTagInfo.tagPtr->GetAttributeValue(WideString("ID"));
                        XMLReference newTagInfoXmlRef = newTagInfo.tagPtr->GetXMLReference();
                        
                        double itemAttributeId =-1;
                        int32 itemIdSeqNo = 0;

                        itemAttributeId = itemAttrributeList[pastedTagCount / numberOfTagsInALine + 1];
                        itemIdSeqNo = (pastedTagCount / numberOfTagsInALine + 1);

                        
                        PMString attributeValue;
                        attributeValue.AppendNumber(PMReal(itemAttributeId));

                        if(indexValue == "4")
                        {
                            //CAlert::InformationAlert("indexValue == 4 &&  groupKey != ");
                            Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("ID"),WideString(attributeValue));
                            Utils<IXMLAttributeCommands>()->SetAttributeValue(newTagInfoXmlRef, WideString("dataType"),WideString("-1"));
                        }
                        pastedTagCount++;	
                    }
                }
                
                // Now create outer Tag for AttrributeGroup
                {
                    UIDRef textModelUIDRef =::GetUIDRef(textModel);
                    
                    PMString tableTagName = "AttributeGroup_" + key;
                    
                    ErrorCode err = kFailure;
                    XMLReference CreatedElement;
                    const XMLReference &  parentXMLR = kInvalidXMLReference; 
                    err = Utils<IXMLElementCommands>()->CreateElement(WideString(tableTagName), 
                                                                      textModelUIDRef,
                                                                      lineStartCharIndex, startPasteIndex -1,parentXMLR,& CreatedElement);
                    
                    
                    
                    PMString attribName("ID");
                    PMString attribVal = AttributeGroupId;
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName = "typeId";
                    attribVal = "-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName = "header";
                    attribVal.AppendNumber(PMReal(newTagList[0].header));
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName = "isEventField";
                    attribVal = "-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName = "deleteIfEmpty";
                    attribVal = "-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName = "dataType";
                    attribVal = "6";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="isAutoResize";
                    attribVal="";
                    attribVal.AppendNumber(PMReal(newTagList[0].isAutoResize));
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="LanguageID";
                    attribVal="";
                    attribVal.AppendNumber(PMReal(newTagList[0].languageID));
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName = "index";
                    attribVal = "4";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName = "pbObjectId";
                    attribVal = "-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="parentID";
                    attribVal="-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="childId";
                    attribVal="-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="sectionID";
                    attribVal="-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="parentTypeID";
                    attribVal="-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="isSprayItemPerFrame";
                    attribVal="-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="catLevel";
                    attribVal="-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="imgFlag";
                    attribVal="0";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="imageIndex";
                    attribVal="-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="flowDir";
                    attribVal="-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="childTag";
                    attribVal="-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="tableFlag";
                    attribVal="0";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="tableType";
                    attribVal="-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="tableId";
                    attribVal="-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));				
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="rowno";
                    attribVal="-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="colno";
                    attribVal="-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="field1";
                    attribVal="-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="field2";
                    attribVal="-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));	
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="field3";
                    attribVal="-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));	
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="field4";
                    attribVal="-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));	
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="field5";
                    attribVal="-1";
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                    attribName="groupKey";
                    attribVal= key;
                    err = Utils<IXMLAttributeCommands>()->CreateAttribute(CreatedElement,WideString(attribName),WideString(attribVal));
                    
                    attribName.Clear();
                    attribVal.Clear();
                }
                
                
                for(int32 tagIndex = 0 ; tagIndex < newTagList.size() ; tagIndex++)
                {
                    newTagList[tagIndex].tagPtr->Release();
                }
            }
            firstTagIndex = index;
        }
    } // for Map
    
    if(tList.size() > 0)
    {
        for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
        {
            tList[tagIndex].tagPtr->Release();
        }
    }
}
