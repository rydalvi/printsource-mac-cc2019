#ifndef _TABLEUTILITY_H__
#define _TABLEUTILITY_H__

class PublicationNode;
#include "vector"
#include "ITableModel.h"
#include "ITableTextSelection.h"
#include "XMLReference.h"
#include "SlugStructure.h"
#include "ITool.h"
#include "TagStruct.h"
#include "ITableUtility.h"
#include "ItemTableValue.h"
//#include "CMMYTable.h"


#include "XMLTagAttributeValue.h"

#define DRAGMODE		1
#define CONTENTSMODE	2
using namespace std;

class TableUtility:public CPMUnknown<ITableUtility>
{
public:

	TableUtility(IPMUnknown* boss);

	~TableUtility()
	{
		//Close the file
	}
	//TableUtility();

	bool16 isTablePresent(const UIDRef&, UIDRef&);
	void fillDataInTable(const UIDRef&, PublicationNode&, TagStruct& , double&,const UIDRef&);
	//void fillDataInTable(const UIDRef , int32 objectId, int32 ,int32& );
	void resizeTable(const UIDRef&, const int32&, const int32&, bool16,const int32&,const int32&);
	void setTableRowColData(const UIDRef&, const PMString&, const int32&, const int32&);
	void putHeaderDataInTable(const UIDRef&, vector<double>, bool16, TagStruct& , UIDRef boxUIDRef);
	void SetSlug(const UIDRef&, vector<double>, vector<double>,bool8);
	void Slugreader(const UIDRef&, const int32&, const int32&,double&,double&);
	PMString TabbedTableData(const UIDRef&, PublicationNode&, double, double&, TagStruct&);
	void AddTexttoTabbedTable(const UIDRef&,PMString,vector<double>, vector<double>,bool8);
	void AddSlugtoText(double,double);
	void IterateThroughTable(InterfacePtr<ITableModel>&);
	void NewSlugreader(InterfacePtr<ITableModel> &tblModel, const int32& rowIndex, const int32& colIndex,double& Attr_id,double& Item_id,InterfacePtr<ITableTextSelection> &tblTxtSel);
	void TextSlugReader(const UIDRef& tableUIDRef);
	bool8 IsAttrInTextSlug(const UIDRef& tableUIDRef,double Attr_id);
	bool8 UpdateDataInTextSlug(const UIDRef& tableUIDRef,double Attr_id,PMString Table_col);
	ErrorCode ProcessSimpleCommand(const ClassID& commandClass, const UIDList& itemsIn, UIDList& itemsOut);

	void attachAttributes(XMLReference *newTag, bool16 IsPRINTsourceRootTag, SlugStruct& stencilInfoData, int rowno, int colno , int ProductStandardTableCase = 0);
	ErrorCode TagTableCellText(InterfacePtr<ITableModel> &tableModel,XMLReference &parentXMLRef, GridID id,GridArea gridArea, XMLReference &CellTextxmlRef, SlugStruct& stencilInfoData);
	ErrorCode AddTableAndCellElements(const UIDRef& tableModelUIDRef, const UIDRef& BoxUIDRef,
								const PMString& tableTagName,
								const PMString& cellTagName,
								XMLReference& outCreatedXMLReference,
								double TableID,
								SlugStruct& stencilInfoData								
								);
	ErrorCode TagStory(const PMString& tagName,const UIDRef& textModelUIDRef, const UIDRef& boxRef);
	ErrorCode TagTable(const UIDRef& tableModelUIDRef,UIDRef BoxRef,
								const PMString& tableTagName,
								const PMString& cellTagName,
								XMLReference& outCreatedXMLReference,
								SlugStruct& stencilInfoData,
								int32 rowno, int32 colno, double TableID);
	UIDRef AcquireTag(const UIDRef& boxUIDRef, const PMString& tagName, bool16 &ISTagPresent);
	int16 convertBoxToTextBox(UIDRef boxUIDRef);
	int changeMode(int whichMode);
	//ITool* queryTool(const ClassID& toolClass);//------

	ErrorCode SprayUserTableAsPerCellTagsForOneItem(const UIDRef& boxUIDRef, TagStruct& slugInfo, PublicationNode& pNode);
	void SetDBTableStatus(bool16 Status);
	ErrorCode SprayUserTableAsPerCellTagsForMultipleItem(const UIDRef& boxUIDRef, TagStruct& slugInfo, PublicationNode& pNode);
	ErrorCode CopyTables(const UIDRef& sourceStory, int32 StartIndexForNewTable, int32 repeteCopy);
	ErrorCode ProcessCopyStoryRangeCmd(
			const UIDRef& sourceStory, 
			TextIndex sourceStart, 
			int32 sourceLength, 
			const UIDRef& destStory,
			TextIndex destStart, 
			int32 destLength
		);

	//added on 26Jun by Yogesh for Individual Item_copy_attributes.
	ErrorCode SprayIndividualItemAndNonItemCopyAttributesInsideTable(const UIDRef& boxUIDRef, TagStruct& slugInfo, PublicationNode& pNode);
	//ended on 26Jun by Yogesh for Individual Item_copy_attributes.
	//17Aug addition
	ErrorCode SprayIndividualSectionLevelItemAndNonItemItemCopyAttributesInsideTable(const UIDRef& boxUIDRef, TagStruct& slugInfo, PublicationNode& pNode);
	

	//added on 11July...the serial blast day
	void GetItemTableInTabbedTextForm(TagStruct & tagInfo,PMString & tabbedTextData);
	//end added on 11July...the serial blast day

	bool16 FillDataInsideItemTableOfItem
		(
			XMLReference & boxXMLRef,
			UIDRef & tableModelUIDRef

		);
	//4Aug..ItemTable
	void fillDataInTableForItem(const UIDRef&, PublicationNode&, TagStruct& , double&,const UIDRef&);

	void AddTagToCellText
				(
					const GridAddress & cellAddr,
					const InterfacePtr<ITableModel> & tableModel,
                    PMString & dataToBeSprayed,
					InterfacePtr<ITextModel> & textModel,
					const XMLTagAttributeValue xmlTagAttrVal
				);

//following method is added by vijay on 16-10-2006
	//void fillDataInCMedCustomTable(const UIDRef& , PublicationNode& , TagStruct& ,int32& ,const UIDRef& );

	void fillDataInKitComponentTable
		(const UIDRef& tableUIDRef, PublicationNode& pNode, TagStruct& tStruct,double& tableId,const UIDRef& BoxUIDRef, bool16 isKitTable);

	void putHeaderDataInKitComponentTable
		(const UIDRef& tableUIDRef, vector<double> vec_tableheaders, bool16 isTranspose, TagStruct& tStruct, UIDRef boxUIDRef);

	void sprayTableInsideTableCell(UIDRef& boxUIDRef,TagStruct & tagInfo);

	bool16 FillDataTableForItemOrProduct
			(
				XMLReference & boxXMLRef,
				UIDRef & tableModelUIDRef,
				int32 isProduct
			);

	void fillDataInCMedCustomTableInsideTable(XMLReference & boxXMLRef, UIDRef& tableUIDRef, PublicationNode& pNode);

	void fillDataInXRefTable(const UIDRef& tableUIDRef, PublicationNode& pNode, TagStruct& tStruct,double& tableId,const UIDRef& BoxUIDRef);
	void putHeaderDataInXRefTable(const UIDRef& tableUIDRef, vector<double> vec_tableheaders, bool16 isTranspose, TagStruct& tStruct, UIDRef boxUIDRef);
	
	void fillDataInAccessoryTable(const UIDRef& tableUIDRef, PublicationNode& pNode, TagStruct& tStruct,double& tableId,const UIDRef& BoxUIDRef);
	void putHeaderDataInAccessoryTable(const UIDRef& tableUIDRef, vector<double> vec_tableheaders, bool16 isTranspose, TagStruct& tStruct, UIDRef boxUIDRef);
	
	void fillDataInHybridTable(const UIDRef& tableUIDRef, PublicationNode& pNode, TagStruct& tStruct,double& tableId,const UIDRef& BoxUIDRef);
	//void putHeaderDataInHybridTable(const UIDRef&, vector<vector<PMString> >, bool16, TagStruct& , UIDRef boxUIDRef, vector<vector<CAdvanceTableCellValue> > &);
	void resizeTableForHybridTable(const UIDRef&, const int32&, const int32&, bool16,const int32&,const int32&,const int32&);

	ErrorCode InsertInline(const UIDRef& storyUIDRef, const TextIndex& whereTextIndex,int32 cellHeight);
	ErrorCode CreateFrame(IDataBase* database, UIDRef& newFrameUIDRef,int32 cellHeight);
	ErrorCode ChangeToInline(const UIDRef& storyUIDRef, const TextIndex& whereTextIndex, const UIDRef& frameUIDRef);
	void fillDataInTableForAllStandardTable(PublicationNode& pNode,/* TagStruct& tStruct, */double& tableId,UIDRef& boxUIDRef);
	void sprayAllStandardTables(bool8 isTranspose,const UIDRef& BoxUIDRef,const UIDRef& tableUIDRef, TagStruct& tStruct,CItemTableValue oTableValue,int32 numRows, int32 numCols, vector<double> vec_tableheaders,vector<double> vec_items,double& tableId);
	ErrorCode ReplaceText(ITextModel* textModel, const TextIndex position, const int32 length, const /*K2*/boost::shared_ptr<WideString>& text);//---CS5--	

	ErrorCode SprayMultipleCustomTableFormTABLEsource(const UIDRef& boxUIDRef, TagStruct& slugInfo, PublicationNode& pNode);

	ErrorCode SprayMMYCustomTable(const UIDRef& boxUIDRef, TagStruct& slugInfo, PublicationNode& pNode);
	//int32 getMMYTableRows(const CMMYTable* MMYTablePtr, int32 index);
	//int32 TotalNoOfItemsPerMake(const CMMYTable* MMYTablePtr, const int32 selectedTableIndex);

	bool16 InspectChars(InterfacePtr<ITextModel>& textModel,TextIndex startIndex, TextIndex endIndex, TextIndex& tableStartIndex, TextIndex& tableEndIndex);
	PMString GetCharacter(UTF32TextChar character);
    
    void putListNameInTable(const UIDRef& tableUIDRef, PMString listName, bool16 isTranspose, TagStruct& tStruct, UIDRef boxUIDRef, int32 numCols);

    ErrorCode SprayItemAttributeGroupInsideTable(const UIDRef& boxUIDRef, TagStruct& slugInfo, PublicationNode& pNode);
};

#endif
