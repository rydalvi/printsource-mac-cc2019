#ifndef __COMMONFUNCTIONS_H__
#define __COMMONFUNCTIONS_H__

#include "VCPluginHeaders.h"
#include "XMLTagAttributeValue.h"
#include "XMLReference.h"

#include "PublicationNode.h"
#include "TagStruct.h"
PMString prepareTagName(PMString name);
PMString keepOnlyAlphaNumeric(PMString name);

void setAllXMLTagAttributeValues(XMLReference & boxXMLRef ,const XMLTagAttributeValue & xmlTagAttrVal);
void createAllXMLTagAttributes(XMLReference & xmlRef,const XMLTagAttributeValue & tagAttrVal);
void getAllXMLTagAttributeValues(XMLReference & xmlTagReference,XMLTagAttributeValue & tagAttrVal);

void makeTheTagImpotent(IIDXMLElement * xmlTagPtr);
bool8 getAttributeIDFromNotes(const int32 & isProduct,const double & sectionid,const double & pubObjectId,const  double & selectedItemID,PMString & attributeIDfromNotes);

void handleSprayingOfEventPriceRelatedAdditions(PublicationNode & pNode,TagStruct & tagInfo,PMString & itemAttirbuteValue);
bool8 getAttributeIDFromNotesNew(PublicationNode & pNode ,const double & sectionid,const  double & selectedItemID,PMString & attributeIDfromNotes);
void convertTagStructToXMLTagAttributeValue(const TagStruct & tagInfo,XMLTagAttributeValue & tagVal);
//added by Tushar on 18/12/06 
bool8 getAttributeIDFromNotesNewByElementId(PublicationNode & pNode ,const double & sectionid,const  double & selectedItemID,PMString & attributeIDfromNotes,double eventPriceTableTypeID);
void makeTagInsideCellImpotent(IIDXMLElement * tableXMLElementPtr);

//added by Tushar on 15/01/07 
bool8 getAttributeIDFromNotes(PublicationNode & pNode ,const double & sectionid,const  double & selectedItemID,PMString & attributeIDfromNotesSales,PMString & attributeIDfromNotesRegular,double SalePriceTabletypeId,double RegularPriceTabletypeId);
void handleSprayingOfEventPriceRelatedAdditionsNew(PublicationNode & pNode,TagStruct & tagInfo,PMString & itemAttributeValue);
#endif
