#include "VCPluginHeaders.h"
#ifndef __THREADTEXTFRAME_H__
#define __THREADTEXTFRAME_H__

#include "IDatabase.h"
//#include "ITextFrame.h"

class ThreadTextFrame
{
private:
public:
	//bool16 getMarginBounds(const UIDRef& pageUIDRef, PMRect& marginBoxBounds);
	bool16 getCurrentPage(UIDRef& pageUIDRef, UIDRef& spreadUIDRef);
	UID GetTextContentUID(const UIDRef& graphicFrameUIDRef);
	bool16 CanThreadTextFrames(IDataBase* database, const UID& fromGraphicFrameUID, const UID& toGraphicFrameUID);
	//bool16 IsTextFrameOverset(const InterfacePtr<ITextFrame> textFrame);
	bool16 IsTextFrameOverset(const InterfacePtr<ITextFrameColumn> textFrameColumn);
	//bool16 RecomposeTextFrame(const InterfacePtr<ITextFrame> textFrame);
	bool16 RecomposeTextFrame(const InterfacePtr<ITextFrameColumn> textFrame);
	ErrorCode ThreadTextFrames(IDataBase* database, const UID& fromGraphicFrameUID, const UID& toGraphicFrameUID);
	UIDRef CreateAndThreadTextFrame(const UIDRef& fromGraphicFrameUIDRef);
//public:
	ThreadTextFrame(){}
	~ThreadTextFrame(){}
	
	void DoCreateAndThreadTextFrame(UIDRef& fromGraphicFrameUIDRef);
//10-april
	bool16 getMarginBounds(const UIDRef& pageUIDRef, PMRect& marginBoxBounds);
//10-april
};

#endif