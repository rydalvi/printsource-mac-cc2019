#ifndef __ISlugData_h__
#define __ISlugData_h__

#include "IPMUnknown.h"
#include "DSFID.h"
#include "SlugStructure.h"
#include "vector"

using namespace std;


class PMString;

class ISlugData : public IPMUnknown
{
public:
	enum	{kDefaultIID = IID_ISLUGDATA};
	virtual void SetList(SlugList&, PMString&, PMString&) = 0;
	virtual const bool16 GetList(SlugList&, PMString&, PMString&) = 0;
};

#endif
