
#include "VCPlugInHeaders.h"
#include "CSWFMediatorClass.h"
#include "IControlView.h"

//UIDRef		CSWFMediatorClass::curSelItemUIDRef=-1;
int32       CSWFMediatorClass::count1=-1;
int32       CSWFMediatorClass::count2=-1;
int32       CSWFMediatorClass::count3=-1;
int32       CSWFMediatorClass::count4=-1;
int32       CSWFMediatorClass::count5=-1;
int32		CSWFMediatorClass::curSelRowIndex=-1;
PMString	CSWFMediatorClass::curSelRowString("");
int32		CSWFMediatorClass::curSelRadBtn=1;	
int32		CSWFMediatorClass::curSelLstbox=1;	
bool16		CSWFMediatorClass::imageFlag=kFalse;
double		CSWFMediatorClass::lastSelClasId=0;
PMString	CSWFMediatorClass::lastSelClasName("");
PMString	CSWFMediatorClass::lastSelClasHierarchy("");
double		CSWFMediatorClass::lastSelClasIdForRefresh=0;
PMString	CSWFMediatorClass::lastSelClasNameForRefresh("");
PMString	CSWFMediatorClass::lastSelClasHierarchyForRefresh("");
IControlView* CSWFMediatorClass::PFLstboxCntrlView=nil;
IControlView* CSWFMediatorClass::PGLstboxCntrlView=nil;
IControlView* CSWFMediatorClass::PRLstboxCntrlView=nil;
IControlView* CSWFMediatorClass::ItemLstboxCntrlView=nil;
IControlView* CSWFMediatorClass::ProjectLstboxCntrlView=nil;

// Added by Awasthi
IControlView* CSWFMediatorClass::dropdownCtrlView1=nil;
IControlView* CSWFMediatorClass::refreshBtnCtrlView1=nil;
IControlView* CSWFMediatorClass::appendRadCtrlView1=nil;
IControlView* CSWFMediatorClass::embedRadCtrlView1=nil;
IControlView* CSWFMediatorClass::tagFrameChkboxCtrlView1=nil;
// End Awasthi

IPanelControlData* CSWFMediatorClass::iPanelCntrlDataPtr=nil;
IPanelControlData* CSWFMediatorClass::iPanelCntrlDataPtrTemp=nil;
int32 CSWFMediatorClass::tableFlag=0;
bool16 CSWFMediatorClass::loadData=kFalse;
//CSWFMediaterClass::CSWFListboxData


CLanguageModel CSWFMediatorClass::CurrentLanguageModel;
int32 CSWFMediatorClass::LanguageModelCacheSize = -1;

ActionID CSWFMediatorClass::CurrentLangActionID = -1;
double CSWFMediatorClass::CurrLanguageID = -1;

bool16 CSWFMediatorClass::doReloadFlag = kFalse;

//added by vijay choudhari on 24-04-2006 . int32 of the frame(Active frame which
//has cursor)is stored in following variable to make it persistent.
int32	CSWFMediatorClass::initialSelItemUIDRef = 0;
int32	CSWFMediatorClass::initialCaratPosition = 0;
int32   CSWFMediatorClass::overLapingStatus = 0;
int32   CSWFMediatorClass::addTagToTextDoubleClickVersion = 0;
int32   CSWFMediatorClass::textSpanOFaddTagToTextDoubleClickVersion = 0;
int32   CSWFMediatorClass::appendTextIntoSelectedAndOverlappedBox = 0;
int32	CSWFMediatorClass::isCaratInsideTable = 0;
bool16  CSWFMediatorClass::isInsideTable = kFalse;
bool16	CSWFMediatorClass::checkForOverlapWithTextFrame = kFalse; 
bool16	CSWFMediatorClass::checkForOverlapWithMasterframe = kFalse;
UIDList	CSWFMediatorClass::UIDListofMasterPageItems;
//added by vijay on 9-9-2006
bool16	CSWFMediatorClass::IsOneSourceMode = kFalse;
//added by vijay on 9-11-2006
bool16	CSWFMediatorClass::IsTableInsideTableCell= kFalse;

//----
double CSWFMediatorClass::currTempletSelectedClassID = -1 ;
