#include "VCPluginHeaders.h"
#include "ISelectionManager.h"
#include "SelectionObserver.h"
#include "CSWFID.h"
#include "Trace.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "CAlert.h"
#include "IWidgetParent.h"
#include "ISubject.h"
#include "ISelectableDialogSwitcher.h"
#include "IAppFramework.h"
//#include "CSWFListboxData.h"
//#include "SDKListBoxHelper.h"
#include "IListBoxController.h"
#include "ITextControlData.h"
//#include "IClassificationTree.h"
#include "CSWFMediatorClass.h"
#include "IDialogController.h"
//#include "CSWFCommonFunctions.h"
#include "ITriStateControlData.h"
#include "CSWFSelectionObserver.h"
//#include "CSWFActionComponent.h"
#include "IListBoxController.h"
#include "IEventHandler.h"
#include "IEventDispatcher.h"
#include "IApplication.h"
#include "CSWFDataNode.h"
#include "ITreeViewMgr.h"
#include "CSWFTreeModel.h"
#include "IClientOptions.h"
#include "ICategoryBrowser.h"
#include "AcquireModalCursor.h"
#include "IContentSprayer.h"
#include "ILoginHelper.h" //17-may
#include "IStringListControlData.h"
#include "ILoginHelper.h"
#include "CSWFDataNode.h"

//#include "PRImageHelper.h"

/*CSWFListboxData PFData;
CSWFListboxData PGData;
CSWFListboxData PRData;
CSWFListboxData ItemData;
CSWFListboxData ProjectData;
CSWFListboxData CatagoryData;

//bool16 isAddAsDisplayName;

extern int32 ProjectRow,PRRow,PGRow,PFRow,ItemRow;
*/
 
int32 SelectedRowNo;  //---------4 - Item , 3 - Item Group/Family , 0 - Event or Category----

CSWFDataNodeList WorkTaskDataNodeList;
int32 Flag1;
/*
CSWFDataNodeList CatagoryDataNodeList;
CSWFDataNodeList PFDataNodeList;
CSWFDataNodeList PGDataNodeList;
CSWFDataNodeList PRDataNodeList;
CSWFDataNodeList ITEMDataNodeList;
bool16 IsRefresh = kFalse;
int32 PM_Product_Level;

double CurrentClassID = -1;
bool16 ifCAllFromLanguageDropDown = kFalse;
bool16 ifCallFromLoadPalletDataLangSetting = kFalse;


extern bool16 ISTabbedText;//kTrue;
extern bool16 isAddTableHeader;
extern bool16 isAddAsDisplayName;
extern bool16 isAddImageDescription;
extern bool16 isOutputAsSwatch;
bool16 deleteIfEmpty = kFalse;
extern bool16 isAddListName;
//---------
bool16 isHorizontalFlow = kFalse;
bool16 isSpreadBasedLetterKeys = kFalse;


int32 intSprItemPerFrameFlow = -1; //--------
double table_TypeID = -1;
*/
//MessageServer


//#include "IMessageServer.h"
#define FILENAME			PMString("CSWFSelectionObserver.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAlert::InformationAlert(X)//CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}


CREATE_PMINTERFACE(CSWFSelectionObserver, kCSWFSelectionObserverImpl)

CSWFSelectionObserver::CSWFSelectionObserver(IPMUnknown *boss) :
	ActiveSelectionObserver(boss) {}

CSWFSelectionObserver::~CSWFSelectionObserver() {}

void CSWFSelectionObserver::HandleSelectionChanged(const ISelectionMessage* message)
{
	UpdatePanel();
}

void CSWFSelectionObserver::AutoAttach()
{
	//CA("AutoAttatch");
	
	ActiveSelectionObserver::AutoAttach();
	
	InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
	if (iPanelControlData==nil)
	{
	//	CA("iPanelControlData is nil");
		return;
	}
	/* Storing IPanelControlData in Mediator */
	CSWFMediatorClass::iPanelCntrlDataPtr=iPanelControlData;
	
	//AttachWidget(CSWFMediatorClass::iPanelCntrlDataPtr, kCSWFDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
	//AttachWidget(CSWFMediatorClass::iPanelCntrlDataPtr, kCSWFClassTreeIconSuiteWidgetID, IID_ITRISTATECONTROLDATA);
    

	loadPaletteData();
}

void CSWFSelectionObserver::loadPaletteData(void) 
{
	//CA("CSWFSelectionObserver::loadPaletteData");

	static int i;

		do
		{				
			/* Check if the user has LoggedIn successfully */
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
			{
				//CA("ptrIAppFramework == nil");			
				return;
			}
		
			bool16 result=ptrIAppFramework->getLoginStatus();
			
            
			if(CSWFMediatorClass::iPanelCntrlDataPtr==nil)
			{	
				//CA("In CSWFMediatorClass::iPanelCntrlDataPtr==nil");
				InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
				if (iPanelControlData==nil)
				{
					//CA("iPanelControlData is nil");
					CSWFMediatorClass::iPanelCntrlDataPtr=CSWFMediatorClass::iPanelCntrlDataPtrTemp;
				}				
				else
				{
                    // Storing IPanelControlData in Mediator
                    CSWFMediatorClass::iPanelCntrlDataPtr=iPanelControlData;//
                    CSWFMediatorClass::iPanelCntrlDataPtrTemp=iPanelControlData;
				}
				i++;
			}
            

			//CSWFActionComponent actionObsever(this);
			//actionObsever.DoPalette();
	

            /*
			
			IControlView* refreshBtnCtrlView=
				CSWFMediatorClass::iPanelCntrlDataPtr->FindWidget(kCSWFRefreshIconSuiteWidgetID);
			if(!refreshBtnCtrlView)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::loadPaletteData::!refreshBtnCtrlView");			
				break;
			}
             */
			
			

			IControlView* TreeCtrlView = CSWFMediatorClass::iPanelCntrlDataPtr->FindWidget(kProjectTreeViewWidgetID);
			if(TreeCtrlView==nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::loadPaletteData::TreeCtrlView == nil");			
				break;
			}
            
			//CSWFMediatorClass::refreshBtnCtrlView1=refreshBtnCtrlView;
			CSWFMediatorClass::ProjectLstboxCntrlView=TreeCtrlView;

            
			if(!result)  
			{	
				//CA("Invalid selection.  Please log into PRINTsource before continuing.");
				
                //refreshBtnCtrlView->Disable();
				TreeCtrlView->Disable();
				Flag1 = 1;
				break;
			}
			
			else
			{	
				//CA("Else part");
				//refreshBtnCtrlView->Enable();

				TreeCtrlView->Enable();

			}
			
			
			if(result)
			{  
				//CA("Inside result");	

				//if(CSWFMediatorClass::CurrLanguageID == -1) 
				{
					
					InterfacePtr<IClientOptions> cop((IClientOptions*) ::CreateObject(kClientOptionsReaderBoss,IID_ICLIENTOPTIONS));
					if(cop==nil){
						ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::loadPaletteData::cop==nil");
						break;
					}

					PMString LocaleName("");
					CSWFMediatorClass::CurrLanguageID = cop->getDefaultLocale(LocaleName);

					VectorLanguageModelPtr VectorLocValPtr = ptrIAppFramework->StructureCache_getAllLanguages();
					if(VectorLocValPtr == NULL)
					{
						ptrIAppFramework->LogError("AP7_TemplateBuilder::DynMnuDynamicMenu::RebuildMenu::StructureCache_getAllLanguages's VectorLocValPtr == NULL");
						break;
					}
					ptrIAppFramework->setLocaleId(VectorLocValPtr->at(0).getLanguageID());
					CSWFMediatorClass::CurrLanguageID = VectorLocValPtr->at(0).getLanguageID();

					
				}
				//CA(LocaleName);

				populateProjectPanelLstbox();

				IControlView* ProjectPanelCtrlView = 
					CSWFMediatorClass::iPanelCntrlDataPtr->FindWidget(kProjectTreeViewWidgetID);
				if (ProjectPanelCtrlView == nil)
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::loadPaletteData::ProjectPanelCtrlView == nil");
					break;
				}
			
				CSWFMediatorClass::ProjectLstboxCntrlView=ProjectPanelCtrlView;
				
				if(Flag1 == 1 ||  CSWFMediatorClass::doReloadFlag == kFalse)
				{
					//CA("Flag1 =1");	
					CSWFMediatorClass::doReloadFlag = kTrue;
									
					ProjectPanelCtrlView->ShowView(kTrue);

					//Flag1 =0;

					InterfacePtr<ITreeViewMgr> treeViewMgr(CSWFMediatorClass::ProjectLstboxCntrlView, UseDefaultIID());
					if(!treeViewMgr)
					{
						ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::loadPaletteData::!treeViewMgr");					
						return;
					}
						
					CSWFTreeModel pModel;
					PMString pfName("Root");
					pModel.setRoot(-1, pfName, 1);
					treeViewMgr->ClearTree(kTrue);
					pModel.GetRootUID();
					treeViewMgr->ChangeRoot();
					
				}
				else if(Flag1 == 0)
				{  
					
					InterfacePtr<ITreeViewMgr> treeViewMgr(CSWFMediatorClass::ProjectLstboxCntrlView, UseDefaultIID());
					if(!treeViewMgr) { 
						ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::loadPaletteData::!treeViewMgr");
						return;
					}
						
					CSWFTreeModel pModel;
					PMString root("Root");

                    pModel.setRoot(-1, root, 1);

					
					treeViewMgr->ClearTree(kTrue);
					pModel.GetRootUID();
					treeViewMgr->ChangeRoot();

					//CA("After Reload.........");
				
				//	else
				//		CA("Chkbox unselcted");
			
				}
			}
			else
			{				
				//Awasthi
				//sectionDropListCntrlView->Disable();
				//CA("Again Closing");
				//CSWFActionComponent actionObsever(this);
				//actionObsever.CloseTemplatePalette();
			}
			
		
		} while (kFalse);
		//CA("5");
	
}


void CSWFSelectionObserver::AutoDetach()
{
	//CA("AutoDetach");
	ActiveSelectionObserver::AutoDetach();
	CSWFMediatorClass::loadData=kFalse;
	do
	{
		//DetachWidget(CSWFMediatorClass::iPanelCntrlDataPtr, kCSWFDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
	//	DetachWidget(CSWFMediatorClass::iPanelCntrlDataPtr, kCSWFClassTreeIconSuiteWidgetID, IID_ITRISTATECONTROLDATA);

		
	} while (kFalse);

}

void CSWFSelectionObserver::setTriState
(const WidgetID&  widgetID, ITriStateControlData::TriState state)
{
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return;
	}
	do 
	{
		/*
		InterfacePtr<IPanelControlData> iPanelControlData(this, UseDefaultIID());
		if(iPanelControlData==nil) 
			break;

		//CSWFMediatorClass::iPanelCntrlDataPtr=iPanelControlData;
		*/
		IControlView * iControlView=CSWFMediatorClass::iPanelCntrlDataPtr->FindWidget(widgetID);
		if(iControlView==nil) 
		{
			//ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::setTriState::iControlView == nil");		
			break;
		}
		
		InterfacePtr<ITriStateControlData> itristatecontroldata(iControlView, UseDefaultIID());
		if(itristatecontroldata==nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::setTriState::itristatecontroldata == nil");
			break;
		}

		itristatecontroldata->SetState(state);
	} while(kFalse);	
}

void CSWFSelectionObserver::Update
(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy)
{
	if ((protocol == IID_ILISTCONTROLDATA) && (theChange == kListSelectionChangedByUserMessage)|| (theChange == kListSelectionChangedMessage))//kListSelectionChangedByUserMessage) ) 
	{
	//	CA("Inside if added by Prabhat");
	}


	//CA("CSWFSelectionObserver::Update");

	ActiveSelectionObserver::Update(theChange, theSubject, protocol, changedBy);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return;
	}
	
	do
	{
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			//ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::Update::controlView == nil");
			return;	
		}

		WidgetID theSelectedWidget = controlView->GetWidgetID();
        /*
		if (theSelectedWidget == kCSWFRefreshIconSuiteWidgetID && theChange == kTrueStateMessage)
		{
			bool16 result=ptrIAppFramework->getLoginStatus();
			if(!result)
				return;
			
			AcquireWaitCursor awc;
			awc.Animate(); 
	
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
				break;
			//CA("kCSWFRefreshIconSuiteWidgetID");

			//CA("Refreshing Event Cache");
			
			ptrIAppFramework->EventCache_clearInstance();			
			
			ptrIAppFramework->StructureCache_clearInstance();
			ptrIAppFramework->clearConfigCache();
			
			
			////CA("Refreshing Attribute Cache");
			//retVal=0;
			//retVal=ptrIAppFramework->ATTRIBUTECACHE_clearInstance();
			//if(retVal==0)
			//{
			//	ptrIAppFramework->LogError("AP7_TemplateBuilder::CSWFSelectionObserver::Update::ATTRIBUTECACHE_clearInstance's retVal == 0");
			//	break;
			//}

			////CA("refreshing PossibleValue Cache.");
			//retVal=0;
			//retVal=ptrIAppFramework->POSSIBLEVALUECACHE_clearInstance();
			//if(retVal==0)
			//{
			//	ptrIAppFramework->LogError("AP7_TemplateBuilder::CSWFSelectionObserver::Update::POSSIBLEVALUECACHE_clearInstance's retVal == 0");
			//	break;
			//}

			//ptrIAppFramework->GETCommon_refreshClientCache();

			InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
			if(ptrIClientOptions == nil)//-100 is taken purposely.-100 does not exists.So something has to be selected from the list box.
			{
				//CAlert::ErrorAlert("Interface for IClientOptions not found.");
				break;
			}
			
			ptrIClientOptions->setdoRefreshFlag(kTrue);
			
			//CA("After refresh");
			CSWFMediatorClass::loadData=kTrue;
			IsRefresh = kTrue;
			this->loadPaletteData();
			IsRefresh = kFalse;

		}
         */

	
	} while (kFalse);
}

void CSWFSelectionObserver::UpdatePanel()
{
	//CA("updatepanel");
	do
	{
		InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
		if (iPanelControlData == nil)
		{
			//CA("iPanelControlData == nil");
			break;
		}
	}while(kFalse);
}

IPanelControlData* CSWFSelectionObserver::QueryPanelControlData()
{
	//CA("QueryPanelControlData");
	IPanelControlData* iPanel = nil;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return iPanel;
	}
	do
	{
		InterfacePtr<IWidgetParent> iWidgetParent(this, UseDefaultIID());
		if (iWidgetParent == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::QueryPanelControlData::iWidgetParent == nil");
			break;
		}

		InterfacePtr<IPanelControlData> iPanelControlData(iWidgetParent->GetParent(), UseDefaultIID());
		if (iPanelControlData == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::QueryPanelControlData::iPanelControlData == nil");		
			break;
		}
		iPanelControlData->AddRef();
		iPanel = iPanelControlData;
	}
	while (false); 
	return iPanel;
}

void CSWFSelectionObserver::AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	//CA("AttachWidget");
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil)
			break;
		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil)
			break;
		iSubject->AttachObserver(this, interfaceID);
	}
	while (false); 
}

void CSWFSelectionObserver::DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	//CA("DetachWidget");
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil)
			break;
		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil)
			break;
		iSubject->DetachObserver(this, interfaceID);
	}
	while (false); 
}


/*
void CSWFSelectionObserver::HandleDropDownListClick
(const ClassID& theChange, 
ISubject* theSubject, 
const PMIID& protocol, 
void* changedBy)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return;
	}
	do
	{
		//CA("CSWFSelectionObserver::HandleDropDownListClick");		
		InterfacePtr<IControlView> iControlView(theSubject, UseDefaultIID());
		if (iControlView == nil)
		{
			//ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPSelectionObserver::HandleDropDownListClick::iControlView == nil");
			break;
		}

		WidgetID widgetID = iControlView->GetWidgetID();

		InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
		if (iPanelControlData == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::HandleDropDownClick::iPanelControlData == nil");			
			break;
		}

		IControlView* iDropDownControlView = iPanelControlData->FindWidget(kCSWFDropDownWidgetID);
		if(iDropDownControlView == nil)
		{
			//ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::HandleDropDownClick::iDropDownControlView == nil");
			break;
		}

		InterfacePtr<IDropDownListController> iDropDownListController(iDropDownControlView, UseDefaultIID());
		if (iDropDownListController == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::HandleDropDownClick::iDropDownListController == nil");
			break;
		}

		InterfacePtr<IStringListControlData> TabDropListData(iDropDownListController, UseDefaultIID());
		if (TabDropListData == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::loadPaletteData::iStringListControlData invalid");
			break;
		}
		//TabDropListData->Clear(kFalse,kFalse);


		if (widgetID.Get() == kCSWFDropDownWidgetID)
		{
			//CAlert::InformationAlert("widgetID.Get() == kCSWFDropDownWidgetID");
			int32 selectedRowIndex=0;

			selectedRowIndex = iDropDownListController->GetSelected();

			IControlView* iItemListCntrlView=CSWFMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsItemListWidgetID);
			if(iItemListCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFCommonFunctions::attachAttributes::iItemListCntrlView == nil");
				return ;
			}
            
            InterfacePtr<ITriStateControlData> itristatecontroldata(iItemListCntrlView, UseDefaultIID());
			if(itristatecontroldata==nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::Update::itristatecontroldata == nil");
				return;
			}
            
            
            
			IControlView * itemListTextControlView = CSWFMediatorClass::iPanelCntrlDataPtr->FindWidget(kItemListTextWidgetID);
			if(itemListTextControlView == nil)
			{
				//CA("itemListTextControlView == nil");
				break;
			}
					
			IControlView* iTblGrpCntrlView=CSWFMediatorClass::iPanelCntrlDataPtr->FindWidget(kTableGroupPanelWidgetID);
			if(iTblGrpCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::loadPaletteData::iTblGrpCntrlView == nil");
				return ;
			}
						
			IControlView* iAttrGrpCntrlView = CSWFMediatorClass::iPanelCntrlDataPtr->FindWidget(kAttrGroupPanelWidgetID);
			if(iAttrGrpCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::loadPaletteData::iAttrGrpCntrlView == nil");
				return ;
			}
			
			IControlView* iImgGrpCntrlView=CSWFMediatorClass::iPanelCntrlDataPtr->FindWidget(kImageGroupPanelWidgetID);
			if(iImgGrpCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::loadPaletteData::iImgGrpCntrlView == nil");
				return ;
			}

			IControlView* iSprayItemPerFrameCntrlView=CSWFMediatorClass::iPanelCntrlDataPtr->FindWidget(kSprayItemPerFrameWidgetID);
			if(iSprayItemPerFrameCntrlView==nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::iSprayItemPerFrameCntrlView is nil");	
				return ;
			}

			InterfacePtr<ITriStateControlData> itristatecontroldata5(iSprayItemPerFrameCntrlView, UseDefaultIID());
			if(itristatecontroldata5==nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::itristatecontroldata5 is nil");		
				return ;
			}
		

			//--------------------------till here

			if(selectedRowIndex != 0)
			{
				//CAlert::InformationAlert("Default item list to disable line no. 2418 of selectionobserver");
				int32 selectedindex1 = iDropDownListController->GetSelected();
				PMString abc("selectedindex1: ");
				abc.AppendNumber(selectedindex1);
				//CA(abc);
			
				iTblGrpCntrlView->HideView();
				iTblGrpCntrlView->Disable();
				iImgGrpCntrlView->HideView();
				iImgGrpCntrlView->Disable();
				iAttrGrpCntrlView->ShowView();
				iAttrGrpCntrlView->Enable();

				iItemListCntrlView->Disable();
				itemListTextControlView->Disable();

				iSprayItemPerFrameCntrlView->Disable();                      // it is Disable();
				itristatecontroldata5->Deselect();

			


				{
					selectedRowIndex +=2;		// 3= Item Group, 4= Item, 0 = Event/Category			
					iItemListCntrlView->Enable();
					itemListTextControlView->Enable();
					
				}
				
				IControlView* iAttributeNameCntrlView=CSWFMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsAttributeNameWidgetID);
				if(iAttributeNameCntrlView==nil) 
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::loadPaletteData::iAttributeNameCntrlView == nil");
					return ;
				}


				InterfacePtr<ITriStateControlData> itristatecontroldata2(iAttributeNameCntrlView, UseDefaultIID());
				if(itristatecontroldata2==nil) 
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::Update::itristatecontroldata2 == nil");			
					return;
				}

				IControlView* iIncludeHeaderCntrlView=CSWFMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsIncludeHeaderWidgetID);
				if(iIncludeHeaderCntrlView==nil) 
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::loadPaletteData::iIncludeHeaderCntrlView == nil");
					return ;
				}

				InterfacePtr<ITriStateControlData> itristatecontroldata1(iIncludeHeaderCntrlView, UseDefaultIID());
				if(itristatecontroldata1==nil) 
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::Update::itristatecontroldata1 == nil");			
					return;
				}

				IControlView* iDelUnSprayedCntrlView=CSWFMediatorClass::iPanelCntrlDataPtr->FindWidget(kDelUnSprayedFieldsWidgetID);
				if(iDelUnSprayedCntrlView==nil) 
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::Update::iDelUnSprayedCntrlView == nil");
					return;
				}

				InterfacePtr<ITriStateControlData> itristatecontroldata3(iDelUnSprayedCntrlView, UseDefaultIID());
				if(itristatecontroldata1==nil) 
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::Update::itristatecontroldata1 == nil");			
					return;
				}
					
				//IControlView* iImgGrpCntrlView=CSWFMediatorClass::iPanelCntrlDataPtr->FindWidget(kImageGroupPanelWidgetID);
				//if(iImgGrpCntrlView==nil) 
				//{
				//	ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::iImgGrpCntrlView == nil");
				//	return ;
				//}

				//IControlView* iAttrGrpCntrlView=CSWFMediatorClass::iPanelCntrlDataPtr->FindWidget(kAttrGroupPanelWidgetID);
				//if(iAttrGrpCntrlView==nil) 
				//{
				//	ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::loadPaletteData::iAttrGrpCntrlView == nil");
				//	return ;
				//}

				//IControlView* iTblGrpCntrlView=CSWFMediatorClass::iPanelCntrlDataPtr->FindWidget(kTableGroupPanelWidgetID);
				//if(iTblGrpCntrlView==nil) 
				//{
				//	ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::loadPaletteData::iTblGrpCntrlView == nil");
				//	return ;
				//}
				
				if(iDropDownListController->GetSelected()==1) // item group
				{
					itristatecontroldata2->Deselect();
					iAttributeNameCntrlView->Enable();
					
					itristatecontroldata1->Deselect();
					iDelUnSprayedCntrlView->Enable();
                    
					itristatecontroldata->Deselect();
					iItemListCntrlView->Disable();
					itemListTextControlView->Disable();
				}

				else if(iDropDownListController->GetSelected()==2) // item
				{
					itristatecontroldata2->Deselect();
					iAttributeNameCntrlView->Enable();
					
					itristatecontroldata3->Deselect();
					iDelUnSprayedCntrlView->Enable();

					iItemListCntrlView->Enable();
					itemListTextControlView->Enable();
				}
				else
				{
					//CAlert::InformationAlert("else of iDropDownListController->GetSelected()");

					itristatecontroldata2->Deselect();
					iAttributeNameCntrlView->Enable();
					
					itristatecontroldata1->Deselect();
					iDelUnSprayedCntrlView->Enable();
				}


				//{
				//	CAlert::InformationAlert("iDropDownListController->Select(4)");
				//	itristatecontroldata2->Deselect();
				//	iAttributeNameCntrlView->Enable();
				//	
				//	itristatecontroldata1->Deselect();
				//	iDelUnSprayedCntrlView->Enable();

				//	iItemListCntrlView->Enable();
				//	itemListTextControlView->Enable();

				//}
				//else if(iDropDownListController->Select(3))
				//{
				//	CAlert::InformationAlert("iDropDownListController->Select(3)");
				//	itristatecontroldata2->Deselect();
				//	iAttributeNameCntrlView->Enable();
				//	
				//	itristatecontroldata1->Deselect();
				//	iDelUnSprayedCntrlView->Enable();
				//}
				//else if(iDropDownListController->Select(0))
				//{
				//	CAlert::InformationAlert("iDropDownListController->Select(0)");
				//	itristatecontroldata2->Deselect();
				//	iAttributeNameCntrlView->Enable();
				//	
				//	itristatecontroldata1->Deselect();
				//	iDelUnSprayedCntrlView->Enable();

				//}
				//else{}
				////-------------------------------------------
			}
			
			if(selectedRowIndex == 0)
			{
				iTblGrpCntrlView->HideView();
				iTblGrpCntrlView->Disable();
				iImgGrpCntrlView->HideView();
				iImgGrpCntrlView->Disable();
				iAttrGrpCntrlView->ShowView();
				iAttrGrpCntrlView->Enable();

                itristatecontroldata->Deselect();
				iItemListCntrlView->Disable();
				itemListTextControlView->Disable();

			//----------------------- mahesh added
				iSprayItemPerFrameCntrlView->Disable();                 // it is disable();
				itristatecontroldata5->Deselect();
			//--------------------------

			}
			SelectedRowNo = selectedRowIndex;
			//CA_NUM("selectedRowIndex  :  ",selectedRowIndex);
			IControlView* ProjectPanelCtrlView = iPanelControlData->FindWidget(kProjectTreeViewWidgetID);
			if (ProjectPanelCtrlView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::HandleDropDownListClick::ProjectPanelControlView == nil");
				break;
			}
			
		}
		InterfacePtr<ITreeViewMgr> treeViewMgr(CSWFMediatorClass::ProjectLstboxCntrlView, UseDefaultIID());
		if(!treeViewMgr)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::HandleDropDownListClick::treeViewMgr is nil");
			return;
		}
			
		CSWFTreeModel pModel;
		PMString pfName("Root");

		switch(SelectedRowNo)
		{
			case 0:
				pModel.setRoot(-1, pfName, 1);
				break;
			case 1:
				pModel.setRoot(-1, pfName, 2);
				break;
			case 2:
				pModel.setRoot(-1, pfName, 3);
				break;
			case 3:
				pModel.setRoot(-1, pfName, 4);
				break;
			case 4:
				pModel.setRoot(-1, pfName, 5);
				break;
		}

		treeViewMgr->ClearTree(kTrue);
		pModel.GetRootUID();
		treeViewMgr->ChangeRoot();

		if(SelectedRowNo != 4)
		{
			InterfacePtr<ICategoryBrowser> CatalogBrowserPtr((ICategoryBrowser*)::CreateObject(kCTBCategoryBrowserBoss, IID_ICATEGORYBROWSER));
			if(!CatalogBrowserPtr)
			{
				ptrIAppFramework->LogError("AP7_TemplateBuilder::CSWFSelectionObserver::HandleDropDownListClick::Pointre to TemplateBuilderPtr not found");
				return ;
			}
			CatalogBrowserPtr->CloseCategoryBrowser();
		}

		//-------
		if(ISTabbedText)
		{
			//CAlert::InformationAlert("ISTabbedText");
			//CA("ISTabbedText");
			IControlView* tableListdropdownCtrlView=CSWFMediatorClass::iPanelCntrlDataPtr->FindWidget(kCSWFTableListDropDownWidgetID);
			if(tableListdropdownCtrlView==nil) 
			{
				CAlert::InformationAlert("tableListdropdownCtrlView==nil");
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::HandleDropDownListClick::tableListdropdownCtrlView == nil");
				return;
			}

			InterfacePtr<IDropDownListController> tableListDropDownController(tableListdropdownCtrlView, UseDefaultIID());
			if (tableListDropDownController == nil)
			{
				CAlert::InformationAlert("tableListDropDownController == nil");
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::HandleDropDownListClick::tableListDropDownController == nil");				
				break;		
			}

 			InterfacePtr<IStringListControlData> tablelistDropDownData(tableListDropDownController, UseDefaultIID());
			if (tablelistDropDownData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::HandleDropDownListClick::tablelistDropDownData invalid");
				break;
			}

			IControlView* iAttNameCntrlView=CSWFMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsAttributeNameWidgetID);
			if(iAttNameCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::HandleDropDownListClick::iAttNameCntrlView == nil");
				return;
			}

			IControlView* iDelUnSprayedCntrlView=CSWFMediatorClass::iPanelCntrlDataPtr->FindWidget(kDelUnSprayedFieldsWidgetID);
			if(iDelUnSprayedCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::HandleDropDownListClick::iDelUnSprayedCntrlView == nil");
				return;
			}

			tablelistDropDownData->Clear(kFalse,kFalse);
		
			
			if(SelectedRowNo == 4)
			{
				//CA("Item");
				tablelistDropDownData->AddString("All", 0 );

				tableListdropdownCtrlView->ShowView();
				iAttNameCntrlView->HideView();
				iDelUnSprayedCntrlView->HideView();

				int32 itemListSize = static_cast<int32>(ITEMDataNodeList.size());
				
				for(int32 i = 0; i < itemListSize ; i++)
				{
					if(ITEMDataNodeList[i].getHitCount() == 8 ) 
					{
						tablelistDropDownData->AddString(ITEMDataNodeList[i].getName(), i );
					}

					
				}
				tableListDropDownController->Select(0);
			}
			else
			{
				//CA("else");
				table_TypeID = -1;
				tableListdropdownCtrlView->HideView();
				iAttNameCntrlView->ShowView();
				iDelUnSprayedCntrlView->ShowView();
				iDelUnSprayedCntrlView->Enable();   // added 2nd april by mahesh
				
			}
			
		}
		else
		{
			//CA("!!!!!ISTabbedText  Drop Down");
		}


	}while(false); 
}
*/


void CSWFSelectionObserver::populateProjectPanelLstbox()
{
	//CA("CSWFSelectionObserver::populateProjectPanelLstbox");
	do
	{
        //////////////////////////////////// for populating Project tree  /////////////////////
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;

		if(CSWFMediatorClass::iPanelCntrlDataPtr==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::populateProjectPanelLstbox::Not getting iPanelCntrlDataPtr");
			break;
		}
			
		WorkTaskDataNodeList.clear();
		IControlView* ProjectPanelCtrlView = 
		CSWFMediatorClass::iPanelCntrlDataPtr->FindWidget(kProjectTreeViewWidgetID);
		if (ProjectPanelCtrlView == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::CSWFSelectionObserver::populateProjectPanelLstbox::ProjectPanelCtrlView is nil");
			break;
		}
		CSWFMediatorClass::ProjectLstboxCntrlView=ProjectPanelCtrlView;
		ProjectPanelCtrlView->ShowView(kTrue);
        vector<PricingModule> pricingModuleList;
        
        //CA("Before calling callGetPricingModule");
        bool16 resultForCallPricingModule = ptrIAppFramework->callGetPricingModule(pricingModuleList);
        
        if( pricingModuleList.size() ==0)
        {
            CA("No Pricing Module avalible for your role.");
            break;
        }
        
        VectorPricingModule::iterator itPM;
        bool16 isINDDModuleAvailableForClient = kFalse;
        for(itPM=pricingModuleList.begin(); itPM!=pricingModuleList.end(); itPM++)
        {
            //CA("Pricing Module: " + itPM->getModule());
            if(itPM->getModule() == "INDD")
            {
                //CA("Got INDD");
                isINDDModuleAvailableForClient = kTrue;
                break;
            }
            
        }
        
        if(isINDDModuleAvailableForClient)
        {
            vector<WorkflowTask> workflowTaskList;
             //CA("Before calling callGetWorkflowTasks");
            bool16 resultForCallPricingModule = ptrIAppFramework->callGetWorkflowTasks(workflowTaskList);
            
            if(workflowTaskList.size() ==0)
            {
                CA("No workflow Task avalible!!");
                break;
            }

            
            VectorWorkflowTask::iterator itWT;
            int32 seqNumber = 0;
            for(itWT= workflowTaskList.begin();itWT!=workflowTaskList.end();itWT++)
            {
                WorkflowTask workflowTaskObj = *itWT;
                //CA(workflowTaskObj.getTaskName());
                //CA(workflowTaskObj.getTaskStatus());
                
                CSWFDataNode csfwDataNode;

                
                PMString ItemNumberNameStr("");
                if(workflowTaskObj.getWorkflowItem().getValues().size() > 1)
                {
                    PMString ItemNumber = workflowTaskObj.getWorkflowItem().getValues().at(0).getValue();
                    PMString ItemDescription = workflowTaskObj.getWorkflowItem().getValues().at(1).getValue();
                    ItemNumberNameStr.Append(workflowTaskObj.getObjectType());
                    ItemNumberNameStr.Append("\t - \t");
                    ItemNumberNameStr.Append(ItemNumber);
                    ItemNumberNameStr.Append(" : ");
                    ItemNumberNameStr.Append(ItemDescription);
                    ItemNumberNameStr.Append("\t - \t");
                    ItemNumberNameStr.Append(workflowTaskObj.getTaskName());
                    
                    
                }
                int32 ParentId = -2;
                
                csfwDataNode.setAll(ItemNumberNameStr,
                                    workflowTaskObj.getTaskId(),
                                    ParentId,
                                    seqNumber,
                                    0,
                                    0,
                                    workflowTaskObj.getObjectType(),
                                    workflowTaskObj.getTaskName(),
                                    workflowTaskObj.getTaskStatus(),
                                    workflowTaskObj.getObjectId(),
                                    workflowTaskObj.getCurrentStepId(),
                                    workflowTaskObj.getDueDate()
                                    );
                WorkTaskDataNodeList.push_back(csfwDataNode);
                
                seqNumber++;
                
            }
        }
	}while(kFalse);
}



/*
PMString CSWFSelectionObserver::GetSelectedLocaleName(IControlView* LocaleControlView, int32& selectedRow)
{	
	//CA("PubBrowseDlgObserver::GetSelectedLocaleName");
	PMString locName("");
	do{
		InterfacePtr<IStringListControlData> LocaleDropListData(LocaleControlView,UseDefaultIID());	
		if(LocaleDropListData == nil){
			//CAlert::InformationAlert("LocaleDropListData nil");
			break;
		}
		InterfacePtr<IDropDownListController> LocaleDropListController(LocaleDropListData, UseDefaultIID());
		if(LocaleDropListController == nil){
			//CAlert::InformationAlert("LocaleDropListController nil");
			break;
		}
		selectedRow = LocaleDropListController->GetSelected();
		if(selectedRow <0){
			//CAlert::InformationAlert("invalid row");
			break;
		}
		locName = LocaleDropListData->GetString(selectedRow);
//
		PMString sls;
		sls.AppendNumber (selectedRow );
		//CA("Selected row no ===== "+sls+" Name = "+locName );
//
		//if(selectedRow>=0){
		//	InterfacePtr<IPanelControlData> panelControlData(this,UseDefaultIID());
		//	if (panelControlData == nil){
		//		//CAlert::InformationAlert("panelControlData nil");
		//		break;
		//	}
		//	IControlView* controlView = panelControlData->FindWidget(kPubDropDownWidgetID);
		//	if (controlView == nil){
		//		//CAlert::InformationAlert("Publication Browse Button ControlView nil");
		//		break;
		//	}
		//	IControlView* controlView1 = panelControlData->FindWidget(kOKButtonWidgetID);
		//	if (controlView1 == nil){
		//		//CAlert::InformationAlert("Ok Button ControlView nil");
		//		break;
		//	}
		//	if(!controlView->GetEnableState()){
		//		controlView->Enable(kTrue);
		//		//controlView1->Enable(kTrue);
		//	}
		//}
	}while(0);	
	return locName;
} 
*/
