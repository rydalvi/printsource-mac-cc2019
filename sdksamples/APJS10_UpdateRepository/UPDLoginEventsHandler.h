#ifndef __UPDLoginEventsHandler_h__
#define __UPDLoginEventsHandler_h__

#include "VCPlugInHeaders.h"
#include "IRegisterEvent.h"
#include "UPDID.h"
#include "ILoginEvent.h"

class UPDLoginEventsHandler : public CPMUnknown<ILoginEvent>
{
public:
	UPDLoginEventsHandler(IPMUnknown* );
	~UPDLoginEventsHandler();
	bool16 userLoggedIn();
	bool16 userLoggedOut();
	bool16 resetPlugIn();
};

#endif