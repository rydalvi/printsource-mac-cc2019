#ifndef __SLUGSTRUCT_H__
#define __SLUGSTRUCT_H__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "vector"

using namespace std;

class SlugStruct
{
public:
	double elementId;	// [Element ID] OR Attribute ID
	double typeId;		// Type ID of Element or Attribute field
	int32 header;		// if 1 indicates its a Header (Display Name ) field
	bool16 isEventField; //for Event level attribute if true its 
	int32 deleteIfEmpty;// -1 = not applied, 1= delete If Empty true (Delete empty tags after spray)
	int32 dataType;		// Type of Attribute -1= Normal Attributes, 1 = Possible Value, 2= Image Description , 3 = Image Name ,4 = Table name , 5 = Table Comments
	int32 isAutoResize;
	double LanguageID;
	int16 whichTab; // current lstbox [index]	
	double pbObjectId;  // Pb object id for publication items and product in case of ONEsource its -1
	double parentId; // [parentID] Documnet tag's parent (in case of Table Product Id or Master Item Id)
	double childId;	// in case of Table Child Item's ID
	double sectionID; // storing subsection/ Section ID	
	double parentTypeId; // [parentTypeID]	
	int32 isSprayItemPerFrame; // -1 = not applied, 1= isSprayItemPerFrame with Horizontal Flow, 2 = isSprayItemPerFrame with Verticle flow	
	int32 catLevel;		// Category Level in case of Category Attributes otherwise -1
	
	int32 imgFlag; // image flag [imgFlag]  

	int32 imageIndex;	// image index for PV, Brand, Manufacturer & Supplier images.
	int32 flowDir;		// 0 = Horizontal Flow, 1= Verticle Flow, -1 = No flow is define its in case of all Images or PV images
	
	int32 childTag;		// if 1 indicates Child item tag 
	int32 tableFlag;
	int32 tableType;	// displays different types of tables 1 = DB Table, 2 = Custom Table, 3 = Advance Table, 4 = Component , 5 = Accessaries, 6 XRef, 7 = All Standard Table
	double tableId;		// Table ID after spray
	double row_no;
	double col_no;  // -555 tabbed test table.

	int32 field1;
	int32 field2;
	int32 field3;
	int32 field4;
	int32 field5;

	bool16 isProcessed;
	int32 tagStartPos;
	int32 tagEndPos;

    PMString elementName;
	PMString TagName;

	double tableTypeId;  // added for attaching correct type id for table.
    PMString groupKey;

	SlugStruct()
	{
		elementId = -1;
		typeId = -1;
		header = -1;
		isEventField = kFalse;
		deleteIfEmpty = -1;
		dataType = -1;
		isAutoResize = -1;
		LanguageID = -1;
		whichTab = -1;
		pbObjectId = -1;
		parentId = -1;
		childId = -1;
		sectionID = -1;
		parentTypeId = -1;
		isSprayItemPerFrame = -1;
		catLevel = -1;		
		imgFlag = -1;
		imageIndex = -1;
		flowDir = -1;
		childTag = -1;
		tableFlag = 0;
		row_no=-1;
		col_no=-1;
		field1 = -1;
		field2 = -1;
		field3 = -1;
		field4 = -1;
		field5 = -1;
		
		elementName.Clear();
		TagName.Clear();
		
		tagStartPos= -1;
		tagEndPos = -1;
	
		tableTypeId = -1;
        groupKey = "";
	}
};

typedef vector<SlugStruct> SlugList;

#endif
