#ifndef __UPDPlugInEntrypoint_h__
#define __UPDPlugInEntrypoint_h__

#include "PlugIn.h"
#include "GetPlugin.h"
#include "ISession.h"

class UPDPlugInEntrypoint : public PlugIn
{
public:
	virtual bool16 Load(ISession* theSession);

#ifdef WINDOWS
	static ITypeLib* fSPTypeLib;
#endif                    
};

#endif