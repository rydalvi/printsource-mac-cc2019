#ifndef __TAG_WRITER_H__
#define __TAG_WRITER_H__

#include "VCPluginHeaders.h"
#include "TagStruct.h"

#include "IHierarchy.h"
#include "IStoryList.h"
#include "IFrameList.h"
//#include "ITextFrame.h"
//#include "ISpecifier.h"
#include "IWorkspace.h"
#include "IStyleInfo.h"
#include "IStyleNameTable.h"
#include "ITextAttributes.h"
#include "IBoolData.h"
#include "IIDXMLElement.h"
#include "IXMLreferenceData.h"
#include "IXMLUtils.h"
#include "IXMLTag.h"
#include "IXMLTagList.h"
#include "IXMLStyleToTagMap.h"
#include "IXMLTagToStyleMap.h"
#include "IXMLTagCommands.h"
#include "IXMLElementCommands.h"
#include "IXMLMappingCommands.h"
#include "IXMLAttributeCommands.h"
#include "IFrameUtils.h"
#include "IDocument.h"

#include "IFrameUtils.h" //FrameUtils.h"


class TagWriter
{
public:
	void addTagToGraphicFrame(UIDRef, TagStruct&, PMString);
private:
	XMLReference TagFrameElement(const XMLReference&, UID, const PMString&);
	PMString prepareTagName(PMString name);
};

#endif