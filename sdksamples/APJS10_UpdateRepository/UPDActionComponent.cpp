//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:

// General includes:
#include "CActionComponent.h"
#include "CAlert.h"
//#include "IPaletteMgr.h"
#include "IApplication.h"
#include "IPanelMgr.h"
#include "LocaleSetting.h"
#include "IAppFramework.h"
#include "IActionStateList.h"
#include "ILayoutUtils.h" //cs4

// Project includes:
#include "UPDID.h"
#include "UPDActionComponent.h"
#include "IWindow.h"
#include "MediatorClass.h"
#include "ITextControlData.h"
#include "UPDSelectionObserver.h"
#include "IPanelControlData.h"
#include "DocWchUtils.h"
//#include "LayoutUIUtils.h"
#include "ILayoutUIUtils.h"
#include "PaletteRefUtils.h"
#include "ITagReader.h"
#include "ISpecialChar.h"		
/// Global Pointers
ITagReader* itagReader = NULL;
IAppFramework* ptrIAppFramework = NULL;
ISpecialChar*  iConverter = NULL;
///////////


#define CA(X) CAlert::InformationAlert(X)
/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

	
	@ingroup ap46_updaterepository

*/
bool16 FlgToCheckMenuAction= kFalse;
int32 Flag1;
IPaletteMgr* UPDActionComponent::palettePanelPtr=0;
//bool16 GroupFlag= kTrue;   //modified..
int32 GroupFlag =1;
bool16 HiliteFlag= kFalse;
/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(UPDActionComponent, kUPDActionComponentImpl)

/* UPDActionComponent Constructor
*/
UPDActionComponent::UPDActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{
}

/* DoAction
*/
void UPDActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{
	switch (actionID.Get())
	{

		
		//case kUPDAboutActionID:
	//	{
		//	this->DoAbout();
	//		break;
	//	}
					
		
		case kUPDMenuItem1ActionID:
		{	//CA("kUPDMenuItem1ActionID");
			this->DoMenuItem1(ac);
			break;
		}

		case kUPDMenuItem2ActionID:
		{	//CA("kUPDMenuItem2ActionID");
			this->DoMenuItem2(ac);
			break;
		}

		case kUPDMenuItem3ActionID:
		{	//CA("kUPDMenuItem3ActionID");
		//	this->DoMenuItem3(ac);
			break;
		}
		case kUPDMenuItem4ActionID:  //Unique Attribute
		{
			//CA("  In DoAction case  kUPDMenuItem4ActionID.. 118" );
			FlgToCheckMenuAction = kTrue ;
			this->DoMenuItem4(ac);
			break;
		}

		case kUPDPanelPSMenuActionID:
		case kUPDContentPanelWidgetActionID:
		{	
			//CA("kUPDContentPanelWidgetActionID");
			FlgToCheckMenuAction= kTrue;
			this->DoPalette();
			break;
		}


		default:
		{
			break;
		}
	}
}

/* DoAbout
*/
void UPDActionComponent::DoAbout()
{
	CAlert::ModalAlert
	(
		kUPDAboutBoxStringKey,				// Alert string
		kOKString, 						// OK button
		kNullString, 						// No second button
		kNullString, 						// No third button
		1,							// Set OK button to default
		CAlert::eInformationIcon				// Information icon.
	);
}

/* DoMenuItem1
*/
void UPDActionComponent::DoMenuItem1(IActiveContext* ac)
{	
	//CA("Group by object");
	UPDSelectionObserver qwe(this);	
	qwe.reSortTheListForObject();
	qwe.fillDataInListBox(/*kFalse*/2);
	GroupFlag = 2;     //kFalse;	
}

void UPDActionComponent::DoMenuItem2(IActiveContext* ac)
{	
//CA("Group by element DoMenuItem2()");
	GroupFlag = 3;    ///kTrue;
	UPDSelectionObserver qwe(this);
	qwe.reSortTheListForElem();
	qwe.fillDataInListBox(/*kTrue*/3);
	//CAlert::InformationAlert("Group by Element");
}

void UPDActionComponent::DoMenuItem3(IActiveContext* ac)
{
//CA("In  UPDActionComponent.cpp ...DoMenuItem3()");
//	if(!HiliteFlag)
//		HiliteFlag= kTrue;
//	else
//		HiliteFlag=kFalse;
//	CAlert::InformationAlert("Hilite changed content");
}

void UPDActionComponent::DoMenuItem4(IActiveContext* ac) //Unique Attributes in Doc
{
	//CA("Group by UniqueObject.DoMenuItem4()" );
	
	GroupFlag =  1;  ///kTrue;     //Unique Attributes in Doc
	UPDSelectionObserver qwe(this);
	qwe.reSortTheListForUniqueAttr();
	qwe.fillDataInListBox(1);

}//End of DoMenuItem4()

void UPDActionComponent::DoPalette()
{
	//CA("INside Do palette");
	do
	{	
		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if(iApplication==NULL)
		{
			
			break;
		}
		/*InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPaletteManager());
		if(iPaletteMgr==NULL)
		{
			
			break;
		}
		InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr, UseDefaultIID()); */
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager());
		if(iPanelMgr == NULL)
		{	
			CA("iPanelMgr is NULL");
			break;
		}

		PMLocaleId nLocale=LocaleSetting::GetLocale() ;
		iPanelMgr->ShowPanelByMenuID(kUPDPanelWidgetActionID) ;
	//	UPDActionComponent::palettePanelPtr = iPaletteMgr ;
		DocWchUtils::StartDocResponderMode();

		if(FlgToCheckMenuAction)
		{
			IControlView* icontrol = iPanelMgr->GetVisiblePanel(kUPDPanelWidgetID);
			if(!icontrol)
			{
				return;
			}
			//icontrol->Resize(PMPoint(PMReal(207),PMReal(291)));//16-10-08
			
		}

	}while(kFalse);
}

void UPDActionComponent::UpdateActionStates(IActiveContext* ac, IActionStateList* iListPtr, GSysPoint mousePoint, IPMUnknown* widget)    	//(IActionStateList* iListPtr)
{	

	do
	{
		if(ptrIAppFramework == NULL)
		{  			
			IAppFramework* ptrIAppFrameworkOne(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFrameworkOne == NULL)
				break;
			ptrIAppFramework = ptrIAppFrameworkOne;
		}

		if(iConverter == NULL)
		{	
			ISpecialChar* iConverterOne(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			if(!iConverterOne)
			{	
				ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDActionComponent::UpdateActionStates::!iConverterOne");	
				break;
			}
			iConverter = iConverterOne;
		}

		if(itagReader ==NULL)
		{	
			ITagReader* itagReaderOne
				((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
			if(!itagReaderOne)
			{
				ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDActionComponent::UpdateActionStates::!itagReaderOne");			
				break;
			}
			itagReader = itagReaderOne;
		}
		
	}while(0);

	//CA("Inside UPDActionComponent::UpdateActionStates");
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
		return;

	bool16 result=ptrIAppFramework->getLoginStatus();

	IDocument *iDoc=/*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument(); //Cs4
//############Added....
/*
	if(result  && iDoc)
	{

		iListPtr->SetNthActionState(0,kEnabledAction);
		iListPtr->SetNthActionState(1,kEnabledAction); //Hilite changed content
		iListPtr->SetNthActionState(2,kEnabledAction); // Group by Element
		
		if(GroupFlag == 1)
		{

			iListPtr->SetNthActionState(2,kSelectedAction); 
			iListPtr->SetNthActionState(1,kEnabledAction);
			iListPtr->SetNthActionState(0,kEnabledAction);
		}
		else if(GroupFlag == 2)
		{

			iListPtr->SetNthActionState(1,kSelectedAction);
			iListPtr->SetNthActionState(0,kEnabledAction);
			iListPtr->SetNthActionState(2,kEnabledAction);
		}
		else if(GroupFlag == 3)
		{

			iListPtr->SetNthActionState(0,kSelectedAction);
			iListPtr->SetNthActionState(1,kEnabledAction);
			iListPtr->SetNthActionState(2,kEnabledAction);
		}	
	}
	else
	{
		iListPtr->SetNthActionState(0,kDisabled_Unselected);
		iListPtr->SetNthActionState(1,kDisabled_Unselected);
		iListPtr->SetNthActionState(2,kDisabled_Unselected);
	}
*/
//######Up To Here

	/*if(GroupFlag)
	{	
		CA("7");
		iListPtr->SetNthActionState(0,kSelectedAction);
		CA("8");
		iListPtr->SetNthActionState(1,kEnabledAction);
		CA("9");
	}
	else
	{
		CA("10");
		iListPtr->SetNthActionState(1,kSelectedAction);
		CA("11");
		iListPtr->SetNthActionState(0,kEnabledAction);
		CA("12");
	}*/

//	if(HiliteFlag)
//		iListPtr->SetNthActionState(0,kSelectedAction);
//	else
//		iListPtr->SetNthActionState(0,kEnabledAction);


	for(int32 iter = 0; iter < iListPtr->Length(); iter++) 
	{
		ActionID actionID = iListPtr->GetNthAction(iter);		
		/*if(actionID == kUPDContentPanelWidgetActionID)
		{ 		
			if(result && iDoc)
				iListPtr->SetNthActionState(iter,kEnabledAction);
			else
				iListPtr->SetNthActionState(iter,kDisabled_Unselected);
		}*/

					
		switch(actionID.Get())
		{
			case kUPDPanelPSMenuActionID:
			case kUPDContentPanelWidgetActionID:
			{
				//PMString string("Setting ActionState", -1, PMString::kNoTranslate);
				//CAlert::InformationAlert(string);
				if(result && iDoc)
				{
					InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
					ASSERT(app);
					if(!app)
					{
						return;
					}
					/*
					InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
					ASSERT(paletteMgr);
					if(!paletteMgr)
					{
						return;
					}
					*/
					//InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());
					InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
					ASSERT(panelMgr);
					if(!panelMgr)
					{
						return;
					}
//					if(panelMgr->IsPanelWithWidgetIDVisible(kUPDPanelWidgetID))            ///CS3 Change
					if(panelMgr->IsPanelWithMenuIDMostlyVisible(kUPDPanelWidgetID))
					{
						iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);
					}
					else
					{
						iListPtr->SetNthActionState(iter,kEnabledAction);
					}

				}
				else
				{

					iListPtr->SetNthActionState(iter,kDisabled_Unselected);
				}
				break;
			}
			case kUPDMenuItem4ActionID://For Unique Attribute
			{

				if(iDoc && result)
				{
					if(GroupFlag == 1){

						iListPtr->SetNthActionState(iter, kEnabledAction | kSelectedAction);					
					}
					else{

						iListPtr->SetNthActionState(iter,kEnabledAction );					
					}
				}
				else	
				{

					iListPtr->SetNthActionState(iter,kDisabled_Unselected);			
				}
				break;
			}//End of case :kUPDMenuItem4ActionID
			case kUPDMenuItem1ActionID:// Group By Object..
			{

				if(result && iDoc){					
						if(GroupFlag == 2){

							iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);					
						}
						else{

							iListPtr->SetNthActionState(iter,kEnabledAction );	
						}

				}
				else				
						iListPtr->SetNthActionState(iter,kDisabled_Unselected);	
				break;
			}
			case kUPDMenuItem2ActionID : ////Group by Element
			{
				
				if(result && iDoc){					
						if(GroupFlag == 3){

							iListPtr->SetNthActionState(iter,kEnabledAction | kSelectedAction);		
						}
						else{

							iListPtr->SetNthActionState(iter,kEnabledAction );					
						}
				}
				else				
					iListPtr->SetNthActionState(iter,kDisabled_Unselected);	
				break;
			}
			default:
			{
				break;
			}
		}
	}
}

void UPDActionComponent::CloseUpdateRepositoryPalette(void)
{
	//CA("Closing the palette");
	do
	{	
		//DoPalette();
	// Added By Awasthi
		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if(iApplication==NULL)
			break;
		/*	////CS3 Change
		InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPaletteManager());
		if(iPaletteMgr==NULL)
			break;
		*/
	//	InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr, UseDefaultIID());   ///CS3 Change
//	InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
		//Mediator::iPanelCntrlDataPtr = iPanelMgr;
	// End Awasthi
//		if(Mediator::iPanelCntrlDataPtr== NULL)
//			break;
		
		/* Clear all listboxes and disable all controls */
		
		// MAke comment Awasthi		
		/*IControlView* dropdownCtrlView=
			Mediator::iPanelCntrlDataPtr->FindWidget(kTPLDropDownWidgetID);
		
		if(!dropdownCtrlView)
			break;
		
		IControlView* refreshBtnCtrlView=
			Mediator::iPanelCntrlDataPtr->FindWidget(kTPLRefreshIconSuiteWidgetID);
		if(!refreshBtnCtrlView)
			break;
		
		IControlView* appendRadCtrlView=
			Mediator::iPanelCntrlDataPtr->FindWidget(kAppendRadBtnWidgetID);
		if(!appendRadCtrlView)
			break;
		
		IControlView* embedRadCtrlView=
			Mediator::iPanelCntrlDataPtr->FindWidget(kEmbedRadBtnWidgetID);
		if(!embedRadCtrlView)
			break;
		
		IControlView* tagFrameChkboxCtrlView=
			Mediator::iPanelCntrlDataPtr->FindWidget(kTagFrameWidgetID);
		if(!tagFrameChkboxCtrlView)
			break;
		*/

		// End making Commments Awasthi
	//	IControlView* classTreeCtrlView=
	//		Mediator::iPanelCntrlDataPtr->FindWidget(kTPLClassTreeIconSuiteWidgetID);
	//	if(!classTreeCtrlView)
	//		break;
		
		//Mediator::dropdownCtrlView->Disable();
		//Mediator::UPDRefreshButtonView->Disable();		
		
//		SDKListBoxHelper listHelper(this, kUPDPluginID);	
		
	//after login and then logout and then again login the listbox gets disableed
		//Mediator::listControlView->Hide();		
	//end Yogesh
// 
//////CS3 Change
////////////	Added by Amit
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
		if(iPanelMgr== nil)
		{
			ptrIAppFramework->LogDebug("AP46_RefreshContent::RfhActionComponent::CloseRefreshPalette::iPanelMgr is nil");				
			break;
		}
		//InterfacePtr<IControlView> panelView((const IPMUnknown*)::GetDataBase(iPanelMgr), UseDefaultIID());//Added
		//if(!panelView)
		//{
		//	ASSERT(panelView);
		//	break;
		//}
		IControlView* panelView = iPanelMgr->GetPanelFromActionID(kUPDPanelWidgetActionID);
		if(panelView == NULL)
		{
			CA("panelView is NULL");
			return;
		}
		
		
		////----------------New CS3 Changes--------------------
			PaletteRef palRef=iPanelMgr->GetPaletteRefContainingPanel(panelView);
		////----------------New CS3 Changes--------------------//
			bool16 retval = PaletteRefUtils::IsPaletteVisible(palRef);
		//================================================

///////////		END
//		if(iPaletteMgr)//TPLActionComponent::palettePanelPtr)	 
		if(retval)
		{	
			//InterfacePtr<IPanelMgr> iPanelMgr(TPLActionComponent::palettePanelPtr, UseDefaultIID()); 
/*			InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr, UseDefaultIID()); 
			if(iPanelMgr == NULL)
				break;
*/			
			//if(iPanelMgr->IsPanelWithMenuIDVisible(kUPDContentPanelWidgetActionID))		
			if(iPanelMgr->IsPanelWithMenuIDMostlyVisible(kUPDPanelWidgetActionID))
			{					
				iPanelMgr->HidePanelByMenuID(kUPDPanelWidgetActionID);				
			}
		}

		DocWchUtils::StopDocResponderMode();
		
	}while(kFalse);
	
}

//  Code generated by DollyXS code generator
