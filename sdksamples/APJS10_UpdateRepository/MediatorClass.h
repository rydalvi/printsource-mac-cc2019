#ifndef __MEDIATOR_CLASS_H__
#define __MEDIATOR_CLASS_H__

#include "IDialog.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "ITextControlData.h"

class Mediator
{
private:
	~Mediator();//Cannot instantiate this class
public:
	static int16 selectedRadioButton;
	static int32 refreshPageNumber;
	static IDialog * dialogPtr;
	static IControlView* listControlView;
	static IControlView* editBoxView;
	static IControlView* ChkHighlightView;
	static bool16 isGroupByObj;

	static IPanelControlData* iPanelCntrlDataPtr;
	static bool16	loadData;
	static IControlView* dropdownCtrlView;
	static IPanelControlData* iPanelCntrlDataPtrTemp;
	static IControlView* UPDRefreshButtonView;
	static ITextControlData * EditBoxTextControlData;

	//Yogesh
	static IControlView* UPDLocateButtonView;
	//Chetan Dogra 5-oct
	static IControlView* UPDReloadButtonView;
	
	static void initMembers()
	{
		selectedRadioButton=-1;
		refreshPageNumber=-1;
		listControlView=NULL;
		editBoxView=NULL;
		ChkHighlightView=NULL;
		isGroupByObj=kTrue;
		iPanelCntrlDataPtr=NULL;
		dropdownCtrlView=NULL;
		iPanelCntrlDataPtrTemp=NULL;
		UPDRefreshButtonView=NULL;
		//Yogesh
		UPDLocateButtonView=NULL;
		
	}
};

#endif