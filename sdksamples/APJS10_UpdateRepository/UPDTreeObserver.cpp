#include "VCPlugInHeaders.h"
#include "IHierarchy.h"	
#include "ISpreadlist.h"
#include "IDocument.h"
#include "ISession.h"
//#include "LayoutUtils.h" //Cs3 Depricated
#include "ILayoutUtils.h" //Cs4
#include "K2Vector.h"
#include "K2Vector.tpp" 
#include "SystemUtils.h"
//#include "CTBID.h"
#include "UPDID.h"

#include "CAlert.h"


#include "IAppFrameWork.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
#include "CDialogObserver.h"

#include "ITreeViewController.h"

#include "IApplication.h"
//commented by nitin
//#include "IPaletteMgr.h"
#include "PaletteRefUtils.h"
#include "IPanelMgr.h"
#include "PMString.h"
#include "IControlView.h"
#include "ITextControlData.h"
#include "IContentSprayer.h"
#include "IAppFramework.h"

//
#include "IApplication.h"
//#include "IPaletteMgr.h"
#include "IPanelMgr.h"
//#include "SEAID.h"
//#include "IOneSourceSearch.h"
#include "StringUtils.h"
#include "IntNodeID.h"
#include "IWidgetParent.h"
//
#define CA(X)		CAlert::InformationAlert(X)

extern set<NodeID> UniqueNodeIds;


//-------------The end user changes the node which is selected in the tree view control. To receive notifications
//about this, attach an observer (IObserver implementation of your own) to the ISubject
//interface of your kTreeViewWidgetBoss subclass, and listen along protocol IID_ITREEVIEWCONTROLLER.

class UPDTreeObservser : public CObserver
{
private:
	int32 selectedID;
public:
	UPDTreeObservser(IPMUnknown *boss) : CObserver(boss) {}
	virtual void AutoAttach();
	virtual void AutoDetach();
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
};

CREATE_PMINTERFACE(UPDTreeObservser, kUPDTreeObserverImpl)

void UPDTreeObservser::AutoAttach()
{
	//CAlert::InformationAlert("Inside AutoAttach");
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this,  IID_ITREEVIEWCONTROLLER);
	}

	InterfacePtr<IWidgetParent> iWidgetParent(this, UseDefaultIID());
	if (iWidgetParent == nil)
	{
		//ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::QueryPanelControlData::iWidgetParent == nil");										
		return;
	}

	InterfacePtr<IPanelControlData> iPanelControlData(iWidgetParent->GetParent(), UseDefaultIID());
	if (iPanelControlData == nil)
	{
		//ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::QueryPanelControlData::iPanelControlData == nil");												
		return;
	}

	IControlView* iControlView = iPanelControlData->FindWidget(kUPDCheckBoxWidgetID);
	if (iControlView == nil)
		return;
	InterfacePtr<ISubject> iSubject1(iControlView, UseDefaultIID());
	if (iSubject1 == nil)
		return;
	iSubject1->AttachObserver(this, IID_ITRISTATECONTROLDATA);
}


void UPDTreeObservser::AutoDetach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this,  IID_ITREEVIEWCONTROLLER);
	}

	InterfacePtr<IWidgetParent> iWidgetParent(this, UseDefaultIID());
	if (iWidgetParent == nil)
	{
		//ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::QueryPanelControlData::iWidgetParent == nil");										
		return;
	}

	InterfacePtr<IPanelControlData> iPanelControlData(iWidgetParent->GetParent(), UseDefaultIID());
	if (iPanelControlData == nil)
	{
		//ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::QueryPanelControlData::iPanelControlData == nil");												
		return;

	}
	IControlView* iControlView = iPanelControlData->FindWidget(kUPDCheckBoxWidgetID);
	if (iControlView == nil)
		return;
	InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
	if (iSubject == nil)
		return;
	iSubject->DetachObserver(this, IID_ITRISTATECONTROLDATA);
}

void UPDTreeObservser::Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy)
{
	//CA("UPDTreeObservser::Update");	
	
	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
	{
		CAlert::ErrorAlert("Interface for IAppFramework not found.");
		return;
	}

	ptrIAppFramework->LogError("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update");			
	bool16 isONESource = kFalse ;
	PMString selectedClassName = "";

	int32 asd = (int32)UniqueNodeIds.size();

	switch(theChange.Get())
	{
		case kListSelectionChangedMessage:
		{ 
			ptrIAppFramework->LogDebug("Inside AP7_CategoryBrowser::CTBClassTreeObserver::Update:kListSelectionChangedMessage");		
			//if(isSelectEventCall)
			//{
			//	//CA("isSelectEventCall");
			//	//isSelectEventCall = kFalse;
			//	return;
			//}
			//CA("kListSelectionChangedMessage"); 
			InterfacePtr<ITreeViewController> treeViewCntrl(this, UseDefaultIID());
			if(treeViewCntrl==nil) 
			{
				ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeObserver::Update::treeViewCntrl == nil");			
				break;
			}

			NodeIDList selectedItem;
			
			treeViewCntrl->GetSelectedItems(selectedItem);
			if(selectedItem.size()<=0)
			{
				ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeObserver::Update::selectedItem.size()<=0");			
				break;
			}

			NodeID nid=selectedItem[0];
			TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);

			int32 uid= uidNodeIDTemp->Get();
			                                          

			break;
		}
		

			
	}

}


