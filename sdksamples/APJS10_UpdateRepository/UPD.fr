//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// General includes:
#include "MenuDef.fh"
#include "ActionDef.fh"
#include "ActionDefs.h"
#include "AdobeMenuPositions.h"
#include "LocaleIndex.h"
#include "PMLocaleIds.h"
#include "StringTable.fh"
#include "ObjectModelTypes.fh"
#include "ShuksanID.h"
#include "ActionID.h"
#include "CommandID.h"
#include "WorkspaceID.h"
#include "WidgetID.h"
#include "BuildNumber.h"
#include "PanelList.fh"

#include "InterfaceColorDefines.h"
#include "IControlViewDefs.h"
#include "SysControlIDs.h"
#include "Widgets.fh"	// for PalettePanelWidget or DialogBoss
#include "LayoutID.h"
#include "GenericID.h"
#include "TextID.h"	// kTextISuiteBoss
#include "TablesID.h"
#include "LNGID.h"
#include "PlugInModel_UIAttributes.h"


#include "Tips.fh"
#include "TipsId.h"

// Project includes:
#include "UPDID.h"
#include "ShuksanID.h"
#include "AFWJSID.h"


#ifdef __ODFRC__

/*  
 * Plugin version definition.
 */
resource PluginVersion (kSDKDefPluginVersionResourceID)
{
	kTargetVersion,
	kUPDPluginID,
	kSDKDefPlugInMajorVersionNumber, kSDKDefPlugInMinorVersionNumber,
	kSDKDefHostMajorVersionNumber, kSDKDefHostMinorVersionNumber,
	kUPDCurrentMajorFormatNumber, kUPDCurrentMinorFormatNumber,
	{ kInDesignProduct, kInCopyProduct },
	{ kWildFS },
	kUIPlugIn,
	kUPDVersion
};

/*  
 * The ExtraPluginInfo resource adds extra information to the Missing Plug-in dialog
 * that is popped when a document containing this plug-in's data is opened when
 * this plug-in is not present. These strings are not translatable strings
 * since they must be available when the plug-in isn't around. They get stored
 * in any document that this plug-in contributes data to.
 */
resource ExtraPluginInfo(1)
{
	kUPDCompanyValue,			// Company name
	kUPDMissingPluginURLValue,	// URL 
	kUPDMissingPluginAlertValue,	// Missing plug-in alert text
};

/* 
 * Boss class definitions.
 */
resource ClassDescriptionTable(kSDKDefClassDescriptionTableResourceID)
{{{

	/*
	 * This boss class supports two interfaces:
	 * IActionComponent and IPMPersist.
     *
	 * 
	 * @ingroup apjs9_updaterepository
	 */
	Class
	{
		kUPDActionComponentBoss,
		kInvalidClass,
		{
			// Handle the actions from the menu.
			IID_IACTIONCOMPONENT, kUPDActionComponentImpl,
			// Persist the state of the menu across application instantiation. Implementation provided by the API.
			IID_IPMPERSIST, kPMPersistImpl
		}
	},

    /*
	 * This boss class inherits from an API panel boss class, and
	 * adds an interface to control a pop-up menu on the panel.
	 * The implementation for this interface is provided by the API.
     *
	 * 
	 * @ingroup apjs9_updaterepository
	 */
	Class
	{
		kUPDPanelWidgetBoss ,
		kErasablePrimaryResourcePanelWidgetBoss ,
		{
			/** The plug-in's implementation of ITextControlData with an exotic IID of IID_IPANELMENUDATA.
			Implementation provided by the API.
			*/
			IID_IPANELMENUDATA, kCPanelMenuDataImpl,
			//  IID_ICONTROLVIEW, kPalettePanelView,	
			//	IID_ICONTROLVIEWOBSERVERS, kCControlViewObserversImpl,		
			IID_IOBSERVER, kUPDContentSelectionObserverImpl,//((((()))))//
			IID_IPANELDETAILCONTROLLER, kSizePanelDetailControllerImpl ,
			//IID_ICONTROLVIEW, kUPDUpdateControlViewImpl ,
		}
	},

	Class 
	{
		kUPDPanelListBoxWidgetBoss,
		kWidgetListBoxWidgetNewBoss,
		{
			//IID_IOBSERVER,	kUPDPanelListBoxObserverImpl,
		}
	},

	Class 
	{
		kUPDDocWchResponderServiceBoss,
		kInvalidClass,
		{
			/** 
			Identifies this boss as providing a responder service for 
			multiple document actions, such as for doc open, close, etc. 
			If only one service was needed, then we'd reuse an API-defined 
			implementation ID listed in DocumentID.h.
			*/
			IID_IK2SERVICEPROVIDER,	kUPDDocWchServiceProviderImpl,
			/** 
			Performs the responder service. This implementation delivers
			every service promised by the IK2ServiceProvider implementation.
			*/
			IID_IRESPONDER,	kUPDDocWchResponderImpl,
		}
	},
//	AddIn
//	{
//		kIntegratorSuiteBoss,
//		kInvalidClass,
//		{
//			/** 
//				@see RfhSuiteASB
//			*/
//			IID_IUPDSUITE, kUPDSuiteASBImpl,
//		}
//	},
	
	/**
		Adds ITinMutSuite to the text selection boss allowing 
		text inset to be manipulated for the frames touched
		by the text selection.		
		@ingroup textinsetmutator
	*/
//	AddIn
//	{
//		kTextSuiteBoss,
//		kInvalidClass,
//		{
//			/** 
//				@see RfhSuiteTextCSB
//			*/
//			IID_IUPDSUITE, kUPDSuiteTextCSBImpl,
//		}
//	},

	Class
	{
		kUPDLoginEventsHandler,
		kInvalidClass,
		{
			IID_ILOGINEVENT, kUPDLoginEventsHandlerImpl,
		}
	},
	
	Class
    {
        kUPDPanelActionFilterBoss,
        kInvalidClass,
        {
            IID_IK2SERVICEPROVIDER,        kActionFilterProviderImpl,
            IID_IACTIONFILTER,             kUPDActionFilterImpl,
            IID_IPMPERSIST,                kPMPersistImpl,
        }
    };
	
	/////////// Classes for tree////////////////////

	Class
	{
		kUPDTreeViewWidgetBoss,
		kTreeViewWidgetBoss,
		{
			IID_ITREEVIEWHIERARCHYADAPTER, kUPDTreeViewHierarchyAdapterImpl,
			IID_ITREEVIEWWIDGETMGR, kUPDTreeViewWidgetMgrImpl,
			//IID_IOBSERVER, kUPDTreeObserverImpl;	// Commeted so tht we can get the Drag Drop Event handler
		}
	},	

	Class
	{
		kUPDTreeNodeWidgetBoss,
		kTreeNodeWidgetBoss,
		{
			/** What the application framework thinks is the  control's event handler */
//			IID_IEVENTHANDLER, kRFHTrvNodeEHImpl,
//			/** The real event handler associated with this control, we delegate to this implementation */
//			IID_IRFHTRVSHADOWEVENTHANDLER,  kTreeNodeEventHandlerImpl,
//			/** Observer for changes in node state such as expand/contract */
//			IID_IOBSERVER,  kRFHTreeObserverImpl,
//			/** Provides the node with drag source capability for the SysFile
//				associated with the widget.
//				See PnlTrvDragDropSource. 
//			 */
//			IID_IDRAGDROPSOURCE, kTPLDragSourceImpl, /* kPnlTrvDragDropSourceImpl,*/
		}
	},

}}};

resource TipTable(33)
{{
	//kUPDLocateButtonWidgetID, "Locate Update",
	//kUPDRefreshButtonWidgetID, "Update",
	//kUPDReloadIconWidgetID, "Reload",
	/*kRefreshWidgetID, "Refresh",
	kSprayButtonWidgetID, "Spray Product",
	kPreviewWidgetID, "Preview",
	kSubSectionSprayButtonWidgetID, "Spray Sub-Section",*/
}}

/*  Implementation definition.
*/
resource FactoryList (kSDKDefFactoryListResourceID)
{
	kImplementationIDSpace,
	{
		#include "UPDFactoryList.h"
	}
};


/*  Menu definition.
*/
resource MenuDef (kSDKDefMenuResourceID)
{
	{
		// The About Plug-ins sub-menu item for this plug-in.
	
		kUPDPanelPSMenuActionID,
		"Main:"kAFWJSPRINTsourceKey , //"Main:&PRINTsource7",
		5,
		kSDKDefIsNotDynamicMenuFlag,
		
		//*******Added By Sachin.
//		kUPDMenuItem4ActionID, // Unique Attributes
//		kUPDTargetMenuPath,
//		kLNGDialogMenuItemPosition + 22, //1, //kLNGDialogMenuItemPosition + 16,//kUPDMenuItem1MenuItemPosition,
//		kSDKDefIsNotDynamicMenuFlag,
//		//**********	
//		
//		kUPDMenuItem1ActionID,
//		kUPDTargetMenuPath,
//		kLNGDialogMenuItemPosition + 24, //kUPDMenuItem1MenuItemPosition,
//		kSDKDefIsNotDynamicMenuFlag,
//		
//		kUPDMenuItem2ActionID,
//		kUPDTargetMenuPath,
//		kLNGDialogMenuItemPosition + 23, //kUPDMenuItem2MenuItemPosition,
//		kSDKDefIsNotDynamicMenuFlag,
		


	}
};

/*
// Action definition.
*/
resource ActionDef (kSDKDefActionResourceID)
{
	{

		kUPDActionComponentBoss,
		kUPDPanelPSMenuActionID,
		kUPDUpateApsivaStringKey, //"Update Apsiva", //"Update ONEsource",  //"DB Updater",
		kOtherActionArea,
		kNormalAction,
		kCustomEnabling ,
		kInvalidInterfaceID,
		kSDKDefInvisibleInKBSCEditorFlag,
		
		
		//*******Added By Sachin
//		kUPDActionComponentBoss,
//		kUPDMenuItem4ActionID,
//		kUPDUniqueAttributesStringKey, //"Unique Attributes",
//		kOtherActionArea,
//		kNormalAction,
//		kCustomEnabling, //kDisableIfNoFrontDocument, //
//		kInvalidInterfaceID,
//		kSDKDefInvisibleInKBSCEditorFlag,
//		//********
		
//		kUPDActionComponentBoss,
//		kUPDMenuItem1ActionID,
//		kUPDMenuItem1MenuItemKey,
//		kOtherActionArea,
//		kNormalAction,
//		kCustomEnabling ,
//		kInvalidInterfaceID,
//		kSDKDefInvisibleInKBSCEditorFlag,

//		kUPDActionComponentBoss,
//		kUPDMenuItem2ActionID,
//		kUPDMenuItem2MenuItemKey,
//		kOtherActionArea,
//		kNormalAction,
//		kCustomEnabling,
//		kInvalidInterfaceID,
//		kSDKDefInvisibleInKBSCEditorFlag,



	}
};


resource PluginDependency(kSDKDefPluginDependencyResourceID)
{
	kWildFS
	{
		  kLNGPrefixNumber, // the other Plugin in which there is a boss i want to use
		  kLNGPluginName,
		  kSDKDefPlugInMajorVersionNumber,
		  kSDKDefPlugInMinorVersionNumber,
	}
}

/*
// LocaleIndex
// The LocaleIndex should have indicies that point at your
// localizations for each language system that you are
// localized for.
*/
/*
// String LocaleIndex.
*/
resource LocaleIndex (kSDKDefStringsResourceID)
{
	kStringTableRsrcType,
	{
		kWildFS, k_enUS, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_enGB, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_deDE, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_frFR, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_esES, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_ptBR, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_svSE, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_daDK, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_nlNL, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_itIT, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_nbNO, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_fiFI, kSDKDefStringsResourceID + index_enUS
		kInDesignJapaneseFS, k_jaJP, kSDKDefStringsResourceID + index_jaJP
	}
};

resource LocaleIndex (kSDKDefStringsNoTransResourceID)
{
	kStringTableRsrcType,
	{
		kWildFS, k_Wild, kSDKDefStringsNoTransResourceID + index_enUS
	}
};

resource StringTable (kSDKDefStringsNoTransResourceID + index_enUS)
{
	k_enUS,									// Locale Id
	kEuropeanMacToWinEncodingConverter,		// Character encoding converter
	{
		// No-Translate strings go here:

		kUPDInternalPopupMenuNameKey,	kUPDInternalPopupMenuNameKey,		// No need to translate, internal menu name.

	}
};

resource LocaleIndex (kUPDPanelLstBoxRsrcID)
{
	kViewRsrcType,
	{
		kWildFS, k_Wild, kUPDPanelLstBoxRsrcID + index_enUS
	}
};



/*
// Panel LocaleIndex.
*/
resource LocaleIndex (kSDKDefPanelResourceID)
{
	kViewRsrcType,
	{
		kWildFS, k_Wild, 	kSDKDefPanelResourceID + index_enUS
	}
};

resource LocaleIndex (kUPDTreePanelNodeRsrcID)
{
	 kViewRsrcType,
	 {
			kWildFS, k_Wild,    kUPDTreePanelNodeRsrcID + index_enUS
	 }
};
/*
// Type definition.
*/
type UPDPanelWidget(kViewRsrcType) : ErasablePrimaryResourcePanelWidget(ClassID = kUPDPanelWidgetBoss)
{
	CPanelMenuData ;	
	SizePanelDetailController ;
};

//(((((()))))))//
type UPDPanelLstBox(kViewRsrcType) : 
	WidgetListBoxWidgetN(ClassID = kUPDPanelListBoxWidgetBoss){ };
//((((((()))))))//

///////////////////// Tree Type Definations ///////////////////
type UPDTreeViewWidget(kViewRsrcType) : TreeViewWidget(ClassID = kUPDTreeViewWidgetBoss){}
type UPDTreePanelNodeWidget(kViewRsrcType) : PrimaryResourcePanelWidget(ClassID = kUPDTreeNodeWidgetBoss){}

/*
// PanelList definition.      
*/
resource PanelList (kSDKDefPanelResourceID)
{
	{
		// 1st panel in the list
		kSDKDefPanelResourceID,		// Resource ID for this panel (use SDK default rsrc ID)
		kUPDPluginID,			// ID of plug-in that owns this panel
		kIsResizable,
		kUPDPanelWidgetActionID,	// Action ID to show/hide the panel
		kUPDPanelTitleKey,	// Shows up in the Window list.
		"",							// Alternate menu path of the form "Main:Foo" if you want your palette menu item in a second place
		0.0,						// Alternate Menu position Alternate Menu position for determining menu order
		0,0,						// Rsrc ID, Plugin ID for a PNG icon resource to use for this palette
		c_Panel
	}
};

/*  PanelView definition.
	The view is not currently localised: therefore, it can reside here.
	However, if you wish to localise it, it is recommended to locate it in one of
	the localised framework resource files (i.e. UPD_enUS.fr etc.).
*/
//********************************************************************************
	resource PNGA(kPNGRUPDUpdateIconRsrcID)  "update_hover46x18.png"
	resource PNGR(kPNGRUPDUpdateIconRollRsrcID)  "update46x18.png"
	
	resource PNGA(kPNGRUPDReloadIconRsrcID)  "Reload_hover46x18.png"
	resource PNGR(kPNGRUPDReloadIconRollRsrcID)  "Reload_46x18.png"

	resource PNGA(kUPDSucessIconID) "UPDSucess.png"
	resource PNGA(kUPDFailIconID) "UPDFail.png"
	resource PNGA(kUPDDeactiveIconID) "de-active.png"

//*********************************************************************************
resource UPDPanelWidget(kSDKDefPanelResourceID + index_enUS)
{
	__FILE__, __LINE__,					// Localization macro
	kUPDPanelWidgetID,			// WidgetID
	kPMRsrcID_None,						// RsrcID
	kBindNone,							// Binding (0=none)
	0, 0, 220 - 13  , 291 ,				// Frame: left, top, right, bottom.
	kTrue, kTrue,						// Visible, Enabled
	kFalse,								// Erase background
	kInterfacePaletteFill,				// Erase to color
	//kFalse,								// Draw dropshadow
	kUPDPanelTitleKey,			// Panel name
	{
		
		GroupPanelWidget
		(
			kInvalidWidgetID,					// widget ID
			kPMRsrcID_None,						// PMRsrc ID
			kBindLeft | kBindRight,	 //kBindNone,							// frame binding
			Frame(1,2,219-13,32)   //Frame(1,2,219,65)	// left, top, right, bottom//(1,2,247,75)
			kTrue,								// visible
			kTrue,								// enabled
			0,									// header widget ID
			{	
			}
		),
		
		InfoStaticTextWidget
		(
			kInvalidWidgetID, 
			kPMRsrcID_None,							// WidgetId, RsrcId
			kBindLeft | kBindRight,					// Frame binding
			Frame(10,10,55,25)					 //Frame(10,33,72,51)// Frame//(10,35,100,55)
			kTrue, kTrue, kAlignLeft, 
			kEllipsizeEnd,kTrue,							// Visible, Enabled
			kUPDSelectStringKey, //"Select:",								// Text
			0,
			kPaletteWindowSystemScriptFontId, 
			kPaletteWindowSystemScriptFontId,
		),
		
		DropDownListWidget
		(
			kUPDOptionsDropDownWidgetID ,	// widget ID
			kSysDropDownPMRsrcId ,			// PMRsrc ID
			kBindLeft | kBindRight ,	//kBindNone,						// frame binding
			Frame(62-8 , 8 , 170 - 8 , 26)			//Frame(62,33,170,51)//  left, top, right, bottom//(95,35,240,55)//(85,36,212,54)
			kTrue,							// visible
			kTrue,							// enabled
			{{	
				kUPDSelect2StringKey,  //"--Select--",			
				kUPDSelectedFramesStringKey, //"Selected Frame(s)",
				//"Current Page",
				//"Page Number",
				//"Entire Document",
			}}
		),
		
//		IntEditBoxWidget
//		(
//			kUPDPageNumWidgetID ,			// widget ID
//			kSysEditBoxPMRsrcId ,			// PMRsrc ID
//			kBindRight , // -- kBindNone -- ,		// frame binding
//			Frame(180 - 8 , 8 , 210-8 , 26) //Frame(180.0,33.0,210.0,51.0)	// left, top, right, bottom
//			kTrue ,							// visible
//			kTrue ,							// enabled
//			0 ,								// nudgeWidgetId (0 or kInvalidWidgetID if no nudge required)
//			1 ,								// small nudge amount
//			5 ,								// large nudge amount
//			5 ,								// max num chars
//			kFalse ,							// read only flag
//			kTrue ,  //kFalse,				// should notify on each key stroke
//			kFalse ,							// range checking enabled
//			kFalse  ,							// blank entry allowed
//			30 ,								// upper limit
//			0 ,								// lower limit
//			" " ,								// control label
//		),	
						
		GroupPanelWidget
		(
			kInvalidWidgetID,					// widget ID
			kPMRsrcID_None,						// PMRsrc ID
			kBindLeft | kBindRight,	 //kBindNone,							// frame binding
			Frame(1 , 32 , 219-13 , 59)         //Frame(1,67,219,291)	// left, top, right, bottom//(1,77,247,103)
			kTrue,								// visible
			kTrue,								// enabled
			0,									// header widget ID
			{
			}
		),
		
		RollOverIconButtonWidget//IconSuiteWidget //ADBEIconSuiteButtonWidget
		(
					kUPDRefreshButtonWidgetID,		// widget ID
					kPNGRUPDUpdateIconRsrcID,					// resource ID
					kUPDPluginID,				// plugin ID
					kBindRight , // kBindNone,					// frame binding
					Frame(155 , 37  , 201  , 55)  //Frame(80,230,140,255)   //Frame(80,193,140,218)	/ left, top, right, bottom//(85,183,165,209)
					kTrue,						// visible
					kTrue,						// enabled
					kADBEIconSuiteButtonType,
		),
///////////////////////  Following code is added by Chetan Dogra on 05-October///
//////////////////////////////////////////////////////////////////////from here		
		RollOverIconButtonWidget//IconSuiteWidget //ADBEIconSuiteButtonWidget
		(
					kUPDReloadIconWidgetID,		// widget ID
					kPNGRUPDReloadIconRsrcID,					// resource ID
					kUPDPluginID,				// plugin ID
					kBindLeft , // kBindNone,					// frame binding
					Frame(25 , 37 , 71 , 55 )  //Frame(80,230,140,255)   //Frame(80,193,140,218)	/ left, top, right, bottom//(85,183,165,209)
					kTrue,						// visible
					kTrue,						// enabled
					kADBEIconSuiteButtonType,
		),
////following addittion is added by Nitin 27/aug/2007////from here too		
		CheckBoxWidget
		(
					kUPDSelectionBoxtWidgetID,				// WidgetId
					kSysCheckBoxPMRsrcId,					// RsrcId
					kBindNone,//kBindBottom | kBindLeft ,
					Frame( 6 , 35 , 24 , 55 )				// Frame
					kTrue,									// Visible
					kTrue,									// Enabled
					kAlignLeft,								// Alignment
					""										// Initial text
		),		
		
/////////////////////////////////////////////////////////////////////////upto here		
		


//////////////////////////////////////////////////////////////////////till here
		GroupPanelWidget
		(
			kInvalidWidgetID,					// widget ID
			kPMRsrcID_None,						// PMRsrc ID
			kBindAll , // kBindNone,							// frame binding
			Frame(1 , 60 , 206 , 291 )  //left, top, right, bottom//(1,77,247,103)
			kTrue,								// visible
			kTrue,								// enabled
			0,									// header widget ID
			{		
				//UPDPanelLstBox
				//(
				//	kUPDPanelLstboxWidgetID, 
				//	kSysOwnerDrawListBoxPMRsrcId,		// WidgetId, RsrcId
				//	kBindAll , // kBindNone,			// Frame binding
				//	Frame(2,2,215-13,225+8 - 22 )  //Frame(2,2,215,188)					// Frame	
				//	kTrue, kTrue,						// Visible, Enabled//(4,3,184,167)
				//	1,
				//	0,									// List dimensions
				//	17,									// Cell height
				//	1,									// Border width
				//	kFalse,kTrue,						// Has scroll bar (v,h)
				//	kFalse,								// Multiselection
				//	kFalse,								// List items can be reordered
				//	kFalse,								// Draggable to new/delete buttons
				//	kFalse,								// Drag/Dropable to other windows
				//	kTrue,								// An item always has to be selected
				//	kFalse,								// Don't notify on reselect
				//	kUPDPanelLstBoxRsrcID				// Fill list box with widgets with this ID (default s 0)
				//	{
				//		CellPanelWidget
				//		(
				//			kCellPanelWidgetID, kPMRsrcID_None,		// WidgetId, RsrcId
				//			kBindAll,								// Frame binding
				//			//Frame(-1,1,236,1600),					// Frame//(-1,1,208,1600)
				//			Frame(-1,1,220-13,1600),	
				//			kTrue, kTrue							// Visible, Enabled
				//			{
				//												
				//			}
				//		)
				//	},
				//),
				
				UPDTreeViewWidget
				(
					kUPDPanelTreeViewWidgetID,		// widget ID
					kPMRsrcID_None,					// PMRsrc ID
					kBindAll,						// frame binding
					Frame(2 , 2 , 202, 211)				// left, top, right, bottom	//Frame(1,2,201 ,266)//Frame(1,2,201,190)				
					kTrue, kTrue					// visible
					kTrue,
					kInterfacePaletteFill,			// enabled
					"",								// control label
													// TreeAttributes properties
					kFalse,							// fShouldDisplayRootNode
					kFalse,							// fShouldUseHScrollBar
					kTrue,							// fShouldUseVScrollBar
					18,								// fVScrollButtonIncrement
					18,								// fVThumbScrollIncrement
					18,								// fHScrollButtonIncrement
					18,								// fHThumbScrollIncrement
													// TreeViewController properties
					1,								// fItemsSelectableMode
					kFalse,							// fShouldAllowChildrenFromMultipleParentsSelected
					kFalse,							// fShouldAllowDiscontiguousSelection
					{								// CPanelControlData Children
					}
				),
		
			
		
				
			}
		),						
	}

	kUPDInternalPopupMenuNameKey		// Popup menu name (internal)
	
};

resource UPDTreePanelNodeWidget (kUPDTreePanelNodeRsrcID + index_enUS)
{
	__FILE__, __LINE__,
	kUPDTreePanelNodeWidgetID, kPMRsrcID_None,					// WidgetId, RsrcId
	kBindLeft | kBindRight,						// Frame binding
	Frame(0, 0, 208, 19),						// Frame
	kTrue, kTrue,								// Visible, Enabled
	"",											// name
	{
		TreeExpanderWidget
		(
			kUPDTreeNodeExpanderWidgetID,                    // WidgetId
			kTreeBranchCollapsedRsrcID, 
			kApplicationRsrcPluginID,						// RsrcId for collapsed node icon
			kBindLeft,
			Frame(0,0,0,0)								// Frame//(0,2,16,18)
			kTrue, kTrue,									// Visible, Enabled
			kTreeBranchExpandedRsrcID, 
			kApplicationRsrcPluginID,						// RsrcId for expanded node icon
			kTrue,											// Cmd-Click expands/collapses children,
			kTrue,											// Scroll to show children when expanded
			kIconNoCentering,
			kIconRaisedLook,		// Sets the icon look -- (kIconRaisedLook | kIconFlushLook)
			kFalse					// Bool integer sets whether the icon draws a well.

		),

//		IconSuiteWidget //index 0
//		(
//			kUPDUnCheckIconWidgetID,
//			kUPDUnChkIconID,
//			kUPDPluginID,
//			kBindLeft,
//			Frame(2,0, 21, 20),
//			kTrue, kTrue, 0,
//			kADBEIconSuiteButtonType
//		),
//		IconSuiteWidget //index 1
//		(
//			kUPDCheckIconWidgetID,
//			kUPDChkIconID,
//			kUPDPluginID,
//			kBindLeft,
//			Frame(2,0, 21, 20),
//			kTrue, kTrue, 0,
//			kADBEIconSuiteButtonType
//		),

		CheckBoxWidget
		(
			kUPDCheckBoxWidgetID,		// WidgetId
			kSysCheckBoxPMRsrcId,				// RsrcId
			kBindBottom | kBindLeft ,			// frame binding
			Frame(2,0, 22, 20)					// Frame	
			kTrue,								// Visible
			kTrue,								// Enabled
			kAlignLeft,							// Alignment
			"",//"Display Name"     // "Field Name" //"Attribute Name"		// Initial text
		),

		RollOverIconButtonWidget //index 2
		(
			kUPDSucessIconWidgetID,
			kUPDSucessIconID,
			kUPDPluginID,
			kBindLeft,
			Frame(22,0, 35, 20),
			kTrue, kTrue, 
			kADBEIconSuiteButtonType
					
		),
		
		RollOverIconButtonWidget //index 3
		(
			kUPDFailIconWidgetID,
			kUPDFailIconID,
			kUPDPluginID,
			kBindLeft,
			Frame(22,0, 35, 20),
			kTrue, kTrue, 
			kADBEIconSuiteButtonType
				
		),
		
		RollOverIconButtonWidget //index 4
		(
			kUPDDeactiveIconWidgetID,
			kUPDDeactiveIconID,
			kUPDPluginID,
			kBindLeft,
			Frame(22,0, 35, 20),
			kTrue, kTrue, 
			kADBEIconSuiteButtonType
		),

		
		InfoStaticTextWidget //index 5
		(
			kUPDPanelTextWidgetID, kPMRsrcID_None,	// WidgetId, RsrcId
			kBindLeft | kBindRight,				// Frame binding
			Frame(40,0,208,21)					// Frame
			kTrue, kTrue, kAlignLeft, 
			kEllipsizeEnd,kTrue,						// Visible, Enabled
			"",									// Text
			0,
			kPaletteWindowSystemScriptFontId, 
			kPaletteWindowSystemScriptHiliteFontId,
		),
	}
};



#endif // __ODFRC__

#include "UPD_enUS.fr"
#include "UPD_jaJP.fr"

//  Code generated by DollyXS code generator
