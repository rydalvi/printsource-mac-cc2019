#include "VCPluginHeaders.h"
#include "IPMUnknown.h"
#include "UIDRef.h"
//#include "ITextFrame.h"		//CS3 CHANGE
#include "IXMLUtils.h"
#include "IXMLTagList.h"
//#include "ISelection.h"		//CS3 CHANGE
#include "IDocument.h"
#include "IStoryList.h"
#include "IFrameList.h"
#include "ITextModel.h"
#include "IIDXMLElement.h"
#include "IXMLReferenceData.h"
#include "IGraphicFrameData.h"
#include "UIDList.h"
#include "XMLReference.h"
//#include "LayoutUtils.h"   //CS4
#include "k2smartptr.h"
#include "CAlert.h"
#include "IXMLUtils.h"
#include "TGRID.h"
#include "TagStruct.h"
#include "CPMUnknown.h"
#include "ITagReader.h"
#include "IFrameUtils.h"
//#include "ISelectionManager.h"
//#include "ISelectionUtils.h"
//#include "ITextMiscellanySuite.h"

#include "ITextFrameColumn.h"
#include "IMultiColumnTextFrame.h"
#include "IHierarchy.h"
#include "IAppFramework.h"
#include "IXMLAttributeCommands.h"


#define CA(x) CAlert::InformationAlert(x)

class TGRTagReader:public CPMUnknown<ITagReader>
{
public:
	TGRTagReader(IPMUnknown* boss);
	~TGRTagReader();

	TagList getTagsFromBox(UIDRef, IIDXMLElement ** xmlPtr=nil);
	TagList getFrameTags(UIDRef frameUIDRef);
	bool16	GetUpdatedTag(TagStruct&);
	void	getTextFrameTags(void);
	void	getGraphicFrameTags(void);
	bool16	getCorrespondingTagAttributes(const PMString&, const PMString&, TagStruct&);
	void	getTextFrameTagsForRefresh(void); // To be use only for Refresh Content
	TagList getTagsFromBox_ForRefresh(UIDRef boxId, IIDXMLElement** xmlPtr=nil);   // to be used only for Refresh
	TagList getTextFrameTagsForIndex(UIDRef boxUIDRef);
	TagList getTagsFromBox_ForUpdate(UIDRef boxId, IIDXMLElement** xmlPtr=nil);
	void getTextFrameTagsForUpdate(void);
	void getGraphicFrameTagsForUpdate(void);
	TagList getTagsFromBoxForUpdate(UIDRef boxId, IIDXMLElement** xmlPtr =nil);
	bool16 getCorrespondingTagAttributesForUpdate(const PMString&, const PMString&, TagStruct& /*,int32&*/);
	
	TagList getTagsFromBox_ForRefresh_ByAttribute(UIDRef boxId, IIDXMLElement** xmlPtr=nil);   // To be use only for Refresh Content Attribute Level Refresh
	void getTextFrameTagsForRefresh_ByAttribute(void); // To be use only for Refresh Content Attribute Level Refresh

	TagList getAllChildTags(IIDXMLElement* xmlElement);

	void insertStencilNameInTag(UIDRef BoxUIDRef,PMString stencilName);
	PMString getStencilNameAndIndex(UIDRef BoxUIDRef,PMString& index);

	void insertStencilNameInTag_PrintExpress(UIDRef BoxUIDRef,PMString stencilName,int32 templateId = -1);
};

CREATE_PMINTERFACE(TGRTagReader, kTagReaderImpl)

TGRTagReader::TGRTagReader(IPMUnknown* boss):CPMUnknown<ITagReader>(boss)
{}

TGRTagReader::~TGRTagReader()
{}

void TGRTagReader::getTextFrameTags(void)
{
	do
	{
		//CA("TGRTagReader::getTextFrameTags");
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
			return;

		/*InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		if (textFrame == nil){
			//CA("textFrame == nil");
				break;
		}*/
		UID result = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			
			result = graphicFrameDataOne->GetTextContentUID();
			if(result ==kInvalidUID )
			{
				//CA("Not a textFrame");
				ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::Not a textFrame");
			}
		}
			
		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::getTextFrameTags:graphicFrameHierarchy == nil");
			return;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::getTextFrameTags:multiColumnItemHierarchy == nil");
			return;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::getTextFrameTags:multiColumnItemTextFrame == nil");
			//CA("Its Not MultiColumn in Tag Reader");
			return;
		}

		InterfacePtr<IHierarchy> frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::getTextFrameTags:frameItemHierarchy == nil");
			continue;
		}

		InterfacePtr<ITextFrameColumn> textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			//CA("!!!ITextFrameColumn in Tag Reader");
			ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::getTextFrameTags:textFrame == nil");
			continue;
		}
					
		InterfacePtr<ITextModel> objTxtMdl (textFrame->QueryTextModel());
		if (objTxtMdl == nil){ //CA("objTxtMdl == nil");  
			break;
		}

//		IXMLReferenceData* objXMLRefDat=(IXMLReferenceData*) objTxtMdl->QueryInterface(IID_IXMLREFERENCEDATA);
//		if (objXMLRefDat==nil){
//			//CA("objXMLRefDat==nil ");
//			break;
//		}
		UIDRef txtMdlUIDRef =::GetUIDRef(objTxtMdl);
		if (txtMdlUIDRef.GetDataBase() == nil) 
			break;

		InterfacePtr<IXMLReferenceData> objXMLRefDat(txtMdlUIDRef, UseDefaultIID());
		if (objXMLRefDat==nil){
			//CA("objXMLRefDat==nil ");
			break;
		}

		XMLReference xmlRef=objXMLRefDat->GetReference();

		UIDRef refUID=xmlRef.GetUIDRef();		

		//IIDXMLElement *xmlElement=xmlRef.Instantiate();
		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
		if(xmlElement==nil){
			//CA("xmlElement==nil");
			break;
		}

		//xmlPtr=xmlElement;
		
		PMString tagName=xmlElement->GetTagString();
		//if(tagName == "")

		int elementCount=xmlElement->GetChildCount();
		
		//if(elementCount == 0)
		//{
		//	TagStruct tInfo;
		//	
		//	int32 attribCount=xmlElement->GetAttributeCount();
		//	TextIndex sIndex=0, eIndex=0;
		//	Utils<IXMLUtils>()->GetElementIndices(xmlElement, &sIndex, &eIndex);
		//	
		//	tInfo.startIndex=sIndex;
		//	tInfo.endIndex=eIndex;
		//	tInfo.tagPtr=xmlElement;
		//	
		//	for(int j=0; j<attribCount; j++)
		//	{
		//		PMString attribName=xmlElement->GetAttributeNameAt(j);
		//		PMString attribVal=xmlElement->GetAttributeValue(attribName);
		//		//CA(attribName + " : " + attribVal);
		//		getCorrespondingTagAttributes(attribName, attribVal, tInfo);
		//	}
		//	tInfo.curBoxUIDRef=boxUIDRef;
		//	//CA("******** Before Push in to tList");
		//	tList.push_back(tInfo);

		//}
		for(int i=0; i<elementCount; i++)
		{	
			
			//CA("Inside For loop of element count");
			XMLReference elementXMLref=xmlElement->GetNthChild(i);
			//IIDXMLElement * childElement=elementXMLref.Instantiate();
			InterfacePtr<IIDXMLElement>childElement(elementXMLref.Instantiate());
			if(childElement==nil){
				//CA("childElement==nil");
				continue;
			}
			PMString tagName=childElement->GetTagString();
			//CA("TGRTagReader::getTextFrameTags : else : tagName = " + tagName);
					
			TagStruct tInfo;
			
			int32 attribCount=childElement->GetAttributeCount();
		
			TextIndex sIndex=0, eIndex=0;
			Utils<IXMLUtils>()->GetElementIndices(childElement, &sIndex, &eIndex);
			
			tInfo.startIndex=sIndex;
			tInfo.endIndex=eIndex;

			childElement->AddRef();//Added 0n 17-09-08
			tInfo.tagPtr=childElement;
			
			for(int j=0; j<attribCount; j++)
			{
				PMString attribName=childElement->GetAttributeNameAt(j);
				
			
				PMString attribVal=childElement->GetAttributeValue(WideString(attribName)); //CS4
				//CA(attribName + " : " + attribVal);
				getCorrespondingTagAttributes(attribName, attribVal, tInfo);
			}
			tInfo.curBoxUIDRef=boxUIDRef;
			//CA("******** Before Push in to tList");
			tList.push_back(tInfo);
			//CA("at the end TGRTagReader::getTextFrameTags");				
		}		
	}while(0);
}

void TGRTagReader::getGraphicFrameTags(void)
{
	do{		
		//CA("Inside TGRTagReader::getGraphicFrameTags");
		InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);	
		//InterfacePtr<IXMLReferenceData> objXMLRefDat(Utils<IXMLUtils>()->QueryXMLReferenceData(unknown));
		InterfacePtr<IXMLReferenceData> objXMLRefDat(Utils<IXMLUtils>()->QueryXMLReferenceData(boxUIDRef));
		if (objXMLRefDat == nil)
		{
			//CA("objXMLRefDat == nil");
			break;
		}

		XMLReference xmlRef = objXMLRefDat->GetReference();
		UIDRef refUID = xmlRef.GetUIDRef();
		//IIDXMLElement *xmlElement = xmlRef.Instantiate();
		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
		if(xmlElement == nil)
		{
			//CA("xmlElement == nil");
			break;
		}

		//xmlPtr=xmlElement;
		PMString tagName = xmlElement->GetTagString();

		int elementCount=xmlElement->GetChildCount();
					
		TagStruct tInfo;
		xmlElement->AddRef();//Added on 17-09-08
		tInfo.tagPtr=xmlElement;

		int32 attribCount = xmlElement->GetAttributeCount();

		for(int j=0; j<attribCount; j++)
		{
			PMString attribName = xmlElement->GetAttributeNameAt(j);
			PMString attribVal  = xmlElement->GetAttributeValue(WideString(attribName));//CS4
			getCorrespondingTagAttributes(attribName, attribVal, tInfo);
		}
		tInfo.endIndex=-1;
		tInfo.startIndex=-1;
		tInfo.curBoxUIDRef=boxUIDRef;
		tList.push_back(tInfo);
	}while(0);
}

TagList TGRTagReader::getTagsFromBox(UIDRef boxId, IIDXMLElement** xmlPtr)
{
	//CA("Inside  TGRTagReader::getTagsFromBox ");
	tList.clear();
	/*InterfacePtr<IPMUnknown> unknown(boxId, IID_IUNKNOWN);
	
	UID frameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
	*/
	UID frameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxId, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		frameUID = graphicFrameDataOne->GetTextContentUID();
	}
	boxUIDRef=boxId;
	
	if(frameUID == kInvalidUID)
	{
		//CA("caling getGraphicFrameTags");
		getGraphicFrameTags();		
	}
	else 
	{
		//CA("calling getTextFrameTags");
		getTextFrameTags();
		//CA("After getTextFrameTags");
	}
	for(int i=0; i<tList.size(); i++)
		tList[i].isProcessed=kFalse;
		
	//if(xmlPtr)
	//	*xmlPtr=this->xmlPtr;
	
	return tList;
}

bool16 TGRTagReader::getCorrespondingTagAttributes
	(const PMString& attribName, const PMString& attribVal, TagStruct& tStruct)
{
	if(attribName.IsEqual("ID", kFalse))
	{
		tStruct.elementId=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}	
	if(attribName.IsEqual("typeId", kFalse))
	{
		tStruct.typeId=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}
	if(attribName.IsEqual("header", kFalse))
	{
		tStruct.header=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}
	if(attribName.IsEqual("isEventField", kFalse))
	{
		int32 isEventField = attribVal.GetAsNumber();
		if(isEventField == 1)
		{
			tStruct.isEventField = kTrue;
		}
		else
		{
			tStruct.isEventField = kFalse;
		}

		tStruct.numValidFields++;
		 
		return kTrue;
	}
	if(attribName.IsEqual("deleteIfEmpty", kFalse))
	{
		tStruct.deleteIfEmpty=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}
	if(attribName.IsEqual("dataType", kFalse))
	{
		tStruct.dataType=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}
	if(attribName.IsEqual("isAutoResize", kFalse))
	{
		tStruct.isAutoResize=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}
	if(attribName.IsEqual("LanguageID", kFalse))
	{
		tStruct.languageID=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}
	if(attribName.IsEqual("index", kFalse))
	{
		tStruct.whichTab=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}
	if(attribName.IsEqual("pbObjectId", kFalse))
	{
		tStruct.pbObjectId=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}
	if(attribName.IsEqual("parentId", kFalse))
	{
		tStruct.parentId=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}
	if(attribName.IsEqual("childId", kFalse))
	{
		tStruct.childId=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}
	if(attribName.IsEqual("sectionID", kFalse))
	{
		tStruct.sectionID=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("parentTypeID", kFalse))
	{
		tStruct.parentTypeID=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}
	if(attribName.IsEqual("isSprayItemPerFrame", kFalse))
	{
		tStruct.isSprayItemPerFrame=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("catLevel", kFalse))
	{
		tStruct.catLevel=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}
	if(attribName.IsEqual("imgFlag", kFalse))
	{
		tStruct.imgFlag=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}
	if(attribName.IsEqual("imageIndex", kFalse))
	{
		tStruct.imageIndex=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}
	if(attribName.IsEqual("flowDir", kFalse))
	{
		tStruct.flowDir=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}
	if(attribName.IsEqual("childTag", kFalse))
	{
		tStruct.childTag=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}
	if(attribName.IsEqual("tableFlag", kFalse))
	{
		int32 isTable = attribVal.GetAsNumber();
		if(isTable == 1)
		{
		//	CA("has table");
			tStruct.isTablePresent = kTrue;
		}
		else
		{
			//CA("NO table");
			tStruct.isTablePresent = kFalse;
		}

		tStruct.numValidFields++;
		 
		return kTrue;
	}
	if(attribName.IsEqual("tableType", kFalse))
	{
		tStruct.tableType=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("tableId", kFalse))
	{
		tStruct.tableId=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}
	if(attribName.IsEqual("rowno", kFalse))
	{
		tStruct.rowno=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("colno", kFalse))
	{
		tStruct.colno=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}
	if(attribName.IsEqual("field1", kFalse))
	{
		tStruct.field1=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("field2", kFalse))
	{
		tStruct.field2=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("field3", kFalse))
	{
		tStruct.field3=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("field4", kFalse))
	{
		tStruct.field4=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("field5", kFalse))
	{
		tStruct.field5=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}
    
    if(attribName.IsEqual("groupKey", kFalse))
    {
        tStruct.groupKey=attribVal;
        tStruct.numValidFields++;
        return kTrue;
    }
	
	return kFalse;
}


bool16 TGRTagReader::GetUpdatedTag(TagStruct& tInfo)
{
	if(!tInfo.tagPtr)
		return kFalse;
	
	TextIndex sIndex=0, eIndex=0;
	Utils<IXMLUtils>()->GetElementIndices(tInfo.tagPtr, &sIndex, &eIndex);
	
	tInfo.startIndex=sIndex;
	tInfo.endIndex=eIndex;
	
	return kTrue;
}


TagList TGRTagReader::getFrameTags(UIDRef frameUIDRef)
{
	tList.clear();
	boxUIDRef=frameUIDRef;
	// Get the graphic frame tags as in case of graphic frames tag is attached with the frame itself
	getGraphicFrameTags();			

	for(int i=0; i<tList.size(); i++)
		tList[i].isProcessed=kFalse;

	return tList;
}

void TGRTagReader::getTextFrameTagsForRefresh(void) // To be use only for Refresh Content
{
	//CA("TGRTagReader::getTextFrameTagsForRefresh");
	do{	
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
			return;
			
		/*//CS3 CHANGE
		InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		if (textFrame == nil)
		{
            //CA("textFrame == nil");		
			break;
		}
*/	
		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TagReader::TGTGRTagReader::getTextFrameTagsForRefresh:graphicFrameHierarchy == nil");
			return;
		}
//CA("1");
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGTGRTagReader::getTextFrameTagsForRefresh:multiColumnItemHierarchy == nil");
			return;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGTGRTagReader::getTextFrameTagsForRefresh:multiColumnItemTextFrame == nil");
			//CA("Its Not MultiColumn");
			return;
		}

		InterfacePtr<IHierarchy> frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGTGRTagReader::getTextFrameTagsForRefresh:frameItemHierarchy == nil");
			continue;
		}
//CA("2");
		InterfacePtr<ITextFrameColumn> textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGTGRTagReader::getTextFrameTagsForRefresh:textFrame == nil");
			//CA("!!!ITextFrameColumn");
			continue;
		}
		
		InterfacePtr<ITextModel> objTxtMdl (textFrame->QueryTextModel());
		if (objTxtMdl == nil)
		{
           	break;
		}

		//IXMLReferenceData* objXMLRefDat=(IXMLReferenceData*) objTxtMdl->QueryInterface(IID_IXMLREFERENCEDATA);
		UIDRef txtMdlUIDRef =::GetUIDRef(objTxtMdl);
		if (txtMdlUIDRef.GetDataBase() == nil) 
			break;

		InterfacePtr<IXMLReferenceData> objXMLRefDat(txtMdlUIDRef, UseDefaultIID());
		if (objXMLRefDat==nil)
		{
			break;
		}
//CA("3");
		XMLReference xmlRef=objXMLRefDat->GetReference();

		UIDRef refUID=xmlRef.GetUIDRef();		
       	//IIDXMLElement *xmlElement=xmlRef.Instantiate();
		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
		if(xmlElement==nil)
		{
          	break;
		}
       //	xmlPtr=xmlElement;

		int elementCount=xmlElement->GetChildCount();    
//PMString asd("element count = ");
//asd.AppendNumber(elementCount);
//CA(asd);
		PMString tagName=xmlElement->GetTagString();
//		CA("tagName = " + tagName);
//CA("4");
		TagStruct tInfo;
		int32 attribCount=xmlElement->GetAttributeCount();
		TextIndex sIndex=0, eIndex=0;
		Utils<IXMLUtils>()->GetElementIndices(xmlElement, &sIndex, &eIndex);
		tInfo.startIndex=sIndex;
		tInfo.endIndex=eIndex;

		tInfo.tagPtr=xmlElement;
		
		for(int j=0; j<attribCount; j++)
		{
			PMString attribName=xmlElement->GetAttributeNameAt(j);
			PMString attribVal=xmlElement->GetAttributeValue(WideString(attribName)); //CS4
			getCorrespondingTagAttributes(attribName, attribVal, tInfo);
		}
		//tList.push_back(tInfo);
		if(tInfo.isTablePresent == 1 && tInfo.tableType != 2)
			this->getGraphicFrameTags();
		else
		{
			//CA("TGRTagReader::getTextFrameTagsForRefresh :inside else ");
			for(int i=0; i<elementCount; i++)
			{
           		XMLReference elementXMLref=xmlElement->GetNthChild(i);
				//IIDXMLElement * childElement=elementXMLref.Instantiate();
				InterfacePtr<IIDXMLElement>childElement(elementXMLref.Instantiate());
				if(childElement==nil)
				{
	               continue;
				}
				PMString tagName=childElement->GetTagString();
								
				TagStruct tInfo;
				int32 attribCount=childElement->GetAttributeCount();
				PMString tagNamee=childElement->GetTagString();
	//CA("tagNamee : " + tagName);		
				TextIndex sIndex=0, eIndex=0;
				Utils<IXMLUtils>()->GetElementIndices(childElement, &sIndex, &eIndex);

				tInfo.startIndex=sIndex;
				tInfo.endIndex=eIndex;
				childElement->AddRef();//Added on 17-09-08
				tInfo.tagPtr=childElement;
				
				for(int j=0; j<attribCount; j++)
				{
					PMString attribName=childElement->GetAttributeNameAt(j);
					PMString attribVal=childElement->GetAttributeValue(WideString(attribName)); //CS4
					getCorrespondingTagAttributes(attribName, attribVal, tInfo);
				}
				tList.push_back(tInfo);				
			}
		}

	}while(0);
}


TagList TGRTagReader::getTagsFromBox_ForRefresh(UIDRef boxId, IIDXMLElement** xmlPtr)   // to be used only for Refresh
{
	//CA("TGRTagReader::getTagsFromBox_ForRefresh");
	tList.clear();
	/*InterfacePtr<IPMUnknown> unknown(boxId, IID_IUNKNOWN);
	
	UID frameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
	UID frameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxId, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		frameUID = graphicFrameDataOne->GetTextContentUID();
	}
	
	boxUIDRef=boxId;
	textFrameUID=frameUID;
	if(frameUID == kInvalidUID)
	{
		//CA("caling getGraphicFrameTags");
		getGraphicFrameTags();		
	}
	else 
	{
		//CA("calling getTextFrameTagsForRefresh");
		getTextFrameTagsForRefresh();
	}
	for(int i=0; i<tList.size(); i++)
		tList[i].isProcessed=kFalse;
	
	//if(xmlPtr)
	//	*xmlPtr=this->xmlPtr;
	return tList;
}


TagList TGRTagReader::getTextFrameTagsForIndex(UIDRef boxUIDRef) // To be use only for Refresh Content
{

	//CA("calling getTextFrameTagsForIndex");
	TagList tList;
	do{	
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
			break;
				

		/*InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);
		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
		UID frameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			frameUID = graphicFrameDataOne->GetTextContentUID();
		}
	
		
	/*	InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		if (textFrame == nil)
		{
           // CA("textFrame == nil");		
			break;
		}*/
		
		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TagReader::TGTGRTagReader::getTextFrameTagsForIndex:graphicFrameHierarchy == nil");
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGTGRTagReader::getTextFrameTagsForIndex:multiColumnItemHierarchy == nil");
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGTGRTagReader::getTextFrameTagsForIndex:multiColumnItemTextFrame == nil");
			//CA("Its Not MultiColumn");
			break;
		}

		InterfacePtr<IHierarchy> frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGTGRTagReader::getTextFrameTagsForIndex:frameItemHierarchy == nil");
			break;
		}

		InterfacePtr<ITextFrameColumn> textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGTGRTagReader::getTextFrameTagsForIndex:textFrame == nil");
			//CA("!!!ITextFrameColumn");
			break;
		}
		
		InterfacePtr<ITextModel> objTxtMdl (textFrame->QueryTextModel());
		if (objTxtMdl == nil)
		{
			//CA("objTxtMdl == nil");
           	break;
		}

		//IXMLReferenceData* objXMLRefDat=(IXMLReferenceData*) objTxtMdl->QueryInterface(IID_IXMLREFERENCEDATA);
		UIDRef txtMdlUIDRef =::GetUIDRef(objTxtMdl);
		if (txtMdlUIDRef.GetDataBase() == nil) 
			break;

		InterfacePtr<IXMLReferenceData> objXMLRefDat(txtMdlUIDRef, UseDefaultIID());
		if (objXMLRefDat==nil)
		{
			//CA("objXMLRefDat==nil");
			break;
		}

		XMLReference xmlRef=objXMLRefDat->GetReference();

		UIDRef refUID=xmlRef.GetUIDRef();		
       	//IIDXMLElement *xmlElement=xmlRef.Instantiate();
		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
		if(xmlElement==nil)
		{
			//CA("xmlElement==nil");
          	break;
		}
		
       	xmlPtr=xmlElement;
	
		int elementCount=xmlElement->GetChildCount();    		
			
		PMString tagName=xmlElement->GetTagString();
		TagStruct tInfo;
		int32 attribCount=xmlElement->GetAttributeCount();
		TextIndex sIndex=0, eIndex=0;
		Utils<IXMLUtils>()->GetElementIndices(xmlElement, &sIndex, &eIndex);
		tInfo.startIndex=sIndex;
		tInfo.endIndex=eIndex;
		tInfo.tagPtr=xmlElement;
		
		for(int j=0; j<attribCount; j++)
		{
			PMString attribName=xmlElement->GetAttributeNameAt(j);
			PMString attribVal=xmlElement->GetAttributeValue(WideString(attribName)); //CS4
			getCorrespondingTagAttributes(attribName, attribVal, tInfo);
		}
		if(tInfo.isTablePresent == 1 && tInfo.tableType != 2)
			this->getGraphicFrameTags();
		else
		{
			for(int i=0; i<elementCount; i++)
			{
				XMLReference elementXMLref=xmlElement->GetNthChild(i);
				//IIDXMLElement * childElement=elementXMLref.Instantiate();
				InterfacePtr<IIDXMLElement>childElement(elementXMLref.Instantiate());
				if(childElement==nil)
				{	               
					continue;
				}
				
///////////////////////////////////////////////////////
				//tList.push_back(tInfo);
				PMString tableFlagValue = childElement->GetAttributeValue(WideString("tableFlag")); //Cs4
				PMString ID = childElement->GetAttributeValue(WideString("ID"));//CS4

				if(tableFlagValue == "-11" && ID == "-101")
				{
					//CA("got it");
					int32 childCount = childElement->GetChildCount();
					for(int32 childIndex = 0;childIndex < childCount;childIndex++)
					{
						XMLReference elementXMLref=childElement->GetNthChild(childIndex);
						//IIDXMLElement * grandChildElement=elementXMLref.Instantiate();
						InterfacePtr<IIDXMLElement>grandChildElement(elementXMLref.Instantiate());
						if(grandChildElement==nil)
						{			               
							continue;
						}
						PMString tagName=grandChildElement->GetTagString();
						TagStruct tInfo;
						int32 attribCount=grandChildElement->GetAttributeCount();
						TextIndex sIndex=0, eIndex=0;
						Utils<IXMLUtils>()->GetElementIndices(grandChildElement, &sIndex, &eIndex);
						tInfo.startIndex=sIndex;
						tInfo.endIndex=eIndex;

				/*PMString sindex = "";
				sindex.AppendNumber(sIndex);
				CA("new function if       sIndex =" + sindex);
				sindex.Clear();
				sindex.AppendNumber(eIndex);
				CA("new function if        sIndex =" + sindex);
				sindex.Clear();*/
						grandChildElement->AddRef();//Added on 17-09-08
						tInfo.tagPtr=grandChildElement;
						
						for(int j=0; j<attribCount; j++)
						{
							PMString attribName=grandChildElement->GetAttributeNameAt(j);
							PMString attribVal=grandChildElement->GetAttributeValue(WideString(attribName));//CS4
							getCorrespondingTagAttributes(attribName, attribVal, tInfo);
						}
						tList.push_back(tInfo);				

					}
				}
				
				///////////////////////////////////////////////////////
				else
				{
					PMString tagName=childElement->GetTagString();
					TagStruct tInfo;
					int32 attribCount=childElement->GetAttributeCount();
					TextIndex sIndex=0, eIndex=0;
					Utils<IXMLUtils>()->GetElementIndices(childElement, &sIndex, &eIndex);
					tInfo.startIndex=sIndex;
					tInfo.endIndex=eIndex;
					childElement->AddRef();//Added on 17-09-08
					tInfo.tagPtr=childElement;
					
			/*PMString sindex = "";
			sindex.AppendNumber(sIndex);
			CA("new function else        sIndex= " + sindex);
			sindex.Clear();
			sindex.AppendNumber(eIndex);
			CA("new function else         sIndex= " + sindex);
			sindex.Clear();*/
					for(int j=0; j<attribCount; j++)
					{
						PMString attribName=childElement->GetAttributeNameAt(j);
						PMString attribVal=childElement->GetAttributeValue(WideString(attribName));//CS4
						getCorrespondingTagAttributes(attribName, attribVal, tInfo);
					}
					tList.push_back(tInfo);		
				}
			}
		}

	}while(0);

	return tList;
}

TagList TGRTagReader::getTagsFromBox_ForUpdate(UIDRef boxId, IIDXMLElement** xmlPtr)   // to be used only for Refresh
{
	//CA("TGRTagReader::getTagsFromBox_ForUpdate");
	tList.clear();
	/*InterfacePtr<IPMUnknown> unknown(boxId, IID_IUNKNOWN);
	
	UID frameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
	UID frameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxId, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		frameUID = graphicFrameDataOne->GetTextContentUID();
	}
	
	
	boxUIDRef=boxId;
	textFrameUID=frameUID;
	if(frameUID == kInvalidUID)
	{
		//CA("caling getGraphicFrameTags");
		getGraphicFrameTagsForUpdate();		
	}
	else 
	{
		//CA("calling getTextFrameTagsForUpdate");
		getTextFrameTagsForUpdate();
	}
	for(int i=0; i<tList.size(); i++)
		tList[i].isProcessed=kFalse;
	
	//if(xmlPtr)
	//	*xmlPtr=this->xmlPtr;
	return tList;
}

void TGRTagReader::getTextFrameTagsForUpdate(void) // To be use only for Update
{
	//CA("TGRTagReader::getTextFrameTagsForUpdate");
	do{	
//		
		/*InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		if (textFrame == nil)
		{
            CA("textFrame == nil");		
			break;
		}*/
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
			return;
		
	
		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TagReader::TGTGRTagReader::getTextFrameTagsForUpdate:graphicFrameHierarchy == nil");	
			return;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGTGRTagReader::getTextFrameTagsForUpdate:multiColumnItemHierarchy == nil");	
			return;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGTGRTagReader::getTextFrameTagsForUpdate:multiColumnItemTextFrame == nil");	
			//CA("Its Not MultiColumn");
			return;
		}

		InterfacePtr<IHierarchy> frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGTGRTagReader::getTextFrameTagsForUpdate:frameItemHierarchy == nil");	
			continue;
		}

		InterfacePtr<ITextFrameColumn> textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGTGRTagReader::getTextFrameTagsForUpdate:textFrame == nil");	
			//CA("!!!ITextFrameColumn");
			continue;
		}

		
		InterfacePtr<ITextModel> objTxtMdl (textFrame->QueryTextModel());
		if (objTxtMdl == nil)
		{
           	break;
		}

		//IXMLReferenceData* objXMLRefDat=(IXMLReferenceData*) objTxtMdl->QueryInterface(IID_IXMLREFERENCEDATA);
		UIDRef txtMdlUIDRef =::GetUIDRef(objTxtMdl);
		if (txtMdlUIDRef.GetDataBase() == nil) 
			break;

		InterfacePtr<IXMLReferenceData> objXMLRefDat(txtMdlUIDRef, UseDefaultIID());
		if (objXMLRefDat==nil)
		{
			break;
		}

		XMLReference xmlRef=objXMLRefDat->GetReference();

		UIDRef refUID=xmlRef.GetUIDRef();	
		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
		if(xmlElement==nil)
		{
          	break;
		}
      //xmlPtr=xmlElement;

		int elementCount=xmlElement->GetChildCount();    		
			
		PMString tagName=xmlElement->GetTagString();
		//CA("tagName = " + tagName);

		TagStruct tInfo;
		int32 attribCount=xmlElement->GetAttributeCount();
		TextIndex sIndex=0, eIndex=0;
		Utils<IXMLUtils>()->GetElementIndices(xmlElement, &sIndex, &eIndex);
		tInfo.startIndex=sIndex;
		tInfo.endIndex=eIndex;
		tInfo.tagPtr=xmlElement;
		
		for(int j=0; j<attribCount; j++)
		{
			PMString attribName=xmlElement->GetAttributeNameAt(j);
			PMString attribVal=xmlElement->GetAttributeValue(WideString(attribName));//CS4
			getCorrespondingTagAttributes(attribName, attribVal, tInfo);
		}
		//tList.push_back(tInfo);
		if(tInfo.isTablePresent == 1 && tInfo.tableType != 2)
			this->getGraphicFrameTagsForUpdate();
		else
		{
			//CA("TGRTagReader::getTextFrameTagsForUpdate :inside else ");
			for(int i=0; i<elementCount; i++)
			{
           		XMLReference elementXMLref=xmlElement->GetNthChild(i);
				InterfacePtr<IIDXMLElement>childElement(elementXMLref.Instantiate());
				if(childElement==nil)
				{
	               
					continue;
				}
				PMString tagName=childElement->GetTagString();
				//CA("TGRTagReader::getTextFrameTagsForUpdate : else : tagName = " + tagName);
				if(childElement->GetChildCount()!= 0)/*(tagName == "PSTable0")*/
				{
					int cellCount=childElement->GetChildCount(); 
					for(int cell = 0 ; cell< cellCount ; cell++)
					{

						XMLReference cellXMLref=childElement->GetNthChild(cell);
						//IIDXMLElement * cellElement=cellXMLref.Instantiate();
						InterfacePtr<IIDXMLElement>cellElement(cellXMLref.Instantiate());
						if(cellElement==nil)
						{
							continue;
						}
						PMString tagName=cellElement->GetTagString();
						//CA("TGRTagReader::getTextFrameTagsForUpdate : else : tagName = " + tagName);

						if(cellElement->GetChildCount()!= 0)/*PSCell or any attribute*/
						{
							int tagsInsideCellCount=cellElement->GetChildCount(); 
							for(int cell = 0 ; cell< tagsInsideCellCount ; cell++)
							{
								XMLReference tagInsideCellXMLref=cellElement->GetNthChild(cell);
								//IIDXMLElement * tagInsidecellElement=tagInsideCellXMLref.Instantiate();
								InterfacePtr<IIDXMLElement>tagInsidecellElement(tagInsideCellXMLref.Instantiate());
								if(tagInsidecellElement==nil)
								{
									continue;
								}
								PMString tagInsidecellName=tagInsidecellElement->GetTagString();
								//CA("TGRTagReader::getTextFrameTagsForRefresh : else : tagInsidecellName = " + tagInsidecellName);
									
								TagStruct tInfo;
								int32 attribCount=tagInsidecellElement->GetAttributeCount();
								TextIndex sIndex=0, eIndex=0;
								Utils<IXMLUtils>()->GetElementIndices(tagInsidecellElement, &sIndex, &eIndex);
								tInfo.startIndex=sIndex;
								tInfo.endIndex=eIndex;
								tagInsidecellElement->AddRef();//Added on 17-09-08
								tInfo.tagPtr=tagInsidecellElement;
							
								//int32 indexOfCurrentTag = -1;
								for(int j=0; j<attribCount; j++)
								{
									PMString attribName=tagInsidecellElement->GetAttributeNameAt(j);
									PMString attribVal=tagInsidecellElement->GetAttributeValue(WideString(attribName));//CS4
									getCorrespondingTagAttributesForUpdate(attribName, attribVal, tInfo /*, indexOfCurrentTag*/);
								}
								tList.push_back(tInfo);
							}
						}
						else
						{
							TagStruct tInfo;
							int32 attribCount=cellElement->GetAttributeCount();
							TextIndex sIndex=0, eIndex=0;
							Utils<IXMLUtils>()->GetElementIndices(cellElement, &sIndex, &eIndex);
							tInfo.startIndex=sIndex;
							tInfo.endIndex=eIndex;
							cellElement->AddRef();//Added on 17-09-08
							tInfo.tagPtr=cellElement;
														
							for(int j=0; j<attribCount; j++)
							{
								PMString attribName=cellElement->GetAttributeNameAt(j);
								PMString attribVal=cellElement->GetAttributeValue(WideString(attribName));//Cs4
								getCorrespondingTagAttributesForUpdate(attribName, attribVal, tInfo /*,indexOfCurrentTag*/);
							}
							tList.push_back(tInfo);	
						}

					}
					continue;
				}
		
				TagStruct tInfo;
				int32 attribCount=childElement->GetAttributeCount();
				TextIndex sIndex=0, eIndex=0;
				Utils<IXMLUtils>()->GetElementIndices(childElement, &sIndex, &eIndex);
				tInfo.startIndex=sIndex;
				tInfo.endIndex=eIndex;
				childElement->AddRef();//Added on 17-09-08
				tInfo.tagPtr=childElement;
				
				//int32 indexOfCurrentTag = -1;
				for(int j=0; j<attribCount; j++)
				{
					PMString attribName=childElement->GetAttributeNameAt(j);
					PMString attribVal=childElement->GetAttributeValue(WideString(attribName));//Cs4
					getCorrespondingTagAttributesForUpdate(attribName, attribVal, tInfo /*,indexOfCurrentTag*/);
				}
				tList.push_back(tInfo);				
			}
		}

	}while(0);
}

void TGRTagReader::getGraphicFrameTagsForUpdate(void)
{
	do{		
		//CA("Inside TGRTagReader::getGraphicFrameTagsForUpdate");
		InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);	
		//InterfacePtr<IXMLReferenceData> objXMLRefDat(Utils<IXMLUtils>()->QueryXMLReferenceData(unknown));
		InterfacePtr<IXMLReferenceData> objXMLRefDat(Utils<IXMLUtils>()->QueryXMLReferenceData(boxUIDRef));
		if (objXMLRefDat == nil)
		{
			//CA("objXMLRefDat == nil");
			break;
		}

		XMLReference xmlRef = objXMLRefDat->GetReference();
		UIDRef refUID = xmlRef.GetUIDRef();
		//IIDXMLElement *xmlElement = xmlRef.Instantiate();
		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
		if(xmlElement == nil)
		{
			//CA("xmlElement == nil");
			break;
		}

		//xmlPtr=xmlElement;

		PMString tagName = xmlElement->GetTagString();

		int elementCount=xmlElement->GetChildCount();
		PMString tableTypeStr = xmlElement->GetAttributeValue(WideString("tableType"));
		//CA("tableTypeStr	:	"+tableTypeStr);
		//if(tagName == "Standard" && elementCount != 0)//version 7 change
		if(tableTypeStr == "1" && elementCount != 0)
		{
			int elementCount=xmlElement->GetChildCount();

			for(int i=0; i<elementCount; i++)
			{
           		XMLReference elementXMLref=xmlElement->GetNthChild(i);
				//IIDXMLElement * childElement=elementXMLref.Instantiate();
				InterfacePtr<IIDXMLElement>childElement(elementXMLref.Instantiate());
				if(childElement==nil)
				{	               
					continue;
				}
				PMString tagName=childElement->GetTagString();
				//CA("TGRTagReader::getGraphicFrameTagsForUpdate :tagName = " + tagName);
				if(tagName == "PSTable")
				{

					int cellCount=childElement->GetChildCount(); 
					for(int cell = 0 ; cell< cellCount ; cell++)
					{

						XMLReference cellXMLref=childElement->GetNthChild(cell);
						//IIDXMLElement * cellElement=cellXMLref.Instantiate();
						InterfacePtr<IIDXMLElement>cellElement(cellXMLref.Instantiate());
						if(cellElement==nil)
						{
							continue;
						}
						PMString tagName=cellElement->GetTagString();
						//CA("TGRTagReader::getGraphicFrameTagsForUpdate : else : tagName = " + tagName);

					

						XMLReference tagInsideCellXMLref=cellElement->GetNthChild(0);
						//IIDXMLElement * tagInsidecellElement=tagInsideCellXMLref.Instantiate();
						InterfacePtr<IIDXMLElement>tagInsidecellElement(tagInsideCellXMLref.Instantiate());
						if(tagInsidecellElement==nil)
						{
							continue;
						}
						PMString tagInsidecellName=tagInsidecellElement->GetTagString();
						//CA("TGRTagReader::getGraphicFrameTagsForUpdate : else : tagInsidecellName = " + tagInsidecellName);

						TagStruct tInfo;
						int32 attribCount=tagInsidecellElement->GetAttributeCount();
						TextIndex sIndex=0, eIndex=0;
						Utils<IXMLUtils>()->GetElementIndices(tagInsidecellElement, &sIndex, &eIndex);
						tInfo.startIndex=sIndex;
						tInfo.endIndex=eIndex;
						tagInsidecellElement->AddRef();//Added on 17-09-08
						tInfo.tagPtr=tagInsidecellElement;
						
						//int32 indexOfCurrentTag = -1;
						for(int j=0; j<attribCount; j++)
						{
							PMString attribName=tagInsidecellElement->GetAttributeNameAt(j);
							PMString attribVal=tagInsidecellElement->GetAttributeValue(WideString(attribName));//CS4
							getCorrespondingTagAttributesForUpdate(attribName, attribVal, tInfo /*,indexOfCurrentTag*/);
						}
						tList.push_back(tInfo);	
					}
					continue;
				}
			}
			break;
		}


	
		TagStruct tInfo;
		xmlElement->AddRef();//Added on 17-09-08
		tInfo.tagPtr=xmlElement;

		int32 attribCount = xmlElement->GetAttributeCount();

		//int32 indexOfCurrentTag = -1;
		for(int j=0; j<attribCount; j++)
		{
			PMString attribName = xmlElement->GetAttributeNameAt(j);
			PMString attribVal  = xmlElement->GetAttributeValue(WideString(attribName));//Cs4
			getCorrespondingTagAttributesForUpdate(attribName, attribVal, tInfo/* , indexOfCurrentTag*/);
		}
		tInfo.endIndex=-1;
		tInfo.startIndex=-1;
		tInfo.curBoxUIDRef=boxUIDRef;
		tList.push_back(tInfo);
	}while(0);
}

TagList TGRTagReader::getTagsFromBoxForUpdate(UIDRef boxId, IIDXMLElement** xmlPtr)
{
	//CA("Inside  TGRTagReader::getTagsFromBoxForUpdate ");
	tList.clear();
	/*InterfacePtr<IPMUnknown> unknown(boxId, IID_IUNKNOWN);
	
	UID frameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
	UID frameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxId, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		frameUID = graphicFrameDataOne->GetTextContentUID();
	}
	
	boxUIDRef=boxId;
	textFrameUID=frameUID;
	if(frameUID == kInvalidUID)
	{
		//CA("caling getGraphicFrameTags");
		getGraphicFrameTagsForUpdate();		
	}
	else 
	{
		// CA("calling getTextFrameTags");
		getTextFrameTagsForUpdate();
	}
	for(int i=0; i<tList.size(); i++)
		tList[i].isProcessed=kFalse;
	
	//if(xmlPtr)
	//	*xmlPtr=this->xmlPtr;
	return tList;
}

bool16 TGRTagReader::getCorrespondingTagAttributesForUpdate
	(const PMString& attribName, const PMString& attribVal, TagStruct& tStruct /*,int32& indexOfCurrentTag*/)
{	//CA("TGRTagReader::getCorrespondingTagAttributesForUpdate");
	if(attribName.IsEqual("ID", kFalse))
	{
		tStruct.elementId=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}
	
	if(attribName.IsEqual("imgFlag", kFalse))
	{
		tStruct.imgFlag=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("index", kFalse))
	{
		tStruct.whichTab=attribVal.GetAsNumber();
//		indexOfCurrentTag = attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("typeId", kFalse))
	{
		tStruct.typeId=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("parentId", kFalse))
	{
		tStruct.parentId=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("sectionID", kFalse))
	{
		tStruct.sectionID=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("parentTypeID", kFalse))
	{
		tStruct.parentTypeID=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}
	if(attribName.IsEqual("LanguageID", kFalse))
	{
		tStruct.languageID=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("tableFlag", kFalse))
	{
		int32 isTable = attribVal.GetAsNumber();
		if(isTable == 1)
		{
			//CA("*****************************has table");
			tStruct.isTablePresent = kTrue;
		}
		else
		{
			tStruct.isTablePresent = kFalse;
		}

		tStruct.numValidFields++;
		 
		return kTrue;
	}

	if(attribName.IsEqual("isAutoResize", kFalse))
	{
		tStruct.isAutoResize=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("rowno", kFalse))
	{
		tStruct.rowno=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("colno", kFalse))
	{
		tStruct.colno=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("isEventField", kFalse))
	{
		int32 isEventField = attribVal.GetAsNumber();
		if(isEventField == 1)
		{
			tStruct.isEventField = kTrue;
		}
		else
		{
			tStruct.isEventField = kFalse;
		}

		tStruct.numValidFields++;
		 
		return kTrue;
	}

	if(attribName.IsEqual("pbObjectId", kFalse))
	{
		tStruct.pbObjectId=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("header", kFalse))
	{
		tStruct.header=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("childTag", kFalse))
	{
		tStruct.childTag=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("tableType", kFalse))
	{
		tStruct.tableType=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("tableId", kFalse))
	{
		tStruct.tableId=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("isSprayItemPerFrame", kFalse))
	{
		tStruct.isSprayItemPerFrame=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("catLevel", kFalse))
	{
		tStruct.catLevel=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("imageIndex", kFalse))
	{
		tStruct.imageIndex=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("deleteIfEmpty", kFalse))
	{
		tStruct.deleteIfEmpty=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("dataType", kFalse))
	{
		tStruct.dataType=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("field1", kFalse))
	{
		tStruct.field1=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("field2", kFalse))
	{
		tStruct.field2=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("field3", kFalse))
	{
		tStruct.field3=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("field4", kFalse))
	{
		tStruct.field4=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("field5", kFalse))
	{
		tStruct.field5=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("childId", kFalse))
	{
		tStruct.childId=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}
    if(attribName.IsEqual("groupKey", kFalse))
    {
        tStruct.groupKey=attribVal;
        tStruct.numValidFields++;
        return kTrue;
    }
	
	return kFalse;
}


//////////////////////// for custom tab text
TagList TGRTagReader::getTagsFromBox_ForRefresh_ByAttribute(UIDRef boxId, IIDXMLElement** xmlPtr)   // To be use only for Refresh Content Attribute Level Refresh
{
	//CA("TGRTagReader::getTagsFromBox_ForRefresh");
	tList.clear();
	InterfacePtr<IPMUnknown> unknown(boxId, IID_IUNKNOWN);
	
	UID frameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
	
	boxUIDRef=boxId;
	textFrameUID=frameUID;
	if(frameUID == kInvalidUID)
	{
		getGraphicFrameTags();		
	}
	else 
	{
		getTextFrameTagsForRefresh_ByAttribute();
	}
	for(int i=0; i<tList.size(); i++)
		tList[i].isProcessed=kFalse;
	
	//if(xmlPtr)
	//	*xmlPtr=this->xmlPtr;
	return tList;
}



void TGRTagReader::getTextFrameTagsForRefresh_ByAttribute(void) // To be use only for Refresh Content Attribute Level Refresh
{
	//CA("TGRTagReader::getTextFrameTagsForRefresh");
	do{	
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
			return;
	/*   CS3 Change
		InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		if (textFrame == nil)
		{
            //CA("textFrame == nil");		
			break;
		}*/
		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TagReader::TGTGRTagReader::getTextFrameTagsForRefresh:graphicFrameHierarchy == nil");
			return;
		}
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGTGRTagReader::getTextFrameTagsForRefresh:multiColumnItemHierarchy == nil");
			return;
		}
		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGTGRTagReader::getTextFrameTagsForRefresh:multiColumnItemTextFrame == nil");
			//CA("Its Not MultiColumn");
			return;
		}
		InterfacePtr<IHierarchy> frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGTGRTagReader::getTextFrameTagsForRefresh:frameItemHierarchy == nil");
			continue;
		}
		InterfacePtr<ITextFrameColumn> textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGTGRTagReader::getTextFrameTagsForRefresh:textFrame == nil");
			//CA("!!!ITextFrameColumn");
			continue;
		}
		InterfacePtr<ITextModel> objTxtMdl (textFrame->QueryTextModel());
		if (objTxtMdl == nil)
		{
           	break;
		}

		//IXMLReferenceData* objXMLRefDat=(IXMLReferenceData*) objTxtMdl->QueryInterface(IID_IXMLREFERENCEDATA);
		UIDRef txtMdlUIDRef =::GetUIDRef(objTxtMdl);
		if (txtMdlUIDRef.GetDataBase() == nil) 
			break;

		InterfacePtr<IXMLReferenceData> objXMLRefDat(txtMdlUIDRef, UseDefaultIID());
		if (objXMLRefDat==nil)
		{
			break;
		}

		XMLReference xmlRef=objXMLRefDat->GetReference();

		UIDRef refUID=xmlRef.GetUIDRef();
       	//IIDXMLElement *xmlElement=xmlRef.Instantiate();
		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
		if(xmlElement==nil)
		{
			break;
		}
       //	xmlPtr=xmlElement;

		int elementCount=xmlElement->GetChildCount();

		PMString tagName=xmlElement->GetTagString(); /// PRINTsource tag OR Item Table tag
		//CA("tagName = " + tagName);

		TagStruct tInfo;
		int32 attribCount=xmlElement->GetAttributeCount();
		TextIndex sIndex=0, eIndex=0;
		Utils<IXMLUtils>()->GetElementIndices(xmlElement, &sIndex, &eIndex);
		tInfo.startIndex=sIndex;
		tInfo.endIndex=eIndex;
		tInfo.tagPtr=xmlElement;

		for(int j=0; j<attribCount; j++)
		{
			PMString attribName=xmlElement->GetAttributeNameAt(j);
			PMString attribVal=xmlElement->GetAttributeValue(WideString(attribName));//CS4
			getCorrespondingTagAttributes(attribName, attribVal, tInfo);
		}
		//tList.push_back(tInfo);
		//if(tInfo.isTablePresent == 1 && tInfo.typeId != -3)
		//	this->getGraphicFrameTags();
		//else
		{
			//CA("TGRTagReader::getTextFrameTagsForRefresh :inside else ");
			for(int i=0; i<elementCount; i++) // Product Copy level tags OR PSTable, CustomTabbed text level tags
			{
           		XMLReference elementXMLref=xmlElement->GetNthChild(i);
				//IIDXMLElement * childElement=elementXMLref.Instantiate();
				InterfacePtr<IIDXMLElement>childElement(elementXMLref.Instantiate());
				if(childElement==nil)
				{
					continue;
				}
				else
				{
					int elementCount1=childElement->GetChildCount();
					if(elementCount1 == 0)// these are Product Copy Level tags
					{
						PMString tagName=childElement->GetTagString();
						//CA("tagName : " + tagName);		
						TagStruct tInfo;
						int32 attribCount=childElement->GetAttributeCount();
						TextIndex sIndex=0, eIndex=0;
						Utils<IXMLUtils>()->GetElementIndices(childElement, &sIndex, &eIndex);
						tInfo.startIndex=sIndex;
						tInfo.endIndex=eIndex;
						childElement->AddRef();//Added on 17-09-08
						tInfo.tagPtr=childElement;
						
						for(int j=0; j<attribCount; j++)
						{
							PMString attribName=childElement->GetAttributeNameAt(j);
							PMString attribVal=childElement->GetAttributeValue(WideString(attribName));//Cs4
							getCorrespondingTagAttributes(attribName, attribVal, tInfo);
						}
						tList.push_back(tInfo);	
					}
					else
					{  // We got more tags under 2nd level tags these can be custom tabbed text(Attribute taags) or table(PSCell tags) 
						for(int k=0; k<elementCount1; k++)
						{
							XMLReference elementXMLref1=childElement->GetNthChild(k);
							//IIDXMLElement * childElement2=elementXMLref1.Instantiate();
							InterfacePtr<IIDXMLElement>childElement2(elementXMLref1.Instantiate());
							if(childElement2==nil)
							{	               
								continue;
							}
							else
							{
								int elementCount2=childElement2->GetChildCount();
								if(elementCount2 == 0)
								{// Attributes of Custom Tabbed text tags
									PMString tagName=childElement2->GetTagString();
									//CA("tagName : " + tagName);		
									TagStruct tInfo;
									int32 attribCount=childElement2->GetAttributeCount();
									TextIndex sIndex=0, eIndex=0;
									Utils<IXMLUtils>()->GetElementIndices(childElement2, &sIndex, &eIndex);
									tInfo.startIndex=sIndex;
									tInfo.endIndex=eIndex;
									childElement2->AddRef();//Added on 17-09-08
									tInfo.tagPtr=childElement2;
									
									for(int j=0; j<attribCount; j++)
									{
										PMString attribName=childElement2->GetAttributeNameAt(j);
										PMString attribVal=childElement2->GetAttributeValue(WideString(attribName));//Cs4
										getCorrespondingTagAttributes(attribName, attribVal, tInfo);
									}
									tList.push_back(tInfo);	
								}
								else
								{	// this can be PSCell tag and we are going for Tabel Cell Attributes
									for(int m=0; m<elementCount2; m++)
									{
										XMLReference elementXMLref1=childElement2->GetNthChild(m);
										//IIDXMLElement * childElement3=elementXMLref1.Instantiate();
										InterfacePtr<IIDXMLElement>childElement3(elementXMLref1.Instantiate());
										if(childElement3==nil)
										{	               
											continue;
										}
										else
										{
											int elementCount3=childElement3->GetChildCount();
											if(elementCount3 == 0)
											{  // getting Table cell Attribute tags here 
												PMString tagName=childElement3->GetTagString();
												//CA("tagName : " + tagName);		
												TagStruct tInfo;
												int32 attribCount=childElement3->GetAttributeCount();
												TextIndex sIndex=0, eIndex=0;
												Utils<IXMLUtils>()->GetElementIndices(childElement3, &sIndex, &eIndex);
												tInfo.startIndex=sIndex;
												tInfo.endIndex=eIndex;
												childElement3->AddRef();//Added on 17-09-08
												tInfo.tagPtr=childElement3;
												
												for(int j=0; j<attribCount; j++)
												{
													PMString attribName=childElement3->GetAttributeNameAt(j);
													PMString attribVal=childElement3->GetAttributeValue(WideString(attribName));//Cs4
													getCorrespondingTagAttributes(attribName, attribVal, tInfo);
												}
												tList.push_back(tInfo);	
											}
										}

									}
								}

							}
						}
					}
				}
					
			}
		}

	}while(0);
}

////////////////////////////////////////////

TagList TGRTagReader::getAllChildTags(IIDXMLElement* xmlElement)
{
	//CA("Inside  TGRTagReader::getTagsFromBoxForUpdate ");
	tList.clear();
	do
	{
		if(xmlElement == NULL)
			return tList;

		int elementCount=xmlElement->GetChildCount();    		
			
		PMString tagName=xmlElement->GetTagString();
		//CA("tagName = " + tagName);
					
		for(int i=0; i<elementCount; i++)
		{
           	XMLReference elementXMLref=xmlElement->GetNthChild(i);
			//IIDXMLElement * childElement=elementXMLref.Instantiate();
			InterfacePtr<IIDXMLElement>childElement(elementXMLref.Instantiate());
			if(childElement==nil)
			{
                continue;
			}
			PMString tagName=childElement->GetTagString();
			//CA("TGRTagReader::getAllChildTags : else : tagName = " + tagName);
				
			TagStruct tInfo;
			int32 attribCount=childElement->GetAttributeCount();
			TextIndex sIndex=0, eIndex=0;
			Utils<IXMLUtils>()->GetElementIndices(childElement, &sIndex, &eIndex);
			tInfo.startIndex=sIndex;
			tInfo.endIndex=eIndex;
			childElement->AddRef();//Added on 17-09-08
			tInfo.tagPtr=childElement;
			
			//int32 indexOfCurrentTag = -1;
			for(int j=0; j<attribCount; j++)
			{
				PMString attribName=childElement->GetAttributeNameAt(j);
				PMString attribVal=childElement->GetAttributeValue(WideString(attribName));//Cs4
				getCorrespondingTagAttributesForUpdate(attribName, attribVal, tInfo /*,indexOfCurrentTag*/);
			}
			tList.push_back(tInfo);				
		}		

	}while(0);
	
	for(int i=0; i<tList.size(); i++)
		tList[i].isProcessed=kFalse;	
	
	return tList;
}

void TGRTagReader::insertStencilNameInTag(UIDRef BoxUIDRef,PMString stencilName)
{
	//CA("inside insertStencilNameInTag");

	UID frameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(BoxUIDRef, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		frameUID = graphicFrameDataOne->GetTextContentUID();
	}
		
	if(frameUID == kInvalidUID)
	{
		//CA("frameUID == kInvalidUID");
		InterfacePtr<IPMUnknown> unknown(BoxUIDRef, IID_IUNKNOWN);	
		
		InterfacePtr<IXMLReferenceData> objXMLRefDat(Utils<IXMLUtils>()->QueryXMLReferenceData(BoxUIDRef));
		if (objXMLRefDat == nil)
		{
			//CA("objXMLRefDat == nil");
			return;
		}

		XMLReference xmlRef = objXMLRefDat->GetReference();
		UIDRef refUID = xmlRef.GetUIDRef();
		//IIDXMLElement *xmlElement = xmlRef.Instantiate();
		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
		if(xmlElement == nil)
		{
			//CA("xmlElement == nil");
			return;
		}
		
		PMString tagAttribute("rowno");
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlRef, WideString(tagAttribute),WideString(stencilName));	 //CS4			
	}
	else 
	{
		//CA("frameUID != kInvalidUID");
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
			return;

		/*InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		if (textFrame == nil){
			//CA("textFrame == nil");
				break;
		}*/

		UID result = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(BoxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			
			result = graphicFrameDataOne->GetTextContentUID();
			if(result ==kInvalidUID )
			{
				ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::insertStencilNameInTag:Not a textFrame");
			}
		}
			
		InterfacePtr<IHierarchy> graphicFrameHierarchy(BoxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::insertStencilNameInTag:graphicFrameHierarchy == nil");
			return;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::insertStencilNameInTag:multiColumnItemHierarchy == nil");
			return;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::insertStencilNameInTag:multiColumnItemTextFrame == nil");
			//CA("Its Not MultiColumn in Tag Reader");
			return;
		}

		InterfacePtr<IHierarchy> frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::insertStencilNameInTag:frameItemHierarchy == nil");
			return;
		}

		InterfacePtr<ITextFrameColumn> textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			//CA("!!!ITextFrameColumn in Tag Reader");
			ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::insertStencilNameInTag:textFrame == nil");
			return;
		}
					
		InterfacePtr<ITextModel> objTxtMdl (textFrame->QueryTextModel());
		if (objTxtMdl == nil){ CA("objTxtMdl == nil");  
			return;
		}

		//IXMLReferenceData* objXMLRefDat=(IXMLReferenceData*) objTxtMdl->QueryInterface(IID_IXMLREFERENCEDATA);
		UIDRef txtMdlUIDRef =::GetUIDRef(objTxtMdl);
		if (txtMdlUIDRef.GetDataBase() == nil) 
			return;

		InterfacePtr<IXMLReferenceData> objXMLRefDat(txtMdlUIDRef, UseDefaultIID());
		if (objXMLRefDat==nil){
			//CA("objXMLRefDat==nil ");
			return;
		}

		XMLReference xmlRef=objXMLRefDat->GetReference();

		UIDRef refUID=xmlRef.GetUIDRef();		

		//IIDXMLElement *xmlElement=xmlRef.Instantiate();
		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
		if(xmlElement==nil){
			CA("xmlElement==nil");
			return;
		}		

		PMString tagAttribute("rowno");
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlRef, WideString(tagAttribute),WideString(stencilName));//Cs4

		int elementCount=xmlElement->GetChildCount();    		
			
		if(elementCount > 0)
		{
           	XMLReference elementXMLref=xmlElement->GetNthChild(0);
			//IIDXMLElement * childElement=elementXMLref.Instantiate();
			InterfacePtr<IIDXMLElement>childElement(elementXMLref.Instantiate());
			if(childElement==nil)
			{
                return;
			}
			PMString tagAttribute("index");
			PMString index = childElement->GetAttributeValue(WideString(tagAttribute));//Cs4

			Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlRef, WideString(tagAttribute),WideString(index)); //Cs4
		}


	}
}

PMString TGRTagReader::getStencilNameAndIndex(UIDRef BoxUIDRef,PMString& index)
{
	//CA("TGRTagReader::getStencilNameAndIndex");
	PMString stencilName("");
	UID frameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(BoxUIDRef, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		frameUID = graphicFrameDataOne->GetTextContentUID();
	}
		
	if(frameUID == kInvalidUID)
	{
		//CA("frameUID == kInvalidUID");
		InterfacePtr<IPMUnknown> unknown(BoxUIDRef, IID_IUNKNOWN);	
		
		InterfacePtr<IXMLReferenceData> objXMLRefDat(Utils<IXMLUtils>()->QueryXMLReferenceData(BoxUIDRef));
		if (objXMLRefDat == nil)
		{
			//CA("objXMLRefDat == nil");
			return stencilName;
		}

		XMLReference xmlRef = objXMLRefDat->GetReference();
		UIDRef refUID = xmlRef.GetUIDRef();
		//IIDXMLElement *xmlElement = xmlRef.Instantiate();
		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
		if(xmlElement == nil)
		{
			//CA("xmlElement == nil");
			return stencilName;
		}
		
		PMString tagAttribute("rowno");
		stencilName = xmlElement->GetAttributeValue(WideString(tagAttribute));	//Cs4

		tagAttribute.clear();
		tagAttribute.Append("index");
		index = xmlElement->GetAttributeValue(WideString(tagAttribute)); //Cs4
	}
	else 
	{
		//CA("frameUID != kInvalidUID");
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
			return stencilName;

		/*InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		if (textFrame == nil){
			//CA("textFrame == nil");
				break;
		}*/

		UID result = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(BoxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			
			result = graphicFrameDataOne->GetTextContentUID();
			if(result ==kInvalidUID )
			{
				ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::insertStencilNameInTag:Not a textFrame");
			}
		}
			
		InterfacePtr<IHierarchy> graphicFrameHierarchy(BoxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::insertStencilNameInTag:graphicFrameHierarchy == nil");
			return stencilName;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::insertStencilNameInTag:multiColumnItemHierarchy == nil");
			return stencilName;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::insertStencilNameInTag:multiColumnItemTextFrame == nil");
			//CA("Its Not MultiColumn in Tag Reader");
			return stencilName;
		}

		InterfacePtr<IHierarchy> frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::insertStencilNameInTag:frameItemHierarchy == nil");
			return stencilName;
		}

		InterfacePtr<ITextFrameColumn> textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			//CA("!!!ITextFrameColumn in Tag Reader");
			ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::insertStencilNameInTag:textFrame == nil");
			return stencilName;
		}
					
		InterfacePtr<ITextModel> objTxtMdl (textFrame->QueryTextModel());
		if (objTxtMdl == nil){ //CA("objTxtMdl == nil");  
			return stencilName;
		}

		//IXMLReferenceData* objXMLRefDat=(IXMLReferenceData*) objTxtMdl->QueryInterface(IID_IXMLREFERENCEDATA);
		UIDRef txtMdlUIDRef =::GetUIDRef(objTxtMdl);
		if (txtMdlUIDRef.GetDataBase() == nil) 
			return stencilName;

		InterfacePtr<IXMLReferenceData> objXMLRefDat(txtMdlUIDRef, UseDefaultIID());
		if (objXMLRefDat==nil){
			//CA("objXMLRefDat==nil ");
			return stencilName;
		}

		XMLReference xmlRef=objXMLRefDat->GetReference();

		UIDRef refUID=xmlRef.GetUIDRef();		

		//IIDXMLElement *xmlElement=xmlRef.Instantiate();
		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
		if(xmlElement==nil){
			//CA("xmlElement==nil");
			return stencilName;
		}		

		PMString tagAttribute("rowno");
		stencilName = xmlElement->GetAttributeValue(WideString(tagAttribute));//Cs4

		tagAttribute.clear();
		tagAttribute.Append("index");
		index = xmlElement->GetAttributeValue(WideString(tagAttribute)); //Cs4
	}
	return stencilName;
}


void TGRTagReader::insertStencilNameInTag_PrintExpress(UIDRef BoxUIDRef,PMString stencilName,int32 templateId)
{
	//CA("inside insertStencilNameInTag");

	UID frameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(BoxUIDRef, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		frameUID = graphicFrameDataOne->GetTextContentUID();
	}
		
	if(frameUID == kInvalidUID)
	{
		//CA("frameUID == kInvalidUID");
		InterfacePtr<IPMUnknown> unknown(BoxUIDRef, IID_IUNKNOWN);	
		
		InterfacePtr<IXMLReferenceData> objXMLRefDat(Utils<IXMLUtils>()->QueryXMLReferenceData(BoxUIDRef));
		if (objXMLRefDat == nil)
		{
			//CA("objXMLRefDat == nil");
			return;
		}

		XMLReference xmlRef = objXMLRefDat->GetReference();
		UIDRef refUID = xmlRef.GetUIDRef();
		//IIDXMLElement *xmlElement = xmlRef.Instantiate();
		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
		if(xmlElement == nil)
		{
			//CA("xmlElement == nil");
			return;
		}
		
		PMString tagAttribute("rowno");
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlRef, WideString(tagAttribute),WideString(stencilName));	 //CS4			
	

		PMString templateID("");
		templateID.AppendNumber(PMReal(templateId));
		PMString tagAttribute1("colno");
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlRef, WideString(tagAttribute1),WideString(templateID));	 //CS4			
	
		
	}
	else 
	{
		//CA("frameUID != kInvalidUID");
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
			return;

		/*InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		if (textFrame == nil){
			//CA("textFrame == nil");
				break;
		}*/

		UID result = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(BoxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			
			result = graphicFrameDataOne->GetTextContentUID();
			if(result ==kInvalidUID )
			{
				ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::insertStencilNameInTag:Not a textFrame");
			}
		}
			
		InterfacePtr<IHierarchy> graphicFrameHierarchy(BoxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::insertStencilNameInTag:graphicFrameHierarchy == nil");
			return;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::insertStencilNameInTag:multiColumnItemHierarchy == nil");
			return;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::insertStencilNameInTag:multiColumnItemTextFrame == nil");
			//CA("Its Not MultiColumn in Tag Reader");
			return;
		}

		InterfacePtr<IHierarchy> frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::insertStencilNameInTag:frameItemHierarchy == nil");
			return;
		}

		InterfacePtr<ITextFrameColumn> textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			//CA("!!!ITextFrameColumn in Tag Reader");
			ptrIAppFramework->LogDebug("AP7_TagReader::TGRTagReader::insertStencilNameInTag:textFrame == nil");
			return;
		}
					
		InterfacePtr<ITextModel> objTxtMdl (textFrame->QueryTextModel());
		if (objTxtMdl == nil){ CA("objTxtMdl == nil");  
			return;
		}

		//IXMLReferenceData* objXMLRefDat=(IXMLReferenceData*) objTxtMdl->QueryInterface(IID_IXMLREFERENCEDATA);
		UIDRef txtMdlUIDRef =::GetUIDRef(objTxtMdl);
		if (txtMdlUIDRef.GetDataBase() == nil) 
			return;

		InterfacePtr<IXMLReferenceData> objXMLRefDat(txtMdlUIDRef, UseDefaultIID());
		if (objXMLRefDat==nil){
			//CA("objXMLRefDat==nil ");
			return;
		}

		XMLReference xmlRef=objXMLRefDat->GetReference();

		UIDRef refUID=xmlRef.GetUIDRef();		

		//IIDXMLElement *xmlElement=xmlRef.Instantiate();
		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
		if(xmlElement==nil){
			CA("xmlElement==nil");
			return;
		}		

		PMString tagAttribute("rowno");
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlRef, WideString(tagAttribute),WideString(stencilName));//Cs4

		PMString templateID("");
		templateID.AppendNumber(PMReal(templateId));
		PMString tagAttribute1("colno");
		Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlRef, WideString(tagAttribute1),WideString(templateID));	 //CS4			
	


		int elementCount=xmlElement->GetChildCount();    		
			
		if(elementCount > 0)
		{
           	XMLReference elementXMLref=xmlElement->GetNthChild(0);
			//IIDXMLElement * childElement=elementXMLref.Instantiate();
			InterfacePtr<IIDXMLElement>childElement(elementXMLref.Instantiate());
			if(childElement==nil)
			{
                return;
			}
			PMString tagAttribute("index");
			PMString index = childElement->GetAttributeValue(WideString(tagAttribute));//Cs4

			Utils<IXMLAttributeCommands>()->SetAttributeValue(xmlRef, WideString(tagAttribute),WideString(index)); //Cs4
		}


	}
}
