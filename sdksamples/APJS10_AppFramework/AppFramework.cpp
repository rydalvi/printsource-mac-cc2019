#include "VCPlugInHeaders.h"
#include "HelperInterface.h"
#include "CAlert.h"
#include "PMString.h"
//#include "IJNIFramework.h"
//#include "IAppFramework.h"
#include "FileUtils.h"
#include "MetaDataTypes.h"
#include "iostream"
#include "fstream"
#include "IAppFramework.h"


#include "libjson.h"
#include "JSONNode.h"
#include "JSONOptions.h"
#include "curl.h"
#include <stdio.h>
#include "jsonParser.h"
#include "structureCache.h"
#include "EventCache.h"
#include "ConfigCache.h"
#include "ItemTableValue.h"
#include "string.h"
#include "LoginInfoValue.h"
#include "StringUtils.h"
#include "PartnerCache.h"
#include "IndexReference.h"
#include "SDKUtilities.h"
#include <math.h>


//#include "map"
#define UNIXDELCHAR '/'
#define MACDELCHAR ':'
#define WINDELCHAR '\\'
#define UNIXDELIMETER PMString("/")
#define WINDELIMETER PMString("\\")
#define MACDELIMETER PMString(":")
#define UNIX_WINPARENTDIR PMString("..")
#define MACPARENTDIR PMString("::")

#define CA(x) CAlert::InformationAlert(x)
#define CA_NUM(a,b) {PMString str(a);str.Append(" : ");str.AppendNumber(b);CA(str);}
#define CAI(x) {PMString str;\
			str.AppendNumber(x);\
			CAlert::InformationAlert(str);}\

using namespace std;
//using namespace metadata;

class AppFramework: public IAppFramework
{
	private:
		static bool16 IndesignLoginStatus;
		static PMString SelectedServerURL;
		static PMString jsessionID;
		int INTERFACE_LOG  ;
		int FUNCTION_LOG ;
		static bool16 FileStatus ;
		static PMString filePath;
		static int32 LoggerFileFlag;//0:- error//1:- fatal,error//2:-debug,fatal,error//3:-info,debug,fatal,error
		static bool16 isONEsourceMode;
		static double LocaleID;
		static double CatalogID;
		static PMString assetServerPath;
		static LoginInfoValue currentServerConfigValue;
		static double clientID;
		static int32 AssetServerOption; //1 = Asset Server is same as Image Working directory. // 0 = asset server is diff from IWD Need for File download
		static bool16 MissingFlag;
		static PMString selectedEnvName;
		static PMString currentLoginUserEmail; // Used for relogin in case of Session Timeout
		static PMString currentLoginPassword;  // Used for relogin in case of Session Timeout
		static double SelectedCustPVID; // Used to save customer info for True Mfg.
		static double SelectedDivisionPVID; // Used to save Division info for True Mfg.
        static PMString apiKey;
        static PMString pimVersion;
    

	public:

		AppFramework(IPMUnknown* boss);
		virtual ~AppFramework()
		{
			//loginStatus=FALSE;
		}
		      
		DECLARE_HELPER_METHODS()
		
		bool16 callService(PMString url, PMString &returndata); 
	    bool16 callLogin(PMString ServerURL, PMString username, PMString Password, /*PMString apikey,*/ PMString clientNo, PMString &sessionId);
		bool16 getLoginStatus();
		bool16 callLogout(PMString &logoutResult);
		bool16 callStructureJson( StructureCacheValue &returndata);
		bool16 callAllTreeJson( EventCacheValue &returndata);
		bool16 callConfigJson(vector<ConfigValue> &returndata);
		bool16 callSectionResultJson(double sectionId, double langId, vector<CPbObjectValue> &returndata, PMString itemAttributeId="");
		bool16 callSectionDetailResultJson(double sectionId, double langId, vector<CPbObjectValue> &returndata , PMString itemGroupIds, PMString itemIds, PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds, PMString listItemFieldIds,  bool16 isSprayItemPerFrameFlag, PMString itemAttributeGroupIds = "");
		bool16 callItemGroupDetailJson(double itemGroupId, double sectionId, double langId, CObjectValue &returndata  , PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds, PMString listItemFieldIds);
		bool16 callItemDetailJson( double itemId, double sectionId, double langId, CItemModel &returndata , PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds, PMString listItemFieldIds);
		bool16 callPartnerJson(PartnerCacheValue * partnerCacheValuePtr);

		void LogDebug(PMString errmsg);
		void LogInfo(PMString errmsg);
		void LogError(PMString errmsg);
		void LogFatal(PMString errmsg);			
		void Log(TypeOfError etype, PMString errorString); //Cs4
		bool16 GetFileStatus();
		void SetFileStatus(bool16 status);
		PMString GetFilePath();
		void SetFilePath();
		PMString GetTimeStamp();
		void getUnixPath(PMString& thisstring);
		bool IsWinPath(PMString thisstring);
		void setLoggerFileFlag(int32 setflag);

		VectorClassInfoPtr ClassificationTree_getRoot(double LanguageID);
		VectorClassInfoPtr ClassificationTree_getAllChildren(double ClassID, double LanguageID);
		VectorPubModelPtr ProjectCache_getAllProjects(double);
		VectorPubModelPtr ProjectCache_getAllChildren(double PubId, double LanguageID);

		bool16 SetURL();
		bool16 get_isONEsourceMode();
	    void set_isONEsourceMode(bool16);

		VectorElementInfoPtr StructureCache_getItemGroupCopyAttributesByLanguageId(double languageId);
		VectorTypeInfoPtr StructureCache_getItemGroupImageAttributes();
		bool16 StructureCache_isElementMPV(double elementID);
		VectorTypeInfoPtr StructureCache_getListTableTypes();
		VectorAttributeInfoPtr StructureCache_getItemAttributesForClassAndParents(double ClassID , double LanguageID);
		VectorTypeInfoPtr StructureCache_getItemImagesForClassAndParents(double ClassID);
		VectorElementInfoPtr StructureCache_getSectionCopyAttributesByLanguageId(double languageId);
		VectorTypeInfoPtr StructureCache_getSectionImageAttributes();
		VectorLanguageModelPtr StructureCache_getAllLanguages();
		void setCatalogId(double catalog_id);
	    void setLocaleId(double locale_id);
		double getCatalogId();
		double getLocaleId();
		void StructureCache_clearInstance();
	    void EventCache_clearInstance();

		bool16 CONFIGCACHE_getPVImageFileName(int32 index);
		bool16 ConfigCache_getDisplayItemComponents();
		bool16 ConfigCache_getDisplayItemXref();
		bool16 ConfigCache_getDisplayItemAccessory();
		int32 getPM_Project_Levels();
		
		CPubModel getpubModelByPubID(double PubID,double languageID);
		CClassificationValue StructureCache_getCClassificationValueByClassId(double classId, double LanguageID);
		VectorPbObjectValuePtr getProductsAndItemsForSection(double curSecId, double langId , PMString itemAttributeId ="" ,bool16 isChildDataRequire = kFalse);
		VectorPbObjectValuePtr getProductsAndItemsForSectionInDetail(double curSecId, double langId , PMString itemGroupIds, PMString itemIds , PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds, PMString listItemFieldIds,  bool16 isSprayItemPerFrameFlag, PMString itemAttributeGroupIds = "");

		double ConfigCache_getintConfigValue1ByConfigName(PMString configName);
		PMString ConfigCache_getPMStringConfigValue1ByConfigName(PMString configName);

		VectorPubObjectValuePtr getItemsForSubSection(double subSecID, double LanguageID);
		VectorPubObjectValuePtr getProductsForSubSection(double subSecID, double LanguageID);

		bool16 EventCache_setCurrentSectionData( double sectionId, double langId , PMString itemGroupIds, PMString itemIds , PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds, PMString listItemFieldIds, bool16 isCategorySpecificResultCall = kFalse,  bool16 isSprayItemPerFrameFlag = kFalse, PMString itemAttributeGroupIds = "");
		void EventCache_clearCurrentSectionData();

		void EventCache_setCurrentObjectData(double sectionId, double ObjectId, int32 isProduct, double langId, PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds, PMString listItemFieldIds);
		void EventCache_clearCurrentObjectData();

		VectorScreenTableInfoPtr GETProjectProduct_getAllScreenTablesBySectionidObjectid(double section_id, double object_id, double langId, bool16 isCustomTable = kFalse);
		void clearAllStaticObjects();

		PMString GETItem_GetItemAttributeDetailsByLanguageId(double ItemID,double AttributeID,double LanguageID, double sectionId, bool16 RefreshFlag);
		CObjectValue GETProduct_getObjectElementValue(double objectID, double sectionId, double LanguageId =1);
		PMString GETProduct_getAttributeValueByLanguageID(double ObjectID,double AttributeID,double LanguageID, double sectionId, bool16 IsFlag);

		VectorScreenTableInfoPtr GETProjectProduct_getItemTablesByPubObjectId(double itemId, double sectionId, double langId);
		double ConfigCache_getPubItemSalePriceTableType();
		double ConfigCache_getPubItemRegularPriceTableType();

		PMString ATTRIBUTECache_getItemAttributeName(double AttributeID,double languageID);
		bool16 ElementCache_GetElementModelByElementID(double elementID,CElementModel & cElementModelObj, double languageId = 91);

		bool16 ConfigCache_getSprayTableStructurePerItem();
		VectorLongIntPtr GETProjectProduct_getAllItemIDsFromTables(double ObjectID, double sectionId);

		PMString PUBModel_getAttributeValueByLanguageID(double PubID,double AttributeID,double LanguageID,bool16 Isflag);

		VectorAssetValuePtr GETAssets_GetAssetByParentAndType(double objectId, double sectionId, int32 isProduct, double typeId = -1); 

		CPbObjectValue getPbObjectValueBySectionIdObjectId(double sectionId, double ObjectId, int32 isProduct, double langId);
		PMString StructureCache_getListNameByTypeId(double typeId);
		PMString StructureCache_TYPECACHE_getTypeNameById(double typeId);

		double CONFIGCACHE_getElementIDForItemGroupDescription();
		double CONFIGCACHE_getItemDescriptionAttribute();

		VectorLoginInfoPtr getLoginInfoProperties();

		PMString getAssetServerPath();
		void setAssetServerPath(PMString str);

		void clearConfigCache();

		VectorAssetValuePtr GETAssets_GetPVMPVAssetByParentIdAndAttributeId(double ParentId,double AttributeId, double sectionId, double LangaugeId, int32 objectType, double pvGroupId, int32 PVImageindex);

		PMString getPrintSourcePropertiesPath();
		bool16 LOGINMngr_updateOrCreateNewServerProperties(LoginInfoValue oServerInfo, int updateflag, double clientID);
		bool16 LOGINMngr_deleteUser(char* envName, double ClientId);
		void LOGINMngr_setServerInfoValue(LoginInfoValue ServerConfigValue);

		bool16 callLoginFirstStep(PMString serverURL, PMString username, PMString password, vector<CClientModel> * vectClientModels , PMString & errorMsg);
		bool16 LOGINMngr_logoutCurrentUser();

		void setClientID(double clientid);
		double getClientID();

		int32 getAssetServerOption();
		void setAssetServerOption(int32 Option);

		void setMissingFlag(bool16 status);
		bool16 getMissingFlag();

		void setSelectedEnvName(PMString name);
		PMString getSelectedEnvName();

		CAssetValue getPartnerAssetValueByPartnerIdImageTypeId(double objectId, double sectionId, double languageId, int32 imageTypeId, int32 whichTab);

		bool16 getIndexTerms(set<PMString> resultIds, vector<IndexReference> &returndata , PMString primaryIndexs, PMString secondaryIndexes, PMString tertioryIndexes);
		bool16 callIndexTermJson(set<PMString> resultIds, vector<IndexReference> &returndata , PMString primaryIndexs, PMString secondaryIndexes, PMString tertioryIndexes);
		
		Attribute ATTRIBUTECache_getItemAttributeForIndex(int32 option, double languageID);

		bool16 callCategorySpecificResultJson( double langId, vector<CPbObjectValue> &returndata , PMString itemGroupIds, PMString itemIds , PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds);

		VectorPbObjectValuePtr getProductsAndItemsForCategorySpecificResult( double langId , PMString itemGroupIds, PMString itemIds, PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds );

		bool16 getFieldSwapValues(set<PMString> resultIds, vector<IndexReference>& fieldSwapValues, WideString newLanguageId, PMString resultType, double oldFieldId, double newFieldId, double eventId);
		bool16 callFieldSwapValuesJson(set<PMString> resultIds, vector<IndexReference>& fieldSwapValues, WideString newLanguageId, PMString resultType, double oldFieldId, double newFieldId, double eventId);
		bool16 getRefreshData(set<PMString> resultIds, vector<IndexReference>& fieldSwapValues, WideString newLanguageId, PMString itemFieldIds, PMString itemGroupFieldIds, PMString sectionFieldIdsPMString, PMString itemGroupListIds, PMString itemListIds);
		bool16 callRefreshDataJson(set<PMString> resultIds, vector<IndexReference>& fieldSwapValues, WideString newLanguageId, PMString itemFieldIds, PMString itemGroupFieldIds, PMString sectionFieldIds, PMString itemGroupListIds, PMString itemListIds);

		VectorAttributeInfoPtr StructureCache_getAllItemAttributes(double LanguageID);
		PicklistGroup StructureCache_getPickListGroups(double pickListGroupId);

		double getSectionIdByLevelAndEventId(int32 sectionLevel, double sectionId, double LangId);

		double  getDollerOff(double regularPrice, double eventPrice);
		double  getPercentageOff(double regularPrice, double eventPrice);
		bool16 StructureCache_isAttributeMPV(double attributeId);

		bool16 callReverseUpdateJson(int32 index, double objectId, double fieldId, double langId, double sectionId, PMString value);

		double StructureCache_getPickListGroupIdByElementMPVId(double elementID);
		double StructureCache_getPickListGroupIdByAttributeMPVId(double attributeId);
		Attribute StructureCache_getItemAttributeModelByAttributeId(double AttributeId);

		VectorAdvanceTableScreenValuePtr getHybridTableData(double section_id , double object_id , double language_id, int32 isProduct, bool16 isSectionLevelHybridTable);

		bool16 CONFIGCACHE_getShowCustomerInIndesign();
		double CONFIGCACHE_getCustomerPVTypeId(int32 isCustomer);
		PMString CONFIGCACHE_getCustomerAssetTypes(int32 isCustomer);
		double getSelectedCustPVID(int32 isCustomer);
		void setSelectedCustPVID(double pvID,int32 isCustomer);
    
        // Filter sprayer
        bool16 apiCallService(PMString url, PMString &returndata);
    
        
        bool16 callFilterService(vector<FilterValue> &returndata);
        bool16 callFilterResultService(PMString filterId, int32 limit, int32 offset, vector<CItemModel> &returndata);
        bool16 callFilterResultCountService(PMString filterId, int32 &returndata);
        
    
        bool16 callGetPricingModule(vector<PricingModule> &returndata);
        bool16 callGetWorkflowTasks(vector<WorkflowTask> &returndata);
    
        bool16 callGetAttributeGroups(  vector<AttributeGroup> &returndata);
        VectorAttributeGroupPtr StructureCache_getItemAttributeGroups();
        vector<double> StructureCache_getItemAttributeListforAttributeGroupId(double attributeGroupId, bool16 isRefreshCall= kFalse);
        vector<double> StructureCache_getItemAttributeListforAttributeGroupKey(PMString &attributeGroupKey, bool16 isRefreshCall= kFalse);
        bool16 callGetAttributesByAttributeGroupKeyOrId( PMString keyOrId, bool16 isKey, AttributeGroup &returndata);
        bool16 callGetAttributesByItemIdAndAttributeGroupKeyOrId( PMString keyOrId, bool16 isKey, double itemId,vector<double> &returndata);
        vector<double> getItemAttributeListforAttributeGroupIdAndItemId(double sectionId, double itemId, double attributeGroupId, double languageId);
        vector<double> StructureCache_getItemAttributeListforAttributeGroupKeyAndItemId(PMString &attributeGroupKey, double itemId);
    
        VectorAssetValuePtr GETAssets_GetAssetByParentAndAssetId(double objectId, double sectionId, int32 isProduct, double assetId);
    
        PMString StructureCache_getItemAttributeGroupNameByAttributeGroupId(double attributeGroupId);
        bool16 callGetAttributesV3( vector<Attribute> &itemFields);
    
        bool16 callSectionResultJsonWithChildItemDetails(double sectionId, double langId, vector<CPbObjectValue> &returndata, PMString itemAttributeId);
    

};

bool16 AppFramework::IndesignLoginStatus = kFalse;
PMString AppFramework::SelectedServerURL ="";
PMString AppFramework::jsessionID= "";
int32 AppFramework::LoggerFileFlag = 0;
bool16 AppFramework::isONEsourceMode = kFalse;
double AppFramework::CatalogID = -1;
double AppFramework::LocaleID = -1;
bool16 AppFramework::FileStatus = kFalse;
PMString AppFramework::filePath = "";
PMString AppFramework::assetServerPath ="";
LoginInfoValue AppFramework::currentServerConfigValue;
double AppFramework::clientID = -1;
int32 AppFramework::AssetServerOption = 1;
bool16 AppFramework::MissingFlag = kFalse;
PMString AppFramework::selectedEnvName = "";
PMString AppFramework::currentLoginUserEmail = ""; // Used for relogin in case of Session Timeout
PMString AppFramework::currentLoginPassword = "";
double AppFramework::SelectedCustPVID = -1;
double AppFramework::SelectedDivisionPVID = -1;
PMString AppFramework::apiKey= "";
PMString AppFramework::pimVersion = "";

DEFINE_HELPER_METHODS(AppFramework) //these help to override QueryInterface(),AddRef() etc.

CREATE_PMINTERFACE(AppFramework, kAppFrameworkImpl)

AppFramework::AppFramework(IPMUnknown* boss) : HELPER_METHODS_INIT(boss)
{

}

bool16 AppFramework::SetURL()
{
			return kFalse;
}

struct AppMemoryStruct 
{
	char *memory;
	size_t size;
};

struct data {
	char trace_ascii; /* 1 or 0 */ 
};

static void dump(const char *text, FILE *stream, unsigned char *ptr, size_t size, char nohex)
{
  size_t i;
  size_t c;
 
  unsigned int width=0x10;
 
  if(nohex)
    /* without the hex output, we can fit more on screen */ 
    width = 0x40;
 
  fprintf(stream, "%s, %10.10ld bytes (0x%8.8lx)\n",
          text, (long)size, (long)size);
 
  for(i=0; i<size; i+= width) {
 
    fprintf(stream, "%4.4lx: ", (long)i);
 
    if(!nohex) {
      /* hex not disabled, show it */ 
      for(c = 0; c < width; c++)
        if(i+c < size)
          fprintf(stream, "%02x ", ptr[i+c]);
        else
          fputs("   ", stream);
    }
 
    for(c = 0; (c < width) && (i+c < size); c++) {
      /* check for 0D0A; if found, skip past and start a new line of output */ 
      if (nohex && (i+c+1 < size) && ptr[i+c]==0x0D && ptr[i+c+1]==0x0A) {
        i+=(c+2-width);
        break;
      }
      fprintf(stream, "%c",
              (ptr[i+c]>=0x20) && (ptr[i+c]<0x80)?ptr[i+c]:'.');
      /* check again for 0D0A, to avoid an extra \n if it's at width */ 
      if (nohex && (i+c+2 < size) && ptr[i+c+1]==0x0D && ptr[i+c+2]==0x0A) {
        i+=(c+3-width);
        break;
      }
    }
    fputc('\n', stream); /* newline */ 
  }
  fflush(stream);
}
 
static int my_trace(CURL *handle, curl_infotype type, char *data, size_t size, void *userp)
{
  struct data *config = (struct data *)userp;
  const char *text;
  (void)handle; /* prevent compiler warning */ 
  FILE *headerfile;
  InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("ptrIAppFramework is nil");
		return 0;

	}
  ptrIAppFramework->SetFilePath();

  PMString fileFullPath = ptrIAppFramework->GetFilePath();
  string tempString(fileFullPath.GetPlatformString());
  char* path =const_cast<char *> (tempString.c_str());

  headerfile = fopen(path, "a");

  metadata::Clock a;
	XMP_DateTime dt;
	a.timestamp(dt);
	PMString time("");
	time.AppendNumber(dt.month);
	time.Append("/");
	time.AppendNumber(dt.day);
	time.Append("/");
	time.AppendNumber(dt.year);
	time.Append(" ");
	if(dt.hour < 10 )
	{
		time.Append("0");
	}
	time.AppendNumber(dt.hour);
	time.Append(":");

	if(dt.minute < 10)
	{
		time.Append("0");
	}
	time.AppendNumber(dt.minute);
	time.Append(":");
	if(dt.second < 10 )
	{
		time.Append("0");
	}
	time.AppendNumber(dt.second);
	time.Append(":");
	if(dt.nanoSecond < 10 )
	{
		time.Append("0");
	}
	time.AppendNumber(dt.nanoSecond);

	//text = time.GetUTF8String();
//	 dump(text, headerfile, (unsigned char *)data, size, config->trace_ascii);

  switch (type) {
  case CURLINFO_TEXT:
	  {
		 time.Append(" => CURLINFO_TEXT : ");
		 string tempString1(time.GetPlatformString());
		 text =const_cast<char *> (tempString1.c_str());
		  dump(text, headerfile, (unsigned char *)data, size, config->trace_ascii); 
	  }
  case CURLINFO_HEADER_OUT:
	  {
		 time.Append(" => Send header");
		 string tempString1(time.GetPlatformString());
		 text =const_cast<char *> (tempString1.c_str());
		  dump(text, headerfile, (unsigned char *)data, size, config->trace_ascii);
	  }
    break;
  case CURLINFO_DATA_OUT:
	  {
		 time.Append(" => Send data");
		 string tempString1(time.GetPlatformString());
		 text =const_cast<char *> (tempString1.c_str());
		  dump(text, headerfile, (unsigned char *)data, size, config->trace_ascii);
	  }
    break;
  case CURLINFO_SSL_DATA_OUT:
	  {
		 time.Append(" => Send SSL data");
		 string tempString1(time.GetPlatformString());
		 text =const_cast<char *> (tempString1.c_str());
		  dump(text, headerfile, (unsigned char *)data, size, config->trace_ascii);
	  }
    break;
  case CURLINFO_HEADER_IN:
	  {
		 time.Append(" <= Recv header");
		 string tempString1(time.GetPlatformString());
		 text =const_cast<char *> (tempString1.c_str());
		  dump(text, headerfile, (unsigned char *)data, size, config->trace_ascii);
	  }
    break;
  case CURLINFO_DATA_IN:
	  {
		 time.Append(" <= Recv data") ;
		 string tempString1(time.GetPlatformString());
		 text =const_cast<char *> (tempString1.c_str());
		 dump(text, headerfile, (unsigned char *)data, size, config->trace_ascii);
	  }
    break;
  case CURLINFO_SSL_DATA_IN:
	  {
		time.Append(" <= Recv SSL data");
		string tempString1(time.GetPlatformString());
		text =const_cast<char *> (tempString1.c_str());
		dump(text, headerfile, (unsigned char *)data, size, config->trace_ascii);
	  }
    break;

   default: /* in case a new one is introduced to shock us */ 
    return 0;

  }
 

 
  		

  fclose(headerfile);
  return 0;
}


static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}


static size_t AppWriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
	//CA("Inside AppWriteMemoryCallback");
  size_t realsize = size * nmemb;
  struct AppMemoryStruct *mem = (struct AppMemoryStruct *)userp;
 
  mem->memory = (char *)realloc(mem->memory, mem->size + realsize + 1);
  if (mem->memory == NULL) {
    /* out of memory! */ 
    //CA("not enough memory (realloc returned NULL)\n");
    exit(EXIT_FAILURE);
  }
 
  memcpy(&(mem->memory[mem->size]), contents, realsize);
  mem->size += realsize;
  mem->memory[mem->size] = 0;
 
  return realsize;
}

bool16 AppFramework::callService(PMString url, PMString &returndata)
{
	////bool16 errorCode = kFalse; 
	////CURL *curl;
 ////   CURLcode res;

	////curl_global_init(CURL_GLOBAL_ALL);
	////curl = curl_easy_init();

	////struct data config; 
	////config.trace_ascii = 1; /* enable ascii tracing */ 
	////
	////struct curl_slist *chunk = NULL;
 ////   chunk = curl_slist_append(chunk, "Content-Type: application/json");

	////struct AppMemoryStruct chunk1; 
 ////   chunk1.memory = (char *)malloc(1);  /* will be grown as needed by the realloc above */ 
 ////   chunk1.size = 0;    /* no data at this point */ 

	//// if(curl) {
	//////CA("Inside curl");
 ////   //curl_easy_setopt(curl, CURLOPT_URL, "https://dev08.apsiva.net/cf-cm/master/structure.json;jsessionid=D12B0325A94E711D36C1A614756F98F9.worker1");
	//////curl_easy_setopt(curl, CURLOPT_URL, "https://prd02.apsiva.net/cf-cm/json/244/login.json?username=webapi2@apsiva.com&pass=wa7744$$&apikey=1dd80ee107d3ad9593c857cc6d14d3a3");

	////curl_easy_setopt(curl, CURLOPT_URL, url.GetPlatformString().c_str());

	//////curl_easy_setopt(curl,CURLOPT_SSLCERTTYPE,"PEM");

	////PMString path;
	//////SDKUtilities::GetApplicationFolder(path);
	////FileUtils::GetAppFolder(&path);
	////#ifdef MACINTOSH
	////path+=":";
	////#else
	////	path+="\\";
	////#endif
	////	// appending folder name
	////	path+="Plug-ins";

	////	// appending '\'
	////#ifdef MACINTOSH
	////	path+=":";
	////#else
	////	path+="\\";
	////#endif
	////path+="PRINTsource";

	////	// appending '\'
	////#ifdef MACINTOSH
	////	path+=":";
	////#else
	////	path+="\\";
	////#endif
	////// appending file name
	////path+="printsource.pem";
	//////curl_easy_setopt(curl, CURLOPT_SSLCERT, path.GetPlatformString().c_str()); 
	//////curl_easy_setopt(curl, CURLOPT_SSLCERT, "output5.pem");	
 ////  	
 ////   curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, FALSE); 
 ////   curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);   
	//////curl_easy_setopt(curl,CURLOPT_KEYPASSWD,"");

	////curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, my_trace);
 ////   curl_easy_setopt(curl, CURLOPT_DEBUGDATA, &config);
	//////curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L); 
	////	
 //// 	char error[1024];
	////curl_easy_setopt ( curl, CURLOPT_ERRORBUFFER, error ); 
	//// 
	//// /* send all data to this function  */ 
	////curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &AppWriteMemoryCallback);
 ////
	/////* we pass our 'chunk' struct to the callback function */ 
	////curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk1);
	////
	/////* some servers don't like requests that are made without a user-agent
 ////    field, so we provide one */
 ////   //curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");	
	////
	////curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);

	//// curl_easy_setopt(curl, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.02; Windows NT 5.0)");
	//// curl_easy_setopt(curl, CURLOPT_HEADER, FALSE);

	//////CA("After curl_easy_setopt CURLOPT_HTTPHEADER");
 ////   res = curl_easy_perform(curl);
	//////CA("After curl_easy_perform");

	//////fclose(headerfile);

 ////   if(CURLE_OK != res)
 ////   {
	////	//CA("CURLE_OK != res");
	////	this->LogError("CURLE_OK != res");
	////	//CA( strerror(res));
	////	this->LogError(PMString(error));		
	////	errorCode = kFalse;

	////    if(chunk1.memory)
	////	  {
	////		//CA(chunk1.memory);
	////		free(chunk1.memory);

	////	  }
	////	 curl_easy_cleanup(curl);
 ////     return errorCode;
 ////   }
	////errorCode = kTrue;
	//////CA("CURLE_OK == res");	
	////
	////if(chunk1.memory)
	////{
	////	//CA(chunk1.memory);
	////	returndata.SetCString(chunk1.memory, PMString::kUnknownEncoding );

	////	//CA(returndata);
	////	free(chunk1.memory);

	////}
 ////     /* always cleanup */
 ////   curl_easy_cleanup(curl);
 //// }

 static int callCounter = 0;

    setlocale(LC_ALL, "en_US.utf8");
	bool16 errorCode = kFalse;
    CURL *curl;
    CURLcode res;

    //CA("JL1a");
    curl_global_init(CURL_GLOBAL_ALL);
    //CA("JL1b");
    curl = curl_easy_init();
    //CA("JL1c");

    struct data config;
    config.trace_ascii = 1; /* enable ascii tracing */

    struct curl_slist *chunk = NULL;
    chunk = curl_slist_append(chunk, "Content-Type: application/json; charset=UTF-8");
    //CA("JL1d");

    struct AppMemoryStruct chunk1;
    chunk1.memory = (char *)malloc(1);  /* will be grown as needed by the realloc above */
    chunk1.size = 0;    /* no data at this point */
    
    std::string readBuffer;

     if(curl) {

        //curl_easy_setopt(curl, CURLOPT_URL, "https://mail.google.com/");
       // curl_easy_setopt(curl, CURLOPT_SSLCERTTYPE,"PEM");
	//	curl_easy_setopt(curl, CURLOPT_SSLCERT, "D:\\RD\\Apsiva\\SSL\\output5.pem");
     //   curl_easy_setopt(curl, CURLOPT_KEYPASSWD,"");
      //  curl_easy_setopt(curl, CURLOPT_CAINFO,  "D:\\RD\\Apsiva\\SSL\\output5.pem");
        //curl_easy_setopt(curl, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
        //curl_easy_setopt(curl, CURLOPT_PROXY,"http://64.202.165.130:3128");

        char error[1024];
		// Debug Properties
      //  curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, my_trace);
        curl_easy_setopt(curl, CURLOPT_DEBUGDATA, &config);
       // curl_easy_setopt(curl, CURLOPT_VERBOSE, TRUE);

        curl_easy_setopt(curl, CURLOPT_URL, url.GetPlatformString().c_str());
        //curl_easy_setopt(curl, CURLOPT_URL, "https://74.125.225.118/");
        //curl_easy_setopt(curl, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.02; Windows NT 5.0)");
        curl_easy_setopt(curl, CURLOPT_HEADER, FALSE);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
        curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, error);
        //curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &AppWriteMemoryCallback);
        //curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk1);
         
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
         
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
		curl_easy_setopt(curl, CURLOPT_SSLVERSION, 6);
		curl_easy_setopt(curl, CURLOPT_ENCODING, "UTF-8");

		if(currentServerConfigValue.getVendorName()  == "Use_Proxy")// Check for Proxy Setting
		{
			this->LogDebug("Use_Proxy");
			PMString DbUsername = currentServerConfigValue.getDbUserName();
			PMString DbPassword = currentServerConfigValue.getDbPassword();
			PMString server = currentServerConfigValue.getServerName();
			PMString port= "";
			port.AppendNumber(currentServerConfigValue.getServerPort());

			PMString proxy = server + ":" + port;
			this->LogDebug("proxy = " + proxy );
			curl_easy_setopt(curl, CURLOPT_PROXY, proxy.GetPlatformString().c_str());
			PMString auth = "";
			if(DbUsername != "" && DbPassword != "" )
			{
				auth = DbUsername + ":" + DbPassword;
				this->LogDebug("auth= " + auth);
				curl_easy_setopt(curl, CURLOPT_PROXYUSERPWD, auth.GetPlatformString().c_str());
			}			
			curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
			//curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			
		}
		
		
        //CA("JL1r");
        res = curl_easy_perform(curl);
        //CA("JL1s");

        if(CURLE_OK != res)
        {
            //CA("CURLE_OK != res");
           // CA(error);
            errorCode = kFalse;
			//returndata.Append(url);
			returndata.Append(" Unreachable. Please contact your System Administrator!!");

            if(chunk1.memory)
              {
                //CA(chunk1.memory);
                free(chunk1.memory);

              }
             curl_easy_cleanup(curl);
			 curl_slist_free_all(chunk);
          return errorCode;
        }

        errorCode = kTrue;

        //if(chunk1.memory)
        {
            FILE *headerfile;
            InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
            if(ptrIAppFramework == nil)
            {
                CAlert::InformationAlert("ptrIAppFramework is nil");
                return 0;
                
            }
            PMString fileFullPath = ptrIAppFramework->GetFilePath();
            fileFullPath.Append("1");
            string tempString(fileFullPath.GetPlatformString());
            char* path =const_cast<char *> (tempString.c_str());
            
            //headerfile = fopen(path, "a");
            
            
            std::ofstream file(tempString.c_str());
            std::string my_string = "Hello text in file\n";
            file << my_string;
            file << readBuffer;
            file << "---------------------------------------";
            
            
            //returndata.SetCString(chunk1.memory , PMString::kUnknownEncoding /* kUnknownEncoding kEncodingASCII*/);
			WideString wideString;
            //wideString.SetCString(chunk1.memory, kTrue, chunk1.size);
			//StringUtils::ConvertUTF8ToWideString(chunk1.memory, wideString);
            
            //StringUtils::ConvertUTF8ToWideString(readBuffer, wideString);
            
            //std::string tempString;
            //StringUtils::ConvertWideStringToUTF8(wideString, tempString);
            //returndata.Append(wideString);
            //returndata.SetEncoding(PMString::kEncodingChineseBig5);
			
            //std::string tempString (chunk1.memory /*, chunk1.size*/);
            //returndata.SetUTF8String(tempString);
            
            // returndata.SetUTF8String(tempString.c_str());
            //std::string tempString1;
           // tempString1 = returndata.GetUTF8String();
            StringUtils::ConvertUTF8ToWideString(readBuffer, wideString);
            //returndata.Clear();
            //file << wideString;
            file.close();
            returndata.Append(wideString);
            //bool16 isUnicodePresent = returndata.ParseForEmbeddedCharacters();
            //if(isUnicodePresent)
            //    CA("isUnicodePresent");
            //returndata.ConvertToPlatformEncoding();
            //returndata.SetXString(readBuffer.c_str(), std::strlen(readBuffer.c_str()), PMString::kEncodingVietnamese);
            //returndata.SetUTF8String(readBuffer.c_str());
			this->LogError(returndata);
            free(chunk1.memory);

        }

        /* always cleanup */
        curl_easy_cleanup(curl);
		curl_slist_free_all(chunk);

		jsonParser jsonPraserObj;		
		bool16 isSessionTimeout =  jsonPraserObj.checkSessionTimeoutJson(returndata);
		if(isSessionTimeout == kTrue)
		{
			LogError("Session Timeout Occured");
			if(callCounter < 4) // Calling Re-Login for 3 times.
			{
				callCounter = callCounter + 1;
				PMString callCounterStr("Call Counter : " );
				callCounterStr.AppendNumber(callCounter);
				LogError(callCounterStr);
			}
			else 
			{
				LogError("Existing Re-Login process after 3 try!  Please contact your System Administrator!");
				CA("Existing Re-Login process after 3 try!  Please contact your System Administrator!");
				errorCode = kFalse;
				return errorCode;
			}
			
			LogError("Try to login again.");

			PMString clientName("");
			clientName.AppendNumber(PMReal(this->getClientID()));

			PMString oldJsessionIdString("jsessionid=");
			oldJsessionIdString.Append(jsessionID);

			LogError("Old JsessionId String :");
			LogError(oldJsessionIdString);

			PMString sessionId("");

			bool16 reloginResult = callLogin(SelectedServerURL, currentLoginUserEmail, currentLoginPassword, clientName, sessionId);
			if(reloginResult)
			{
				LogError("Re-Login successful.");
				returndata = "";

				LogError("OldURL : ");
				LogError(url);

				int32 LengthOfOldJsessionString = oldJsessionIdString.NumUTF16TextChars();

				int32 jessionStringPlaceCount = url.IndexOfString(oldJsessionIdString );

				PMString asd( " jessionStringPlaceCount : ");
				asd.AppendNumber(jessionStringPlaceCount);
				LogError(asd);

				PMString newJsessionIdString("jsessionid=");
				newJsessionIdString.Append(jsessionID);

				LogError("New JsessionId String :");
				LogError(newJsessionIdString);

				url.Remove(jessionStringPlaceCount, LengthOfOldJsessionString );

				LogError("URL After removing old JsessionId : ");
				LogError(url);

				url.Insert(newJsessionIdString , jessionStringPlaceCount ); 

				LogError("URL After Adding new JsessionId : ");
				LogError(url);

				callService(url, returndata);

				callCounter = 0;
			}
			else
			{
				LogError("Re-Login Failed. Please contact your System Administrator!");
				//CA("Re-Login Failed. Please contact your System Administrator!");
			}
		}
		else
		{
			callCounter = 0; // No session Timeout Everything is Fine.
		}
    }

  return errorCode;
}

bool16 AppFramework::callLoginFirstStep(PMString serverURL, PMString username, PMString password, vector<CClientModel> * vectClientModels, PMString & errorMsg)
{
	PMString jSonURL("");
	bool16 result = kFalse;

//htps://prd02.apsiva.net/cf-cm/json/244/login.json?username=webapi2@apsiva.com&pass=wa7744$$&apikey=1dd80ee107d3ad9593c857cc6d14d3a3

	jSonURL.Append(serverURL); //htps://prd02.apsiva.net/cf-cm
	jSonURL.Append("/json/");	
	jSonURL.Append("login.json?username=");
	jSonURL.Append(username); //webapi2@apsiva.com
	jSonURL.Append("&pass=");
    
    CURL *curl = curl_easy_init();
    if(curl) {
        char *output = curl_easy_escape(curl, password.GetPlatformString().c_str(), 0);
        if(output) {
            //printf("Encoded: %sn", output)
            password.Clear();
            password.Append(output);
            curl_free(output);
        }
    }
	jSonURL.Append(password); //wa7744$$
	//jSonURL.Append("&apikey=");
	//jSonURL.Append(apikey); //1dd80ee107d3ad9593c857cc6d14d3a3

	//this->LogDebug("jSonURL: " + jSonURL);
	PMString ASD("LogInjSonURL: " + jSonURL);
	//this->LogDebug(ASD);

	PMString loginOutput("");

	bool16 errorCode = callService(jSonURL, loginOutput);
	this->LogDebug(loginOutput);
	if(!errorCode)
	{
		//CA("Login Failed");
		this->LogError(loginOutput);
		errorMsg.Append(serverURL);
		errorMsg.Append(loginOutput);
		result = kFalse;
	}
	else
	{
		//CA("Login Successful: "+ loginOutput);

		jsonParser jsonPraserObj;
		//this->LogError(loginOutput);
		bool16 parseResult =  jsonPraserObj.parseClientValues(loginOutput, vectClientModels);

		if(parseResult)
		{
			//CA("Finally got sessionId : " + sessionId);

			/*IndesignLoginStatus = kTrue;
			SelectedServerURL = serverURL;
			jsessionID = sessionId;*/
			result = kTrue;
		}
		else
		{
			//CA("Login Falied  : " + sessionId);
			errorMsg =  "Incorrect Login. Invalid ID or password. Please try again!";
			result = kFalse;
		}

	}
	return result;
}


bool16 AppFramework::callLogin(PMString serverURL, PMString username, PMString password, /*PMString apikey,*/ PMString clientNo, PMString &sessionId)
{
	PMString jSonURL("");
	bool16 result = kFalse;


//htps://prd02.apsiva.net/cf-cm/json/244/login.json?username=webapi2@apsiva.com&pass=wa7744$$&apikey=1dd80ee107d3ad9593c857cc6d14d3a3
    PMString originalPassword=password;

	jSonURL.Append(serverURL); //htps://prd02.apsiva.net/cf-cm
	jSonURL.Append("/json/");
	jSonURL.Append(clientNo); //244
	jSonURL.Append("/login.json?username=");
	jSonURL.Append(username); //webapi2@apsiva.com
	jSonURL.Append("&pass=");
    
    CURL *curl = curl_easy_init();
    if(curl) {
        char *output = curl_easy_escape(curl, password.GetPlatformString().c_str(), 0);
        if(output) {
            //printf("Encoded: %sn", output)
            password.Clear();
            password.Append(output);
            curl_free(output);
        }
    }


	jSonURL.Append(password); //wa7744$$
	//jSonURL.Append("&apikey=");
	//jSonURL.Append(apikey); //1dd80ee107d3ad9593c857cc6d14d3a3

	//this->LogDebug("jSonURL: " + jSonURL);
	PMString ASD("LogInjSonURL: " + jSonURL);
	//this->LogError(ASD);

	PMString loginOutput("");

	bool16 errorCode = callService(jSonURL, loginOutput);
	if(!errorCode)
	{
		//CA("Login Failed");
		this->LogError(loginOutput);
		result = kFalse;
	}
	else
	{
		//this->LogError("Login Successful: "+ loginOutput);

		jsonParser jsonPraserObj;
        apiKey="";
        pimVersion ="";
		
		bool16 parseResult =  jsonPraserObj.parseLogin(loginOutput, sessionId, apiKey, pimVersion);

		if(parseResult)
		{
			this->LogError("Finally got sessionId : " + sessionId);

			IndesignLoginStatus = kTrue;
			SelectedServerURL = serverURL;
			jsessionID = sessionId;
			result = kTrue;
			currentLoginUserEmail = username;
			currentLoginPassword = originalPassword;
            
		}
		else
		{
			//CA("Login Falied  : " + sessionId);
			result = kFalse;
		}

	}
	return result;
}


bool16 AppFramework::getLoginStatus()
{
	return IndesignLoginStatus;
}

bool16 AppFramework::callLogout(PMString &logoutResult)
{
	PMString jSonURL("");

	if(jsessionID == "")
	{
		//CA("jsessionID == """);
		return kFalse;
	}
	
	//https://prd02.apsiva.net/cf-cm/main/logout.json;jsessionid=8D3913339C091CCD459C27F748452240.worker1

	jSonURL.Append(SelectedServerURL); //https://prd02.apsiva.net/cf-cm
	jSonURL.Append("/main/logout.json;jsessionid=");
	jSonURL.Append(jsessionID);
	
	//CA("LogOutjSonURL: " + jSonURL);
	PMString ASD("LogOutjSonURL: " + jSonURL);
	this->LogError(ASD);
	
	PMString jsonServiceOutput("");

	bool16 errorCode = callService(jSonURL, jsonServiceOutput);
	if(!errorCode)
	{
		PMString ls("Logout Failed");
		ls.SetTranslatable(kFalse);
		CA(ls);
	}
	else
	{
		//CA("Logout Successful: " + jsonServiceOutput);

		
		jsonParser jsonPraserObj;
		jsonPraserObj.parseLogout(jsonServiceOutput, logoutResult);

		//CA("Finally got logout  : " + logoutResult);

		IndesignLoginStatus = kFalse;
		SelectedServerURL = "";
		jsessionID = "";
        apiKey = "";
		CatalogID = -1;
		LocaleID = -1;
		clientID = -1;
		currentLoginUserEmail = "";
		currentLoginPassword = "";

	}
	return errorCode;

}

bool16 AppFramework::callStructureJson( StructureCacheValue & returndata)
{
	PMString jSonURL("");
	if(jsessionID == "")
	{
		//CA("jsessionID == """);
		return kFalse;
	}

	
	
	//https://prd02.apsiva.net/cf-cm/master/structure.json;jsessionid=8D3913339C091CCD459C27F748452240.worker1

	//https://prd34.apsiva.net/cf-cm/main/logout.json;jsessionid=8D3913339C091CCD459C27F748452240.worker1


	jSonURL.Append(SelectedServerURL); //https://prd02.apsiva.net/cf-cm
	jSonURL.Append("/master/structure.json;jsessionid=");
	jSonURL.Append(jsessionID);
	
	//CA("StructurejSonURL: " + jSonURL);
	PMString ASD("StructurejSonURL: " + jSonURL);
	this->LogDebug(ASD);
	
	PMString jsonServiceOutput("");

	bool16 errorCode = callService(jSonURL, jsonServiceOutput);
	if(!errorCode)
	{
		//CA("Json Service Call Failed");
        this->LogError("Json Service Call Failed");
	}
	else
	{
		//CA("Structure Json call Successful : " + jsonServiceOutput);
		
		jsonParser jsonPraserObj;
		jsonPraserObj.parseStructure(jsonServiceOutput, returndata);

		//long ChildrenSize = ;
		//CA("Finally got Structure  : " + returndata);
		//vector<StructureCacheValue> vectorObj = returndata.getChildren();

		/*CA("ClassificationName : " + returndata.getClassificationName());
		
		CA_NUM("ChildrenSize : ", (int32)vectorObj.size() );
		CA_NUM("ItemFields : ", (int32)returndata.getItemFields().size() );
		CA_NUM("ItemGroupFields : ", (int32)returndata.getItemGroupFields().size() );
		CA_NUM("itemAssetTypes : ", (int32)returndata.getItemAssetTypes().size() );
		CA_NUM("itemGroupAssetTypes : ", (int32)returndata.getItemGroupAssetTypes().size() );
		CA_NUM("listTypes : ", (int32)returndata.getListTypes().size() );
		CA_NUM("sectionFields : ", (int32)returndata.getSectionFields().size() );
		CA_NUM("sectionAssetTypes : ", (int32)returndata.getSectionAssetTypes().size() );
		CA_NUM("languages : ", (int32)returndata.getLanguages().size() );
		CA_NUM("statuses : ", (int32)returndata.getStatuses().size() );
		CA_NUM("picklistGroups : ", (int32)returndata.getPicklistGroups().size() );
		CA_NUM("picklistValues : ", (int32)returndata.getPicklistGroups().at(0).getPicklistValues().size() );*/
		//ChildrenSize = ChildrenSize +1 ;

	}
    
    if(pimVersion == "3" )
    {
        //call V3 getAttribute & replace itemFields with result of V3 call.
        vector<Attribute> itemFields;
        returndata.setItemFields(itemFields);
        callGetAttributesV3(itemFields);
        returndata.setItemFields(itemFields);
    }
    
	return errorCode;

}


bool16 AppFramework::callAllTreeJson( EventCacheValue &returndata)
{
	PMString jSonURL("");
	if(jsessionID == "")
	{
		//CA("jsessionID == """);
		return kFalse;
	}
	
	//https://prd02.apsiva.net/cf-cm/master/structure.json;jsessionid=8D3913339C091CCD459C27F748452240.worker1
	//https://prd02.apsiva.net/cf-cm/event/allTrees.json


	jSonURL.Append(SelectedServerURL); //https://prd02.apsiva.net/cf-cm
	jSonURL.Append("/event/allTrees.json;jsessionid=");
	jSonURL.Append(jsessionID);
    jSonURL.Append("?printEventsOnly=true");
	
	//CA("EventjSonURL: " + jSonURL);
	PMString ASD("EventjSonURL: " + jSonURL);
	this->LogDebug(ASD);

	
	PMString jsonServiceOutput("");

	bool16 errorCode = callService(jSonURL, jsonServiceOutput);
	if(!errorCode)
	{
		//CA("callAllTreeJson Service Call Failed");
        this->LogError("callAllTreeJson Service Call Failed");
	}
	else
	{		
		jsonParser jsonPraserObj;
		jsonPraserObj.parseAllTree(jsonServiceOutput, returndata);
	}
	return errorCode;

}

bool16 AppFramework::callConfigJson(vector<ConfigValue> &returndata)
{
	PMString jSonURL("");
	if(jsessionID == "")
	{
		//CA("jsessionID == """);
		return kFalse;
	}
	
	//https://prd02.apsiva.net/cf-cm/master/structure.json;jsessionid=8D3913339C091CCD459C27F748452240.worker1
	//https://prd02.apsiva.net/cf-cm2/sysConfig/list.json?indesign=true


	jSonURL.Append(SelectedServerURL); //https://prd02.apsiva.net/cf-cm
	jSonURL.Append("/sysConfig/list.json;jsessionid="); //?indesign=true
	jSonURL.Append(jsessionID);
	
	//CA("ConfigjSonURL: " + jSonURL);
	PMString ASD("ConfigjSonURL: " + jSonURL);
	this->LogDebug(ASD);

	
	PMString jsonServiceOutput("");

	bool16 errorCode = callService(jSonURL, jsonServiceOutput);
	if(!errorCode)
	{
		//CA("callConfigJson Service Call Failed");
		this->LogError("callConfigJson Service Call Failed");
	}
	else
	{		
		jsonParser jsonPraserObj;
		jsonPraserObj.parseSysConfig(jsonServiceOutput, returndata);
	}
	return errorCode;
}

static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream)
{
  size_t written = fwrite(ptr, size, nmemb, (FILE *)stream);
  return written;
}


bool16 AppFramework::callSectionResultJson(double sectionId, double langId, vector<CPbObjectValue> &returndata, PMString itemAttributeId)
{
	PMString jSonURL("");
	if(jsessionID == "")
	{
		//CA("jsessionID == """);
		return kFalse;
	}
		
	//https://prd02.apsiva.net/cf-cm/master/structure.json;jsessionid=8D3913339C091CCD459C27F748452240.worker1
	//https://prd02.apsiva.net/cf-cm2/event/section/12580/results.json


	jSonURL.Append(SelectedServerURL); //https://prd02.apsiva.net/cf-cm
	jSonURL.Append("/event/section/");
	jSonURL.AppendNumber(PMReal(sectionId));
	jSonURL.Append("/results.json;jsessionid="); //?indesign=true
	jSonURL.Append(jsessionID);
	jSonURL.Append("?cmsite=true");
    jSonURL.Append("&languageId=");
    jSonURL.AppendNumber(PMReal(langId));
    
    if(itemAttributeId != "")
    {
        jSonURL.Append("&itemFieldIds=");
        jSonURL.Append(itemAttributeId);
    }
    jSonURL.Append("&includeListsAndTables=FALSE");
	//CA("SectionResultjSonURL: " + jSonURL);

	PMString ASD("SectionResultjSonURL: " + jSonURL);
	this->LogDebug(ASD);
	
	PMString jsonServiceOutput("");

	bool16 errorCode = callService(jSonURL, jsonServiceOutput);
	if(!errorCode)
	{
		//CA("callSectionResultJson Service Call Failed");
		this->LogError("callSectionResultJson Service Call Failed for " );
		this->LogError( jSonURL.GetPlatformString().c_str() );
	}
	else
	{		
		jsonParser jsonPraserObj;
		jsonPraserObj.parseSectionResult(jsonServiceOutput, returndata, sectionId, itemAttributeId);
	}



	 // CURL *curl_handle;
	 // static const char *pagefilename = "D:\\page.json";
	 // FILE *pagefile;

	 ///* if(argc < 2 ) {
		//printf("Usage: %s <URL>\n", argv[0]);
		//return 1;
	 // }*/

	 // curl_global_init(CURL_GLOBAL_ALL);

	 // /* init the curl session */
	 // curl_handle = curl_easy_init();

	 // /* set URL to get here */
	 // curl_easy_setopt(curl_handle, CURLOPT_URL, jSonURL.GetPlatformString().c_str());

	 // /* Switch on full protocol/debug output while testing */
	 // curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1L);

	 // /* disable progress meter, set to 0L to enable and disable debug output */
	 // curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1L);

	 // /* send all data to this function  */
	 // curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_data);

	 // /* open the file */
	 // pagefile = fopen(pagefilename, "w");
	 // if (pagefile) {
		// // CA("Got pageFile");
		///* write the page body to this file handle. CURLOPT_FILE is also known as
		//   CURLOPT_WRITEDATA*/
		//curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, pagefile);

		///* get it! */
		//curl_easy_perform(curl_handle);

		///* close the header file */
		//fclose(pagefile);
	 // }

	 // /* cleanup curl stuff */
	 // curl_easy_cleanup(curl_handle);






	return errorCode;
}

bool16 AppFramework::callSectionDetailResultJson(double sectionId, double langId, vector<CPbObjectValue> &returndata , PMString itemGroupIds, PMString itemIds ,
	PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds, PMString listItemFieldIds, bool16 isSprayItemPerFrameFlag, PMString itemAttributeGroupIds)
{
	PMString jSonURL("");
	if(jsessionID == "")
	{
		//CA("jsessionID == """);
		return kFalse;
	}
	
	//https://prd02.apsiva.net/cf-cm2/event/section/12580/results.json;jsessionid=8D3913339C091CCD459C27F748452240.worker1
	bool FirstAttributeflag = false;

	jSonURL.Append(SelectedServerURL); //https://prd02.apsiva.net/cf-cm
	jSonURL.Append("/event/section/");
	jSonURL.AppendNumber(PMReal(sectionId));
	jSonURL.Append("/results.json;jsessionid="); //?indesign=true
	jSonURL.Append(jsessionID);

	//jSonURL.Append("?printActive=true");
	jSonURL.Append("?languageId=");
	jSonURL.AppendNumber(PMReal(langId));
	
	if( itemGroupIds != "" )
	{		
		jSonURL.Append("&itemGroupIds=");
		jSonURL.Append(itemGroupIds);		
	}
	if( itemIds != "" )
	{
		
		jSonURL.Append("&itemIds=");
		jSonURL.Append(itemIds);		
	}
	if( itemFieldIds != "" )
	{
		
		jSonURL.Append("&itemFieldIds=");
		jSonURL.Append(itemFieldIds);		
	}
	if( itemAssetTypeIds != "" )
	{
		jSonURL.Append("&");
		jSonURL.Append("itemAssetTypeIds=");
		jSonURL.Append(itemAssetTypeIds);

	}
	if( itemGroupFieldIds != "" )
	{
		jSonURL.Append("&");
		jSonURL.Append("itemGroupFieldIds=");
		jSonURL.Append(itemGroupFieldIds);
	}
    if( itemAttributeGroupIds != "" )
    {
        jSonURL.Append("&");
        jSonURL.Append("itemAttributeGroupIds=");
        jSonURL.Append(itemAttributeGroupIds);
    }
	if( itemGroupAssetTypeIds != "" )
	{
		jSonURL.Append("&");
		jSonURL.Append("itemGroupAssetTypeIds=");
		jSonURL.Append(itemGroupAssetTypeIds);
	}
	if( listTypeIds != "" )
	{
		jSonURL.Append("&");
		jSonURL.Append("listTypeIds=");
		jSonURL.Append(listTypeIds);
	}
	if( listItemFieldIds != "" )
	{
		jSonURL.Append("&");
		jSonURL.Append("listItemFieldIds=");
		jSonURL.Append(listItemFieldIds);
	}
	if( isSprayItemPerFrameFlag == kTrue || listItemFieldIds != "" )
	{
		jSonURL.Append("&");
		jSonURL.Append("includeListItemsAsResults=true");		
	}

    jSonURL.Append("&includeListsAndTables=TRUE");
	
	PMString ASD("SectionResultjSonURL: " + jSonURL);
	this->LogDebug(ASD);

	PMString jsonServiceOutput("");
	bool16 errorCode = callService(jSonURL, jsonServiceOutput);
	if(!errorCode)
	{
		//CA("callSectionDetailResultJson Service Call Failed");
		this->LogError("callSectionDetailResultJson Service Call Failed for " );
		this->LogError( jSonURL.GetPlatformString().c_str() );
	}
	else
	{		
		jsonParser jsonPraserObj;
		jsonPraserObj.parseSectionResult(jsonServiceOutput, returndata, sectionId);
	}
	return errorCode;
}


bool16 AppFramework::callItemGroupDetailJson(double itemGroupId, double sectionId, double langId, CObjectValue &returndata , PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds, PMString listItemFieldIds )
{
	PMString jSonURL("");
	if(jsessionID == "")
	{
		//CA("jsessionID == """);
		return kFalse;
	}
	bool FirstAttributeflag = false;
	//https://prd02.apsiva.net/cf-cm2/event/section/12580/10014111/groupDetails.json;jsessionid=8D3913339C091CCD459C27F748452240.worker1
	
	jSonURL.Append(SelectedServerURL); //https://prd02.apsiva.net/cf-cm
	jSonURL.Append("/event/section/");
	jSonURL.AppendNumber(PMReal(sectionId));
	jSonURL.Append("/");
	jSonURL.AppendNumber(PMReal(itemGroupId));
	jSonURL.Append("/groupDetails.json;jsessionid="); 
	jSonURL.Append(jsessionID);	

	//jSonURL.Append("?printActive=true");
	jSonURL.Append("?languageId=");
	jSonURL.AppendNumber(PMReal(langId));
	FirstAttributeflag = true;

	if( itemFieldIds != "" )
	{
		jSonURL.Append("&itemFieldIds=");
		jSonURL.Append(itemFieldIds);
		FirstAttributeflag = true;
	}
	if( itemAssetTypeIds != "" )
	{
		if(!FirstAttributeflag)
		{
			jSonURL.Append("?");
			FirstAttributeflag = true;
		}
		else
			jSonURL.Append("&");
		jSonURL.Append("itemAssetTypeIds=");
		jSonURL.Append(itemAssetTypeIds);
	}
	if( itemGroupFieldIds != "" )
	{
		if(!FirstAttributeflag)
		{
			jSonURL.Append("?");
			FirstAttributeflag = true;
		}
		else
			jSonURL.Append("&");
		jSonURL.Append("itemGroupFieldIds=");
		jSonURL.Append(itemGroupFieldIds);
	}
	if( itemGroupAssetTypeIds != "" )
	{
		if(!FirstAttributeflag)
		{
			jSonURL.Append("?");
			FirstAttributeflag = true;
		}
		else
			jSonURL.Append("&");
		jSonURL.Append("itemGroupAssetTypeIds=");
		jSonURL.Append(itemGroupAssetTypeIds);
	}
	if( listTypeIds != "" )
	{
		if(!FirstAttributeflag)
		{
			jSonURL.Append("?");
			FirstAttributeflag = true;
		}
		else
			jSonURL.Append("&");
		jSonURL.Append("listTypeIds=");
		jSonURL.Append(listTypeIds);
	}
	if( listItemFieldIds != "" )
	{
		if(!FirstAttributeflag)
		{
			jSonURL.Append("?");
			FirstAttributeflag = true;
		}
		else
			jSonURL.Append("&");
		jSonURL.Append("listItemFieldIds=");
		jSonURL.Append(listItemFieldIds);
	}
	
	PMString ASD("ItemGroupDetailJson: " + jSonURL);
	this->LogDebug(ASD);
	//CA("callItemGroupDetailJson: " + jSonURL);
		
	PMString jsonServiceOutput("");
	bool16 errorCode = callService(jSonURL, jsonServiceOutput);
	if(!errorCode)
	{
		//CA("callItemGroupDetailJson Service Call Failed");
		this->LogError("callItemGroupDetailJson Service Call Failed for " );
		this->LogError( jSonURL.GetPlatformString().c_str() );
	}
	else
	{		
		jsonParser jsonPraserObj;
		jsonPraserObj.parseItemGroupDetails(jsonServiceOutput, returndata);
	}
	return errorCode;
}

bool16 AppFramework::callItemDetailJson(double itemId, double sectionId, double langId, CItemModel &returndata , PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds, PMString listItemFieldIds )
{
	PMString jSonURL("");
	if(jsessionID == "")
	{
		//CA("jsessionID == """);
		return kFalse;
	}
	bool FirstAttributeflag = false;
		
	//https://prd02.apsiva.net/cf-cm2/event/section/12580/30211703/itemDetails.json;jsessionid=8D3913339C091CCD459C27F748452240.worker1


	jSonURL.Append(SelectedServerURL); //https://prd02.apsiva.net/cf-cm
	jSonURL.Append("/event/section/");
	jSonURL.AppendNumber(PMReal(sectionId));
	jSonURL.Append("/");
	jSonURL.AppendNumber(PMReal(itemId));
	jSonURL.Append("/itemDetails.json;jsessionid="); 
	jSonURL.Append(jsessionID);	

	//jSonURL.Append("?printActive=true");
	jSonURL.Append("?languageId=");
	jSonURL.AppendNumber(PMReal(langId));
	FirstAttributeflag = true;

	if( itemFieldIds != "" )
	{
		jSonURL.Append("&itemFieldIds=");
		jSonURL.Append(itemFieldIds);
		FirstAttributeflag = true;
	}
	if( itemAssetTypeIds != "" )
	{
		if(!FirstAttributeflag)
		{
			jSonURL.Append("?");
			FirstAttributeflag = true;
		}
		else
			jSonURL.Append("&");
		jSonURL.Append("itemAssetTypeIds=");
		jSonURL.Append(itemAssetTypeIds);
	}
	if( itemGroupFieldIds != "" )
	{
		if(!FirstAttributeflag)
		{
			jSonURL.Append("?");
			FirstAttributeflag = true;
		}
		else
			jSonURL.Append("&");
		jSonURL.Append("itemGroupFieldIds=");
		jSonURL.Append(itemGroupFieldIds);
	}
	if( itemGroupAssetTypeIds != "" )
	{
		if(!FirstAttributeflag)
		{
			jSonURL.Append("?");
			FirstAttributeflag = true;
		}
		else
			jSonURL.Append("&");
		jSonURL.Append("itemGroupAssetTypeIds=");
		jSonURL.Append(itemGroupAssetTypeIds);
	}
	if( listTypeIds != "" )
	{	
		if(!FirstAttributeflag)
		{
			jSonURL.Append("?");
			FirstAttributeflag = true;
		}
		else
			jSonURL.Append("&");
		jSonURL.Append("listTypeIds=");
		jSonURL.Append(listTypeIds);
	}
	if( listItemFieldIds != "" )
	{	
		if(!FirstAttributeflag)
		{
			jSonURL.Append("?");
			FirstAttributeflag = true;
		}
		else
			jSonURL.Append("&");
		jSonURL.Append("listItemFieldIds=");
		jSonURL.Append(listItemFieldIds);
	}
	
	PMString ASD("ItemDetailJson: " + jSonURL);
	this->LogDebug(ASD);
	//CA("callItemGroupDetailJson: " + jSonURL);

	
	PMString jsonServiceOutput("");

	bool16 errorCode = callService(jSonURL, jsonServiceOutput);
	if(!errorCode)
	{
		//CA("callItemDetailJson Service Call Failed");
		this->LogError("callItemDetailJson Service Call Failed for " );
		this->LogError( jSonURL.GetPlatformString().c_str() );
	}
	else
	{		
		jsonParser jsonPraserObj;
		jsonPraserObj.parseItemDetails(jsonServiceOutput, returndata);
	}
	return errorCode;
}


bool16 AppFramework::callPartnerJson(PartnerCacheValue * returndata)
{
	PMString jSonURL("");
	if(jsessionID == "")
	{
		//CA("jsessionID == """);
		return kFalse;
	}
	
	//https://prd02.apsiva.net/cf-cm/master/structure.json;jsessionid=8D3913339C091CCD459C27F748452240.worker1
	//https://prd02.apsiva.net/cf-cm2/master/partners.json;jsessionid=8D3913339C091CCD459C27F748452240.worker1


	jSonURL.Append(SelectedServerURL); //https://prd02.apsiva.net/cf-cm
	jSonURL.Append("/master/partners.json;jsessionid="); 
	jSonURL.Append(jsessionID);
	
	//CA("ConfigjSonURL: " + jSonURL);
	PMString ASD("PartnerjSonURL: " + jSonURL);
	this->LogDebug(ASD);

	
	PMString jsonServiceOutput("");

	bool16 errorCode = callService(jSonURL, jsonServiceOutput);
	if(!errorCode)
	{
		//CA("callPartnerJson Service Call Failed");
		this->LogError("callPartnerJson Service Call Failed");
	}
	else
	{		
		jsonParser jsonPraserObj;
		jsonPraserObj.parsePartners(jsonServiceOutput, *returndata);
	}
	return errorCode;
}




void AppFramework::SetFilePath()
{

	if(FileStatus)
	return;

	PMString path;
	//SDKUtilities::GetApplicationFolder(path);
	FileUtils::GetAppFolder(&path);
    #ifdef MACINTOSH
    // Result path = Macintosh HD:Applications:Adobe InDesign CC Debug:Adobe InDesign CC.app:Contents:MacOS
    // so removing last 3 folder names as actual name is till Macintosh HD:Applications:Adobe InDesign CC Debug
    SDKUtilities::RemoveLastElement(path);
    SDKUtilities::RemoveLastElement(path);
    SDKUtilities::RemoveLastElement(path);
    #endif
    
    
	//CA(path);
	// appending  '\'
#ifdef MACINTOSH
	path+=":";
#else
	path+="\\";
#endif
	// appending folder name
	path+="Plug-ins";

	// appending '\'
#ifdef MACINTOSH
	path+=":";
#else
	path+="\\";
#endif
	// appending file name
	path+="PRINTSource.log";
	
	//CA(path);
#ifdef MACINTOSH
    {
        this->getUnixPath(path);
    }
#else
		{}
#endif
	filePath = path;
    //CA(path);

}

PMString AppFramework::GetFilePath()
{
	///char* pathfile; 	
	//pathfile = filePath.GetUTF8String(); //*****Depricated
	return filePath;
	//string tempString(filePath.GetPlatformString());
	//return const_cast<char *> (tempString.c_str());
	//pathfile = const_cast<char *> (filePath.GetPlatformString().c_str()); //*******CS4
	//return pathfile;
}

PMString AppFramework::GetTimeStamp()
{
	metadata::Clock a;
	XMP_DateTime dt;
	a.timestamp(dt);
	PMString time("");
	time.AppendNumber(dt.month);
	time.Append("/");
	time.AppendNumber(dt.day);
	time.Append("/");
	time.AppendNumber(dt.year);
	time.Append(" ");
	if(dt.hour < 10 )
	{
		time.Append("0");
	}
	time.AppendNumber(dt.hour);
	time.Append(":");

	if(dt.minute < 10)
	{
		time.Append("0");
	}
	time.AppendNumber(dt.minute);
	time.Append(":");
	if(dt.second < 10 )
	{
		time.Append("0");
	}
	time.AppendNumber(dt.second);
	time.Append(":");
	if(dt.nanoSecond < 10 )
	{
		time.Append("0");
	}
	time.AppendNumber(dt.nanoSecond);
	return time;

	
}

 void AppFramework::Log(TypeOfError etype, PMString errorString) //Cs4
{
	char* Filepath;
	 ofstream outFile ;
 do
 {
	 SetFilePath();
	 string tempString(filePath.GetPlatformString());
	 Filepath = const_cast<char *> (tempString.c_str());// GetFilePath();

	 GetFileStatus();
	 if(FileStatus == kFalse)
	 {
		 outFile.open( Filepath , ios::app);
		if(!outFile)
		{
			//CA("Unable to open Log File");
			break;
		}
		SetFileStatus(kTrue);
	 }
	 else
	 {
		 outFile.open( Filepath , ios::app);
		if(!outFile)
		{
			//CA("Unable to open Log File");
			break;
		}
	 }
   
	PMString FEED;
	//char* feedData; //**** Depricated
	const char* feedData;
	FEED = GetTimeStamp();

	if (etype == Debug)
	{
		FEED.Append(" Debug: ");
	}
	else if(etype == Info)
	{
		FEED.Append(" Info: ");
	}
	else if(etype == Error)
	{
		FEED.Append(" Error: ");
	}
	else if(etype == Fatal)
	{
		FEED.Append(" Fatal: ");
	}
	//FEED.Append("  Error Msg: ");
	//FEED+= errorString;
	string tempString1(FEED.GetPlatformString());
	feedData = const_cast<char *> (tempString1.c_str());
	//feedData= FEED.GetPlatformString().c_str(); //CS4

	string tempString2(errorString.GetPlatformString());
	char* errorData = const_cast<char *> (tempString2.c_str());

	outFile<<feedData<<errorData<<endl;
	outFile.close();
 } while(0);
}


 void AppFramework::LogDebug(PMString err_msg)
 {	
	//char *msg =  err_msg.GetUTF8String(); ///*****Depricated 
	//const char *msg =  (err_msg.GetPlatformString().c_str()/*GetPlatformString().c_str()*/); //******CS4
	if(LoggerFileFlag == 3  )//0:- error//1:- fatal,error//2:-info,fatal,error//3:-info,debug,fatal,error
		Log(Debug, err_msg);
 }


void AppFramework::LogInfo(PMString err_msg)	
{
	//char *msg = err_msg.GetUTF8String(); ////***Depricated
	//const char *msg = (err_msg./*GetPlatformString().c_str()*/GetPlatformString().c_str());//***CS4
	if(LoggerFileFlag == 3 ||LoggerFileFlag == 2  )//0:- error//1:- fatal,error//2:-info,fatal,error//3:-info,debug,fatal,error
	Log(Info,err_msg);
}

void AppFramework::LogError(PMString err_msg)	
{
	//char *msg = err_msg.GetUTF8String(); //******Depricated
	//const char *msg = (err_msg./*GetPlatformString().c_str()*/GetPlatformString().c_str()); //***CS4
	if(LoggerFileFlag == 3 || LoggerFileFlag == 2 || LoggerFileFlag == 1  || LoggerFileFlag == 0 )//0:- error//1:- fatal,error//2:-info,fatal,error//3:-info,debug,fatal,error
	Log(Error,err_msg);
}

void AppFramework::LogFatal(PMString err_msg)	
{
	//char *msg = err_msg.GetUTF8String(); //*****Depricated
	//const char *msg = (err_msg./*GetPlatformString().c_str()*/GetPlatformString().c_str()); //****Cs4	
	if(LoggerFileFlag == 3 || LoggerFileFlag == 2  || LoggerFileFlag == 1) //0:- error//1:- fatal,error//2:-info,fatal,error//3:-info,debug,fatal,error
	Log(Fatal,err_msg);
}


void AppFramework::getUnixPath(PMString& thisstring)
{
	 if (IsWinPath(thisstring))
		return;
	// Replace all instances of :: with ..
	int32 position = thisstring.IndexOfString(MACPARENTDIR);
	while (position!=-1)
	{
		thisstring.Remove(position,2); // remove the "::"
		thisstring.Insert(UNIX_WINPARENTDIR,position);
		thisstring.Insert("../",position+2); // replace it with "..\"
		position = thisstring.IndexOfString(MACPARENTDIR);
	}

	//  get all the MAC delimeters sorted.
	position = thisstring.IndexOfString(MACDELIMETER);
	while (position!=-1)
	{
		thisstring.Remove(position); // remove the ":"
		thisstring.Insert('/',position); // and put in a '\'
		position = thisstring.IndexOfString(MACDELIMETER);
	}

	// now get the unix delimeter sorted.
	/*position = thisstring.IndexOfString(UNIXDELIMETER);
	while (position!=-1)
	{
		thisstring.Remove(position); // remove the "/"
		thisstring.Insert('/',position); // and put in a '\'
		position = thisstring.IndexOfString(UNIXDELIMETER);
	}*/
	char*temp=NULL, *ptr=NULL;
	//temp=thisstring.GetUTF8String(); //******Depricated
	temp=const_cast<char*> (thisstring./*GetPlatformString().c_str()*/GetPlatformString().c_str()); //CS4
	ptr=std::strstr(temp, "/");
	if(ptr)
	{
		PMString aNewString(ptr);
		thisstring=aNewString;
			
	}
}


bool AppFramework::IsWinPath(PMString thisstring)
{
	// the way I do this is if it has a mac or unix delimeter then it can't be a win path
	// I also consider device names here...
	 return !(thisstring.Contains(UNIXDELIMETER)||(thisstring.Contains(MACDELIMETER)));
}

void AppFramework::setLoggerFileFlag(int32 setflag)
{
	LoggerFileFlag = setflag;
}

bool16 AppFramework::GetFileStatus()
{
	return FileStatus;
}

void AppFramework:: SetFileStatus(bool16 status)
{
	FileStatus = status;
}

VectorClassInfoPtr AppFramework::ClassificationTree_getRoot(double LanguageID)
{
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	return structureCacheObjPtr->ClassificationTree_getRoot(LanguageID);
}


VectorClassInfoPtr AppFramework::ClassificationTree_getAllChildren(double ClassID, double LanguageID)
{
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	return structureCacheObjPtr->ClassificationTree_getAllChildren(ClassID, LanguageID);
}

VectorPubModelPtr AppFramework::ProjectCache_getAllProjects(double LanguageID)
{
	EventCache* eventCacheObjPtr = EventCache::getInstance();
	return eventCacheObjPtr->ProjectCache_getAllProjects(LanguageID);
}

VectorPubModelPtr AppFramework::ProjectCache_getAllChildren(double PubId, double LanguageID)
{
	EventCache* eventCacheObjPtr = EventCache::getInstance();
	return eventCacheObjPtr->ProjectCache_getAllChildren(PubId, LanguageID);
}

void AppFramework::set_isONEsourceMode(bool16 mode)
{
	isONEsourceMode = mode;
}

bool16 AppFramework::get_isONEsourceMode()
{ 
	return isONEsourceMode;
}

VectorElementInfoPtr AppFramework::StructureCache_getItemGroupCopyAttributesByLanguageId(double languageId)
{
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	return structureCacheObjPtr->StructureCache_getItemGroupCopyAttributesByLanguageId( languageId);
}

VectorTypeInfoPtr AppFramework::StructureCache_getItemGroupImageAttributes()
{
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	return structureCacheObjPtr->StructureCache_getItemGroupImageAttributes();
}

bool16 AppFramework::StructureCache_isElementMPV(double elementID)
{
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	return structureCacheObjPtr->StructureCache_isElementMPV(elementID);
}

VectorTypeInfoPtr AppFramework::StructureCache_getListTableTypes()
{
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	return structureCacheObjPtr->StructureCache_getListTableTypes();
}

VectorAttributeInfoPtr AppFramework::StructureCache_getItemAttributesForClassAndParents(double ClassID , double LanguageID)
{
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	return structureCacheObjPtr->StructureCache_getItemAttributesForClassAndParents(ClassID, LanguageID);
}

VectorTypeInfoPtr AppFramework::StructureCache_getItemImagesForClassAndParents(double ClassID)
{
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	return structureCacheObjPtr->StructureCache_getItemImagesForClassAndParents(ClassID);
}

VectorElementInfoPtr AppFramework::StructureCache_getSectionCopyAttributesByLanguageId(double languageId)
{
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	return structureCacheObjPtr->StructureCache_getSectionCopyAttributesByLanguageId(languageId);
}

VectorTypeInfoPtr AppFramework::StructureCache_getSectionImageAttributes()
{
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	return structureCacheObjPtr->StructureCache_getSectionImageAttributes();
}

VectorLanguageModelPtr AppFramework::StructureCache_getAllLanguages()
{
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	return structureCacheObjPtr->StructureCache_getAllLanguages();
}

double AppFramework::getLocaleId()
{
	return LocaleID;
}
void AppFramework::setLocaleId(double locale_id)
{	
	LocaleID=locale_id;
}

void AppFramework::setCatalogId(double catalog_id)
{
	CatalogID = catalog_id;
}
	   
double AppFramework::getCatalogId()
{
	return CatalogID;
}

void AppFramework::StructureCache_clearInstance()
{
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	structureCacheObjPtr->clearInstace();
	
}
void AppFramework::EventCache_clearInstance()
{
	EventCache* eventCacheObjPtr = EventCache::getInstance();
	eventCacheObjPtr->clearInstace();
	PartnerCache* parnerCacheObj = PartnerCache::getInstance();
	parnerCacheObj->clearInstace();
}

bool16 AppFramework::CONFIGCACHE_getPVImageFileName(int32 index)
{
	ConfigCache* configCacheObjPtr = ConfigCache::getInstance();
	bool16 result = kFalse;
	ConfigValue configValueObj;

	switch(index)
	{
		case 1:
			configValueObj = configCacheObjPtr->getConfigCacheByConfigName("PV_IMAGE_FILENAME_1");
			break;
		case 2:
			configValueObj = configCacheObjPtr->getConfigCacheByConfigName("PV_IMAGE_FILENAME_2");
			break;
		case 3:
			configValueObj = configCacheObjPtr->getConfigCacheByConfigName("PV_IMAGE_FILENAME_3");
			break;
		case 4:
			configValueObj = configCacheObjPtr->getConfigCacheByConfigName("PV_IMAGE_FILENAME_4");
			break;
		case 5:
			configValueObj = configCacheObjPtr->getConfigCacheByConfigName("PV_IMAGE_FILENAME_5");
			break;
	}
	if((configValueObj.getConfigValue3()!= "") && (configValueObj.getConfigValue3() == "true"))
	{
		result = kTrue;
	}

	return result;
}

bool16 AppFramework::ConfigCache_getDisplayItemComponents()
{
	ConfigCache* configCacheObjPtr = ConfigCache::getInstance();
	bool16 result = kFalse;
	ConfigValue configValueObj= configCacheObjPtr->getConfigCacheByConfigName("CM_SHOW_KIT");	
	if(configValueObj.getConfigValue1() == "1")
	{
		result = kTrue;
	}
	return result;
}


bool16 AppFramework::ConfigCache_getDisplayItemXref()
{
	ConfigCache* configCacheObjPtr = ConfigCache::getInstance();
	bool16 result = kFalse;
	ConfigValue configValueObj = configCacheObjPtr->getConfigCacheByConfigName("CM_SHOW_XREF");
	if(configValueObj.getConfigValue1() == "1")
	{
		result = kTrue;
	}
	return result;
}

bool16 AppFramework::ConfigCache_getDisplayItemAccessory()
{
	ConfigCache* configCacheObjPtr = ConfigCache::getInstance();
	bool16 result = kFalse;
	ConfigValue configValueObj = configCacheObjPtr->getConfigCacheByConfigName("CM_SHOW_ACCESSORY");
	if(configValueObj.getConfigValue1() == "1")
	{
		result = kTrue;
	}
	return result;
}

int32 AppFramework::getPM_Project_Levels()
{
	ConfigCache* configCacheObjPtr = ConfigCache::getInstance();
	int32 result = 2;
	ConfigValue configValueObj = configCacheObjPtr->getConfigCacheByConfigName("PM_PROJECT_LEVELS");
	if((configValueObj.getConfigValue1() == "3") ||  (configValueObj.getConfigValue1() == "2"))
	{
		result = configValueObj.getConfigValue1().GetAsNumber();
	}
	
	return result;
}


CPubModel AppFramework::getpubModelByPubID(double PubID,double languageID)
{
	EventCache* eventCacheObjPtr = EventCache::getInstance();
	return eventCacheObjPtr->getpubModelByPubID(PubID, languageID);
}

CClassificationValue AppFramework::StructureCache_getCClassificationValueByClassId(double classId, double LanguageID)
{
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	return structureCacheObjPtr->StructureCache_getCClassificationValueByClassId(classId,LanguageID );
}

VectorPbObjectValuePtr AppFramework::getProductsAndItemsForSection(double curSecId, double langId , PMString itemAttributeId ,bool16 isChildDataRequire)
{
	VectorPbObjectValuePtr nodeVectorPtr = new VectorPbObjectValue;
    
    if(!isChildDataRequire)
        this->callSectionResultJson(curSecId,  langId, *nodeVectorPtr, itemAttributeId);
    else
        this->callSectionResultJsonWithChildItemDetails(curSecId,  langId, *nodeVectorPtr, itemAttributeId);

	return nodeVectorPtr;
}

double AppFramework::ConfigCache_getintConfigValue1ByConfigName(PMString configName)
{
	ConfigCache* configCacheObjPtr = ConfigCache::getInstance();
	return configCacheObjPtr->getintConfigValue1ByConfigName(configName);
}

PMString AppFramework::ConfigCache_getPMStringConfigValue1ByConfigName(PMString configName)
{
	ConfigCache* configCacheObjPtr = ConfigCache::getInstance();
	return configCacheObjPtr->getPMStringConfigValue1ByConfigName(configName);
}


VectorPubObjectValuePtr AppFramework::getItemsForSubSection(double subSecID, double LanguageID)
{
	VectorPbObjectValuePtr nodeVectorPtr = new VectorPbObjectValue;

	this->callSectionResultJson(subSecID,  LanguageID, *nodeVectorPtr);

	VectorPbObjectValuePtr itemNodeVectorPtr = new VectorPbObjectValue;

	if(nodeVectorPtr->size() > 0)
	{
		VectorPubObjectValue::iterator it;
		for(it = nodeVectorPtr->begin(); it != nodeVectorPtr->end(); it++)
		{
			if(it->getisProduct() == 0)
			{
				itemNodeVectorPtr->push_back(*it);
			}
		}
	}

	delete nodeVectorPtr;
	return itemNodeVectorPtr;
}
		
VectorPubObjectValuePtr AppFramework::getProductsForSubSection(double subSecID, double LanguageID)
{
	VectorPbObjectValuePtr nodeVectorPtr = new VectorPbObjectValue;

	this->callSectionResultJson(subSecID,  LanguageID, *nodeVectorPtr);

		VectorPbObjectValuePtr productNodeVectorPtr = new VectorPbObjectValue;

	if(nodeVectorPtr->size() > 0)
	{
		VectorPubObjectValue::iterator it;
		for(it = nodeVectorPtr->begin(); it != nodeVectorPtr->end(); it++)
		{
			if(it->getisProduct() == 1)
			{
				productNodeVectorPtr->push_back(*it);
			}
		}
	}

	delete nodeVectorPtr;
	return productNodeVectorPtr;
}

VectorPbObjectValuePtr AppFramework::getProductsAndItemsForSectionInDetail(double curSecId, double langId , PMString itemGroupIds, PMString itemIds, PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds, PMString listItemFieldIds,  bool16 isSprayItemPerFrameFlag, PMString itemAttributeGroupIds )
{
	VectorPbObjectValuePtr nodeVectorPtr = new VectorPbObjectValue;

	this->callSectionDetailResultJson(curSecId,  langId, *nodeVectorPtr , itemGroupIds, itemIds, itemFieldIds, itemAssetTypeIds, itemGroupFieldIds, itemGroupAssetTypeIds, listTypeIds, listItemFieldIds, isSprayItemPerFrameFlag, itemAttributeGroupIds);

	return nodeVectorPtr;
}

void AppFramework::clearAllStaticObjects()
{
	EventCache* eventCacheObjPtr = EventCache::getInstance();
	eventCacheObjPtr->clearCurrentSectionData();
	eventCacheObjPtr->clearCurrentObjectData();
	/*PartnerCache* parnerCacheObj = PartnerCache::getInstance();
	parnerCacheObj->clearInstace();*/
	
}

bool16 AppFramework::EventCache_setCurrentSectionData( double sectionId, double langId , PMString itemGroupIds, PMString itemIds, PMString itemFieldIds, 
	PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds , PMString listItemFieldIds ,
	bool16 isCategorySpecificResultCall, bool16 isSprayItemPerFrameFlag, PMString itemAttributeGroupIds)
{
	bool16 resultFlag = kTrue;
	EventCache* eventCacheObjPtr = EventCache::getInstance();
	resultFlag = eventCacheObjPtr->setCurrentSectionData(sectionId, langId, itemGroupIds, itemIds, itemFieldIds, itemAssetTypeIds, itemGroupFieldIds, 
		itemGroupAssetTypeIds, listTypeIds, listItemFieldIds, isCategorySpecificResultCall, isSprayItemPerFrameFlag, itemAttributeGroupIds);

	return resultFlag;
}		

void AppFramework::EventCache_clearCurrentSectionData()
{
	EventCache* eventCacheObjPtr = EventCache::getInstance();
	eventCacheObjPtr->clearCurrentSectionData();
	/*PartnerCache* parnerCacheObj = PartnerCache::getInstance();
	parnerCacheObj->clearInstace();*/
}

void AppFramework::EventCache_setCurrentObjectData(double sectionId, double ObjectId, int32 isProduct, double langId, PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds , PMString listItemFieldIds)
{
	EventCache* eventCacheObjPtr = EventCache::getInstance();
	eventCacheObjPtr->setCurrentObjectData(sectionId, ObjectId, isProduct, langId, itemFieldIds, itemAssetTypeIds, itemGroupFieldIds, itemGroupAssetTypeIds, listTypeIds, listItemFieldIds);
	/*PartnerCache* parnerCacheObj = PartnerCache::getInstance();
	parnerCacheObj->clearInstace();*/
}

void AppFramework::EventCache_clearCurrentObjectData()
{
	EventCache* eventCacheObjPtr = EventCache::getInstance();
	eventCacheObjPtr->clearCurrentObjectData();
}

VectorScreenTableInfoPtr AppFramework::GETProjectProduct_getAllScreenTablesBySectionidObjectid(double section_id, double object_id, double langId,  bool16 isCustomTable )
{
	VectorScreenTableInfoPtr itemGroupListPtr=  new VectorScreenTableInfoValue();
	int32 isProduct = 1; // getting screen table for ItemGroup.
	EventCache* eventCacheObjPtr = EventCache::getInstance();
	CPbObjectValue pbObjValue = eventCacheObjPtr->getPbObjectValueBySectionIdObjectId(section_id, object_id, isProduct, langId);
	//*itemGroupListPtr = pbObjValue.getObjectValue().getLists();

	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	
	for(int32 count = 0; count < pbObjValue.getObjectValue().getLists().size(); count++ )
	{			
		//if(structureCacheObjPtr->isWebListTableType(pbObjValue.getObjectValue().getLists().at(count).getTableTypeID()))  // check for Weblist
		//	continue;

		itemGroupListPtr->push_back(pbObjValue.getObjectValue().getLists().at(count));
	}

	return itemGroupListPtr;
}

VectorAdvanceTableScreenValuePtr AppFramework::getHybridTableData(double section_id , double object_id , double language_id, int32 isProduct, bool16 isSectionLevelHybridTable=0)
{
	VectorAdvanceTableScreenValuePtr hybridTableDataPtr=  new VectorAdvanceTableScreenValue();
	EventCache* eventCacheObjPtr = EventCache::getInstance();
	CPbObjectValue pbObjValue = eventCacheObjPtr->getPbObjectValueBySectionIdObjectId(section_id, object_id, isProduct, language_id);
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	
	if(isProduct == 1)
	{
		for(int32 count = 0; count < pbObjValue.getObjectValue().getAdvanceTables().size(); count++ )
		{				
			hybridTableDataPtr->push_back(pbObjValue.getObjectValue().getAdvanceTables().at(count));
		}
	}
	else if(isProduct == 0)
	{
		for(int32 count = 0; count < pbObjValue.getItemModel().getAdvanceTables().size(); count++ )
		{				
			hybridTableDataPtr->push_back(pbObjValue.getItemModel().getAdvanceTables().at(count));
		}
	}

	return hybridTableDataPtr;
}

PMString AppFramework::GETItem_GetItemAttributeDetailsByLanguageId(double ItemID,double AttributeID,double LanguageID, double sectionId, bool16 RefreshFlag)
{
	PMString resultValue("");
	int32 isProduct =0;
	EventCache* eventCacheObjPtr = EventCache::getInstance();
	CPbObjectValue pbObjValue = eventCacheObjPtr->getPbObjectValueBySectionIdObjectId(sectionId, ItemID, isProduct, LanguageID);
	CItemModel itemModelObj = pbObjValue.getItemModel();
	bool16 gotResultValueFlag = kFalse;
	if(itemModelObj.getValues().size() >0 )
	{
		for(int count =0; count < itemModelObj.getValues().size(); count++)
		{
			if(itemModelObj.getValues().at(count).getFieldId() == AttributeID)
			{
				resultValue = itemModelObj.getValues().at(count).getFieldValue();
				gotResultValueFlag = kTrue;
				break;
			}
		}
	}

	if(gotResultValueFlag == kFalse)
	{ // check if attribute is of Brand, Manufacturrer or Supplier/Vendor
		StructureCache* structureCacheObjPtr = StructureCache::getInstance();
		int32 isBMS = structureCacheObjPtr->isItemAttributeOfBMS(AttributeID);
		do
		{
			if((isBMS == 0) && (AttributeID != -225) && (AttributeID != -226))
				break;

			else if(isBMS == 1) // Brand Id
			{
				PartnerCache* partnerCachePtr = PartnerCache::getInstance();
				resultValue = partnerCachePtr->getPartnerNameByPartnerIdPartnerType(itemModelObj.getBrandID(), 1);				
			}
			else if(isBMS == 2) // Manufacturer Id
			{
				PartnerCache* partnerCachePtr = PartnerCache::getInstance();
				resultValue = partnerCachePtr->getPartnerNameByPartnerIdPartnerType(itemModelObj.getManufacturerID(), 2);				
			}
			else if(isBMS == 3 || AttributeID == -225 || AttributeID == -226) // Vendor/Supplier Id
			{
				PartnerCache* partnerCachePtr = PartnerCache::getInstance();
				
				if( AttributeID == -225)
				{
					resultValue = partnerCachePtr->getPartnerDescription1ByPartnerIdPartnerType(itemModelObj.getSupplierID(), 3);	
				}
				else if( AttributeID == -226)
				{
					resultValue = partnerCachePtr->getPartnerDescription2ByPartnerIdPartnerType(itemModelObj.getSupplierID(), 3);	
				}
				else
				{
					resultValue = partnerCachePtr->getPartnerNameByPartnerIdPartnerType(itemModelObj.getSupplierID(), 3);	
				}
			}
		}while(0);
	}
	return resultValue;
}

CObjectValue AppFramework::GETProduct_getObjectElementValue(double objectID, double sectionId, double LanguageId)
{	
	int32 isProduct =1;
	EventCache* eventCacheObjPtr = EventCache::getInstance();
	CPbObjectValue pbObjValue = eventCacheObjPtr->getPbObjectValueBySectionIdObjectId(sectionId, objectID, isProduct, LanguageId);
	return pbObjValue.getObjectValue();
}

PMString AppFramework::GETProduct_getAttributeValueByLanguageID(double ObjectID,double AttributeID,double LanguageId, double sectionId, bool16 IsFlag)
{
	int32 isProduct =1;
	PMString resultValue("");
	EventCache* eventCacheObjPtr = EventCache::getInstance();
	CPbObjectValue pbObjValue = eventCacheObjPtr->getPbObjectValueBySectionIdObjectId(sectionId, ObjectID, isProduct, LanguageId);
	CObjectValue objectValueObj = pbObjValue.getObjectValue();
	bool16 gotResultValueFlag = kFalse;
	if(objectValueObj.getObjectValues().size() >0 )
	{
		for(int count =0; count < objectValueObj.getObjectValues().size(); count++)
		{
			if(AttributeID == 0)
			{
				resultValue = objectValueObj.get_number();
				gotResultValueFlag = kTrue;
				break;
			}
			if(objectValueObj.getObjectValues().at(count).getElementId() == AttributeID)
			{
				resultValue = objectValueObj.getObjectValues().at(count).getObjectValue();
				gotResultValueFlag = kTrue;
				break;
			}
			
		}
	}

	if(gotResultValueFlag == kFalse)
	{ // check if attribute is of Brand, Manufacturrer or Supplier/Vendor
		StructureCache* structureCacheObjPtr = StructureCache::getInstance();
		int32 isBMS = structureCacheObjPtr->isProductElementOfBMS(AttributeID);
		do
		{
			if((isBMS == 0) && (AttributeID != -225) && (AttributeID != -226))
				break;

			else if(isBMS == 1) // Brand Id
			{
				PartnerCache* partnerCachePtr = PartnerCache::getInstance();
				resultValue = partnerCachePtr->getPartnerNameByPartnerIdPartnerType(objectValueObj.getBrandId(), 1);				
			}
			else if(isBMS == 2) // Manufacturer Id
			{
				PartnerCache* partnerCachePtr = PartnerCache::getInstance();
				resultValue = partnerCachePtr->getPartnerNameByPartnerIdPartnerType(objectValueObj.getManufacturerId(), 2);				
			}
			else if(isBMS == 3 || AttributeID == -225 || AttributeID == -226) // Vendor/Supplier Id
			{
				PartnerCache* partnerCachePtr = PartnerCache::getInstance();
				
				if( AttributeID == -225)
				{
					resultValue = partnerCachePtr->getPartnerDescription1ByPartnerIdPartnerType(objectValueObj.getVendor_id(), 3);	
				}
				else if( AttributeID == -226)
				{
					resultValue = partnerCachePtr->getPartnerDescription2ByPartnerIdPartnerType(objectValueObj.getVendor_id(), 3);	
				}
				else
				{
					resultValue = partnerCachePtr->getPartnerNameByPartnerIdPartnerType(objectValueObj.getVendor_id(), 3);	
				}
			}
		}while(0);
	}
	return resultValue;
}

PMString AppFramework::ATTRIBUTECache_getItemAttributeName(double AttributeID,double languageID)
{
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	Attribute AttrObj = structureCacheObjPtr->StructureCache_getItemAttributeModelByAttributeId(AttributeID);
	return AttrObj.getDisplayNameByLanguageId(languageID);
}

VectorScreenTableInfoPtr AppFramework::GETProjectProduct_getItemTablesByPubObjectId(double itemId, double sectionId, double langId)
{
	VectorScreenTableInfoPtr itemListPtr=  new VectorScreenTableInfoValue;
	int32 isProduct = 0; // getting screen table for ItemGroup.
	EventCache* eventCacheObjPtr = EventCache::getInstance();
	CPbObjectValue pbObjValue = eventCacheObjPtr->getPbObjectValueBySectionIdObjectId(sectionId, itemId, isProduct, langId);
	//*itemListPtr = pbObjValue.getItemModel().getLists();

	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	
	for(int32 count = 0; count < pbObjValue.getItemModel().getLists().size(); count++ )
	{			
		//if(structureCacheObjPtr->isWebListTableType(pbObjValue.getItemModel().getLists().at(count).getTableTypeID()))  // check for Weblist
		//	continue;

		itemListPtr->push_back(pbObjValue.getItemModel().getLists().at(count));
	}
	return itemListPtr;
}

double AppFramework::ConfigCache_getPubItemSalePriceTableType()
{
	ConfigCache* configCacheObjPtr = ConfigCache::getInstance();
	return configCacheObjPtr->getintConfigValue1ByConfigName("PUB_ITEM_SALE_PRICE_TABLE_TYPE");
}

double AppFramework::ConfigCache_getPubItemRegularPriceTableType()
{
	ConfigCache* configCacheObjPtr = ConfigCache::getInstance();
	return configCacheObjPtr->getintConfigValue1ByConfigName("PUB_ITEM_REGULAR_PRICE_TABLE_TYPE");
}

bool16 AppFramework::ElementCache_GetElementModelByElementID(double elementID,CElementModel & cElementModelObj, double languageId )
{
	bool16 result = kFalse;
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	VectorElementInfoPtr ElementInfoObj = structureCacheObjPtr->StructureCache_getItemGroupCopyAttributesByLanguageId(elementID);

	if(ElementInfoObj->size() > 0)
	{
		for(int count =0; count < ElementInfoObj->size(); count++ )
		{
			if(ElementInfoObj->at(count).getElementId() == elementID)
			{
				cElementModelObj = ElementInfoObj->at(count);
				result = kTrue;
				break;
			}
		}
	}
	return result;
}

bool16 AppFramework::ConfigCache_getSprayTableStructurePerItem()
{
	ConfigCache* configCacheObjPtr = ConfigCache::getInstance();
	if(configCacheObjPtr->getintConfigValue1ByConfigName("IDCS2D_SPRAY_TABLE_STRUCTURE_PER_ITEM") == 1)
	{
		return kTrue;
	}
	else
	{
		return kFalse;
	}
}

VectorLongIntPtr AppFramework::GETProjectProduct_getAllItemIDsFromTables(double ObjectID, double sectionId)
{
	VectorScreenTableInfoPtr itemGroupListPtr=  new VectorScreenTableInfoValue();
	VectorLongIntPtr allItemIdVector = new VectorLongIntValue();
	int32 isProduct = 1; // getting screen table for ItemGroup.'
	double langId = 91; // hardcoded to default not relevent here as we need only child Item Ids
	int32 flag=0;
	EventCache* eventCacheObjPtr = EventCache::getInstance();
	CPbObjectValue pbObjValue = eventCacheObjPtr->getPbObjectValueBySectionIdObjectId(sectionId, ObjectID, isProduct, langId);
	*itemGroupListPtr = pbObjValue.getObjectValue().getLists();
	

	if(itemGroupListPtr != NULL && itemGroupListPtr->size() > 0)
	{
		StructureCache* structureCacheObjPtr = StructureCache::getInstance();
		for(int count=0; count < itemGroupListPtr->size(); count++)
		{
			
			//if(structureCacheObjPtr->isWebListTableType(itemGroupListPtr->at(count).getTableTypeID()))  // check for Weblist
			//	continue;

			VectorLongIntValue itemList = itemGroupListPtr->at(count).getItemIds();

			if(itemList.size() > 0 )
			{
				for(int childCt = 0; childCt < itemList.size(); childCt++)
				{
					double rowid = itemList.at(childCt);
					if(allItemIdVector->size() != 0)
					{
						bool16 fLag = kFalse;
						VectorLongIntValue::iterator it2;
						for(it2=allItemIdVector->begin(); it2!=allItemIdVector->end(); it2++)
						{
							if(rowid == (*it2))
							{	fLag = kTrue;
								break;
							}
						}
						if(fLag)
							continue;
					}
					allItemIdVector->push_back(rowid);
					flag =1;
				}
			}
		}
	}

	if(flag==1)
	{
		delete itemGroupListPtr;
		return allItemIdVector;
	}
	else
	{
		delete itemGroupListPtr;
		delete allItemIdVector;
		return NULL;
	}
}


PMString AppFramework::PUBModel_getAttributeValueByLanguageID(double PubID,double AttributeID,double LanguageID,bool16 Isflag)
{
	PMString result("");
	EventCache* eventCacheObjPtr = EventCache::getInstance();
	EventCacheValue eventCacheObj = eventCacheObjPtr->getEventCacheValueByEventId(PubID);
	result = eventCacheObj.getObjectValueByAttributeIdLangId(AttributeID, LanguageID);
	return result;
}

// isProduct = 0 - Item Assets, 1- Item Group Assets, 5- Section/Event Assets
VectorAssetValuePtr AppFramework::GETAssets_GetAssetByParentAndType(double objectId, double sectionId, int32 isProduct, double typeId)
{
	VectorAssetValuePtr vectAssetPtr = NULL;
	double LanguageId = 91; // hardcoded not relevent here
	EventCache* eventCacheObjPtr = EventCache::getInstance();
	CPbObjectValue pbObjValue = eventCacheObjPtr->getPbObjectValueBySectionIdObjectId(sectionId, objectId, isProduct, LanguageId);
	
	VectorAssetValue vectAssetList;
	if(isProduct == 1)
	{
		CObjectValue objectValueObj = pbObjValue.getObjectValue();
		vectAssetList = objectValueObj.getAssets();
	}
	else if(isProduct == 0)
	{
		CItemModel itemModelObj = pbObjValue.getItemModel();
		vectAssetList = itemModelObj.getAssetList();
	}
	else if(isProduct == 5)
	{
		EventCache* eventCacheObjPtr = EventCache::getInstance();
		EventCacheValue eventCacheObj = eventCacheObjPtr->getEventCacheValueByEventId(objectId);
		vectAssetList = eventCacheObj.getAssets();
	}

	if(typeId == -1)
	{
		if(vectAssetList.size() == 0)
		{
			return NULL;
		}
		vectAssetPtr =  new VectorAssetValue;
		*vectAssetPtr = vectAssetList;
		return vectAssetPtr;
	}

	if(vectAssetList.size() > 0)
	{
		vectAssetPtr =  new VectorAssetValue;
		for(int count =0; count < vectAssetList.size(); count ++ )
		{
			if(vectAssetList.at(count).getType_id() == typeId)
			{
				vectAssetPtr->push_back((vectAssetList.at(count)));
			}
		}
	}
	return vectAssetPtr;
}

 CPbObjectValue AppFramework::getPbObjectValueBySectionIdObjectId(double sectionId, double ObjectId, int32 isProduct, double langId)
 {	
	EventCache* eventCacheObjPtr = EventCache::getInstance();
	CPbObjectValue result = eventCacheObjPtr->getPbObjectValueBySectionIdObjectId(sectionId, ObjectId, isProduct, langId);	
	return result;
 }

 PMString AppFramework::StructureCache_getListNameByTypeId(double typeId)
 {
	 StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	 return structureCacheObjPtr->StructureCache_getListNameByTypeId(typeId);
 }

 PMString AppFramework::StructureCache_TYPECACHE_getTypeNameById(double typeId)
 {
	 StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	 return structureCacheObjPtr->StructureCache_TYPECACHE_getTypeNameById(typeId);
 }

 double AppFramework::CONFIGCACHE_getElementIDForItemGroupDescription()
 {
	ConfigCache* configCacheObjPtr = ConfigCache::getInstance();
	return configCacheObjPtr->getintConfigValue1ByConfigName("ITEM_GROUP_DESCRIPTION_ATTRIBUTE");
 }

 double AppFramework::CONFIGCACHE_getItemDescriptionAttribute()
 {
	ConfigCache* configCacheObjPtr = ConfigCache::getInstance();
	return configCacheObjPtr->getintConfigValue1ByConfigName("ITEM_DESCRIPTION_ATTRIBUTE");
 }

 PMString AppFramework::getPrintSourcePropertiesPath()
 {
	PMString path;	
	FileUtils::GetAppFolder(&path);
    
    #ifdef MACINTOSH
    // Result path = Macintosh HD:Applications:Adobe InDesign CC Debug:Adobe InDesign CC.app:Contents:MacOS
    // so removing last 3 folder names as actual name is till Macintosh HD:Applications:Adobe InDesign CC Debug
     SDKUtilities::RemoveLastElement(path);
     SDKUtilities::RemoveLastElement(path);
     SDKUtilities::RemoveLastElement(path);
    #endif

	#ifdef MACINTOSH
		path+=":";
	#else
		path+="\\";
	#endif
		// appending folder name
		path+="Plug-ins";

		// appending '\'
	#ifdef MACINTOSH
		path+=":";
	#else
		path+="\\";
	#endif
		// appending file name
		path+="PRINTSource";
	#ifdef MACINTOSH
		path+=":";
	#else
		path+="\\";
	#endif
		path+="PrintsourceProperties.json";		
		//CA(path);
	#ifdef MACINTOSH
		this->getUnixPath(path);
	#else
			{}
	#endif

	return path;
 }


 VectorLoginInfoPtr AppFramework::getLoginInfoProperties()
{
	PMString path = getPrintSourcePropertiesPath();	

	std::string text;
	string tempString(path.GetPlatformString());
	char * filepathstr = const_cast<char *> (tempString.c_str());// GetFilePath();

	VectorLoginInfoPtr vectorLoginIfoValuePtr = new VectorLoginInfoValue;
	LoginInfoValue loginInfoValObj;

	FILE *file = fopen( filepathstr, "rb" );
	if ( !file )
      return vectorLoginIfoValuePtr; 

	
	fseek( file, 0, SEEK_END );
	long size = ftell( file );
	fseek( file, 0, SEEK_SET );
   
	char *buffer = new char[size+1];
	buffer[size] = 0;
	if ( fread( buffer, 1, size, file ) == (unsigned long)size )
      text = buffer;
	fclose( file );
	

	PMString loginInfoInput("");
	loginInfoInput.SetCString(buffer, PMString::kUnknownEncoding);
	loginInfoInput.SetTranslatable(kFalse);
	//LogDebug(loginInfoInput);
  
	delete[] buffer;

	

	jsonParser jsonPraserObj;
	jsonPraserObj.parseLoginProperties(loginInfoInput, vectorLoginIfoValuePtr);

	return vectorLoginIfoValuePtr;
}

PMString AppFramework::getAssetServerPath()
{
	return assetServerPath;
}

void AppFramework::setAssetServerPath(PMString str)
{
	assetServerPath = str;
}

void AppFramework::clearConfigCache()
{
	ConfigCache* configCacheObjPtr = ConfigCache::getInstance();
	configCacheObjPtr->clearInstace();
}

VectorAssetValuePtr AppFramework::GETAssets_GetPVMPVAssetByParentIdAndAttributeId(double ParentId,double AttributeId, double sectionId, double LangaugeId, int32 objectType, double pvGroupId, int32 PVImageindex)
{
	VectorAssetValuePtr vectAssetPtr = NULL;	
	EventCache* eventCacheObjPtr = EventCache::getInstance();
	CPbObjectValue pbObjValue = eventCacheObjPtr->getPbObjectValueBySectionIdObjectId(sectionId, ParentId, objectType, LangaugeId);
	PMString picListGroupIdstr("");

	if(objectType == 0)
	{
		CItemModel itemModelObj = pbObjValue.getItemModel();
		if(itemModelObj.getValues().size() >0 )
		{
			for(int count =0; count < itemModelObj.getValues().size(); count++)
			{
				if(itemModelObj.getValues().at(count).getFieldId() == AttributeId)
				{
					picListGroupIdstr = itemModelObj.getValues().at(count).getPicklistValueIdsString();
					break;
				}
			}
		}
	}
	else if(objectType == 1)
	{
		CObjectValue objectValueObj = pbObjValue.getObjectValue();
		if(objectValueObj.getObjectValues().size() >0 )
		{
			for(int count =0; count < objectValueObj.getObjectValues().size(); count++)
			{				
				if(objectValueObj.getObjectValues().at(count).getElementId() == AttributeId)
				{
					picListGroupIdstr = objectValueObj.getObjectValues().at(count).getPicklistValueIdsString();
					break;
				}			
			}
		}
	}
	
	if(picListGroupIdstr.NumUTF16TextChars() == 0)
	{
		return vectAssetPtr;
	}
	
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	PicklistGroup picListGroupObj = structureCacheObjPtr->StructureCache_getPickListGroups(pvGroupId);
	vectAssetPtr =  new VectorAssetValue;
	vector<PicklistValue> picklistValues = picListGroupObj.getPicklistValues();

	for(int32 j=0; j < picklistValues.size(); j++)
	{
		PicklistValue pickListValObj = picklistValues.at(j);	
		PMString ValueStr("");
		ValueStr.AppendNumber(PMReal(pickListValObj.getValueId()));

		if(picListGroupIdstr.Contains(ValueStr) )
		{
			CAssetValue pcAssetRecordObj;
			pcAssetRecordObj.setMpv_value_id(pickListValObj.getValueId());

			if(PVImageindex == 1)
				pcAssetRecordObj.setUrl(pickListValObj.getImageFileName1());
			else if(PVImageindex == 2)
				pcAssetRecordObj.setUrl(pickListValObj.getImageFileName2());
			else if(PVImageindex == 3)
				pcAssetRecordObj.setUrl(pickListValObj.getImageFileName3());
			else if(PVImageindex == 4)
				pcAssetRecordObj.setUrl(pickListValObj.getImageFileName4());
			else if(PVImageindex == 5)
				pcAssetRecordObj.setUrl(pickListValObj.getImageFileName5());

			pcAssetRecordObj.setMissing(0);
			pcAssetRecordObj.setDescription(pickListValObj.getDescription());
            pcAssetRecordObj.setPickListValue(pickListValObj.getPossibleValue());
            pcAssetRecordObj.setAbbreviation(pickListValObj.getAbbreviation());
			
			vectAssetPtr->push_back(pcAssetRecordObj);

		}
	}

	return vectAssetPtr;
}

bool16 AppFramework::LOGINMngr_updateOrCreateNewServerProperties(LoginInfoValue oServerInfo, int updateflag, double clientID)
{
	PMString path = getPrintSourcePropertiesPath();	
	
	VectorLoginInfoPtr vectorServerInfoPtr = getLoginInfoProperties();

	if(vectorServerInfoPtr->size() > 0)
	{
		if(updateflag == 0)
		{
			vectorServerInfoPtr->push_back(oServerInfo);
		}
		else
		{
			for(int count=0; count < vectorServerInfoPtr->size(); count++ )
			{
				if(vectorServerInfoPtr->at(count).getEnvName() == oServerInfo.getEnvName())
				{
					vectorServerInfoPtr->at(count).setEnvName(oServerInfo.getEnvName());
					vectorServerInfoPtr->at(count).setCmurl(oServerInfo.getCmurl());
					vectorServerInfoPtr->at(count).setUsername(oServerInfo.getUsername());
					vectorServerInfoPtr->at(count).setPassword(oServerInfo.getPassword());
					vectorServerInfoPtr->at(count).setVendorName(oServerInfo.getVendorName());
					vectorServerInfoPtr->at(count).setVersion(oServerInfo.getVersion());
					vectorServerInfoPtr->at(count).setServerName(oServerInfo.getServerName());
					vectorServerInfoPtr->at(count).setServerPort(oServerInfo.getServerPort());
					vectorServerInfoPtr->at(count).setSchema(oServerInfo.getSchema());
					vectorServerInfoPtr->at(count).setDbUserName(oServerInfo.getDbUserName());
					vectorServerInfoPtr->at(count).setDbPassword(oServerInfo.getDbPassword());

					PMString clientno("");
					clientno.AppendNumber(PMReal(clientID));
					//LogDebug(clientno);
					if(clientID != -1 && clientno != "")
					{
						//LogDebug("clientID != -1 && clientno != ");
						if(vectorServerInfoPtr->at(count).getClientList().size() > 0)
						{
							//LogDebug("getClientList().size() > 0");
							vector<ClientInfoValue> vectClientList = vectorServerInfoPtr->at(count).getClientList();
							bool flagClinetInfo = false;
							for(int i=0; i < vectClientList.size(); i++)
							{
								if(clientno == vectClientList.at(i).getClientno())
								{
									ClientInfoValue clientInfoObj = oServerInfo.getClientInfoByClientNo(clientno);

									if(clientInfoObj.getClientno() == "")
										vectClientList.at(i).setClientno(clientno);
									else
										vectClientList.at(i).setClientno(clientInfoObj.getClientno());
									vectClientList.at(i).setAssetserverpath(clientInfoObj.getAssetserverpath());
									vectClientList.at(i).setDocumentPath(clientInfoObj.getDocumentPath());
									vectClientList.at(i).setLanguage(clientInfoObj.getLanguage());
									vectClientList.at(i).setProject(clientInfoObj.getProject());
									vectClientList.at(i).setIsActive(clientInfoObj.getIsActive());
									vectClientList.at(i).setisItemTablesasTabbedText(clientInfoObj.getisItemTablesasTabbedText());
									vectClientList.at(i).setisAddTableHeaders(clientInfoObj.getisAddTableHeaders());
									vectClientList.at(i).setAssetStatus(clientInfoObj.getAssetStatus());
									vectClientList.at(i).setLogLevel(clientInfoObj.getLogLevel());
									vectClientList.at(i).setMissingFlag(clientInfoObj.getMissingFlag());
									vectClientList.at(i).setSectionID(clientInfoObj.getSectionID());
									vectClientList.at(i).setIsONESource(clientInfoObj.getIsONESource());
									vectClientList.at(i).setdefaultFlow(clientInfoObj.getdefaultFlow());
									vectClientList.at(i).setDisplayPartnerImages(clientInfoObj.getDisplayPartnerImages());
									vectClientList.at(i).setDisplayPickListImages(clientInfoObj.getDisplayPickListImages());
									vectClientList.at(i).setShowObjectCountFlag(clientInfoObj.getShowObjectCountFlag());
									vectClientList.at(i).setByPassForSingleSelectionSprayFlag(clientInfoObj.getByPassForSingleSelectionSprayFlag());
									vectClientList.at(i).setHorizontalSpacing(clientInfoObj.getHorizontalSpacing());
									vectClientList.at(i).setVerticalSpacing(clientInfoObj.getVerticalSpacing());

									flagClinetInfo = true;
									break;
								}
							}

							if(!flagClinetInfo)
							{
								//LogDebug("!flagClinetInfo");
								vectClientList.push_back(oServerInfo.getClientInfoByClientNo(clientno));
							}
							vectorServerInfoPtr->at(count).setClientList(vectClientList);
						}
						else if(vectorServerInfoPtr->at(count).getClientList().size() == 0)
						{
							//LogDebug("getClientList().size() == 0");
							if(oServerInfo.getClientList().size() == 0)
								{
									//LogDebug("ZZZZZZZZZZZZZZZZZZZ");
									ClientInfoValue clinetInfoObj;
									clinetInfoObj.setClientno(clientno);
									vector<ClientInfoValue> list;
									list.push_back(clinetInfoObj);
									oServerInfo.setClientList(list);
								}
							vectorServerInfoPtr->at(count).setClientList(oServerInfo.getClientList());
						}
					}
					
				}			
			}
		}
	}
	else if(vectorServerInfoPtr->size() == 0)
	{
		vectorServerInfoPtr->push_back(oServerInfo);
	}

	jsonParser jsonPraserObj;
	jsonPraserObj.writeLoginProperties(path, vectorServerInfoPtr);

	if(vectorServerInfoPtr != NULL)
		delete vectorServerInfoPtr;

	return kTrue;
}

bool16 AppFramework::LOGINMngr_deleteUser(char* envName, double ClientId)
{
	PMString path = getPrintSourcePropertiesPath();	
	PMString envNameStr(envName);
	
	VectorLoginInfoPtr vectorServerInfoPtr = getLoginInfoProperties();

	VectorLoginInfoPtr newVectorServerInfoPtr = new VectorLoginInfoValue;

	if(vectorServerInfoPtr->size() > 0)
	{
		for(int count=0; count < vectorServerInfoPtr->size(); count++ )
		{
			if(vectorServerInfoPtr->at(count).getEnvName() != envNameStr)
			{
				newVectorServerInfoPtr->push_back(vectorServerInfoPtr->at(count));
			}
		}
	}

	jsonParser jsonPraserObj;
	jsonPraserObj.writeLoginProperties(path, newVectorServerInfoPtr);

	if(vectorServerInfoPtr != NULL)
		delete vectorServerInfoPtr;

	if(newVectorServerInfoPtr != NULL)
		delete newVectorServerInfoPtr;

	return kTrue;
}

void AppFramework::LOGINMngr_setServerInfoValue(LoginInfoValue ServerConfigValue)
{
	this->currentServerConfigValue = ServerConfigValue;
}


bool16 AppFramework::LOGINMngr_logoutCurrentUser()
{
	bool16 result = kFalse;
	PMString LogOutResult("");
	result = this->callLogout(LogOutResult);
	
	return result;
}

void AppFramework::setClientID(double clientid)
{
	clientID = clientid;
}
double AppFramework::getClientID()
{
	return clientID;
}

int32 AppFramework::getAssetServerOption()
{
	return AssetServerOption;
}

void AppFramework::setAssetServerOption(int32 Option)
{	
	this->AssetServerOption = Option;
}

void AppFramework::setMissingFlag(bool16 status)
{
	this->MissingFlag = status;
}

bool16 AppFramework::getMissingFlag()
{
	return this->MissingFlag;
}

void AppFramework::setSelectedEnvName(PMString name)
{
	selectedEnvName = name;
}

PMString AppFramework::getSelectedEnvName()
{
	return selectedEnvName;
}

CAssetValue AppFramework::getPartnerAssetValueByPartnerIdImageTypeId(double objectId, double sectionId, double languageId, int32 imageTypeId, int32 whichTab)
{
	CAssetValue resultValue;
	int32 isProduct = 0;
	if(whichTab == 3)
		isProduct =1;
	else if(whichTab == 4)
		isProduct = 0;

	double partnerId = -1;

	EventCache* eventCacheObjPtr = EventCache::getInstance();
	CPbObjectValue pbObjValue = eventCacheObjPtr->getPbObjectValueBySectionIdObjectId(sectionId, objectId, isProduct, languageId);
	if(whichTab == 3)
	{
		CObjectValue objectValueObj = pbObjValue.getObjectValue();
		if( imageTypeId == -207 || imageTypeId == -208 || imageTypeId == -209 || imageTypeId == -210 || imageTypeId == -211 )
		{
			partnerId = objectValueObj.getBrandId();
		}
		else if( imageTypeId == -212 || imageTypeId == -213 || imageTypeId == -214 || imageTypeId == -215 || imageTypeId == -216 )
		{
			partnerId = objectValueObj.getManufacturerId();
		}
		else if( imageTypeId == -217 || imageTypeId == -218 || imageTypeId == -219 || imageTypeId == -220 || imageTypeId == -221 )
		{
			partnerId = objectValueObj.getVendor_id();
		}

	}
	else if(whichTab == 4)
	{
		CItemModel itemModelObj = pbObjValue.getItemModel();
		if( imageTypeId == -207 || imageTypeId == -208 || imageTypeId == -209 || imageTypeId == -210 || imageTypeId == -211 )
		{
			partnerId = itemModelObj.getBrandID();
		}
		else if( imageTypeId == -212 || imageTypeId == -213 || imageTypeId == -214 || imageTypeId == -215 || imageTypeId == -216 )
		{
			partnerId = itemModelObj.getManufacturerID();
		}
		else if( imageTypeId == -217 || imageTypeId == -218 || imageTypeId == -219 || imageTypeId == -220 || imageTypeId == -221 )
		{
			partnerId = itemModelObj.getSupplierID();
		}
	}

	PartnerCache* partnerCachePtr = PartnerCache::getInstance();
	resultValue = partnerCachePtr->getPartnerAssetValueByPartnerIdImageTypeId(partnerId, imageTypeId);	

	return resultValue;

}

bool16 AppFramework::getIndexTerms(set<PMString> resultIds, vector<IndexReference>& indexReferences, PMString primaryIndexs, PMString secondaryIndexes, PMString tertioryIndexes)
{
	//vector<IndexReference> returndata;
	callIndexTermJson(resultIds, indexReferences , primaryIndexs, secondaryIndexes, tertioryIndexes);

	return kTrue;
}


bool16 AppFramework::callIndexTermJson(set<PMString> resultIds, vector<IndexReference>& indexReferences, PMString primaryIndexs, PMString secondaryIndexes, PMString tertioryIndexes)
{
	bool16 errorCode;
	PMString jSonURL("");
	//PMString postFields("");
	if(jsessionID == "")
	{
		return kFalse;
	}
		
	jSonURL.Append(SelectedServerURL);
	jSonURL.Append("/event/section/indexTerms.json;jsessionid="); 
	jSonURL.Append(jsessionID);	

	// ** DON TERMS ** //
	//jSonURL.Append("?primaryIndexFieldIds=g635,,");
	//jSonURL.Append("&secondaryIndexFieldIds=g636,,");
	//jSonURL.Append("&tertiaryIndexFieldIds=g637,,");

	//// ** ADS TERMS ** //
	//jSonURL.Append("?primaryIndexFieldIds=g362,g431,");
	//jSonURL.Append("&secondaryIndexFieldIds=,,");
	//jSonURL.Append("&tertiaryIndexFieldIds=,,");


	//jSonURL.Append("?primaryIndexFieldIds=l3,g427,i11001171");
	//jSonURL.Append("?primaryIndexFieldIds=l2,l3,l4");
	//postFields.Append("primaryIndexFieldIds=g635");


	jSonURL.Append("?primaryIndexFieldIds=");
	jSonURL.Append(primaryIndexs);
	jSonURL.Append("&secondaryIndexFieldIds=");
	jSonURL.Append(secondaryIndexes);
	jSonURL.Append("&tertiaryIndexFieldIds=");
	jSonURL.Append(tertioryIndexes);

	jSonURL.Append("&resultIds=");

	//for (set<PMString>::iterator i = resultIds.begin(); i != resultIds.end(); i++)
	//{
	//}

	vector<IndexReference> tempIndexReferences;
	// outer loop re-verifies resultIds set is empty
	while (resultIds.size() > 0) 
	{
		// this inner loop creates all of the index server URLs
		// break when URL > 2000 chars
		PMString tempUrl = jSonURL;
		bool longUrl = false;
		while (resultIds.size() > 0) 
		{
			PMString resultId = *resultIds.begin();
			tempUrl.Append(resultId);
			tempUrl.Append(",");

			if (tempUrl.CharCount() > 2000)
			{
				tempUrl.Remove(tempUrl.CharCount() - 2 - resultId.CharCount(), resultId.CharCount() + 2);
				longUrl = true;
				break;
			}
			else
				resultIds.erase(resultId);
		}

		if (!longUrl)
			tempUrl.Remove(tempUrl.CharCount() - 1, 1);

		PMString ASD("IndexTermjSonURL: " + tempUrl);
		this->LogDebug(ASD);

		PMString jsonServiceOutput("");
		errorCode = callService(tempUrl, jsonServiceOutput);
		//this->LogDebug(jsonServiceOutput);

		if(!errorCode)
		{
			CA("callItemDetailJson Service Call Failed");
			break;
		}
		else
		{		
			jsonParser jsonPraserObj;
			jsonPraserObj.parseIndexReferences(jsonServiceOutput, tempIndexReferences);
		}
	}

	//////// *********** This is when you need to take the indexReferences vector and shrink it down so all dupes are removed
	//////// *********** You will need to add indexTerms2 and indexTerms3 to the C side model object
	//////// *********** Then when doing TagDocument(), you will have to add all three indexterm Lists (if populated
	for (vector<IndexReference>::iterator i = tempIndexReferences.begin(); i != tempIndexReferences.end(); i++)
	{
		IndexReference tempIndexReference = *i;

		bool found = false;
		for (vector<IndexReference>::iterator j = indexReferences.begin(); j != indexReferences.end(); j++)
		{
			IndexReference indexReference = *j;
			if (tempIndexReference.getSectionID() == indexReference.getSectionID() &&
				tempIndexReference.getParentTypeID() == indexReference.getParentTypeID() &&
				tempIndexReference.getParentID() == indexReference.getParentID() &&
				tempIndexReference.getChildID() == indexReference.getChildID())
			{
				if (indexReference.getIndexTerms2().empty())
				{
					indexReference.setIndexTerms2(tempIndexReference.getIndexTerms());
					indexReferences.erase(j);
					indexReferences.push_back(indexReference);
				}
				else if (indexReference.getIndexTerms3().empty())
				{
					indexReference.setIndexTerms3(tempIndexReference.getIndexTerms());
					indexReferences.erase(j);
					indexReferences.push_back(indexReference);
				}

				found = true;
				break;
			}
		}

		if (!found)
			indexReferences.push_back(tempIndexReference);
	}

	return errorCode;
}


Attribute AppFramework::ATTRIBUTECache_getItemAttributeForIndex(int32 option, double languageID)
{
	Attribute attibuteModelObj;
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	ConfigCache* configCacheObjPtr = ConfigCache::getInstance();

	switch(option)
	{
		case 1:
			{	//unique key Item attribute.
				double uniqueKeyAttrId = structureCacheObjPtr->StructureCache_getUniqueItemCopyAttributeId();
				attibuteModelObj = structureCacheObjPtr->StructureCache_getItemAttributeModelByAttributeId(uniqueKeyAttrId);
				break;
			}
		case 2:
			{	// 				
				double itemDesriptionAttrId = configCacheObjPtr->getintConfigValue1ByConfigName("ITEM_DESCRIPTION_ATTRIBUTE");
				if(itemDesriptionAttrId != -1)
					attibuteModelObj = structureCacheObjPtr->StructureCache_getItemAttributeModelByAttributeId(itemDesriptionAttrId);
				break;
			}
		case 3:
			{	// Brand Attribute Id
				attibuteModelObj = structureCacheObjPtr->getBMSAttributeForItem(1);			

				break;
			}
		case 4:
			{	// Manufacturer Attribute Id
				attibuteModelObj = structureCacheObjPtr->getBMSAttributeForItem(2);				
				break;
			}
		case 5:
			{ // Supplier Attribute Id
				attibuteModelObj = structureCacheObjPtr->getBMSAttributeForItem(3);
				break;
			}
		case 6:
			{	// ItemDescriptionAttribute
				double itemDesriptionAttrId = configCacheObjPtr->getintConfigValue1ByConfigName("ITEM_DESCRIPTION_ATTRIBUTE");
				if(itemDesriptionAttrId != -1)
					attibuteModelObj = structureCacheObjPtr->StructureCache_getItemAttributeModelByAttributeId(itemDesriptionAttrId);
				break;
			}
	}
	return attibuteModelObj;
}	



/**
URL:
/cf-cm2/category/specificResults.json

Parameters:
itemIds
itemFieldIds
itemAssetTypeIds
itemGroupIds
itemGroupFieldIds
itemGroupAssetTypeIds
includeMissingAssets
printActive=true
languageId

// URL requires the itemIds and itemGroupIds parameters to both be sent over.  If you don't use one, just send it as itemIds=& or itemGroupIds=&
*/

bool16 AppFramework::callCategorySpecificResultJson( double langId, vector<CPbObjectValue> &returndata , PMString itemGroupIds, PMString itemIds , PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds)
{
	PMString jSonURL("");
	if(jsessionID == "")
	{
		//CA("jsessionID == """);
		return kFalse;
	}
	
	//https://prd02.apsiva.net/cf-cm2/category/specificResults.json;jsessionid=8D3913339C091CCD459C27F748452240.worker1
	bool FirstAttributeflag = false;

	jSonURL.Append(SelectedServerURL); //https://prd02.apsiva.net/cf-cm
	jSonURL.Append("/category/");
	jSonURL.Append("specificResults.json;jsessionid="); //?indesign=true
	jSonURL.Append(jsessionID);

	//jSonURL.Append("?printActive=true");
	jSonURL.Append("?languageId=");
	jSonURL.AppendNumber(PMReal(langId));
	
	if( itemGroupIds == "" )
	{		
		jSonURL.Append("&itemGroupIds=");		
	}
	else if( itemGroupIds != "" )
	{		
		jSonURL.Append("&itemGroupIds=");
		jSonURL.Append(itemGroupIds);		
	}
	
	if( itemIds == "" )
	{		
		jSonURL.Append("&itemIds=");			
	}
	if( itemIds != "" )
	{		
		jSonURL.Append("&itemIds=");
		jSonURL.Append(itemIds);		
	}
	if( itemFieldIds != "" )
	{		
		jSonURL.Append("&itemFieldIds=");
		jSonURL.Append(itemFieldIds);		
	}
	if( itemAssetTypeIds != "" )
	{
		jSonURL.Append("&");
		jSonURL.Append("itemAssetTypeIds=");
		jSonURL.Append(itemAssetTypeIds);
	}
	if( itemGroupFieldIds != "" )
	{
		jSonURL.Append("&");
		jSonURL.Append("itemGroupFieldIds=");
		jSonURL.Append(itemGroupFieldIds);
	}
	if( itemGroupAssetTypeIds != "" )
	{
		jSonURL.Append("&");
		jSonURL.Append("itemGroupAssetTypeIds=");
		jSonURL.Append(itemGroupAssetTypeIds);
	}
	if( this->getMissingFlag() == kTrue )
	{
		jSonURL.Append("&");
		jSonURL.Append("includeMissingAssets=");
		jSonURL.Append("true");
	}
	else
	{
		jSonURL.Append("&");
		jSonURL.Append("includeMissingAssets=");
		jSonURL.Append("false");
	}
	
	PMString ASD("CategorySpecificResultJsonURL: " + jSonURL);
	this->LogDebug(ASD);

	PMString jsonServiceOutput("");
	bool16 errorCode = callService(jSonURL, jsonServiceOutput);
	if(!errorCode)
	{
		CA("callCategorySpecificResultJson Service Call Failed");
		this->LogError("callCategorySpecificResultJson Service Call Failed for " );
		this->LogError( jSonURL.GetPlatformString().c_str() );
	}
	else
	{		
		jsonParser jsonPraserObj;
		jsonPraserObj.parseSectionResult(jsonServiceOutput, returndata, /*sectionId*/-1);  // For Category Specific Result sectionId not required.
	}
	return errorCode;
}


VectorPbObjectValuePtr AppFramework::getProductsAndItemsForCategorySpecificResult( double langId , PMString itemGroupIds, PMString itemIds, PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds )
{
	VectorPbObjectValuePtr nodeVectorPtr = new VectorPbObjectValue;
	this->callCategorySpecificResultJson( langId, *nodeVectorPtr , itemGroupIds, itemIds , itemFieldIds, itemAssetTypeIds, itemGroupFieldIds, itemGroupAssetTypeIds);

	return nodeVectorPtr;
}

bool16 AppFramework::getRefreshData(set<PMString> resultIds, vector<IndexReference>& updatedValues, WideString newLanguageId, PMString itemFieldIds, PMString itemGroupFieldIds, PMString sectionFieldIds, PMString itemGroupListIds, PMString itemListIds)
{
	callRefreshDataJson(resultIds, updatedValues, newLanguageId, itemFieldIds, itemGroupFieldIds, sectionFieldIds, itemGroupListIds, itemListIds);

	return kTrue;
}

bool16 AppFramework::callRefreshDataJson(set<PMString> resultIds, vector<IndexReference>& updatedValues, WideString newLanguageId, PMString itemFieldIds, PMString itemGroupFieldIds, PMString sectionFieldIds, PMString itemGroupListIds, PMString itemListIds)
{
	bool16 errorCode;
	PMString jSonURL("");
	if(jsessionID == "")
	{
		return kFalse;
	}

	jSonURL.Append(SelectedServerURL);
	jSonURL.Append("/category/refreshData.json;jsessionid="); 
	jSonURL.Append(jsessionID);	

	jSonURL.Append("?itemFieldIds=");
	jSonURL.Append(itemFieldIds);
	jSonURL.Append("&itemGroupFieldIds=");
	jSonURL.Append(itemGroupFieldIds);
	jSonURL.Append("&sectionFieldIds=");
	jSonURL.Append(sectionFieldIds);
	jSonURL.Append("&itemGroupListIds=");
	jSonURL.Append(itemGroupListIds);
	jSonURL.Append("&itemListIds=");
	jSonURL.Append(itemListIds);
	jSonURL.Append("&languageId=");
	jSonURL.Append(newLanguageId);

	jSonURL.Append("&resultIds=");

	vector<IndexReference> refreshDataValues;
	// outer loop re-verifies resultIds set is empty
	while (resultIds.size() > 0) 
	{
		// this inner loop creates all of the index server URLs
		// break when URL > 2000 chars
		PMString tempUrl = jSonURL;
		bool longUrl = false;
		while (resultIds.size() > 0) 
		{
			PMString resultId = *resultIds.begin();
			tempUrl.Append(resultId);
			tempUrl.Append(",");

			if (tempUrl.CharCount() > 2000)
			{
				tempUrl.Remove(tempUrl.CharCount() - 2 - resultId.CharCount(), resultId.CharCount() + 2);
				longUrl = true;
				break;
			}
			else
				resultIds.erase(resultId);
		}

		if (!longUrl)
			tempUrl.Remove(tempUrl.CharCount() - 1, 1);

		PMString ASD("callRefreshDataJson: " + tempUrl);
		this->LogDebug(ASD);

		PMString jsonServiceOutput("");
		errorCode = callService(tempUrl, jsonServiceOutput);
		//this->LogDebug(jsonServiceOutput);

		if(!errorCode)
		{
			//CA("callRefreshDataJson Service Call Failed");
            this->LogError("callRefreshDataJson Service Call Failed");
			break;
		}
		else
		{		
			jsonParser jsonPraserObj;
			jsonPraserObj.parseIndexReferences(jsonServiceOutput, refreshDataValues);
		}
	}

	//////// *********** This is when you need to take the updatedValues vector and shrink it down so all dupes are removed
	for (vector<IndexReference>::iterator i = refreshDataValues.begin(); i != refreshDataValues.end(); i++)
	{
		IndexReference refreshDataValue = *i;

		bool found = false;
		for (vector<IndexReference>::iterator j = updatedValues.begin(); j != updatedValues.end(); j++)
		{
			IndexReference indexReference = *j;
			if (refreshDataValue.getParentTypeID() == indexReference.getParentTypeID() &&
				refreshDataValue.getParentID() == indexReference.getParentID() &&
				refreshDataValue.getNewFieldID() == indexReference.getNewFieldID() &&
				refreshDataValue.getTableId() == indexReference.getTableId())
			{
				found = true;
				break;
			}
		}

		if (!found)
			updatedValues.push_back(refreshDataValue);
	}

	return errorCode;
}

bool16 AppFramework::getFieldSwapValues(set<PMString> resultIds, vector<IndexReference>& fieldSwapValues, WideString newLanguageId, PMString resultType, double oldFieldId, double newFieldId, double eventId)
{
	callFieldSwapValuesJson(resultIds, fieldSwapValues, newLanguageId, resultType, oldFieldId, newFieldId, eventId );
	return kTrue;
}

bool16 AppFramework::callFieldSwapValuesJson(set<PMString> resultIds, vector<IndexReference>& fieldSwapValues, WideString newLanguageId, PMString resultType, double oldFieldId, double newFieldId, double eventId )
{
	bool16 errorCode;
	PMString jSonURL("");
	if(jsessionID == "")
	{
		return kFalse;
	}
		
	jSonURL.Append(SelectedServerURL);
	jSonURL.Append("/category/fieldSwap.json;jsessionid="); 
	jSonURL.Append(jsessionID);	

	// ** DON TERMS ** //
	jSonURL.Append("?fieldParentType=");
	jSonURL.Append(resultType);
	jSonURL.Append("&oldFieldId=");
	jSonURL.AppendNumber(PMReal(oldFieldId));
	jSonURL.Append("&newFieldId=");
	jSonURL.AppendNumber(PMReal(newFieldId));
	jSonURL.Append("&languageId=");
	jSonURL.Append(newLanguageId);
    
    if(eventId != -1)
    {
        jSonURL.Append("&eventId=");
        jSonURL.AppendNumber(PMReal(eventId));
    }
	

	//// ** ADS TERMS ** //
	//jSonURL.Append("?primaryIndexFieldIds=g362,g431,");
	//jSonURL.Append("&secondaryIndexFieldIds=,,");
	//jSonURL.Append("&tertiaryIndexFieldIds=,,");


	//jSonURL.Append("?primaryIndexFieldIds=l3,g427,i11001171");
	//jSonURL.Append("?primaryIndexFieldIds=l2,l3,l4");
	//postFields.Append("primaryIndexFieldIds=g635");

	jSonURL.Append("&resultIds=");

	//for (set<PMString>::iterator i = resultIds.begin(); i != resultIds.end(); i++)
	//{
	//}

	vector<IndexReference> tempFieldSwapValues;
	// outer loop re-verifies resultIds set is empty
	while (resultIds.size() > 0) 
	{
		// this inner loop creates all of the index server URLs
		// break when URL > 2000 chars
		PMString tempUrl = jSonURL;
		bool longUrl = false;
		while (resultIds.size() > 0) 
		{
			PMString resultId = *resultIds.begin();
			tempUrl.Append(resultId);
			tempUrl.Append(",");

			if (tempUrl.CharCount() > 2000)
			{
				tempUrl.Remove(tempUrl.CharCount() - 2 - resultId.CharCount(), resultId.CharCount() + 2);
				longUrl = true;
				break;
			}
			else
				resultIds.erase(resultId);
		}

		if (!longUrl)
			tempUrl.Remove(tempUrl.CharCount() - 1, 1);

		PMString ASD("FieldSwapJsonURL: " + tempUrl);
		this->LogDebug(ASD);

		PMString jsonServiceOutput("");
		errorCode = callService(tempUrl, jsonServiceOutput);
		//this->LogDebug(jsonServiceOutput);

		if(!errorCode)
		{
			//CA("callFieldSwapValuesJson Service Call Failed");
            this->LogError("callFieldSwapValuesJson Service Call Failed");
			break;
		}
		else
		{		
			jsonParser jsonPraserObj;
			jsonPraserObj.parseIndexReferences(jsonServiceOutput, tempFieldSwapValues);
		}
	}

	//////// *********** This is when you need to take the fieldSwapValues vector and shrink it down so all dupes are removed
	for (vector<IndexReference>::iterator i = tempFieldSwapValues.begin(); i != tempFieldSwapValues.end(); i++)
	{
		IndexReference tempFieldSwap = *i;

		bool found = false;
		for (vector<IndexReference>::iterator j = fieldSwapValues.begin(); j != fieldSwapValues.end(); j++)
		{
			IndexReference indexReference = *j;
			if (tempFieldSwap.getParentTypeID() == indexReference.getParentTypeID() &&
				tempFieldSwap.getParentID() == indexReference.getParentID() &&
				tempFieldSwap.getNewFieldID() == indexReference.getNewFieldID())
			{
				found = true;
				break;
			}
		}

		if (!found)
			fieldSwapValues.push_back(tempFieldSwap);
	}

	return errorCode;
}


VectorAttributeInfoPtr AppFramework::StructureCache_getAllItemAttributes(double LanguageID)
{
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	return structureCacheObjPtr->StructureCache_getAllItemAttributes(LanguageID);

}


PicklistGroup AppFramework::StructureCache_getPickListGroups(double pickListGroupId)
{
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	return structureCacheObjPtr->StructureCache_getPickListGroups(pickListGroupId);

}

double AppFramework::getSectionIdByLevelAndEventId(int32 sectionLevel, double sectionId, double LangId)
{
	EventCache* eventCacheObjPtr = EventCache::getInstance();
	return eventCacheObjPtr->getSectionIdByLevelAndEventId(sectionLevel, sectionId, LangId);	
	
}


double  AppFramework::getDollerOff(double regularPrice, double eventPrice)
{
	double difference = regularPrice - eventPrice;
	double diffRoundDown = floor(difference);
	return diffRoundDown;
}

double  AppFramework::getPercentageOff(double regularPrice, double eventPrice)
{
	double percentageOff = (((regularPrice - eventPrice) / regularPrice) * 100);
	double percentageOffRounfDown = floor(percentageOff);
	return percentageOffRounfDown;
}

bool16 AppFramework::StructureCache_isAttributeMPV(double attributeId)
{
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	return structureCacheObjPtr->StructureCache_isAttributeMPV(attributeId);
}


//http://localhost:8080/cf-cm/category/updateData?
//objectType=i&objectType=i
//&objectId=30913618&objectId=30913618
//&fieldId=11002911&fieldId=11002918
//&languageId=91&languageId=91
//&sectionId=585156&sectionId=585156
//&value=new%20Title%20value&value=new%20Desc%20value&

//There are six parameters.  For each item/group/section value that needs to get updated, you will have to send ALL six values, even if empty/null.
//1. objectType will be i/g/s for item/group/section
//2. objectId will be the item/group/section id
//3. fieldId will be the item/group/section field id (attribute_id, element_id)
//4. languageId will be the language id of the value
//5. sectionId will be there if the item/group field is an event-specific field
//6. value will be the updated value
bool16 AppFramework::callReverseUpdateJson(int32 index, double objectId, double fieldId, double langId, double sectionId, PMString value)
{
	PMString jSonURL("");
	if(jsessionID == "")
	{
		//CA("jsessionID == """);
		return kFalse;
	}
		
	//https://prd02.apsiva.net/cf-cm/category/updateData?objectType=i&objectId=30913618&fieldId=11002911&languageId=91&sectionId=585156&value=new%20Title%20value
	
	jSonURL.Append(SelectedServerURL); //https://prd02.apsiva.net/cf-cm
	jSonURL.Append("/category/updateData.json");
	jSonURL.Append(";jsessionid="); 
	jSonURL.Append(jsessionID);	

	jSonURL.Append("?objectType=");	
	PMString objectType("");
	if(index == 3)
	{
		objectType = "g";
	}
	else if(index == 4)
	{
		objectType = "i";
	}
	else if(index == 5)
	{
		objectType = "s";
	}
	jSonURL.Append(objectType);
	
	jSonURL.Append("&objectId=");
	jSonURL.AppendNumber(PMReal(objectId));

	jSonURL.Append("&fieldId=");
	jSonURL.AppendNumber(PMReal(fieldId));

	jSonURL.Append("&languageId=");
	jSonURL.AppendNumber(PMReal(langId));

	jSonURL.Append("&sectionId=");
	jSonURL.AppendNumber(PMReal(sectionId));

	this->LogDebug(value);
	CURL *curl;
	curl_global_init(CURL_GLOBAL_ALL);
    curl = curl_easy_init();
	curl_easy_setopt(curl, CURLOPT_ENCODING, "UTF-8");

    value.ParseForEmbeddedCharacters();
	WideString wideValue(value);
	string UTFvalueStr;
	StringUtils::ConvertWideStringToUTF8 (wideValue, UTFvalueStr );

	char* valueStr = curl_easy_escape(curl, UTFvalueStr.c_str(), 0/*value.NumUTF16TextChars()*/);

	jSonURL.Append("&value=");
	jSonURL.Append(valueStr);

		
	PMString ASD("ReverseUpdate: " + jSonURL);
	this->LogDebug(ASD);
	//CA("callReverseUpdateJson: " + jSonURL);

	
	PMString jsonServiceOutput("");

	bool16 errorCode = callService(jSonURL, jsonServiceOutput);
	if(!errorCode)
	{
		//CA("callReverseUpdateJson Service Call Failed");
		this->LogError("callReverseUpdateJson Service Call Failed for " );
		this->LogError( jSonURL.GetPlatformString().c_str() );
	}
	else
	{		
		//jsonParser jsonPraserObj;
		//jsonPraserObj.parseItemDetails(jsonServiceOutput, returndata);
	}
	return errorCode;
}


double AppFramework::StructureCache_getPickListGroupIdByElementMPVId(double elementID)
{
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	return structureCacheObjPtr->StructureCache_getPickListGroupIdByElementMPVId(elementID);
}

double AppFramework::StructureCache_getPickListGroupIdByAttributeMPVId(double attributeId)
{
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	return structureCacheObjPtr->StructureCache_getPickListGroupIdByAttributeMPVId(attributeId);
}

Attribute AppFramework::StructureCache_getItemAttributeModelByAttributeId(double AttributeId)
{
	StructureCache* structureCacheObjPtr = StructureCache::getInstance();
	return structureCacheObjPtr->StructureCache_getItemAttributeModelByAttributeId(AttributeId);
}

bool16 AppFramework::CONFIGCACHE_getShowCustomerInIndesign()
{
	ConfigCache* configCacheObjPtr = ConfigCache::getInstance();
	if(configCacheObjPtr->getintConfigValue1ByConfigName("SHOW_CUSTOMER_IN_INDESIGN") == 1)
	{
		return kTrue;
	}
	else
	{
		return kFalse;
	}
}

double AppFramework::CONFIGCACHE_getCustomerPVTypeId(int32 isCustomer)
{
	int32 CustomerPVTypeId = -1;
	ConfigCache* configCacheObjPtr = ConfigCache::getInstance();

	if(isCustomer == 1)		
		CustomerPVTypeId = configCacheObjPtr->getintConfigValue1ByConfigName("CUSTOMER_PV_TYPE_ID");
	else
		CustomerPVTypeId = configCacheObjPtr->getintConfigValue1ByConfigName("DIVISION_PV_TYPE_ID");

	return CustomerPVTypeId;
}

PMString AppFramework::CONFIGCACHE_getCustomerAssetTypes(int32 isCustomer)
{
	PMString result("");
	ConfigCache* configCacheObjPtr = ConfigCache::getInstance();

	if(isCustomer == 1)		
		result = configCacheObjPtr->getPMStringConfigValue1ByConfigName("CUSTOMER_ASSET_TYPES");
	else
		result = configCacheObjPtr->getPMStringConfigValue1ByConfigName("DIVISION_ASSET_TYPES");

	return result;	
}

double AppFramework::getSelectedCustPVID(int32 isCustomer)
{
	ConfigCache* configCacheObjPtr = ConfigCache::getInstance();
	if(isCustomer == 1)
		return AppFramework::SelectedCustPVID;
	else
		return AppFramework::SelectedDivisionPVID;
}

void AppFramework::setSelectedCustPVID(double pvID,int32 isCustomer)
{
	ConfigCache* configCacheObjPtr = ConfigCache::getInstance();
	if(isCustomer == 1)
		AppFramework::SelectedCustPVID = pvID;
	else
		AppFramework::SelectedDivisionPVID = pvID;
}

// Filter sprayer
bool16 AppFramework::apiCallService(PMString url, PMString &returndata)
{
    bool16 errorCode = kFalse;
    if(apiKey == "")
    {
        return kFalse;
    }
    CURL *curl;
    CURLcode res;
    PMString auth = "Authorization:  Bearer " + apiKey;
    
    curl_global_init(CURL_GLOBAL_DEFAULT);
    
    curl = curl_easy_init();
    
    std::string readBuffer;
    //
    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, auth.GetPlatformString().c_str());
    headers = curl_slist_append(headers, "Content-Type: application/json; charset=UTF-8");
    
    if(curl) {
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
        
        curl_easy_setopt(curl, CURLOPT_URL, url.GetPlatformString().c_str());
        
        #ifdef SKIP_PEER_VERIFICATION
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
        #endif
        
        #ifdef SKIP_HOSTNAME_VERIFICATION
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
        #endif
        
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
        curl_easy_setopt(curl, CURLOPT_SSLVERSION, 6);
        curl_easy_setopt(curl, CURLOPT_ENCODING, "UTF-8");
        
        if(currentServerConfigValue.getVendorName()  == "Use_Proxy")// Check for Proxy Setting
        {
            this->LogDebug("Use_Proxy");
            PMString proxyUsername = currentServerConfigValue.getDbUserName();
            PMString proxyPassword = currentServerConfigValue.getDbPassword();
            PMString server = currentServerConfigValue.getServerName();
            PMString port= "";
            port.AppendNumber(currentServerConfigValue.getServerPort());
            
            PMString proxy = server + ":" + port;
            this->LogDebug("proxy = " + proxy );
            curl_easy_setopt(curl, CURLOPT_PROXY, proxy.GetPlatformString().c_str());
            PMString auth = "";
            if(proxyUsername != "" && proxyPassword != "" )
            {
                auth = proxyUsername + ":" + proxyPassword;
                this->LogDebug("auth= " + auth);
                curl_easy_setopt(curl, CURLOPT_PROXYUSERPWD, auth.GetPlatformString().c_str());
            }			
            curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
            //curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            
        }

        
        /* Perform the request, res will get the return code */
        res = curl_easy_perform(curl);
        /* Check for errors */
        if(res != CURLE_OK)
        {
            errorCode = kFalse;
            curl_easy_cleanup(curl);
            return errorCode;
        }
        
        errorCode = kTrue;
        WideString wideString;
        StringUtils::ConvertUTF8ToWideString(readBuffer, wideString);
        returndata.Append(wideString);
        
        
        
        /* always cleanup */
        curl_easy_cleanup(curl);
    }
    
    curl_global_cleanup();
    return errorCode;

}


bool16 AppFramework::callFilterService(vector<FilterValue> &returndata)
{
    PMString apiURL("");
    bool16 errorCode = kFalse;
    if(apiKey == "")
    {
        return kFalse;
    }
    
    apiURL.Append(SelectedServerURL); //https://prd02.apsiva.net/cf-cm2
    apiURL.Append("/api/content/filter");
    
    PMString jsonServiceOutput("");
    
    try
    {
        errorCode = apiCallService(apiURL, jsonServiceOutput);
    }
    catch (const std::exception& e)
    {
        errorCode = kFalse;
    }
    
    if(!errorCode)
    {
        //CA("callFilterService Service Call Failed");
        this->LogError("callFilterService Service Call Failed");
    }
    else
    {
        jsonParser jsonPraserObj;
        jsonPraserObj.parseFilterValue(jsonServiceOutput, returndata);
    }
    return errorCode;
}

bool16 AppFramework::callFilterResultService(PMString filterId, int32 limit, int32 offset, vector<CItemModel> &returndata)
{
    PMString apiURL("");
    bool16 errorCode = kFalse;
    if(apiKey == "")
    {
        return kFalse;
    }
    
    
    apiURL.Append(SelectedServerURL); //https://prd02.apsiva.net/cf-cm2
    apiURL.Append("/api/content/filter/results?filter_id=");
    apiURL.Append(filterId);
    apiURL.Append("&limit=");
    apiURL.AppendNumber(limit);
    apiURL.Append("&offset=");
    apiURL.AppendNumber(offset);
    
    PMString jsonServiceOutput("");
    
    try
    {
        errorCode = apiCallService(apiURL, jsonServiceOutput);
    }
    catch (const std::exception& e)
    {
        errorCode = kFalse;
    }
    
    if(!errorCode)
    {
        //CA("callFilterResultService Service Call Failed");
        this->LogError("callFilterResultService Service Call Failed");
    }
    else
    {
        jsonParser jsonPraserObj;
        jsonPraserObj.parseFilterResultValue(jsonServiceOutput, returndata);
    }
    return errorCode;

}

bool16 AppFramework::callFilterResultCountService(PMString filterId, int32 &returndata)
{
    PMString apiURL("");
    bool16 errorCode = kFalse;
    if(apiKey == "")
    {
        return kFalse;
    }
    
    apiURL.Append(SelectedServerURL); //https://prd02.apsiva.net/cf-cm2
    apiURL.Append("/api/content/filter/results/count?filter_id=");
    apiURL.Append(filterId);
    
    PMString jsonServiceOutput("");
    
    try
    {
        errorCode = apiCallService(apiURL, jsonServiceOutput);
    }
    catch (const std::exception& e)
    {
        errorCode = kFalse;
    }
    
    if(!errorCode)
    {
        //CA("callFilterResultCountService Service Call Failed");
        this->LogError("callFilterResultCountService Service Call Failed");
    }
    else
    {
        jsonParser jsonPraserObj;
        jsonPraserObj.parseFilterResultCountValue(jsonServiceOutput, returndata);
    }
    return errorCode;

}


bool16 AppFramework::callGetPricingModule(  vector<PricingModule> &returndata)
{
    PMString apiURL("");
    bool16 errorCode = kFalse;
    if(apiKey == "")
    {
        return kFalse;
    }
    
    apiURL.Append(SelectedServerURL); //https://prd02.apsiva.net/cf-cm2
    apiURL.Append("/v2/api/users/pricingModule");
    this->LogDebug(apiURL);
    
    PMString jsonServiceOutput("");
    
    try
    {
        errorCode = apiCallService(apiURL, jsonServiceOutput);
    }
    catch (const std::exception& e)
    {
        errorCode = kFalse;
    }
    
    if(!errorCode)
    {
        //CA("callGetPricingModule Service Call Failed");
        this->LogError("callGetPricingModule Service Call Failed");
    }
    else
    {
        this->LogDebug(jsonServiceOutput);
        jsonParser jsonPraserObj;
        jsonPraserObj.parsePricingModuleList(jsonServiceOutput, returndata);
    }
    return errorCode;
    
}

bool16 AppFramework::callGetWorkflowTasks(  vector<WorkflowTask> &returndata)
{
    PMString apiURL("");
    bool16 errorCode = kFalse;
    if(apiKey == "")
    {
        return kFalse;
    }
    
    apiURL.Append(SelectedServerURL); //https://prd02.apsiva.net/cf-cm2
    apiURL.Append("/v2/api/workflow/tasks");
    
    this->LogDebug(apiURL);
    
    PMString jsonServiceOutput("");
    
    try
    {
        errorCode = apiCallService(apiURL, jsonServiceOutput);
    }
    catch (const std::exception& e)
    {
        errorCode = kFalse;
    }
    
    if(!errorCode)
    {
        //CA("callGetWorkflowTasks Service Call Failed");
        this->LogError("callGetWorkflowTasks Service Call Failed");
    }
    else
    {
        this->LogDebug(jsonServiceOutput);
        jsonParser jsonPraserObj;
        jsonPraserObj.parseWorkflowTaskList(jsonServiceOutput, returndata);
    }
    return errorCode;
    
}

bool16 AppFramework::callGetAttributeGroups(  vector<AttributeGroup> &returndata)
{
    PMString apiURL("");
    bool16 errorCode = kFalse;
    if(apiKey == "")
    {
        return kFalse;
    }
    
    apiURL.Append(SelectedServerURL); //https://prd02.apsiva.net/cf-cm2
    apiURL.Append("/api/v3/attributes/groups?locale=en_US&limit=100&offset=0");
    
    this->LogDebug(apiURL);
    
    PMString jsonServiceOutput("");
    
    try
    {
        errorCode = apiCallService(apiURL, jsonServiceOutput);
    }
    catch (const std::exception& e)
    {
        errorCode = kFalse;
    }
    
    if(!errorCode)
    {
        //CA("callGetAttributeGroups Service Call Failed");
        this->LogError("callGetAttributeGroups Service Call Failed");
    }
    else
    {
        this->LogDebug(jsonServiceOutput);
        jsonParser jsonPraserObj;
        jsonPraserObj.parseAttributeGoupsJson(jsonServiceOutput, returndata);
    }
    return errorCode;
    
}

VectorAttributeGroupPtr AppFramework::StructureCache_getItemAttributeGroups()
{
    StructureCache* structureCacheObjPtr = StructureCache::getInstance();
    return structureCacheObjPtr->StructureCache_getItemAttributeGroups();
}

vector<double> AppFramework::StructureCache_getItemAttributeListforAttributeGroupId(double attributeGroupId, bool16 isRefreshCall)
{
    if(!isRefreshCall)
    {
        StructureCache* structureCacheObjPtr = StructureCache::getInstance();
        return structureCacheObjPtr->StructureCache_getItemAttributeListforAttributeGroupId(attributeGroupId);
    }
    else
    {
        PMString keyOrId("");
        keyOrId.AppendNumber(PMReal(attributeGroupId));
        bool16 isKey = kFalse;
        vector<double> attributeList;
        AttributeGroup attrGroup;
        bool16 result = callGetAttributesByAttributeGroupKeyOrId(keyOrId, isKey, attrGroup );
        if(result)
            attributeList = attrGroup.getAttributeIds();
        
        return attributeList;
    }
}

vector<double> AppFramework::StructureCache_getItemAttributeListforAttributeGroupKey(PMString &attributeGroupKey, bool16 isRefreshCall)
{
    if(!isRefreshCall)
    {
        StructureCache* structureCacheObjPtr = StructureCache::getInstance();
        return structureCacheObjPtr->StructureCache_getItemAttributeListforAttributeGroupKey(attributeGroupKey);
    }
    else
    {
        PMString keyOrId("");
        keyOrId.Append(attributeGroupKey);
        bool16 isKey = kTrue;
        vector<double> attributeList;
        AttributeGroup attrGroup;
        bool16 result = callGetAttributesByAttributeGroupKeyOrId(keyOrId, isKey, attrGroup );
        if(result)
            attributeList = attrGroup.getAttributeIds();
        
        return attributeList;
    }
}


bool16 AppFramework::callGetAttributesByAttributeGroupKeyOrId( PMString keyOrId, bool16 isKey, AttributeGroup &returndata)
{
    PMString apiURL("");
    bool16 errorCode = kFalse;
    if(apiKey == "")
    {
        return kFalse;
    }
    
    apiURL.Append(SelectedServerURL); //https://prd02.apsiva.net/cf-cm2
    apiURL.Append("/api/v3/attributeGroups/");
    apiURL.Append(keyOrId);
    apiURL.Append("/attributes?isKey=");
    if(isKey)
        apiURL.Append("true");
    else
        apiURL.Append("false");
    apiURL.Append("&locale=en_US");
    
    this->LogDebug(apiURL);
    
    PMString jsonServiceOutput("");
    
    try
    {
        errorCode = apiCallService(apiURL, jsonServiceOutput);
    }
    catch (const std::exception& e)
    {
        errorCode = kFalse;
    }
    
    if(!errorCode)
    {
        //CA("callGetAttributesByAttributeGroupKeyOrId Service Call Failed");
        this->LogError("callGetAttributesByAttributeGroupKeyOrId Service Call Failed");
    }
    else
    {
        this->LogDebug(jsonServiceOutput);
        jsonParser jsonPraserObj;
        jsonPraserObj.parseAttributeGoupJson(jsonServiceOutput, returndata);
    }
    return errorCode;
    
}



bool16 AppFramework::callGetAttributesByItemIdAndAttributeGroupKeyOrId( PMString keyOrId, bool16 isKey, double itemId,vector<double> &returndata)
{
    PMString apiURL("");
    bool16 errorCode = kFalse;
    if(apiKey == "")
    {
        return kFalse;
    }
    
    apiURL.Append(SelectedServerURL); //https://prd02.apsiva.net/cf-cm2
    ///api/v3/items/{itemId}/attributeGroups/{attributeGroupId}/attributes  getItemAttributeGroupAttributes
    apiURL.Append("/api/v3/items/");
    apiURL.AppendNumber( PMReal(itemId));
    apiURL.Append("/attributeGroups/");
    apiURL.Append(keyOrId);
    apiURL.Append("/attributes");
    //apiURL.Append("?isKey=");
    //if(isKey)
    //    apiURL.Append("true");
    //else
    //    apiURL.Append("false");
    //apiURL.Append("&locale=en_US");
    
    this->LogDebug(apiURL);
    
    PMString jsonServiceOutput("");
    
    try
    {
        errorCode = apiCallService(apiURL, jsonServiceOutput);
    }
    catch (const std::exception& e)
    {
        errorCode = kFalse;
    }
    
    if(!errorCode)
    {
        //CA("callGetAttributesByItemIdAndAttributeGroupKeyOrId Service Call Failed");
        this->LogError("callGetAttributesByItemIdAndAttributeGroupKeyOrId Service Call Failed");
    }
    else
    {
        this->LogDebug(jsonServiceOutput);
        jsonParser jsonPraserObj;
        jsonPraserObj.parseAttributesOfGroupJson(jsonServiceOutput, returndata);
    }
    return errorCode;
    
}

vector<double> AppFramework::getItemAttributeListforAttributeGroupIdAndItemId(double sectionId, double itemId, double attributeGroupId, double languageId)
{
    //PMString keyOrId("");
    //keyOrId.AppendNumber(PMReal(attributeGroupId));
    //bool16 isKey = kFalse;
    //bool16 result = callGetAttributesByItemIdAndAttributeGroupKeyOrId(keyOrId, isKey, itemId, attributeList );
   
    vector<double> attributeList;
    int32 isProduct =0;
    EventCache* eventCacheObjPtr = EventCache::getInstance();
    CPbObjectValue pbObjValue = eventCacheObjPtr->getPbObjectValueBySectionIdObjectId(sectionId, itemId, isProduct, languageId);
    CItemModel itemModelObj = pbObjValue.getItemModel();
    attributeList = itemModelObj.getAttributeIdsByAttributeGroupId(attributeGroupId);
    
    return attributeList;
}

vector<double> AppFramework::StructureCache_getItemAttributeListforAttributeGroupKeyAndItemId(PMString &attributeGroupKey, double itemId)
{
    PMString keyOrId("");
    keyOrId.Append(attributeGroupKey);
    bool16 isKey = kFalse;
    vector<double> attributeList;
    bool16 result = callGetAttributesByItemIdAndAttributeGroupKeyOrId(keyOrId, isKey, itemId, attributeList );    
    return attributeList;
}


// isProduct = 0 - Item Assets, 1- Item Group Assets, 5- Section/Event Assets
VectorAssetValuePtr AppFramework::GETAssets_GetAssetByParentAndAssetId(double objectId, double sectionId, int32 isProduct, double assetId)
{
    VectorAssetValuePtr vectAssetPtr = NULL;
    double LanguageId = 91; // hardcoded not relevent here
    EventCache* eventCacheObjPtr = EventCache::getInstance();
    CPbObjectValue pbObjValue = eventCacheObjPtr->getPbObjectValueBySectionIdObjectId(sectionId, objectId, isProduct, LanguageId);
    
    VectorAssetValue vectAssetList;
    if(isProduct == 1)
    {
        CObjectValue objectValueObj = pbObjValue.getObjectValue();
        vectAssetList = objectValueObj.getAssets();
    }
    else if(isProduct == 0)
    {
        CItemModel itemModelObj = pbObjValue.getItemModel();
        vectAssetList = itemModelObj.getAssetList();
    }
    else if(isProduct == 5)
    {
        EventCache* eventCacheObjPtr = EventCache::getInstance();
        EventCacheValue eventCacheObj = eventCacheObjPtr->getEventCacheValueByEventId(objectId);
        vectAssetList = eventCacheObj.getAssets();
    }
    
    if(assetId == -1)
    {
        if(vectAssetList.size() == 0)
        {
            return NULL;
        }
        vectAssetPtr =  new VectorAssetValue;
        *vectAssetPtr = vectAssetList;
        return vectAssetPtr;
    }
    
    if(vectAssetList.size() > 0)
    {
        vectAssetPtr =  new VectorAssetValue;
        for(int count =0; count < vectAssetList.size(); count ++ )
        {
            if(vectAssetList.at(count).getAsset_id() == assetId)
            {
                vectAssetPtr->push_back((vectAssetList.at(count)));
            }
        }
    }
    return vectAssetPtr;
}


PMString AppFramework::StructureCache_getItemAttributeGroupNameByAttributeGroupId(double attributeGroupId)
{
    StructureCache* structureCacheObjPtr = StructureCache::getInstance();
    return structureCacheObjPtr->StructureCache_getItemAttributeGroupNameByAttributeGroupId(attributeGroupId);

}


bool16 AppFramework::callGetAttributesV3( vector<Attribute> &itemFields)
{
    PMString apiURL("");
    bool16 errorCode = kFalse;
    if(apiKey == "")
    {
        return kFalse;
    }
    
    apiURL.Append(SelectedServerURL); //https://prd02.apsiva.net/cf-cm2
    ///api/v3/items/{itemId}/attributeGroups/{attributeGroupId}/attributes  getItemAttributeGroupAttributes
    apiURL.Append("/api/v3/attributes?resultType=MASTER");
    
    this->LogDebug(apiURL);
    
    PMString jsonServiceOutput("");
    
    try
    {
        errorCode = apiCallService(apiURL, jsonServiceOutput);
    }
    catch (const std::exception& e)
    {
        errorCode = kFalse;
    }
    
    if(!errorCode)
    {
        //CA("callGetAttributesByItemIdAndAttributeGroupKeyOrId Service Call Failed");
        this->LogError("callGetAttributesByItemIdAndAttributeGroupKeyOrId Service Call Failed");
    }
    else
    {
        this->LogDebug(jsonServiceOutput);
        jsonParser jsonPraserObj;
        jsonPraserObj.parseAttributesV3Json(jsonServiceOutput, itemFields);
    }
    return errorCode;
    
}

bool16 AppFramework::callSectionResultJsonWithChildItemDetails(double sectionId, double langId, vector<CPbObjectValue> &returndata, PMString itemAttributeId)
{
    PMString jSonURL("");
    if(jsessionID == "")
    {
        //CA("jsessionID == """);
        return kFalse;
    }

    jSonURL.Append(SelectedServerURL);
    jSonURL.Append("/event/section/");
    jSonURL.AppendNumber(PMReal(sectionId));
    jSonURL.Append("/results.json;jsessionid=");
    jSonURL.Append(jsessionID);
    jSonURL.Append("?languageId=");
    jSonURL.AppendNumber(PMReal(langId));
    
    if(itemAttributeId != "")
    {
        jSonURL.Append("&itemFieldIds=");
        jSonURL.Append(itemAttributeId);
    }
    jSonURL.Append("&includeListsAndTables=TRUE");
    //CA("SectionResultjSonURL: " + jSonURL);
    
    PMString ASD("SectionResultjSonURL: " + jSonURL);
    this->LogDebug(ASD);
    
    PMString jsonServiceOutput("");
    
    bool16 errorCode = callService(jSonURL, jsonServiceOutput);
    if(!errorCode)
    {
        //CA("callSectionResultJson Service Call Failed");
        this->LogError("callSectionResultJsonWithChildItemDetails Service Call Failed for " );
        this->LogError( jSonURL.GetPlatformString().c_str() );
    }
    else
    {
        jsonParser jsonPraserObj;
        jsonPraserObj.parseSectionResult(jsonServiceOutput, returndata, sectionId, itemAttributeId);
    }
    
    
    return errorCode;
}
