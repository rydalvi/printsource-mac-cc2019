#ifndef __PARTNERCACHE__
#define __PARTNERCACHE__


#include "AFWJSID.h"
#include "vector"
//#include <map>
#include <set>
#include "list"
#include "libjson.h"
#include "JSONNode.h"
#include "JSONOptions.h"
#include "IAppFramework.h"
#include "PartnerCacheValue.h"

using namespace std;

class PartnerCache
{

private: 
		static PartnerCache* pcObjPtr;
		static bool pcInstanceFlag ;
		PartnerCacheValue* pcValuePtr;
		bool16 isPartnerCacheLoaded;
		

		PartnerCache() 
		{
			pcValuePtr = NULL;
			// private constructor
		}
		
		

public:

	static PartnerCache* getInstance(); // SingleTone Implementation.
	void clearInstace();

	~PartnerCache() 
	{
		pcInstanceFlag = false;
		isPartnerCacheLoaded =  kFalse;
		pcValuePtr = NULL;
	}

	bool16 loadPartnerCache();
	
	PartnerCacheValue* getPartnerCacheValueObj()
	{
		return pcValuePtr;
	}

	PMString getPartnerNameByPartnerIdPartnerType(double partnerId, int32 partnerType); // 1= Brand, 2= Manfacturer, 3=Vendor/Supplier
	
	CAssetValue getPartnerAssetValueByPartnerIdImageTypeId(double partnerId, int32 imageTypeId);

	PMString getPartnerDescription1ByPartnerIdPartnerType(double partnerId, int32 partnerType); // 1= Brand, 2= Manfacturer, 3=Vendor/Supplier
	PMString getPartnerDescription2ByPartnerIdPartnerType(double partnerId, int32 partnerType); // 1= Brand, 2= Manfacturer, 3=Vendor/Supplier

};

#endif
