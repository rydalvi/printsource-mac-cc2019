#ifndef __JSONPARSER__
#define __JSONPARSER__


#include "AFWJSID.h"
#include "vector"
#include "map"
#include <set>
#include "list"
#include "libjson.h"
#include "JSONNode.h"
#include "JSONOptions.h"
#include "StructureCacheValue.h"
#include "EventCacheValue.h"
#include "ConfigValue.h"
#include "PbObjectValue.h"
#include "Column.h"
#include "ItemValue.h"
#include "LoginInfoValue.h"
#include "ClientModel.h"
#include "PartnerValue.h"
#include "PartnerCacheValue.h"
#include "IndexReference.h"
#include "Details.h"
#include "PricingModule.h"
#include "WorkflowTask.h"
#include "FilterValue.h"
#include "AttributeGroup.h"


using namespace std;

class jsonParser
{
public:

	bool16 parseLogin(PMString& inputJson, PMString &sessionId, PMString &apiKey, PMString &pimVersion);
	bool16 parseLogout(PMString inputJson, PMString &logoutStatus);
	bool16 parseStructure(PMString inputJson, StructureCacheValue &structureTreeObj);
	bool16 parseAllTree(PMString inputJson, EventCacheValue &eventTreeObj);
	bool16 parseSysConfig(PMString inputJson, vector<ConfigValue> &configVectorObj);
	bool16 parseSectionResult(PMString inputJson, vector<CPbObjectValue> &pbObjVectorObj , double sectionId, PMString attributeIdStr="");
	bool16 parseItemGroupDetails(PMString inputJson, CObjectValue &itemGroupObject);
	bool16 parseItemDetails(PMString inputJson, CItemModel &itemObject);
	bool16 parseLoginProperties(PMString inputJson, vector<LoginInfoValue>* loginInfoObject);
	bool16 parseClientValues(PMString inputJson, vector<CClientModel>* vectClientModel);
	bool16 parsePartners(PMString inputJson, PartnerCacheValue &partnerCacheValueObj);
	bool16 parseIndexReferences(PMString inputJson, vector<IndexReference>& indexReferenceVectorObj);
	bool16 checkSessionTimeoutJson(PMString inputJson);

	void ParseStructureChildernJSON(JSONNode & nodeObj, StructureCacheValue &structureTreeObj);
	void ParseAttributeJSON(JSONNode & nodeObj, Attribute &itemFieldValue);
	void ParseElementJSON(JSONNode & nodeObj, CElementModel &itemGroupFieldValue);
	void ParseTypeJSON(JSONNode & nodeObj, CTypeModel &itemAssetTypeValue);
	void ParseLanguageJSON(JSONNode & nodeObj, CLanguageModel &languageValue);
	void ParseStatusJSON(JSONNode & nodeObj, StageValue &statuseValue);
	void ParseItemGroupValueJSON(JSONNode & nodeObj, ItemGroupValue &objectValuesValue);
	void ParsePicklistGroupValueJSON(JSONNode & nodeObj, PicklistGroup &picklistGroupsValue);
	void ParsePicklistValueJSON(JSONNode & nodeObj, PicklistValue &picklistValue);
	void ParseEventChildernJSON(JSONNode & nodeObj, EventCacheValue &eventTreeObj);
	void ParseAssetsJSON(JSONNode & nodeObj, CAssetValue &assetValue);
	void ParseConfigJSON(JSONNode & nodeObj, ConfigValue &configValue);
	void ParseSectionResultJSON(JSONNode & nodeObj, CPbObjectValue &pbObjValue, double sectionId, PMString attributeIdStr="");
	void ParseTableValueJSON(JSONNode & nodeObj, CItemTableValue &objTableValue);
	void ParseTableColumnJSON(JSONNode & nodeObj, Column &colValue);
	void ParseTableRowJSON(JSONNode & nodeObj, vector<PMString> &rowValue, double &rowitemId);
	void ParseStringArrayJSON(JSONNode & nodeObj, vector<PMString> &stringArrayValue);
	void ParseItemValueJSON(JSONNode & nodeObj, ItemValue &objectValuesValue);
	void ParseStringJSON(JSONNode & nodeObj, PMString &stringValue);
	void ParseItemGroupDetailsJson(JSONNode & nodeObj, CObjectValue &itemGroupObjectValue);
	void ParseItemDetailsJson(JSONNode & nodeObj, CItemModel &itemObjectValue);
	void ParseLoginInfoJson(JSONNode & nodeObj, LoginInfoValue &loginInfoObject);
	void ParseClientModelJson(JSONNode & nodeObj, CClientModel &clientModelObject);
	void ParseClientInfoJson(JSONNode & nodeObj, ClientInfoValue &clientInfoObject);
	void ParsePartnerValueJson(JSONNode & nodeObj, PartnerValue &partnerValueObject);
	void ParseIndexReferenceJSON(JSONNode & nodeObj, IndexReference &indexReference);
	void ParseAdvanceTableValueJSON(JSONNode & nodeObj, CAdvanceTableScreenValue &advanceTableValueObject);
	void ParseAdvanceTableCellValueJSON(JSONNode & nodeObj, CAdvanceTableCellValue &advanceTableCellValueObject);

	bool16 writeLoginProperties(PMString FilePath, vector<LoginInfoValue>* vectloginInfoObject);	
	
	//void setParentIdInStructureObjects(StructureCacheValue &structureTreeObj , int32 parentId);
    string convert_to_string(int32 a);
	
	void ParseDetailsJSON(JSONNode & nodeObj, Details &detailValue);
    
    
    // Filter sprayer
    bool16 parseFilterValue(PMString inputJson, vector<FilterValue> &filterObject);
    void ParseFilterJSON(JSONNode & nodeObj, FilterValue &filterValue);
    bool16 parseFilterResultValue(PMString inputJson, vector<CItemModel> &filterResultObject);
    void ParseFilterResultJSON(JSONNode & nodeObj, CItemModel &filterResultValue);
    bool16 parseFilterResultCountValue(PMString inputJson, int32 &filterResultCount);

	 void ParsePricingModuleJSON(JSONNode & nodeObj, PricingModule &pricingModule);
    bool16 parsePricingModuleList(PMString inputJson, vector<PricingModule>& pricingModuleVectorObj);
    
    bool16 parseWorkflowTaskList(PMString inputJson, vector<WorkflowTask>& workflowTaskVectorObj);
    void ParseWorkflowTaskJSON(JSONNode & nodeObj, WorkflowTask &workflowTask);
    void ParseWorkflowItemJson(JSONNode & nodeObj, WorkflowItem &workflowItem);
    void ParseWorkflowItemValueJSON(JSONNode & nodeObj, WorkflowItemValue &workflowItemValue);
  
    bool16 parseAttributeGoupsJson(PMString inputJson, vector<AttributeGroup> &attributeGroups);
    void ParseAttributeGoupJSON(JSONNode & nodeObj, AttributeGroup &attributeGroup);
    void ParseLocalizationJson(JSONNode & nodeObj, PMString &name);
    void ParseLanguageSpecificDataJson(JSONNode & nodeObj, PMString &name);
    void ParseAttributeIdListJSON(JSONNode & nodeObj, double &attributeId);
    
    bool16 parseAttributeGoupJson(PMString inputJson, AttributeGroup &attributeGroup);
    bool16 parseAttributesOfGroupJson(PMString inputJson, vector<double> &attributeIds);
    
    void ParseAttributeGroupValueJSONForSectionResult(JSONNode & nodeObj, vector<AttributeGroup> &attributeGroups);
    bool16 parseAttributesV3Json(PMString inputJson, vector<Attribute> &attributes);
    void parseAttributeV3Json(JSONNode & nodeObj, Attribute &itemFieldValue);
    
    void ParseTableRowJSONForChildItem(JSONNode & nodeObj, ChildItemModel &childItem);
    

};

#endif
