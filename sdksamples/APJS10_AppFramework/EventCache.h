#ifndef __EVENTCACHE__
#define __EVENTCACHE__


#include "AFWJSID.h"
#include "vector"
#include "map"
#include <set>
#include "list"
#include "libjson.h"
#include "JSONNode.h"
#include "JSONOptions.h"
#include "IAppFramework.h"
#include "PubModel.h"
#include "PbObjectValue.h"
#include "EventCacheValue.h"

using namespace std;

class EventCache
{

private: 
		static EventCache* ecObjPtr;
		static bool ecInstanceFlag ;
		EventCacheValue ecValueObj;
		bool16 isEventTreeLoaded;
		map< double, EventCacheValue> eventCacheMap;

		double currentEventId;
		double currentSectionId;
		CObjectValue *currentItemGroupValue;
		CItemModel *currentItemModelObj;
		double currentObjectId; // for Single Item Spray or Refresh.

		//vector<CPbObjectValue> *vectorPtrPbObjectValueForCurrentSectionId; // For section spray
		map<double ,VectorPbObjectValuePtr> multipleSectionDetailsMap;

		EventCache() 
		{
			// private constructor
			currentEventId = -1;
			currentSectionId = -1;
			//vectorPtrPbObjectValueForCurrentSectionId = NULL;
			currentItemGroupValue = NULL;
			currentItemModelObj = NULL;
			ecInstanceFlag = false;
			isEventTreeLoaded = false;

		}
		
		

public:

	static EventCache* getInstance(); // SingleTone Implementation.
	void clearInstace();

	~EventCache() 
	{
		ecInstanceFlag = false;
		isEventTreeLoaded =  false;
		currentItemGroupValue = NULL;
		currentItemModelObj = NULL;
	}

	bool16 loadEventCache();
	void loadEventCacheMap(EventCacheValue & eventChildObj , double rootId, double parentId);
	EventCacheValue getEventCacheValueByEventId(double eventID);
	VectorPubModelPtr ProjectCache_getAllProjects(double);
	VectorPubModelPtr ProjectCache_getAllChildren(double PubId, double LanguageID);
	CPubModel getpubModelByPubID(double PubID,double languageID);
	
	bool16 setCurrentSectionData(double sectionId, double langId , PMString itemGroupIds, PMString itemIds, PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds, PMString listItemFieldIds, bool16 isCategorySpecificResultCall = kFalse,  bool16 isSprayItemPerFrameFlag = kFalse, PMString itemAttributeGroupIds="");
	CPbObjectValue getPbObjectValueBySectionIdObjectId(double sectionId, double ObjectId, int32 isProduct, double langId);  // ObjectId can be ItemGroupId or ItemId
	void clearCurrentSectionData();
	void clearMultipleSectionDetailsMap();

	void setCurrentObjectData(double sectionId, double ObjectId, int32 isProduct, double langId, PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds, PMString listItemFieldIds );
    
	void clearCurrentObjectData();

	void loadListItemToSectionVectorList(vector<CItemTableValue> listVectorObj, VectorPbObjectValuePtr ptrObj);

	double getSectionIdByLevelAndEventId(int32 sectionLevel, double sectionId, double LangId);
    void loadAdvancedTableItemToSectionVectorList(vector<CAdvanceTableScreenValue> listVectorObj, VectorPbObjectValuePtr vectorPtrPbObjectValueForCurrentSectionId);
};

#endif
