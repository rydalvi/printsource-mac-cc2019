

#include "VCPlugInHeaders.h"
#include "persistData.h"

PMString PersistData::PubName;
OptionsValue PersistData::curOptions;
InterfacePtr<IPanelControlData> PersistData::generalPanelControlData; 
bool16 PersistData::PublicationDialogType=0;
bool16 PersistData::PathBrowseDialogType=0;

//Publication name 
PMString& PersistData::getPubName(void)
{
	return PersistData::PubName;
}
void  PersistData::setPubName(PMString& pub)
{
	PersistData::PubName=pub;
}

// OptionsValue
OptionsValue&  PersistData::getOptions(void)
{	
	return PersistData::curOptions;
}
void  PersistData::setOptions(OptionsValue& value)
{
	curOptions= value;
}

// PublicationDialogType : 1 - showing with options dialog, 
// 2 - stand along with some other plugins
bool16 PersistData::getPublicationDialogType(void)
{
	return PersistData::PublicationDialogType;
}
void  PersistData::setPublicationDialogType(bool16 dlgType)
{
	PersistData::PublicationDialogType=dlgType;
}

// PathBrowseDialogType : 1 - image path, 2- doc path
bool16 PersistData::getPathBrowseDialogType(void)
{
	return PersistData::PathBrowseDialogType;
}
void  PersistData::setPathBrowseDialogType(bool16 dlgType)
{
	PersistData::PathBrowseDialogType=dlgType;
}

InterfacePtr<IPanelControlData> PersistData::getGeneralPanelControlData(void)
{
	return generalPanelControlData;
}
void PersistData::setGeneralPanelControlData(InterfacePtr<IPanelControlData> paneldata)
{
	generalPanelControlData=paneldata;
}