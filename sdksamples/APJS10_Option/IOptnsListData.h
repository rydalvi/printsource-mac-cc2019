
#ifndef __IOptnsListData_h__
#define __IOptnsListData_h__

#include "IPMUnknown.h"
#include "OptnsDlgID.h"
#include "PMString.h"

class IPstLstData : public IPMUnknown
{
public:

	enum	{kDefaultIID = IID_IPSTLSTDATA};

	virtual void SetName(const PMString& theLabel) = 0;

	virtual const PMString&	GetName() = 0;

};
#endif // __IPstLstData_h__
