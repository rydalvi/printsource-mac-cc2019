#include "VCPlugInHeaders.h"
#include "CPanelCreator.h"
#include "CAlert.h"
#include "OptnsDlgID.h"

class AttribsPanelCreator : public CPanelCreator
{
	public:
		AttribsPanelCreator(IPMUnknown *boss) : CPanelCreator(boss) {}
		virtual ~AttribsPanelCreator() {}
		virtual RsrcID GetPanelRsrcID() const;
};

CREATE_PMINTERFACE(AttribsPanelCreator, kAttribsPanelCreatorImpl)

RsrcID AttribsPanelCreator::GetPanelRsrcID() const
{
	return kAttribsPanelCreatorResourceID;
}


