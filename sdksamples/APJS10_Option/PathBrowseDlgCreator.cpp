/*
//	File:	PathBrowseDlgCreator.cpp
//
//	Date:	07-Jun-2001
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

#include "PathBrowseDlgCreator.h"


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(PathBrowseDlgCreator, kPathBrowseDlgCreatorImpl)

/* 
	CreateDialog
*/
IDialog* PathBrowseDlgCreator::CreateDialog()
{
	IDialog* dialog = nil;
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
		if (application == nil)
		{
			ASSERT_FAIL("LNGActionComponent::DoAction: application invalid");
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		if (dialogMgr == nil)
		{ 
			ASSERT_FAIL("LNGActionComponent::DoAction: dialogMgr invalid"); 
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kOptnsDlgPluginID,				// Our Plug-in ID from LNGID.h. 
			kViewRsrcType,				// This is the kViewRsrcType.
			kPathBrowse1DialogResourceID,							// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		if (dialog == nil)
		{ 
			ASSERT_FAIL("LNGActionComponent::DoAction: can't create dialog"); 
			break;
		}	
		
		// Open the dialog.
		dialog->Open();		
		dialog->WaitForDialog();

	} while(false);		

	return dialog;
}

