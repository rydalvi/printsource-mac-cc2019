#include "VCPlugInHeaders.h"
#include "WidgetID.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IListControlData.h"
#include "IListBoxController.h"
#include "CAlert.h"
#include "CObserver.h"
#include "OptnsDlgID.h"
#include "SystemUtils.h"
#include "SDKUtilities.h"
#include "SDKListBoxHelper.h"
#include "IAppFramework.h"
#include "IPanelControlData.h"
#include "ISelection.h"
#include "IApplication.h"
#include "k2smartptr.h"
#include "LayoutUtils.h"
#include "listData.h"
#include "CmdUtils.h"
#include "IToolCmdData.h"
#include "ITool.h"
#include "IToolManager.h"
#include "ITextModel.h"
#include "FrameUtils.h"
#include "ISpecifier.h"
#include "IFrameList.h"
#include "ITextFrame.h"
#include "IHierarchy.h"
#include "ILayoutTarget.h"
#include "ITransform.h"
#include "ITextFocusManager.h"
#include "IDocument.h"
#include "ISpreadList.h"
#include "IGeometry.h"
#include "TransformUtils.h"
#include "ISpread.h"
#include "IScrapItem.h"
#include "ITextControlData.h"
#include "CommonFunctions.h"
#include "OptnsDragEventFinder.h"
#include "listData.h"

#define CA(x)	CAlert::InformationAlert(x)
#define DRAGMODE	1

//listData genPanelListData;

class OptnsGenpanelLstBoxObserver : public CObserver
{
public:
	OptnsGenpanelLstBoxObserver(IPMUnknown *boss);
	~OptnsGenpanelLstBoxObserver();
	virtual void AutoAttach();
	virtual void AutoDetach();
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);

private:
	void updateListBox(bool16 isAttaching);
};

CREATE_PMINTERFACE(OptnsGenpanelLstBoxObserver, kOptnsGenpanelLstBoxObserverImpl)

OptnsGenpanelLstBoxObserver::OptnsGenpanelLstBoxObserver(IPMUnknown* boss)
: CObserver(boss)
{
}

OptnsGenpanelLstBoxObserver::~OptnsGenpanelLstBoxObserver()
{
}

void OptnsGenpanelLstBoxObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
	}
}

void OptnsGenpanelLstBoxObserver::AutoDetach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this);
	}
}

void OptnsGenpanelLstBoxObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	int selectedRowIndex=-1;
	PMString name("");

	if ((protocol == IID_ILISTCONTROLDATA) && (theChange == kListSelectionChangedByUserMessage) ) 
	{
		do 
		{
			InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
			if(panel==nil)
			{
				CA("panel is nil");
				break;
			}

			SDKListBoxHelper listHelper(this, kOptnsDlgPluginID);
			IControlView * listBoxCntrlView = listHelper.FindCurrentListBox(panel, 1);
			if(listBoxCntrlView == nil) 
			{
				CA("listbox controlview is nil");
				break;
			}

			InterfacePtr<IListBoxController> listCntl(listBoxCntrlView,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
			if(listCntl == nil) 
			{
				CA("listboxcntrller is nil");
				break;
			}

			K2Vector<int32> multipleSelection ;
			listCntl->GetSelected( multipleSelection ) ;

			const int kSelectionLength =  multipleSelection.Length() ;
			PMString currentSelectionRowString("");

			/* getting current selection here */
			if (kSelectionLength> 0 )
			{
				selectedRowIndex=multipleSelection[0];

				listData genPanelListData;
				int32 vectSize = genPanelListData.returnListVectorSize(1);

				/* For getting data of the selected row of listbox */
				for(int y=0; y<vectSize; y++)
				{
					if(y==selectedRowIndex)
					{
						listInfo list = genPanelListData.getData(1,y);
						currentSelectionRowString = list.name;
						OptnsDragEventFinder::isImgflag=list.isImageFlag;
						break;
					}
				}
			}

			bool flag=kFalse;
			UID currentSelection=-1;
			do
			{
				InterfacePtr<ISelection> selection(::QuerySelection());
				if (selection == nil)
				{
				//	CA("Selection nil");
					break;
				}
				
				scoped_ptr<UIDList> selectedUIDList( selection->CreateUIDList() );
				if (selectedUIDList == nil)
				{
				//	CA("SelectedUID list nil");
					break;
				}

				const int32 listLength=selectedUIDList->Length();
				
				if(listLength==0)
				{
				//	CA("listlen is zero");
					break;
				}
				currentSelection=selectedUIDList->GetRef(0).GetUID();
				PMString curBoxUID("UID of cur box ");
				curBoxUID.AppendNumber(selectedUIDList->GetRef(0).GetUID().Get());
				//CA(curBoxUID);
				flag=kTrue;
			}while(kFalse);

			OptnsDragEventFinder::listboxType = 1;
			OptnsDragEventFinder::selectedRowIndex=selectedRowIndex;
			OptnsDragEventFinder::currentSelectionRowString=currentSelectionRowString;
			OptnsDragEventFinder::cntrlView= listBoxCntrlView;
		} while(0);
	}
}

