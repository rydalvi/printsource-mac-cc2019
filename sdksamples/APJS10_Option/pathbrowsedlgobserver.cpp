/*
//	File:	PathBrowseDlgObserver.cpp
//
//	Date:	5-Sep-2003
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All rights reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
#include "PMString.h"
#include "InterfacePtr.h"
#include "ITextControlData.h"
//#include "ISelectFolderDialog.h"//Deprecated
#include "IOpenFileDialog.h"

// General includes:
#include "CDialogObserver.h"
#include "SysFileList.h"
#include "SystemUtils.h"
#include "SDKUtilities.h"
#include "CAlert.h"

// Project includes:
#include "OptnsDlgID.h"
#include "OptionsStaticData.h"
#include "OptionsUtils.h"
#include "ITriStateControlData.h"
#include "FileUtils.h"


#define CA(X) CAlert::InformationAlert(X);

/** PathBrowseDlgObserver
	Allows dynamic processing of dialog widget changes, in this case
	the dialog's info button. 
  
	Implements IObserver based on the partial implementation CDialogObserver. 
	@author raghu
*/
class PathBrowseDlgObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		PathBrowseDlgObserver(IPMUnknown* boss) : CDialogObserver(boss) { }

		/** Destructor. */
		virtual ~PathBrowseDlgObserver() {}

		/** 
			Called by the application to allow the observer to attach to the subjects 
			to be observed, in this case the dialog's info button widget. If you want 
			to observe other widgets on the dialog you could add them here. 
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);
	private:
		void ShowBrowseDialog(PMString&, PMString&);
		void setControlText(WidgetID, PMString);
		void enableControl(WidgetID );
		PMString path;
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(PathBrowseDlgObserver, kPathBrowseDlgObserverImpl)

/* AutoAttach
*/
void PathBrowseDlgObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("PathBrowseDlgObserver::AutoAttach() panelControlData invalid");
			break;
		}		
		// Attach to other widgets you want to handle dynamically here.
		AttachToWidget(kBrowsePathButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);		
		AttachToWidget(kselectMaptoRepositryWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kselectDownloadImagesToWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kselectimagePathButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		AttachToWidget(kselectDownloadImagePathButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		AttachToWidget(kOKButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		AttachToWidget(kBrwsImgPathCancelWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);  //*****
		AttachToWidget(kBrwsImgPathOKBtnWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);  //*****
		
		
	} while (false);
	path = "";
}

/* AutoDetach
*/
void PathBrowseDlgObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("PathBrowseDlgObserver::AutoDetach() panelControlData invalid");
			break;
		}		
		// Detach from other widgets you handle dynamically here.		
		DetachFromWidget(kBrowsePathButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kselectMaptoRepositryWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kselectDownloadImagesToWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kselectimagePathButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kselectDownloadImagePathButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kOKButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kBrwsImgPathCancelWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);  //*****
		DetachFromWidget(kBrwsImgPathOKBtnWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);  //*****
		

	} while (false);
}

/* Update
*/
void PathBrowseDlgObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	//CDialogObserver::Update(theChange, theSubject, protocol, changedBy);	
	do
	{
			
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			//ASSERT_FAIL("PathBrowseDlgObserver::Update() controlView invalid");
			//CAlert::InformationAlert("Observer : controlView invalid");
			break;
		}
		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();
		//added by mane
		if(theSelectedWidget==kselectimagePathButtonWidgetID && theChange == kTrueStateMessage)
		{
			//CA("kselectimagePathButtonWidgetID");
			PMString infoMsg("Map to Image Directory");
			PMString path("");
			//OptionsValue optnsObj = PersistData::getOptions();
			//path = optnsObj.getImagePath();

			ShowBrowseDialog(path,infoMsg);

			setControlText(kselectimagePathStaticTextWidgetID,path);
			//optnsObj.setImagePath(path);
			//optnsObj.setAssetStatus(1);///added by mane
			//PersistData::setOptions(optnsObj);
		}

		if(theSelectedWidget==kselectDownloadImagePathButtonWidgetID && theChange == kFalseStateMessage)
		{
			//CA("kImagePathButtonWidgetID");
			PMString infoMsg("Browse for Image Working Directory");
			PMString path("");
			path.SetTranslatable(kFalse);
			infoMsg.SetTranslatable(kFalse);
			//OptionsValue optnsObj = PersistData::getOptions();
			//path = optnsObj.getImagePath();

			ShowBrowseDialog(path,infoMsg);

			
			setControlText(kselectDownloadImagePathStaticTextWidgetID,path);
			//optnsObj.setImagePath(path);
			//optnsObj.setAssetStatus(0);//added by mane
			//PersistData::setOptions(optnsObj);
		}
		//up to here
		if((theSelectedWidget==kOKButtonWidgetID && theChange == kTrueStateMessage || theSelectedWidget==kBrwsImgPathOKBtnWidgetID) && theChange == kTrueStateMessage)
		{
			//CA("PathBrowseDlgObserver::Update::kOKButtonWidgetID");
			do{
				ClientOptionsManip cops;			
				bool8 isFileExists=cops.IsClientOptionsFileExiats();
				/*if(isFileExists==FALSE){
					PMString filePath = cops.GetOptionsFilePath();
					if(cops.CreateOptionsFile(filePath)==FALSE){
						CAlert::InformationAlert("Fail to Create Client Options File...");
						break;
					}
				}*/
				OptionsValue optnsObj = cops.ReadClientOptions();				
				if(PersistData::getPathBrowseDialogType()== 1)
					optnsObj.setImagePath(path);
				else if(PersistData::getPathBrowseDialogType()== 2)
					optnsObj.setIndesignDocPath(path);
				
				InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
				if (panelControlData == nil){			
					CAlert::InformationAlert("AutoAttach panelControlData nil");
					break;
				}
				IControlView * redioButtonControlView = panelControlData->FindWidget(kselectMaptoRepositryWidgetID);
				if(redioButtonControlView == nil)
				{
					//CA("redioButtonControlView == nil");
					break;
				}
				InterfacePtr<ITriStateControlData>redioButtonriState(redioButtonControlView,UseDefaultIID());
				if(redioButtonriState == nil)
				{
					//CA("redioButtonriState == nil");
					break;
				}
				if(redioButtonriState->IsSelected () == kTrue)
				{
					IControlView* iControlView = panelControlData->FindWidget(kselectimagePathStaticTextWidgetID);
					if (iControlView == nil){
						CAlert::InformationAlert("iControlView nil");
						break;
					}	
					InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
					if (textControlData == nil){
						CAlert::InformationAlert("ITextControlData nil");	
						break;
					}
					PMString imagePath(textControlData->GetString());
					optnsObj.setAssetStatus(1);
					optnsObj.setImagePath(imagePath);///added by mane
				}
				else
				{
					IControlView* iControlView = panelControlData->FindWidget(kselectDownloadImagePathStaticTextWidgetID);
					if (iControlView == nil){
						CAlert::InformationAlert("iControlView nil");
						break;
					}	
					InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
					if (textControlData == nil){
						CAlert::InformationAlert("ITextControlData nil");	
						break;
					}
					PMString imagePath(textControlData->GetString());
					optnsObj.setAssetStatus(0);
					optnsObj.setImagePath(imagePath);///added by mane
				}
				
				IControlView* iselectMissingChkboxCntrlView=panelControlData->FindWidget(kselectMissingFlagWidgetID);
				if(iselectMissingChkboxCntrlView==nil) 
					return;

				InterfacePtr<ITriStateControlData> itristatecontroldataSelectMissingFlag(iselectMissingChkboxCntrlView, UseDefaultIID());
				if(itristatecontroldataSelectMissingFlag==nil) 
					return;
				
				bool16 status = itristatecontroldataSelectMissingFlag->IsSelected();
				if(status == kTrue)
					optnsObj.setMissingFlag(1);
				else
					optnsObj.setMissingFlag(0);
				
				cops.WriteClientOptions(optnsObj);
				
				CDialogObserver::CloseDialog();

			}while(0);				
			break;

		}
		//added by mane
		if(theSelectedWidget==kselectMaptoRepositryWidgetID && theChange == kTrueStateMessage)
		{
			//CA("kselectMaptoRepositryWidgetID");
			InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
			if (panelControlData == nil)
			{
				//CA("PathBrowseDlgObserver::AutoDetach() panelControlData invalid");
				break;
			}
			//CA("theSelectedWidget==kselectMaptoRepositryWidgetID && theChange == kTrueStateMessage");
			IControlView* iSelectImagePathButtonCntrlView=panelControlData->FindWidget(kselectimagePathButtonWidgetID);
			if(iSelectImagePathButtonCntrlView==nil) 
			{
				//CA("iOptnsImagePathTextCntrlView==nil");
				return;			
			}
			iSelectImagePathButtonCntrlView->ShowView();

			IControlView* iSelectDownloadPathButtonCntrlView=panelControlData->FindWidget(kselectDownloadImagePathButtonWidgetID);
			if(iSelectDownloadPathButtonCntrlView==nil) 
			{
				//CA("iOptnsImagePathTextCntrlView==nil");
				return;			
			}
			iSelectDownloadPathButtonCntrlView->HideView();
			
			IControlView* iselectimagePathStaticTextWidgetID1=panelControlData->FindWidget(kselectimagePathStaticTextWidgetID);
			if(iselectimagePathStaticTextWidgetID1==nil) 
			{
				//CA("iOptnsImagePathTextCntrlView==nil");
				return;			
			}
			iselectimagePathStaticTextWidgetID1->ShowView();

			IControlView* iselectDownloadImagePathStaticTextWidgetID=panelControlData->FindWidget(kselectDownloadImagePathStaticTextWidgetID);
			if(iselectDownloadImagePathStaticTextWidgetID==nil) 
			{
				//CA("iOptnsImagePathTextCntrlView==nil");
				return;			
			}
			iselectDownloadImagePathStaticTextWidgetID->HideView();

			ClientOptionsManip cops;
			OptionsValue optnsObj = cops.ReadClientOptions();
			setControlText(kselectimagePathStaticTextWidgetID,optnsObj.getImagePath());
		}

		if(theSelectedWidget==kselectDownloadImagesToWidgetID && theChange == kTrueStateMessage)
		{
			//CA("kselectDownloadImagesToWidgetID");
			InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
			if (panelControlData == nil)
			{
				//CA("PathBrowseDlgObserver::AutoDetach() panelControlData invalid");
				break;
			}
			//CA("theSelectedWidget==kselectMaptoRepositryWidgetID && theChange == kTrueStateMessage");
			IControlView* iSelectImagePathButtonCntrlView=panelControlData->FindWidget(kselectimagePathButtonWidgetID);
			if(iSelectImagePathButtonCntrlView==nil) 
			{
				//CA("iOptnsImagePathTextCntrlView==nil");
				return;			
			}
			iSelectImagePathButtonCntrlView->HideView();

			IControlView* iSelectDownloadPathButtonCntrlView=panelControlData->FindWidget(kselectDownloadImagePathButtonWidgetID);
			if(iSelectDownloadPathButtonCntrlView==nil) 
			{
				//CA("iOptnsImagePathTextCntrlView==nil");
				return;			
			}
			iSelectDownloadPathButtonCntrlView->ShowView();
		
			IControlView* iselectimagePathStaticTextWidgetID1=panelControlData->FindWidget(kselectimagePathStaticTextWidgetID);
			if(iselectimagePathStaticTextWidgetID1==nil) 
			{
				//CA("iOptnsImagePathTextCntrlView==nil");
				return;			
			}
			iselectimagePathStaticTextWidgetID1->HideView();

			IControlView* iselectDownloadImagePathStaticTextWidgetID=panelControlData->FindWidget(kselectDownloadImagePathStaticTextWidgetID);
			if(iselectDownloadImagePathStaticTextWidgetID==nil) 
			{
				//CA("iOptnsImagePathTextCntrlView==nil");
				return;			
			}
			iselectDownloadImagePathStaticTextWidgetID->ShowView();

			ClientOptionsManip cops;
			OptionsValue optnsObj = cops.ReadClientOptions();
			setControlText(kselectDownloadImagePathStaticTextWidgetID,optnsObj.getImagePath());
		}
		//up to here
		/*if(theSelectedWidget==kBrowsePathButtonWidgetID && theChange == kTrueStateMessage)
			{
				PMString infoMsg("");
				if(PersistData::getPathBrowseDialogType()== 1)
					infoMsg+="Browse for Image Working Directory";
				else if(PersistData::getPathBrowseDialogType()== 2) 
					infoMsg+="Browse for Document Path";
				
				ShowBrowseDialog(infoMsg);
				setControlText(kPathDlgStaticTextWidgetID);
				if(path.NumUTF16TextChars() > 0 && path != ""){
					enableControl(kOKButtonWidgetID);
				}			
			}*/
		if((theSelectedWidget == kBrwsImgPathCancelWidgetID || theSelectedWidget == kCancelButton_WidgetID) && theChange == kTrueStateMessage){			
			CDialogObserver::CloseDialog();
			break;
		}


	} while (false);
}
//  Generated by Dolly build 17: template "Dialog".
// End, PathBrowseDlgObserver.cpp.

void PathBrowseDlgObserver::ShowBrowseDialog(PMString& path,PMString& infoMsg)
{
	//path.Clear();

	do {
    	SysFileList	fileList;
    	InterfacePtr</*ISelectFolderDialog*/IOpenFileDialog>	openDlg(::CreateObject2</*ISelectFolderDialog*/IOpenFileDialog>(kSelectFolderDialogBoss));//CS4
		//InterfacePtr<IOpenFileDialog> openDlg(static_cast<IOpenFileDialog*>(::CreateObject(kSelectFolderDialogBoss,IOpenFileDialog::kDefaultIID)));
    	if(!openDlg)
    	{
    		CAlert::InformationAlert("!openDlg...");
			break;	
    	}
    	infoMsg.SetTranslatable(kFalse);
		IDFile	fPath;	
		if (SDKUtilities::AbsolutePathToSysFile(path, fPath) != kSuccess){
			CAlert::InformationAlert("Can not convert to SysFile format...");
			break;
		}
    	if (openDlg->DoDialog(&fPath, fileList, kTrue, nil, &infoMsg))
    	{   					
    		IDFile	folderPath = *fileList.GetNthFile(0);
    		PMString	folderName;
#ifdef WINDOWS
			folderName = folderPath.GetString();
#endif
#ifdef MACINTOSH
            folderName = FileUtils::SysFileToPMString(folderPath);
#endif
     		folderName.SetTranslatable(kFalse);
			path.SetString(folderName);	
			path.Append("\\");
            path.ParseForEmbeddedCharacters();
    	}				
	} while(false);	
}

////////////////////////////


void PathBrowseDlgObserver::setControlText(WidgetID widgetID,PMString text1)
{
	//do{
	//	InterfacePtr<IPanelControlData> panelControlData(this,UseDefaultIID());
	//	if (panelControlData == nil){
	//		CAlert::InformationAlert("panelControlData nil");
	//		break;
	//	}

	//	IControlView* iControlView = panelControlData->FindWidget(widgetID);
	//	if (iControlView == nil){
	//		CAlert::InformationAlert("iControlView nil");
	//		break;
	//	}	
	//	InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
	//	if (textControlData == nil){
	//		CAlert::InformationAlert("ITextControlData nil");	
	//		break;
	//	}

	//	PMString displayString("");
	//	if(PersistData::getPathBrowseDialogType()== 1)
	//		displayString = "Browse for Image Working Directory: ";		// 
	//	else if(PersistData::getPathBrowseDialogType()== 2)
	//		displayString = "Browse for Document download path: ";

	//	displayString += path;
	//	textControlData->SetString(displayString);
	//}
	//while(0);

	//added by mane
	do{
		//CA("GeneralPanelObserver::setControlText");
		InterfacePtr<IPanelControlData> panelControlData(this,UseDefaultIID());
		if (panelControlData == nil){
			CAlert::InformationAlert("panelControlData nil");
			break;
		}

		IControlView* iControlView = panelControlData->FindWidget(widgetID);
		if (iControlView == nil){
			CAlert::InformationAlert("iControlView nil");
			break;
		}	
		InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
		if (textControlData == nil){
			CAlert::InformationAlert("ITextControlData nil");	
			break;
		}
		text1.SetTranslatable(kFalse);
        text1.ParseForEmbeddedCharacters();
		textControlData->SetString(text1);
	}
	while(0);
}
//up to here
void PathBrowseDlgObserver::enableControl(WidgetID widgetID)
{
	do{
		InterfacePtr<IPanelControlData> panelControlData(this,UseDefaultIID());
		if (panelControlData == nil){
			CAlert::InformationAlert("path browse dlg PanelControlData nil...");
			break;
		}

		IControlView* controlView = panelControlData->FindWidget(widgetID);
		if (controlView == nil){
			//CAlert::InformationAlert("Path Browse Dlg ok btn Controlview nil...");
			break;
		}			
		controlView->Enable(kTrue);		
	}while(kFalse);
}

