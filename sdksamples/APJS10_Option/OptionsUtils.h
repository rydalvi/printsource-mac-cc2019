
#ifndef __IClientOptionsManip__
#define __IClientOptionsManip__

#include "PMString.h"
#include "OptionsValue.h"

class ClientOptionsManip
{
	public:
		ClientOptionsManip();		
		~ClientOptionsManip();

		bool8			IsClientOptionsFileExiats(void);		
		bool8			CreateOptionsFile(PMString&);
		PMString		GetOptionsFilePath(void);	
		OptionsValue&	ReadClientOptions(void);
		void			WriteClientOptions(OptionsValue&);
		PMString		getStringByKey(PMString& ,PMString& );		
		double			checkforPublicationExistance(PMString& ,double& pID ); //argument(pID)added by mane
		bool8			checkforFolderExistance(PMString&);
		double			checkforLocaleExistance(PMString& pName,double& languageID); 
		void			SetCatalogIDInAppFramework(double catalogID);
		void			SetLocaleIDInAppFramework(double localeID);


		/*
		int32			getCurrentPublicationDetails(PMString);
		PMString		getCurrentImagePath(void);
		PMString		getCurrentDocPath(void);
		*/
	private:
		//PMString curPub;
		//PMString curImageDir;
		//PMString curDocDir;
		OptionsValue OptionsObj;
		PMString OptionsFilepath;
};

#endif