#include "VCPlugInHeaders.h"
#include "IClassIDData.h"
#include "ISignalMgr.h"
#include "IDocumentSignalData.h"
#include "IUIDData.h"
#include "IObserver.h"
#include "CreateObject.h"
#include "CResponder.h"
#include "OptnsDlgID.h"
#include "CommonFunctions.h"
#include "CAlert.h"

#define CA(Z) CAlert::InformationAlert(Z)

class OptnsDocResponder : public CResponder
{
	public:
		OptnsDocResponder(IPMUnknown* boss);
		virtual void Respond(ISignalMgr* signalMgr);
};

CREATE_PMINTERFACE(OptnsDocResponder, kOptnsDocResponderImpl)

OptnsDocResponder::OptnsDocResponder(IPMUnknown* boss) :
	CResponder(boss)
{
}

void OptnsDocResponder::Respond(ISignalMgr* signalMgr)
{
	// Get the service ID from the signal manager
	ServiceID serviceTrigger = signalMgr->GetServiceID();

	// Get a UIDRef for the document.  It will be an invalid UIDRef
	// for BeforeNewDoc, BeforeOpenDoc, AfterSaveACopy, and AfterCloseDoc because the
	// document doesn't exist at that point.
	InterfacePtr<IDocumentSignalData> docData(signalMgr, UseDefaultIID());
	if (docData == nil)
	{
		ASSERT_FAIL("Invalid IDocumentSignalData* - OptnsDocResponder::Respond");
		return;
	}
	UIDRef docRef = docData->GetDocument();

	// Take action based on the service ID
	switch (serviceTrigger.Get())
	{
		case kAfterNewDocSignalResponderService:
			{
				refreshLstbox(kInvalidUID);
			}
			break;
		case kDuringOpenDocSignalResponderService:
		{
			InterfacePtr<IObserver> docObserver(docRef, IID_IPSTLSTOBSERVER);
			if (docObserver != nil)
			{
				docObserver->AutoAttach();
			}
			break;
		}
		case kBeforeCloseDocSignalResponderService:
		{
			InterfacePtr<IObserver> docObserver(docRef, IID_IPSTLSTOBSERVER);
			if (docObserver != nil)
			{
				docObserver->AutoDetach();
			}
			break;
		}
		default:
			break;
	}

}

