#include "VCPlugInHeaders.h"

#include "IDragDropController.h"
#include "CDragDropSource.h"
// General includes:
#include "SDKUtilities.h"
#include "CAlert.h"
#include "UIDList.h"
#include "CmdUtils.h"
#include "UIDRef.h"
#include "CmdUtils.h"
#include "CAlert.h"
// Project includes:
#include "OptnsDlgID.h"
#include "CEventHandler.h"

class OptnsEventHandler : public CEventHandler
{
	public:
		OptnsEventHandler(IPMUnknown* boss);
	
		bool16 OptnsEventHandler::LButtonDn(IEvent* e)
		{ 
			CAlert::InformationAlert("Left Btn down");
			CEventHandler::LButtonDn(e);
			return kFalse;
		}

		
};

CREATE_PMINTERFACE(OptnsEventHandler, kOptnsEHImpl)

/* OptnsEventHandler Constructor
*/
OptnsEventHandler::OptnsEventHandler(IPMUnknown* boss)
: CEventHandler(boss)
{
}
