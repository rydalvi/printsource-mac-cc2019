
#include "VCPlugInHeaders.h"

// API includes
#include "HelperInterface.h"
#include "CPMUnknown.h"

// Project includes:
#include "IOptnsListData.h"
#include "IPMStream.h"
#include "listData.h"
#include "OptnsDragEventFinder.h"

/** PstLstDataPersist is the persistent \Ref{IPstLstData} implementation.

	this implementation of IPstLstData is persistent and is added to the kPstLstDataBoss, this data boss
	is created when the New command is called.  When this plug-in is loaded, a page item will carry a UID 
	list that contains the UIDs of all the data object being added to itself. 
*/

class PstLstDataPersist : public CPMUnknown<IPstLstData>
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		PstLstDataPersist(IPMUnknown* boss);

		/**
			This function returns its only data member, fName.
			@return PMString the data member this interface carries.
		*/
		virtual const PMString&	GetName();

		/**
			This function set its only data member with the parameter passed in.
			@param theData data to be set
		*/
		virtual void SetName(const PMString& theData);

		/**
			Because this is a persistent interface, it must support the ReadWrite method. This method is used for 
			both writing information out to the database and reading information in from the database.

			Note: the first thing we read/write here is a version number.  It is used to identify which plug-in
			is used to write out this IPstLstData.  Since this is the first version of the PstLst plug-in, thus the version
			number is 1, and then followed by the IPstLstData.  Later if there is a newer version of this plug-in which
			will write out an extra field of data, then we can just come to this routine, and check the version number
			of the doc, and then read in the data accordingly.  this will make sure the newer plug-in is backward
			compatible with an older version of document.  Lastly, since the version number should be the first thing
			to read in for this interface, all the new data should be append at the bottom.

			@param stream contains the stream to be written or read.
			@param implementation the implementation ID.
		*/
		void ReadWrite(IPMStream* stream, ImplementationID implementation);
		void GetOtherInfo(int32* eleId, int32* typid);

	private:
		PMString	fName;
		int32 elementId;
		int32 typeId;

};

CREATE_PERSIST_PMINTERFACE(PstLstDataPersist,kPstLstDataPersistImpl)

/*	PstLstDataPersist::PstLstDataPersist
*/
PstLstDataPersist::PstLstDataPersist(IPMUnknown* boss)
: CPMUnknown<IPstLstData>(boss)
{
}

/*	PstLstDataPersist::SetName
*/
void PstLstDataPersist::SetName(const PMString& newData)
{
	if (fName != newData)
	{
		fName = newData;
		listData lstdta;
		listInfo info;

		info  = lstdta.getData
			(OptnsDragEventFinder::listboxType,
			 OptnsDragEventFinder::selectedRowIndex);
		
		elementId = info.id;
		typeId = info.typeId;
		Dirty();
	}
}

/*	PstLstDataPersist::GetName
*/
const PMString& PstLstDataPersist::GetName()
{
	return fName;
}

/*	PstLstDataPersist::ReadWrite
*/
void PstLstDataPersist::ReadWrite(IPMStream* stream, ImplementationID implementation)
{
	fName.ReadWrite(stream);
	stream->XferInt32(elementId);
	stream->XferInt32(typeId);

}

void PstLstDataPersist::GetOtherInfo(int32* eleId, int32* typid)
{
	*eleId=elementId;
	*typid=typeId;
}
