#include "VCPlugInHeaders.h"
#include "CDialogController.h"
#include "SystemUtils.h"
#include "OptnsDlgID.h"
#include "CAlert.h"
#include "SDKListBoxHelper.h"
#include "IAppFramework.h"
#include "listData.h"
#include "LayoutUtils.h"
#include "IDocument.h"
#include "ISpread.h"
#include "ISpreadList.h"
#include "IHierarchy.h"
#include "CommonFunctions.h"
#include "OptnsDragEventFinder.h"

listData genPanelListData;
listData attribPanelListData;
listData taskPanelListData;
listData itemPanelListData;
listData imageInfoVector;

#define CA(x)	CAlert::InformationAlert(x)

class OptnsTabDlgController : public CDialogController
{
	public:
		OptnsTabDlgController(IPMUnknown* boss) : CDialogController(boss) {}
		virtual ~OptnsTabDlgController() {}
		virtual void InitializeFields();
		virtual WidgetID ValidateFields();
		virtual void ApplyFields(const WidgetID& widgetId);
		void populateGenPanelLstbox();
		void populateAttribPanelLstbox();
		void populateTaskPanelLstbox();
		void populateItemPanelLstbox();
		int getAllItemsOnCurPage(UIDList& allItemsOnPage);
		virtual void ClosingDialog();
};

CREATE_PMINTERFACE(OptnsTabDlgController, kOptnsTabDlgControllerImpl)

void OptnsTabDlgController::ClosingDialog()
{
	OptnsDragEventFinder::currentSelectionRowString="";
	OptnsDragEventFinder::selectedRowIndex=-1;
	OptnsDragEventFinder::cntrlView=nil;
	OptnsDragEventFinder::listboxType=1;
	OptnsDragEventFinder::whichRadBtnSelected=1;
	OptnsDragEventFinder::currentTab=1;
	OptnsDragEventFinder::isImgflag=kFalse;
	OptnsDragEventFinder::lst1CntrlView=nil;
	OptnsDragEventFinder::lst2CntrlView=nil;
	OptnsDragEventFinder::lst3CntrlView=nil;
	OptnsDragEventFinder::lst4CntrlView=nil;
}
void OptnsTabDlgController::InitializeFields() 
{
	CDialogController::InitializeFields();

	do 
	{
		InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
		if(panel==nil)
		{
			CA("panel is nil");
			break;
		}

		SDKListBoxHelper listHelper(this, kOptnsDlgPluginID);
		IControlView * listBox1CntrlView = listHelper.FindCurrentListBox(panel, 1);
		if(listBox1CntrlView == nil) 
		{
			CA("listbox controlview is nil");
			break;
		}
		OptnsDragEventFinder::lst1CntrlView=listBox1CntrlView;

		IControlView * listBox2CntrlView = listHelper.FindCurrentListBox(panel, 2);
		if(listBox2CntrlView == nil) 
		{
			CA("listbox controlview is nil");
			break;
		}
		OptnsDragEventFinder::lst2CntrlView=listBox2CntrlView;

		IControlView * listBox3CntrlView = listHelper.FindCurrentListBox(panel, 3);
		if(listBox3CntrlView == nil) 
		{
			CA("listbox controlview is nil");
			break;
		}
		OptnsDragEventFinder::lst3CntrlView=listBox3CntrlView;

		IControlView * listBox4CntrlView = listHelper.FindCurrentListBox(panel, 4);
		if(listBox4CntrlView == nil) 
		{
			CA("listbox controlview is nil");
			break;
		}
		OptnsDragEventFinder::lst4CntrlView=listBox4CntrlView;
	}while(kFalse);

	populateGenPanelLstbox();
	populateAttribPanelLstbox();
	populateTaskPanelLstbox();
	populateItemPanelLstbox();	
	refreshLstbox(kInvalidUID);
}

WidgetID OptnsTabDlgController::ValidateFields() 
{
	WidgetID result = CDialogController::ValidateFields();
	return result;
}

void OptnsTabDlgController::ApplyFields(const WidgetID& widgetId) 
{
	SystemBeep();  
}

void OptnsTabDlgController::populateGenPanelLstbox()
{
	do
	{
		InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
		if(panel==nil)
		{
			CA("panel is nil");
			break;
		}

		SDKListBoxHelper listHelper(this, kOptnsDlgPluginID);

		listHelper.EmptyCurrentListBox(panel, 1);

		IControlView* lstboxControlView = listHelper.FindCurrentListBox(panel, 1);
		if(lstboxControlView==nil)
		{
			CA("lstboxControlViewis nil");
			break;
		}
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			CAlert::InformationAlert("ptrIAppFramework is nil");
			break;
		}
/*
public String PRODUCT_FAMILY_ELEMENT 	= "PF_ELEMENT";
public String PRODUCT_GROUP_ELEMENT 	= "PG_ELEMENT";
public String PRODUCT_ELEMENT 			= "PR_ELEMENT";
public String PF_ASSETS = "PFTY"; 
public String PG_ASSETS = "PGTY"; 
public String PR_ASSETS = "PRTY"; 
*/
		long typeId = ptrIAppFramework->TYPEMngr_getObjectTypeID("PRODUCT_FAMILY_ELEMENT");
		if(typeId==-1)
		{
			break;
		}

		VectorElementInfoPtr eleValObj = ptrIAppFramework->EleMngr_getElementsByTypeId(typeId);
		if(eleValObj==nil)
		{
			CA("eleValObj is nil");
			break;
		}
		VectorElementInfoValue::iterator it;
		int i=0;
		genPanelListData.ClearVector(1);
		genPanelListData.setData
		(1,
		 i,
		 -1,
		 -1,
		 "", 
		 "PF Name",
		 kTrue,
		 "",
		 kFalse
		 );

		listHelper.AddElement(lstboxControlView, "PF Name", kGeneralPanelTextWidgetID, i);
		listHelper.CheckUncheckRow(lstboxControlView, i, kFalse);
		i++;

		for(it=eleValObj->begin();it!=eleValObj->end();it++)
		{
			genPanelListData.setData
			(1,
			 i,
			 it->getElement_id(),
			 it->getElement_type_id(),
			 it->getTable_col().GrabCString(), 
			 // No table col for this threrefore put there empty string
			 it->getName().GrabCString(),
			 kTrue,
			 "",
			 kFalse
			 );

			listHelper.AddElement(lstboxControlView, it->getName(), kGeneralPanelTextWidgetID, i);
			listHelper.CheckUncheckRow(lstboxControlView, i, kFalse);
			i++;
		}

		VectorTypeInfoPtr typeValObj = ptrIAppFramework->TYPEMngr_getAllTypesOfTypeGroupByCode("PF_ASSETS");
		if(typeValObj==nil)
		{
			//CA("typeValObj is nil");
			break;
		}
		VectorTypeInfoValue::iterator it1;

		for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
		{
			genPanelListData.setData
			(1,
			 i,
			 -1,
			 it1->getType_id(),
			 "", 
			 it1->getName().GrabCString(),
			 kTrue,
			 it1->getCode().GrabCString(),
			 kTrue
			 );
			listHelper.AddElement(lstboxControlView, it1->getName(), kGeneralPanelTextWidgetID, i);
			listHelper.CheckUncheckRow(lstboxControlView, i, kFalse);
			i++;
		}

		IControlView * listBox = listHelper.FindCurrentListBox(panel, 1);
		if(listBox == nil) 
		{
			CAlert::InformationAlert("FindCurrentListBox nil");
			break;
		}
		listBox->Invalidate();
	}while(kFalse);
}

void OptnsTabDlgController::populateAttribPanelLstbox()
{
	do
	{
		InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
		if(panel==nil)
		{
			CA("panel is nil");
			break;
		}

		SDKListBoxHelper listHelper(this, kOptnsDlgPluginID);
		listHelper.EmptyCurrentListBox(panel, 2);

		IControlView* lstboxControlView = listHelper.FindCurrentListBox(panel, 2);
		if(lstboxControlView==nil)
		{
			CA("lstboxControlViewis nil");
			break;
		}
		
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			CAlert::InformationAlert("ptrIAppFramework is nil");
			break;
		}

		long typeId = ptrIAppFramework->TYPEMngr_getObjectTypeID("PRODUCT_GROUP_ELEMENT");
		if(typeId==-1)
		{
			break;
		}

		VectorElementInfoPtr eleValObj = ptrIAppFramework->EleMngr_getElementsByTypeId(typeId);
		if(eleValObj==nil)
		{
			CA("eleValObj is nil");
			break;
		}
		VectorElementInfoValue::iterator it;
		int i=0;
		attribPanelListData.ClearVector(2);
		attribPanelListData.setData
		(2,
		 i,
		 -1,
		 -1,
		 "", 
		 "PG Name",
		 kTrue,
		 "",
		 kFalse
		 );

		listHelper.AddElement(lstboxControlView, "PG Name", kAttribPanelTextWidgetID, i);
		listHelper.CheckUncheckRow(lstboxControlView, i, kFalse);
		i++;

		for(it=eleValObj->begin();it!=eleValObj->end();it++)
		{
			attribPanelListData.setData
				(2,
				 i,
				 it->getElement_id(),
				 it->getElement_type_id(),
				 it->getTable_col().GrabCString(), 
				 // No table col for this threrefore put there empty string
				 it->getName().GrabCString(),
				 kTrue,
				 "",
				 kFalse
				 );
			listHelper.AddElement(lstboxControlView, it->getName(), kAttribPanelTextWidgetID, i);
			listHelper.CheckUncheckRow(lstboxControlView, i, kFalse);
			i++;
		}

		VectorTypeInfoPtr typeValObj = ptrIAppFramework->TYPEMngr_getAllTypesOfTypeGroupByCode("PG_ASSETS");
		if(typeValObj==nil)
		{
			//CA("typeValObj is nil");
			break;
		}
		VectorTypeInfoValue::iterator it1;

		for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
		{
			attribPanelListData.setData
			(2,
			 i,
			 -1,
			 it1->getType_id(),
			 "", 
			 // No table col for this threrefore put there empty string
			 it1->getName().GrabCString(),
			 kTrue,
			 it1->getCode().GrabCString(),
			 kTrue
			 );
			listHelper.AddElement(lstboxControlView, it1->getName(), kAttribPanelTextWidgetID, i);
			listHelper.CheckUncheckRow(lstboxControlView, i, kFalse);
			i++;
		}

		IControlView * listBox = listHelper.FindCurrentListBox(panel, 2);
		if(listBox == nil) 
		{
			CAlert::InformationAlert("FindCurrentListBox nil");
			break;
		}
		listBox->Invalidate();
	}while(kFalse);
}

void OptnsTabDlgController::populateTaskPanelLstbox()
{
	do
	{
		InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
		if(panel==nil)
		{
			CA("panel is nil");
			break;
		}

		SDKListBoxHelper listHelper(this, kOptnsDlgPluginID);
		listHelper.EmptyCurrentListBox(panel, 3);

		IControlView* lstboxControlView = listHelper.FindCurrentListBox(panel, 3);
		if(lstboxControlView==nil)
		{
			CA("lstboxControlViewis nil");
			break;
		}
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			CAlert::InformationAlert("ptrIAppFramework is nil");
			break;
		}
/*
public String PRODUCT_FAMILY_ELEMENT 	= "PF_ELEMENT";
public String PRODUCT_GROUP_ELEMENT 	= "PG_ELEMENT";
public String PRODUCT_ELEMENT 			= "PR_ELEMENT";
*/
		long typeId = ptrIAppFramework->TYPEMngr_getObjectTypeID("PRODUCT_ELEMENT");
		if(typeId==-1)
		{
			break;
		}

		VectorElementInfoPtr eleValObj = ptrIAppFramework->EleMngr_getElementsByTypeId(typeId);
		if(eleValObj==nil)
		{
			CA("eleValObj is nil");
			break;
		}
		VectorElementInfoValue::iterator it;
		int i=0;
		taskPanelListData.ClearVector(3);
		taskPanelListData.setData
		(3,
		 i,
		 -1,
		 -1,
		 "", 
		 "PR Name",
		 kTrue,
		 "",
		 kFalse
		 );

		listHelper.AddElement(lstboxControlView, "PR Name", kTaskPanelTextWidgetID, i);
		listHelper.CheckUncheckRow(lstboxControlView, i, kFalse);
		i++;
		for(it=eleValObj->begin();it!=eleValObj->end();it++)
		{
			taskPanelListData.setData
			(3,
			 i,
			 it->getElement_id(),
			 it->getElement_type_id(),
			 it->getTable_col().GrabCString(), 
			 // No table col for this threrefore put there empty string
			 it->getName().GrabCString(),
			 kTrue,
			 "",
			 kFalse
			 );

			listHelper.AddElement(lstboxControlView, it->getName(), kTaskPanelTextWidgetID, i);
			listHelper.CheckUncheckRow(lstboxControlView, i, kFalse);
			i++;
		}

		VectorTypeInfoPtr typeValObj = ptrIAppFramework->TYPEMngr_getAllTypesOfTypeGroupByCode("PR_ASSETS");
		if(typeValObj==nil)
		{
			//CA("typeValObj is nil");
			break;
		}
		VectorTypeInfoValue::iterator it1;

		for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
		{
			taskPanelListData.setData
			(3,
			 i,
			 -1,
			 it1->getType_id(),
			 "", 
			 // No table col for this threrefore put there empty string
			 it1->getName().GrabCString(),
			 kTrue,
			 it1->getCode().GrabCString(),
			 kTrue
			 );
			listHelper.AddElement(lstboxControlView, it1->getName(), kTaskPanelTextWidgetID, i);
			listHelper.CheckUncheckRow(lstboxControlView, i, kFalse);
			i++;
		}

		IControlView * listBox = listHelper.FindCurrentListBox(panel, 3);
		if(listBox == nil) 
		{
			CAlert::InformationAlert("FindCurrentListBox nil");
			break;
		}
		listBox->Invalidate();
	}while(kFalse);
}

void OptnsTabDlgController::populateItemPanelLstbox()
{
	do
	{
		InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
		if(panel==nil)
		{
			CA("panel is nil");
			break;
		}

		SDKListBoxHelper listHelper(this, kOptnsDlgPluginID);
		listHelper.EmptyCurrentListBox(panel, 4);

		IControlView* lstboxControlView = listHelper.FindCurrentListBox(panel, 4);
		if(lstboxControlView==nil)
		{
			CA("lstboxControlViewis nil");
			break;
		}
		
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			CAlert::InformationAlert("ptrIAppFramework is nil");
			break;
		}

		VectorAttributeInfoPtr attribValObj = ptrIAppFramework->ATTRIBMngr_getCoreAttributes();
		if(attribValObj==nil)
		{
			CA("attribValObj is nil");
			break;
		}
		VectorAttributeInfoValue::iterator it;
		int i=0;
		itemPanelListData.ClearVector(4);
		itemPanelListData.setData
		(4,
		 i,
		 -1,
		 -1,
		 "", 
		 "Item Name",
		 kTrue,
		 "",
		 kFalse
		 );

		listHelper.AddElement(lstboxControlView, "Item Name", kItemPanelTextWidgetID, i);
		listHelper.CheckUncheckRow(lstboxControlView, i, kFalse);
		i++;

		for(it=attribValObj->begin();it!=attribValObj->end();it++)
		{
			itemPanelListData.setData
			(4,
			 i,
			 it->getAttribute_id(),
			 0,
			 it->getTable_col().GrabCString(),
			 it->getDisplay_name().GrabCString(),
			 kTrue,
			 "",
			 kFalse
			 );
			listHelper.AddElement(lstboxControlView, it->getDisplay_name(), kItemPanelTextWidgetID, i);
			listHelper.CheckUncheckRow(lstboxControlView, i, kFalse);
			i++;
		}

		VectorTypeInfoPtr typeValObj = ptrIAppFramework->TYPEMngr_getAllTypesOfTypeGroupByCode("PRIM");
		if(typeValObj==nil)
		{
			//CA("typeValObj is nil");
			break;
		}
		VectorTypeInfoValue::iterator it1;

		for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
		{
			itemPanelListData.setData
			(4,
			 i,
			 -1,
			 it1->getType_id(),
			 "", 
			 // No table col for this threrefore put there empty string
			 it1->getName().GrabCString(),
			 kTrue,
			 it1->getCode().GrabCString(),
			 kTrue
			 );
			listHelper.AddElement(lstboxControlView, it1->getName(), kItemPanelTextWidgetID, i);
			listHelper.CheckUncheckRow(lstboxControlView, i, kFalse);
			i++;
		}
		IControlView * listBox = listHelper.FindCurrentListBox(panel, 4);
		if(listBox == nil) 
		{
			CAlert::InformationAlert("FindCurrentListBox nil");
			break;
		}
		listBox->Invalidate();
	}while(kFalse);
}

/* Get UID of all boxes on current page */
/* For each UID check for each lstbox whether the vector contains UID */
/* if yes then set the dragged box and change the icon accordingly */

int OptnsTabDlgController::getAllItemsOnCurPage(UIDList& allItemsOnPage)
{
	IDocument* fntDoc = Utils<ILayoutUtils>()->GetFrontDocument();
	if(fntDoc==nil)
	{
		CA("There is no front document");
		return -1;
	}
	InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUtils>()->QueryFrontLayoutData());
	if (layout == nil)
	{
		CA("ILayoutControlData pointer nil");
		return -1;
	}
	IDataBase* database = ::GetDataBase(fntDoc);
	if(database==nil)
	{
		CA("Database is NIL");
		return -1;
	}
	UIDRef pageUIDRef = UIDRef(database,layout->GetPage());
	InterfacePtr<IGeometry> pageGeometry(pageUIDRef, UseDefaultIID());

	if(pageGeometry==nil)
	{
		CA("Page Geometry is NIL");
		return -1;
	}
	InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
	if (iSpreadList==nil)
	{
		CA("There are no spreads in the document");
		return -1;
	}
	UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(0));

	InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());

	UIDList allPageItems(database);
	spread->GetItemsOnPage(0, &allPageItems, kFalse);
	
	int numPageItems=(int)allPageItems.Length();

	for(int i=0; i<allPageItems.Length(); i++)
	{
		InterfacePtr<IHierarchy> iChild(allPageItems.GetRef(i), IID_IHIERARCHY);
		if(iChild)
		{
			UID kidUID;
			int numKids=iChild->GetChildCount();

			for(int j=0; j<numKids-1; j++)
			{
				kidUID=iChild->GetChildUID(j);
				allPageItems.Append(kidUID);
			}
		}
	}
	allItemsOnPage = allPageItems;
	return allPageItems.Length();
}

