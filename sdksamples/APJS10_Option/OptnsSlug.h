#ifndef __OptnsSlug_h__
#define __OptnsSlug_h__

#include "VCPlugInHeaders.h"

class OptnsSlug
{
	private:
		int32 objectId;
		int32 typeId;
		int32 elementId;
		PMString elementName;
};

#endif