//========================================================================
//
//	File:	PstLstDeletePageItemCmd.cpp
//
//	Author:	Lee Huang
//
//	Date:	18-Jun-2001
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
//========================================================================

#include "VCPluginHeaders.h"

// Interface includes
#include "IUIDData.h"
#include "IDocument.h"
#include "ISubject.h"

// API includes
#include "ErrorUtils.h"
#include "PageItemUtils.h"	 
#include "Command.h"
#include "K2Vector.h"
#include "K2Vector.tpp"
#include "CAlert.h"
// Project includes
#include "OptnsListUID.h"
#include "IOptnsListData.h"
#include "IOptnsListUID.h"

/** 
	Provides the command to delete ALL the \Ref{IPstLstData} boss objects associated
	with a page item. 

	This command is similar to PstLstDeleteDataCmd, but it is used when a page item is
	deleted, we need to delete ALL the data boss we added to the item, also we need to 
	restore them in the event of undo deleting page item.  So unlike PstLstDeleteDataCmd,
	which only deletes the current selected UID and its associated data boss for each
	selected page item, this command delete EVERY one that's carried by a page item.  For
	undo purpose, a K2Vector (fOldSelectedUID) is used to store the UID list of 
	IPstLstUIDList for all selected page items.
	
	PstLstDeletePageItemCmd implements ICommand based on the partial implementation Command. 
	
	@author Lee Huang
*/
class PstLstDeletePageItemCmd : public Command
{
	public:
		/**
			Constructor, we initialize all the private members and pass the boss pointer to 
			the parent class (Command).
		*/
		PstLstDeletePageItemCmd(IPMUnknown* boss);

		/**
			Destructor
		*/
		~PstLstDeletePageItemCmd();

		/**
			Don't execute this command if the memory is low.
		*/
		bool16 LowMemIsOK() const { return kFalse; }

	protected:
		/**
			Loop thru all the selected page items, delete all items' kPstLstDataBoss UID from 
			the database, also remove all corresponding entries in the IPstLstUIDList of the 
			item.  It will remember the item's IPstLstUIDList in the a K2Vector fOldSelectedUID 
			before the deletion.
		*/
		void Do();

		/**
			Undoes the command, by simply restore the UID object in the database, and append
			the UID back to the IPstLstUIDList of the page items.
		*/
		void Undo();

		/**
			change the page item to be what the command changed it to in the Do method.
		*/
		void Redo();

		/**
			Notify all the document observers that a change to the page item's IPstLstData
			has happened.
		*/
		void DoNotify();

		/**
			Gives the name that shows up under the Edit menu and allows the user to undo or redo the command.

			@return pointer to a PMString allocated using the new operator. Deleted by caller.
		*/
		PMString* CreateName();

	private:
		K2Vector<UID*> fOldSelectedUID;
		K2Vector<int32>	fOldListCount;
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(PstLstDeletePageItemCmd, kPstLstDeletePageItemCmdImpl)

/*	PstLstDeletePageItemCmd::Constructor
*/
PstLstDeletePageItemCmd::PstLstDeletePageItemCmd(IPMUnknown* boss) :
	Command(boss)
{
}

/*	PstLstDeletePageItemCmd::Destructor
*/
PstLstDeletePageItemCmd::~PstLstDeletePageItemCmd()
{
	if (fOldSelectedUID.Length() != 0)
	{
		int32	items = fItemList.Length();
		for (int32 i = 0; i < items; i++)
		{
			delete [] fOldSelectedUID[i];
		}

		fOldSelectedUID.Clear();
	}
}

/*	PstLstDeletePageItemCmd::Do
*/
void PstLstDeletePageItemCmd::Do()
{
	ErrorCode err = kSuccess;

	int32	items = fItemList.Length();
	if (fOldSelectedUID.Length() != 0)
	{
		int32	items = fItemList.Length();
		for (int32 i = 0; i < items; i++)
		{
			delete [] fOldSelectedUID[i];
		}

		fOldSelectedUID.Clear();
	}
	
	// We have to loop thru all selected page items to delete all IPstLstData stored in its IPstLstUIDList.
	for (int32 i = 0; i < items; i++)
	{
		InterfacePtr<IPstLstUIDList> iPstLstList(fItemList.GetRef(i), UseDefaultIID());
		if (iPstLstList != nil)
		{
			// find out how many IPstLstData are in the IPstLstUIDList
			int32	listLen = iPstLstList->GetUIDList()->Length();
			UID*	uidList = new UID[listLen];

			// save a copy of the IPstLstUIDList for undo before I remove UID from the list 
			for (int32 j = 0; j < listLen; j++)
			{
				uidList[j] = iPstLstList->GetRef(j).GetUID();
			}
			
			// remove EVERY UID from the IPstLstUIDList, also from the database
			for (int32 k = 0; k < listLen; k++)
			{
				iPstLstList->Remove(uidList[k]);
				fItemList.GetDataBase()->DeleteUID(uidList[k]);
			}
			
			// now save the UIDList into a stack that stores all page items' UIDList
			fOldSelectedUID.push_back(uidList);
			fOldListCount.push_back(listLen);
		}
		else
			err = kFailure;
	}	
	
	//Handle errors
	if (err != kSuccess)
	{
		ErrorUtils::PMSetGlobalErrorCode(err);
	}
}

/*	PstLstDeletePageItemCmd::Redo
*/
void PstLstDeletePageItemCmd::Redo()
{
	Do();
}

/*	PstLstDeletePageItemCmd::Undo
*/
void PstLstDeletePageItemCmd::Undo()
{
	int32	items = fItemList.Length();
	// We have to loop thru all selected page items to restore its IPstLstUIDList and associated IPstLstData.
	for (int32 i = 0; i < items; i++)
	{
		InterfacePtr<IPstLstUIDList> iPstLstList(fItemList.GetRef(i), UseDefaultIID());
		if (iPstLstList != nil)
		{
			// the old UIDList before deletion is stored in fOldSelectedUID
			UID*	oldList = fOldSelectedUID[i];
			for (int32 j = 0; j < fOldListCount[i]; j++)
			{
				fItemList.GetDataBase()->UndeleteUID(oldList[j], kPstLstDataBoss);
				iPstLstList->Append(oldList[j]);
			}
		}
	}
}

/*	PstLstDeletePageItemCmd::DoNotify
*/
void PstLstDeletePageItemCmd::DoNotify()
{
	// For notfication we'll let PageItemUtils::NotifyDocumentObservers do the work.
	// Since this is the changed on the IPstLstData, we use its interface ID and the 
	// this command boss'd id as parameter to go into the Change() 
	CAlert::InformationAlert("PstLstDeletePageItemCmd::DoNotify");// 2 box delete

	PageItemUtils::NotifyDocumentObservers
	(
		fItemList, 
		kPstLstDeletePageItemCmdBoss,
		IID_IPSTLSTDATA, 
		this
	);
}

/*	PstLstDeletePageItemCmd::CreateName
*/
PMString* PstLstDeletePageItemCmd::CreateName()
{
	PMString* str = new PMString(kPstLstDeleteStringKey);
	return str;
}
	
// End, PstLstDeletePageItemCmd.cpp.

