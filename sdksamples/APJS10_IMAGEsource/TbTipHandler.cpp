#include "VCPlugInHeaders.h"

//interfaces
#include "ITip.h"
#include "ITool.h"
#include "IControlView.h"
//#include "ITriStateControlData.h"

//includes
#include "PMString.h"
#include "Trace.h"
#include "HelperInterface.h"
#include "StringUtils.h"

#include "DCNID.h"

class TbTipHandler : public ITip
{ 
	public: 
		TbTipHandler(IPMUnknown *boss); 
		virtual ~TbTipHandler(); 
		virtual PMString GetTipText(const PMPoint& mouseLocation/*, ITip::TipWindowPlacementAdvice *outWindowPlacement*/); 

		DECLARE_HELPER_METHODS()
}; 

//CREATE_PMINTERFACE( TbTipHandler,kTbTextWidgetTipImpl)
DEFINE_HELPER_METHODS( TbTipHandler )

TbTipHandler::TbTipHandler(IPMUnknown *boss):
	HELPER_METHODS_INIT(boss)
{
}

TbTipHandler::~TbTipHandler()
{
}

PMString TbTipHandler::GetTipText(const PMPoint& mouseLocation/*, ITip::TipWindowPlacementAdvice *outWindowPlacement*/) 
{ 
	PMString result; 
	InterfacePtr<IControlView> view( this, IID_ICONTROLVIEW ); 
	WidgetID wid = view->GetWidgetID(); 
	/*ASSERT(*/


//	if( wid == kMyFirstTextBoxWidgetID || wid == kMySecondTextBoxWidgetID ) 
//	{ 
		//InterfacePtr<ITextControlData> widget(view, IID_ITEXTCONTROLDATA); 
		//PMString text( widget->GetString() ); 
		//if(! text.IsEmpty() ) 
		//{ 
		//	result.SetString( text ); 
		//} 
//	} 
	//result.SetTranslated(); 
	return result; 
} 
