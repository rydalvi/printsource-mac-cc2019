//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __DCNID_h__
#define __DCNID_h__

#include "SDKDef.h"

// Company:
#define kDCNCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kDCNCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kDCNPluginName		"IMAGEsource"//"AP7_IMAGEsource"			// Name of this plug-in.
#define kDCNPrefixNumber	0xAB613		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kDCNVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kDCNAuthor		"Apsiva Inc."					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kDCNPrefixNumber above to modify the prefix.)
#define kDCNPrefix		RezLong(kDCNPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kDCNStringPrefix	SDK_DEF_STRINGIZE(kDCNPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kDCNMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kDCNMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kDCNPluginID, kDCNPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kDCNActionComponentBoss,		kDCNPrefix + 0)
DECLARE_PMID(kClassIDSpace, kDCNPanelWidgetBoss,			kDCNPrefix + 1)
DECLARE_PMID(kClassIDSpace, kXLibraryItemViewPanelBoss,		kDCNPrefix + 3)
DECLARE_PMID(kClassIDSpace, kXLibraryItemGridBoss,			kDCNPrefix + 4)
DECLARE_PMID(kClassIDSpace, kXLibraryItemListBoxBoss,		kDCNPrefix + 5)
DECLARE_PMID(kClassIDSpace, kXLibraryItemButtonBoss,		kDCNPrefix + 6)
DECLARE_PMID(kClassIDSpace, kDCNProductImageIFaceBoss,		kDCNPrefix + 7)
DECLARE_PMID(kClassIDSpace, kDCNDNDCustomFlavorHelperBoss,	kDCNPrefix + 8)
DECLARE_PMID(kClassIDSpace, kDCNDNDCustomScrapHandlerBoss,	kDCNPrefix + 9)
DECLARE_PMID(kClassIDSpace, kXLibraryInfoPanelBoss,			kDCNPrefix + 10) //for double-click
DECLARE_PMID(kClassIDSpace, kDCNActiveSelectionBoss,		kDCNPrefix + 11)
DECLARE_PMID(kClassIDSpace, kDCNFitIconWidgetBoss,			kDCNPrefix + 12)
DECLARE_PMID(kClassIDSpace, kDCNLoginEventsHandler,			kDCNPrefix + 13)
DECLARE_PMID(kClassIDSpace, kDCNlstviewListBoxWidgetBoss,	kDCNPrefix + 14)///Added For ListView
DECLARE_PMID(kClassIDSpace, kDCNlstviewViewPanelWidgetBoss,	kDCNPrefix + 15)
DECLARE_PMID(kClassIDSpace, kDCNTextWidgetBoss,				kDCNPrefix + 16)
DECLARE_PMID(kClassIDSpace,kDCNHTILstboxMultilineTxtWidgetBoss,kDCNPrefix + 18)
DECLARE_PMID(kClassIDSpace, kDCNListViewPanelWidgetBoss,	kDCNPrefix + 19)
DECLARE_PMID(kClassIDSpace,kHHTILstboxMultilineTxtWidgetBoss,kDCNPrefix + 20)
DECLARE_PMID(kClassIDSpace, kDCNCheckBoxWidgetBoss, kDCNPrefix + 21)
DECLARE_PMID(kClassIDSpace, kFilterDialogBoss, kDCNPrefix + 22)

//DECLARE_PMID(kClassIDSpace, kDCNBoss, kDCNPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kDCNBoss, kDCNPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kDCNBoss, kDCNPrefix + 25)

// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_IXLIBRARYVIEWCONTROLLER,	kDCNPrefix + 0)
DECLARE_PMID(kInterfaceIDSpace, IID_IXLIBRARYSELECTIONOBSERVER, kDCNPrefix + 2)
DECLARE_PMID(kInterfaceIDSpace, IID_IXLIBRARYBUTTONDATA,		kDCNPrefix + 4)
DECLARE_PMID(kInterfaceIDSpace, IID_IPRODUCTIMAGEIFACE,			kDCNPrefix + 5)
DECLARE_PMID(kInterfaceIDSpace, IID_DCNITEXTMISCELLANYSUITEE,	kDCNPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDCNINTERFACE, kDCNPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDCNINTERFACE, kDCNPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDCNINTERFACE, kDCNPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDCNINTERFACE, kDCNPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDCNINTERFACE, kDCNPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDCNINTERFACE, kDCNPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDCNINTERFACE, kDCNPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDCNINTERFACE, kDCNPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDCNINTERFACE, kDCNPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDCNINTERFACE, kDCNPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDCNINTERFACE, kDCNPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDCNINTERFACE, kDCNPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDCNINTERFACE, kDCNPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDCNINTERFACE, kDCNPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDCNINTERFACE, kDCNPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDCNINTERFACE, kDCNPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kDCNActionComponentImpl,				kDCNPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kXLibraryItemViewPanelImpl,			kDCNPrefix + 1)
DECLARE_PMID(kImplementationIDSpace, kXLibraryItemGridPanelImpl,			kDCNPrefix + 2)
DECLARE_PMID(kImplementationIDSpace, kXLibraryItemGridEHImpl,				kDCNPrefix + 3)
DECLARE_PMID(kImplementationIDSpace, kXLibraryItemListBoxControllerImpl,	kDCNPrefix + 4)
DECLARE_PMID(kImplementationIDSpace, kXLibraryItemGridControllerImpl,		kDCNPrefix + 5)
DECLARE_PMID(kImplementationIDSpace, kXLibraryItemGridPanoramaImpl,			kDCNPrefix + 6)
DECLARE_PMID(kImplementationIDSpace, kXLibraryItemListViewImpl,				kDCNPrefix + 8)
DECLARE_PMID(kImplementationIDSpace, kXLibraryItemListViewControllerImpl,	kDCNPrefix + 9)
DECLARE_PMID(kImplementationIDSpace, kXLibraryItemListViewPanoramaImpl,		kDCNPrefix + 10)
DECLARE_PMID(kImplementationIDSpace, kXLibraryItemListViewEHImpl,			kDCNPrefix + 11)
DECLARE_PMID(kImplementationIDSpace, kXLibrarySelectionObserverImpl,		kDCNPrefix + 12)
DECLARE_PMID(kImplementationIDSpace, kXLibraryButtonDataImpl,				kDCNPrefix + 13)
DECLARE_PMID(kImplementationIDSpace, kXLibraryItemButtonImpl,				kDCNPrefix + 15)
DECLARE_PMID(kImplementationIDSpace, kXLibraryItemGridDDSourceImpl,			kDCNPrefix + 16)
DECLARE_PMID(kImplementationIDSpace, kDCNResizeControlViewImpl,				kDCNPrefix + 18)
DECLARE_PMID(kImplementationIDSpace, kProductImageIFaceImpl,				kDCNPrefix + 19)
DECLARE_PMID(kImplementationIDSpace, kDCNDNDCustomFlavorHelperImpl,			kDCNPrefix + 20)
DECLARE_PMID(kImplementationIDSpace, kDCNDragDropSourceEHImpl,				kDCNPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kDCNCustomDragSourceImpl,				kDCNPrefix + 22)
DECLARE_PMID(kImplementationIDSpace, kDCNDNDCustomDEHandlerImpl,			kDCNPrefix + 24)
DECLARE_PMID(kImplementationIDSpace, kXLibraryItemGridDDTargetImpl,			kDCNPrefix + 25)
//DECLARE_PMID(kImplementationIDSpace, kTbTextWidgetTipImpl,					kDCNPrefix + 26)
DECLARE_PMID(kImplementationIDSpace, kXLibraryInfoPanelControllerImpl,		kDCNPrefix + 27)
DECLARE_PMID(kImplementationIDSpace, kDCNFrameObserverImpl,					kDCNPrefix + 28)
DECLARE_PMID(kImplementationIDSpace, kDCNTextMiscellanySuiteASBImpl,		kDCNPrefix + 29)
DECLARE_PMID(kImplementationIDSpace, kDCNTextMiscellanySuiteLayoutCSBImpl,	kDCNPrefix + 30)
DECLARE_PMID(kImplementationIDSpace, kDCNIconWidgetObserverImpl,			kDCNPrefix + 31)
DECLARE_PMID(kImplementationIDSpace, kMyTipImpl,							kDCNPrefix + 32)
DECLARE_PMID(kImplementationIDSpace, kMyGridTipImpl,						kDCNPrefix + 33)
DECLARE_PMID(kImplementationIDSpace, kDCNLoginEventsHandlerImpl,			kDCNPrefix + 34)
/////////Added By Dattatray on 9/08/2006
DECLARE_PMID(kImplementationIDSpace, kDCNlstviewListBoxObserverImpl,		kDCNPrefix + 35)
DECLARE_PMID(kImplementationIDSpace, kDCNTrvCustomViewImpl,					kDCNPrefix + 36)
//added by vijay on 22/8/2006
DECLARE_PMID(kImplementationIDSpace, kDCNActionFilterImpl,					kDCNPrefix + 37)
DECLARE_PMID(kImplementationIDSpace, kMYframeObserverImpl,					kDCNPrefix + 38)	   
DECLARE_PMID(kImplementationIDSpace, kDCNChkBoxObserverImpl,					kDCNPrefix + 39)	
DECLARE_PMID(kImplementationIDSpace, kDCNFilterDialogControllerImpl,					kDCNPrefix + 40)	
DECLARE_PMID(kImplementationIDSpace, kDCNFilterDialogObserverImpl,					kDCNPrefix + 41)	

// ActionIDs:
DECLARE_PMID(kActionIDSpace, kDCNAboutActionID,				kDCNPrefix + 0)
DECLARE_PMID(kActionIDSpace, kDCNPanelWidgetActionID,		kDCNPrefix + 1)
DECLARE_PMID(kActionIDSpace, kDCNSeparator1ActionID,		kDCNPrefix + 11)
DECLARE_PMID(kActionIDSpace, kDCNPopupAboutThisActionID,	kDCNPrefix + 3)
DECLARE_PMID(kActionIDSpace, kDCNPopupItem1ActionID,		kDCNPrefix + 6)
DECLARE_PMID(kActionIDSpace, kDCNFitFrameToImageActionID,	kDCNPrefix + 7)
DECLARE_PMID(kActionIDSpace, kDCNFitImageToFrameActionID,	kDCNPrefix + 8)
DECLARE_PMID(kActionIDSpace, kDCNFCPActionID2,				kDCNPrefix + 9)  //for menu
DECLARE_PMID(kActionIDSpace, kDCNFFTCActionID3,				kDCNPrefix + 10)
DECLARE_PMID(kActionIDSpace, kDCNThumbViewActionID4,		kDCNPrefix + 12)
DECLARE_PMID(kActionIDSpace, kDCNListViewActionID5,			kDCNPrefix + 13)
DECLARE_PMID(kActionIDSpace, kDCNBrowseImageFolderActionID, kDCNPrefix + 14)
DECLARE_PMID(kActionIDSpace, kDCNBrowsePrintSourceActionID, kDCNPrefix + 15)
DECLARE_PMID(kActionIDSpace, kDCNPanelPSMenuActionID,		kDCNPrefix + 16)
DECLARE_PMID(kActionIDSpace, kDCNShowPVImagesActionID, 		kDCNPrefix + 17)
DECLARE_PMID(kActionIDSpace, kDCNShowPartnerImasgesActionID, kDCNPrefix + 18)
DECLARE_PMID(kActionIDSpace, kDCNSeparator2ActionID, 	kDCNPrefix + 19)
DECLARE_PMID(kActionIDSpace, kDCNSeparator3ActionID, kDCNPrefix + 20)
DECLARE_PMID(kActionIDSpace, kDCNSeparator4ActionID, kDCNPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kDCNActionID, kDCNPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kDCNActionID, kDCNPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kDCNActionID, kDCNPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kDCNActionID, kDCNPrefix + 25)


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kDCNPanelWidgetID,							kDCNPrefix + 0)
DECLARE_PMID(kWidgetIDSpace, kXLibraryItemViewPanelWidgetId,			kDCNPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kXLibraryItemScrollBarWidgetId,			kDCNPrefix + 3)
DECLARE_PMID(kWidgetIDSpace, kXLibraryItemListViewScrollBarWidgetId,	kDCNPrefix + 4)
DECLARE_PMID(kWidgetIDSpace, kXLibraryItemListBoxWidgetId,				kDCNPrefix + 5)
DECLARE_PMID(kWidgetIDSpace, kXLibraryItemGridWidgetId,					kDCNPrefix + 6)
DECLARE_PMID(kWidgetIDSpace, kDCNControlStripWidgetID,					kDCNPrefix + 7)
DECLARE_PMID(kWidgetIDSpace, kXLibraryPanelWidgetID0,					kDCNPrefix + 8)
DECLARE_PMID(kWidgetIDSpace, kXLibraryPanelWidgetID1,					kDCNPrefix + 9)
DECLARE_PMID(kWidgetIDSpace, kDCNFitConFrameWidgetID,					kDCNPrefix + 10)
//DECLARE_PMID(kWidgetIDSpace, kDCNFitFrameConWidgetID,					kDCNPrefix + 11)
DECLARE_PMID(kWidgetIDSpace, kDCNCentreConWidgetID,						kDCNPrefix + 12)
//DECLARE_PMID(kWidgetIDSpace, kDCNFitConPropornWidgetID,					kDCNPrefix + 13)
DECLARE_PMID(kWidgetIDSpace, kDCNFillFramePropornWidgetID,				kDCNPrefix + 14)
DECLARE_PMID(kWidgetIDSpace, kDCNFileNameWidgetID,						kDCNPrefix + 15)
DECLARE_PMID(kWidgetIDSpace, kDCNFileDescriptionWidgetID,				kDCNPrefix + 16)
DECLARE_PMID(kWidgetIDSpace, kDCNlstPnlWidgetID,						kDCNPrefix + 19)
DECLARE_PMID(kWidgetIDSpace, kDCNMultilineTextWidgetID,					kDCNPrefix + 20)
DECLARE_PMID(kWidgetIDSpace, kListboxlstviewParentWidgetId,				kDCNPrefix + 21)////////Added For ListView 
DECLARE_PMID(kWidgetIDSpace, kDCNlstviewListboxWidgetID,				kDCNPrefix + 22)
DECLARE_PMID(kWidgetIDSpace, kDCNlstviewCustomPanelViewWidgetID,		kDCNPrefix + 23)
DECLARE_PMID(kWidgetIDSpace, kDCNTextWidgetID,							kDCNPrefix + 24)
DECLARE_PMID(kWidgetIDSpace, kDCNThumbPnlWidgetID,						kDCNPrefix + 25)
DECLARE_PMID(kWidgetIDSpace, kDCNLstMultilineTextWidgetID,				kDCNPrefix + 26)
DECLARE_PMID(kWidgetIDSpace, kDCNPicExpanderWidgetID,					kDCNPrefix + 27)
DECLARE_PMID(kWidgetIDSpace, kDCNLstViewMultilineTextWidgetID,			kDCNPrefix + 28)

DECLARE_PMID(kWidgetIDSpace, kDCNListViewImageWidgetID,					kDCNPrefix + 29)//Added on 14/08 For ListView image
DECLARE_PMID(kWidgetIDSpace, kDCNlstViewImagePnlWidgetID,				kDCNPrefix + 30)//Added on 14/08 For ListView image
DECLARE_PMID(kWidgetIDSpace, kFitContentProportionallyButtonWidgetID,				kDCNPrefix + 31)
DECLARE_PMID(kWidgetIDSpace, kFitFrametoContentButtonWidgetID,				kDCNPrefix + 32)
DECLARE_PMID(kWidgetIDSpace, kPartnerImagesWidgetID,				kDCNPrefix + 33)
DECLARE_PMID(kWidgetIDSpace, kPickListImagesWidgetID,				kDCNPrefix + 34)
DECLARE_PMID(kWidgetIDSpace, kDCNFilterButtonWidgetID,				kDCNPrefix + 35)
DECLARE_PMID(kWidgetIDSpace, kFilterDialogWidgetID,				kDCNPrefix + 36)
DECLARE_PMID(kWidgetIDSpace, kDisplayPartnerImagesCheckBoxWidgetID,				kDCNPrefix + 37)
DECLARE_PMID(kWidgetIDSpace, kDisplayPickListImagesCheckBoxWidgetID,				kDCNPrefix + 38)
DECLARE_PMID(kWidgetIDSpace, kDCNSaveButtonWidgetID,				kDCNPrefix + 39)
DECLARE_PMID(kWidgetIDSpace, kDCNCancelButtonWidgetID,				kDCNPrefix + 40)
DECLARE_PMID(kWidgetIDSpace, kDCNFileSizeTextWidgetID,				kDCNPrefix + 41)




// "About Plug-ins" sub-menu:
#define kDCNAboutMenuKey			kDCNStringPrefix "kDCNAboutMenuKey"
#define kDCNAboutMenuPath		kSDKDefStandardAboutMenuPath kDCNCompanyKey

// "Plug-ins" sub-menu:
#define kDCNPluginsMenuKey 		kDCNStringPrefix "kDCNPluginsMenuKey"
#define kDCNPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kDCNCompanyKey kSDKDefDelimitMenuPath kDCNPluginsMenuKey

// Menu item keys:
#define kDCNItem1MenuKey		kDCNStringPrefix "kDCNItem1MenuKey"
#define kDCNFCPMenuItemKey2		kDCNStringPrefix "kDCNFCPMenuItemKey2"
#define kDCNFFTCMenuItemKey3		kDCNStringPrefix "kDCNFFTCMenuItemKey3"
#define kDCNThumbViewMenuItemKey4		kDCNStringPrefix "kDCNThumbViewMenuItemKey4"
#define kDCNListViewMenuItemKey5		kDCNStringPrefix "kDCNListViewMenuItemKey5"
// Other StringKeys:
#define kDCNAboutBoxStringKey	kDCNStringPrefix "kDCNAboutBoxStringKey"
#define kDCNPanelTitleKey					kDCNStringPrefix	"kDCNPanelTitleKey"
#define kDCNStaticTextKey kDCNStringPrefix	"kDCNStaticTextKey"
#define kDCNInternalPopupMenuNameKey kDCNStringPrefix	"kDCNInternalPopupMenuNameKey"
#define kDCNTargetMenuPath kDCNInternalPopupMenuNameKey
#define kDCNEmptyDialogKey		kDCNStringPrefix "kDCNEmptyDialogKey"

#define kDCNBrowseImageFolderStringKey kDCNStringPrefix "kDCNBrowseImageFolderStringKey"
#define kDCNBrowsePRINTSourceStringKey kDCNStringPrefix "kDCNBrowsePRINTSourceStringKey"
#define kDCNFitImageToFrameStringKey kDCNStringPrefix "kDCNFitImageToFrameStringKey"
#define kDCNFitFrameToImageStringKey kDCNStringPrefix "kDCNFitFrameToImageStringKey"
#define kDCNFilterStringKey kDCNStringPrefix "kDCNFilterStringKey"
#define kDCNDislayPartnerImagesStringKey kDCNStringPrefix "kDCNDislayPartnerImagesStringKey"
#define kDCNDisplayPickListImagesStringKey kDCNStringPrefix "kDCNDisplayPickListImagesStringKey"
#define kDCNBlankStringKey kDCNStringPrefix "kDCNBlankStringKey"

// Menu item positions:

#define kDCNAboutThisMenuItemPosition		15.0
#define kDCNMenuItem1Position				12.0
#define kDCNMenuItem2Position				9.0
#define kDCNMenuItem3Position				10.0
#define kDCNSeparator1MenuItemPosition		11.0
#define kDCNMenuItem4Position				12.0
#define kDCNMenuItem5Position				13.0

#define customFlavorAssetBr PMFlavor('APS4')

// Initial data format version numbers
#define kDCNFirstMajorFormatNumber  RezLong(1)
#define kDCNFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kDCNCurrentMajorFormatNumber kDCNFirstMajorFormatNumber
#define kDCNCurrentMinorFormatNumber kDCNFirstMinorFormatNumber

#define kDCNListElementRsrcID					1200 
#define kXLibraryInfoDialogRsrcID               1500
#define kDCNlstviewLstBoxRsrcID					1600

#define kFilterDialogResourceID					1601

// some grid dimensions
#define kDCNGridCellWidth						98//	64
#define kDCNGridCellHeight						92 //91.65//	77
#define kDCNGridDelta							(0.0)
#define kDCNListViewCellHeight					21

#define FitConFrameID				9000
#define FitFraneConID				9001
#define CentreContentID				9002
#define	FitConPropnID				9003
#define FillFramePropornID			9004
#define ListImageID					9005


//PNGR ID
#define kPNGFitContentProRsrcID					1000
#define kPNGFitContentProRollRsrcID             1000

#define kPNGFitFrametoContentRsrcID				1001   
#define kPNGFitFrametoContentRollRsrcID         1001

#define kPNGFilterRsrcID						1002   
#define kPNGFilterRollRsrcID					1002

#define kPNGSaveRsrcID							1003   
#define kPNGSaveRollRsrcID						1003

#define kPNGCancelRsrcID						1004   
#define kPNGCancelRollRsrcID					1004

#endif // __DCNID_h__
