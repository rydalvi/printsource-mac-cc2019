//========================================================================================
//  
//  $File: //depot/indesign_4.0/gm/source/sdksamples/wlistboxcomposite/DCNIconWidgetObserver.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2005/03/08 13:31:35 $
//  
//  $Revision: #1 $
//  
//  $Change: 323509 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Implementation includes
#include "WidgetID.h"

// Interface includes
#include "ISubject.h"
#include "ITriStateControlData.h"
#include "IPanelControlData.h"
// Implem includes
#include "CAlert.h"
#include "CObserver.h"

#include "ISelectionManager.h"
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
#include "CAlert.h"
#include "DCNID.h"
#include "IApplication.h"
#include "IAppFramework.h"
//#include "IPaletteMgr.h"
#include "IPanelMgr.h"
#include "ITextControlData.h"

#define CA(X) CAlert::InformationAlert(X)


/**
	Observes the "eyeball" widget.
	
	@ingroup wlistboxcomposite
	
*/
class DCNIconWidgetObserver : public CObserver
{
public:
	
	/**
		Constructor for WLBListBoxObserver class.
		@param interface ptr from boss object on which this interface is aggregated.
	*/
	DCNIconWidgetObserver(IPMUnknown *boss);

	/**
		Destructor for DCNIconWidgetObserver class - 
		performs cleanup 
	*/	
	~DCNIconWidgetObserver();

	/**
		AutoAttach is only called for registered observers
		of widgets.  This method is called by the application
		core when the widget is shown.
	
	*/	
	virtual void AutoAttach();

	/**
		AutoDetach is only called for registered observers
		of widgets. Called when widget hidden.
	*/	
	virtual void AutoDetach();

	/**
		Update when there's been a change on the eyeball widget.
	
		@param theChange this is specified by the agent of change; it can be the class ID of the agent,
		or it may be some specialised message ID.
		@param theSubject this provides a reference to the object which has changed; in this case, the button
		widget boss object that is being observed.
		@param protocol the protocol along which the change occurred.
		@param changedBy this can be used to provide additional information about the change or a reference
		to the boss object that caused the change.
	*/	
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);

	bool16 fitImageAndFrameToEachOtherOBS(int num);


private:
	/**
		Helper method to change state of list-box, dependent on whether
		it is being shown or hidden.
		@param isAttaching specifies whether attaching (shown) or detaching (hidden)
	*/
	void updateListBox(bool16 isAttaching);
	void AttachWidget(InterfacePtr<IPanelControlData>&  panelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
	void DetachWidget(InterfacePtr<IPanelControlData>& panelControlData, const WidgetID& widgetID, const PMIID& interfaceID);

	const PMIID fObserverIID;
	
};

CREATE_PMINTERFACE(DCNIconWidgetObserver, kDCNIconWidgetObserverImpl)


DCNIconWidgetObserver::DCNIconWidgetObserver(IPMUnknown* boss)
: CObserver(boss), fObserverIID(IID_ITRISTATECONTROLDATA)
{
	
}


DCNIconWidgetObserver::~DCNIconWidgetObserver()
{
	
}


void DCNIconWidgetObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ITRISTATECONTROLDATA);
	}
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CA("ptrIAppFramework == nil");			
			return;
		}

		bool16 result=ptrIAppFramework->getLoginStatus();

		if(!result)
		{
			InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
			if(iApplication == nil)
			{
				//CA("XLibraryItemGridController::UpdateGridLayout::No iApplication");		
				return ;
			}

			InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
			if(iPanelMgr == NULL)
			{
				//CA("XLibraryItemGridController::UpdateGridLayout::No iPanelMgr");
				return ;
			}
			const ActionID MyPalleteActionID = kDCNPanelWidgetActionID;
			IControlView *ctrlView = iPanelMgr->GetPanelFromActionID(MyPalleteActionID);
			if(ctrlView == NULL)
				CAlert::InformationAlert("still i m not getting");

			InterfacePtr<IPanelControlData> panelControlData(ctrlView, UseDefaultIID());
			if(panelControlData == NULL)
			{
				//CA("XLibraryItemGridController::UpdateGridLayout::panelControlData is NULL");	
				return;
			}
			IControlView* textView = panelControlData->FindWidget( kDCNMultilineTextWidgetID);
			if(textView == NULL)
			{
				//CA("XLibraryItemGridController::UpdateGridLayout::textView is NULL");		
				return;
			}

			InterfacePtr<ITextControlData> textPtr( textView ,IID_ITEXTCONTROLDATA);
			if(textPtr == NULL)
			{
				//CA("XLibraryItemGridController::UpdateGridLayout::textPtr is NULL");		
				return;
			}
			PMString blank("");
			blank.SetTranslatable(kFalse);
			textPtr->SetString(blank);


			IControlView* textView1 = panelControlData->FindWidget( kDCNFileSizeTextWidgetID);
			if(textView == NULL)
			{
				//CA("XLibraryItemGridController::UpdateGridLayout::textView is NULL");		
				return;
			}

			InterfacePtr<ITextControlData> textPtr1( textView1 ,IID_ITEXTCONTROLDATA);
			if(textPtr == NULL)
			{
				//CA("XLibraryItemGridController::UpdateGridLayout::textPtr is NULL");		
				return;
			}			
			textPtr->SetString(blank);
		}
	}
	do
	{
		InterfacePtr<IPanelControlData> panelData(this, UseDefaultIID());
		ASSERT(panelData);
		if (panelData == nil)
		{ 
			//CA("panelData is nil");
			break;
		}
		
		
		AttachWidget(panelData, kDCNFitConFrameWidgetID, IID_ITRISTATECONTROLDATA);
		//AttachWidget(panelData, kFitFrametoContentButtonWidgetID/*kDCNFitFrameConWidgetID*/, IID_ITRISTATECONTROLDATA);
		AttachWidget(panelData, kDCNCentreConWidgetID, IID_ITRISTATECONTROLDATA);
		AttachWidget(panelData, kFitContentProportionallyButtonWidgetID/*kDCNFitConPropornWidgetID*/, IID_ITRISTATECONTROLDATA);
		AttachWidget(panelData, kDCNFillFramePropornWidgetID, IID_ITRISTATECONTROLDATA);
		//AttachWidget(panelData, kDCNListViewImageWidgetID, IID_ITRISTATECONTROLDATA);
		
	}while(0);
}


void DCNIconWidgetObserver::AutoDetach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this,IID_ITRISTATECONTROLDATA);
	}

	do
	{
		InterfacePtr<IPanelControlData> panelData(this, UseDefaultIID());
		ASSERT(panelData);
		if (panelData == nil)
		{
			break;
		}
		
		DetachWidget(panelData, kDCNFitConFrameWidgetID, IID_ITRISTATECONTROLDATA);
		//DetachWidget(panelData, kFitFrametoContentButtonWidgetID/*kDCNFitFrameConWidgetID*/, IID_ITRISTATECONTROLDATA);
		DetachWidget(panelData, kDCNCentreConWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(panelData, kFitContentProportionallyButtonWidgetID/*kDCNFitConPropornWidgetID*/, IID_ITRISTATECONTROLDATA);
		DetachWidget(panelData, kDCNFillFramePropornWidgetID, IID_ITRISTATECONTROLDATA);
	//	DetachWidget(panelData, kDCNListViewImageWidgetID, IID_ITRISTATECONTROLDATA);
	}while(0);
}

void DCNIconWidgetObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	do
	{
		// Take action based on the message protocol
		if (theChange == kTrueStateMessage)
		{
			InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
			ASSERT(controlView != nil);
			if (controlView==nil)
			{
				break;
			}

			WidgetID widgetID = controlView->GetWidgetID();
			switch (widgetID.Get())
			{				
				case kDCNFitConFrameWidgetID:
					this->fitImageAndFrameToEachOtherOBS(1);
					break;
			//	case kFitFrametoContentButtonWidgetID/*kDCNFitFrameConWidgetID*/:
			//		this->fitImageAndFrameToEachOtherOBS(2);
			//		break;
			//	case kFitContentProportionallyButtonWidgetID/*kDCNFitConPropornWidgetID*/:
			//		this->fitImageAndFrameToEachOtherOBS(3);
			//		break;
			//	
				case kDCNCentreConWidgetID:
					this->fitImageAndFrameToEachOtherOBS(4);
					break;
				case kDCNFillFramePropornWidgetID:
					this->fitImageAndFrameToEachOtherOBS(5);
					break;
				/*case kDCNListViewImageWidgetID:
					this->fitImageAndFrameToEachOtherOBS(6);
					break;*/
				default:
					break;
			}

		}
	}while(0);

}


/* attachWidget
*/
void DCNIconWidgetObserver::AttachWidget(InterfacePtr<IPanelControlData>& panelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	ASSERT(panelControlData);

	//TRACE("TblAttWidgetObserver::attachWidget(widgetID=0x%x, interfaceID=0x%x\n", widgetID, interfaceID);
	do
	{
		if (panelControlData == nil) break;
		
		IControlView* controlView = panelControlData->FindWidget(widgetID);
		ASSERT(controlView);
		if (controlView == nil)
		{
			break;
		}

		InterfacePtr<ISubject> subject(controlView, UseDefaultIID());
		ASSERT(subject);
		if (subject == nil)
		{
			break;
		}
		subject->AttachObserver(this, interfaceID, fObserverIID);
	}
	while (kFalse);
}

/* detachWidget
*/
void DCNIconWidgetObserver::DetachWidget(InterfacePtr<IPanelControlData>& panelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	ASSERT(panelControlData);
	do
	{
		if (panelControlData == nil) break;

		IControlView* controlView = panelControlData->FindWidget(widgetID);
		ASSERT(controlView);
		if (controlView == nil)
		{
			break;
		}

		InterfacePtr<ISubject> subject(controlView, UseDefaultIID());
		ASSERT(subject);
		if (subject == nil)
		{
			break;
		}
		subject->DetachObserver(this, interfaceID, fObserverIID);
	}
	while (kFalse);
}

bool16 DCNIconWidgetObserver::fitImageAndFrameToEachOtherOBS(int num)
{
//	
//	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
//	if(!iSelectionManager)
//	{	
//		return kFalse ;
//	}
//
//	InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
//	( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID ,iSelectionManager))); 
//	if(!txtMisSuite)
//	{	
//		return kFalse ; 
//	}
//
//	switch(num)
//	{		
//		case 1 :
//		txtMisSuite->setFrameUser(1); // then FitContentToFrame .....
//		break ;
//
//		case 2 :
//		txtMisSuite->setFrameUser(2); // FitFrameToContent ......
//		break ;
//
//		case 3 :
//		txtMisSuite->setFrameUser(3); // FitContentProp ......
//		break ;
//
//		case 4 :
//		txtMisSuite->setFrameUser(4); //CenterContentInFrame ......
//		break ;
//
//		case 5 :
//		txtMisSuite->setFrameUser(5); // FillFrameProp ......
//		break ;
//		
//	/*	case 6 :
//		txtMisSuite->setFrameUser(6); // FillFrameProp ......*/
//		default:
//			break;
//	}
//
	return kTrue ;
}

