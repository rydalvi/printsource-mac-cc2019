#include "VCPlugInHeaders.h"
#include "CommonFunction.h"
#include "CAlert.h"
#include "SDKUtilities.h"
#include "IDocument.h"
#include "ILayoutUIUtils.h"
#include "ICommand.h"
#include "IHierarchy.h"
//#include "IImportFileCmdData.h"
#include "IPlaceGun.h"
#include "OpenPlaceID.h"
#include "IReplaceCmdData.h"
#include "IIDXMLElement.h"
#include "IXMLUtils.h"
#include "IXMLAttributeCommands.h"
#include "ISpreadList.h"
#include "ISpread.h"
#include "ITriStateControlData.h"
#include "IMasterSpreadList.h"
#include "IGeometry.h"
#include "TransformUtils.h"
#include "IScrapItem.h"
#include "CommonFunction.h"
#include "IURIUtils.h" //cs4
#include "IImportResourceCmdData.h"
#include "IAppFramework.h"

#include "AssetInfo.h"

#define CA(x) CAlert::InformationAlert(x)
#define CA_NUM(i,Message,NUM) PMString K##i(Message);K##i.AppendNumber(NUM);CA(K##i)

//extern K2Vector<PMString>imageType;
//extern K2Vector<PMString> vectorImageTypeTagName;
//extern K2Vector<int32>imageTypeID;
extern int32 imageIndex ;  
extern double global_lang_id ;
extern double global_section_id ;
extern double global_parent_id ;
extern double global_parent_type_id ;
extern double globle_item_parenttype_id;
extern int32 global_item_start_position ;
//extern K2Vector<int32> itemIDS ;
extern double global_product_id ;
extern double global_product_type_id ;
//extern K2Vector<int32> imageMPVID ;

extern AssetInfo assetInfoObj;

bool16 CommonFunction::ImportFileInFrame(const UIDRef& imageBox,  PMString& fromPath)
{

	if(imageBox.GetUID() == kInvalidUID){
		//CA("InValid UID"); 
		return kFalse;
	}
    fromPath.ParseForEmbeddedCharacters();
	bool16 fileExists =  (SDKUtilities::FileExistsForRead(fromPath) == kSuccess);
	if(!fileExists) 
		return kFalse;	
	
	
	IDocument* doc =Utils<ILayoutUIUtils>()->GetFrontDocument(); 
	IDataBase* db = ::GetDataBase(doc); 
	
	if (db == nil)
		return kFalse;
	
	IDFile sysFile = SDKUtilities::PMStringToSysFile(const_cast<PMString* >(&fromPath));	
	InterfacePtr<ICommand> importCmd(CmdUtils::CreateCommand(kImportAndLoadPlaceGunCmdBoss));
	if(!importCmd) 
		return kFalse;
	
	//***** Commented By Sachin Sharma
	//InterfacePtr<IImportFileCmdData> importFileCmdData(importCmd, IID_IIMPORTFILECMDDATA); // no DefaultIID for this
	//if(!importFileCmdData){
	//	CA(" !importFileCmdData ");
	//	return kFalse;
	//}
	//
	//importFileCmdData->Set(db, sysFile, kMinimalUI,kTrue,kTrue,kTrue);
	//******

	//*** Added By  Sachin Sharma for Cs4
	URI tmpURI;
	Utils<IURIUtils>()->IDFileToURI(sysFile, tmpURI);
	InterfacePtr<IImportResourceCmdData> importFileCmdData(importCmd, IID_IIMPORTRESOURCECMDDATA); // no kDefaultIID	
	if (importFileCmdData == nil) {
		//CA(" importResourceCmdData == nil ");
		return kFalse;
	}	
	importFileCmdData->Set(db,tmpURI,kMinimalUI);
	//***

	ErrorCode err = CmdUtils::ProcessCommand(importCmd);
	if(err != kSuccess) 
		return kFalse;
	
	InterfacePtr<IPlaceGun> placeGun(db, db->GetRootUID(), UseDefaultIID());
	if(!placeGun)
		return kFalse;
	
	UIDRef placedItem(db, placeGun->GetFirstPlaceGunItemUID());       //GetItemUID());  Modified By Sachin Sharma 22/06/07
	
	InterfacePtr<ICommand> replaceCmd(CmdUtils::CreateCommand(kReplaceCmdBoss));
	if (replaceCmd == nil)
		return kFalse;
	
	InterfacePtr<IReplaceCmdData>iRepData(replaceCmd, IID_IREPLACECMDDATA);
	if(!iRepData)
		return kFalse;
	
	iRepData->Set(db, imageBox.GetUID(), placedItem.GetUID(), kFalse);
	
	ErrorCode status = CmdUtils::ProcessCommand(replaceCmd);
	if(status==kFailure)
	{
		//CA("status==kFailure");	
		return kFalse;
	}
	return kTrue;
}

bool16 CommonFunction::fileExists(PMString& fromPath)
{
	PMString theName("");
	theName.Append(fromPath);

    
#ifdef MACINTOSH
    {
        InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
        if(ptrIAppFramework == NULL)
        {
            //CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
            return kFalse;
        }
        
        ptrIAppFramework->getUnixPath(theName);
    }
#endif
    
	std::string tempString(theName.GetPlatformString());
	const char *file= const_cast<char *> (tempString.c_str());
	//CA("theName  :  "+theName);
	FILE *fp=NULL;
    
	fp=std::fopen(file, "r");
	if(fp!=NULL)
	{
		std::fclose(fp);
		return kTrue;
	}
	return kFalse;
}

void CommonFunction::fitImageInBox(const UIDRef& boxUIDRef)
{
	do 
	{
		InterfacePtr<ICommand> iAlignCmd(CmdUtils::CreateCommand(kFitContentPropCmdBoss));
		
		if(!iAlignCmd)
			return;

		InterfacePtr<IHierarchy> iHier(boxUIDRef, IID_IHIERARCHY);
		if(!iHier)
			return;

		if(iHier->GetChildCount()==0)//There may not be any image at all????
			return;

		UID childUID=iHier->GetChildUID(0);

		UIDRef newChildUIDRef(boxUIDRef.GetDataBase(), childUID);


		iAlignCmd->SetItemList(UIDList(newChildUIDRef));

		CmdUtils::ProcessCommand(iAlignCmd);
	} while (false);
}


void CommonFunction::addTagToGraphicFrame(UIDRef curBox,int32 selIndex,int32 selTab)
{
	//CA("CommonFunction::addTagToGraphicFrame");
	
	do{
		
		InterfacePtr<IPMUnknown> unknown(curBox, IID_IUNKNOWN);
		if(unknown == nil)
		{
			break;
		}
		InterfacePtr<IDocument> doc(curBox.GetDataBase(), curBox.GetDataBase()->GetRootUID(), UseDefaultIID());
		if(doc == nil){
			//CA("Document not found...");
			break;
		}
		InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(doc));
		if (rootElement == nil){			
			//CA("IIDXMLElement NIL...");
			break;
		}
	
		XMLReference parent = rootElement->GetXMLReference();
		PMString storyTagName =	this->RemoveWhileSpace(assetInfoObj.vectorImageTypeTagName[selIndex]);///*vectorImageTypeTagName[selIndex]*/);
		storyTagName = this->keepOnlyAlphaNumeric(storyTagName);
		//CA(storyTagName);
		XMLReference storyXMLRef = this->TagFrameElement(parent, curBox.GetUID(),storyTagName);
	
		this->attachAttributes(&storyXMLRef,selIndex,selTab);
	}while(kFalse);
}

// Method Purpose: Use to attach XML tag structure to the Frame (Graphics/Text frame with Table).
XMLReference CommonFunction::TagFrameElement
(const XMLReference& newElementParent,UID frameUID, const PMString& frameTagName)
{
	XMLReference resultXMLRef=kInvalidXMLReference;
	do {
		// Acquire the IXLMElementCommands interface on the Utils boss, and use its method
		// to tag the frame.  Arbitrarily ask for the element to be the 0th child of it's parent.
		ErrorCode errCode = Utils<IXMLElementCommands>()->CreateElement(WideString(frameTagName), frameUID, newElementParent, 0, &resultXMLRef); //Cs4
		// Verify the results: no errors, valid XMLRef returned, we can instantiate it.
		if (errCode != kSuccess)
		{
			//CA("ExpXMLActionComponent::TagFrameElement - CreateElement failed");
			break;
		}		
		if (resultXMLRef == kInvalidXMLReference)
		{
			//CA("ExpXMLActionComponent::TagFrameElement - Can't create new XMLReference");
			break;
		}		
		InterfacePtr<IIDXMLElement> newXMLElement (resultXMLRef.Instantiate());
		if (newXMLElement==nil) 
		{
			//CA("ExpXMLActionComponent::TagFrameElement - Can't instantiate new XML element");
		}		
	}while (false);

	return resultXMLRef;
}

//Method Purpose: Replace the white spaces with '_' 
PMString CommonFunction::RemoveWhileSpace(PMString name)
{
	PMString tagName("");
	for(int i=0;i<name.NumUTF16TextChars();i++)
	{
		if(name.GetChar(i) ==' '){
			tagName.Append("_");
		}
		else tagName.Append(name.GetChar(i));
	}
	return tagName;
}


// Methos Purpose: adds attributes to XMLelement
void CommonFunction::attachAttributes
(XMLReference* newTag,int32 selIndex,int32 selTab)
{	
	//CA_NUM(789 , " selected index " , selIndex);

	PMString attribName("ID");
	PMString attribVal("");
	attribVal.AppendNumber(PMReal(assetInfoObj.attributeID[selIndex]));//attribVal.AppendNumber(-1);
	//if(flag){
	//	attribVal="-1";
	//} 
	ErrorCode err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	
	attribName.Clear();
	attribVal.Clear();
	attribName = "typeId";
	attribVal.AppendNumber(PMReal(assetInfoObj.imageTypeID[selIndex]));//imageTypeID[selIndex]);
	//if(flag){
	//	attribVal="-1";
	//}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4

	attribName.Clear();
	attribVal.Clear();
	attribName = "header";
	attribVal.AppendNumber(-1);
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4

	attribName.Clear();
	attribVal.Clear();
	attribName = "isEventField";
	attribVal.AppendNumber(-1);
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4

	attribName.Clear();
	attribVal.Clear();
	attribName = "deleteIfEmpty";
	attribVal.AppendNumber(-1);
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName = "dataType";
	attribVal.AppendNumber(-1);
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName = "isAutoResize";
	//if(this->getAutoResizeChkboxState())
	//attribVal.AppendNumber(1);
	//else
	attribVal.AppendNumber(0);
	//if(flag){
	//	attribVal="-1";
	//}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4

	attribName.Clear();
	attribVal.Clear();
	attribName = "LanguageID";	
	//if(lstboxInfo.id != -1 && lstboxInfo.tableFlag == 1)
	attribVal.AppendNumber(PMReal(/*Mediator::languageID*/global_lang_id));
	//else
	//	attribVal.AppendNumber(lstboxInfo.LanguageID);

	//if(flag){
	//	attribVal="NULL";
	//}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4

	attribName.Clear();
	attribVal.Clear();
	attribName = "index";
	if(global_item_start_position < selIndex)
		attribVal.AppendNumber(4);
	else
		attribVal.AppendNumber(3);
	//if(flag){
	//	attribVal="-1";
	//}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4

	attribName.Clear();
	attribVal.Clear();
	attribName = "pbObjectId";
	attribVal.AppendNumber(-1);
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();	
	attribName = "parentID";
	if(global_item_start_position < selIndex)
		attribVal.AppendNumber(PMReal(assetInfoObj.itemIDS[selIndex]));//itemIDS[selIndex]);
	else
		attribVal.AppendNumber(PMReal(global_product_id));
	//if(flag){
	//	attribVal="-1";
	//}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//CS4

	attribName.Clear();
	attribVal.Clear();
	attribName = "childId";
	attribVal.AppendNumber(-1);
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName = "sectionID";
	attribVal.AppendNumber(PMReal(global_section_id));
	//if(flag){
	//	attribVal="-1";
	//}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//CS4
	
	attribName.Clear();
	attribVal.Clear();
	attribName = "parentTypeID";
	//global_product_type_id
	if(global_item_start_position < selIndex)
	{
		//CA("global_item_start_position < selIndex");
		attribVal.AppendNumber(PMReal(globle_item_parenttype_id));
	}
	else
	{
		//CA("else global_item_start_position < selIndex");
		attribVal.AppendNumber(PMReal(global_parent_type_id));
	}
	
	//if(flag){
	//	attribVal="-1";
	//}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//CS4

	attribName.Clear();
	attribVal.Clear();
	attribName = "isSprayItemPerFrame";
	attribVal.AppendNumber(-1);
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName = "catLevel";
	attribVal.AppendNumber(-1);
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	
	attribName.Clear();
	attribVal.Clear();
	attribName = "imgFlag";
	attribVal.AppendNumber(1/*lstboxInfo.isImageFlag*/);
	//if(flag){
	//	attribVal="-1";
	//}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	
	attribName.Clear();
	attribVal.Clear();
	attribName = "imageIndex";
	attribVal.AppendNumber(PMReal(assetInfoObj.PVImageIndex[selIndex]));
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName = "flowDir";
	attribVal.AppendNumber(-1);
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName = "childTag";
	attribVal.AppendNumber(-1);
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
	
	attribName.Clear();
	attribVal.Clear();
	attribName = "tableFlag";
	attribVal.AppendNumber(0/*lstboxInfo.tableFlag*/);
	//if(flag){
	//	attribVal="-1";
	//}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4

	attribName.Clear();
	attribVal.Clear();
	attribName = "tableType";
	attribVal.AppendNumber(-1);
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
	
	attribName.Clear();
	attribVal.Clear();
	attribName = "tableId";
	attribVal.AppendNumber(-1);
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
	
	attribName.Clear();
	attribVal.Clear();
	attribName = "rowno";
	attribVal.AppendNumber(PMReal(assetInfoObj.imageMPVID[selIndex]));//imageMPVID[selIndex]);
	//if(flag){
	//	attribVal="-1";
	//}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4

	attribName.Clear();
	attribVal.Clear();
	attribName = "colno";
	attribVal.AppendNumber(-1);
	//if(flag){
	//	attribVal="-1";
	//}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName = "field1";
	attribVal.AppendNumber(-1);
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName = "field2";
	attribVal.AppendNumber(-1);
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName = "field3";
	attribVal.AppendNumber(-1);
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName = "field4";
	attribVal.AppendNumber(-1);
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

	attribName.Clear();
	attribVal.Clear();
	attribName = "field5";
	attribVal.AppendNumber(-1);
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
    
    attribName.Clear();
    attribVal.Clear();
    attribName = "groupKey";
    attribVal.Append("");
    err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));

}




/*
 purpose to check wether image frame already dragged
*/

int16 CommonFunction::checkForOverLaps
(UIDRef draggedItem, int32 spreadNumber, UIDRef& overlappingItem, UIDList& theList)
{
	
	IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();
	if(fntDoc==nil){
	return -1;}

	InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
	if (layout == nil){
	return -1;}

	IDataBase* database = ::GetDataBase(fntDoc);
	if(database==nil){
	return -1;}

	InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
	if (iSpreadList==nil){
	return -1;}
	
	bool16 collisionFlag=kFalse;
	for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
	{
		UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));

		InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
		if(!spread)
			return -1;

		UIDList allPageItems(database);
		int numPages=spread->GetNumPages();

		for(int i=0; i<numPages; i++)
		{
			UIDList tempList(database);
			spread->GetItemsOnPage(i, &tempList, kFalse ,kTrue);
			allPageItems.Append(tempList);
		}

		PMRect theArea, dragItemArea;
		PMRect smallestItem(0,0,0,0);
		if(!this->getBoxDimensions(draggedItem, dragItemArea))
		{
		return 0;}

		PMReal centerX, centerY;
		centerX=(dragItemArea.Bottom()+dragItemArea.Top())/2;
		centerY=(dragItemArea.Right()+dragItemArea.Left())/2;

		for(int32 i=0; i<allPageItems.Length(); i++)
		{
			if(allPageItems.GetRef(i)==draggedItem)
				continue;
			if(this->getBoxDimensions(allPageItems.GetRef(i), theArea)<1)
				continue;

			//This box may have some kids
			InterfacePtr<IHierarchy> iChild(allPageItems.GetRef(i), IID_IHIERARCHY);
			if(iChild)
			{
				UID kidUID;
				int numKids=iChild->GetChildCount();
				
				for(int j=0; j<numKids-1; j++)
				{
					kidUID=iChild->GetChildUID(j);
					allPageItems.Append(kidUID);
				}
			}
			if(centerX>=theArea.Top() && centerY>=theArea.Left() && centerX<=theArea.Bottom() && centerY<=theArea.Right())
			{
				if(!collisionFlag)
				{
					collisionFlag=kTrue;
					smallestItem=theArea;
					overlappingItem=allPageItems.GetRef(i);
					theList=allPageItems;
					continue;
				}
				if((theArea.Bottom()-theArea.Top()) < (smallestItem.Bottom()-smallestItem.Top()) ||
					(theArea.Right()-theArea.Left()) < (smallestItem.Right()-smallestItem.Left()))
				{
					overlappingItem=allPageItems.GetRef(i);
					theList=allPageItems;
					smallestItem=theArea;
				}
			}
		}
	}

	if(collisionFlag)
	{
		//CA("collision flag return 1 here");
	    return 1;collisionFlag=kTrue;
	}
	return 0;
}


int16 CommonFunction::getBoxDimensions(UIDRef theBox, PMRect& bind)
{
	PMRect theArea;
	
	InterfacePtr<IGeometry> iGeometry(theBox, UseDefaultIID());
	if(iGeometry==nil)
		return kFalse;

	theArea=iGeometry->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeometry)));
	bind=theArea;
	return kTrue;
}


int16 CommonFunction::deleteThisBox(UIDRef boxUIDRef)
{
	InterfacePtr<IScrapItem> scrap(boxUIDRef, UseDefaultIID());
	if(scrap==nil)
		return 0;
	InterfacePtr<ICommand> command (scrap->GetDeleteCmd());
	if(!command)
		return 0;
	command->SetItemList(UIDList(boxUIDRef));
	if(CmdUtils::ProcessCommand(command)!=kSuccess)
		return 0;
	return 1;
}

PMString CommonFunction::keepOnlyAlphaNumeric(PMString name)
{
	//CA(__FUNCTION__);
	PMString tagName("");

	for(int i=0;i<name.NumUTF16TextChars(); i++)
	{
		bool isAlphaNumeric = false ;

		PlatformChar ch = name.GetChar(i);
		
		if(i == 0 && ch.IsNumber())
			ch.Set(' ');
			
			

		if(ch.IsAlpha() || ch.IsNumber())
			isAlphaNumeric = true ;

		if(ch.IsSpace())
		{
			isAlphaNumeric = true ;
			ch.Set('_') ;
		}

		if(ch == '_')
			isAlphaNumeric = true ;
	
		if(isAlphaNumeric) 
			tagName.Append(ch);
	}

	return tagName ;
}
