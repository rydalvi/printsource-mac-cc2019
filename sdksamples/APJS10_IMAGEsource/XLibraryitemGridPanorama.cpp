
#include "VCPlugInHeaders.h"

// ----- Interface Includes -----
#include "IGridAttributes.h"
#include "IPanelControlData.h"

// ----- Implementation Includes -----
#include "CPanorama.h"
#include "CAlert.h"
#include "DCNID.h"

#define CA(X) CAlert::InformationAlert(X);
class XLibraryItemGridPanorama : public CPanorama
{
// ----- Constructors/desctructors
public:
	XLibraryItemGridPanorama(IPMUnknown *boss);	
	virtual ~XLibraryItemGridPanorama() { }

// ----- Panorama attribtues
public:
	virtual PMRect		GetBounds() const;
	virtual void		GetPanoramaDelta(PMPoint& delta) const;
	virtual void		GetPanoramaDeltaMultiple(PMPoint& delta) const;
};

CREATE_PMINTERFACE(XLibraryItemGridPanorama, kXLibraryItemGridPanoramaImpl)

XLibraryItemGridPanorama::XLibraryItemGridPanorama(IPMUnknown *boss) :
	CPanorama(boss)
{
}

//----------------------------------------------------------------------------------------
// XLibraryItemGridPanorama::GetBounds
//----------------------------------------------------------------------------------------

PMRect XLibraryItemGridPanorama::GetBounds() const
{
	//CA("GetBounds");
	// ----- Based on how many child widgets are in the grid and the grid dimensions,
	//		 calculate the bounds of the panorama. [amb]
	//CAlert::InformationAlert("GetBounds");

	InterfacePtr<IPanelControlData> panelData(this, IID_IPANELCONTROLDATA);
	if(panelData == NULL)
	{
		CAlert::InformationAlert("PanelData iss nil");
	}
	InterfacePtr<IGridAttributes> attrs(this, IID_IGRIDATTRIBUTES);
	if(attrs == nil)
	{
		CAlert::InformationAlert("attrs iss nil");
	}

	PMPoint gridDims = attrs->GetGridDimensions();
	PMPoint cellSpacing = attrs->GetCellDimensions() 
							+ PMPoint(attrs->GetBorderWidth(), attrs->GetBorderWidth() + kDCNGridDelta);
	//CS3
	int16 rows = 0;

	PMString newD("Zero::");
	newD.AppendNumber(panelData->Length());
	newD.Append("^^^");
	newD.AppendNumber(::ToInt16(gridDims.X()));
	//CAlert::InformationAlert(newD);

	if(::ToInt16(gridDims.X()) == 0)
	{
		return PMRect(0, 0, gridDims.X() * cellSpacing.X() -1.0 , rows * cellSpacing.Y() -1.0);
	}
	rows = panelData->Length() / ::ToInt16(gridDims.X());
	if (panelData->Length() % ::ToInt16(gridDims.X()) != 0)
		rows++;
	
		
	


	// Remember to remove the outermost borders
	return PMRect(0, 0, gridDims.X() * cellSpacing.X() -1.0 , rows * cellSpacing.Y() -1.0);
}

//----------------------------------------------------------------------------------------
// XLibraryItemGridPanorama::GetPanoramaDelta
//----------------------------------------------------------------------------------------

void XLibraryItemGridPanorama::GetPanoramaDelta(PMPoint& delta) const
{
	//CA("GetPanoramaDelta");
	InterfacePtr<IGridAttributes> attrs( this, IID_IGRIDATTRIBUTES);
	// 1 Point is added because of Bug#461581
	PMPoint cellSpacing = attrs->GetCellDimensions() 
							+ PMPoint(attrs->GetBorderWidth(), attrs->GetBorderWidth() + kDCNGridDelta);

	// ----- Set increment to scroll by one cell at a time.
	// x is 0 since there isn't a horizontal scroll bar.
	delta.X(0);
	delta.Y(cellSpacing.Y() / GetYScaleFactor());
}

//----------------------------------------------------------------------------------------
// XLibraryItemGridPanorama::GetPanoramaDeltaMultiple
//----------------------------------------------------------------------------------------

void XLibraryItemGridPanorama::GetPanoramaDeltaMultiple(PMPoint& delta) const
{
	//CA("GetPanoramaDeltaMultiple");
	InterfacePtr<IGridAttributes> attrs(this, IID_IGRIDATTRIBUTES);
	InterfacePtr<IControlView> view(this, IID_ICONTROLVIEW);
	PMRect viewRt(view->GetBBox());

	PMPoint gridDims = attrs->GetGridDimensions();
	PMPoint cellSpacing = attrs->GetCellDimensions() 
							+ PMPoint(attrs->GetBorderWidth(), attrs->GetBorderWidth() + kDCNGridDelta);

	// ----- Set increment multiple to scroll by the size of the view.
	// ----- Round to nearest cell size multiple	
	delta.X(0);
	delta.Y(cellSpacing.Y()*Round(viewRt.Height()/cellSpacing.Y()) / GetYScaleFactor());
}