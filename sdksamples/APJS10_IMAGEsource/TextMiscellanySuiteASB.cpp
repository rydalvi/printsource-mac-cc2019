#include "VCPlugInHeaders.h"


#include "ITextMiscellanySuite.h"

#include "CmdUtils.h"
#include "UIDList.h"
#include "SelectionASBTemplates.tpp"
#include <ITextMiscellanySuite.h>


class TextMiscellanySuiteASB : public CPMUnknown<ITextMiscellanySuite>
{
	public:
		TextMiscellanySuiteASB(IPMUnknown *iBoss);
	
	/** Destructor. */
	virtual ~TextMiscellanySuiteASB(void);

	//virtual bool16 GetCurrentSpecifier(ISpecifier *&);
	virtual bool16 GetUidList(UIDList &);
	virtual bool16 setFrameUser(int32);
};

CREATE_PMINTERFACE(TextMiscellanySuiteASB, kDCNTextMiscellanySuiteASBImpl)

TextMiscellanySuiteASB::TextMiscellanySuiteASB(IPMUnknown* iBoss) :
CPMUnknown<ITextMiscellanySuite>(iBoss)
{
}

TextMiscellanySuiteASB::~TextMiscellanySuiteASB(void)
{

}

bool16 TextMiscellanySuiteASB::GetUidList(UIDList & TempUIDList)
{
	return (AnyCSBSupports(make_functor(&ITextMiscellanySuite::GetUidList,TempUIDList), this));
}
bool16 TextMiscellanySuiteASB::setFrameUser(int32 OptionSelected)
{
	return (AnyCSBSupports(make_functor(&ITextMiscellanySuite::setFrameUser,OptionSelected), this));
}