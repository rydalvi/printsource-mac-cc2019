
#include "VCPlugInHeaders.h"

// ----- Interfaces -----

#include "IApplication.h"
#include "IBooleanControlData.h"
#include "IControlView.h"
#include "IDialogController.h"
#include "IDialogMgr.h"
#include "IDragDropController.h"
#include "IDragDropSource.h"
#include "IEvent.h"
#include "IXLibraryButtonData.h"
//#include "ILibraryAsset.h"
//#include "ILibraryAssetProxy.h"
//#include "ILibraryItemButtonData.h"
#include "IXLibraryViewController.h"
#include "IListBoxController.h"
#include "IPanelControlData.h"
#include "ISession.h"
#include "ISubject.h"
#include "IWidgetParent.h"
#include "IWindow.h"
#include "IWindowPort.h"
#include "IWidgetParent.h"
#include "ITextControlData.h"
#include "IWidgetUtils.h"
#include "ISelectionManager.h"
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
#include "Utils.h"
#include "IGeometry.h"
#include "TransformUtils.h"
#include "IGraphicFrameData.h"
#include "ILayoutUIUtils.h"
#include "IDataBase.h"
#include "IDocument.h"
#include "SDKUtilities.h"
#include "ICommand.h"
//#include "IImportFileCmdData.h"
#include "CmdUtils.h"
#include "IPlaceGun.h"
#include "OpenPlaceID.h"
#include "IReplaceCmdData.h"
#include "IPlaceGun.h" //Modified By Sachin Sharma on 22/6/07




//---From XPanelEventHandler
//#include "XPanelEventHandler.h"
#include "IPanelControlData.h"
#include "IControlView.h"
//#include "IEvent.h"
//#include "ViewPortAccess.h"
#include "AcquireViewPort.h"
#include "IWindowPort.h"
#include "IWidgetParent.h"
#include "IWindow.h"
#include "ISuppressedUI.h"
#include "CAlert.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"


// ----- Includes -----

#include "AcquireViewPort.h"
#include "InterfacePtr.h"
//#include "LibraryPanelDefs.h"
//#include "XPanelEventHandler.h"
#include "UIDList.h"
//#include "ViewPortAccess.h"

#include <K2Vector.h> 

// ----- Utility files -----
#include "IEvent.h"
#include "IEventHandler.h"
#include "CEventHandler.h"
#include "EventUtilities.h"
//#include "LibraryAssetUtils.h"

// ----- ID.h files -----

#include "DragDropID.h"
#include "CAlert.h"
#include "DCNID.h"
#include "IURIUtils.h" //Cs4
#include "IImportResourceCmdData.h"

#include "AssetInfo.h"

#define CA(X) CAlert::InformationAlert(X)
#define CAI(X,Y) {PMString ASD(X);\
	ASD.Append(" : ");\
	ASD.AppendNumber(Y);\
	CAlert::InformationAlert(ASD);}

K2Vector<int32> ImageSelectionList;
//extern K2Vector<PMString> imageType ;

//extern K2Vector<PMString> imageVector1 ;
extern K2Vector<int32> libSelectionList;
extern K2Vector<int32> curSelection ;

extern AssetInfo assetInfoObj;


class XLibraryItemGridEH : public CEventHandler//XPanelEventHandler
{
public:
	XLibraryItemGridEH(IPMUnknown *boss);
	virtual ~XLibraryItemGridEH();
	
	virtual bool16 LButtonDn(IEvent* e); 
	virtual bool16 ButtonDblClk(IEvent* e);

	//XPanelEventHandler(IPMUnknown *boss);
	//virtual ~XPanelEventHandler();
	
	virtual bool16 MouseMove(IEvent* e); 
	virtual bool16 MouseDrag(IEvent* e) ; 
	virtual bool16 LButtonUp(IEvent* e) ; 
	virtual bool16 ControlCmd(IEvent* e) ; 
	virtual bool16 KeyDown(IEvent* e) ; 
	virtual bool16 KeyCmd(IEvent* e) ; 
	virtual bool16 KeyUp(IEvent* e) ; 
	virtual bool16 Update(IEvent* e) ; 
	virtual bool16 ButtonTrplClk(IEvent* e);
	virtual bool16 RButtonDn(IEvent* e) ; 
	virtual bool16 RButtonUp(IEvent* e) ;
	virtual bool16 PlatformEvent(IEvent* e); 

protected:
	IControlView*	GetWidgetView(IEvent* e);
	void			UpdateSelectionObservers();
	virtual void		ComputeDragExitRect(int32 itemClicked, SysRect& exitGRect);

	//From XPanelEventHandler
	virtual IEventHandler	*QueryWidgetEH(IEvent* e); 	// Caller of this method must release the returned IEventHandler
	virtual void			ClearCachedChildEH();
	virtual IEventHandler	*QueryCachedWidgetEH();	// Caller must release

private:
	int32	fPrevButtonClick;

	//From XPanelEventHandler
	UIDRef				fLastWidgetUIDRef;
	IEventHandler*			fLastLButtonDnEventHandler;
};


CREATE_PMINTERFACE(XLibraryItemGridEH, kXLibraryItemGridEHImpl)


//---------------------------------------------------------------
// XLibraryItemGridEH constructor / destructor
//---------------------------------------------------------------

XLibraryItemGridEH::XLibraryItemGridEH(IPMUnknown *boss) :
	CEventHandler(boss),
	fLastWidgetUIDRef(UIDRef::gNull),
	fLastLButtonDnEventHandler(nil),
	fPrevButtonClick( -1 )
{
}

XLibraryItemGridEH::~XLibraryItemGridEH()
{
}

//---------------------------------------------------------------
// XLibraryItemGridEH::LButtonDn
//---------------------------------------------------------------

bool16 XLibraryItemGridEH::LButtonDn(IEvent* e) 
{
	bool16 handled = kTrue;
	bool16 cmdKeyDown = e->CmdKeyDown();
	bool16 shiftKeyDown = e->ShiftKeyDown();
	InterfacePtr<IXLibraryViewController> viewController( this, IID_IXLIBRARYVIEWCONTROLLER );
	InterfacePtr<IPanelControlData> panelData( this, IID_IPANELCONTROLDATA );
	ImageSelectionList.clear();
	libSelectionList.clear();

	int32		i;
	bool16		isNewSelection = kFalse;
	IControlView*	widgetView = GetWidgetView(e);
	if ( widgetView ) 
	{
		
		int32	curWidgetIndex = panelData->GetIndex(widgetView);
		ImageSelectionList.push_back(curWidgetIndex);
		libSelectionList.push_back(curWidgetIndex);
		/*PMString ASD("");
		ASD.Append("curWidgetIndex : ");
		ASD.AppendNumber(curWidgetIndex);
		CA(ASD);*/
		//CAI("curWidgetIndex", curWidgetIndex);
		
		///////////Added By Dattatray on 1/08/2007 for DefinedBreakMultiLineTextWidget
		InterfacePtr<IBooleanControlData>	buttonData( widgetView, IID_IBOOLEANCONTROLDATA );
		if ( buttonData && !buttonData->IsSelected())
		{
			isNewSelection = kTrue;
			InterfacePtr<IXLibraryButtonData> btnData(widgetView, IID_IXLIBRARYBUTTONDATA);
			if(btnData== nil){
				//CA("No btnData");
				return 0;
			}
			PMString str=btnData->GetName();
			//CA(str);
			InterfacePtr<IWidgetParent> btnparent(widgetView, IID_IWIDGETPARENT);
			IPMUnknown* unknownbtn=btnparent->GetParent();
			if(unknownbtn == nil){
				//CA("no unknownbtn");
				return 0;
			}
			InterfacePtr<IWidgetParent> gridparent(unknownbtn, IID_IWIDGETPARENT);
			IPMUnknown* unknowngrid=gridparent->GetParent();
			if(unknowngrid == nil){
				//CA("no unknowngrid");
				return 0;
			}
			InterfacePtr<IControlView> ctr(unknowngrid,UseDefaultIID());
			if(ctr == nil){
				//CA("no ctr");
				return 0;
			}
			InterfacePtr<IWidgetParent> panelparent(ctr, IID_IWIDGETPARENT);
			IPMUnknown* unknownpanel=panelparent->GetParent();
			if(unknownpanel == nil){
				//CA("no unknownpanel");
				return 0;
			}
			InterfacePtr<IControlView> ctr1(unknownpanel,UseDefaultIID());
			if(ctr1 == nil){
				//CA("no ctr1");
				return 0;
			}
			InterfacePtr<IPanelControlData> iData(ctr1,IID_IPANELCONTROLDATA);
				if(iData == nil)
			{
				//CA("no iData");
				return 0;
			 }
			IControlView* iView =iData->FindWidget(kDCNMultilineTextWidgetID);
			if(iView == nil)
			{
				//CA("no iView");
				return 0;
			}
			InterfacePtr<ITextControlData> txtData(iView,IID_ITEXTCONTROLDATA);
			if(txtData == nil)
			{
				//CA("no txtData");
				return 0;
			}
			str.SetTranslatable(kFalse);
            str.ParseForEmbeddedCharacters();
			txtData->SetString(str);



			IControlView* iView1 =iData->FindWidget(kDCNFileSizeTextWidgetID);
			if(iView == nil)
			{
				//CA("no iView");
				return 0;
			}

			InterfacePtr<ITextControlData> txtData1(iView1,IID_ITEXTCONTROLDATA);
			if(txtData == nil)
			{
				//CA("no txtData");
				return 0;
			}
			int32 filesize = btnData->GetFileSize();
			PMString tmp("File Size : ");
			tmp.AppendNumber(filesize);
			tmp.Append(" K");
			tmp.SetTranslatable(kFalse);
            tmp.ParseForEmbeddedCharacters();
			txtData1->SetString(tmp);
			///////////////////////////////upto here...................
		}

		// If new selection and no cmd key is pressed, remove all old selection
		if ( isNewSelection && !cmdKeyDown && !shiftKeyDown  ) {
			for ( i=0; i<panelData->Length(); i++) {
				IControlView*	view = panelData->GetWidget(i);
				InterfacePtr<IBooleanControlData>	buttonData( view, IID_IBOOLEANCONTROLDATA );
				if ( buttonData->IsSelected() )
					buttonData->Deselect();
				
			}
		}
		else  if( shiftKeyDown )
		{
			
			// double-check the previous selection
			// fPrevButtonClick would not be updated if a range of selection was deleted.
			InterfacePtr<IListBoxController> libListData( this, IID_ILISTBOXCONTROLLER );
			K2Vector<int32> libSelectionList;
			libListData->GetSelected( libSelectionList );
			if ( libSelectionList.size() == 0 )
				fPrevButtonClick = -1;
				
			if ( fPrevButtonClick > -1 ) {
				
				int32	firstIndex = fPrevButtonClick;
				int32	secondIndex = curWidgetIndex;
				if ( firstIndex > secondIndex )
				{
					firstIndex = curWidgetIndex;
					secondIndex = fPrevButtonClick;
				}
				if ( secondIndex > panelData->Length()-1 )
					secondIndex = panelData->Length() - 1;
				
				// Select everything in between the first and second index
				for ( i=firstIndex; i < secondIndex; i++ ) {
						
					IControlView*	view = panelData->GetWidget(i);
					InterfacePtr<IBooleanControlData>	buttonData( view, IID_IBOOLEANCONTROLDATA );
					buttonData->Select(kTrue, kFalse);
					InterfacePtr<ISubject> buttonSubject(buttonData, IID_ISUBJECT);
					if( buttonSubject )
						buttonSubject->Change( kTrueStateMessage, IID_IBOOLEANCONTROLDATA, (IEvent*)e);
				}
			}
		}

		// Select/Deselect 'the' button
		if ( !isNewSelection && cmdKeyDown )
			buttonData->Deselect();
		
		else
		{
			buttonData->Select(kTrue, kFalse);
			InterfacePtr<ISubject> buttonSubject(buttonData, IID_ISUBJECT);
			if( buttonSubject )
				buttonSubject->Change( kTrueStateMessage, IID_IBOOLEANCONTROLDATA, (IEvent*)e);
		}	
		fPrevButtonClick = curWidgetIndex;
		
		// Check if there are still items selected
		InterfacePtr<IListBoxController> libListData( this, IID_ILISTBOXCONTROLLER );
		K2Vector<int32> libSelectionList;
		libListData->GetSelected( libSelectionList );

		//if ( libSelectionList.Length() > 0 )
		//{

			//ImageSelectionList = libSelectionList;  /// ImageSelectionList is global variable to store the current selected Indexes	
			// Check if we can drag
			InterfacePtr<IDragDropSource> dragSource(this, IID_IDRAGDROPSOURCE);
			if (dragSource && dragSource->WillDrag(e)) {
				
				// Check to see if user is trying to start a drag
				bool16 isPatientUser;
				SysRect dragExitGRect;
				ComputeDragExitRect(curWidgetIndex, dragExitGRect);
				if (::IsUserStartingDrag(e/*->GetSysEvent()*/, isPatientUser, dragExitGRect))
				{
					InterfacePtr<IDragDropController> ddController(GetExecutionContextSession()/*gSession*/, IID_IDRAGDROPCONTROLLER);//Cs4
					if (ddController)
					{
						ddController->StartDrag(dragSource, e);	
						e->SetSystemHandledState(IEvent::kDontCall);
						
						// Notify any selection observers that the selection changed
						UpdateSelectionObservers();
						
						return kTrue; // Make up for the fact that StartDrag will swallow the mouse up
					}
				}
			}
		//}
	}
	else
	{
		// Clicked on empty part of the grid panel, deselect all.
		fPrevButtonClick = -1;
		for (int32 i=0; i<panelData->Length(); i++)
		{
			IControlView*	view = panelData->GetWidget(i);
			InterfacePtr<IBooleanControlData>	buttonData( view, IID_IBOOLEANCONTROLDATA );
			if ( buttonData->IsSelected() )
				buttonData->Deselect();
		}
	}
	
	// Notify any selection observers that the selection changed
	UpdateSelectionObservers();

	return handled;
}

//---------------------------------------------------------------
// XLibraryItemGridEH::ButtonDblClk
//---------------------------------------------------------------

bool16 XLibraryItemGridEH::ButtonDblClk(IEvent* e) 
{	
	//CA("XLibraryItemGridEH::ButtonDblClk");
	bool16 result=kFalse;
	bool16 ImGFlg;
	PMRect theArea;
	PMReal rel;
	int32 ht;
	int32 wt;

	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
	if(!iSelectionManager)
	{	//CA("Slection nil");
		//LogPtr->LogError("NO Iteam is selected IN Selection obsever iSelectionManager is NIL");
		return result;
	}
	
	InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
	( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
	if(!txtMisSuite)
	{	//LogPtr->LogError("NO Iteam is selected IN Selection obsever iSelectionManager is NIL");
		//CA("My Suit nil");
		return result; 
	}
	
	UIDList uidLst;
	txtMisSuite->GetUidList(uidLst);
	UIDRef imageBox=uidLst.GetRef(0);

	InterfacePtr<IGeometry> iGeometry(imageBox, UseDefaultIID());
	if(iGeometry==nil){
		//LogPtr->LogError("Geometry of Image box is not found IN Selection obsever iGeometry is NIL");
		return kFalse;
	}

	theArea=iGeometry->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeometry)));
	rel = theArea.Height();
	ht=::ToInt32(rel);
	rel=theArea.Width();	
	wt=::ToInt32(rel);

	//CA("1");

	if(uidLst.Length()!=0)
	{	
		do{
			InterfacePtr<IGraphicFrameData> frameData(imageBox,IID_IGRAPHICFRAMEDATA);
			if (frameData)
			{
				if (frameData->HasContent())
				{	//CA("yes it has content already");
					//LogPtr->LogDebug("Image box already has Picture in image box IN Selection obsever ");
					//PREMediatorClass::
					txtMisSuite->setFrameUser(2);
					ImGFlg=kFalse;
					break;
					/*if (frameData->IsGraphicFrame())
					{
					}*/
				}
			}
			PMString imPath="";
			//CA("imPath : " + imPath);
			imPath.Append(assetInfoObj.imageVector1[libSelectionList[0]]);
			//CA("imPath : " + imPath);

			IDocument* doc =Utils<ILayoutUIUtils>()->GetFrontDocument(); 
			IDataBase* db = ::GetDataBase(doc); 
			if (db == nil){
				//LogPtr->LogError("Databse is not found IN Selection obsever db is NIL");			
				return kFalse;
			}

			/*SysFile*/IDFile sysFile = SDKUtilities::PMStringToSysFile(const_cast<PMString* >(&imPath));	
			InterfacePtr<ICommand> importCmd(CmdUtils::CreateCommand(kImportAndLoadPlaceGunCmdBoss));
			if(!importCmd){ 
				//LogPtr->LogError("IN Selection obsever cant import the image");			
				return kFalse;
			}
				
			//*** Commented By Sachin Sharma Depricated cs3
			//InterfacePtr<IImportFileCmdData> importFileCmdData(importCmd, IID_IIMPORTFILECMDDATA); // no DefaultIID for this
			//if(!importFileCmdData){
			//	CA("!importFileCmdData XLibraryItemGridEH::ButtonDblClick");
			//	return kFalse;
			//}
			//
			//importFileCmdData->Set(db, sysFile, kMinimalUI);
			//******* Cs4

			URI tmpURI;
			Utils<IURIUtils>()->IDFileToURI(sysFile, tmpURI);
			InterfacePtr<IImportResourceCmdData> importFileCmdData(importCmd, IID_IIMPORTRESOURCECMDDATA); // no kDefaultIID	
			if (importFileCmdData == nil) {
				CA(" importResourceCmdData == nil ");
				return kFalse;
			}	
			importFileCmdData->Set(db,tmpURI,kMinimalUI);
			//********

			ErrorCode err = CmdUtils::ProcessCommand(importCmd);
			if(err != kSuccess) 
				return kFalse;

			InterfacePtr<IPlaceGun> placeGun(db, db->GetRootUID(), UseDefaultIID());
			if(!placeGun)
				return kFalse;
			
			UIDRef placedItem(db, placeGun->GetFirstPlaceGunItemUID());        //GetItemUID());:GetFirstPlaceGunItemUID

			InterfacePtr<ICommand> replaceCmd(CmdUtils::CreateCommand(kReplaceCmdBoss));
			if (replaceCmd == nil)
				return kFalse;

			InterfacePtr<IReplaceCmdData>iRepData(replaceCmd, IID_IREPLACECMDDATA);
			if(!iRepData)
				return kFalse;
		//CA("2");	
			iRepData->Set(db, imageBox.GetUID(), placedItem.GetUID(), kFalse);

			ErrorCode status = CmdUtils::ProcessCommand(replaceCmd);
			if(status==kFailure)
				return kFalse;
			
			txtMisSuite->setFrameUser(3);

		}while(kFalse);
	}

	/*IControlView*	widgetView = GetWidgetView(e);
	if(widgetView == nil)
		return kFalse;*/ // no button identified as being clicked on

	// If we have an interface that contains the data of the widget. We can query it here
	// and pass it to the dialog below

    // Get the Dialog Manager
//    InterfacePtr<IApplication> app(gSession->QueryApplication());
//    InterfacePtr<IDialogMgr> dialogMgr(app, IID_IDIALOGMGR);
//
//    // Make the resource specification for the dialog
//    RsrcSpec rSpec(LocaleSetting::GetLocale(),
//                    kDCNPluginID,
//					kViewRsrcType,
//                    kXLibraryInfoDialogRsrcID);
//
//    // Make the dialog
//    // Apparently, IDialogs are self deleting...
//    IDialog* theInfoDialog = dialogMgr->CreateNewDialog(rSpec, IDialog::kMovableModal);
//
//    IControlView*   theControlView = theInfoDialog->GetDialogPanel();
//    InterfacePtr<IDialogController> theController(theControlView, IID_IDIALOGCONTROLLER);
//
//    // let the dialog know about the asset to display info on...
////    InterfacePtr<IPMUnknownData> controllerAsUnknownHolder(theController, IID_IPMUNKNOWNDATA);
////    controllerAsUnknownHolder->SetPMUnknown(assetAsUnknown);
//
//    if(theInfoDialog)
//        theInfoDialog->Open();
//	int INDEX = ImageSelectionList[0];
//	CAlert::InformationAlert(imageType[INDEX]);
		
	return kTrue;	
}

//__________________________________________________________________
//	ComputeDragExitRect: Compute the bounds rect that triggers the start of a drag operation.
//		In this case we use the bounds of the grid item clicked on.
//__________________________________________________________________
void XLibraryItemGridEH::ComputeDragExitRect(int32 itemClicked, SysRect& exitGRect)
{
	// Get the list of currently selected library items
	//ViewPortAccess<IWindowPort> windowPort(this, IID_IWINDOWPORT);
	InterfacePtr<IControlView>		myView(this, UseDefaultIID());
		if(myView == nil) 
			return ;

	InterfacePtr<const IListBoxController> libListData( this, IID_ILISTBOXCONTROLLER );
	InterfacePtr<const IControlView> gridView(this, IID_ICONTROLVIEW);
	InterfacePtr<const IPanelControlData> gridData( gridView, IID_IPANELCONTROLDATA );
		
	//AcquireViewPort							aqViewPort(windowPort);

	// Get the bounding box of the grid button
	IControlView* button = gridData->GetWidget( itemClicked );
	SysRect buttonRect( button->GetBBox() );

	//exitGRect = windowPort->LocalToGlobal(buttonRect);
	PMRect buttonPMRect(buttonRect);
	//PMRect exitPMGRect = myView->ViewToGlobal(buttonPMRect);
	exitGRect = ::ToSys(myView->ViewToGlobal(buttonRect)); //WindowToGlobal
}

//---------------------------------------------------------------
// XLibraryItemGridEH::GetWidgetView
//---------------------------------------------------------------

IControlView*	XLibraryItemGridEH::GetWidgetView(IEvent* e)
{
	//ViewPortAccess<IWindowPort> windowPort(this, IID_IWINDOWPORT);
	//AcquireViewPort aq(windowPort);
	InterfacePtr<IControlView>		myView(this, UseDefaultIID());
		if(myView == nil) 
			return nil;
	
	InterfacePtr<IPanelControlData>	myData(this, IID_IPANELCONTROLDATA);

	SysPoint localWhere;
	IControlView	*widgetView;
	
	for(int32 i = myData->Length()-1; i  >= 0 ; --i)
	{
		widgetView = myData->GetWidget(i);
		SysPoint pt = e->GlobalWhere();
		localWhere = ::ToSys(myView->GlobalToWindow(pt)); //windowPort->GlobalToLocal(pt);
		if( widgetView->IsVisible() && widgetView->HitTest(localWhere) )
		{
			return widgetView;
		}
	}
	return nil;
}

//---------------------------------------------------------------
// XLibraryItemGridEH::UpdateSelectionObservers
//---------------------------------------------------------------

void	XLibraryItemGridEH::UpdateSelectionObservers()
{
}

//---------------------------------------------------------------
// XLibraryItemGridEH::MouseMove
//---------------------------------------------------------------

bool16 XLibraryItemGridEH::MouseMove(IEvent* e) 
{
	bool16 handled = kFalse;
	IEventHandler *eh = QueryWidgetEH(e);
	
	if(eh) 
	{
		handled = eh->MouseMove(e);
		eh->Release();
	}

	return handled;
}

//---------------------------------------------------------------
// XLibraryItemGridEH::MouseDrag
//---------------------------------------------------------------

bool16 XLibraryItemGridEH::MouseDrag(IEvent* e) 
{
	bool16 handled = kFalse;
	IEventHandler *eh = QueryWidgetEH(e);
	
	if(eh) 
	{
		handled = eh->MouseDrag(e);
		eh->Release();
	}

	return handled;
}


//---------------------------------------------------------------
// XLibraryItemGridEH::RButtonDn
//---------------------------------------------------------------

bool16 XLibraryItemGridEH::RButtonDn(IEvent* e) 
{
	bool16 handled = kFalse;

	// Save away UID ref of event handler/widget so we can check its existence in double click.
	InterfacePtr<IEventHandler> eh(QueryWidgetEH(e));
	fLastWidgetUIDRef = ::GetUIDRef(eh);
	fLastLButtonDnEventHandler = eh;	

	if(eh) 
	{
		handled = eh->RButtonDn(e);
	}
	
	return handled;
}

//---------------------------------------------------------------
// XLibraryItemGridEH::LButtonUp
//---------------------------------------------------------------

bool16 XLibraryItemGridEH::LButtonUp(IEvent* e) 
{
	bool16 handled = kFalse;
	InterfacePtr<IEventHandler> eh(QueryWidgetEH(e));
	
	if(eh)
	{
		handled = eh->LButtonUp(e);
	}
	
	return handled;
}

//---------------------------------------------------------------
// XLibraryItemGridEH::RButtonUp
//---------------------------------------------------------------

bool16 XLibraryItemGridEH::RButtonUp(IEvent* e) 
{
	bool16 handled = kFalse;
	InterfacePtr<IEventHandler> eh(QueryWidgetEH(e));
	
	if(eh)
		handled = eh->RButtonUp(e);
	
	return handled;
}

//---------------------------------------------------------------
// XLibraryItemGridEH::ControlCmd
//---------------------------------------------------------------

bool16 XLibraryItemGridEH::ControlCmd(IEvent* e) 
{
	bool16 handled = kFalse;
	InterfacePtr<IEventHandler> eh(QueryWidgetEH(e));
	
	if(eh) 
		handled = eh->ControlCmd(e);
	
	return handled;
}

//---------------------------------------------------------------
// XLibraryItemGridEH::KeyDown
//---------------------------------------------------------------

bool16 XLibraryItemGridEH::KeyDown(IEvent* e) 
{
	bool16 handled = kFalse;
	InterfacePtr<IEventHandler> eh(QueryWidgetEH(e));
	
	if(eh) 
		handled = eh->KeyDown(e);
	
	return handled;
}

//---------------------------------------------------------------
// XLibraryItemGridEH::KeyCmd
//---------------------------------------------------------------

bool16 XLibraryItemGridEH::KeyCmd(IEvent* e) 
{
	bool16 handled = kFalse;
	InterfacePtr<IEventHandler> eh(QueryWidgetEH(e));
	
	if(eh) 
		handled = eh->KeyCmd(e);
	
	return handled;
}

//---------------------------------------------------------------
// XLibraryItemGridEH::KeyUp
//---------------------------------------------------------------

bool16 XLibraryItemGridEH::KeyUp(IEvent* e) 
{
	bool16 handled = kFalse;
	InterfacePtr<IEventHandler> eh(QueryWidgetEH(e));
	
	if(eh) 
		handled = eh->KeyUp(e);
	
	return handled;
}

//---------------------------------------------------------------
// XLibraryItemGridEH::Update
//---------------------------------------------------------------

bool16 XLibraryItemGridEH::Update(IEvent* e) 
{
	bool16 handled = kFalse;
	InterfacePtr<IEventHandler> eh(QueryWidgetEH(e));
	
	//if(eh) 
	//	handled = eh->Update(e);
	
	return handled;
}

bool16 XLibraryItemGridEH::PlatformEvent(IEvent* e)
{
	bool16 handled = kFalse;
	InterfacePtr<IEventHandler> eh(QueryWidgetEH(e));

	if(eh) 
	{
		//handled = eh->PlatformEvent(e);
	}
	return handled;
}

//---------------------------------------------------------------
// XLibraryItemGridEH::ButtonTrplClk
//---------------------------------------------------------------

bool16 XLibraryItemGridEH::ButtonTrplClk(IEvent* e)
{
	InterfacePtr<IEventHandler> lastEH(QueryCachedWidgetEH());
	if (lastEH)
		return (lastEH->ButtonTrplClk(e));

	return kFalse;
}
 
//---------------------------------------------------------------
// XLibraryItemGridEH::QueryWidgetEH
//---------------------------------------------------------------

//NOTE: We assume drawing will walk the paneldata in increasing order. Hence, it is more efficient to walk
// the hittest in decreasing order, so that we retain the proper z-order.

IEventHandler	*XLibraryItemGridEH::QueryWidgetEH(IEvent* e)
{
	InterfacePtr<ISuppressedUI>	suppressedUI(GetExecutionContextSession()/*gSession*/, UseDefaultIID());//CS4
	InterfacePtr<IControlView> panelView(this,UseDefaultIID());
	if(!panelView->GetEnableState() || (suppressedUI && suppressedUI->IsWidgetDisabled(panelView) == kTrue))
		return nil;

	//ViewPortAccess<IWindowPort> windowPort(this, IID_IWINDOWPORT);
	//AcquireViewPort aqViewPort(windowPort);

	InterfacePtr<IPanelControlData>	myData(this, IID_IPANELCONTROLDATA);
	SysPoint localWhere;
	IControlView	*widgetView;
	//IEventHandler	*widgetEH;
	
	for(int32 i = myData->Length()-1; i  >= 0 ; --i)
	{
		SysPoint pt = e->GlobalWhere();
		localWhere = ::ToSys(panelView->GlobalToWindow(pt));  //windowPort->GlobalToLocal(pt);
		widgetView = myData->GetWidget(i);
		if(widgetView->GetVisibleState() == kTrue && (!suppressedUI || suppressedUI->IsWidgetHidden(widgetView) == kFalse)) 
		{
			if(widgetView->GetEnableState() && widgetView->HitTest(localWhere) && (!suppressedUI || suppressedUI->IsWidgetDisabled(widgetView) == kFalse) ) 
			{
				PMString ASD("Called here");
				//SysWindow controlWindow = widgetView->GetSysWindow();
				//bool16 testWidget = controlWindow == nil || controlWindow == e->GetSysWindow();
				//if (testWidget) 
				//{
					//widgetEH = (IEventHandler *)widgetView->QueryInterface(IID_IEVENTHANDLER); // Released by caller
					//return widgetEH;
				//}
			}
		}
	}
	return nil;
}

//---------------------------------------------------------------
// XLibraryItemGridEH::ClearCachedChildEH
//---------------------------------------------------------------

void XLibraryItemGridEH::ClearCachedChildEH() 
{
	fLastWidgetUIDRef = UIDRef::gNull;
	fLastLButtonDnEventHandler = nil;
}

IEventHandler* XLibraryItemGridEH::QueryCachedWidgetEH()
{
	// Verify that we can still instantiate the control view from the cached reference.
	// It may have been deleted in some circumstances.
	IDataBase       *db = fLastWidgetUIDRef.GetDataBase();
	if (db && db->IsValidUID(fLastWidgetUIDRef.GetUID()))
	{
		InterfacePtr<IEventHandler> lastEH(fLastWidgetUIDRef, UseDefaultIID());
		if (lastEH)
		{
			InterfacePtr<IControlView> lastView(lastEH, UseDefaultIID());
			InterfacePtr<IPanelControlData> panelData(this, UseDefaultIID());
			if (panelData && lastView)
			{
				int32 index = panelData->GetIndex(lastView);
				if (index != -1)
				{
					lastEH->AddRef();
					return lastEH;
				}
			}
		}
	}

	// if we get here, it may be that the event handler we had cached was not persistent. 
	//	in that case, hand back the cached IEventHandler*, and hope that it hasn't gone
	//	out of scope or been deleted.
	if ((fLastWidgetUIDRef == UIDRef::gNull) && (fLastLButtonDnEventHandler != nil))
	{
		fLastLButtonDnEventHandler->AddRef();
		return fLastLButtonDnEventHandler;
	}

	return nil;
}

//---------------------------------------------------------------
// static ::GetWindow()
//---------------------------------------------------------------

IWindow *GetWindow(XLibraryItemGridEH *panel)
{
	IWindow *w = (IWindow*)panel->QueryInterface(IID_IWINDOW);
	if (!w)
	{
		InterfacePtr<IWidgetParent> parent(panel, IID_IWIDGETPARENT);
		w = (IWindow*)parent->QueryParentFor(IID_IWINDOW);
	}
	return w;
}
