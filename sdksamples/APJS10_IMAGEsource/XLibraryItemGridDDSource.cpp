
#include "VCPlugInHeaders.h"

#include "IUIDListData.h"
#include "IWidgetParent.h"
#include "IControlView.h"
#include "IListBoxController.h"
#include "IXLibraryButtonData.h"
#include "IPageItemScrapData.h"
#include "IPanelControlData.h"
#include "IDataExchangeHandler.h"
#include "IDragDropController.h"
#include "IPMDataObject.h"
#include "IPMPointData.h"
#include "IWindowPort.h"
#include "IShape.h"
#include "IGeometry.h"
#include "ITransform.h"
#include "ISession.h"
#include "SystemUtils.h"//Added by Sachin Sharma
#ifdef WINDOWS
	#include "WSysType.h"
#endif

#include "CDragDropSource.h"
#include "DragDropID.h"
#include "WidgetID.h"
#include "CmdUtils.h"
//#include "ViewPortAccess.h"
#include "AcquireViewPort.h"
#include "UIDList.h"
#include "PMRect.h"
#include "PMMatrix.h"
#include "TransformUtils.h"
#include "CAlert.h"
#include "SplineID.h"
#include "PMFlavorTypes.h"
#include "PMRect.h"
#include "ILayoutUIUtils.h"
#include "CreateObject.h"
#include "HelperInterface.h"
#include "K2Vector.h"
#include "K2Vector.tpp"
#include "IDataExchangeHandler.h"
#include "DCNID.h"

#include "IPageItemScrapData.h"
#include "IPageItemLayerData.h"
#include "IPathUtils.h"
#include "IPMDataObject.h"
#include "IDragDropController.h"
#include "IGraphicAttributeUtils.h"
#include "IGraphicAttributeSuite.h"
#include "ISwatchUtils.h"
#include "ISwatchList.h"
#include "ILayoutUIUtils.h"
#include "IGeometry.h"
#include "SDKListBoxHelper.h"
#include "IApplication.h"
//#include "WSystemUtils.h"
//#include "IPaletteMgr.h"     //Commented By Sachin sharma
#include "PaletteRefUtils.h"
#include "IPanelMgr.h"
#include "IListBoxController.h"
#include "IXLibraryViewController.h"

//#include "WINUSER.h"   //Added By Sachin Sharma




#define CA(X) CAlert::InformationAlert(X)
K2Vector<int32> libSelectionList;
extern K2Vector<int32> ImageSelectionList;
extern bool16 isThumb;
extern bool16 BrowseFolderOption;
extern K2Vector<int32> ImageSelectionList;

class XLibraryItemGridDDSource : public CDragDropSource
{
public:
	XLibraryItemGridDDSource(IPMUnknown *boss);
	virtual ~XLibraryItemGridDDSource();
	
	virtual bool16		WillDrag(IEvent* e) const;
		// Could a drag be initiated from the mouse position in the event?

	virtual bool16		DoAddDragContent(IDragDropController*);
		// Add drag content for the current drag operation
	
	//virtual /*SysRgn*/SysWireframe 		DoMakeDragOutlineRegion() const; // //APSCC2017_Comment
		// Create a SysRgn to use for drag feedback (usually only used on Mac).

	ErrorCode  CreatePicturebox
	(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight);

	DECLARE_HELPER_METHODS()
	private:
		/** 
			this method just creates a spline
			@param newPageItem OUT the UIDRef for the created page item.
			@param parent IN the UIDRef for the parent that the new item should be attached to.
			@param points IN the pair of points that define the spline.
			@param strokeWeight IN the initial stroke applied to the path for the new page item.
			@return kSuccess if the page item is created without error, kFailure otherwise.
		*/
		ErrorCode CreatePageItem(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight);
		
		/** Cache the page item */
		UIDRef fPageItem;
		

	
};


//========================================================================
// Class XLibraryItemGridDDSource
//========================================================================
CREATE_PMINTERFACE(XLibraryItemGridDDSource, kXLibraryItemGridDDSourceImpl)
DEFINE_HELPER_METHODS(XLibraryItemGridDDSource)

XLibraryItemGridDDSource::XLibraryItemGridDDSource(IPMUnknown *boss) :
	CDragDropSource(boss),
	HELPER_METHODS_INIT(boss)
{
}

XLibraryItemGridDDSource::~XLibraryItemGridDDSource()
{
}


//--------------------------------------------------------------------------------------
// WillDrag
//--------------------------------------------------------------------------------------
bool16 XLibraryItemGridDDSource::WillDrag(IEvent* e) const
{
	//CA("Inside XLibraryItemGridDDSource::WillDrag");
	// Get the list of currently selected  items
	IDocument* theFrontDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();
	if (theFrontDoc == nil)
	{
		//CA("theFrontDoc is NULL");
		return kFalse;
	}
	int32	curWidgetIndex = -1;
	//libSelectionList.clear();
	if(isThumb==kTrue )
	{
		//CA("Inside");
		/*InterfacePtr<const IListBoxController> listController( this, IID_ILISTBOXCONTROLLER );
		listController->GetSelected( libSelectionList );*/
		
	}
	else
	{
		InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if(app == NULL){  
			//CA("app == NULL");
 			return kFalse;
		}
		//Commented  By Sachin Sharma on 23/06/07
		//InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPanelManager()); //QueryPaletteManager()); Modified By Sachin Sharma on 23/06/07
		//if(paletteMgr == NULL){ //CA("paletteMgr == NULL");
		//	return kFalse;
		//}
		//InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());

		InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
		if(panelMgr == NULL){ 
			//CA("panelMgr == NULL");
			return kFalse;
		}
		//IControlView* myPanel = panelMgr->GetVisiblePanel(kDCNPanelWidgetID);
		IControlView* myPanel = panelMgr->GetPanelFromActionID(kDCNPanelWidgetActionID);
		if(myPanel == NULL) { //CA("myPanel == NULL99");
			return kFalse;
		}
		InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
		if(panelControlData == NULL){ //CA("panelControlData == NULL");
			return kFalse;
		}
		IControlView* lstgridWidget = panelControlData->FindWidget( kDCNlstPnlWidgetID);
		if(lstgridWidget == NULL){ //CA("lstgridWidget == NULL");
			return kFalse;
		}
		//SDKListBoxHelper listHelper(lstgridWidget,kDCNPluginID,kDCNlstviewListboxWidgetID, kDCNlstviewCustomPanelViewWidgetID);

		//IControlView* ListView = listHelper.FindCurrentListBox();
		//if(ListView == NULL)
		//{
		//	//CA("ListView == NULL");
		//	return kFalse;
		//}else{
		//	//InterfacePtr<IListBoxController> listCntl(ListView,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
		//	//ASSERT_MSG(listCntl != nil, "listCntl nil");
		//	//if(listCntl == nil) {  //CA("listCntl is nil");
		//	//	return kFalse;
		//	//}
		//	////  Get the first item in the selection (should be only one if the list box
		//	//// has been set up correctly in the framework resource file)			
		//	//listCntl->GetSelected( libSelectionList ) ;
		//}
	}
		
	// Check if there really are items selected
	//if ( libSelectionList.Length() > 0 )
	if(ImageSelectionList.size() > 0)
	{ //CA("Will deag true");
		return kTrue;
	}
	else{ //CA("Will deag false");
		return kFalse;
	}
}

//APSCC2017_Comment
//--------------------------------------------------------------------------------------
// DoMakeDragOutlineRegion
//--------------------------------------------------------------------------------------
/*
SysWireframe XLibraryItemGridDDSource::DoMakeDragOutlineRegion() const
//{

	do 
	{	
		// get the geometry of the page item
		InterfacePtr<IGeometry> iGeometry(fPageItem,UseDefaultIID());
		if (iGeometry == nil)
		{
			//CA("No geometry on scrap item!");
			break;
		}
		
		// we will draw a rectangle around the bounding box
		PMRect thePIGeo = iGeometry->GetStrokeBoundingBox();
	
		// we need to draw the bounding box at the current mouse location
		SysPoint currentMouse = GetMousePosition();
		PMPoint start(currentMouse);
		
		PMPoint lTop(thePIGeo.LeftTop());
		PMPoint rBottom(thePIGeo.RightBottom());

		lTop+=start;
		rBottom+=start;
		// create a new pmrect based on the page item, offset from the mouse location
		// On windows this feedback resorts to the default, on the mac it would be 
		PMRect offsetRect(lTop,rBottom);

			
			//Commented By Sachin Sharma
////		SysRect windowRect = ::ToSys(offsetRect);
////		SysRgn origRgn = ::CreateRectSysRgn(windowRect);
////
////		// Get the window region for the rectangle.
//////+=========================
////		::InsetSysRect(windowRect,7,7);    
////		
//////++++++=================================
////		//::InsetSysRect(windowRect,50,50);
////		
////		SysRgn windowRgn = ::CreateRectSysRgn(windowRect);
////	
////		DiffSysRgn(origRgn,windowRgn,origRgn);
////	
////		return origRgn;

//++++++++++++======================================================= Added By SAchin Sharma

		SysRect windowRect = ::ToSys(offsetRect);
		SysWireframe origRgn = ::CreateRectSysWireframe(windowRect);
		// Get the window region for the rectangle.
	    ::InsetSysRect(windowRect,7,7);
		SysWireframe windowRgn = ::CreateRectSysWireframe(windowRect);
		DiffSysWireframe(origRgn,windowRgn,origRgn);
		::DeleteSysWireframe(windowRgn);
		return origRgn;

//+===================================================================
	}
	while (kFalse);
	//CA("end of DoMakeOutLine Region");
	return nil;
}
*/

//--------------------------------------------------------------------------------------
// DoAddDragContent
//--------------------------------------------------------------------------------------
bool16 XLibraryItemGridDDSource::DoAddDragContent(IDragDropController* DNDController)
{
	//CA("Inside DoAddDragContent");
	//int32 i;
	//
	//// Get the list of currently selected library items
	//InterfacePtr<IListBoxController> libListData( this, IID_ILISTBOXCONTROLLER );
	//K2Vector<int32> libSelectionList;
	//libListData->GetSelected( libSelectionList );
	//
	//// Check if there really are items selected
	//if ( libSelectionList.Length() == 0 ) return kFalse;
	//
	//// Convert the selection list to a list of asset IDs
	//InterfacePtr<IControlView> gridView(this, IID_ICONTROLVIEW);
	//InterfacePtr<IPanelControlData> gridData( gridView, IID_IPANELCONTROLDATA );

	//for ( i=0; i<libSelectionList.Length(); i++ ) {
	//	
	//	//CA("Inside for Loop of libSelectionList");
	//	// Get the asset ID for this widget
	//	IControlView* button = gridData->GetWidget( libSelectionList[i] );
	//	InterfacePtr<IXLibraryButtonData> buttonData( button, IID_IXLIBRARYBUTTONDATA );
	//	
	//	// get data to be dragged for each selected widget...
	//}
	//
	//// add data to drag...
	//
	//return kFalse;

	// code for Dragging Rect Image Box (Rect Spline)

	bool16 result = kFalse;
	// do while(false) loop for error control
	do 
	{
		// Get the dataexchangehandler for the flavor we want to add 
		// The method QueryHandler is on the dataexchangecontroller, which adds extra functionality
		// to the dragdropcontroller. I am assuming we drag from our custom source to custom target
		// via the layout widget, if we attempt to go directly, the handler will be nil.
		InterfacePtr<IDataExchangeHandler> ourHandler(DNDController->QueryHandler(customFlavorAssetBr));
		// if we cannot get the handler for our flavor, we are dead in the water.
		// The handler gives us the scrap to store our content on...
		if (ourHandler==nil)
		{
			//CA("Handler nil for our flavor?");
			break;
		}	
		// we need to place our content onto the scrap
		InterfacePtr<IPageItemScrapData> scrapData(ourHandler, UseDefaultIID());
		if (scrapData == nil)
		{
			//CA("No scrap data for DEHandler?");
			break;
		}
		
		// get the root of the scrap	
		UIDRef 	scrapRoot =	scrapData->GetRootNode();
		// and its associated database
		IDataBase* scrapDB = scrapRoot.GetDataBase();

		// new page item
		//UIDList DragedBoxesList(scrapDB);
		UIDList DragedBoxesList;
		
		for(int p=0; p <libSelectionList.size() ; p++)
		{   
			
			UIDRef newPageItem = UIDRef::gNull;
			// set up a list of points for the page item
			PMPointList points(2, PMPoint());
			// define the text bounding box using two pmpoints
			PMPoint startPoint(0,0);
			PMPoint endPoint(100,100);
			points.push_back( startPoint );
			points.push_back( endPoint );
			PMReal strokeWeight(1.0);

			if(CreatePicturebox(newPageItem,scrapRoot,points,strokeWeight)==kFailure)
			{
				//CA("Failed to create page item?");
				break;
			}
			
			if(DragedBoxesList == NULL || DragedBoxesList.Length() ==0)
			{
				UIDList dragTempUIDList(newPageItem.GetDataBase(), newPageItem.GetUID() );
				DragedBoxesList = dragTempUIDList;
			}
			else
			{
				DragedBoxesList.Append(newPageItem.GetUID());
			}
			
			//DragedBoxesList.Append(newPageItem.GetUID());
			fPageItem = newPageItem;
			//scrapData->Replace(UIDList(newPageItem));
		}
		//Mediator::ImageBoxUIDRef = newPageItem;
		// Must manually clear the handler before adding any data...
		ourHandler->Clear();
		
		// we call replace to define the DB the item exists in. We add our new item to the scrap
		
		scrapData->Replace(DragedBoxesList);
		// Update the cached variable
		
		InterfacePtr<IPageItemLayerData> layerData(scrapData,IID_IPAGEITEMLAYERDATA);
		// I faux the layer the item is on.
		K2Vector<int32> layerIndexList(1, int32());
		layerIndexList.push_back(0);

		// Make a list of the layer names.
		K2Vector<PMString> layerNameList;
		layerNameList.reserve(1);
		PMString layerName("Untitled");
		layerNameList.push_back(layerName);
 
		//layerData->SetPageItemList(UIDList(newPageItem));
		layerData->SetPageItemList(DragedBoxesList);
		layerData->SetLayerNameList(layerNameList);
		layerData->SetLayerIndexList(layerIndexList);
		layerData->SetIsGuideLayer(kFalse);
		
		// we point the controller at the pageitem handler 
		DNDController->SetSourceHandler(ourHandler) ;
			
		// we get the data object that represents the drag
		//InterfacePtr<IPMDataObject> item(DNDController->AddDragItem(1));
		InterfacePtr<IPMDataObject> item(DNDController->AddDragItem(/*libSelectionList*/ImageSelectionList.size()));

		// no flavor flags	
		PMFlavorFlags flavorFlags = 0;
		
		// we set the type (flavour) in the drag object 
		item->PromiseFlavor(customFlavorAssetBr, flavorFlags);

 		result = kTrue;
	} while (false);
	//CA(" end of BscDNDCustomDragSource::DoAddDragContent");
	return result; 

}


/* this method takes in a points list, stroke weight and parent UIDRef and creates a spline with those points, the stroke weight
	and attaches it to the parent.
*/
ErrorCode 
XLibraryItemGridDDSource::CreatePageItem(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight)
{
	ErrorCode returnValue = kFailure;
	//CA(__FUNCTION__);
	do 
	{
		// wrap the commands in a sequence
		ICommandSequence *sequence = CmdUtils::BeginCommandSequence();
		if (sequence == nil)
		{
			ASSERT_FAIL("Cannot create command sequence?");
			break;
		}

		// create the page item
		UIDRef newSplineItem = Utils<IPathUtils>()->CreateLineSpline(parent, points, INewPageItemCmdData::kDefaultGraphicAttributes);
		
		// put the new item into a splinelist
		const UIDList splineItemList(newSplineItem);

		// we apply the stroke to our item
		InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->CreateStrokeWeightCommand(strokeWeight,&splineItemList,kTrue,kTrue));
		if(strokeSplineCmd==nil)
		{
			ASSERT_FAIL("Cannot create the command to stroke the spline?");
			break;
		}
				
		if(CmdUtils::ProcessCommand(strokeSplineCmd)==kFailure)
		{
			ASSERT_FAIL("Failed to stroke the spline?");
			break;
		}

		//// we want to apply the stroke, color it black. We need to get the black swatch...
		//InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QuerySwatchList(splineItemList.GetDataBase()));
		//if(iSwatchList==nil)
		//{
		//	ASSERT_FAIL("Cannot get swatch list?");
		//	break;
		//}
		//UID blackUID = iSwatchList->GetBlackSwatchUID();
		//
		//// Now get the command to apply the stroke...
		//InterfacePtr<ICommand> applyStrokeCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeRenderingCommand(blackUID,&splineItemList,kTrue,kTrue));
		//if(applyStrokeCmd==nil)
		//{
		//	ASSERT_FAIL("Cannot create the command to render the stroke?");
		//	break;
		//}

		//if(CmdUtils::ProcessCommand(applyStrokeCmd)!=kSuccess)
		//{
		//	ASSERT_FAIL("Failed to render the stroke?");
		//	break;
		//}
		
		// now we can end the command sequence, we are done.
		CmdUtils::EndCommandSequence(sequence);
		newPageItem = newSplineItem;
		returnValue=kSuccess;
	} while(false);
	//CA("end of Create Page Item");
	return returnValue;
}

// End, BscDNDCustomDragSource.cpp.
ErrorCode 
XLibraryItemGridDDSource::CreatePicturebox
(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight)
{
	//CA(__FUNCTION__);
	ErrorCode returnValue = kFailure;
	do 
	{
		ICommandSequence *sequence = CmdUtils::BeginCommandSequence();
		if (sequence == nil)
			break;

		PMPoint topLeft(5,5);
		PMPoint bottomRight(120,120);
		PMRect rect(topLeft, bottomRight);
		UIDRef newSplineItem = Utils<IPathUtils>()->CreateRectangleSpline
		(parent, rect, INewPageItemCmdData::kGraphicFrameAttributes);

		const UIDList splineItemList(newSplineItem);

		InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->
		CreateStrokeWeightCommand(strokeWeight,&splineItemList,kTrue,kTrue));
		if(strokeSplineCmd==nil)
			break;
				
		if(CmdUtils::ProcessCommand(strokeSplineCmd)==kFailure)
			break;

		//InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QueryActiveSwatchList());
		//if(iSwatchList==nil)
		//	break;
		//UID blackUID = iSwatchList->GetRegistrationSwatchUID();/*GetPaperSwatchUID(); GetNoneSwatchUID();*/
		//
		//InterfacePtr<ICommand> applyStrokeCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeRenderingCommand(blackUID,&splineItemList,kTrue,kTrue));
		//if(applyStrokeCmd==nil)
		//	break;

		//if(CmdUtils::ProcessCommand(applyStrokeCmd)!=kSuccess)
		//	break;
		
		CmdUtils::EndCommandSequence(sequence);
		newPageItem = newSplineItem;

		//CA("Box created successfully");
		returnValue=kSuccess;
	} while(false);
	return returnValue;
}
