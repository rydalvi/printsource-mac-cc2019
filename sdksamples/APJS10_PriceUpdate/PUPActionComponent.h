#include "VCPlugInHeaders.h"

// Interface includes:
#include "ISession.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "IActiveContext.h"
#include "IAppFramework.h"
// General includes:
#include "CActionComponent.h"
//#include "CAlert.h"
//#define CA(X) CAlert::InformationAlert(X)

// Project includes:
#include "PUPID.h"
class PUPActionComponent : public CActionComponent
{
public:
	
/**
 Constructor.
 @param boss interface ptr from boss object on which this interface is aggregated.
 */
		PUPActionComponent(IPMUnknown* boss);

		/** The action component should perform the requested action.
			This is where the menu item's action is taken.
			When a menu item is selected, the Menu Manager determines
			which plug-in is responsible for it, and calls its DoAction
			with the ID for the menu item chosen.

			@param actionID identifies the menu item that was selected.
			@param ac active context
			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
			@param widget contains the widget that invoked this action. May be nil. 
			*/
		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);
		virtual void OpenSelectFileDialog();
		void DoDialog();
		void CloseMainDialog();
		void CloseSelDocDialog();
		void OpenProcessDialog();
		void CloseProcessDialog();
		void OpenSummaryDialog();
		void CloseSummaryDialog();
		void ShowMainDialog();


		void UpdateActionStates (IActionStateList* iListPtr);
		void    UpdateActionStates(IActiveContext* ac, IActionStateList *listToUpdate, GSysPoint mousePoint, IPMUnknown* widget); 

	private:
		/** Encapsulates functionality for the about menu item. */
		void DoAbout();
		
		/** Opens this plug-in's dialog. */
		//void DoDialog();
		
	       /** Encapsulates functionality for the MenuItem1 menu item. */
		void DoMenuItem1(IActiveContext* ac);



};
