//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
// General includes:
#include "CDialogObserver.h"
#include "IDialogMgr.h"
#include "IDialog.h"
#include "IApplication.h"
//#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "IDialogController.h"
//#include "CActionComponent.h"
#include "PUPActionComponent.h"
// Project includes:
#include "PUPID.h"
#include "GlobalData.h"
#include "SDKFileHelper.h"
//#include "SDKListBoxHelper.h"
#include "SDKUtilities.h"
#include "FileUtils.h"
#include "PlatformFolderTraverser.h"
//#include "SDKListBoxHelper.h"
#include "ListBoxData.h"
#include "ITriStateControlData.h"

#include "IBookUtils.h"
#include "IDocument.h"
#include "UIDList.h"
#include "IDocumentList.h"

#include "ISpread.h"
#include "ISpreadList.h"
#include "IHierarchy.h"
#include "IFrameUtils.h"

#include "IProgressBarManager.h"
#include "ProgressBar.h"
#include "WidgetID.h"
#include "vector"
#include "AttrData.h"
#include "IStringListControlData.h"
#include "ITableModel.h"
#include "ITextModel.h"
#include "ITableModelList.h"
//#include "ITextFrame.h"
#include "ITextAttributeSuite.h"
#include "ISelectionUtils.h"
#include "ITableSelectionSuite.h"
#include "IConcreteSelection.h"
#include "ITableTextSelection.h"
#include "ITableModel.h"
#include "ISelectionManager.h"
#include "ISelectionUtils.h"
#include "TextEditorID.h"
#include "ITableSelectionSuite.h"
#include "ITextAttributeSuite.h"
#include "ISlugData.h"
#include "SlugStructure.h"
#include "ILayoutSelectionSuite.h"
#include "IDataSprayer.h"
#include "DSFID.h"
#include "IAppFramework.h"
#include "ITableCommands.h"
#include "IApplication.h"
#include "IDocumentList.h"
#include "IDataBase.h"
#include "SDKLayoutHelper.h"
#include "ITagReader.h"
//#include "ITagWriter.h"
#include "ITableTextContent.h"
//#include "SKUdata.h"
#include "MetaDataTypes.h"
#include "ITableUtility.h"
#include "CAlert.h"
//#include "LayoutUtils.h" //Cs3
#include "ILayoutUtils.h" //Cs4
#include "TagStruct.h"
#include "IBookManager.h"
#include "IBookContentMgr.h"
#include "IBook.h"
#include "IDataLink.h"
#include "ISession.h"

#include "CommonFunctions.h"
//Added due to cs3 change
#include "IHierarchy.h"
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
#include "ISelectionUtils.h"
#include "ITextColumnSizer.h"
#include "ITextFrameColumn.h"
#include "IGraphicFrameData.h"
#include "IPageItemTypeUtils.h"
#include <time.h>
#include "IXMLReferenceData.h"
#include "IXMLUtils.h"
#include "ILayoutUIUtils.h"
#include "IXMLAttributeCommands.h"
#include <stdio.h>
#include "iostream"
#include "fstream"


using namespace std;
PMString gActiveBookName;
//end
//#include "IDocument.h"
#include "CAlert.h"
inline PMString numToPMString(int32 num)
{
	PMString x;
	x.AppendNumber(num);
	return x;
}
#define CA(X) CAlert::InformationAlert \
	( \
		PMString("PUPDialogObserver.cpp") + PMString("\n") + \
		PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + \
		PMString("\n Message : ")+ X \
	)

//#define CA(X) CAlert::InformationAlert(X);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
using namespace std;
using namespace metadata;

bool16 IsChangePriceModeSelected = kFalse;
extern bool16 ISFirstDialog;
extern vector<ListBoxData> lstBoxData;
extern vector<SelectedDoc> selDoc;
extern IControlView* PriceComboControlView;
extern IControlView* OldPriceComboView;
extern IControlView* NewPriceComboView;
extern IControlView* EventListControlView;

static int32 TotalTagsInDocument ; 
static int32 TotalUnusedTagsRemoved;


class BookContentDocInfo
{
public:
	PMString DocumentName;
	IDFile DocFile;
	int32 index;
	UID DocUID;
	//IDocument* documentPtr;
};

typedef vector<BookContentDocInfo> BookContentDocInfoVector;

/** Implements IObserver based on the partial implementation CDialogObserver.

	
	@ingroup apjs9_priceupdate
*/
class PUPDialogObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		int32 dlgNo;
		vector<PMString> fileList;
		DataList dataList;
		PMString folderName;
		bool16 cancelClicked;

		PUPDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~PUPDialogObserver() {}

		/**
			Called by the application to allow the observer to attach to the subjects
			to be observed, in this case the dialog's info button widget. If you want
			to observe other widgets on the dialog you could add them here.
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange,
			ISubject* theSubject,
			const PMIID& protocol,
			void* changedBy
		);
		virtual void SetFolderName(PMString nameOfFolder);
		virtual int32 SetFileNames();
		void makeList();
		void SelectDeselectAllEntries(bool16 checked);
		bool16 StartUpdate();
		void UpdateDocument(InterfacePtr<IDocument> bgDoc,PMString);
		bool16 IsAnyDocSelected();
		void UpdateParentList(const UIDRef& tableUIDRef);
		PMString GetCellText(InterfacePtr<ITableModel>& tableModel,int32 startIndex,int32 endIndex);
		void UpdateTabbedData(const UIDRef& tableUIDRef,PMString FileName,int32 pageNo);
		void UpdateTabelData(InterfacePtr<ITableModel>& tableModel,const UIDRef& TextUIDRef,PMString FileName,int32);
		void GentrateReport(void);
		void UpdateTabelDataWithXML(InterfacePtr<ITableModelList>& tableList,const UIDRef& TextUIDRef,PMString FileName,int32 pageNo);
		void UpdateTabbedDataWithXML(const UIDRef& tableUIDRef,PMString FileName,int32 pageNo);
		bool16 GetTextstoryFromBox(/*InterfacePtr<ITextModel>*/ITextModel* iModel, int32 startIndex, int32 finishIndex, PMString& story);
		BookContentDocInfoVector* GetBookContentDocInfo(IBookContentMgr* bookContentMgr);
		void UpdateTotalItemList(TagStruct tStruct);
		bool16 doesExist(TagList &tagList,UIDList selectUIDList);

		void UpdateDocumentJL(InterfacePtr<IDocument> bgDoc,PMString);	
		void AddKeysWithoutSectionIds(IIDXMLElement* xmlElement, set<PMString>& resultIds);
		void FieldSwapJL(IIDXMLElement* xmlElement, vector<IndexReference> &indexReferences, 
									   bool isTable, set<WideString> &childIds, bool isChild,
									   WideString oldFieldId, WideString newFieldId, WideString oldLanguageId, PMString FileName);

		//bool16 IsAnyTableInFrame(InterfacePtr<TextFrame>textFrame,InterfacePtr<ITableModelList>&tableList);

};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(PUPDialogObserver, kPUPDialogObserverImpl)

/* AutoAttach
*/
bool16 PUPDialogObserver::IsAnyDocSelected()
{
	 bool16 result=kFalse;
	int32/*int64*/ totDocs=static_cast<int32>(lstBoxData.size()); //cs4
	int32 count=0;
	////////////Traversing through List
	/*List::*/selDoc.clear();
	for(int32 index=0;index<totDocs;index++)
	{
		if(/*List::*/lstBoxData[index].isChecked)
		{
			SelectedDoc sDoc;
			sDoc.docName=lstBoxData[index].fileName;
			sDoc.isBook = lstBoxData[index].isBook;
			selDoc.push_back(sDoc);
			result=kTrue;
		}
	}
return result;
}
void PUPDialogObserver::UpdateDocument(InterfacePtr<IDocument> bgDoc,PMString FileName)
{
	int32 TotalPageCount=-1;
	InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)bgDoc,UseDefaultIID());
	if (iSpreadList==nil)
	{
		CA("Error iSpreadList==nil");
		return;
	}
	//iSpreadList->AddRef();
	///////////////For loop for investigeting all spreads////////////////////////
	for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
	{
		UIDRef spreadUIDRef(::GetDataBase(bgDoc), iSpreadList->GetNthSpreadUID(numSp));

		InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
		if(!spread)
		{
			CA("Error !spread");
			return;
		}
		int numPages=spread->GetNumPages();

		////////////FOR loop for investigeting all pages of Spread///////////
		for(int pageNo=0; pageNo<numPages; pageNo++)
		{
			//CA("pageeeee");
			TotalPageCount++;
			
			UIDList itemList(::GetDataBase(bgDoc));
			UID pageUIDs = spread->GetNthPageUID(pageNo);
			spread->GetItemsOnPage(pageNo, &itemList, kFalse);
///////////////////////////////////////////////////////////////////////////////////////
			for( int p=0; p<itemList.Length(); p++)
			{	
				InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
				if(!itagReader)
					return ;
				InterfacePtr<IHierarchy> iHier(itemList.GetRef(p), UseDefaultIID());
				if(!iHier)
					continue;
				UID kidUID;
				int32 numKids=iHier->GetChildCount();
				bool16 isGroupFrame = kFalse ;
				isGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(itemList.GetRef(p));

				for(int j=0;j<numKids;j++)
				{
					//CA("1");
					kidUID=iHier->GetChildUID(j);
					UIDRef boxRef(itemList.GetDataBase(), kidUID);
					TagList tList;					
					if(isGroupFrame == kTrue) 
					{
						tList = itagReader->getTagsFromBox(boxRef);
					}
					else 
					{
						tList = itagReader->getTagsFromBox(itemList.GetRef(p));						
					}

					if(!doesExist(tList,itemList))
					{
						itemList.Append(kidUID);
					}
				}
			}
//////////////////////////////////////////////////////////////////////
			/*PMString listCount("Total List Element: ");
			listCount.AppendNumber(itemList.Length());
			CA(listCount);*/

			/////FOR loop for investigeting all items of page//////////////
			for(int count=0;count<itemList.Length();count++)
			{
				//CA("page itemsssss");
				InterfacePtr<IHierarchy> iHier(itemList.GetRef(count), UseDefaultIID());
				if(!iHier)
				{
					//CA("Continue   ");					
					continue;
				}
					
				/*InterfacePtr<IPMUnknown> Funknown(itemList.GetRef(count), IID_IUNKNOWN);
				UID frameUID1 = Utils<IFrameUtils>()->GetTextFrameUID(Funknown);*/
				UID frameUID1 = kInvalidUID;
				InterfacePtr<IGraphicFrameData> graphicFrameDataOne(itemList.GetRef(count), UseDefaultIID());
				if (graphicFrameDataOne) 
				{
					frameUID1 = graphicFrameDataOne->GetTextContentUID();
				}
				if(frameUID1==kInvalidUID)
				{
					//CA("Main Graphic Frame");
				}
				else
				{
						InterfacePtr<IHierarchy>graphicFrameHierarchy(itemList.GetRef(count), UseDefaultIID());
						if (graphicFrameHierarchy == nil) 
						{
							//CA("graphicFrameHierarchy == nil");
							return;
						}
										
						InterfacePtr<IHierarchy>multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
						if (!multiColumnItemHierarchy) {
							//CA("multiColumnItemHierarchy == nil");
							return;
						}

						InterfacePtr<IMultiColumnTextFrame>multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
						if (!multiColumnItemTextFrame) {
							ASSERT(multiColumnItemTextFrame);
							//CA("multiColumnItemTextFrame == nil");
							return;
						}
						InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
						if (!frameItemHierarchy) {
							//CA("frameItemHierarchy == nil");
							continue;
						}

						InterfacePtr<ITextFrameColumn>textFrame(frameItemHierarchy, UseDefaultIID());
						if (!textFrame) {
							//CA("!!!ITextFrameColumn");
							continue;
						}
					//To this
						InterfacePtr<ITextModel>textModel(textFrame->QueryTextModel());
						if (textModel == nil)
						{
							//CA("textModel == nil");
							continue;
						}
						InterfacePtr<ITableModelList>tableList(textModel, UseDefaultIID());
						if(tableList==nil)
						{
							//CA("tableList==nil");
							continue;
						}
						int32	tableIndex = tableList->GetModelCount() ;

						//14July..Yogesh
						bool16 customItemTableFound = kFalse;
						for(int32 index = 0;index < tableIndex ; index++)
						{
							
							InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(index));
							if(tableModel == nil) 
							{
								continue;
							}
							InterfacePtr<IXMLReferenceData>xmlRefData(tableModel,UseDefaultIID());
							if(xmlRefData == nil)
							{
								continue;
							}
							XMLReference tableXMLRef = xmlRefData->GetReference();
							//IIDXMLElement * tableXMLElementPtr = tableXMLRef.Instantiate();
							InterfacePtr<IIDXMLElement>tableXMLElementPtr(tableXMLRef.Instantiate());
							if(tableXMLElementPtr == nil)
							{
								continue;
							}
							PMString strIDValue = tableXMLElementPtr->GetAttributeValue(WideString("ID")); //Cs4
							if(strIDValue == "-102")
							{
								customItemTableFound = kTrue;
								break;
							}
						}
						//end 14July..Yogesh
						if(tableIndex<=0 || customItemTableFound) //This check is very important...  this ascertains if the table is in box or not.
						{
							//CA("tableIndex<=0 || customItemTableFound");
							//UIDRef TextUIDRef(::GetUIDRef(textFrame)); //CS3 Change
							UIDRef TextUIDRef = itemList.GetRef(count);
							//UpdateTabbedData(TextUIDRef,FileName,TotalPageCount);
							UpdateTabbedDataWithXML(TextUIDRef,FileName,TotalPageCount);
								continue;
						}
						else
						{		
							//CA("tableIndex > 0 || ! customItemTableFound");
							/*InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(p));
							if(tableModel == nil) 
							{
								continue;
							}*/
							//UIDRef textUIDRef(::GetUIDRef(textFrame)); //CS3Change
							UIDRef textUIDRef = itemList.GetRef(count);
							UpdateTabelDataWithXML(tableList,textUIDRef,FileName,TotalPageCount);
							
						}

					}
			}
			/////end  ofFOR loop for investigeting all items of page//////////////			
		}
		////////////End of FOR loop for investigeting all pages of Spread///////////
	}
	///////////////end of FOR loop for investigeting all spreads//////////////////
}

bool16 PUPDialogObserver::StartUpdate()
{
	bool16 result=kFalse;
	//CA("In Start Update");
	int32/*int64*/ totElements=static_cast<int32>(/*List::*/selDoc.size()); //Cs4
	PMString total("Total Files: ");
	total.AppendNumber(totElements);
	//CA(total);
	
	GlobalData::TotalDoc=totElements;
	GlobalData::ProductList.clear();
	GlobalData::skuDataList.clear();
	GlobalData::TotalItemList.clear();
	GlobalData::TotalItemPresentList.clear();
	PMString title("Price Update in Progress...");
	title.SetTranslatable(kFalse);
	//bool16 IsBookOpen = kFalse;
	
	RangeProgressBar progressBar(title, 0, totElements, kTrue);
	for(int32 currentFileIndex=0;currentFileIndex<totElements;currentFileIndex++)
	{
		//CA("9999");
		
		IDFile flName=/*List::*/selDoc[currentFileIndex].docName;
		//CA(flName);
		if(/*List::*/selDoc[currentFileIndex].isBook == 1)
		{
			//IsBookOpen = kTrue;
			continue;
		}
		
		if(currentFileIndex > 0)
		{
			CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 
		}

		bool16 flag1=kFalse;
		bool16 flag2=kFalse;

		//IDocument* fntDoc = Utils<IBookUtils>()->OpenDocAtBackground 
		//				(
		//					flName, // in
		//					flag1,// out
		//					flag2
		//				);
		//InterfacePtr<IDocument>bgDoc(fntDoc,UseDefaultIID());
		//bgDoc->AddRef();
		//UIDRef wsUIDRef=bgDoc->GetDocWorkSpace();
		//UIDList itemList(wsUIDRef.GetDataBase());
		/*InterfacePtr<IApplication> App(gSession->QueryApplication());
		InterfacePtr<IDocumentList> Doclist(App->QueryDocumentList());

		IDocument* fntDoc = Doclist->OpenDoc(flName,IDataBase::kProtectMiniSave);*/
		//IDocument* fntDoc=::GetFrontDocument();

		SDKLayoutHelper sdklhelp;
		UIDRef CurrDocRef = sdklhelp.OpenDocument(flName);

		ErrorCode err = sdklhelp.OpenLayoutWindow(CurrDocRef);
		/*InterfacePtr<IDocument>bgDoc(fntDoc,UseDefaultIID());
		bgDoc->AddRef();
		UIDRef wsUIDRef=bgDoc->GetDocWorkSpace();*/
		//UIDList itemList(wsUIDRef.GetDataBase());

		InterfacePtr<IDocument> currDoc(CurrDocRef,UseDefaultIID());

		//currDoc->AddRef();

		/*UIDList itemList(::GetDataBase(bgDoc));
		PMString totPageItem("Total Page Item: ");
		totPageItem.AppendNumber(itemList.Length());*/
		//CA("Before");		
		PMString FileName("");
        #if WINDOWS
            FileName= file.GetString();
        #else
            FileName = FileUtils::SysFileToPMString(flName);
        #endif
        
        
        //FileName.Append(FileName);
		//CA(FileName);
		FileName.SetTranslatable(kFalse);
		progressBar.SetTaskText(FileName);
		
		//this->UpdateDocument(currDoc,FileName);
		this->UpdateDocumentJL(currDoc,FileName);
		//CA("after");
       
		//CA("111");	
		/*InterfacePtr<IApplication> AppHandler(gSession->QueryApplication());
		if (AppHandler == nil)
		{
			CA("AppHandler invalid");
			return kFalse;
		}
		CA("222");
		InterfacePtr<IDocumentList> Doclist(AppHandler->QueryDocumentList());
   		if (Doclist == nil)
		{
			CA("Doclist invalid");
			return kFalse;
		}*/
		//CA("333");
		sdklhelp.SaveDocumentAs(CurrDocRef,flName);
		//CA("Afetr Saving");
		//Doclist->CloseDoc(bgDoc);
		sdklhelp.CloseDocument(CurrDocRef, kFalse);
	
		//currentFileIndex++;
		progressBar.SetPosition(currentFileIndex);
		if(progressBar.WasCancelled())
		{
			//CA("Cancel of ProgressBar is clicked");
			progressBar.Abort();
			PMString lastIndex("Last Index is: ");
			lastIndex.AppendNumber(currentFileIndex);
			//CA(lastIndex);
			return kTrue;
		}
		//CA("444");

	}
	//CA("555");

	//if(IsBookOpen)
	//{
	//	do
	//	{
	//		InterfacePtr<IBookManager> bookManager(gSession, UseDefaultIID());
	//		if (bookManager == nil) 
	//		{ 
	//			//CA("There is not book manager!");
	//			break; 
	//		}
	//		if (bookManager->GetBookCount() <= 0)
	//		{
	//			//CA("There is no book open. You must first open a book before running this snippet.");
	//			break;
	//		}

	//		int32 bookCount = bookManager->GetBookCount();
	//		
	//		for (int32 i = 0 ; i < bookCount ; i++) 
	//		{
	//			IBook* book = bookManager->GetNthBook(i);
	//			if (book == nil) 
	//			{
	//				 //go onto the next book...
	//				continue;
	//			}				
	//			bookManager->CloseBook(book);			
	//		}
	//	}while(0);

	//}
	GentrateReport();
	
	return kTrue;
}
void PUPDialogObserver::SelectDeselectAllEntries(bool16 checked)
{
	////CA("In select deselect");
	///*InterfacePtr<IPanelControlData>panelData(this,UseDefaultIID());
	//if(!panelData)
	//{
	//	CA("Dialog's Panel Control Data is null");
	//	return;
	//}*/
	//SDKListBoxHelper lstBoxHelper(this,kPUPPluginID);
	////IControlView *listControlView=lstBoxHelper.FindCurrentListBox(panelData,1);
	///*if(!listControlView)
	//{
	//	CA("List Control View in Select Entry is null");
	//}*/
	//if(!GlobalData::ListBoxControlView)
	//{
	//	CA("IN SElECT List Box is not valid");
	//	return;
	//}
	//int32/*int64*/ totEntries=static_cast<int32>(lstBoxData.size()); //Cs4
	//
	//for(int32 entryNo=0;entryNo<totEntries;entryNo++)
	//{
	//	lstBoxHelper.CheckUncheckRow(GlobalData::ListBoxControlView,entryNo,checked);
	//	/*List::*/lstBoxData[entryNo].isChecked=checked;
	//	//CA("Checked");
	//}
}
void PUPDialogObserver::makeList()
{
	//InterfacePtr<IPanelControlData>panelData(this,UseDefaultIID());
	//if(!panelData)
	//{
	//	CA("Dialog's Panel Control Data is null");
	//	return;
	//}
	//SDKListBoxHelper lstBoxHelper(this,kPUPPluginID);
	//IControlView *listControlView=lstBoxHelper.FindCurrentListBox(panelData,1);
	//if(!listControlView)
	//{
	//	CA("In making list control view is null 1");
	//	return;
	//}
	////listControlView->AddRef();
	//GlobalData::ListBoxControlView=listControlView;
	//lstBoxHelper.EmptyCurrentListBox(panelData,1);
	////int32 totFiles=this->dataList.size();
	//int32/*int64*/ totFiles= static_cast<int32>(lstBoxData.size()); //Cs4
	//PMString msg("Total files in list: ");
	//msg.AppendNumber(totFiles);
	////CA(msg);
	//for(int32 index=0;index<totFiles;index++)
	//{		
	//	PMString OrgFileName("");
	//	FileUtils::GetBaseFileName(this->dataList[index].fileName,OrgFileName);
	//	//CA(OrgFileName);
	//	PMString FileExtension("");
	//	FileUtils::GetExtension(this->dataList[index].fileName,FileExtension);
	//	//CA(FileExtension);
	//	OrgFileName.Append(".");
	//	OrgFileName.Append(FileExtension);
	//	PMString FinalFileName("");

	//	if(this->dataList[index].isBook == 2)
	//	{
	//		FinalFileName.Append("     ");
	//		FinalFileName.Append(OrgFileName);
	//	}
	//	else
	//		FinalFileName = OrgFileName;
	//	lstBoxHelper.AddElement(listControlView,FinalFileName,kSPRFltOptTextWidgetID);
	//	lstBoxHelper.CheckUncheckRow(listControlView,index,/*List::*/lstBoxData[index].isChecked);
	//	//CA(name);
	//}
	////CA("Returning from Make List function");
}
int32 PUPDialogObserver::SetFileNames()
{
	cancelClicked=kFalse;
	InterfacePtr<IDialogController>dlgControler(this,UseDefaultIID());
	if(!dlgControler)
	{
		CA("IDialogControler is null");
		return -1;
	}
	bool16 initialState;
	if(dlgControler->GetTriStateControlData(kPUPAllDocCheckBoxWidgetID)==ITriStateControlData::kSelected)
		initialState=kTrue;
	else if(dlgControler->GetTriStateControlData(kPUPAllDocCheckBoxWidgetID)==ITriStateControlData::kUnselected)
		initialState=kFalse;
	
	int imagecounter=0;
	do
	{	
		//const int32 cCountOfOptions = 1;
		//K2Vector<PMString> optionsVec(cCountOfOptions);
		//PMString assetPathTitle(kPRLMenuItem1MenuItemKey);
	//	assetPathTitle.Translate();
	//	assetPathTitle.SetTranslatable(kFalse);
		SDKFolderChooser folderChooser;
		folderChooser.SetTitle("Select Folder");
		folderChooser.ShowDialog();
		if(!folderChooser.IsChosen())
		{	
			cancelClicked=kTrue;
			return /*List::*/static_cast<int32>(lstBoxData.size());
		}
		
		this->folderName=folderChooser.GetPath();
//		CA(folderName);
		if(this->folderName.IsEmpty() == kTrue)
		{	CA("Please Select document folder.");
			return -1;
		}			
		/*List::*/lstBoxData.clear();
		
		SDKFileHelper rootFileHelper(this->folderName);
		IDFile rootSysFile = rootFileHelper.GetIDFile();
		//CA(rootSysFile.GetString());
        
        PlatformFolderTraverser folderTraverser(rootSysFile, kFalse, kTrue, kFalse, kTrue);
        IDFile sysfile;
        while (folderTraverser.Next(&sysfile))
        {
            PMString extension;
            FileUtils::GetExtension(sysfile, extension);
            if (extension == PMString("DS_Store"))
            {
                continue;
            }
            
            SDKFileHelper fileHelper(sysfile);
        
            if(extension == (PMString(".indd")))
            {
                PMString baseFile("");
                /*FileUtils::GetBaseFileName(fil,baseFile);
                 fileList.push_back(baseFile);*/
                ListBoxData listData;
                listData.fileName=sysfile;
                listData.isBook = 0;
                listData.isChecked=initialState;
                /*List::*/lstBoxData.push_back(listData);
                this->dataList.push_back(listData);
                //CA(fil);
            }
            if(extension == (PMString(".indb")))
            {
                PMString baseFile("");
                /*FileUtils::GetBaseFileName(fil,baseFile);
                 fileList.push_back(baseFile);*/
                ListBoxData listData;
                listData.fileName=sysfile;
                listData.isBook = 1;
                listData.isChecked=initialState;
                /*List::*/lstBoxData.push_back(listData);
                this->dataList.push_back(listData);
                //CA("fil11111111111");
                
                InterfacePtr<IBookManager> bookManager(/*gSession*/GetExecutionContextSession(), UseDefaultIID()); //Cs4
                if (bookManager == nil)
                {
                    CA("There is not book manager!");
                    break;
                }
                Bool16 ISBookOpen;
                IBook* book = bookManager->OpenBook(sysfile, IDataBase::kProtectSave , &ISBookOpen, kFalse, IBookManager::kIgnoreMissing);
                
                InterfacePtr<IBookContentMgr> bookContentMgr(book, UseDefaultIID());
                if (bookContentMgr == nil)
                {
                    CA("This book doesn't have a book content manager!  Something is wrong.");
                    break;
                }
                
            }
        }


        /*

		PlatformFileSystemIterator iter;
		if(!iter.IsDirectory(rootSysFile))
		{	
			CA("Invalid Directory..");
			return 0;
		}
		//#ifdef WINDOWS
		//	// Windows dir iteration a little diff to Mac
		//	CA("4");
		//	FileUtils::AppendPath(&rootSysFile,fFilter);	
		//	PMString RootSYSFile = rootSysFile.GetString();
		//	RootSYSFile.Append("\\");
		//	//FileUtils::PMStringToIDFile(RootSYSFile,rootSysFile);
		//	CA(rootSysFile.GetString());
		//	
		//#endif
			iter.SetStartingPath(rootSysFile);
		IDFile sysFile;
		PMString filter("\\*.*");
		bool16 hasNext= iter.FindFirstFile(sysFile,filter);
		//CA(sysFile.GetString());
		//do{
		//if(hasNext){
		while(hasNext)
		{	
			
			SDKFileHelper fileHelper(sysFile);
			hasNext=iter.FindNextFile(sysFile);
			if(!hasNext)
				break;
			
			//const IDFile fil=sysFile;
			//PMString dispfile=SDKUtilities::SysFileToPMString(&fil);
			//////////////////////////////
			//PMString lcPath = dispfile;
			PMString lcPath = sysFile.GetFileName();
			lcPath.ToLower();

			
			if(lcPath.Contains(PMString(".indd")))
			{				
				PMString baseFile("");
				ListBoxData listData;
				listData.fileName=sysFile;
				listData.isBook = 0;
				listData.isChecked=initialState;
				lstBoxData.push_back(listData);
				this->dataList.push_back(listData);
				//CA(fil);
			}
			if(lcPath.Contains(PMString(".indb")))
			{				
				PMString baseFile("");
				ListBoxData listData;
				listData.fileName=sysFile;
				listData.isBook = 1;
				listData.isChecked=initialState;
				lstBoxData.push_back(listData);
				this->dataList.push_back(listData);
				//CA("fil11111111111");

				InterfacePtr<IBookManager> bookManager(GetExecutionContextSession(), UseDefaultIID()); //Cs4
				if (bookManager == nil) 
				{ 
					CA("There is not book manager!");
					break; 
				}
				Bool16 ISBookOpen;
				IBook* book = bookManager->OpenBook(sysFile, IDataBase::kProtectSave , &ISBookOpen, kFalse, IBookManager::kIgnoreMissing);

				InterfacePtr<IBookContentMgr> bookContentMgr(book, UseDefaultIID());
				if (bookContentMgr == nil) 
				{
					CA("This book doesn't have a book content manager!  Something is wrong.");
					break;
				}
//New code to be add here

			//	 = this->GetBookContentNames(bookContentMgr);
				//BookContentDocInfoVector* BookContentinfovector = NULL;
				//BookContentinfovector = this->GetBookContentDocInfo(bookContentMgr);
				//
				//if(BookContentinfovector->size()<=0)
				//{
				//	CA(" BookContentinfovector->size()<=0 ");
				//	break;
				//}

				//BookContentDocInfoVector::iterator it1;
				//for(it1 = BookContentinfovector->begin(); it1 != BookContentinfovector->end(); it1++)
				//{
				//	ListBoxData listData ;
				//	listData.isBook = 2;
				//	listData.isChecked=initialState;
				//	listData.fileName= it1->DocFile ;
				//	List::lstBoxData.push_back(listData);
				//	this->dataList.push_back(listData);
				//	//CA("fil");
				//}

				//bookManager->CloseAll();  //			CloseBook(book);
				//for(int j=0; j<bookContentNames.size(); j++)
				//{
				//	PMString baseFile("");
				//	ListBoxData listData ;
				//	SDKFileHelper booktitleFileHelper(book->GetBookFileSpec());
				//	PMString bookTitleName = booktitleFileHelper.GetPath();
				//	CA(bookTitleName);
				//	//listData.fileName=  ;
				//	CA(bookContentNames[j]);
				//	listData.isBook = 2;
				//	listData.isChecked=initialState;
				//	List::lstBoxData.push_back(listData);
				//	this->dataList.push_back(listData);
				//	CA("fil");
				//}		

			}
			
		}*/
	} while(kFalse);
	
	return static_cast<int32>(lstBoxData.size()); //Cs4
}
void PUPDialogObserver::SetFolderName(PMString nameOfFolder)
{
	//GlobalData::folderName=nameOfFolder;
	InterfacePtr<IDialogController>dlgController(this,UseDefaultIID());
	if(!dlgController)
	{
		CA("Dialog controller is null");
		return;
	}
	dlgController->SetTextControlData(kFolderNameWidgetID,"");
	dlgController->SetTextControlData(kFolderNameWidgetID,nameOfFolder);
	
}
void PUPDialogObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) 
		{
			break;
		}
		// Attach to other widgets you want to handle dynamically here.
		AttachToWidget(kSelectDocButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		AttachToWidget(kPriceAttrButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		AttachToWidget(kUpdateButtonWidgetID,	IID_IBOOLEANCONTROLDATA,panelControlData);
		AttachToWidget(kNewUpdateButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kFinishButtonWidgetID,	IID_ITRISTATECONTROLDATA,panelControlData);
		//AttachToWidget(kCancelButtonWidgetID,	IID_IBOOLEANCONTROLDATA,panelControlData);
		AttachToWidget(kBrowseButtonWidgetID,	IID_IBOOLEANCONTROLDATA,panelControlData);
		AttachToWidget(kFolderNameWidgetID,		IID_ITEXTCONTROLDATA,panelControlData);
		AttachToWidget(kPUPAllDocCheckBoxWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kProgressBarWidgetID,	IID_IPROGRESSBARCONTROLDATA,panelControlData);
		AttachToWidget(kPUPAttributeComboBoxWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		AttachToWidget(kRediobtnRefreshPriceID,	IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kRediobtnChangePriceID,	IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kNewPriceUpdateButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kCancelPriceUpdateButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
        AttachToWidget(kPUPEventListComboBoxWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		

	} while (kFalse);
}

/* AutoDetach
*/
void PUPDialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
		DetachFromWidget(kSelectDocButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kPriceAttrButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kUpdateButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kNewUpdateButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kFinishButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		//DetachFromWidget(kCancelButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kBrowseButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kFolderNameWidgetID,IID_ITEXTCONTROLDATA,panelControlData);
		DetachFromWidget(kPUPAllDocCheckBoxWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kProgressBarWidgetID,IID_IPROGRESSBARCONTROLDATA,panelControlData);
		DetachFromWidget(kPUPAttributeComboBoxWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		DetachFromWidget(kRediobtnRefreshPriceID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kRediobtnChangePriceID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kNewPriceUpdateButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kCancelPriceUpdateButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kPUPEventListComboBoxWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		

	} while (kFalse);
}

/* Update
*/
void PUPDialogObserver::Update
(
	const ClassID& theChange,
	ISubject* theSubject,
	const PMIID& protocol,
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);
	InterfacePtr<IPanelControlData>panelData(this,UseDefaultIID());
	if(!panelData)
	{
		//CA("Panel Control Data is null");
		return;
	}

	do
	{
		PUPActionComponent ac(this);
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		ASSERT(controlView);
		if(!controlView)
		{
			break;
		}
		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();
		// TODO: process this

		if(theSelectedWidget==kPBDlgCancelButtonWidgetID && theChange==kTrueStateMessage)
		{
			//CA("Cancel of Progressbar is clicked");
			break;
		}
		else if(theSelectedWidget==kSelectDocButtonWidgetID && theChange==kTrueStateMessage)
		{
			//InterfacePtr<IApplication> application(gSession->QueryApplication());
			//ASSERT(application);
			//if (application == nil) 
			//{	
			//	break;
			//}
			//InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
			//ASSERT(dialogMgr);
			//if (dialogMgr == nil)
			//{
			//	break;
			//}

			//// Load the plug-in's resource.
			//PMLocaleId nLocale = LocaleSetting::GetLocale();
			//RsrcSpec dialogSpec
			//(
			//	nLocale,					// Locale index from PMLocaleIDs.h. 
			//	kPUPPluginID,			// Our Plug-in ID  
			//	kViewRsrcType,				// This is the kViewRsrcType.
			//	kSDKDefSelectFileDialogResourceID,	// Resource ID for our dialog.
			//	kTrue						// Initially visible.
			//);

			//// CreateNewDialog takes the dialogSpec created above, and also
			//// the type of dialog being created (kMovableModal).
			//IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kModeless);
			//ASSERT(dialog);
			//if (dialog == nil) 
			//{
			//	break;
			//}

			//// Open the dialog.
			//dialog->Open();

			PMString attrName;
			InterfacePtr<IDialogController>dlgControler(this,UseDefaultIID());
			if(!dlgControler)
			{
				//CA("DlgControler is null");
				return;
			}

			attrName=dlgControler->GetTextControlData(kPUPAttributeComboBoxWidgetID);
			InterfacePtr<IControlView>ctrlView(panelData->FindWidget(kPUPAttributeComboBoxWidgetID));
			if(!ctrlView)
			{
				//CA("Ctrl View Is null");
				break;
			}

			//ctrlView->AddRef();
			InterfacePtr<IStringListControlData>strListCtrlData(ctrlView,UseDefaultIID());
			if(!strListCtrlData)
			{
				//CA("Strin list control data is null");
				break;
			}

			int32 index=strListCtrlData->GetIndex(attrName);

			if(index == 0)
			{
				//CA("Please select the Attribute");
				break;
			}

			/*PMString as("Index is...");
			as.AppendNumber(index);
			CA(as);*/

			AttributeData attrdata;
			AttrData attr1 = attrdata.getAttrVectorInfo(index);
			int32 temp = attr1.getPubId();
            
            if(temp == -1)
            {
                break;
            }
			GlobalData::SelectedAttributeId = temp;
			GlobalData::Table_col = attr1.getTable_col();

			/*PMString qw("GlobalData::SelectedAttributeId:::::::");
			qw.AppendNumber(temp);
			CA(qw);*/

			ac.CloseMainDialog();
			ac.OpenSelectFileDialog();
			
			if(GlobalData::ListBoxControlView)
				this->SelectDeselectAllEntries(kTrue);
			//CA("End of Updatae");
		}
		else if(theSelectedWidget==kPriceAttrButtonWidgetID && theChange==kTrueStateMessage)
		{
			//ac.CloseSelDocDialog();
			//ac.DoDialog();
			ac.ShowMainDialog();
		}
		else if(theSelectedWidget==kUpdateButtonWidgetID && theChange==kTrueStateMessage)
		{
			//ac.CloseSelDocDialog();
			if(/*List::*/lstBoxData.size()>0)
			{
				if(this->IsAnyDocSelected())
				{
					ac.CloseMainDialog();
					GlobalData::isUpdateStart=kTrue;
					//ac.OpenProcessDialog();
					if(GlobalData::isUpdateStart)
					{
						this->StartUpdate();
						//CA("after StartUpdate");
						GlobalData::isUpdateStart=kFalse;
						//ac.CloseProcessDialog();						
						ac.OpenSummaryDialog();						
					}
				}
				else
				{
					CA("No Document is selected");
				}
			}
			else
			{
				CA("No document in List");
			}
		}
//		else if(theSelectedWidget==kCancelButtonWidgetID && theChange==kTrueStateMessage)
//		{
//			ac.CloseProcessDialog();
//			GlobalData::isUpdateStart=kFalse;
//			ac.OpenSummaryDialog();
//		}
		else if(theSelectedWidget==kNewUpdateButtonWidgetID && theChange==kTrueStateMessage)
		{	//CA("1");
			ac.CloseSummaryDialog();
			ISFirstDialog = kTrue;
			ac.DoDialog();
			//ac.ShowMainDialog();
			//CA("2");
		}
		else if(theSelectedWidget==kFinishButtonWidgetID&& theChange==kTrueStateMessage)
		{
			ac.CloseSummaryDialog();
		}
		else if(theSelectedWidget==kBrowseButtonWidgetID&& theChange==kTrueStateMessage)
		{
			
			//CA("Browse Button Clicked");
			int32 totFiles=this->SetFileNames();
			//CA("File Names are Set");
			this->SetFolderName(this->folderName);
			//CA("Folder name is set");
			if(cancelClicked)
				break;
			if(totFiles==0)
			{
				CA("No InDesign documents found in this folder");
			}
			makeList();
		}
		/*if(theSelectedWidget==kPUPAllDocCheckBoxWidgetID)
		{
			
			if(dlgControler->GetTriStateControlData(kPUPAllDocCheckBoxWidgetID)==ITriStateControlData::kSelected)
			{
				SelectDeselectAllEntries(kTrue);
			}
			if(dlgControler->GetTriStateControlData(kPUPAllDocCheckBoxWidgetID)==ITriStateControlData::kUnselected)
			{
				SelectDeselectAllEntries(kFalse);
			}
		}*/
		else if(theSelectedWidget==kPUPAllDocCheckBoxWidgetID && theChange==kTrueStateMessage)
		{
			//if(dlgControler->GetTriStateControlData(kPUPAllDocCheckBoxWidgetID)==ITriStateControlData::kSelected)
			//{
				SelectDeselectAllEntries(kTrue);
			//}
		}
		else if(theSelectedWidget==kPUPAllDocCheckBoxWidgetID && theChange==kFalseStateMessage)
		{
			//if(dlgControler->GetTriStateControlData(kPUPAllDocCheckBoxWidgetID)==ITriStateControlData::kSelected)
			//{
				SelectDeselectAllEntries(kFalse);
			//}
		}
		else if(theSelectedWidget == kRediobtnRefreshPriceID && theChange==kTrueStateMessage)
		{
			IControlView* DropdwnRefreshPriceView = panelData->FindWidget(kPUPAttributeComboBoxWidgetID);
			if(DropdwnRefreshPriceView == nil)
				return;

			IControlView* DropdwnOldPriceView = panelData->FindWidget(kPUPOldAttributeComboBoxWidgetID);
			if(DropdwnOldPriceView == nil)
				return;

			IControlView* DropdwnNewPriceView = panelData->FindWidget(kPUPNewAttributeComboBoxWidgetID);
			if(DropdwnNewPriceView == nil)
				return;		
			
			DropdwnRefreshPriceView->Enable();
			DropdwnOldPriceView->Disable();
			DropdwnNewPriceView->Disable();
			IsChangePriceModeSelected = kFalse;
		}

		else if(theSelectedWidget == kRediobtnChangePriceID && theChange==kTrueStateMessage)
		{
			IControlView* DropdwnRefreshPriceView = panelData->FindWidget(kPUPAttributeComboBoxWidgetID);
			if(DropdwnRefreshPriceView == nil)
				return;

			IControlView* DropdwnOldPriceView = panelData->FindWidget(kPUPOldAttributeComboBoxWidgetID);
			if(DropdwnOldPriceView == nil)
				return;

			IControlView* DropdwnNewPriceView = panelData->FindWidget(kPUPNewAttributeComboBoxWidgetID);
			if(DropdwnNewPriceView == nil)
				return;		
			
			DropdwnRefreshPriceView->Disable();
			DropdwnOldPriceView->Enable();
			DropdwnNewPriceView->Enable();
			IsChangePriceModeSelected = kTrue;
		}
		if(theSelectedWidget == kCancelPriceUpdateButtonWidgetID && theChange == kTrueStateMessage)
		{
			CDialogObserver::CloseDialog();
			break;
		}


		else if(theSelectedWidget == kNewPriceUpdateButtonWidgetID && theChange==kTrueStateMessage)
		{	
			InterfacePtr<IBookManager> bookManager(/*gSession*/GetExecutionContextSession(), UseDefaultIID()); //Cs4
			if (bookManager == nil) 
			{ 
				//CA("There is not book manager!");
				break; 
			}
		
			if (bookManager->GetBookCount() <= 0)
			{
				//CA("There is no book open. You must first open a book before running this snippet.");
				break;
			}
		
			int32 bookCount = bookManager->GetBookCount();

			PMString  ActiveBookName;
			bool16 result = Utils<IBookUtils>()->GetActiveBookName(ActiveBookName);
	
			
			int32 strLength = ActiveBookName.WCharLength();
			gActiveBookName = ActiveBookName;
			gActiveBookName.Remove(strLength-5,5);
				
			for (int32 i = 0 ; i < bookCount ; i++) 
			{
				IBook* book = bookManager->GetNthBook(i);
				if (book == nil) 
				{	//go onto the next book...
					continue;
				}				
				PMString BookTitalName = book->GetBookTitleName();

				if(BookTitalName == ActiveBookName)
				{	
					/*List::*/lstBoxData.clear();
					this->dataList.clear();
					InterfacePtr<IBookContentMgr> bookContentMgr(book, UseDefaultIID());
					if (bookContentMgr == nil) 
					{
						//CA("This book doesn't have a book content manager!  Something is wrong.");
						break;
					}
					//BookContentDocInfoVector* BookContentinfovector = NULL;	
					BookContentDocInfoVector* BookContentinfovector = this->GetBookContentDocInfo(bookContentMgr);
					if(BookContentinfovector== nil)
						break;

					if(BookContentinfovector->size()<=0)
					{
						//CA(" BookContentinfovector->size()<=0 ");
						break;
					}

					BookContentDocInfoVector::iterator it1;
					for(it1 = BookContentinfovector->begin(); it1 != BookContentinfovector->end(); it1++)
					{
						ListBoxData listData ;
						listData.isBook = 2;
						listData.isChecked=kTrue;
						listData.fileName= it1->DocFile ;
						/*List::*/lstBoxData.push_back(listData);
						this->dataList.push_back(listData);
						//CA("fil");
					}
					if(BookContentinfovector)//--------
					{
						BookContentinfovector->clear();
						delete BookContentinfovector;
					}

				}
			}
			
			InterfacePtr<IDialogController>dlgControler(this,/*UseDefaultIID()*/IID_IDIALOGCONTROLLER);
			if(!dlgControler)
			{
				//CA("DlgControler is null");
				return;
			}
            
            PMString attrName;
            attrName=dlgControler->GetTextControlData(kPUPEventListComboBoxWidgetID);
            
            InterfacePtr<IStringListControlData>strEventListCtrlData(EventListControlView,UseDefaultIID());
            if(!strEventListCtrlData)
            {
                //CA("Strin list control data is null");
                break;
            }
            int32 eventIndex=strEventListCtrlData->GetIndex(attrName);
            if(eventIndex == 0)
            {
                //CA("Please select the Attribute");
                GlobalData::eventId = -1;
            }
            
            InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
            if(ptrIAppFramework == nil)
                return;
            
            if(eventIndex > 0)
            {
                VectorPubModelPtr vectorEventListptr=  ptrIAppFramework->ProjectCache_getAllProjects(1); // 1 default for English
                if(vectorEventListptr==nil)
                {
                    ptrIAppFramework->LogError("AP46_PriceUpdate::PUPDialogController::InitializeDialogFields::ClassificationTree_getRoot's vectorEventListptr==nil");
                    return;
                }
            
                VectorPubModel::iterator it1;
                int32 ct = 1;
                for(it1=vectorEventListptr->begin(); it1!=vectorEventListptr->end(); it1++, ct++)
                {
                    if(it1->getPubTypeId() != -1 && ct == eventIndex)
                    {
                        GlobalData::eventId = it1->getEventId();
                        //CA(it1->getName());
                        break;
                    }
                }
            }


			if(IsChangePriceModeSelected == kFalse)
			{					
				PMString attrName;
				attrName=dlgControler->GetTextControlData(kPUPAttributeComboBoxWidgetID);

				InterfacePtr<IStringListControlData>strListCtrlData(PriceComboControlView,UseDefaultIID());
				if(!strListCtrlData)
				{
					//CA("Strin list control data is null");
					break;
				}
				int32 index=strListCtrlData->GetIndex(attrName);
				if(index == 0)
				{
					//CA("Please select the Attribute");
                    
					break;
				}
			
				/*PMString as("Index is...");
				as.AppendNumber(index);
				CA(as);*/

				AttributeData attrdata;
				AttrData attr1 = attrdata.getAttrVectorInfo(index);
				double temp = attr1.getPubId();
                
                if(temp == -1)
                {
                    //CA("Please select the Attribute");
                    break;
                }
				GlobalData::SelectedAttributeId = temp;
				GlobalData::Table_col = attr1.getTable_col();
				GlobalData::attributeID = attr1.getAttribute_ID();
				GlobalData::oldAttributeName = attr1.getPubName();
//CA_NUM("xxxGlobalData::attributeID : ",GlobalData::attributeID);

			
				/*PMString qw("GlobalData::SelectedAttributeId:::::::");
				qw.AppendNumber(temp);
				CA(qw);*/
									
				if(/*List::*/lstBoxData.size()>0)
				{
					if(this->IsAnyDocSelected())
					{
						ac.CloseMainDialog();
						GlobalData::isUpdateStart=kTrue;
						//ac.OpenProcessDialog();
						if(GlobalData::isUpdateStart)
						{
							this->StartUpdate();
							//CA("after StartUpdate");
							GlobalData::isUpdateStart=kFalse;										
							//ac.CloseProcessDialog();									
							//CA("Going to open summary dialog");
							ac.OpenSummaryDialog();	
							//CA("After summary dialog");
						}
					}
					else
					{
						CA("No Document in Book");
					}
				}
				else
				{
					CAlert::InformationAlert("No document in Book");
				}
				break;											
						
			}
			else if(IsChangePriceModeSelected == kTrue)
			{
				PMString OldattrName;
				OldattrName=dlgControler->GetTextControlData(kPUPOldAttributeComboBoxWidgetID);
				/*InterfacePtr<IControlView>ctrlView(panelData->FindWidget(kPUPOldAttributeComboBoxWidgetID));
				if(!ctrlView)
				{
					CA("Ctrl View Is null");
					break;
				}*/
			
				//ctrlView->AddRef();
				InterfacePtr<IStringListControlData>strListCtrlData(OldPriceComboView,UseDefaultIID());
				if(!strListCtrlData)
				{
					//CA("Strin list control data is null");
					break;
				}
				int32 Oldindex=strListCtrlData->GetIndex(OldattrName);
				if(Oldindex == 0)
				{
					//CA("Please select the Old Attribute");
					break;
				}
			
				/*PMString as("Index is...");
				as.AppendNumber(index);
				CA(as);*/

				AttributeData attrdata;
				AttrData attr1 = attrdata.getAttrVectorInfo(Oldindex);
				double temp = attr1.getPubId();
                if(temp == -1)
                {
                    //CA("Please select the Attribute");
                    break;
                }
				GlobalData::SelectedOldAttrID = temp;
				GlobalData::OldTable_col = attr1.getTable_col();
				GlobalData::oldAttributeName = attr1.getPubName();
				GlobalData::attributeID = attr1.getAttribute_ID();
//CA_NUM("xxxGlobalData::SelectedOldAttrID : ",GlobalData::SelectedOldAttrID);
				PMString NewattrName;
				NewattrName=dlgControler->GetTextControlData(kPUPNewAttributeComboBoxWidgetID);
				/*InterfacePtr<IControlView>ctrlView1(panelData->FindWidget(kPUPNewAttributeComboBoxWidgetID));
				if(!ctrlView1)
				{
					CA("Ctrl View Is null");
					break;
				}*/
			
				//ctrlView->AddRef();
				InterfacePtr<IStringListControlData>strListCtrlData1(NewPriceComboView,UseDefaultIID());
				if(!strListCtrlData1)
				{
					//CA("Strin list control data is null");
					break;
				}
				int32 Newindex=strListCtrlData1->GetIndex(NewattrName);
				if(Newindex == 0)
				{
					//CA("Please select the New Attribute");
					break;
				}
			
				/*PMString as("Index is...");
				as.AppendNumber(index);
				CA(as);*/

				AttributeData attrdata1;
				AttrData attr2 = attrdata1.getAttrVectorInfo(Newindex);
				double temp1 = attr2.getPubId();
                if(temp1 == -1)
                {
                    //CA("Please select the Attribute");
                    break;
                }
				GlobalData::SelectedNewAttrID = temp1;
				GlobalData::NewTable_col = attr2.getTable_col();
				GlobalData::NewAttributeName = attr2.getPubName();
			
				if(/*List::*/lstBoxData.size()>0)
				{
					if(this->IsAnyDocSelected())
					{
						ac.CloseMainDialog();
						GlobalData::isUpdateStart=kTrue;
						//ac.OpenProcessDialog();
						if(GlobalData::isUpdateStart)
						{
							this->StartUpdate();
							//CA("after StartUpdate");
							GlobalData::isUpdateStart=kFalse;										
							//ac.CloseProcessDialog();									
							ac.OpenSummaryDialog();										
						}
					}
					else
					{
						CA("No Document in Book");
					}
				}
				else
				{
					CA("No document in Book");
				}
				break;
			}			
		}
//CA("4");
	} while (kFalse);
}

void PUPDialogObserver::UpdateParentList(const UIDRef& tableUIDRef)
{
	int32 i;

	//CA("PUPDialogObserver::UpdateParentList");

	InterfacePtr<ITagReader> itagReader
	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
		return;

	//CA("111");

	TagList tList=itagReader->getFrameTags(tableUIDRef);

	if(tList.size()==0)
	{
		//CA("11111111");
		return;
	}
	//CA("222");

	TagStruct tStruct=tList[0];

	//CA("333");

	/*PMString as;
	as.AppendNumber(tStruct.parentId);
	CA(as);*/

	if(tStruct.parentId!=-1)
	{
		for(i=0;i<GlobalData::ProductList.size();i++)
		{
			if(GlobalData::ProductList[i]==tStruct.parentId)
				break;
		}
		if(i==GlobalData::ProductList.size())
			GlobalData::ProductList.push_back(tStruct.parentId);
	}
//---------------
	for(int32 tagIndex = 0 ; tagIndex < tList.size(); tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}
}

void PUPDialogObserver::UpdateTotalItemList(TagStruct tStruct)
{
	int32 i;
	
	/*PMString as;
	as.AppendNumber(tStruct.parentId);
	CA(as);*/
	if(tStruct.childTag == 1){
		for(i=0;i<GlobalData::TotalItemList.size();i++)
		{
			if(GlobalData::TotalItemList[i]==tStruct.childId)
				break;
		}
		if(i==GlobalData::TotalItemList.size())
			GlobalData::TotalItemList.push_back(tStruct.childId);
	}
	else if(tStruct.typeId != -1)
	{
		for(i=0;i<GlobalData::TotalItemList.size();i++)
		{
			if(GlobalData::TotalItemList[i]==tStruct.typeId)
				break;
		}
		if(i==GlobalData::TotalItemList.size())
			GlobalData::TotalItemList.push_back(tStruct.typeId);
	}
}

PMString PUPDialogObserver::GetCellText(InterfacePtr<ITableModel>& tableModel,int32 startIndex,int32 endIndex)
{
	PMString result("");

	InterfacePtr<ITableTextContent> textContent(tableModel,UseDefaultIID());
	if(!textContent)
	{
		//CA("textContent=nil");
		return NULL;
	}
	InterfacePtr<ITextModel> textmodel(textContent->QueryTextModel()/*,UseDefaultIID()*/);
	if(!textmodel)
	{
		//CA("textmodel=nil");
		return NULL;
	}

	TextIterator begin(textmodel, startIndex);
	TextIterator end(textmodel, endIndex);

	int32 j=0,start=0;
	for (TextIterator iter = begin; iter <= end-1; iter++)
	{
		const textchar characterCode = (*iter).GetValue();
		char buf;

		::CTUnicodeTranslator::Instance()->TextCharToChar(&characterCode, 1, &buf, 1); 
		result.Append(buf);

	}

	return result;
}
/**
commented bcoz presently this function is not using 

*/

//void PUPDialogObserver::UpdateTabbedData(const UIDRef& tableUIDRef,PMString FileName,int32 pageNo)
//{
//	bool8 Flag=kFalse;
//
//	InterfacePtr<ITagWriter> tWrite
//	((static_cast<ITagWriter*> (CreateObject(kTGWTagWriterBoss,IID_ITAGWRITER))));
//	if(!tWrite)
//		return;
//
////	Flag = tWrite->IsAttrInTextSlug(tableUIDRef,GlobalData::SelectedAttributeId);
//	if(Flag==kTrue)
//	{
//		
//		VectorSKUPtr tempsku = nil;
//
//		tempsku = tWrite->UpdateDataInTextSlug(tableUIDRef,GlobalData::SelectedAttributeId,GlobalData::Table_col);
//
//		if(tempsku)
//		{
//			VectorSKUValue::iterator it;
//
//			bool8 Flag1=kFalse;
//
//			for(it=tempsku->begin();it!=tempsku->end();it++)
//			{
//				SKUData tempdata;
//				if(Flag1==kFalse)
//				{
//					UpdateParentList(tableUIDRef);
//					Flag1=kTrue;
//				}
//
//				metadata::Clock a;
//				metadata::DateTime dt;
//				a.timestamp(dt);
//
//				PMString Date;
//				Date.AppendNumber(dt.mday);
//				Date.Append("-");
//				Date.AppendNumber(dt.month);
//				Date.Append("-");
//				Date.AppendNumber(dt.year);
//
//				tempdata.OriginalData.Append("");
//				tempdata.ChangedData.Append(*it);
//				tempdata.DocPath.Append(FileName);
//				tempdata.PageNo = pageNo;
//				tempdata.Date.Append(Date);
//				
//				GlobalData::skuDataList.push_back(tempdata);
//			}
//
//
//		}
//	}
//
//}

void PUPDialogObserver::UpdateTabbedDataWithXML(const UIDRef& BoxUIDRef,PMString FileName,int32 pageNo)
{	
	/* We have to handle following conditions.
	   condition 1 : ItemTableInTabbedTextFormat
	   condition 2 : CustomItemTableInTabbedTextFormat..
				     Tabbed Text with item and non item copy attributes.
	   condition 3 : CustomItemTableInTable


	 Lets analyze each one by one
	 Condition 1: ItemTableInTabedTextFormat
	 The TagStruct attached to the frame is as below
	 [PRINTsource]__
				    |
				    |
				 [Items]
				 (ID = "-101" ,tableFlag = "-11",whichTab = "3")
					     |
					     |
						  ----[ItemCopyHeader]
						 |	 (typeID = "-2",tableFlag = "-12",whichTab = "3")
					     |
						  ----[ItemCopyValue]
							 (tableFlag = "-12",whichTab = "3")

	Condition 2: CustomItemTableInTabbedTextFormat
	The TagStruct attached to the frame is as below
	[PRINTsource]__
				   |
				   |
				    ---[ItemCopyHeader]
				   |  (typeID = "-2" ,tableFlag = "-12",whichTab = "4")
				   |
				    ---[ItemCopyValue]
					  (tableFlag = "-12",whichTab = "4")
	  Condition 3: CustomItemTableInTable
	  The tagStructure attached to the frame is as below
	  [PRINTsource]__
				     |
				     |
				  [PSTable0]
				 (ID = "-102" ,tableFlag = "1",whichTab = "3",typeId = "-3")
					     |
					     |
						  ----[ItemCopyHeader]
						 |	 (typeID = "-2",tableFlag = "1",whichTab = "4")
					     |
						  ----[ItemCopyValue]
							 (tableFlag = "1",whichTab = "4")


	  So in first and third condition , the TagStructure is of 3 level and in the second contdition
	  the TagStructure is of 2 level.
	  */

	//CA("Inside UpdateTabbedDataWithXML");
	TagList tList;

	InterfacePtr<ITagReader> itagReader
	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
		return ;
	//CA("1");
	tList=itagReader->getTagsFromBox(BoxUIDRef);
	if(tList.size()==0)
	{	
		tList=itagReader->getFrameTags(BoxUIDRef);
		if(tList.size()==0)
			return ;		
	}

	/*PMString temp="";
	temp.AppendNumber(tList.size());
	CA("tList.size() = " + temp);*/

	
	InterfacePtr<IPMUnknown> unknown(BoxUIDRef, IID_IUNKNOWN);
	if(!unknown){
		//CA("unknown");
	}
	/*UID textFrameUID =  Utils<IFrameUtils>()->GetTextFrameUID(unknown);
	if (textFrameUID == kInvalidUID)
	{
		CA("textFrameUID == kInvalidUID");
		return;
	}*/
	//CS3 Change
	/*InterfacePtr<ITextFrame> textFrame(BoxUIDRef.GetDataBase(), textFrameUID, ITextFrameColumn::kDefaultIID);
	if (textFrame == nil)
		return;*/

	
	//Added by Amit
	InterfacePtr<IHierarchy> graphicFrameHierarchy(BoxUIDRef, UseDefaultIID());
	if (graphicFrameHierarchy == nil) 
	{
		//CA("graphicFrameHierarchy  == nil");
		return;
	}
					
	InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
	if (!multiColumnItemHierarchy) {
		//CA("multiColumnItemHierarchy  == nil");
		return;
	}

	InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
	if (!multiColumnItemTextFrame) {
		//CA("multiColumnItemTextFrame  == nil");
		//CA("Its Not MultiColumn");
		return;
	}
	InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
	if (!frameItemHierarchy) {
		//CA("frameItemHierarchy  == nil");
		return;
	}

	InterfacePtr<ITextFrameColumn>textFrame(frameItemHierarchy, UseDefaultIID());
	if (!textFrame) {
		//CA("!!!ITextFrameColumn");
		return;
	}

	//To this


	/*UID result = kInvalidUID;
	
	InterfacePtr<IGraphicFrameData> graphicFrameData(BoxUIDRef, UseDefaultIID());
	if (!graphicFrameData) {CA("!graphicFrameData");
		return;
	}
	result = graphicFrameData->GetTextContentUID();

	InterfacePtr<IMultiColumnTextFrame> mcf(BoxUIDRef.GetDataBase(), result, UseDefaultIID());
	if (!mcf) {
		CA("!mcf");
		return;
	}
	CA("mcf");*/

	TextIndex startIndex = textFrame->TextStart();
	TextIndex finishIndex = startIndex + textFrame->TextSpan()-1;

	InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());

	/*TextIndex startIndex = mcf->TextStart();
	TextIndex finishIndex = startIndex + mcf->TextSpan()-1;

	InterfacePtr<ITextModel> textModel(mcf->QueryTextModel());*/
		if (textModel == nil)
			return;
		UIDRef txtMdlUIDRef =::GetUIDRef(textModel);

		InterfacePtr<ITextFocusManager> textFocusManager(textModel, UseDefaultIID());
		if (textFocusManager == nil)
			return;
		InterfacePtr<ITextFocus> frameTextFocus(textFocusManager->NewFocus(RangeData(startIndex, finishIndex, RangeData::kLeanForward)));
		if (frameTextFocus == nil)
			return;
	

		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
			return;	

		double currentTypeID =-1;
		for(int32 i=0; i<tList.size(); i++)
		{
						
			//Condition 1
			//ItemTableInTabbedTextFormat
			PMString strID = tList[i].tagPtr->GetAttributeValue(WideString("ID"));
			PMString strTableFlag = tList[i].tagPtr->GetAttributeValue(WideString("tableFlag"));
			/*
			PMString tagData;
			tagData.Append("ID : ");
			tagData.Append(strID);
			tagData.Append("\n");
			tagData.Append("whichTab : ");
			tagData.AppendNumber(tList[i].whichTab);
			tagData.Append("\n");
			tagData.Append("tableFlag : ");
			tagData.Append(strTableFlag);
			tagData.Append("\n");
			tagData.Append("-----------------\n");
			tagData.Append("GlobalData ::SelectedNewAttrID : ");
			tagData.AppendNumber(GlobalData ::SelectedNewAttrID);
			tagData.Append("\n-----------------\n");
			tagData.Append("GlobalData ::SelectedAttributeId : ");
			tagData.AppendNumber(GlobalData ::SelectedAttributeId);
			tagData.Append("\n-----------------\n");
			tagData.Append("GlobalData ::SelectedOldAttrID : ");
			tagData.AppendNumber(GlobalData ::SelectedOldAttrID);
			CA(tagData)
			*/


			//if((tList[i].whichTab == 3 || tList[i].whichTab == 4)&& strID == "-102" && tList[i].typeId == -3)
			if((tList[i].whichTab == 3 || tList[i].whichTab == 4)&& strID == "-102" && tList[i].tableType == 2)
			{//customItemTableInsideTable
				InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
				if(tableList==nil)
				{
					//CA("tableList==nil");
					continue;
				}
				UpdateTabelDataWithXML(tableList,BoxUIDRef,FileName,pageNo);
			}		
			else if((tList[i].whichTab == 3 || tList[i].whichTab == 4) && tList[i].childTag == 1)//else if((tList[i].whichTab == 3 || tList[i].whichTab == 4) && strID == "-101")
			{//start if..1
			//ItemTableInTabbedTextFormat
			//Its a "Items" tag.
			//Get all the child tag of "Items" tag.
			IIDXMLElement * itemsTagXMLElementPtr = tList[i].tagPtr;
			if(itemsTagXMLElementPtr == nil)
			{					
					//CA("itemsTagXMLElementPtr == nil");
					continue;
			}	
			int32 childTagCount = itemsTagXMLElementPtr->GetChildCount();
			for(int32 childTagIndex = 0;childTagIndex < childTagCount;childTagIndex++)
			{
				XMLReference childTagRef = itemsTagXMLElementPtr->GetNthChild(childTagIndex);
				//IIDXMLElement * childTagXMLElementPtr = childTagRef.Instantiate();
				InterfacePtr<IIDXMLElement>childTagXMLElementPtr(childTagRef.Instantiate());
				if(childTagXMLElementPtr == nil)
					continue;
				
				
				PMString displayTagData;
				PMString strID = childTagXMLElementPtr->GetAttributeValue(WideString("ID")); //Cs4 .....AAAAAAAALLLL
				double ID = strID.GetAsDouble();
				displayTagData.Append("elementID : " + strID + "\n");				
				
				PMString strTypeID = childTagXMLElementPtr->GetAttributeValue(WideString("typeId"));
				double typeID = strTypeID.GetAsDouble();				
				displayTagData.Append("typeID : " + strTypeID + "\n");						
				
				PMString strIndex = childTagXMLElementPtr->GetAttributeValue(WideString("index"));
				int32 index = strIndex.GetAsNumber();
				displayTagData.Append("index : " + strIndex + "\n");		

				PMString strImgFlag = childTagXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
				int32 imgFlag = strImgFlag.GetAsNumber();
				 displayTagData.Append("imgFlag : " + strImgFlag + "\n");						

				PMString strParentID = childTagXMLElementPtr->GetAttributeValue(WideString("parentID"));
				double parentID = strParentID.GetAsDouble();
				displayTagData.Append("parentID : " + strParentID + "\n");
						
				PMString strParentTypeID = childTagXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
				double parentTypeID = strParentTypeID.GetAsDouble();
				displayTagData.Append("parentTypeID : " + strParentTypeID+ "\n");
						
				PMString strSectionID = childTagXMLElementPtr->GetAttributeValue(WideString("sectionID"));
				double sectionID = strSectionID.GetAsDouble();
				displayTagData.Append("sectionID : " + strSectionID+ "\n");
						
				PMString strTableFlag = childTagXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
				int32 tableFlag = strTableFlag.GetAsNumber();
				displayTagData.Append("tableFlag : " + strTableFlag+ "\n");

				PMString strLanguageID = childTagXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
				double languageID = strLanguageID.GetAsDouble();
				displayTagData.Append("languageID : " + strLanguageID+ "\n");

				PMString strIsAutoResize = childTagXMLElementPtr->GetAttributeValue(WideString("isAutoResize"));
				int32 isAutoResize = strIsAutoResize.GetAsNumber();
				displayTagData.Append("isAutoResize : " + strIsAutoResize+ "\n");

				PMString strchildTag = childTagXMLElementPtr->GetAttributeValue(WideString("childTag"));
				int32 childTag = strchildTag.GetAsDouble();
				displayTagData.Append("childTag : " + strchildTag+ "\n");
				
				PMString strchildId = childTagXMLElementPtr->GetAttributeValue(WideString("childId"));
				double childId = strchildId.GetAsDouble();
				displayTagData.Append("childId : " + strchildId+ "\n");
				
				PMString strisEventField = childTagXMLElementPtr->GetAttributeValue(WideString("isEventField"));
				int32 isEventField = strisEventField.GetAsNumber();
				displayTagData.Append("isEventField : " + strIsAutoResize+ "\n");

				PMString strheader = childTagXMLElementPtr->GetAttributeValue(WideString("header"));
				int32 header = strheader.GetAsNumber();
				displayTagData.Append("header : " + strheader+ "\n");

				PMString strtableId = childTagXMLElementPtr->GetAttributeValue(WideString("tableId"));
				double tableId = strtableId.GetAsDouble();
				displayTagData.Append("tableId : " + strtableId+ "\n");

				PMString strtableType = childTagXMLElementPtr->GetAttributeValue(WideString("tableType"));
				double tableType = strtableType.GetAsDouble();
				displayTagData.Append("tableType : " + strtableType+ "\n");

				TagStruct tagInfo;
				tagInfo.elementId = ID;
				tagInfo.tagPtr = childTagXMLElementPtr;
				tagInfo.typeId = typeID;
				tagInfo.languageID = languageID;
				tagInfo.childTag = childTag;
				tagInfo.childId = childId;
				tagInfo.isEventField = isEventField;
				tagInfo.header = header;
				tagInfo.tableId = tableId;
				tagInfo.tableType = tableType;
				tagInfo.sectionID = sectionID;
						
				if(tagInfo.header == 1)//if(typeID == -2)
				{//if typeID == -2
//CA("header");							
					if(IsChangePriceModeSelected == kFalse)
					{//if(IsChangePriceModeSelected == kFalse)
												//CA(tList[i].tagPtr->GetTagString());
												//if(tList[i].id != GlobalData::Table_col) //commented bcoz have to use attribute ID
						if(ID != GlobalData::attributeID)
							continue;													
						if(!itagReader->GetUpdatedTag(tagInfo))
							return ;
						refreshTheItemHeader(textModel,currentTypeID,FileName,pageNo,tagInfo);

					}//end if(IsChangePriceModeSelected == kFalse) 
					else if(IsChangePriceModeSelected == kTrue)
					{//else if (IsChangePriceModeSelected == kTrue)					
						if(ID != GlobalData::SelectedOldAttrID)
							continue;											
						if(!itagReader->GetUpdatedTag(tagInfo))
							return;
						replaceTheOldItemHeaderWithNewItemHeader(textModel,currentTypeID,FileName,pageNo,tagInfo);

					}//end else if (IsChangePriceModeSelected == kTrue)
				}//end if typeID == -2
				else
				{//start else
					
					/////////////////////////////////////	Added to show total iems present in catlog					
					bool16 itemIdFound = kFalse;
					if(childTag == 1){
						for(int32 i = 0 ; i < GlobalData::TotalItemPresentList.size() ; i++)
						{
							if(GlobalData::TotalItemPresentList[i] == childId)
								itemIdFound = kTrue;
						}
						if(itemIdFound == kFalse)
							GlobalData::TotalItemPresentList.push_back(childId); 
					}
					else{
						for(int32 i = 0 ; i < GlobalData::TotalItemPresentList.size() ; i++)
						{
							if(GlobalData::TotalItemPresentList[i] == typeID)
								itemIdFound = kTrue;
						}
						if(itemIdFound == kFalse)
							GlobalData::TotalItemPresentList.push_back(typeID); 
					}
					//////////////////////////////////////

					if(IsChangePriceModeSelected == kFalse)
					{
					//CA(tList[i].COLName);
						UpdateParentList(BoxUIDRef);

					//if(tList[i].id != GlobalData::Table_col) //commented bcoz attribute id funda comes into picture @vaibhav
						if(ID != GlobalData::attributeID)
							continue;
						if(!itagReader->GetUpdatedTag(tagInfo))
							return;
						
						refreshTheItemAttribute(textModel,currentTypeID,FileName,pageNo,tagInfo);
					
					}
					else if(IsChangePriceModeSelected == kTrue)
					{				
					//CA(tList[i].tagPtr->GetTagString());
						UpdateParentList(BoxUIDRef);

					//if(tList[i].id != GlobalData::OldTable_col) // commented @Vaibhav
						if(ID != GlobalData::SelectedOldAttrID)
							continue;
						if(!itagReader->GetUpdatedTag(tagInfo))
							return;										
						replaceOldTagWithNewTag(textModel,currentTypeID,FileName,pageNo,tagInfo);						
					}
				}//end else
			}//end for
		}//end if..1
		//Condition 2
		//CustomItemTableInTabbedTextFormat
		else if(tList[i].whichTab == 4 &&  (strTableFlag == "-12" || strTableFlag == "-1001"))
		{//ItemCopyAttributes
	
			if(tList[i].header == 1)//if(tList[i].typeId == -2)
			{
				if(IsChangePriceModeSelected == kFalse)
				{
					if(tList[i].elementId != GlobalData::attributeID)
						continue;
					if(!itagReader->GetUpdatedTag(tList[i]))
					return;
					refreshTheItemHeader(textModel,currentTypeID,FileName,pageNo,tList[i]);
	
				}
				else if(IsChangePriceModeSelected == kTrue)
				{
					if(tList[i].elementId != GlobalData::SelectedOldAttrID)
						continue;
					if(!itagReader->GetUpdatedTag(tList[i]))
						return;
					replaceTheOldItemHeaderWithNewItemHeader(textModel,currentTypeID,FileName,pageNo,tList[i]);
				
				}
			}	
			else
			{
			
				/////////////////////////////////////	Added to show total iems present in catlog					
				bool16 itemIdFound = kFalse;
				if(tList[i].childTag == 1){
					for(int32 k = 0 ; k < GlobalData::TotalItemPresentList.size() ; k++)
					{
						if(GlobalData::TotalItemPresentList[k] == tList[i].childId)
							itemIdFound = kTrue;
					}
					if(itemIdFound == kFalse)
						GlobalData::TotalItemPresentList.push_back(tList[i].childId); 
				}
				else{
					for(int32 k = 0 ; k < GlobalData::TotalItemPresentList.size() ; k++)
					{
						if(GlobalData::TotalItemPresentList[k] == tList[i].typeId)
							itemIdFound = kTrue;
					}
					if(itemIdFound == kFalse)
						GlobalData::TotalItemPresentList.push_back(tList[i].typeId); 
				}
				//////////////////////////////////////

				if(IsChangePriceModeSelected == kFalse)
				{
					UpdateParentList(BoxUIDRef);
					//if(tList[i].id != GlobalData::Table_col) //commented bcoz attribute id funda comes into picture @vaibhav
					if(tList[i].elementId != GlobalData::attributeID)
						continue;					
					if(!itagReader->GetUpdatedTag(tList[i]))
						return;
					
					refreshTheItemAttribute(textModel,currentTypeID,FileName,pageNo,tList[i]);
	
				}
				else if(IsChangePriceModeSelected == kTrue)
				{					
					UpdateParentList(BoxUIDRef);
					if(tList[i].elementId != GlobalData::SelectedOldAttrID)
						continue;
					if(!itagReader->GetUpdatedTag(tList[i]))
						return;
					replaceOldTagWithNewTag(textModel,currentTypeID,FileName,pageNo,tList[i]);                 
				}
			}


		}
	}
//CA("3");

	//------------------
	for(int32 tagIndex = 0 ; tagIndex < tList.size(); tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}	

}




/**
commented bcoz presently not using
@vaibhav 8 Feb
*/

//void PUPDialogObserver::UpdateTabelData(InterfacePtr<ITableModel>& tableModel,const UIDRef& TextUIDRef,PMString FileName,int32 pageNo)
//{	//CA("Inside UpdateTabelData");
//			
//	do{
//			UIDRef tableUIDRef(::GetUIDRef(tableModel));	
//
//			ColRange col = tableModel->GetTotalCols();
//			RowRange row = tableModel->GetTotalRows();
//
//			int32 rows = row.count;
//			int32 cols = col.count;
//
//			InterfacePtr<ISelectionManager> iSelectionManager(Utils<ISelectionUtils>()->QueryActiveSelection());
//			if(!iSelectionManager)
//			{
//				CA("Source Selection Manager is null");
//				return;
//			}
////CA("2");
//			InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
//			if(!pTextSel)
//			{
//				CA("Source IConcrete Selection is null");
//				return;
//			}
////CA("Got Concrete Selection");
//
//			InterfacePtr<ITableTextSelection>tblTxtSel(pTextSel,UseDefaultIID());
//			if(!tblTxtSel)
//			{
//				CA(" Source Table Text Selection is null");
//				return;
//			}
//
//				InterfacePtr<ITagWriter> tWrite
//				((static_cast<ITagWriter*> (CreateObject(kTGWTagWriterBoss,IID_ITAGWRITER))));
//				if(!tWrite)
//					return;
//				
//				bool8 amit=kFalse;
//				int j,i;
//
//				bool8 TransposeFlag=kFalse;
//				int32 Attr_id;
//				int32 Item_id;					
////CA("3");
////				tWrite->NewSlugreader(tableModel,1,0,Attr_id,Item_id,tblTxtSel); //presently not using  @vaibhav
//				if(Item_id==-1)
//				{
//					TransposeFlag=kTrue;
//					rows = col.count;
//					cols = row.count;
//				}
//				/*PMString QWE("cols : ");
//				QWE.AppendNumber(cols);
//				CA(QWE);*/
//					i=0;
//					for(j=0;j<cols;j++)
//					{						
//						if(j==0 && i==0)
//						{
//							if(TransposeFlag!=kTrue)
//							{								
//							//	tWrite->NewSlugreader(tableModel,i+1,j,Attr_id,Item_id,tblTxtSel);
//								Item_id=-1;
//							}
//							else
//							{	//CA("4");
//							//	tWrite->NewSlugreader(tableModel,j,i+1,Attr_id,Item_id,tblTxtSel);
//								Item_id=-1;
//								//CA("5");
//							}
//						}
//						else
//						{
//							if(TransposeFlag!=kTrue)
//							//	tWrite->NewSlugreader(tableModel,i,j,Attr_id,Item_id,tblTxtSel);
//							else{
//								//CA("6");
//					//			tWrite->NewSlugreader(tableModel,j,i,Attr_id,Item_id,tblTxtSel);
//								//CA("7");
//							}
//								
//						}
//						if(GlobalData::SelectedAttributeId==Attr_id)
//								break;
//
//					}
//					j--;
//					/*PMString ASD("Value of i : ");
//					ASD.AppendNumber(i);
//					ASD.Append("  Val of j : ");
//					ASD.AppendNumber(j);
//					ASD.Append(" rows : ");
//					ASD.AppendNumber(rows);
//					ASD.Append("  Cols : ");
//					ASD.AppendNumber(cols);
//					CA(ASD);*/
//				TagStruct tInfo; // new added @ vaibhav
//
//				if(i<rows && j<cols)
//				{
//	//CA("8");				
//					InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
//					if(ptrIAppFramework == nil)
//						return;
//	//CA("9");
//					InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
//					if(tableCommands==nil)
//					{
//							CA("Err: invalid interface pointer ITableCommands");
//							break;
//					}
//	//CA("10");
//					for(i=1;i<rows;i++)
//					{
//						int32 Attr_id,Item_id=0;
////CA("11");
//						if(TransposeFlag!=kTrue)
////							tWrite->NewSlugreader(tableModel,i,j,Attr_id,Item_id,tblTxtSel);
//						else
////							tWrite->NewSlugreader(tableModel,j,i,Attr_id,Item_id,tblTxtSel);
//
////CA("12");
//						int32 fc=tblTxtSel->GetIndexOfFirstCharInCell();
//						int32 lc=tblTxtSel->GetIndexOfLastCharInCell();
//CA("13");
//						PMString source = GetCellText(tableModel,fc,lc);
//						//CA(source);					
//						//PMString result;//temp_to_test  = ptrIAppFramework->findProductCore(Item_id,GlobalData::Table_col.GrabCString());
//						
//					//	PMString result = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(Item_id,GlobalData::attributeID,tInfo.languageID);
//					//	CA(result);
//						if(result.IsEqual(source)==kFalse)
//						{
//							SKUData tempData;
//
//							metadata::Clock a;
//							metadata::DateTime dt;
//							a.timestamp(dt);
//
//							PMString Date;
//							Date.AppendNumber(dt.mday);
//							Date.Append("-");
//							Date.AppendNumber(dt.month);
//							Date.Append("-");
//							Date.AppendNumber(dt.year);
//						
//							tempData.OriginalData.Append(source);
//							tempData.ChangedData.Append(result);
//							tempData.DocPath.Append(FileName);
//							tempData.Date.Append(Date);
//							tempData.PageNo=pageNo;
//							
//							GlobalData::skuDataList.push_back(tempData);
//
//							UpdateParentList(TextUIDRef);
//							
//							WideString* wStr=new WideString(result.GrabCString());
//							if(TransposeFlag!=kTrue)
//								tableCommands->SetCellText(*wStr, GridAddress(i,j));
//							else{
//							//	CA("8");
//								tableCommands->SetCellText(*wStr, GridAddress(j,i));
//							//	CA("9");
//							}
//						}
//						
//					}
//				}
//
//
//}while(kFalse);
//}

void PUPDialogObserver::UpdateTabelDataWithXML(InterfacePtr<ITableModelList>& tableList,const UIDRef& TextUIDRef,PMString FileName,int32 pageNo)
{	
//CA("Inside PUPDialogObserver::UpdateTabelDataWithXML");
			
do{
	int32	tableIndex = tableList->GetModelCount() ;

	for(int32 p=0; p<tableIndex; p++)
	{
		InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(p));
		if(tableModel == nil) 
		{
			continue;
		}

		//start 17July
		InterfacePtr<ITableTextContent> tableTextContent(tableModel,UseDefaultIID());
		if(tableTextContent == nil)
			{
				//CA("tableTextContent == nil");
				continue;
			}
		InterfacePtr<ITextModel> tableTextModel(tableTextContent->QueryTextModel());
		if(tableTextModel == nil)
			{
				//CA("tableTextModel == nil");
				continue;
			}
		//end 17July

		UIDRef tableUIDRef(::GetUIDRef(tableModel));
		
		ColRange col = tableModel->GetTotalCols();
		RowRange row = tableModel->GetTotalRows();

		int32 rows = row.count;
		int32 cols = col.count;
		/*
		InterfacePtr<ISelectionManager> iSelectionManager(Utils<ISelectionUtils>()->QueryActiveSelection());
		if(!iSelectionManager)
		{
			CA("Source Selection Manager is null");
			return;
		}

		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		if(!pTextSel)
		{
			CA("Source IConcrete Selection is null");
			return;
		}

		InterfacePtr<ITableTextSelection>tblTxtSel(pTextSel,UseDefaultIID());
		if(!tblTxtSel)
		{
			CA(" Source Table Text Selection is null");
			return;
		}
		*/

		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
			return;

		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==nil)
		{
			//CA("Err: invalid interface pointer ITableCommands");
			break;
		}

		InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader)
			return ;

		InterfacePtr<ITableUtility> iTableUtlObj
		((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
		if(!iTableUtlObj){// CA("!iTableUtlObj");
			return ;
		}
		
		TagList tList=itagReader->getTagsFromBox(TextUIDRef);	
		IIDXMLElement* XMLElementPtr =  NULL; 
		TagStruct tStruct;
		if(tList.size()==0)
			return;

		for(int32 r=0; r<tList.size(); r++)
		{
			XMLElementPtr = tList[r].tagPtr;
			tStruct = tList[r];
			XMLContentReference contentRef = XMLElementPtr->GetContentReference();
			if(contentRef.IsTable()) {
				//CA("Table Content");
			} else if(contentRef.IsTableCell()) {
				//CA("Table Cell");
			} 

			UIDRef ContentRef = contentRef.GetUIDRef();
			if( ContentRef != tableUIDRef)
			{
				//CA("ContentRef != tableUIDRef");	
				continue;
			}
			else
			{
				break;
			}
		}
		/*if(tStruct.isTablePresent == kFalse)
		{
			CA("Table Tag not Found");
			return;
		}*/
			
		//UIDRef tableRef(::GetUIDRef(tableModel));
		//IIDXMLElement* XMLElementPtr =  tStruct.tagPtr; 		
		//PMString Maintag = XMLElementPtr->GetTagString();
		//CA(Maintag);
		//int MainelementCount=XMLElementPtr->GetChildCount();
		//if(MainelementCount > 0)
		//{
		//	XMLReference MainelementXMLref=XMLElementPtr->GetNthChild(0);
		//	IIDXMLElement * TableElement=MainelementXMLref.Instantiate();
		//	if(TableElement==nil)
		//		break;
		//	//CA("22");
		//	PMString TabletagName=TableElement->GetTagString();
		//	CA(TabletagName);	

		IIDXMLElement * TableElement = XMLElementPtr;
		XMLContentReference contentRef = TableElement->GetContentReference();
		if(contentRef.IsTable()) {
			//CA("Table Content");
		} else if(contentRef.IsTableCell()) {
			//CA("Table Cell");
		} 
		UIDRef ContentRef = contentRef.GetUIDRef();
		if( ContentRef != tableUIDRef)
		{
			//CA("ContentRef != tableUIDRef");	
			return;
		}
		
		IXMLReferenceData* xmlRefData = contentRef.Instantiate();
		XMLReference xmlRef=xmlRefData->GetReference();
		//IIDXMLElement *xmlElement=xmlRef.Instantiate();
		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
		int elementCount=xmlElement->GetChildCount();
		
		PMString ORgTagString = xmlElement->GetTagString();
		//CA("Content Tag");
		//CA(ORgTagString);		
		
		for(int i1=0; i1<elementCount; i1++)
		{
			//CA("21");		
			XMLReference elementXMLref=xmlElement->GetNthChild(i1);
			//IIDXMLElement * childElement=elementXMLref.Instantiate();
			InterfacePtr<IIDXMLElement>childElement(elementXMLref.Instantiate());
			if(childElement==nil)
				continue;
			//CA("22");
			PMString ChildtagName=childElement->GetTagString();
			//CA(ChildtagName);
			//if(tagName == ChildtagName){  //CA("tagName == ChildtagName");
			//	existingTagUID = childElement->GetTagUID();
			//	break;
			//}
			int elementCount1=childElement->GetChildCount();
			/*PMString ASD("elementCount1 for Cell : ");
			ASD.AppendNumber(elementCount1);
			CA(ASD);*/
			for(int i=0; i<elementCount1; i++)
			{
				XMLReference elementXMLref1=childElement->GetNthChild(i);
				//IIDXMLElement * childElement1=elementXMLref1.Instantiate();
				InterfacePtr<IIDXMLElement>childElement1(elementXMLref1.Instantiate());
				if(childElement1==nil)
					continue;
				//check for the tableFlag
				PMString strTableFlag =childElement1->GetAttributeValue(WideString("tableFlag"));//Cs4
				if(strTableFlag != "0" && strTableFlag != "-1001")
				{
					//CA("strTableFlag != 0 && strTableFlag != -1001");
					continue;
				}			
				PMString ChileEleName = childElement1->GetTagString();
				TagStruct tInfo;
				int32 attribCount=childElement1->GetAttributeCount();
				TextIndex sIndex=0, eIndex=0;
				Utils<IXMLUtils>()->GetElementIndices(childElement1, &sIndex, &eIndex);
				
				tInfo. startIndex=sIndex;
				tInfo.endIndex=eIndex;
				tInfo.tagPtr=childElement1;
				
				
				for(int j=0; j<attribCount; j++)
				{
					PMString attribName=childElement1->GetAttributeNameAt(j);
					PMString attribVal=childElement1->GetAttributeValue(WideString(attribName));//Cs4
					itagReader->getCorrespondingTagAttributes(attribName, attribVal, tInfo);
					
				}

				tInfo.curBoxUIDRef=TextUIDRef;

				double currentTypeID = -1;

				//mainPriceUpdate(tableTextModel,currentTypeID,FileName,pageNo,tInfo);
				
				if(tInfo.header == 1)//if(tInfo.typeId == -2)
				{
					//CA(GlobalData::Table_col);
					//CA(tInfo.COLName);
					if(IsChangePriceModeSelected == kFalse)
					{
						//if(tInfo.id != GlobalData::Table_col)  // commented @Vaibhav
						if(tInfo.elementId != GlobalData::attributeID)
							continue;
						refreshTheItemHeader(tableTextModel,currentTypeID,FileName,pageNo,tInfo);


					}
					else
					{
						//if(tInfo.id != GlobalData::OldTable_col) // commented @vaibhav
						if(tInfo.elementId != GlobalData::SelectedOldAttrID)
							continue;
						replaceTheOldItemHeaderWithNewItemHeader(tableTextModel,currentTypeID,FileName,pageNo,tInfo);

					}
				}
				//cell is not a header cell
				else
				{
					/////////////////////////////////////	Added to show total iems present in catlog					
					bool16 itemIdFound = kFalse;
					if(tInfo.childTag == 1){
						for(int32 i = 0 ; i < GlobalData::TotalItemPresentList.size() ; i++)
						{
							if(GlobalData::TotalItemPresentList[i] == tInfo.childId)
								itemIdFound = kTrue;
						}
						if(itemIdFound == kFalse)
							GlobalData::TotalItemPresentList.push_back(tInfo.childId); 
					}
					else{
						for(int32 i = 0 ; i < GlobalData::TotalItemPresentList.size() ; i++)
						{
							if(GlobalData::TotalItemPresentList[i] == tInfo.typeId)
								itemIdFound = kTrue;
						}
						if(itemIdFound == kFalse)
							GlobalData::TotalItemPresentList.push_back(tInfo.typeId); 
					}
					//////////////////////////////////////

					if(IsChangePriceModeSelected == kFalse)
					{
						
						UpdateParentList(TextUIDRef);
						//if(tInfo.id != GlobalData::Table_col) // commented @ Vaibhav
						if(tInfo.elementId != GlobalData::attributeID)
							continue;
						
						refreshTheItemAttribute(tableTextModel,currentTypeID,FileName,pageNo,tInfo);
						
					}
					else if(IsChangePriceModeSelected == kTrue)
					{
						
						UpdateParentList(TextUIDRef);

						//if(tInfo.id != GlobalData::OldTable_col) // commented @Vaibhav
						if(tInfo.elementId != GlobalData::SelectedOldAttrID)
							continue;
						replaceOldTagWithNewTag(tableTextModel,currentTypeID,FileName,pageNo,tInfo);
								
					}

				}

			}
		}
		//------------------
		for(int32 tagIndex = 0 ; tagIndex < tList.size(); tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
	}
 }while(kFalse);
}


void PUPDialogObserver::GentrateReport()
{
	
	for(int i=0;i<GlobalData::skuDataList.size();i++)
	{
		char timeStr [9];
		PMString as;
		as.AppendNumber(PMReal(GlobalData::skuDataList[i].ItemID));
		as.Append("\t");
		as.Append(GlobalData::skuDataList[i].baseNumber); //****Added 
		as.Append("\t");
		as.Append(GlobalData::skuDataList[i].attributeName/*GlobalData::skuDataList[i].ColumName*/);
		as.Append("\t");
		as.Append(GlobalData::skuDataList[i].OriginalData);
		as.Append("\t");
		as.Append(GlobalData::skuDataList[i].ChangedData);
		as.Append("\t");
		as.Append(GlobalData::skuDataList[i].DocPath);
		as.Append("\t");
		//as.AppendNumber(GlobalData::skuDataList[i].PageNo);
		as.Append(" ");
		as.Append("\t");
		as.Append(GlobalData::skuDataList[i].Date);
        #ifdef WINDOWS
            _strtime( timeStr );
            as.Append(" : ");
            as.Append(timeStr);//Cs4
        #endif
		
		//as.Append("\n");

		PMString ExpFilePath;
		//SDKUtilities::GetApplicationFolder(ExpFilePath);
        
        FileUtils::GetAppFolder(&ExpFilePath);
        #ifdef MACINTOSH
            // Result path = Macintosh HD:Applications:Adobe InDesign CC Debug:Adobe InDesign CC.app:Contents:MacOS
            // so removing last 3 folder names as actual name is till Macintosh HD:Applications:Adobe InDesign CC Debug
            SDKUtilities::RemoveLastElement(ExpFilePath);
            SDKUtilities::RemoveLastElement(ExpFilePath);
            SDKUtilities::RemoveLastElement(ExpFilePath);
        #endif
	
		PMString pluginFolderStr("");
		PMString tempString;
		// appending '\'
		#ifdef MACINTOSH
			pluginFolderStr.Append(":");
		#else
			pluginFolderStr.Append("\\");
		#endif
			// appending folder name
			pluginFolderStr.Append("Plug-ins");

			// appending '\'
		#ifdef MACINTOSH
			pluginFolderStr.Append(":");
		#else
			pluginFolderStr.Append("\\");
		#endif
			// appending file name  //**Commented By Sachin Sharma
			pluginFolderStr.Append(gActiveBookName);/*GlobalData::skuDataList[i].Date*/ //file name "Vinit.pp";

		ExpFilePath.Append(pluginFolderStr);
		//ExpFilePath.Append("_");
		//ExpFilePath.Append(GlobalData::skuDataList[i].Date);
		ExpFilePath.Append(".SKU.txt");
        
        #ifdef MACINTOSH
        {
            InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
            if(ptrIAppFramework == nil)
            {
                return;
            }
            
            ptrIAppFramework->getUnixPath(ExpFilePath);
        }
        #endif

		//CAlert::InformationAlert(ExpFilePath);
		string tempString2(ExpFilePath.GetPlatformString());
		const char* path=const_cast<char*>(tempString2.c_str())/**/;//Cs4
        
        
		//as+="\n";
		string tempString1(as.GetPlatformString());
		const char* data=const_cast<char*>(tempString1.c_str())/*GrabCString()*/; //Cs4
		//CA(data);
		//FILE* fp=NULL;
		ofstream outFile ;
		//char str[20];
		//CA(path);
		outFile.open( path , ios::app);
		if(!outFile)
			break;
		if( (i == 0))
		{
			//CA("We are going to create file");
			/*fp=std::fopen(path, "a");
			if(!fp)
				break;*/
			PMString asd("");
			asd.Append("\n");			
			asd.Append("Itemid");
			asd.Append("\t");
			asd.Append("Base Number");
			asd.Append("\t");
			asd.Append("Attribute Name");
			asd.Append("\t");
			asd.Append("Original Value");
			asd.Append("\t");
			asd.Append("Changed Value");
			asd.Append("\t");
			asd.Append("Document Path");
			asd.Append("\t");
			//asd.Append("PageNo or Seq No");
			asd.Append(" ");
			asd.Append("\t");
			asd.Append("Date"); //:Time
			asd.Append("\n");

			string tempString3(asd.GetPlatformString());
			const char* data1 = const_cast<char*>(tempString3.c_str())/*GrabCString()*/; //Cs4


			//std::fprintf(fp, "%s", data1);
			//std::fprintf(fp, "%s",data);
			outFile<<data1<<endl;
			outFile<<data<<endl;
			
		}
		else
		{
			/*std::fclose(fp);
			fp=std::fopen(path, "a");
			if(!fp)
				break;*/
			//std::fprintf(fp, "%s",data);
			outFile<<data<<endl;
			
		}
		//std::fwrite(data,strlen(data),1,fp);
		//std::fclose(fp);
		outFile.close();
	}
	//CA("12");
}

bool16 PUPDialogObserver::GetTextstoryFromBox(ITextModel* iModel, int32 startIndex, int32 finishIndex, PMString& story)
{	
	TextIterator begin(iModel, startIndex);
	TextIterator end(iModel, finishIndex);
	
	for (TextIterator iter = begin; iter <= end; iter++)
	{	
		const textchar characterCode = (*iter).GetValue();
		char buf;
		::CTUnicodeTranslator::Instance()->TextCharToChar(&characterCode, 1, &buf, 1); 
		story.Append(buf);
	}	
	return kTrue;
}
/* GetBookContentNames
*/
BookContentDocInfoVector* PUPDialogObserver::GetBookContentDocInfo(IBookContentMgr* bookContentMgr)
{
	bool16 flag=kFalse;
	BookContentDocInfoVector* BookContentDocInfovectorPtr = new BookContentDocInfoVector;
	BookContentDocInfovectorPtr->clear();
	do {
		if (bookContentMgr == nil) 
		{
			ASSERT(bookContentMgr);
			break;
		}

		// get the book's database (same for IBook)
		IDataBase* bookDB = ::GetDataBase(bookContentMgr);
		if (bookDB == nil) 
		{
			ASSERT_FAIL("bookDB is nil - wrong database?"); 
			break;
		}

		int32 contentCount = bookContentMgr->GetContentCount();
		for (int32 i = 0 ; i < contentCount ; i++) 
		{
			BookContentDocInfo contentDocInfo;
			UID contentUID = bookContentMgr->GetNthContent(i);
			if (contentUID == kInvalidUID) 
			{
				// somehow, we got a bad UID
				continue; // just goto the next one
			}
			// get the datalink that points to the book content
			InterfacePtr<IDataLink> bookLink(bookDB, contentUID, UseDefaultIID());
			if (bookLink == nil) 
			{
				ASSERT_FAIL(FORMAT_ARGS("IDataLink for book #%d is missing", i));
				break; // out of for loop
			}

			// get the book name and add it to the list
			PMString* baseName = bookLink->GetBaseName();
			//ASSERT(baseName && baseName->IsNull() == kFalse);
			
			IDFile CurrFile;
			bool16 IsMissingPluginFlag = kFalse;

			IDocument* CurrDoc = Utils<IBookUtils>()->FindDocFromContentUID
				(
					bookDB,
					contentUID,
					CurrFile,
					IsMissingPluginFlag
				);

			bool16 Flag1 = kFalse;
			Flag1 =  FileUtils::DoesFileExist(CurrFile);
			if(Flag1 == kFalse)
			{
				//CA("File dOES nOT Exists");
				continue;
			}
			else
			{				
				contentDocInfo.DocFile = CurrFile;	
				contentDocInfo.DocumentName = (*baseName);
				contentDocInfo.DocUID = contentUID;
				contentDocInfo.index = i;
				//contentDocInfo.documentPtr = CurrDoc;				
				BookContentDocInfovectorPtr->push_back(contentDocInfo);
				flag=kTrue;
			}			
		}

	} while (false);
	
	if(flag)
		return BookContentDocInfovectorPtr;
	else
	{
		delete BookContentDocInfovectorPtr;
		return NULL;
	}
}
bool16 PUPDialogObserver::doesExist(TagList &tagList,UIDList selectUIDList)
{
	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
		return kFalse;

	TagList tList;
	for(int i=0; i< selectUIDList.Length(); i++)
	{
		tList = itagReader->getTagsFromBox(selectUIDList.GetRef(i));

		if(tList.size()==0||tagList.size()==0 || !tList[0].tagPtr || !tagList[0].tagPtr )
			continue;

		if(tagList[0].tagPtr == tList[0].tagPtr )
		{
			//CA("return kTrue");
			return kTrue;
		}
	}
	//CA("return kFalse");
	return kFalse;
}

//  Code generated by DollyXS code generator




void PUPDialogObserver::UpdateDocumentJL(InterfacePtr<IDocument> bgDoc,PMString FileName)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
        return;
	}

	InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
	IDocument* doc = layoutData->GetDocument();
	IDataBase* docDatabase = ::GetDataBase((IPMUnknown*) doc);
	const IDFile* idFile = docDatabase->GetSysFile();

	IIDXMLElement* rootElem = Utils<IXMLUtils>()->QueryRootElement(docDatabase);

	int32 count1 = rootElem->GetChildCount();
	int32 badTagsCnt = 0;

	set<PMString> uniqueResultIds;

	do
	{
		PMString str11("Unused Frame Removal");
		str11.SetTranslatable(kFalse);
		PMString str12("Unused frame removal is in progress ...");
		str12.SetTranslatable(kFalse);
		RangeProgressBar progressBar(str11, 0, count1, kTrue);
		progressBar.SetTaskText(str12);
				
		for(int32 i = count1 - 1; i >= 0; --i)
		{
			XMLReference xmlRef = rootElem->GetNthChild(i);
			IIDXMLElement* childElem = xmlRef.Instantiate();

			const XMLContentReference& contentRef = childElem->GetContentReference();
			const UIDRef & uidref = contentRef.GetUIDRef();
			if(uidref  == UIDRef::gNull)
			{
				ErrorCode status = Utils<IXMLElementCommands>()->DeleteElement(xmlRef,kTrue);
				badTagsCnt++;
			}
			else
			{
				AddKeysWithoutSectionIds(childElem, uniqueResultIds);  // this will have to move into the new logic
			}

			childElem->Release();

			progressBar.SetPosition(count1 - i);
		}
	} while (kFalse);

	do
	{
		vector<IndexReference> indexReferences;

		PMString indexingDoc("Refreshing Book");
		PMString taskText("");
		indexingDoc.SetTranslatable(kFalse);

		int32 taskCount = rootElem->GetChildCount() + 2;

		RangeProgressBar progressBar2(indexingDoc, 0, taskCount, kTrue);
		taskText.Append("Retrieving updated values from server");
		taskText.SetTranslatable(kFalse);
		progressBar2.SetTaskText(taskText);

		/////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////
		PMString resultType("i");
		double oldFieldId = -1;
		double newFieldId = -1;


		if(IsChangePriceModeSelected == kFalse) // Price Refresh Mode
		{
			oldFieldId = GlobalData::SelectedAttributeId;
			newFieldId = GlobalData::SelectedAttributeId;
		}
		else if(IsChangePriceModeSelected == kTrue)  // Replace Fileds,  Field Swap Mode
		{
			oldFieldId = GlobalData::SelectedOldAttrID;
			newFieldId = GlobalData::SelectedNewAttrID;
		}


		PMString tempLangId("");
		tempLangId.AppendNumber(PMReal(GlobalData::languageId));
		WideString oldLanguageId(tempLangId);

		PMString tempOld("");
		tempOld.AppendNumber(PMReal(oldFieldId));
		WideString oldFieldIdString(tempOld);
		PMString tempNew("");
		tempNew.AppendNumber(PMReal(newFieldId));
		WideString newFieldIdString(tempNew);
					
		/////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////

		ptrIAppFramework->getFieldSwapValues(uniqueResultIds, indexReferences, oldLanguageId, resultType, oldFieldId, newFieldId, GlobalData::eventId);
		progressBar2.SetPosition(1);

		TotalTagsInDocument = count1;
		TotalUnusedTagsRemoved = badTagsCnt;

		taskText.Clear();
		taskText.Append("");
		taskText.SetTranslatable(kFalse);
		progressBar2.SetTaskText(taskText);
		//DeleteAllTopics(doc);
		progressBar2.SetPosition(2);

		taskText.Clear();
		taskText.Append("Refreshing values in ");
		taskText.Append(FileName);
		taskText.SetTranslatable(kFalse);
		progressBar2.SetTaskText(taskText);

		//UIDRef documentTopicListRef;
		//InterfacePtr <IIndexTopicListList> pIndexTopicListList(doc->GetDocWorkSpace(), UseDefaultIID());

		//int32 topicListCount = pIndexTopicListList->GetNumTopicLists();
		//if (topicListCount == 0)
		//{
		//	InterfacePtr<ICommand> piCreateTopicListCmd(CmdUtils::CreateCommand(kCreateTopicListCmdBoss));
		//	InterfacePtr<ICreateTopicListCmdData> piCreateTopicListCmdData(piCreateTopicListCmd, UseDefaultIID());
		//	piCreateTopicListCmdData->SetTargetItem(doc->GetDocWorkSpace());
		//	piCreateTopicListCmdData->SetDoNotifyFlag(kTrue);
		//	if (CmdUtils::ProcessCommand(piCreateTopicListCmd) != kSuccess)
		//		break;
		//}

		//// Indesign only supports upto one topic list currently
		//UID uid = pIndexTopicListList->GetNthTopicList(0);
		//documentTopicListRef = UIDRef(docDatabase, uid);

		set<WideString> childIds;
		for(int32 i=0; i < rootElem->GetChildCount(); i++)
		{
			XMLReference xmlRef = rootElem->GetNthChild(i);
			IIDXMLElement* childElem = xmlRef.Instantiate();
						
			const XMLContentReference& contentRef = childElem->GetContentReference();
			if (contentRef.GetContentType() == XMLContentReference::kContentType_PageItem) 
			{
				FieldSwapJL(childElem, indexReferences, false, childIds, false, oldFieldIdString, newFieldIdString, oldLanguageId, FileName);
				progressBar2.SetPosition(i + 3);
			}

			childElem->Release();
		}
	} while (kFalse);

	rootElem->Release();


}

void PUPDialogObserver::AddKeysWithoutSectionIds(IIDXMLElement* xmlElement, set<PMString>& resultIds)
{
	WideString parentTypeIDValue("");
	WideString parentIDValue("");
	WideString childIdValue("");
	WideString imgFlagValue("");
	PMString childHelperKey("");
	PMString parentHelperKey("");
	WideString underscore("_");
	WideString negativeOne("-1");
	WideString zero("0");
	WideString one("1");
	//WideString itemTypeForAssets("6882"); // JAMIE this needs to be dynamic!!
	//WideString itemTypeForValues("6936"); // JAMIE this needs to be dynamic!!
	//WideString itemTypeForAssets("2192"); // JAMIE this needs to be dynamic!!
	//WideString itemTypeForValues("2246"); // JAMIE this needs to be dynamic!!

	WideString parentID("parentID");
	WideString parentTypeID("parentTypeID");
	WideString childId("childId");
	WideString imgFlag("imgFlag");
	WideString ID("ID");
	WideString LanguageID("LanguageID");

	WideString AttrIdValue("");
	
	PMString selectedAttrId("");
	selectedAttrId.AppendNumber(PMReal(GlobalData::attributeID));
	WideString selectedAttrIDWS(selectedAttrId);

	if (xmlElement->HasAttribute(parentID)) {
		parentIDValue.Append(xmlElement->GetAttributeValue(parentID));
		if (parentIDValue != negativeOne) {
			if (xmlElement->HasAttribute(parentTypeID)) {
				parentTypeIDValue.Append(xmlElement->GetAttributeValue(parentTypeID));
				if (parentTypeIDValue != negativeOne) {
					if (xmlElement->HasAttribute(imgFlag)) {
						imgFlagValue.Append(xmlElement->GetAttributeValue(imgFlag));
						if (imgFlagValue != one) {
							if (xmlElement->HasAttribute(childId)) {
								// add the child details
								childIdValue.Append(xmlElement->GetAttributeValue(childId));
								if (childIdValue != negativeOne) {
									childHelperKey.Append(zero);
									childHelperKey.Append(underscore);
									childHelperKey.Append(childIdValue);
									childHelperKey.Append(underscore);
									childHelperKey.Append(one);
									resultIds.insert(set<PMString> ::value_type(childHelperKey));

									//if(xmlElement->HasAttribute(ID))
									//{
									//	AttrIdValue.Append(xmlElement->GetAttributeValue(ID));
									//	if(AttrIdValue == selectedAttrIDWS )
										//{
											PMString langStr( xmlElement->GetAttributeValue(LanguageID));
											GlobalData::languageId = langStr.GetAsDouble();
										//}
									//}
									PMString childID(childIdValue);

									bool16 itemIdFound = kFalse;									
									for(int32 i = 0 ; i < GlobalData::TotalItemPresentList.size() ; i++)
									{
										if(GlobalData::TotalItemPresentList[i] == childID.GetAsDouble())
											itemIdFound = kTrue;
									}
									if(itemIdFound == kFalse)
										GlobalData::TotalItemPresentList.push_back(childID.GetAsDouble()); 
									
								}
                                
                                PMString langStr( xmlElement->GetAttributeValue(LanguageID));
                                GlobalData::languageId = langStr.GetAsDouble();

								// add the parent details
								parentHelperKey.Append(parentTypeIDValue);
								parentHelperKey.Append(underscore);
								parentHelperKey.Append(parentIDValue);
								parentHelperKey.Append(underscore);
								parentHelperKey.Append(zero);
								parentHelperKey.SetTranslatable(kFalse);
								resultIds.insert(set<PMString> ::value_type(parentHelperKey));
							}
						}
					}
				}
			}
		}
	}

	for(int32 j = 0; j < xmlElement->GetChildCount(); j++)
	{
		XMLReference xmlReference = xmlElement->GetNthChild(j);
		IIDXMLElement* childElement = xmlReference.Instantiate();
		AddKeysWithoutSectionIds(childElement, resultIds);
		childElement->Release();
	}
}


void PUPDialogObserver::FieldSwapJL(IIDXMLElement* xmlElement, vector<IndexReference> &indexReferences, 
									   bool isTable, set<WideString> &childIds, bool isChild, 
									   WideString oldFieldId, WideString newFieldId, WideString oldLanguageId, PMString FileName)
{
	WideString parentTypeIDValue("");
	WideString parentIDValue("");
	WideString childIdValue("");
	WideString imgFlagValue("");
	WideString fieldIdValue("");
	WideString languageIdValue("");
	PMString helperKey("");
	WideString minusOne("_-1");
	WideString zero("0");
	WideString one("1");
	WideString underscore("_");
	WideString negativeOne("-1");

	WideString parentID("parentID");
	WideString parentTypeID("parentTypeID");
	WideString childId("childId");
	WideString imgFlag("imgFlag");
	WideString attributeID("ID");
	WideString languageId("LanguageID");


	if (xmlElement->HasAttribute(attributeID))
	{
		fieldIdValue.Append(xmlElement->GetAttributeValue(attributeID));
		if (fieldIdValue == oldFieldId)
		{
			if (xmlElement->HasAttribute(languageId))
			{
				languageIdValue.Append(xmlElement->GetAttributeValue(languageId));
				if (languageIdValue == oldLanguageId)
				{
					if (xmlElement->HasAttribute(parentID))
					{
						parentIDValue.Append(xmlElement->GetAttributeValue(parentID));
						if (parentIDValue != negativeOne)
						{
							if (xmlElement->HasAttribute(parentTypeID))
							{
								parentTypeIDValue.Append(xmlElement->GetAttributeValue(parentTypeID));
								if (parentTypeIDValue != negativeOne)
								{
									if (xmlElement->HasAttribute(imgFlag)) {
										imgFlagValue.Append(xmlElement->GetAttributeValue(imgFlag));
										if (imgFlagValue != one) 
										{
											if (xmlElement->HasAttribute(childId)) {
												childIdValue.Append(xmlElement->GetAttributeValue(childId));

												bool16 foundValue = false;

												for(int32 i=0; i < indexReferences.size(); i++) 
												{
													IndexReference indexReference = indexReferences.at(i);

													PMString irParentTypeId("");
													PMString irParentId("");
													bool16 irIsItem;
													PMString irOldFieldId("");
													PMString irNewFieldId("");
													irParentTypeId.AppendNumber(PMReal(indexReference.getParentTypeID()));
													irParentId.AppendNumber(PMReal(indexReference.getParentID()));
													irIsItem = indexReference.getIsItem();
													irOldFieldId.AppendNumber(PMReal(indexReference.getOldFieldID()));
													irNewFieldId.AppendNumber(PMReal(indexReference.getNewFieldID()));

													// if the indexReference is found, add to index
													if (( (irParentTypeId == parentTypeIDValue) && (!isChild && (irParentId == parentIDValue)) && (irOldFieldId == fieldIdValue))
														|| (irIsItem && isChild && (irParentId == childIdValue) && (irOldFieldId == fieldIdValue)))
													{
														// change the text for the tag
														PMString childID(childIdValue);
                                                        PMString newValue = indexReference.getNewValue();
                                                        newValue.ParseForEmbeddedCharacters();
														boost::shared_ptr<WideString> newText(new WideString(newValue));

														int32 tagStartPos = -1;
														int32 tagEndPos = -1;
														Utils<IXMLUtils>()->GetElementIndices(xmlElement, &tagStartPos, &tagEndPos);
														tagStartPos = tagStartPos + 1;
														tagEndPos = tagEndPos -1;

														ITextModel* textModel = Utils<IXMLUtils>()->QueryTextModel(xmlElement);
														
														PMString entireStory("");
														bool16 result1 = kFalse;				
														result1=GetTextstoryFromBox(textModel, tagStartPos, tagEndPos, entireStory);

														if(entireStory.IsEqual(indexReference.getNewValue())==kFalse)
														{
															SKUData tempData;
															metadata::Clock a;
															XMP_DateTime dt;  //---CS5 change--
															a.timestamp(dt);
														
															PMString Date;
															Date.AppendNumber(dt.month);
															Date.Append("-");
															Date.AppendNumber(dt.day);  //--CS5--Changes--
															Date.Append("-");
															Date.AppendNumber(dt.year);

															tempData.OriginalData.Append(entireStory);
															tempData.ChangedData.Append(indexReference.getNewValue());
															tempData.DocPath.Append(FileName);
															tempData.Date.Append(Date);
															tempData.PageNo = 0 ; //pageNo;
															tempData.ColumName.Append(GlobalData::OldTable_col);
															tempData.ColumName.Append(" >> ");
															tempData.ColumName.Append(GlobalData::NewTable_col);
															tempData.ItemID = childID.GetAsDouble();
		
															//**** Now Getting the AttriBute Name from AttributeID		
															PMString dispname = GlobalData::oldAttributeName;  //ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(tagInfo.elementId,tagInfo.languageID);
															tempData.attributeName = dispname;		
															tempData.baseNumber ="";
															//*******Up To Here	

															GlobalData::skuDataList.push_back(tempData);

														}

														if(textModel != NULL)
															ReplaceText(textModel, tagStartPos, tagEndPos-tagStartPos +1, newText);

														//change the attributeId in the XML
														Utils<IXMLAttributeCommands> xmlCmd;
														xmlCmd->SetAttributeValue(xmlElement->GetXMLReference(), attributeID, WideString(irNewFieldId));

														textModel->Release();

														
														bool16 itemIdFound = kFalse;									
														for(int32 i = 0 ; i < GlobalData::TotalItemList.size() ; i++)
														{
															if(GlobalData::TotalItemList[i] == childID.GetAsDouble())
																itemIdFound = kTrue;
														}
														if(itemIdFound == kFalse)
															GlobalData::TotalItemList.push_back(childID.GetAsDouble()); 

														foundValue = true;
														break;	
													}
												}

												if (!foundValue) {
													boost::shared_ptr<WideString> newText(new WideString(""));

													int32 tagStartPos = -1;
													int32 tagEndPos = -1;
													Utils<IXMLUtils>()->GetElementIndices(xmlElement, &tagStartPos, &tagEndPos);
													tagStartPos = tagStartPos + 1;
													tagEndPos = tagEndPos -1;

													ITextModel* textModel = Utils<IXMLUtils>()->QueryTextModel(xmlElement);

													if(textModel != NULL)
														ReplaceText(textModel, tagStartPos, tagEndPos-tagStartPos +1, newText);

													Utils<IXMLAttributeCommands> xmlCmd;
													xmlCmd->SetAttributeValue(xmlElement->GetXMLReference(), attributeID, WideString(newFieldId));

													textModel->Release();
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	WideString sectionTag("Section_");
	WideString customTabbedTextTag("CustomTabbedText");
	WideString psTableTag("PSTable");
	WideString header("header");
    WideString psHybridTable("PSHybridTable");

	for(int32 j = 0; j < xmlElement->GetChildCount(); j++)
	{
		XMLReference xmlReference = xmlElement->GetNthChild(j);
		IIDXMLElement* childElement = xmlReference.Instantiate();

		WideString tagName = childElement->GetTagString();
		bool isSectionTag = tagName.Contains(sectionTag);
		bool isCustomTabbedTextTag = tagName == customTabbedTextTag;
		bool isPsTableTag = tagName.Contains(psTableTag);
        bool isAdvTable = tagName.Contains(psHybridTable);

		if (isCustomTabbedTextTag || isPsTableTag || isAdvTable)
			isTable = true;

		if (!isTable && !isSectionTag && !isAdvTable)
		{
			childIds.clear();
            //CA("1111");
            if (childElement->HasAttribute(childId))
            {
                WideString childIdValue("");
                childIdValue.Append(childElement->GetAttributeValue(childId));
                bool isChild = childIdValue != negativeOne;
                
                FieldSwapJL(childElement, indexReferences, isTable, childIds, isChild, oldFieldId, newFieldId, oldLanguageId, FileName);
            }
		}
		else if ( (isTable || isAdvTable)  && !isSectionTag)
		{
			bool isHeaderRowTag = false;
			if (childElement->HasAttribute(header))
			{
				WideString headerValue("");
				headerValue.Append(childElement->GetAttributeValue(header));
				if (headerValue == one)
					isHeaderRowTag = true;
			}

			if (isCustomTabbedTextTag || isPsTableTag || isAdvTable ||!isHeaderRowTag)
			{
				if (childElement->HasAttribute(childId))
				{
					WideString childIdValue("");
					childIdValue.Append(childElement->GetAttributeValue(childId));
					bool isChild = childIdValue != negativeOne;
					if (isChild)
						childIds.insert(set<WideString> ::value_type(childIdValue));

					FieldSwapJL(childElement, indexReferences, isTable, childIds, isChild, oldFieldId, newFieldId, oldLanguageId, FileName);
				}
				else
				{
					FieldSwapJL(childElement, indexReferences, isTable, childIds, false, oldFieldId, newFieldId, oldLanguageId, FileName);
				}
			}
		}
		
		childElement->Release();
	}
}

