//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifdef __ODFRC__

// English string table is defined here

resource StringTable (kSDKDefStringsResourceID + index_enUS)
{
        k_enUS,									// Locale Id
        kEuropeanWinToMacEncodingConverter,		// Character encoding converter (irp) I made this WinToMac as we have a bias to generate on Win...
        {
        	 // ----- Menu strings
                kPUPCompanyKey,					kPUPCompanyValue,
                kPUPAboutMenuKey,					kPUPPluginName "[US]...",
                kPUPPluginsMenuKey,				kPUPPluginName "[US]",
				kPUPDialogMenuItemKey,			"Refresh Price/ Replace Field",//"Refresh Pricing",//"Catalog Price Refresh",
	
                kSDKDefAboutThisPlugInMenuKey,			kSDKDefAboutThisPlugInMenuValue_enUS,

                // ----- Command strings

                // ----- Window strings

                // ----- Panel/dialog strings
				kPUPDialogTitleKey,     kPUPPluginName "",


		// ----- Misc strings
                kPUPAboutBoxStringKey,			kPUPPluginName " [US], version " kPUPVersion " by " kPUPAuthor "\n\n" kSDKDefCopyrightStandardValue "\n\n" kSDKDefPartnersStandardValue_enUS,

				kPUPCatlogPriceRefresgStringKey,				"Catalog Price Refresh",
				kPUPRefreshPriceStringKey,						"Refresh Price",
				kPUPCurrentPriceStringKey,						"Current Price:",		
				kPUPReplaceFieldStringKey,						"Replace field",
				kPUPCurrentFieldStringKey,						"Current Field:",	
				kPUPNewFieldStringKey,							"New Field:",		
				kPUPRefreshPriceResultStringKey,				"Refresh Price Results",
				kPUPNoOfDocRefreshedStringKey,					"No. of document refreshed",
				kPUPColenStringKey,								" : ",
				kPUPTotalItemFoundStringKey,					"Total items found",
				kPUPItemsRefreshedStringKey,					"Items refreshed",
				kPUPDashSeparatorStringKey,						"-----------------------------------------------------------------------------",
				kPUPChangeLogStringKey,							"Change log : ",
                kPUPEventToRefreshStringKey,                    "Event to Refresh:",
				
							
        }

};

#endif // __ODFRC__
