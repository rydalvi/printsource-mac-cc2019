#include "VCPluginHeaders.h"
#include "PMString.h"
#include "AttrData.h"
#include "vector"

using namespace std;

vector<AttrData> AttributeData::AttrInfoVector;

void AttributeData::setAttrVectorInfo(double id,PMString name,PMString lvl,double attrID){
	AttrData publdata;
	publdata.setPubId(id);
	publdata.setPubName(name);
	publdata.setTable_col(lvl);
	publdata.setAttribute_ID(attrID);
	AttrInfoVector.push_back(publdata);
}

AttrData AttributeData::getAttrVectorInfo(int32 index){
	return AttrInfoVector[index];
}

void AttributeData::clearAttrVector(void){
	//AttrInfoVector.erase(AttrInfoVector.begin(),AttrInfoVector.end());
	AttrInfoVector.clear();
	//sectionInfoVector.clear();
}

