#ifndef __Globaldata_h__
#define __Globaldata_h__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "IControlView.h"
#include "vector"
#include "SKUdata.h"

//#include "MSysType.h"
using namespace std;
//typedef vector<PMString> FileList;
class GlobalData
{
public:
	static PMString folderName;
	static bool16 isUpdateStart;
	static IControlView *ListBoxControlView;
	static double SelectedAttributeId;
	static PMString Table_col;
	static double SelectedOldAttrID;
	static PMString OldTable_col;
	static double SelectedNewAttrID;
	static PMString NewTable_col;
	static vector<double> ProductList;
	static int32 TotalDoc;
	static vector<double> TotalItemList;
	static SKUDataList skuDataList;
	// new added by vaibhav
	static double attributeID;
	static vector<double> TotalItemPresentList;

	static double languageId;
	static PMString oldAttributeName;
	static PMString NewAttributeName;
    static double eventId;

	
};
#endif