#ifndef __SLUGSTRUCT_H__
#define __SLUGSTRUCT_H__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "vector"

using namespace std;

class SlugStruct
{
public:
	int32 elementId;
	int32 typeId;
	int32 parentId;
	int32 imgflag;
	int32 parentTypeId;
    PMString elementName;
	PMString colName;
	int16 whichTab;
	int32 reserved1;
	int32 reserved2;
	int32 isAutoResize;

	SlugStruct()
	{
		elementId = -1;
		typeId = -1;
		parentId = -1;
		imgflag = -1;
		parentTypeId = -1;
		elementName.Clear();
		colName.Clear();
		whichTab = -1;
		reserved1 = -1;
		reserved2 = -1;
		isAutoResize = -1;
	}
};

typedef vector<SlugStruct> SlugList;

#endif