//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __PUPID_h__
#define __PUPID_h__

#include "SDKDef.h"

// Company:
#define kPUPCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kPUPCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kPUPPluginName	"APJS10_PriceUpdate"			// Name of this plug-in.
#define kPUPPrefixNumber	0xDA1509 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kPUPVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kPUPAuthor		"Apsiva Inc."					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kPUPPrefixNumber above to modify the prefix.)
#define kPUPPrefix		RezLong(kPUPPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kPUPStringPrefix	SDK_DEF_STRINGIZE(kPUPPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kPUPMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kPUPMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kPUPPluginID, kPUPPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kPUPActionComponentBoss, kPUPPrefix + 0)
DECLARE_PMID(kClassIDSpace, kPUPDialogBoss, kPUPPrefix + 2)
DECLARE_PMID(kClassIDSpace, kPUPSelectFileDialogBoss,		kPUPPrefix + 4)
DECLARE_PMID(kClassIDSpace, kSPListBoxWidgetBoss,		kPUPPrefix + 5)
DECLARE_PMID(kClassIDSpace, kPUPProgressBarWidgetBoss,		kPUPPrefix + 6)
DECLARE_PMID(kClassIDSpace, kBscTAAttrBoss1,		kPUPPrefix + 7)

//DECLARE_PMID(kClassIDSpace, kAttrBossReferencingBoss, kPUPPrefix + 8)


// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_ISLUGDATA1,		kPUPPrefix + 0 )
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 1)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_IPUPINTERFACE, kPUPPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kPUPActionComponentImpl, kPUPPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kPUPDialogObserverImpl,	kPUPPrefix + 1 )
DECLARE_PMID(kImplementationIDSpace, kSPListBoxObserverImpl,	kPUPPrefix + 3 )
DECLARE_PMID(kImplementationIDSpace, kSlug1Impl,				kPUPPrefix + 4)
DECLARE_PMID(kImplementationIDSpace, kBscTAAttrReport1Impl,		kPUPPrefix + 5)
DECLARE_PMID(kImplementationIDSpace, kPUPDialogControllerImpl, kPUPPrefix + 8)


// ActionIDs:
//DECLARE_PMID(kActionIDSpace, kPUPAboutActionID, kPUPPrefix + 0)
DECLARE_PMID(kActionIDSpace, kPUPDialogActionID, kPUPPrefix + 0)
//DECLARE_PMID(kActionIDSpace, kPUPMenuItem1ActionID,		kPUPPrefix + (10 + 1))
DECLARE_PMID(kActionIDSpace, BookPanelFlyoutPanel_MySeparator1_ActionID,		kPUPPrefix + 1)


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kPUPDialogWidgetID, kPUPPrefix + 1)
DECLARE_PMID(kWidgetIDSpace, kSelectDocButtonWidgetID, 		kPUPPrefix +2)
DECLARE_PMID(kWidgetIDSpace, kPUPSelectFileDialogWidgetID, 	kPUPPrefix +3)
DECLARE_PMID(kWidgetIDSpace, kPUPEditBoxWidgetID, 			kPUPPrefix +4)
DECLARE_PMID(kWidgetIDSpace, kBrowseButtonWidgetID, 		kPUPPrefix +5)
DECLARE_PMID(kWidgetIDSpace, kPUPAllDocCheckBoxWidgetID, 	kPUPPrefix +6)
DECLARE_PMID(kWidgetIDSpace, kUpdateButtonWidgetID, 		kPUPPrefix +7)
DECLARE_PMID(kWidgetIDSpace, kPriceAttrButtonWidgetID, 		kPUPPrefix +8)
DECLARE_PMID(kWidgetIDSpace, kPUPCancelButtonWidgetID, 		kPUPPrefix +9)
DECLARE_PMID(kWidgetIDSpace, kPUPProgressDialogWidgetID, 	kPUPPrefix +10)
DECLARE_PMID(kWidgetIDSpace, kPUPReportDialogWidgetID, 		kPUPPrefix +11)
DECLARE_PMID(kWidgetIDSpace, kFinishButtonWidgetID, 		kPUPPrefix +12)
DECLARE_PMID(kWidgetIDSpace, kNewUpdateButtonWidgetID, 		kPUPPrefix +13)
DECLARE_PMID(kWidgetIDSpace, kFolderNameWidgetID, 			kPUPPrefix +29)
DECLARE_PMID(kWidgetIDSpace, kSDFilterOptionListWidgetID,	kPUPPrefix +14)
DECLARE_PMID(kWidgetIDSpace, kSPRFltOPtListParentWidgetId,	kPUPPrefix +15)
DECLARE_PMID(kWidgetIDSpace, kSPRUnCheckIconWidgetID,		kPUPPrefix +16)
DECLARE_PMID(kWidgetIDSpace, kSPRCheckIconWidgetID,			kPUPPrefix +17)
DECLARE_PMID(kWidgetIDSpace, kSPRFltOptTextWidgetID,		kPUPPrefix +18)
DECLARE_PMID(kWidgetIDSpace, kProgressBarWidgetID,			kPUPPrefix +19)
DECLARE_PMID(kWidgetIDSpace, kPUPAttributeComboBoxWidgetID,	kPUPPrefix +20)
DECLARE_PMID(kWidgetIDSpace, kTotalDocWidgetID,				kPUPPrefix +21)
//DECLARE_PMID(kWidgetIDSpace, kTotalProductWidgetID,			kPUPPrefix +22)
//DECLARE_PMID(kWidgetIDSpace, kTotalPriceAttributeWidgetID,	kPUPPrefix +23)
DECLARE_PMID(kWidgetIDSpace, kRediobtnRefreshPriceID,		kPUPPrefix +24)
DECLARE_PMID(kWidgetIDSpace, kRediobtnChangePriceID,		kPUPPrefix +25)
DECLARE_PMID(kWidgetIDSpace, kPUPOldAttributeComboBoxWidgetID,	kPUPPrefix +26)
DECLARE_PMID(kWidgetIDSpace, kPUPNewAttributeComboBoxWidgetID,	kPUPPrefix +27)
DECLARE_PMID(kWidgetIDSpace, kNewPriceUpdateButtonWidgetID,	kPUPPrefix +28)
DECLARE_PMID(kWidgetIDSpace, kSecondScreenWidgetID,			kPUPPrefix +30)
DECLARE_PMID(kWidgetIDSpace, kFirstScreenWidgetID,			kPUPPrefix +31)
//DECLARE_PMID(kWidgetIDSpace, kTotalItemWidgetID,			kPUPPrefix +32)
//DECLARE_PMID(kWidgetIDSpace, kTotalUnChangedItemPriceWidgetID,	kPUPPrefix +33)
DECLARE_PMID(kWidgetIDSpace, kColonWidgetID,				kPUPPrefix +34)
DECLARE_PMID(kWidgetIDSpace, kTotalitemsfoundWidgetID,		kPUPPrefix +35)
DECLARE_PMID(kWidgetIDSpace, kItemsRefreshedWidgetID,		kPUPPrefix +36)
DECLARE_PMID(kWidgetIDSpace, klogPathWidgetID,				kPUPPrefix +37)
DECLARE_PMID(kWidgetIDSpace, kCancelPriceUpdateButtonWidgetID,	kPUPPrefix +38)
DECLARE_PMID(kWidgetIDSpace, kPUPEventListComboBoxWidgetID,	kPUPPrefix +39)



// "About Plug-ins" sub-menu:
#define kPUPAboutMenuKey			kPUPStringPrefix "kPUPAboutMenuKey"
#define kPUPAboutMenuPath		kSDKDefStandardAboutMenuPath kPUPCompanyKey

// "Plug-ins" sub-menu:
#define kPUPPluginsMenuKey 		kPUPStringPrefix "kPUPPluginsMenuKey"
#define kPUPPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kPUPCompanyKey kSDKDefDelimitMenuPath kPUPPluginsMenuKey

#define kLNGDialogMenuItemPosition	1.0
// Menu item keys:
#define kPUPMenuItem1MenuItemKey		kPUPStringPrefix "kPUPMenuItem1MenuItemKey"

// Other StringKeys:
#define kPUPAboutBoxStringKey	kPUPStringPrefix "kPUPAboutBoxStringKey"
#define kPUPMenuItem1StringKey			kPUPStringPrefix "kPUPMenuItem1StringKey"
#define kPUPTargetMenuPath kPUPPluginsMenuPath

#define kPUPCatlogPriceRefresgStringKey kPUPStringPrefix "kPUPCatlogPriceRefresgStringKey"
#define kPUPRefreshPriceStringKey kPUPStringPrefix "kPUPRefreshPriceStringKey"
#define kPUPCurrentPriceStringKey kPUPStringPrefix "kPUPCurrentPriceStringKey"
#define kPUPReplaceFieldStringKey kPUPStringPrefix "kPUPReplaceFieldStringKey"
#define kPUPCurrentFieldStringKey kPUPStringPrefix "kPUPCurrentFieldStringKey"
#define kPUPNewFieldStringKey kPUPStringPrefix "kPUPNewFieldStringKey"
#define kPUPRefreshPriceResultStringKey kPUPStringPrefix "kPUPRefreshPriceResultStringKey"
#define kPUPNoOfDocRefreshedStringKey kPUPStringPrefix "kPUPNoOfDocRefreshedStringKey"
#define kPUPColenStringKey kPUPStringPrefix "kPUPColenStringKey"
#define kPUPTotalItemFoundStringKey kPUPStringPrefix "kPUPTotalItemFoundStringKey"
#define kPUPItemsRefreshedStringKey kPUPStringPrefix "kPUPItemsRefreshedStringKey"
#define kPUPDashSeparatorStringKey kPUPStringPrefix "kPUPDashSeparatorStringKey"
#define kPUPChangeLogStringKey kPUPStringPrefix "kPUPChangeLogStringKey"
#define kPUPEventToRefreshStringKey kPUPStringPrefix "kPUPEventToRefreshStringKey"


// Menu item positions:

#define kPUPMenuItem1MenuItemPosition 1
#define kPUPDialogTitleKey kPUPStringPrefix "kPUPDialogTitleKey"
// "Plug-ins" sub-menu item key for dialog:
#define kPUPDialogMenuItemKey kPUPStringPrefix "kPUPDialogMenuItemKey"
// "Plug-ins" sub-menu item position for dialog:
#define kPUPDialogMenuItemPosition	12.0

#define kSPRFilterOptionElementRsrcID 1001
#define kUnChkIconID				21011	
#define kChkIconID					21012

// Initial data format version numbers
#define kPUPFirstMajorFormatNumber  RezLong(1)
#define kPUPFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kPUPCurrentMajorFormatNumber kPUPFirstMajorFormatNumber
#define kPUPCurrentMinorFormatNumber kPUPFirstMinorFormatNumber

//PNGR ID
#define kPNGNewUpdateIconRsrcID               1000
#define kPNGNewUpdateIconRollRsrcID           1000
#define kPNGFinishIconRsrcID                   1001
#define kPNGFinishIconRollRsrcID               1001
#define kPNGUpdateIconRsrcID                   1002
#define kPNGUpdateIconRollRsrcID               1002
#define kPNGCancelUpdateIconRsrcID             1003
#define kPNGCancelUpdateIconRollRsrcID             1003
#endif // __PUPID_h__
