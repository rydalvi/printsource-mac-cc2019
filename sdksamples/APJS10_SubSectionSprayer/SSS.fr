//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// General includes:
#include "MenuDef.fh"
#include "ActionDef.fh"
#include "ActionDefs.h"
#include "AdobeMenuPositions.h"
#include "LocaleIndex.h"
#include "PMLocaleIds.h"
#include "StringTable.fh"
#include "ObjectModelTypes.fh"
#include "ShuksanID.h"
#include "ActionID.h"
#include "CommandID.h"
#include "WorkspaceID.h"
#include "WidgetID.h"
#include "BuildNumber.h"
#include "PlugInModel_UIAttributes.h"

#include "InterfaceColorDefines.h"
#include "IControlViewDefs.h"
#include "SysControlIDs.h"
#include "Widgets.fh"	// for PalettePanelWidget or DialogBoss
#include "LayoutID.h"
#include "EveInfo.fh"	// Required when using EVE for dialog layout/widget placement

// Project includes:
#include "SSSID.h"
#include "GenericID.h"
#include "ShuksanID.h"
#include "TextID.h"


#ifdef __ODFRC__

/*  
 * Plugin version definition.
 */
resource PluginVersion (kSDKDefPluginVersionResourceID)
{
	kTargetVersion,
	kSSSPluginID,
	kSDKDefPlugInMajorVersionNumber, kSDKDefPlugInMinorVersionNumber,
	kSDKDefHostMajorVersionNumber, kSDKDefHostMinorVersionNumber,
	kSSSCurrentMajorFormatNumber, kSSSCurrentMinorFormatNumber,
	{ kInDesignProduct, kInCopyProduct},
	{ kWildFS },
	kUIPlugIn,
	kSSSVersion
};

/*  
 * The ExtraPluginInfo resource adds extra information to the Missing Plug-in dialog
 * that is popped when a document containing this plug-in's data is opened when
 * this plug-in is not present. These strings are not translatable strings
 * since they must be available when the plug-in isn't around. They get stored
 * in any document that this plug-in contributes data to.
 */
resource ExtraPluginInfo(1)
{
	kSSSCompanyValue,			// Company name
	kSSSMissingPluginURLValue,	// URL 
	kSSSMissingPluginAlertValue,	// Missing plug-in alert text
};

/* 
 * Boss class definitions.
 */
resource ClassDescriptionTable(kSDKDefClassDescriptionTableResourceID)
{{{

	Class
	{
		kSubSectionSprayerBoss,
		kInvalidClass,
		{
			IID_ISUBSECTIONSPRAYER, kSubSectionSprayerImpl,
		}
	},
	/*
	 * This boss class supports two interfaces:
	 * IActionComponent and IPMPersist.
     *
	 * 
	 * @ingroup apjs9_subsectionsprayer
	 */
	Class
	{
		kSSSActionComponentBoss,
		kInvalidClass,
		{
			// Handle the actions from the menu.
			IID_IACTIONCOMPONENT, kSSSActionComponentImpl,
			// Persist the state of the menu across application instantiation. Implementation provided by the API.
			IID_IPMPERSIST, kPMPersistImpl
		}
	},

	/*
	 * This boss class implements the dialog for this plug-in. All
	 * dialogs must implement IDialogController. Specialisation of
	 * IObserver is only necessary if you want to handle widget
	 * changes dynamically rather than just gathering their values
	 * and applying in the IDialogController implementation.
	 * In this implementation IObserver is specialised so that the
	 * plug-in's about box is popped when the info button widget
	 * is clicked.
     *
	 * 
	 * @ingroup apjs10_subsectionsprayer
	 */
	Class
	{
		kSSSDialogBoss,
		kDialogBoss,
		{
			// Provides management and control over the dialog.
			IID_IDIALOGCONTROLLER, kSSSDialogControllerImpl,
			
			// Allows dynamic processing of dialog changes.
			IID_IOBSERVER, kSSSDialogObserverImpl,
		}
	},

	/**
		Gives a list-box that has additional observer added... 
		to get selection change notifications
		@ingroup wlistboxcomposite
	*/
	Class 
	{
		kSSSListBoxWidgetBoss,
		kWidgetListBoxWidgetNewBoss,
		{
			//IID_IOBSERVER,	kPpsixListBoxObserverImpl,
		}
	},

	/**
		Text Widget boss.
	*/
	Class
	{
		kSSSTextWidgetBoss,
		kInfoStaticTextWidgetBoss,
		{
			// Add the following interfaces for a white background (they'd override the default.)
			// IID_ICONTROLVIEW, kAboutBoxStaticTextViewImpl,
			// IID_IBOOLDATA, kPersistBoolDataImpl,
		}
		
	 }

	 AddIn
	{
		kIntegratorSuiteBoss,
		kInvalidClass,
		{
			/** Provides capability to insert table, get and set text.
			*/
			//IID_ITBLBSCSUITE, kTblBscSuiteASBImpl,
			/*IID_ITEXTMISCELLANYSUITEE*/IID_SSSITEXTMISCELLANYSUITEE, kSSSTextMiscellanySuiteASBImpl/*kTextMiscellanySuiteASBImpl*/,
		}
	},
	AddIn
	{
		kLayoutSuiteBoss,
		kInvalidClass,
		{
			/*IID_ITEXTMISCELLANYSUITEE*/IID_SSSITEXTMISCELLANYSUITEE, kSSSTextMiscellanySuiteLayoutCSBImpl,
			//IID_ITBLBSCSUITE, kTblBscSuiteTextCSBImpl,
		}
	},	
		
	AddIn
	{
		kTextSuiteBoss,
		kInvalidClass,
		{
			/** Provides capability to insert table, get and set text.
			*/
			/*IID_ITEXTMISCELLANYSUITEE*/IID_SSSITEXTMISCELLANYSUITEE, kSSSTextMiscellanySuiteLayoutCSBImpl,
			//IID_ITBLBSCSUITE, kTblBscSuiteTextCSBImpl,
		}
	},
}}};

/*  
 * Implementation definition.
 */
resource FactoryList (kSDKDefFactoryListResourceID)
{
	kImplementationIDSpace,
	{
		#include "SSSFactoryList.h"
	}
};

/*  
 * Menu definition.
 */
resource MenuDef (kSDKDefMenuResourceID)
{
	{
		// The About sub-menu item for this plug-in.
/*		kSSSAboutActionID,			// ActionID (kInvalidActionID for positional entries)
		kSSSAboutMenuPath,			// Menu Path.
		kSDKDefAlphabeticPosition,											// Menu Position.
		kSDKDefIsNotDynamicMenuFlag,										// kSDKDefIsNotDynamicMenuFlag or kSDKDefIsDynamicMenuFlag

		// The Plug-ins menu sub-menu items for this plug-in.
		kSSSDialogActionID,
		kSSSPluginsMenuPath,
		kSSSDialogMenuItemPosition,
		kSDKDefIsNotDynamicMenuFlag,
*/
	}
};

/* 
 * Action definition.
 */
resource ActionDef (kSDKDefActionResourceID)
{
	{
/*		kSSSActionComponentBoss, 		// ClassID of boss class that implements the ActionID.
		kSSSAboutActionID,	// ActionID.
		kSSSAboutMenuKey,	// Sub-menu string.
		kOtherActionArea,				// Area name (see ActionDefs.h).
		kNormalAction,					// Type of action (see ActionDefs.h).
		kDisableIfLowMem,				// Enabling type (see ActionDefs.h).
		kInvalidInterfaceID,			// Selection InterfaceID this action cares about or kInvalidInterfaceID.
		kSDKDefInvisibleInKBSCEditorFlag, // kSDKDefVisibleInKBSCEditorFlag or kSDKDefInvisibleInKBSCEditorFlag.

		kSSSActionComponentBoss,
		kSSSDialogActionID,		
		kSSSDialogMenuItemKey,		
		kOtherActionArea,			
		kNormalAction,				
		kDisableIfLowMem,	
		kInvalidInterfaceID,		
		kSDKDefVisibleInKBSCEditorFlag,
*/
	}
};


/*  
 * Locale Indicies.
 * The LocaleIndex should have indicies that point at your
 * localizations for each language system that you are localized for.
 */

/*  
 * String LocaleIndex.
 */
resource LocaleIndex ( kSDKDefStringsResourceID)
{
	kStringTableRsrcType,
	{
		kWildFS, k_enUS, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_enGB, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_deDE, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_frFR, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_esES, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_ptBR, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_svSE, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_daDK, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_nlNL, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_itIT, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_nbNO, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_fiFI, kSDKDefStringsResourceID + index_enUS
		kInDesignJapaneseFS, k_jaJP, kSDKDefStringsResourceID + index_jaJP
	}
};

resource LocaleIndex (kSDKDefStringsNoTransResourceID)
{
	kStringTableRsrcType,
	{
		kWildFS, k_Wild, kSDKDefStringsNoTransResourceID + index_enUS
	}
};

// Strings not being localized
resource StringTable (kSDKDefStringsNoTransResourceID + index_enUS)
{
	k_enUS,									// Locale Id
	kEuropeanMacToWinEncodingConverter,		// Character encoding converter
	{
	}
};

/*  
 * Dialog LocaleIndex.
 */
resource LocaleIndex (kSDKDefDialogResourceID)
{
   kViewRsrcType,
	{
		kWildFS, k_Wild, kSDKDefDialogResourceID + index_enUS
	}
};

resource LocaleIndex (kSSSListElementRsrcID)
{
	kViewRsrcType,
	{
		kWildFS, k_Wild, kSSSListElementRsrcID + index_enUS
	}
};


/*  
 * Type definition.
 */
type SSSDialogWidget(kViewRsrcType) : DialogBoss(ClassID = kSSSDialogBoss)
{
	//WidgetEveInfo;
};

type SSSListBox			(kViewRsrcType) : WidgetListBoxWidgetN	(ClassID = kSSSListBoxWidgetBoss) { };
type SSSListBoxTwo			(kViewRsrcType) : WidgetListBoxWidgetN	(ClassID = kSSSListBoxWidgetBoss) { };
type SSSTextWidget		(kViewRsrcType) : InfoStaticTextWidget	(ClassID = kSSSTextWidgetBoss) { };

/*
type PicIcoPictureWidget(kViewRsrcType) : IconSuiteWidget(ClassID = kPicIcoPictureWidgetBoss) //--CS5--
{
	UIFontSpec;	// because we want to read in two integers for the default and hilite states.
};
*/


resource PNGA(kPNGProductIconRsrcID)  "ItemGroup_hover80x22.png"
resource PNGR(kPNGProductIconRollRsrcID)  "ItemGroup_80x22.png"
	
resource PNGA(kPNGItemIconRsrcID)  "Item_hover80x22.png"
resource PNGR(kPNGItemIconRollRsrcID)  "Item_80x22.png"
	
resource PNGA(kPNGTableIconRsrcID)  "Table_hover80x22.png"
resource PNGR(kPNGTableIconRollRsrcID)  "Table_80x22.png"
	
resource PNGA(kPNGSectionIconRsrcID)  "Section_hover80x22.png"
resource PNGR(kPNGSectionIconRollRsrcID)  "Section_80x22.png"
	
resource PNGA(kPNGSectionSprayOKIconRsrcID)  "Spray_hover80x22.png"
resource PNGR(kPNGSectionSprayOKIconRollRsrcID)  "Spray_80x22.png"
	
resource PNGA(kPNGSectionSprayCancelIconRsrcID)  "Cancel1_hover80x22.png"
resource PNGR(kPNGSectionSprayCancelIconRollRsrcID)  "Cancel1_80x22.png"
	
	
//---for CS5, Remove BMP Images in place add PNG Images ----
resource PNGA(kSSSVerticalWidget1IconID) "vertical_flow1.PNG"		
resource PNGR(kSSSVerticalWidget1IconRsrcID) "vertical_flow1.PNG"

resource PNGA(kSSSVerticalWidgetIconID) "vertical_flow.PNG"		
resource PNGR(kSSSVerticalWidgetIconRsrcID) "vertical_flow.PNG"

resource PNGA(kSSSHorizontalWidget1IconID) "horizontal_flow1.PNG"		
resource PNGR(kSSSHorizontalWidget1IconRsrcID) "horizontal_flow1.PNG"

resource PNGA(kSSSHorizontalWidgetIconID) "horizontal_flow.PNG"		
resource PNGR(kSSSHorizontalWidgetIconRsrcID) "horizontal_flow.PNG"

/*  
 * Dialog definition.
 * Use EVE for laying our dialogs.
 *
 * This view is not localised: therefore, it can reside here.
 * However, if you wish to localise it, it is recommended to locate it in one of
 * the localised framework resource files (i.e. SSS_enUS.fr etc.) and
 * update your Dialog LocaleIndex accordingly.
 */
resource SSSDialogWidget (kSDKDefDialogResourceID + index_enUS)
{
	__FILE__, __LINE__,
	kSSSDialogWidgetID,	// WidgetID
	kPMRsrcID_None,				// RsrcID
	kBindNone,					// Binding
	//0, 0, 590,429,				// Frame (l,t,r,b)
	//0, 0, 310,205,				// Frame (l,t,r,b)
    0, 0, 310,235,
	kTrue, kTrue,				// Visible, Enabled
	kSSSDialogTitleKey,	// Dialog name
	{
	GenericPanelWidget
	(
		kInvalidWidgetID,  //kMarginsPanelWidgetID,										// widget ID
		kPMRsrcID_None,										// PMRsrc ID
		kBindNone,											// frame binding	
		//Frame(3.0,10.0,587.0,389.0)							// left, top, right, bottom
		//Frame(3.0,10.0,307.0,204.0)							// left, top, right, bottom
        Frame(3.0,10.0,307.0,234.0)							// left, top, right, bottom
		kTrue,												// visible
		kTrue,												// enabled
		"",													// control label
		{ 
					
			
			GroupPanelWidget
			(
				kFirstGroupPanelWidgetID1,					// widget ID
				kPMRsrcID_None,								// PMRsrc ID
				kBindNone,								// frame binding				
				//Frame(3.0,4.0,580.0,45.0)	            // left, top, right, bottom
				Frame(3.0,4.0,303,45.0)	            // left, top, right, bottom
				kTrue,										// visible
				kTrue,										// enabled
				0,											// header widget ID
				{ 
					InfoStaticTextWidget
					(
															// CControlView properties
						kItemGroupItemWidgetID,					// widget ID
						kSysStaticTextPMRsrcId,				// PMRsrc ID
						kBindNone,							// frame binding				
						Frame(70,-10,230,0)					// left, top, right, bottom	
 						kTrue,								// visible
						kTrue,								// enabled
						kAlignLeft,							// Alignment
						kDontEllipsize,	
						kTrue,					// Ellipsize style
						kBkanStringKey, //"",//"Spacing between Products/Items", // control label
						kInvalidWidgetID,					// associated widget ID
						kPaletteWindowSystemScriptFontId,	// fFontID
						kPaletteWindowSystemScriptFontId,	// fHiliteFontID
					),
					InfoStaticTextWidget
					(
															// CControlView properties
						kInvalidWidgetID,					// widget ID
						kSysStaticTextPMRsrcId,				// PMRsrc ID
						kBindNone,							// frame binding			
						//Frame(110.0,12.0,168.0,34.0)           // left, top, right, bottom
						Frame(28,12.0,98,34.0)           // left, top, right, bottom
						kTrue,								// visible
						kTrue,								// enabled
						kAlignLeft,							// Alignment
						kDontEllipsize,kTrue,						// Ellipsize style
						kSSSVerticalStringKey, //"Vertical Points:", // control label
						kInvalidWidgetID,					// associated widget ID
						kPaletteWindowSystemScriptFontId,	// fFontID
						kPaletteWindowSystemScriptFontId,	// fHiliteFontID
					),
					
					RealEditBoxWidget
					(
						// CControlView properties
						kVerticalSpacingWidgetID, // widget ID
						kSysEditBoxPMRsrcId, // PMRsrc ID
						kBindNone, // frame binding					
						//Frame(176.0,15.0,211.0,30.0)   //  left, top, right, bottom
						Frame(108,13.0,138,33.0)   //  left, top, right, bottom
						kTrue, // visible
						kTrue, // enabled
						// CEditBoxAttributes
						0, // nudgeWidgetId (0 or kInvalidWidgetID if no nudge required)
						1.0, // small nudge amount
						5.0, // large nudge amount
						0, // max num chars
						kFalse, // read only flag
						kFalse, // should notify on each key stroke
						// TextDataValidation properties
						kTrue, // range checking enabled
						kFalse, // blank entry allowed
						1000.0, // upper limit
						0.0, // lower limit
						// CTextControlData properties
						"0", // control label
					),
					
					//------------------------------ horizontal-----------------------------------------------//
					InfoStaticTextWidget
					(
															// CControlView properties
						kInvalidWidgetID,					// widget ID
						kSysStaticTextPMRsrcId,				// PMRsrc ID
						kBindNone,							// frame binding					
						//Frame(355,12,427,34)		        // left, top, right, bottom
						Frame(158,12,228,34)		        // left, top, right, bottom
						kTrue,								// visible
						kTrue,								// enabled
						kAlignLeft,							// Alignment
						kDontEllipsize,	kTrue,					// Ellipsize style
						kSSSHorizontalStringKey, //"Horizontal Points:", // control label
						kInvalidWidgetID,					// associated widget ID
						kPaletteWindowSystemScriptFontId,	// fFontID
						kPaletteWindowSystemScriptFontId,	// fHiliteFontID
					),
					
					RealEditBoxWidget
					(
						// CControlView properties
						kHorizontalSpacingWidgetID, // widget ID
						kSysEditBoxPMRsrcId, // PMRsrc ID
						kBindNone, // frame binding					
						//Frame(435,15,470,30) //  left, top, right, bottom 
						Frame(238,13,268,33) //  left, top, right, bottom 
						kTrue, // visible
						kTrue, // enabled
						// CEditBoxAttributes
						0, // nudgeWidgetId (0 or kInvalidWidgetID if no nudge required)
						1.0, // small nudge amount
						5.0, // large nudge amount
						0, // max num chars
						kFalse, // read only flag
						kFalse, // should notify on each key stroke
						// TextDataValidation properties
						kTrue, // range checking enabled
						kFalse, // blank entry allowed
						1000.0, // upper limit
						0.0, // lower limit
						// CTextControlData properties
						"0", // control label
					),
				}
			),
			
			GroupPanelWidget
			(
				kSecondGroupPanelWidgetID1,							// widget ID
				kPMRsrcID_None,								// PMRsrc ID
				kBindNone,									// frame binding		
				//Frame(3.0,58,580,138)						// left, top, right, bottom				
				Frame(3.0,58,303,138)						// left, top, right, bottom		
				kTrue,										// visible
				kTrue,										// enabled
				0,											// header widget ID
				{
					InfoStaticTextWidget
					(
															// CControlView properties
						kInvalidWidgetID,					// widget ID
						kSysStaticTextPMRsrcId,				// PMRsrc ID
						kBindNone,							// frame binding
				//		Frame(184.0,-4,240.0, 4.0)			// left, top, right, bottom
						Frame(130,-5,158,5)					// left, top, right, bottom
						kTrue,								// visible
						kTrue,								// enabled
						kAlignLeft,							// Alignment
						kDontEllipsize,kTrue,						// Ellipsize style
						kPageStringKey, //" Page ", // control label
						kInvalidWidgetID,					// associated widget ID
						kPaletteWindowSystemScriptFontId,//kDialogWindowSystemScriptFontId, //kPaletteWindowSystemScriptFontId,	// fFontID
						kDialogWindowSystemScriptFontId, //kPaletteWindowSystemScriptFontId,	// fHiliteFontID
					),
					
					ClusterPanelWidget
					(
						// CControlView properties
						kInvalidWidgetID, // widget ID
						kPMRsrcID_None, // PMRsrc ID
						kBindNone, // frame binding		
						Frame(0,10,575,108) //  left, top, right, bottom
						kTrue, // visible
						kTrue, // enabled
						// CTextControlData properties
						"", // control label
						{ // CPanelControlData Children
								
							RadioButtonWidget
							(
								// CControlView properties
								kSSSVerticalFlowRadioButtonWidgetID, // widget ID
								kSysRadioButtonPMRsrcId, // PMRsrc ID
								kBindNone, // frame binding						
								Frame(28.0,5.0,49.0,55.0 ) //  left, top, right, bottom
								kTrue, // visible
								kTrue, // enabled
								// CTextControlData properties
								"", // control label
							),
						
							IconSuiteWidget		//PictureWidget		//----CS5--
							(
								kInvalidWidgetID,				// WidgetId
								kSSSVerticalWidgetIconID, 
								kSSSPluginID,	// RsrcId
								kBindNone,									// Frame binding
								Frame(50,5,130,55), 						
								kTrue,kTrue,								// Visible, Enabled
								kADBEIconSuiteButtonType,0,
							),
							
						/*	RadioButtonWidget
							(
								// CControlView properties
								kSSSAltVerticalFLowRadioButtonWidgetID, // widget ID
								kSysRadioButtonPMRsrcId, // PMRsrc ID
								kBindNone, // frame binding						
								Frame(158.0,5.0,178.0,55.0)
								kTrue, // visible
								kTrue, // enabled
								// CTextControlData properties
								"", // control label
							),
							
					
							IconSuiteWidget		//PictureWidget		//----CS5--
							(
								kInvalidWidgetID,				// WidgetId
								kSSSVerticalWidget1IconID, 
								kSSSPluginID,	// RsrcId
								kBindNone,									// Frame binding						
								Frame( 184,5,269,55),						
								kTrue,kTrue,								// Visible, Enabled
								kADBEIconSuiteButtonType,0,
							),
						*/	
							RadioButtonWidget
							(
								// CControlView properties
								kSSSHorzintalFlowRadioButtonWidgetID, // widget ID
								kSysRadioButtonPMRsrcId, // PMRsrc ID
								kBindNone, // frame binding						
								Frame(158.0,5.0,178.0,55.0) //Frame(297,5,317,55 )  //  left, top, right, bottom
								kTrue, // visible
								kTrue, // enabled
								// CTextControlData properties
								"", // control label
							),
							
							IconSuiteWidget		//PictureWidget		//----CS5--
							(
								kInvalidWidgetID,				// WidgetId
								kSSSHorizontalWidgetIconID, 
								kSSSPluginID,	// RsrcId
								kBindNone,									// Frame binding						
								Frame( 179,5,264,55),	//Frame(323,5,408,55),  	//  left, top, right, bottom		
								kTrue,kTrue,								// Visible, Enabled
								kADBEIconSuiteButtonType,0,
							),
															
						/*	RadioButtonWidget
							(
								// CControlView properties
								kSSSAltHorzintalFlowRadioButtonWidgetID, // widget ID
								kSysRadioButtonPMRsrcId, // PMRsrc ID
								kBindNone, // frame binding						
								Frame(436,5,456,55) //  left, top, right, bottom
								kTrue, // visible
								kTrue, // enabled
								// CTextControlData properties
								"", // control label
							),						
						
							IconSuiteWidget		//PictureWidget		//----CS5--
							(
								kInvalidWidgetID,				// WidgetId
								kSSSHorizontalWidget1IconID, 
								kSSSPluginID,	// RsrcId
								kBindNone,									// Frame binding						
								Frame(462,5,542,55), 	 //  left, top, right, bottom			
								kTrue,kTrue,								// Visible, Enabled
								kADBEIconSuiteButtonType,0,
							),
							*/
						}
					)         
				}
			)


			
/*
			GroupPanelWidget
			(
				kThirdGroupPanelWidgetID,					// widget ID
				kPMRsrcID_None,								// PMRsrc ID
				kBindNone,									// frame binding
				//Frame(3.0,200.0,290.0,255.6)              
			//	Frame(3.0,305.0,290.0,340.0)				// left, top, right, bottom
			//	Frame(3.0,305.0,435.0,340.0)				// left, top, right, bottom
				Frame(3,257,580,378)				// left, top, right, bottom
				kTrue,										// visible
				kTrue,										// enabled
				0,											// header widget ID
				{
					
					CheckBoxWidget
					(
						kIsAllSectionSprayWidgetID,					// WidgetId
						kSysCheckBoxPMRsrcId,				// RsrcId
						kBindRight | kBindBottom,
						//Frame(10,15,570,40)					
						//Frame(10,10,250,30)						// Frame
						Frame(20,6,260,26)						// Frame
						kTrue,								// Visible
						kTrue,								// Enabled
						kAlignLeft,							// Alignment
						kSSSEventStringKey, //"Event",//"Spray All Sections At The Current Level,"// commented By Amarjit on 11 nov
					),
					StaticTextWidget
					(
						// CControlView properties
						kIsAllSectionSpraySecondLineWidgetID, // widget ID
						kSysStaticTextPMRsrcId, // PMRsrc ID
						kBindNone, // frame binding
						Frame(45,27,285,47),
						kTrue, // visible
						kTrue, 
						// StaticTextAttributes properties
						kAlignLeft, // Alignment
						kDontEllipsize,kTrue, // Ellipsize style
						// CTextControlData properties
						"",//"Under The Same Parent(all of the children)", // control label //---
						// AssociatedWidgetAttributes properties
						kInvalidWidgetID, // associated widget ID
					), 
////////////////////////Vivekanand					
					CheckBoxWidget
					(
						kIsWithoutPageBreakWidgetID,					// WidgetId
						kSysCheckBoxPMRsrcId,				// RsrcId
						kBindRight | kBindBottom,
						Frame(45,48,240,68)					// Frame
						kTrue,								// Visible
						kTrue,								// Enabled
						kAlignLeft,							// Alignment
						kSSSWithoutPageBreakStringKey, //"Without Page Break",							// Initial text
					),
					
					CheckBoxWidget
					(
						kIsAddSectionStencilWidgetID,					// WidgetId
						kSysCheckBoxPMRsrcId,				// RsrcId
						kBindRight | kBindBottom,
						Frame(320,6,510,26)					// Frame
						kTrue,								// Visible
						kTrue,								// Enabled
						kAlignLeft,							// Alignment
						kAddSectionStencilStringKey, //"Add Section Stencil"							// Initial text
					),
					
					CheckBoxWidget
					(
						kIsAtStartOfSectionWidgetID,					// WidgetId
						kSysCheckBoxPMRsrcId,				// RsrcId
						kBindRight | kBindBottom,
						Frame(340,27,510,47)					// Frame
						kTrue,								// Visible
						kTrue,								// Enabled
						kAlignLeft,							// Alignment
						kAtStartOfSectionStringKey, //"At The Start Of Section"							// Initial text
					),
					
					CheckBoxWidget
					(
						kIsAtStartOfEachPageWidgetID,					// WidgetId
						kSysCheckBoxPMRsrcId,				// RsrcId
						kBindRight | kBindBottom,
						Frame(340,48,510,68)					// Frame
						kTrue,								// Visible
						kTrue,								// Enabled
						kAlignLeft,							// Alignment
						kAtStartOfEachPageStringKey, //"At The Start Of Each Page"							// Initial text
					),
					
					CheckBoxWidget
					(
						kIsAtStartOfFirstPageWidgetID,					// WidgetId
						kSysCheckBoxPMRsrcId,				// RsrcId
						kBindRight | kBindBottom,
						Frame(340,69,560,89)					// Frame
						kTrue,								// Visible
						kTrue,								// Enabled
						kAlignLeft,							// Alignment
						kAtStartOfFirstPageOfSpreadStringKey, //"At The Start Of First Page Of Spread"		// Initial text
					),
					
//////////////////////////
					
					
					
					CheckBoxWidget
					(
						kIsHorizontalFlowForAllImageSprayWidgetID,					// WidgetId
						kSysCheckBoxPMRsrcId,				// RsrcId
						kBindRight | kBindBottom,
						Frame(20,81,240,101)//Frame(20,48.0,260,68)						// Frame
						kTrue,								// Visible
						kTrue,								// Enabled
						kAlignLeft,							// Alignment
						kHorizontalFlowForAllImageSpray, //"Horizontal Flow For All Image Spray "							// Initial text
					),
					
				}
			),
			
		*/	

//////////Code Added By Dattatray For ListBox on 25/07/2006///////////////
/*
		GroupPanelWidget
		(
			kFourthGroupPanelWidgetID,							// widget ID
			kPMRsrcID_None,								// PMRsrc ID
			kBindNone,									// frame binding
		//	Frame(3.0,204.0,435.0,298.0)				// left, top, right, bottom	//Amit
			Frame(3,151,584,241)						// left, top, right, bottom
			kTrue,										// visible
			kTrue,										// enabled
			0,											// header widget ID
			{ 
				InfoStaticTextWidget
				(
														// CControlView properties
					kInvalidWidgetID,					// widget ID
					kSysStaticTextPMRsrcId,				// PMRsrc ID
					kBindNone,							// frame binding
					//Frame(5.0,-17.0,55.0,28.0)	
				//	Frame(15.0,-5,75.0, 5.0)			// left, top, right, bottom
					Frame(264,-5,324, 5)			// left, top, right, bottom
					kTrue,								// visible
					kTrue,								// enabled
					kAlignLeft,							// Alignment
					kDontEllipsize,kTrue,						// Ellipsize style
					kSelectStencilStringKey, //"Select Stencils", // control label
					kInvalidWidgetID,					// associated widget ID
					kPaletteWindowSystemScriptFontId,	// fFontID
					kPaletteWindowSystemScriptFontId,	// fHiliteFontID
				),

				RollOverIconButtonWidget
				(
					kProductButtonWidgetID, 
					//kNextIconID, 
					kPNGProductIconRsrcID
					kSSSPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(30,5.0,110,27.0)	                    //Frame(171+2 , 85 , 200 , 108 )
					kTrue, kTrue,
					kADBEIconSuiteButtonType,
				),	
				
				RollOverIconButtonWidget
				(
					kItemButtonWidgetID, 
					//kNextIconID, 
					kPNGItemIconRsrcID
					kSSSPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(170+10,5.0,260,27.0)	                    //Frame(171+2 , 85 , 200 , 108 )
					kTrue, kTrue,
					kADBEIconSuiteButtonType,
				),	
				
				RollOverIconButtonWidget
				(
					kHybridButtonWidgetID, 
					//kNextIconID, 
					kPNGTableIconRsrcID
					kSSSPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(330,5.0,410,27.0)	                    //Frame(171+2 , 85 , 200 , 108 )
					kTrue, kTrue,
					kADBEIconSuiteButtonType,
				),	
				
				RollOverIconButtonWidget
				(
					kSectionButtonWidgetID, 
					//kNextIconID, 
					kPNGSectionIconRsrcID
					kSSSPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(471,5.0,551,27.0)	                    //Frame(171+2 , 85 , 200 , 108 )
					kTrue, kTrue,
					kADBEIconSuiteButtonType,
				),	
*/

/////////////////////For ListBox on 25/07/2006/////////////////////////////////				
/*				WidgetListBoxWidgetN
				(
					kSSSListBoxWidgetID, kSysOwnerDrawListBoxPMRsrcId,	// WidgetId, RsrcId
					kBindAll,											// Frame binding
					Frame(1,30,140,93)		// Frame
					kTrue, kTrue,									// Visible, Enabled
					1,0,												// List dimensions
					19,													// Cell height
					1,													// Border width
					kFalse,kTrue,									// Has scroll bar (h,v)
					kTrue,												// Multiselection
					kTrue,												// List items can be reordered
					kTrue,												// Draggable to new/delete buttons
					kFalse,												// Drag/Dropable to other windows
					kTrue,												// An item always has to be selected
					kFalse,												// Don't notify on reselect
					kSSSListElementRsrcID								// Fill list box with widgets with this ID (default is 0)
					{
						CellPanelWidget
						(
							kCellPanelWidgetID, kPMRsrcID_None,			// WidgetId, RsrcId
							kBindAll,									// Frame binding
							Frame(0,0,135,165)		// Frame
							kTrue, kTrue								// Visible, Enabled
							{
								// ----- This is the CPanelControlData that holds the widgets
								//		 that are items in the list box. They are not persistent
								//		 and so are not specified as part of the resource. [amb]
							}
						)
					},
				),
/////////////////////For ListBox2 on 25/07/2006/////////////////////////////////				
				WidgetListBoxWidgetN
				(
					kSSSListBoxTwoWidgetID, kSysOwnerDrawListBoxPMRsrcId,	// WidgetId, RsrcId
					kBindAll,											// Frame binding
					Frame(150,30,286,93)		// Frame
					kTrue, kTrue,									// Visible, Enabled
					1,0,												// List dimensions
					19,													// Cell height
					1,													// Border width
					kFalse,kTrue,									// Has scroll bar (h,v)
					kTrue,												// Multiselection
					kTrue,												// List items can be reordered
					kTrue,												// Draggable to new/delete buttons
					kFalse,												// Drag/Dropable to other windows
					kTrue,												// An item always has to be selected
					kFalse,												// Don't notify on reselect
					kSSSListElementRsrcID								// Fill list box with widgets with this ID (default is 0)
					{
						CellPanelWidget
						(
							kCellPanelWidgetID, kPMRsrcID_None,			// WidgetId, RsrcId
							kBindAll,									// Frame binding
							Frame(0,0,123,87)		// Frame
							kTrue, kTrue								// Visible, Enabled
							{
								// ----- This is the CPanelControlData that holds the widgets
								//		 that are items in the list box. They are not persistent
								//		 and so are not specified as part of the resource. [amb]
							}
						)
					},
				),
				
////////////////////////ListBox2 end.		
/////////////////////// ListBox3 Start	Amit
				WidgetListBoxWidgetN
				(
					kSSSListBoxThreeWidgetID, kSysOwnerDrawListBoxPMRsrcId,	// WidgetId, RsrcId
					kBindAll,											// Frame binding
					Frame(296,30,432,93)		// Frame
					kTrue, kTrue,									// Visible, Enabled
					1,0,												// List dimensions
					19,													// Cell height
					1,													// Border width
					kFalse,kTrue,									// Has scroll bar (h,v)
					kTrue,												// Multiselection
					kTrue,												// List items can be reordered
					kTrue,												// Draggable to new/delete buttons
					kFalse,												// Drag/Dropable to other windows
					kTrue,												// An item always has to be selected
					kFalse,												// Don't notify on reselect
					kSSSListElementRsrcID								// Fill list box with widgets with this																			//ID (default is 0)
					{
						CellPanelWidget
						(
							kCellPanelWidgetID, kPMRsrcID_None,			// WidgetId, RsrcId
							kBindAll,									// Frame binding
							Frame(0,0,123,87)		// Frame
							kTrue, kTrue								// Visible, Enabled
							{
								// ----- This is the CPanelControlData that holds the widgets
								//		 that are items in the list box. They are not persistent
								//		 and so are not specified as part of the resource. [amb]
							}
						)
					},
				),
////////////////////////	ListBox3 End	Amit
/////////////////////// ListBox4 Start	Amit
				WidgetListBoxWidgetN
				(
					kSSSListBoxFourWidgetID, kSysOwnerDrawListBoxPMRsrcId,	// WidgetId, RsrcId
					kBindAll,												// Frame binding
					Frame(441,30,577,93)									// Frame 
					kTrue, kTrue,										// Visible, Enabled
					1,0,												// List dimensions
					19,													// Cell height
					1,													// Border width
					kFalse,kTrue,									// Has scroll bar (h,v)
					kTrue,												// Multiselection
					kTrue,												// List items can be reordered
					kTrue,												// Draggable to new/delete buttons
					kFalse,												// Drag/Dropable to other windows
					kTrue,												// An item always has to be selected
					kFalse,												// Don't notify on reselect
					kSSSListElementRsrcID								// Fill list box with widgets with this																			//ID (default is 0)
					{
						CellPanelWidget
						(
							kCellPanelWidgetID, kPMRsrcID_None,			// WidgetId, RsrcId
							kBindAll,									// Frame binding
							Frame(0,0,123,87)		// Frame
							kTrue, kTrue								// Visible, Enabled
							{
								// ----- This is the CPanelControlData that holds the widgets
								//		 that are items in the list box. They are not persistent
								//		 and so are not specified as part of the resource. [amb]
							}
						)
					},
				),
			*/

/*
			}
		),
*/
		}// First Generic Panel
	),//

    CheckBoxWidget
    (
    kIsAllSectionSprayWidgetID,					// WidgetId
    kSysCheckBoxPMRsrcId,				// RsrcId
    kBindRight | kBindBottom,
    Frame(40,148,270,168)						// Frame
    kTrue,								// Visible
    kTrue,								// Enabled
    kAlignLeft,							// Alignment
    kSSSEventStringKey, //"Event",//"Spray All Sections At The Current Level,"// commented By Amarjit on 11 nov
    ),

	DefaultButtonWidget
	(
		// CControlView properties
		kOKButtonWidgetID, // widget ID
		kSysButtonPMRsrcId, // PMRsrc ID
		kBindNone, // frame binding
	//	Frame(145.0,357.0,215.0,382.0)//  left, top, right, bottom
	//	Frame(145.0,396.0,215.0,423.0)//  left, top, right, bottom 
	//  Frame(217,396,287,423)//  left, top, right, bottom  
		Frame(0,0,1,1)
		kTrue, // visible
		kTrue, // enabled
		// CTextControlData properties
		kSDKDefOKButtonApplicationKey, // control label
	),
	CancelButtonWidget
	(
		// CControlView properties
		kCancelButton_WidgetID, // widget ID
		kSysButtonPMRsrcId, // PMRsrc ID
		kBindNone, // frame binding
		//Frame(225.0,357.0,295.0,382.0)	//  left, top, right, bottom
		//Frame(225.0,396.0,295.0,423.0)	//  left, top, right, bottom
		//Frame(297,396,367,423)	//  left, top, right, bottom
		Frame(0,0,1,1)
		kTrue, // visible
		kTrue, // enabled
		// CTextControlData properties
		kSDKDefCancelButtonApplicationKey, // control label
		// CIntegerControlData properties
		kFalse, //kTrue, // current value
	),

	RollOverIconButtonWidget
	(
		kSectionSprayOKButtonWidgetID, 
		//kNextIconID, 
		kPNGSectionSprayOKIconRsrcID
		kSSSPluginID,									// WidgetId, RsrcId
		kBindNone,										// Frame binding
		//Frame(217,396,297,418)	                    //Frame(171+2 , 85 , 200 , 108 )
		Frame(55,183,135,205)	                    //Frame(171+2 , 85 , 200 , 108 )
		kTrue, kTrue,
		kADBEIconSuiteButtonType,
	),	
	
	RollOverIconButtonWidget
	(
		kSectionSprayCancelButtonWidgetID, 
		//kNextIconID, 
		kPNGSectionSprayCancelIconRsrcID
		kSSSPluginID,									// WidgetId, RsrcId
		kBindNone,										// Frame binding
		//Frame(317,396,397,418)	                    //Frame(171+2 , 85 , 200 , 108 )
		Frame(170,183,250,205)	                    //Frame(171+2 , 85 , 200 , 108 )
		kTrue, kTrue,
		kADBEIconSuiteButtonType,
	),	

  },

	//kEVEArrangeChildrenInRow | kEVESmallMargin,
};

////////////////Code Added By Dattatray to add ListBox///////////////////////////////////////
resource PrimaryResourcePanelWidget (kSSSListElementRsrcID + index_enUS)
{
	__FILE__, __LINE__,
	kSSSListParentWidgetID, 0,	// WidgetId, RsrcId
	kSSSPluginID,	
	kBindLeft | kBindRight,			// Frame binding
	Frame(0, 0, 194, 19),			// Frame
	kTrue, kTrue,					// Visible, Enabled
	"",								// Panel name
	{
		InfoStaticTextWidget
		(
			kSSSTextWidgetID, kPMRsrcID_None,		// WidgetId, RsrcId
			kBindLeft | kBindRight,								// Frame binding
			Frame(3,0,194,19)
			//Frame(3.0,152.0,290.0,247.0)											// Frame
			kTrue, kTrue, kAlignLeft,kEllipsizeEnd,kTrue,				// Visible, Enabled, Ellipsize style
			"",													// Initial text
			0,													// Associated widget for focus
			kPaletteWindowSystemScriptFontId,					// default font 
			kPaletteWindowSystemScriptHiliteFontId,				// for highlight state.
		),
		
	}
};
////////////////////////////////////////////////////////////upto here........


#endif // __ODFRC__

#include "SSS_enUS.fr"
#include "SSS_jaJP.fr"
