//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IActiveContext.h"
// General includes:
#include "CDialogController.h"
// Project includes:
#include "SSSID.h"
// Interface includes:
// none.

// General includes:
#include "CDialogController.h"
#include "SystemUtils.h"

// Project includes:

# include "IPanelControlData.h"
#include "IDropDownListController.h"
#include "IDialogController.h"
#include "ISubSectionSprayer.h"
#include "ITriStateControlData.h"
#include "Mediator.h"
#include "UIDRef.h"
#include "CAlert.h"
#include "SDKListBoxHelper.h"
#include "UIDList.h"
#include "OMTypes.h"
#include <vector>
#include "IAppFramework.h"
#include "IClientOptions.h"


#include "ISelectionManager.h"
#include "IGroupItemSuite.h"
#include "Utils.h"
#include "ISelectionUtils.h"
#include "ILoginHelper.h"


double CurrLanguageID=-1;
#define CA(x) CAlert::InformationAlert(x)
using namespace std;

typedef vector<UID> PageUIDList;

extern vector<PMString> dataString;
extern UIDList productSelUIDList;
extern UIDList itemSelUIDList;
extern UIDList hybridSelUIDList;
extern UIDList sectionSelUIDList;
extern PageUIDList pageUidList;
UIDList originalBoxUIDList;

extern bool16 isSingleSprayFlag;

/** SSSDialogController
	Methods allow for the initialization, validation, and application of dialog widget
	values.
	Implements IDialogController based on the partial implementation CDialogController.

	
	@ingroup apjs10_subsectionsprayer
*/
class SSSDialogController : public CDialogController
{
	public:
		/** Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		SSSDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** Destructor.
		*/
		virtual ~SSSDialogController() {}

		/** Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
	       virtual void InitializeDialogFields(IActiveContext* dlgContext);

		/** Validate the values in the widgets.
			By default, the widget with ID kOKButtonWidgetID causes
			ValidateFields to be called. When all widgets are valid,
			ApplyFields will be called.
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
	       virtual WidgetID ValidateDialogFields(IActiveContext* myContext);


		/** Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId);



		virtual void UserCancelled();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(SSSDialogController,kSSSDialogControllerImpl)

/* ApplyFields
*/
void SSSDialogController::InitializeDialogFields(IActiveContext* ac) 
{
	//CA("SSSDialogController::InitializeDialogFields");
	CDialogController::InitializeDialogFields(ac);

	do
	{
//CA("1");
		// Put code to initialize widget values here.
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
		{
			CAlert::ErrorAlert("Interface for IAppFramework not found.");
			return;
		}
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (!panelControlData)
			break;

		//------
		PMString LocaleName("");
		InterfacePtr<IClientOptions> cop((IClientOptions*) ::CreateObject(kClientOptionsReaderBoss,IID_ICLIENTOPTIONS));
		if(cop==nil)
		{
			//CA("cop==nil");
			return;
		}
		
		CurrLanguageID = cop->getDefaultLocale(LocaleName);
		if(CurrLanguageID == -1)
		{
			//CA("CurrLanguageID == -1");
			return;
		}
        
        InterfacePtr<ISubSectionSprayer> iSSSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
		if(iSSSprayer==nil)
		{
			CA("Plugin Ap_SubSectionSprayer.pln was not found in the plugins directory of adobe.");
			return;
		}
        
        int32 horizontalBoxSpacing = 5;
        int32 verticalBoxSpacing = 5;
        InterfacePtr<ILoginHelper> ptrLogInHelper(static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss,ILoginHelper::kDefaultIID)));
        if(ptrLogInHelper == nil)
        {
            CAlert::InformationAlert("Pointer to ptrLogInHelper is nil.");
            return ;
        }
        
        LoginInfoValue cserverInfoValue;
        bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue);
        if(result)
        {
            ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();
            
            // Added by Prabhat Singh
            //int32 horizontalspace1 = ToInt32(Hspace);
            //clientInfoObj.setHorizontalSpacing(horizontalspace1);
            // till here
            horizontalBoxSpacing = clientInfoObj.getHorizontalSpacing();
            verticalBoxSpacing = clientInfoObj.getVerticalSpacing();
            
            iSSSprayer->setHorizontalBoxSpacing(horizontalBoxSpacing);
            iSSSprayer->setVerticalBoxSpacing(verticalBoxSpacing);
            
        }

		/*	commited by amit
		IControlView* marginsPanelCtrlView=panelControlData->FindWidget(kMarginsPanelWidgetID);
		if(!marginsPanelCtrlView)
			break;
	
		IControlView* flowPanelCtrlView=panelControlData->FindWidget(kFlowPanelWidgetID);
		if(!flowPanelCtrlView)
			break;
	//	IControlView* repeatingDataPanelCtrlView=panelControlData->FindWidget(kRepeatingDataPanelWidgetID);
	//	if(!repeatingDataPanelCtrlView)
	//		break;
		
		flowPanelCtrlView->HideView();
		marginsPanelCtrlView->ShowView();
	//	repeatingDataPanelCtrlView->HideView();

		IControlView* optionCtrlView = panelControlData->FindWidget(kOptionsDropDownWidgetID);
		if(optionCtrlView == nil)
			break;

		InterfacePtr<IDropDownListController> iDropDownCtrl(optionCtrlView, UseDefaultIID());
		if(iDropDownCtrl==nil)
			break;

		iDropDownCtrl->Select(0);
*/
		

		InterfacePtr<IDialogController> dlgController(this,IID_IDIALOGCONTROLLER);
		if(dlgController==nil)
			break;
		//dlgController->SetTextValue(kTopMarginWidgetID, iSSSprayer->getTopMargin(), panelControlData);
		//dlgController->SetTextValue(kBottomMarginWidgetID, iSSSprayer->getBottomMargin(), panelControlData);
		//dlgController->SetTextValue(kLeftMarginWidgetID, iSSSprayer->getLeftMargin(), panelControlData);
		//dlgController->SetTextValue(kRightMarginWidgetID, iSSSprayer->getRightMargin(), panelControlData);
		dlgController->SetTextValue(kHorizontalSpacingWidgetID, iSSSprayer->getHorizontalBoxSpacing(), panelControlData);
		dlgController->SetTextValue(kVerticalSpacingWidgetID, iSSSprayer->getVerticalBoxSpacing(), panelControlData);
				
		
		IControlView *firstGroupPanel = panelControlData->FindWidget(kFirstGroupPanelWidgetID1);
		if(firstGroupPanel == nil)
		{
			//CA("firstGroupPanel == nil");
			ptrIAppFramework->LogError("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::No firstGroupPanel");				
			return ;
		}
		IControlView *secondGroupPanel = panelControlData->FindWidget(kSecondGroupPanelWidgetID1);
		if(secondGroupPanel == nil)
		{
			//CA("secondGroupPanel == nil");
			ptrIAppFramework->LogError("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::No secondGroupPanel");				
			return ;
		}
		//IControlView *thirdGroupPanel = panelControlData->FindWidget(kThirdGroupPanelWidgetID);
		//if(thirdGroupPanel == nil)
		//{
		//	//CA("thirdGroupPanel == nil");
		//	ptrIAppFramework->LogError("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::No thirdGroupPanel");				
		//	return ;
		//}
		//IControlView *fourthGroupPanel = panelControlData->FindWidget(kFourthGroupPanelWidgetID);
		//if(fourthGroupPanel == nil)
		//{
		//	//CA("fourthGroupPanel == nil");
		//	ptrIAppFramework->LogError("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::No fourthGroupPanel");				
		//	return ;
		//}

		secondGroupPanel->ShowView ();
		//thirdGroupPanel->ShowView ();
		//fourthGroupPanel->ShowView ();
		
		
		
		dlgController->SetTriStateControlData(kSSSVerticalFlowRadioButtonWidgetID,iSSSprayer->getVerticalFlow(), nil, kTrue, kFalse);
		//dlgController->SetTriStateControlData(kSSSAltVerticalFLowRadioButtonWidgetID, iSSSprayer->getAltVerticalFlow(), nil, kTrue, kFalse);
		dlgController->SetTriStateControlData(kSSSAltHorzintalFlowRadioButtonWidgetID, iSSSprayer->getAltHorizontalFlow(), nil, kTrue, kFalse);
		//dlgController->SetTriStateControlData(kSSSHorzintalFlowRadioButtonWidgetID, iSSSprayer->getHorizontalFlow(), nil, kTrue, kFalse);


		dlgController->SetTriStateControlData(kIsAllSectionSprayWidgetID, iSSSprayer->getSprayAllSectionsFlag(), nil, kTrue, kFalse);
				//added on 8Nov..
		//IControlView * altVerticalFLowRadioButtonControlView = panelControlData->FindWidget(kSSSAltVerticalFLowRadioButtonWidgetID);
		//IControlView * altHorzintalFlowRadioButtonControlView = panelControlData->FindWidget(kSSSAltHorzintalFlowRadioButtonWidgetID);	
		
		//-------
		//InterfacePtr<ILoginHelper> ptrLogInHelper(static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss,ILoginHelper::kDefaultIID)));
		//if(ptrLogInHelper == nil)
		//{
		//	//CAlert::InformationAlert("Pointer to ptrLogInHelper is nil.");
		//	return ;
		//}
		//CServerInfoValue cserverInfoValue;
		//bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue);

		//if(iSSSprayer->getSprayCustomProductOrItemListFlag() && !iSSSprayer->getIsSectionSprayFlag())
		//{
		//	IControlView * isAllSectionCheckBoxControlView = panelControlData->FindWidget(kIsAllSectionSprayWidgetID);	
		//	IControlView * isAllSectionCheckBoxForSecondLineControlView = panelControlData->FindWidget(kIsAllSectionSpraySecondLineWidgetID);
		//	isAllSectionCheckBoxControlView->Disable();
		//	isAllSectionCheckBoxForSecondLineControlView->Disable();

		//	
		//			
		//	altVerticalFLowRadioButtonControlView->Disable();
		//	altHorzintalFlowRadioButtonControlView->Disable();

		//	int32 def_Flow = 0; //cserverInfoValue.getdefaultFlow();
		//	if(def_Flow == 1)
		//	{
		//		iSSSprayer->setVerticalFlow(kFalse);
		//		iSSSprayer->setHorizontalFlow(kTrue);
		//		dlgController->SetTriStateControlData(kSSSHorzintalFlowRadioButtonWidgetID, iSSSprayer->getHorizontalFlow(), nil, kTrue, kFalse);
		//		dlgController->SetTriStateControlData(kSSSVerticalFlowRadioButtonWidgetID,iSSSprayer->getVerticalFlow(), nil, kTrue, kFalse);

		//	}
		//	else
		//	{
		//		iSSSprayer->setVerticalFlow(kTrue);
		//		iSSSprayer->setHorizontalFlow(kFalse);
		//		dlgController->SetTriStateControlData(kSSSHorzintalFlowRadioButtonWidgetID, iSSSprayer->getHorizontalFlow(), nil, kTrue, kFalse);
		//		dlgController->SetTriStateControlData(kSSSVerticalFlowRadioButtonWidgetID,iSSSprayer->getVerticalFlow(), nil, kTrue, kFalse);

		//	}
		//	
		//}
		//else if(iSSSprayer->getSprayCustomProductOrItemListFlag() && iSSSprayer->getIsSectionSprayFlag())
		//{
		//	IControlView * isAllSectionCheckBoxControlView = panelControlData->FindWidget(kIsAllSectionSprayWidgetID);	
		//	IControlView * isAllSectionCheckBoxForSecondLineControlView = panelControlData->FindWidget(kIsAllSectionSpraySecondLineWidgetID);
		//	isAllSectionCheckBoxControlView->Enable();
		//	isAllSectionCheckBoxForSecondLineControlView->Enable();

		//	/*altVerticalFLowRadioButtonControlView->Enable();
		//	altHorzintalFlowRadioButtonControlView->Enable();*/
		//	altVerticalFLowRadioButtonControlView->Disable();
		//	altHorzintalFlowRadioButtonControlView->Disable();

		//	iSSSprayer->setVerticalFlow(kTrue);
		//	iSSSprayer->setHorizontalFlow(kFalse);
		//	dlgController->SetTriStateControlData(kSSSHorzintalFlowRadioButtonWidgetID, iSSSprayer->getHorizontalFlow(), nil, kTrue, kFalse);
		//	dlgController->SetTriStateControlData(kSSSVerticalFlowRadioButtonWidgetID,iSSSprayer->getVerticalFlow(), nil, kTrue, kFalse);


		//}
		
	/*	bool16 state = dlgController->GetTriStateControlData(kIsAllSectionSprayWidgetID);
		IControlView * IsWithoutPageBreakCheckBoxControlView = panelControlData->FindWidget(kIsWithoutPageBreakWidgetID);
		if(state)
			IsWithoutPageBreakCheckBoxControlView->Enable();
		else
			IsWithoutPageBreakCheckBoxControlView->Disable();
		
		bool16 state1 = dlgController->GetTriStateControlData(kIsAddSectionStencilWidgetID);
		IControlView * IsAtStartOfSectionCheckBoxControlView = panelControlData->FindWidget(kIsAtStartOfSectionWidgetID);
		IControlView * IsAtStartOfEachPageCheckBoxControlView = panelControlData->FindWidget(kIsAtStartOfEachPageWidgetID);
		IControlView * IsAtStartOfFirstPageCheckBoxControlView = panelControlData->FindWidget(kIsAtStartOfFirstPageWidgetID);
		
		if(state1)
		{
			IsAtStartOfSectionCheckBoxControlView->Enable();
			IsAtStartOfEachPageCheckBoxControlView->Enable();
			IsAtStartOfFirstPageCheckBoxControlView->Enable();
		}
		else
		{
			IsAtStartOfSectionCheckBoxControlView->Disable();
			IsAtStartOfEachPageCheckBoxControlView->Disable();
			IsAtStartOfFirstPageCheckBoxControlView->Disable();
		}*/
//////////////	End
		Mediator::hasUserCancelled = kFalse;

		
		productSelUIDList.Clear();

		

		itemSelUIDList.Clear();

		

		hybridSelUIDList.Clear();
		
		

		sectionSelUIDList.Clear();

		originalBoxUIDList.Clear();
//CA("4");
		do
		{
			 UIDRef originalPageUIDRef=UIDRef::gNull;
			 UIDRef originalSpreadUIDRef = UIDRef::gNull;
			 bool16 result=kFalse;	

			 pageUidList.clear();

			 InterfacePtr<ISubSectionSprayer> iSubSectSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
			 if(!iSubSectSprayer){
				//CA("No SubSectSprayer");
				break;
			}
		 //	//***Added
			//InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
			//if(!iSelectionManager)
			//	break;
			//InterfacePtr<IGroupItemSuite> igpItem(iSelectionManager,UseDefaultIID());
			//if(!igpItem){
			//	//CA("Not Found");	
			//	break;
			//}
			//if(igpItem->CanItemsBeUngrouped()){
			//	ErrorCode  sucess = igpItem->UngroupItems();					
			//}
			////**** Up To Here

			result=iSubSectSprayer->getCurrentPage(originalPageUIDRef,originalSpreadUIDRef);
			if(result == kFalse) 
				break;
			int32 PageCount=0;
			PageCount=1;
			UID originalPageUID = originalPageUIDRef.GetUID();
			pageUidList.push_back(originalPageUID);

			//get the list of selected box UIDRef's
			originalBoxUIDList.Clear();
			result = iSubSectSprayer->getSelectedBoxIds(originalBoxUIDList);
//CA("5");
			if(result)  
			{
				//SDKListBoxHelper lstHelper(this,kSSSPluginID,kSSSListBoxWidgetID,kSSSDialogWidgetID);			
				//lstHelper.EmptyCurrentListBox();

				//for(int i = static_cast<int>(dataString.size()-1);i>=0;i--)
				//	lstHelper.AddElement(dataString[i],kSSSTextWidgetID,0);
				
				productSelUIDList = originalBoxUIDList;


				//All the Selected tags are populated in Item's ListBox.
				//SDKListBoxHelper lstHelper1(this,kSSSPluginID,kSSSListBoxTwoWidgetID,kSSSDialogWidgetID);			
				//lstHelper1.EmptyCurrentListBox();

				//for(int i =static_cast<int>(dataString.size()-1);i>=0;i--)
				//	lstHelper1.AddElement(dataString[i],kSSSTextWidgetID,0);

				itemSelUIDList = originalBoxUIDList;
//CA("6");
				//SDKListBoxHelper lstHelper2(this,kSSSPluginID,kSSSListBoxThreeWidgetID,kSSSDialogWidgetID);			
				//lstHelper2.EmptyCurrentListBox();

				//for(int i =static_cast<int>(dataString.size()-1);i>=0;i--)
				//	lstHelper2.AddElement(dataString[i],kSSSTextWidgetID,0);
				
				hybridSelUIDList = originalBoxUIDList;
			
				//SDKListBoxHelper lstHelper3(this,kSSSPluginID,kSSSListBoxFourWidgetID,kSSSDialogWidgetID);			
				//lstHelper3.EmptyCurrentListBox();

				//for(int i =static_cast<int>(dataString.size()-1);i>=0;i--)
				//	lstHelper3.AddElement(dataString[i],kSSSTextWidgetID,0);
				
				sectionSelUIDList = originalBoxUIDList;
//CA("7");
				dataString.clear();
			}
			
			PMString textValue("Spacing between ");
			textValue.SetTranslatable(kFalse);
			//textValue.Append(ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename("Products"),CurrLanguageID));
			textValue.Append("Item Group");

			textValue.Append("/");
			//textValue.Append(ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename("Items"),CurrLanguageID));
			textValue.Append("Items");
			dlgController->SetTextControlData(kItemGroupItemWidgetID,textValue,panelControlData,kTrue,kTrue);
			
			PMString textItem("Spray ");
			textItem.SetTranslatable(kFalse);
			//textItem.Append(ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename("Item"),CurrLanguageID));
			textValue.Append("Item");
			textItem.Append(" Per Frame");
			//CA("KKKKKKKKKKKKKKKKKK");
			textItem.SetTranslatable(kFalse);
			dlgController->SetTextControlData(kSprayItemWidgetID,textItem,panelControlData,kTrue,kTrue);
			
		
			if(isSingleSprayFlag == kTrue)
			{
				//CA("isSingleSprayFlag == kTrue");
				secondGroupPanel->HideView();
				//thirdGroupPanel->HideView();
				//fourthGroupPanel->HideView();
			}
			
		}while(0);
	}while(kFalse);
}

/* ValidateFields
*/
WidgetID SSSDialogController::ValidateDialogFields(IActiveContext* ac)
{
	WidgetID result = CDialogController::ValidateDialogFields(ac);

	// Put code to validate widget values here.

	return result;
}

/* ApplyFields
*/
void SSSDialogController::ApplyDialogFields(IActiveContext* ac, const WidgetID& widgetId)
{
	//CA("in applydialogfields");
	// Replace with code that gathers widget values and applies them.
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (!panelControlData)
			break;

		InterfacePtr<IDialogController> dlgController(this,IID_IDIALOGCONTROLLER);
		if(dlgController==nil)
			break;
		
		InterfacePtr<ISubSectionSprayer> iSSSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
		//InterfacePtr<ISubSectionSprayer> iSSSprayer(this,IID_ISUBSECTIONSPRAYER);
		if(iSSSprayer==nil)
		{
			CA("Plugin Ap_SubSectionSprayer.pln was not found in the plugins directory of adobe.");
			return;
		}

		PMString value;
		/*value = dlgController->GetTextControlData(kTopMarginWidgetID, panelControlData);
		if(value != "")
		{
			CA("kTopMarginWidgetID");
			PMReal topMarginReal = dlgController->GetTextValue(kTopMarginWidgetID, panelControlData);
			iSSSprayer->setTopMargin(topMarginReal);
		}
		
		
		value = dlgController->GetTextControlData(kBottomMarginWidgetID, panelControlData);
		if(value != "")
		{
			PMReal bottomMarginReal = dlgController->GetTextValue(kBottomMarginWidgetID, panelControlData);
			iSSSprayer->setBottomMargin(bottomMarginReal);
		}
		
		value = dlgController->GetTextControlData(kLeftMarginWidgetID, panelControlData);
		if(value != "")
		{
			PMReal leftMarginReal = dlgController->GetTextValue(kLeftMarginWidgetID, panelControlData);
			iSSSprayer->setLeftMargin(leftMarginReal);
		}
		
		value = dlgController->GetTextControlData(kRightMarginWidgetID, panelControlData);
		if(value != "")
		{
			PMReal rightMarginReal = dlgController->GetTextValue(kRightMarginWidgetID, panelControlData);
			iSSSprayer->setRightMargin(rightMarginReal);
		}
		*/
		value = dlgController->GetTextControlData(kHorizontalSpacingWidgetID, panelControlData);
		if(value != "")
		{
			PMReal horizontalSpacingReal = dlgController->GetTextValue(kHorizontalSpacingWidgetID, panelControlData);
			iSSSprayer->setHorizontalBoxSpacing(horizontalSpacingReal);
		}
				
		value = dlgController->GetTextControlData(kVerticalSpacingWidgetID, panelControlData);
		if(value != "")
		{
			PMReal verticalSpacingReal = dlgController->GetTextValue(kVerticalSpacingWidgetID, panelControlData);
			iSSSprayer->setVerticalBoxSpacing(verticalSpacingReal);
		}
		
		ITriStateControlData::TriState theSprayAllSectionsFlafgState= dlgController->GetTriStateControlData(kIsAllSectionSprayWidgetID, panelControlData);
		if(theSprayAllSectionsFlafgState == ITriStateControlData::kSelected)
			iSSSprayer->setSprayAllSectionsFlag(kTrue);
		else
			iSSSprayer->setSprayAllSectionsFlag(kFalse);

		//ITriStateControlData::TriState theLToRState = dlgController->GetTriStateControlData(kLeftToRightWidgetID, panelControlData);
		////ITriStateControlData::TriState theRToLState= dlgController->GetTriStateControlData(kRightToLeftWidgetID, panelControlData);
		//if(theLToRState == ITriStateControlData::kSelected)
		//	iSSSprayer->setLeftToRightVal(kTrue);
		//else
		//	iSSSprayer->setLeftToRightVal(kFalse);

		//ITriStateControlData::TriState theAlternateState= dlgController->GetTriStateControlData(kAlternateValueWidgetID, panelControlData);
		//if(theAlternateState == ITriStateControlData::kSelected)
		//	iSSSprayer->setAlternatingVal(kTrue);
		//else
		//	iSSSprayer->setAlternatingVal(kFalse);

		//ITriStateControlData::TriState theTToBState = dlgController->GetTriStateControlData(kTopToBottomWidgetID, panelControlData);
		////ITriStateControlData::TriState theRToLState= dlgController->GetTriStateControlData(kRightToLeftWidgetID, panelControlData);
		//if(theTToBState == ITriStateControlData::kSelected)
		//	iSSSprayer->setTopToBottomVal(kTrue);
		//else
		//	iSSSprayer->setTopToBottomVal(kFalse);

		//ITriStateControlData::TriState theHorizFlowState = dlgController->GetTriStateControlData(kHorizontalFlowWidgetID, panelControlData);
		////ITriStateControlData::TriState theRToLState= dlgController->GetTriStateControlData(kRightToLeftWidgetID, panelControlData);
		//if(theHorizFlowState == ITriStateControlData::kSelected)
		//	iSSSprayer->setHorizFlowType(kTrue);
		//else
		//	iSSSprayer->setHorizFlowType(kFalse);


		bool16 AltVerticalFlowFlag = kFalse;
		bool16 VerticalFlowFlag = kFalse;
		bool16 AltHorizontalFlowFlag = kFalse;
		bool16 HorizontalFLowFlag = kFalse;

		ITriStateControlData::TriState AltVerticalFlowState = dlgController->GetTriStateControlData(kSSSAltVerticalFLowRadioButtonWidgetID, panelControlData);
		if(AltVerticalFlowState == ITriStateControlData::kSelected)
		{
			AltVerticalFlowFlag = kTrue;
			iSSSprayer->setAltVerticalFlow(kTrue);
		}
		else
		{
			AltVerticalFlowFlag = kFalse;
			iSSSprayer->setAltVerticalFlow(kFalse);
		}

		ITriStateControlData::TriState VerticalFlowState = dlgController->GetTriStateControlData(kSSSVerticalFlowRadioButtonWidgetID, panelControlData);
		if(VerticalFlowState == ITriStateControlData::kSelected)
		{
			VerticalFlowFlag = kTrue;
			iSSSprayer->setVerticalFlow(kTrue);
		}
		else
		{
			VerticalFlowFlag = kFalse;
			iSSSprayer->setVerticalFlow(kFalse);
		}

		ITriStateControlData::TriState AltHorizontalFlowState = dlgController->GetTriStateControlData(kSSSAltHorzintalFlowRadioButtonWidgetID, panelControlData);
		if(AltHorizontalFlowState == ITriStateControlData::kSelected)
		{
			AltHorizontalFlowFlag = kTrue;
			iSSSprayer->setAltHorizontalFlow(kTrue);
		}
		else
		{
			AltHorizontalFlowFlag = kFalse;
			iSSSprayer->setAltHorizontalFlow(kFalse);
		}

		ITriStateControlData::TriState HorizontalFlowState = dlgController->GetTriStateControlData(kSSSHorzintalFlowRadioButtonWidgetID, panelControlData);
		if(HorizontalFlowState == ITriStateControlData::kSelected)
		{
			HorizontalFLowFlag = kTrue;
			iSSSprayer->setHorizontalFlow(kTrue);
		}
		else
		{
			HorizontalFLowFlag = kFalse;
			iSSSprayer->setHorizontalFlow(kFalse);
		}

		if(AltVerticalFlowFlag)
		{
			iSSSprayer->setLeftToRightVal(kTrue);    
			iSSSprayer->setAlternatingVal(kTrue);    
			iSSSprayer->setHorizFlowType(kFalse);    
			iSSSprayer->setTopToBottomVal(kTrue);
		}
		else if(VerticalFlowFlag)
		{
			iSSSprayer->setLeftToRightVal(kTrue);    
			iSSSprayer->setAlternatingVal(kFalse);    
			iSSSprayer->setHorizFlowType(kFalse);    
			iSSSprayer->setTopToBottomVal(kTrue);
		}
		else if(AltHorizontalFlowFlag)
		{
			iSSSprayer->setLeftToRightVal(kTrue);    
			iSSSprayer->setAlternatingVal(kTrue);    
			iSSSprayer->setHorizFlowType(kTrue);    
			iSSSprayer->setTopToBottomVal(kTrue);
		}
		else if(HorizontalFLowFlag)
		{
			iSSSprayer->setLeftToRightVal(kTrue);    
			iSSSprayer->setAlternatingVal(kFalse);    
			iSSSprayer->setHorizFlowType(kTrue);    
			iSSSprayer->setTopToBottomVal(kTrue);
		}

	}
	while(kFalse);

	//SystemBeep();  
}

void SSSDialogController::UserCancelled()
{
	Mediator::hasUserCancelled = kTrue;
}


//  Code generated by DollyXs code generator
