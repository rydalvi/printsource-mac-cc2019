//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifdef __ODFRC__


// Japanese string table is defined here

resource StringTable (kSDKDefStringsResourceID + index_jaJP)
{
        k_jaJP,	// Locale Id
        0,		// Character encoding converter

        {
        	// ----- Menu strings
                kSSSCompanyKey,					kSSSCompanyValue,
                kSSSAboutMenuKey,					kSSSPluginName "[JP]...",
                kSSSPluginsMenuKey,				kSSSPluginName "[JP]",
				kSSSDialogMenuItemKey,			"Show dialog[JP]",

                kSDKDefAboutThisPlugInMenuKey,			kSDKDefAboutThisPlugInMenuValue_jaJP,

                // ----- Command strings

                // ----- Window strings

                // ----- Panel/dialog strings
					kSSSDialogTitleKey,     kSSSPluginName "[JP]",

              // ----- Error strings

                // ----- Misc strings
                kSSSAboutBoxStringKey,			kSSSPluginName " [JP], version " kSSSVersion " by " kSSSAuthor "\n\n" kSDKDefCopyrightStandardValue "\n\n" kSDKDefPartnersStandardValue_jaJP,
                kSSSHorizontalStringKey, "Horizontal Points:",
                kSSSVerticalStringKey, "Vertical Points:",
                kSSSWithoutPageBreakStringKey, "Without Page Break",
                kSSSEventStringKey, "Spray All Subsections", //"Event",
                kPageStringKey, " Page ",   
                kAtStartOfSectionStringKey, "At The Start Of Section",
                kAtStartOfEachPageStringKey, "At The Start Of Each Page",
                kAtStartOfFirstPageOfSpreadStringKey, "At The Start Of First Page Of Spread",
                kHorizontalFlowForAllImageSpray, "Horizontal Flow For All Image Spray ",
                kBkanStringKey, "", 
        }

};

#endif // __ODFRC__
