#include "VCPlugInHeaders.h"	

#include "ILayoutTarget.h"

#include "ITextMiscellanySuite.h"
#include "CmdUtils.h"
//#include "ISpecifier.h"
#include "IPageItemUtils.h" //Cs4
#include "ITextTarget.h"
#include "ITextModel.h"
#include "IFrameList.h"
//#include "ITextFrame.h"
#include "ILayoutUIUtils.h"
#include "IDocument.h"
#include "ITextFocus.h"////can be deleted
#include "CAlert.h"
#include "SSSID.h"

#define CA(X) CAlert::InformationAlert(X)

class TextMiscellanySuiteLayoutCSB : public CPMUnknown<ITextMiscellanySuite>
{
public:
	TextMiscellanySuiteLayoutCSB(IPMUnknown* iBoss);

	virtual	~TextMiscellanySuiteLayoutCSB(void);

	//virtual bool16 GetCurrentSpecifier(ISpecifier* & );

	virtual bool16 GetUidList(UIDList &);
	virtual bool16 GetFrameUIDRef(UIDRef & frameUIDRef);
	virtual bool16 GetCaretPosition(TextIndex &pos);
};
CREATE_PMINTERFACE(TextMiscellanySuiteLayoutCSB, kSSSTextMiscellanySuiteLayoutCSBImpl/*kTextMiscellanySuiteLayoutCSBImpl*/)

TextMiscellanySuiteLayoutCSB::TextMiscellanySuiteLayoutCSB(IPMUnknown* iBoss) :
	CPMUnknown<ITextMiscellanySuite>(iBoss)
{
}

/* Destructor
*/
TextMiscellanySuiteLayoutCSB::~TextMiscellanySuiteLayoutCSB(void)
{
}
//bool16 TextMiscellanySuiteLayoutCSB::GetCurrentSpecifier(ISpecifier * & Xspec)
//{
//	InterfacePtr<ILayoutTarget>		iLayoutTarget(this, UseDefaultIID());
//	const UIDList					selectedItems(iLayoutTarget->GetUIDList(kStripStandoffs));
//	ISpecifier * spec = PageItemUtils::QuerySpecifier(selectedItems);
//	if(spec )
//	{
//		Xspec = spec;
//		return(1);
//	}
//	return(0);
//}
bool16 TextMiscellanySuiteLayoutCSB::GetUidList(UIDList & TempUidList)
{
	InterfacePtr<ILayoutTarget>iLayoutTarget(this, UseDefaultIID());
	if(!iLayoutTarget){ //CA("!iLayoutTarget");
		return kFalse;
	}
	const UIDList	selectedItems(iLayoutTarget->GetUIDList(kStripStandoffs));
	TempUidList = selectedItems;	
	return 1;
}
bool16 TextMiscellanySuiteLayoutCSB::GetFrameUIDRef(UIDRef & frameUIDRef)
{
	InterfacePtr<ITextTarget>txtTarget(this,UseDefaultIID());
	if(!txtTarget){
		//CA("Type Tool Selection not found 11111");
		return kFalse;
	}
	else
	{
		//CA("Text Target in suite is not null");
		InterfacePtr<ITextModel>txtModel(txtTarget->QueryTextModel());
		if(txtModel)
		{

			InterfacePtr<IFrameList>frameList(txtModel->QueryFrameList());
			if(!frameList)
				return kTrue;
//Commented by Nitin
			/*InterfacePtr<ITextFrame>textFrame(frameList->QueryNthFrame(0),UseDefaultIID());
			if(!textFrame)
				return kTrue;*/
//Code Added By Nitin
			
			InterfacePtr<IDocument>currDoc(Utils<ILayoutUIUtils>()->GetFrontDocument(),UseDefaultIID());
			//IDocument *currDoc=Utils<ILayoutUIUtils>()->GetFrontDocument();
			frameUIDRef=UIDRef(currDoc->GetDocWorkSpace().GetDataBase(),frameList->GetNthFrameUID(0));
          
				InterfacePtr<ITextFocus>textFocus(txtTarget->QueryTextFocus()/*,UseDefaultIID()*/);
				if(!textFocus)
				{
					CA("Type Tool Selection not found");				
					//break;
				}
				else
				{	/*				
					PMString ep("End Position: ");
					ep.AppendNumber(textFocus->GetEnd());
					CA(ep);*/
				}
            		 
		}

	}
	return kTrue;
}

bool16 TextMiscellanySuiteLayoutCSB::GetCaretPosition(TextIndex &pos)
{
	bool16 result=kFalse;
	InterfacePtr<ITextTarget>txtTarget(this,UseDefaultIID());
	if(!txtTarget){
		//CA("Type Tool Selection not found");
	}
	else
	{
		RangeData rangData=txtTarget->GetRange();
		pos=rangData.End();
		result=kTrue;
	}
	return result;
}