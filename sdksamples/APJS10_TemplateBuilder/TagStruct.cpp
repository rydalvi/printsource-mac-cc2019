#ifndef __TAGSTRUCT_H__
#define __TAGSTRUCT_H__

#include "VCPluginHeaders.h"
#include "vector"
#include "IIDXMLElement.h"

#define NUM_TAGS_FIELDS 7

using namespace std;

class TagStruct
{
public:
	int32 elementId;
	int32 typeId;
	int32 parentId;
	int16 whichTab;
	int32 reserved1;
	int32 reserved2;
	bool16 isProcessed;
	int32 parentTypeID;

	int32 startIndex;
	int32 endIndex;
	IIDXMLElement *tagPtr;
	int numValidFields;
	bool16 isTablePresent;
	UIDRef curBoxUIDRef;
	TagStruct();
};

typedef vector<TagStruct> TagList;

inline TagStruct::TagStruct()
{
	elementId=-1;
	typeId=-1;
	parentId=-1;
	whichTab=-1;
	reserved1=-1;
	reserved2=-1;
	isProcessed=kFalse;
	parentTypeID=-1;
	startIndex=-1;
	endIndex=-1;
	tagPtr=nil;
	numValidFields=0;
}
#endif