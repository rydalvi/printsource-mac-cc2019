//========================================================================================
//  
//  $File: $
//  
//  Owner: Rahul Dalvi
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"


#include "CAlert.h"

// Project includes:
#include "TPLID.h"
#include "IoverlapProperties.h"
inline PMString numToPMString(int32 num)
{
	PMString x;
	x.AppendNumber(num);
	return x;
}
#define CA(X) CAlert::InformationAlert \
	( \
		PMString("TPLActionComponent.cpp") + PMString("\n") + \
		PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + \
		PMString("\n Message : ")+ X \
	)
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
//end Alert Macros./////////////


class OverlapProperties : public CPMUnknown<IoverlapProperties>
{
	bool16 isOverlapable;
public:
/**
 Constructor.
 @param boss interface ptr from boss object on which this interface is aggregated.
 */
		OverlapProperties(IPMUnknown* boss);
		~OverlapProperties();		
		virtual bool16 IsOverlapable();
		virtual void setOverlapableFlag(bool16 flag);

};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(OverlapProperties, kIoverlapPropertiesImpl)

/* OverlapProperties Constructor
*/
OverlapProperties::OverlapProperties(IPMUnknown* boss)
: CPMUnknown<IoverlapProperties>(boss)
{
	isOverlapable = kFalse;
}

OverlapProperties::~OverlapProperties()
{
	
}
bool16 OverlapProperties::IsOverlapable()
{
	return isOverlapable;
}

void OverlapProperties::setOverlapableFlag(bool16 flag)
{
	isOverlapable = flag;
}

