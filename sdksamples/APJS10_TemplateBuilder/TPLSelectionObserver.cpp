#include "VCPluginHeaders.h"
#include "ISelectionManager.h"
#include "SelectionObserver.h"
#include "TPLID.h"
#include "Trace.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "CAlert.h"
#include "IWidgetParent.h"
#include "ISubject.h"
#include "ISelectableDialogSwitcher.h"
#include "IAppFramework.h"
#include "TPLListboxData.h"
#include "SDKListBoxHelper.h"
#include "IListBoxController.h"
#include "ITextControlData.h"
//#include "IClassificationTree.h"
#include "TPLMediatorClass.h"
#include "IDialogController.h"
#include "TPLCommonFunctions.h"
#include "ITriStateControlData.h"
#include "TPLSelectionObserver.h"
#include "TPLActionComponent.h"
#include "IListBoxController.h"
#include "IEventHandler.h"
#include "IEventDispatcher.h"
#include "IApplication.h"
#include "TPLDataNode.h"
#include "ITreeViewMgr.h"
#include "TPLTreeModel.h"
#include "IClientOptions.h"
#include "ICategoryBrowser.h"
#include "AcquireModalCursor.h"
#include "IContentSprayer.h"
#include "ILoginHelper.h" //17-may
#include "IStringListControlData.h"
#include "ILoginHelper.h"
//#include "PRImageHelper.h"

TPLListboxData PFData;
TPLListboxData PGData;
TPLListboxData PRData;
TPLListboxData ItemData;
TPLListboxData ProjectData;
TPLListboxData CatagoryData;

//bool16 isAddAsDisplayName;
extern int32 Flag1;
extern int32 ProjectRow,PRRow,PGRow,PFRow,ItemRow;
int32 SelectedRowNo;  //---------4 - Item , 3 - Item Group/Family , 0 - Event or Category----
TPLDataNodeList ProjectDataNodeList;
TPLDataNodeList CatagoryDataNodeList;
TPLDataNodeList PFDataNodeList;
TPLDataNodeList PGDataNodeList;
TPLDataNodeList PRDataNodeList;
TPLDataNodeList ITEMDataNodeList;
bool16 IsRefresh = kFalse;
int32 PM_Product_Level;

double CurrentClassID = -1;
bool16 ifCAllFromLanguageDropDown = kFalse;
bool16 ifCallFromLoadPalletDataLangSetting = kFalse;


extern bool16 ISTabbedText;//kTrue;
extern bool16 isAddTableHeader;
extern bool16 isAddAsDisplayName;
extern bool16 isAddImageDescription;
extern bool16 isOutputAsSwatch;
bool16 deleteIfEmpty = kFalse;
extern bool16 isAddListName;
//---------
bool16 isHorizontalFlow = kFalse;
bool16 isSpreadBasedLetterKeys = kFalse;


int32 intSprItemPerFrameFlow = -1; //--------
double table_TypeID = -1;

//MessageServer
#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("TPLSelectionObserver.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAlert::InformationAlert(X)//CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}


CREATE_PMINTERFACE(TPLSelectionObserver, kTPLSelectionObserverImpl)

TPLSelectionObserver::TPLSelectionObserver(IPMUnknown *boss) :
	ActiveSelectionObserver(boss) {}

TPLSelectionObserver::~TPLSelectionObserver() {}

void TPLSelectionObserver::HandleSelectionChanged(const ISelectionMessage* message)
{
	UpdatePanel();
}

void TPLSelectionObserver::AutoAttach()
{
	//CA("AutoAttatch");
	
	ActiveSelectionObserver::AutoAttach();
	TPLMediatorClass::loadData=kTrue;
	
	InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
	if (iPanelControlData==nil)
	{
	//	CA("iPanelControlData is nil");
		return;
	}
	/* Storing IPanelControlData in Mediator */
	TPLMediatorClass::iPanelCntrlDataPtr=iPanelControlData;
	
	AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kTPLDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
	//AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kTPLClassTreeIconSuiteWidgetID, IID_ITRISTATECONTROLDATA);
	AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kTPLRefreshIconSuiteWidgetID, IID_ITRISTATECONTROLDATA);
	AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kOutputIncludeTableHeaderWidgetID, IID_ITRISTATECONTROLDATA);
		
	AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kAutoResizeWidgetID, IID_ITRISTATECONTROLDATA);
	AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kOverFlowWidgetID, IID_ITRISTATECONTROLDATA);

	AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kOutputAsItemListWidgetID, IID_ITRISTATECONTROLDATA);
	AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kOutputAsIncludeHeaderWidgetID, IID_ITRISTATECONTROLDATA);
	AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kOutputAsAttributeNameWidgetID, IID_ITRISTATECONTROLDATA);
	AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kOutputTableNameWidgetID,IID_ITRISTATECONTROLDATA);
	AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kOutputAsTabbedTextWidgetID,IID_ITRISTATECONTROLDATA);
	AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kOutputAsTableWidgetID,IID_ITRISTATECONTROLDATA);
	AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kOutputAsImageDescriptionWidgetID,IID_ITRISTATECONTROLDATA);
	AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kDelUnSprayedFieldsWidgetID,IID_ITRISTATECONTROLDATA);	
	//--------
	//AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kHorizontalFlowCheckBoxWidgetID,IID_ITRISTATECONTROLDATA);
	AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kSpreadBasedLetterKeysWidgetID, IID_ITRISTATECONTROLDATA);
	AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kOutputAsFieldNameCheckBoxWidgetID, IID_ITRISTATECONTROLDATA);
	AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kHorizontalFlowRadioButtonWidgetID, IID_ITRISTATECONTROLDATA);
	AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kVerticalFlowRadioButtonWidgetID, IID_ITRISTATECONTROLDATA);
	
	AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kSprayItemPerFrameWidgetID, IID_ITRISTATECONTROLDATA);
	AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kHorizontalFlowWidgetID, IID_ITRISTATECONTROLDATA);
	AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kTPLTableListDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
    AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kOutputIncludeListNameWidgetID, IID_ITRISTATECONTROLDATA);
    AttachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kOutputAsSwatchWidgetID,IID_ITRISTATECONTROLDATA);

    

	loadPaletteData();
}

void TPLSelectionObserver::loadPaletteData(void) 
{
	//CA("TPLSelectionObserver::loadPaletteData");
	//TPLMediatorClass md;
	static int i;

	if(TPLMediatorClass::loadData==kFalse)
	{
		//CA("TPLMediatorClass::loadData==kFalse");
		Flag1 = 1;
	}

	if(TPLMediatorClass::loadData==kTrue)
	{
		//CA("TPLMediatorClass::loadData==kTrue");
		do
		{				
			/* Check if the user has LoggedIn successfully */
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
			{
				//CA("ptrIAppFramework == nil");			
				return;
			}
		
			bool16 result=ptrIAppFramework->getLoginStatus();
			
			if(TPLMediatorClass::iPanelCntrlDataPtr==nil)
			{	
				//CA("In TPLMediatorClass::iPanelCntrlDataPtr==nil");
				InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
				if (iPanelControlData==nil)
				{
					//CA("iPanelControlData is nil");
					TPLMediatorClass::iPanelCntrlDataPtr=TPLMediatorClass::iPanelCntrlDataPtrTemp;
				}				
				else
				{
				/* Storing IPanelControlData in Mediator */
				TPLMediatorClass::iPanelCntrlDataPtr=iPanelControlData;//
				TPLMediatorClass::iPanelCntrlDataPtrTemp=iPanelControlData;
				}
				i++;
			}

			TPLActionComponent actionObsever(this);
			actionObsever.DoPalette();
	

			TPLMediatorClass::IsOneSourceMode = ptrIAppFramework->get_isONEsourceMode();

			TPLMediatorClass::dropdownCtrlView1=nil;
			TPLMediatorClass::refreshBtnCtrlView1=nil;
			TPLMediatorClass::appendRadCtrlView1=nil;
			TPLMediatorClass::embedRadCtrlView1=nil;
			TPLMediatorClass::tagFrameChkboxCtrlView1=nil;

			IControlView* dropdownCtrlView =
				TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTPLDropDownWidgetID);
			if(!dropdownCtrlView)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!dropdownCntrlView");			
				break;
			}
			
			IControlView* refreshBtnCtrlView=
				TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTPLRefreshIconSuiteWidgetID);
			if(!refreshBtnCtrlView)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!refreshBtnCtrlView");			
				break;
			}
			
			IControlView* appendRadCtrlView=
				TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kAppendRadBtnWidgetID);
			if(!appendRadCtrlView)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!appendRadCtrlView");			
				break;
			}

			IControlView* embedRadCtrlView=
				TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kEmbedRadBtnWidgetID);
			if(!embedRadCtrlView)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!embedRadCtrlView");			
				break;
			}
			
			IControlView* tagFrameChkboxCtrlView=
				TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTagFrameWidgetID);
			if(!tagFrameChkboxCtrlView)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!tagFrameChkboxCtrlView");
				break;
			}

			IControlView* autoResizeChkboxCtrlView = 
				TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kAutoResizeWidgetID);
			if(autoResizeChkboxCtrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!autoResizeChkboxCtrlView");			
				break;
			}

			IControlView* TreeCtrlView = TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kProjectTreeViewWidgetID);
			if(TreeCtrlView==nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::TreeCtrlView == nil");			
				break;
			}

			TPLMediatorClass::dropdownCtrlView1=dropdownCtrlView;
			TPLMediatorClass::refreshBtnCtrlView1=refreshBtnCtrlView;
			TPLMediatorClass::appendRadCtrlView1=appendRadCtrlView;
			TPLMediatorClass::embedRadCtrlView1=embedRadCtrlView;
			TPLMediatorClass::tagFrameChkboxCtrlView1=tagFrameChkboxCtrlView;
			TPLMediatorClass::ProjectLstboxCntrlView=TreeCtrlView;

			IControlView* iTblGrpCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTableGroupPanelWidgetID);
			if(iTblGrpCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iTblGrpCntrlView == nil");
				return ;
			}
						
			IControlView* iAttrGrpCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kAttrGroupPanelWidgetID);
			if(iAttrGrpCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iAttrGrpCntrlView == nil");
				return ;
			}
			
			IControlView* iImgGrpCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kImageGroupPanelWidgetID);
			if(iImgGrpCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iImgGrpCntrlView == nil");
				return ;
			}

			IControlView* iOverFlowCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOverFlowWidgetID);
			if(iOverFlowCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iOverFlowCntrlView == nil");
				return ;
			}
            
            IControlView* iOutputAsSwatchCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsSwatchWidgetID);
			if(iOutputAsSwatchCntrlView==nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iOutputAsSwatchCntrlView == nil");
				return ;
			}
			InterfacePtr<ITriStateControlData> itristatecontroldataOpAsSwatch(iOutputAsSwatchCntrlView, UseDefaultIID());
			if(itristatecontroldataOpAsSwatch==nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldataOpAsSwatch == nil");
				return;
			}

			IControlView* iImageDescriptionCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsImageDescriptionWidgetID);
			if(iImageDescriptionCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iImageDescriptionCntrlView == nil");
				return ;
			}
			InterfacePtr<ITriStateControlData> itristatecontroldata3(iImageDescriptionCntrlView, UseDefaultIID());
			if(itristatecontroldata3==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata3 == nil");			
				return;
			}
			
			IControlView* iItemListCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsItemListWidgetID);
			if(iItemListCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iItemListCntrlView == nil");
				return ;
			}
			InterfacePtr<ITriStateControlData> itristatecontroldata(iItemListCntrlView, UseDefaultIID());
			if(itristatecontroldata==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata == nil");			
				return;
			}
			IControlView* tableListdropdownCtrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTPLTableListDropDownWidgetID);
			if(tableListdropdownCtrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::tableListdropdownCtrlView == nil");
				return;
			}
			
			if(!result)  
			{	
				//CA("Invalid selection.  Please log into PRINTsource before continuing.");
				
				IControlView * itemListTextControlView = TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kItemListTextWidgetID);
				if(itemListTextControlView == nil)
				{
					//CA("itemListTextControlView == nil");
					break;
				}
				InterfacePtr<ITextControlData>itemListTextControlData(itemListTextControlView,UseDefaultIID());
				if(itemListTextControlData == nil)
				{
					//CA("itemListTextControlData == nil");
					break;
				}				

				PMString insertItemText("");
				insertItemText.Append("Item List");
				//PMString insertItemText(kItemListStringKey);
				insertItemText.SetTranslatable(kFalse);
				insertItemText.ParseForEmbeddedCharacters();
				itemListTextControlData->SetString(insertItemText);

				dropdownCtrlView->Disable();
				refreshBtnCtrlView->Disable();
				appendRadCtrlView->Disable();
				embedRadCtrlView->Disable();
				tagFrameChkboxCtrlView->Disable();
				autoResizeChkboxCtrlView->Disable();
				TreeCtrlView->Disable();
				Flag1 = 1;
				
				iTblGrpCntrlView->HideView();
				iTblGrpCntrlView->Disable();
				iAttrGrpCntrlView->Disable();
				iImgGrpCntrlView->HideView();
				iImgGrpCntrlView->Disable();

				iOverFlowCntrlView->Disable();
			
			//	TPLActionComponent actionObsever(this);
			//	actionObsever.CloseTemplatePalette();
			
			//	classTreeCtrlView->Disable();
				break;
			}
			
			else
			{	
				//CA("Else part");

				dropdownCtrlView->Enable();
				refreshBtnCtrlView->Enable();
				if(!IsRefresh && Flag1 == 1)
				{
					appendRadCtrlView->Enable();
					embedRadCtrlView->Enable();
					tagFrameChkboxCtrlView->Enable();
				}
				autoResizeChkboxCtrlView->Enable();

				TreeCtrlView->Enable();

				iOverFlowCntrlView->Enable();
				
				/*AP*/
				InterfacePtr<ILoginHelper> ptrLogInHelper(static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss,ILoginHelper::kDefaultIID)));
				if(ptrLogInHelper == nil)
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::Pointer to ptrLogInHelper is nil.");
					return  ;
				}	

				LoginInfoValue cserverInfoValue;
				bool16 resultValue = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue);
				if(resultValue)
				{
					ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();
					if(clientInfoObj.getisItemTablesasTabbedText() == 0)
						ISTabbedText = kFalse;
					else
						ISTabbedText = kTrue;
						
					if(clientInfoObj.getisAddTableHeaders() == 0)
						isAddTableHeader = kFalse;
					else
						isAddTableHeader = kTrue;
				}
				
			
				IControlView* iIncludeHeaderCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsIncludeHeaderWidgetID);
				if(iIncludeHeaderCntrlView==nil) 
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iIncludeHeaderCntrlView == nil");
					return ;
				}
				InterfacePtr<ITriStateControlData> itristatecontroldata1(iIncludeHeaderCntrlView, UseDefaultIID());
				if(itristatecontroldata1==nil) 
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata1 == nil");			
					return;
				}
				
				IControlView* iAttributeNameCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsAttributeNameWidgetID);
				if(iAttributeNameCntrlView==nil) 
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iAttributeNameCntrlView == nil");
					return ;
				}

				InterfacePtr<ITriStateControlData> itristatecontroldata2(iAttributeNameCntrlView, UseDefaultIID());
				if(itristatecontroldata2==nil) 
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata2 == nil");			
					return;
				}

				IControlView* iSpreadBasedLetterKeysCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kSpreadBasedLetterKeysWidgetID);
				if(iSpreadBasedLetterKeysCntrlView==nil) 
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iSpreadBasedLetterKeysCntrlView == nil");
					return ;
				}
				InterfacePtr<ITriStateControlData> itristatecontroldata3(iSpreadBasedLetterKeysCntrlView, UseDefaultIID());
				if(itristatecontroldata3==nil) 
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::itristatecontroldata3 == nil");			
					return;
				}

				itristatecontroldata3->Deselect();
				iSpreadBasedLetterKeysCntrlView->Disable();
				iSpreadBasedLetterKeysCntrlView->HideView();
								
				if(isAddImageDescription)
					itristatecontroldata3->Select();
				else
					itristatecontroldata3->Deselect();
                
                
                

				if(ISTabbedText)
				{
					//CA("ISTabbedText");
					itristatecontroldata->Select();	
					if(isAddTableHeader)
					{
						iIncludeHeaderCntrlView->Enable();
						itristatecontroldata1->Select();
					}
					itristatecontroldata2->Deselect();
					//iAttributeNameCntrlView->Disable();	
					if(SelectedRowNo == 4)		//-------
					{
						//CA("SelectedRowNo == 4");
						tableListdropdownCtrlView->ShowView();
						iAttributeNameCntrlView->HideView();
					}
					else
					{
						tableListdropdownCtrlView->HideView();
						iAttributeNameCntrlView->ShowView();
					}
                    
				}
				else
				{
					if(itristatecontroldata->IsSelected())
						itristatecontroldata->Deselect();
					tableListdropdownCtrlView->HideView();
					
					if(itristatecontroldata1->IsSelected())
						itristatecontroldata1->Deselect();

					iIncludeHeaderCntrlView->Disable();
					
					//iAttributeNameCntrlView->Enable(); 
					iAttributeNameCntrlView->ShowView();
					
					if(isAddAsDisplayName)
						itristatecontroldata2->Select();
					else
					{
						itristatecontroldata2->Deselect();
						iItemListCntrlView->Enable();
					}
				}
				
				
				iTblGrpCntrlView->HideView();
				iTblGrpCntrlView->Disable();
				iAttrGrpCntrlView->ShowView();
				iAttrGrpCntrlView->Enable();
				iImgGrpCntrlView->HideView();
				iImgGrpCntrlView->Disable();
				//classTreeCtrlView->Enable();
				
				
				IControlView *horizontalFlowRdButtonControlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kHorizontalFlowRadioButtonWidgetID);
				if(horizontalFlowRdButtonControlView == nil)
				{
					//CA("horizontalFlowCheckBoxControlView == nil");
					return;
				}
				InterfacePtr<ITriStateControlData> horizontalFlowCtrlData (horizontalFlowRdButtonControlView,UseDefaultIID());
				if(horizontalFlowCtrlData == nil)
				{
					//CA("horizontalFlowCtrlData == nil");
					return;
				}
				horizontalFlowCtrlData->Select();
				isHorizontalFlow=kTrue;
				
			}
			
/***************************************************************/
			
			if(result)
			{  
				//CA("Inside result");	

				//if(TPLMediatorClass::CurrLanguageID == -1) 
				{
					
					InterfacePtr<IClientOptions> cop((IClientOptions*) ::CreateObject(kClientOptionsReaderBoss,IID_ICLIENTOPTIONS));
					if(cop==nil){
						ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::cop==nil");
						break;
					}

					PMString LocaleName("");
					TPLMediatorClass::CurrLanguageID = cop->getDefaultLocale(LocaleName);

					VectorLanguageModelPtr VectorLocValPtr = ptrIAppFramework->StructureCache_getAllLanguages();
					if(VectorLocValPtr == NULL)
					{
						ptrIAppFramework->LogError("AP7_TemplateBuilder::DynMnuDynamicMenu::RebuildMenu::StructureCache_getAllLanguages's VectorLocValPtr == NULL");
						break;
					}
					ptrIAppFramework->setLocaleId(VectorLocValPtr->at(0).getLanguageID());
					TPLMediatorClass::CurrLanguageID = VectorLocValPtr->at(0).getLanguageID();

					
				}
				//CA(LocaleName);
					
				//CAI(TPLMediatorClass::CurrLanguageID);

				IControlView * itemListTextControlView = TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kItemListTextWidgetID);
				if(itemListTextControlView == nil)
				{
					//CA("itemListTextControlView == nil");
					break;
				}
				InterfacePtr<ITextControlData>itemListTextControlData(itemListTextControlView,UseDefaultIID());
				if(itemListTextControlData == nil)
				{
					//CA("itemListTextControlData == nil");
					break;
				}
				
				PMString insertItemText("");
				/*AP*/
				//insertItemText.Append(ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename("Item"),TPLMediatorClass::CurrLanguageID));
				insertItemText.Append("Item");
				insertItemText.Append(" List");
				insertItemText.SetTranslatable(kFalse);
                insertItemText.ParseForEmbeddedCharacters();
				itemListTextControlData->SetString(insertItemText);

				//PM_Product_Level = ptrIAppFramework->getPM_Product_Levels();

				/*PMString ASD("PM_Product_Level : ");
				ASD.AppendNumber(PM_Product_Level);
				CA(ASD);*/
				
				if(TPLMediatorClass::currTempletSelectedClassID != -1)	//----20/03/2010-----
					CurrentClassID = TPLMediatorClass::currTempletSelectedClassID;

				/*if(PM_Product_Level ==3){			
					populatePFPanelLstbox();
				}
				if((PM_Product_Level ==3) || (PM_Product_Level ==2)){
					populatePGPanelLstbox();
				}*/
				populatePRPanelLstbox();
				populateItemPanelLstbox();
				populateProjectPanelLstbox();

				if(TPLMediatorClass::IsOneSourceMode)
					populateCatagoryPanelLstbox();
				
				InterfacePtr<IDropDownListController> iDropDownListController(dropdownCtrlView, UseDefaultIID());
				if (iDropDownListController == nil)
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iDropDownListController == nil");				
					break;		
				}

				InterfacePtr<IStringListControlData> TabDropListData(iDropDownListController, UseDefaultIID());
				if (TabDropListData == nil)
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iStringListControlData invalid");
					break;
				}

				IControlView* ProjectPanelCtrlView = 
					TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kProjectTreeViewWidgetID);
				if (ProjectPanelCtrlView == nil)
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::ProjectPanelCtrlView == nil");
					break;
				}
			
				TPLMediatorClass::ProjectLstboxCntrlView=ProjectPanelCtrlView;
				
				if(Flag1 == 1 || TPLMediatorClass::doReloadFlag == kFalse) 
				{
					//CA("Flag1 =1");	
					TPLMediatorClass::doReloadFlag = kTrue;
					TabDropListData->Clear(kFalse,kFalse);	

					
					//if(PM_Product_Level ==1)// 1
					{
						//CA("if(PM_Product_Level ==1)");
						PMString TabName;
						if(TPLMediatorClass::IsOneSourceMode)
						{
							//CA("1");
							TabName.Append("Category");
							//TabName = ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename(TabName),TPLMediatorClass::CurrLanguageID);
							TabName.SetTranslatable(kFalse);
							TabDropListData->AddString(TabName, 0, kFalse, kFalse);	
						}				
						else
						{
							//CA("2");
							TabName.Append("Event");
							//TabName = ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename(TabName),TPLMediatorClass::CurrLanguageID);
							TabName.SetTranslatable(kFalse);
							TabDropListData->AddString(TabName, 0, kFalse, kFalse);	
						}
						//CA("out of else");
						TabName.Clear();
						TabName = "Family"; //"Item Group"; //"Product";
						//TabName = ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename(TabName),TPLMediatorClass::CurrLanguageID);
						//CA(ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename(TabName),TPLMediatorClass::CurrLanguageID));
						TabName.SetTranslatable(kFalse);
						TabDropListData->AddString(TabName, 1, kFalse, kFalse);                // this is to show itemgroup in dropdownlist.

						TabName.Clear();
						TabName = "Item";
						//TabName = ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename(TabName),TPLMediatorClass::CurrLanguageID);
						TabName.SetTranslatable(kFalse);
						TabDropListData->AddString(TabName, 2, kFalse, kFalse);   
						
						iDropDownListController->Select(2);  // By Default Item
					}
					
									
					ProjectPanelCtrlView->ShowView(kTrue);
                    SelectedRowNo=4;
					Flag1 =0;

					InterfacePtr<ITreeViewMgr> treeViewMgr(TPLMediatorClass::ProjectLstboxCntrlView, UseDefaultIID());
					if(!treeViewMgr)
					{
						ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");					
						return;
					}
						
					TPLTreeModel pModel;
					PMString pfName("Root");
					pModel.setRoot(-1, pfName, 1);
					treeViewMgr->ClearTree(kTrue);
					pModel.GetRootUID();
					treeViewMgr->ChangeRoot();
					
					if(!IsRefresh)
					{
						//CA("!IsRefresh");
						this->setTriState( kAutoResizeWidgetID ,ITriStateControlData::/*kUnselected*/kSelected);
						if(TPLMediatorClass::curSelRadBtn==1) // Append
						{
							this->setTriState(kAppendRadBtnWidgetID,ITriStateControlData::kSelected);
							this->setTriState(kEmbedRadBtnWidgetID,ITriStateControlData::kUnselected);
							tagFrameChkboxCtrlView->Disable();
						}
						else if(TPLMediatorClass::curSelRadBtn==2) // Embed
						{
							this->setTriState(kAppendRadBtnWidgetID,ITriStateControlData::kUnselected);
							this->setTriState(kEmbedRadBtnWidgetID,ITriStateControlData::kSelected);

							tagFrameChkboxCtrlView->Enable();
							InterfacePtr<ITriStateControlData> itristatecontroldata
								(tagFrameChkboxCtrlView, UseDefaultIID());
							
							if(itristatecontroldata==nil) 
							{
								ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::itristatecontroldata == nil");							
								break;
							}
					
						}
					}
				}
				else if(Flag1 == 0)
				{  
					
					InterfacePtr<ITreeViewMgr> treeViewMgr(TPLMediatorClass::ProjectLstboxCntrlView, UseDefaultIID());
					if(!treeViewMgr) { 
						ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");
						return;
					}
						
					TPLTreeModel pModel;
					PMString root("Root");
									
						switch(SelectedRowNo) 
						{
							case 0:
								pModel.setRoot(-1, root, 1);
								break;
							case 1:
								pModel.setRoot(-1, root, 2);
								break;
							case 2:
								pModel.setRoot(-1, root, 3);
								break;
							case 3:
								pModel.setRoot(-1, root, 4);
								break;
							case 4:
								pModel.setRoot(-1, root, 5);
								break;
						}
					
					
					treeViewMgr->ClearTree(kTrue);
					pModel.GetRootUID();
					treeViewMgr->ChangeRoot();

					//CA("After Reload.........");
				
				//	else
				//		CA("Chkbox unselcted");
			
				}
			}
			else
			{				
				//Awasthi
				//sectionDropListCntrlView->Disable();
				//CA("Again Closing");
				TPLActionComponent actionObsever(this);
				actionObsever.CloseTemplatePalette();
			}
			
		
		} while (kFalse);
		//CA("5");
	}
}


void TPLSelectionObserver::AutoDetach()
{
	//CA("AutoDetach");
	ActiveSelectionObserver::AutoDetach();
	TPLMediatorClass::loadData=kFalse;
	do
	{
		DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kTPLDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
	//	DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kTPLClassTreeIconSuiteWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kTPLRefreshIconSuiteWidgetID, IID_ITRISTATECONTROLDATA);
		// Added By Awasthi

		//Added by Tushar on 11/12/06
		DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kAutoResizeWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kOverFlowWidgetID, IID_ITRISTATECONTROLDATA);

		DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kOutputAsItemListWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kOutputAsIncludeHeaderWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kOutputAsAttributeNameWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kOutputTableNameWidgetID,IID_ITRISTATECONTROLDATA);
		DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kOutputAsTabbedTextWidgetID,IID_ITRISTATECONTROLDATA);
		DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kOutputAsTableWidgetID,IID_ITRISTATECONTROLDATA);
		DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kOutputIncludeTableHeaderWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kOutputAsImageDescriptionWidgetID,IID_ITRISTATECONTROLDATA);
		DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kDelUnSprayedFieldsWidgetID,IID_ITRISTATECONTROLDATA);

		//------
		//DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kHorizontalFlowCheckBoxWidgetID,IID_ITRISTATECONTROLDATA);
		DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kSpreadBasedLetterKeysWidgetID,IID_ITRISTATECONTROLDATA);
		DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kOutputAsFieldNameCheckBoxWidgetID,IID_ITRISTATECONTROLDATA);
		DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kHorizontalFlowRadioButtonWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kVerticalFlowRadioButtonWidgetID, IID_ITRISTATECONTROLDATA);

		DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kSprayItemPerFrameWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr,kHorizontalFlowWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kTPLTableListDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
        DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kOutputIncludeListNameWidgetID, IID_ITRISTATECONTROLDATA);
        DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kOutputAsSwatchWidgetID, IID_ITRISTATECONTROLDATA);

		
	} while (kFalse);
	/////////poping the event .///////  Commented by rahul
//InterfacePtr<IApplication> piApp(gSession->QueryApplication());
//InterfacePtr<IEventDispatcher> piEventDispatcher(piApp, UseDefaultIID());
//// Remove it from the event handler stack
//piEventDispatcher->Remove(this->piEventHandler);
//// Decrease refcount
//this->piEventHandler->Release();
//this->piEventHandler = nil;
	/////// end /////
}

void TPLSelectionObserver::setTriState
(const WidgetID&  widgetID, ITriStateControlData::TriState state)
{
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return;
	}
	do 
	{
		/*
		InterfacePtr<IPanelControlData> iPanelControlData(this, UseDefaultIID());
		if(iPanelControlData==nil) 
			break;

		//TPLMediatorClass::iPanelCntrlDataPtr=iPanelControlData;
		*/
		IControlView * iControlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(widgetID);
		if(iControlView==nil) 
		{
			//ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::setTriState::iControlView == nil");		
			break;
		}
		
		InterfacePtr<ITriStateControlData> itristatecontroldata(iControlView, UseDefaultIID());
		if(itristatecontroldata==nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::setTriState::itristatecontroldata == nil");
			break;
		}

		itristatecontroldata->SetState(state);
	} while(kFalse);	
}

void TPLSelectionObserver::Update
(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy)
{
	if ((protocol == IID_ILISTCONTROLDATA) && (theChange == kListSelectionChangedByUserMessage)|| (theChange == kListSelectionChangedMessage))//kListSelectionChangedByUserMessage) ) 
	{
	//	CA("Inside if added by Prabhat");
	}


	//CA("TPLSelectionObserver::Update");

	ActiveSelectionObserver::Update(theChange, theSubject, protocol, changedBy);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return;
	}
	///newCode
	IControlView* iSprayItemPerFrameCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kSprayItemPerFrameWidgetID);
	if(iSprayItemPerFrameCntrlView==nil) 
	{
		ptrIAppFramework->LogDebug("AP46_TemplateBuilder::TPLSelectionObserver::Update::iIncludeHeaderCntrlView == nil");
		return;
	}
	IControlView* iHorizontalFlowControlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kHorizontalFlowWidgetID);
	if(iHorizontalFlowControlView == NULL)
	{
		ptrIAppFramework->LogDebug("AP46_TemplateBuilder::TPLSelectionObserver::Update::iIncludeHeaderCntrlView == nil");
		return;
	}
	InterfacePtr<ITriStateControlData> itristatecontroldataForSprayItemPerFrame(iSprayItemPerFrameCntrlView, UseDefaultIID());
	if(itristatecontroldataForSprayItemPerFrame==nil) 
	{
		ptrIAppFramework->LogDebug("AP46_TemplateBuilder::TPLSelectionObserver::loadPaletteData::itristatecontroldata3 == nil");			
		return;
	}
	InterfacePtr<ITriStateControlData> itristatecontroldataForHorizontalFlow(iHorizontalFlowControlView, UseDefaultIID());
	if(itristatecontroldataForHorizontalFlow==nil) 
	{
		ptrIAppFramework->LogDebug("AP46_TemplateBuilder::TPLSelectionObserver::loadPaletteData::itristatecontroldata3 == nil");			
		return;
	}
	IControlView* iItemListCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsItemListWidgetID);
	if(iItemListCntrlView==nil) 
	{
		ptrIAppFramework->LogDebug("AP46_TemplateBuilder::TPLSelectionObserver::Update::iIncludeHeaderCntrlView == nil");
		return;
	}

	IControlView* iIncludeHeaderControlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsIncludeHeaderWidgetID);
	if(iIncludeHeaderControlView==nil) 
	{
		ptrIAppFramework->LogDebug("AP46_TemplateBuilder::TPLSelectionObserver::Update::iIncludeHeaderCntrlView == nil");
		return;
	}
	InterfacePtr<ITriStateControlData> itristatecontroldataForItemList(iItemListCntrlView, UseDefaultIID());
	if( itristatecontroldataForItemList==nil) 
	{
		ptrIAppFramework->LogDebug("AP46_TemplateBuilder::TPLSelectionObserver::loadPaletteData::itristatecontroldata3 == nil");			
		return;
	}
	InterfacePtr<ITriStateControlData> itristatecontroldataForIncludeHeader(iItemListCntrlView, UseDefaultIID());
	if( itristatecontroldataForIncludeHeader==nil) 
	{
		ptrIAppFramework->LogDebug("AP46_TemplateBuilder::TPLSelectionObserver::loadPaletteData::itristatecontroldata3 == nil");			
		return;
	}
	///newCode
	
	do
	{
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			//ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::controlView == nil");
			return;	
		}

		WidgetID theSelectedWidget = controlView->GetWidgetID();

		if(theSelectedWidget==kTPLDropDownWidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		{
			HandleDropDownListClick(theChange,theSubject,protocol,changedBy);
			break;
		}
//newCode1
		if(theSelectedWidget == kSprayItemPerFrameWidgetID && (theChange == kTrueStateMessage || theChange == kFalseStateMessage))
		{
			itristatecontroldataForIncludeHeader->Deselect();
			iIncludeHeaderControlView->Disable();
			itristatecontroldataForItemList->Deselect();
			iItemListCntrlView->Disable();
			
			IControlView* iChkboxCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kHorizontalFlowWidgetID);
			if(iChkboxCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iChkboxCntrlView == nil");
				return;
			}			

			InterfacePtr<ITriStateControlData> itristatecontroldata(iChkboxCntrlView, UseDefaultIID());
			if(itristatecontroldata==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata == nil");
				return;
			}

			if(theChange == kTrueStateMessage)
			{
				//CAlert::InformationAlert("theChange == kTrueStateMessage");
				iChkboxCntrlView->ShowView();                  // it is Enabled
				iChkboxCntrlView->Enable();
				iItemListCntrlView->Enable();				// added 23 april by mahesh
				intSprItemPerFrameFlow = 2;
				populateItemPanelLstbox();

			}
			else
			{
				//CAlert::InformationAlert(" else of theChange == kTrueStateMessage");
				itristatecontroldata->Deselect();
				//iChkboxCntrlView->HideView();               // changed by mahesh it was disabled
				iChkboxCntrlView->Disable();
				intSprItemPerFrameFlow = -1;
				populateItemPanelLstbox();
			}
		}
		if(theSelectedWidget == kSprayItemPerFrameWidgetID && theChange != kTrueStateMessage)
		{
			//CAlert::InformationAlert("theSelectedWidget == kSprayItemPerFrameWidgetID && theChange != kTrueStateMessage");
			itristatecontroldataForHorizontalFlow->Deselect();
			iHorizontalFlowControlView->Disable();
			itristatecontroldataForIncludeHeader->Deselect();
			iIncludeHeaderControlView->Disable();
			itristatecontroldataForItemList->Deselect();
			iItemListCntrlView->Enable();
			iSprayItemPerFrameCntrlView->Disable();
		}
//newCode1
		if(theSelectedWidget == kHorizontalFlowWidgetID  && theChange == kTrueStateMessage)
		{
			intSprItemPerFrameFlow = 1;
			populateItemPanelLstbox();
		}

		if(theSelectedWidget == kHorizontalFlowWidgetID  && theChange == kFalseStateMessage)
		{
			intSprItemPerFrameFlow = 2;
			populateItemPanelLstbox();
		}

		if(theSelectedWidget == kAutoResizeWidgetID && theChange == kTrueStateMessage)
		{
			//CA("theSelectedWidget == kAutoResizeWidgetID");
			IControlView* iChkboxCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOverFlowWidgetID);
			if(iChkboxCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iChkboxCntrlView == nil");
				return;
			}

			InterfacePtr<ITriStateControlData> itristatecontroldata(iChkboxCntrlView, UseDefaultIID());
			if(itristatecontroldata==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata == nil");
				return;
			}
			if(itristatecontroldata->IsSelected())
			{
				itristatecontroldata->Deselect();
			}
		}

		if(theSelectedWidget == kOverFlowWidgetID && theChange == kTrueStateMessage)
		{
			//CA("theSelectedWidget == kOverFlowWidgetID");
			IControlView* iChkboxCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kAutoResizeWidgetID);
			if(iChkboxCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iChkboxCntrlView == nil");
				return;
			}

			InterfacePtr<ITriStateControlData> itristatecontroldata(iChkboxCntrlView, UseDefaultIID());
			if(itristatecontroldata==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata == nil");			
				return;
			}
			if(itristatecontroldata->IsSelected())
			{
				itristatecontroldata->Deselect();
			}
		}
/////////////////upto here added by Tushar on 11/12/06

		
/*		if (theSelectedWidget == kTPLClassTreeIconSuiteWidgetID
				&& theChange == kTrueStateMessage)
		{
			InterfacePtr<IClassificationTree> interfPtr(static_cast<IClassificationTree*> (CreateObject(kClassificationTreeBoss,IClassificationTree::kDefaultIID)));
			if(interfPtr == nil)
				return;

			int32 selectClsId=0;
			PMString selectClasName="";
			PMString selectHierarchy=""; 

			interfPtr->ShowClassificationTree
			(selectClsId, selectClasName, selectHierarchy);

			if(selectClsId==0)
			{
				if(TPLMediatorClass::lastSelClasId!=selectClsId)
				{
					selectClsId = TPLMediatorClass::lastSelClasId;
					selectClasName = TPLMediatorClass::lastSelClasName;
					selectHierarchy = TPLMediatorClass::lastSelClasHierarchy;
				}
				else
				{
					InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
					if(ptrIAppFramework == nil)
						break;

					VectorClassInfoPtr vectClassInfoValuePtr = 
					ptrIAppFramework->CLASSMngr_getClassificationRoot();

					if(vectClassInfoValuePtr==nil)
						break;

					VectorClassInfoValue::iterator it2;
					
					PMString masterName("");
					int32 classID = -1;
					for(it2=vectClassInfoValuePtr->begin(); it2!=vectClassInfoValuePtr->end(); it2++)
					{
						masterName = it2->getClassification_name();
						classID = it2->getClass_id();
						break;
					}
					selectClsId = classID;
					selectClasName = masterName;
					selectHierarchy = masterName;
				}
			}

			TPLMediatorClass::lastSelClasId=selectClsId;
			TPLMediatorClass::lastSelClasName=selectClasName;
			TPLMediatorClass::lastSelClasHierarchy=selectHierarchy;
			TPLMediatorClass::lastSelClasIdForRefresh=selectClsId;
			TPLMediatorClass::lastSelClasNameForRefresh=selectClasName;
			TPLMediatorClass::lastSelClasHierarchyForRefresh=selectHierarchy;
//
			InterfacePtr<IPanelControlData> panel(QueryPanelControlData());
			if(panel==nil)
				break;
*/
/*			IControlView* textView=
				TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kClassHierarchyStaticTxtWidgetID);
			if(textView==nil)
				break;

			InterfacePtr<ITextControlData> iData(textView, UseDefaultIID());
			if(iData==nil)
				break;
			iData->SetString(selectHierarchy);

			SDKListBoxHelper listHelper(this, kTPLPluginID);
			listHelper.EmptyCurrentListBox(TPLMediatorClass::iPanelCntrlDataPtr, 4);

			IControlView* lstboxControlView=
				listHelper.FindCurrentListBox(TPLMediatorClass::iPanelCntrlDataPtr, 4);
			if(lstboxControlView==nil)
				break;
				
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
				break;

			VectorAttributeInfoPtr attribValObj 
				= ptrIAppFramework->ATTRIBMngr_getAllAttribsForClassId(selectClsId);
			
			if(attribValObj==nil)
				break;

			VectorAttributeInfoValue::iterator it;
			int i=0;
			ItemData.clearVector(4);
			ItemData.setData
				(4,
				 i,
				 -1,
				 -1,
				 "", 
				 "Item Name",
				 kTrue,
				 "",
				 kFalse,
				 0
				 );

			listHelper.AddElement(lstboxControlView, "Item Name", kItemPanelTextWidgetID, i);
			listHelper.AddIcons(lstboxControlView, i, 0); 
			i++;

			for(it=attribValObj->begin();it!=attribValObj->end();it++)
			{
				ItemData.setData
				(4,
				 i,
				 it->getAttribute_id(),
				 0,
				 it->getTable_col().GrabCString(),
				 it->getDisplay_name().GrabCString(),
				 kTrue,
				 "",
				 kFalse,
				 0
				 );
				listHelper.AddElement(lstboxControlView, it->getDisplay_name(), kItemPanelTextWidgetID, i);
				listHelper.AddIcons(lstboxControlView, i, 0); 
				i++;
			}

			VectorTypeInfoPtr typeValObj = ptrIAppFramework->TYPEMngr_getAllTypesOfTypeGroupByCode("PRD_IMAGES");
			if(typeValObj==nil)
				break;

			VectorTypeInfoValue::iterator it1;

			for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
			{
				ItemData.setData
				(4,
				 i,
				 -1,
				 it1->getType_id(),
				 "", 
				 it1->getName().GrabCString(),
				 kTrue,
				 it1->getCode().GrabCString(),
				 kTrue,
				 0
				 );
				listHelper.AddElement(lstboxControlView, it1->getName(), kItemPanelTextWidgetID, i);
				listHelper.AddIcons(lstboxControlView, i, 1); 
				i++;
			}

			IControlView * listBox=
				listHelper.FindCurrentListBox(TPLMediatorClass::iPanelCntrlDataPtr, 4);
			if(listBox == nil) 
				break;

			listBox->Invalidate();
			TPLCommonFunctions comnFuns;
			comnFuns.refreshLstbox(kInvalidUID);
			break;
		}
*/
		if (theSelectedWidget == kTPLRefreshIconSuiteWidgetID && theChange == kTrueStateMessage)
		{
			bool16 result=ptrIAppFramework->getLoginStatus();
			if(!result)
				return;
			
			AcquireWaitCursor awc;
			awc.Animate(); 
	
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
				break;
			//CA("kTPLRefreshIconSuiteWidgetID");

			//CA("Refreshing Event Cache");
			
			ptrIAppFramework->EventCache_clearInstance();			
			
			ptrIAppFramework->StructureCache_clearInstance();
			ptrIAppFramework->clearConfigCache();
			
			/*AP*/
			////CA("Refreshing Attribute Cache");
			//retVal=0;
			//retVal=ptrIAppFramework->ATTRIBUTECACHE_clearInstance();
			//if(retVal==0)
			//{
			//	ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::Update::ATTRIBUTECACHE_clearInstance's retVal == 0");
			//	break;
			//}

			////CA("refreshing PossibleValue Cache.");
			//retVal=0;
			//retVal=ptrIAppFramework->POSSIBLEVALUECACHE_clearInstance();
			//if(retVal==0)
			//{
			//	ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::Update::POSSIBLEVALUECACHE_clearInstance's retVal == 0");
			//	break;
			//}

			//ptrIAppFramework->GETCommon_refreshClientCache();

			InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
			if(ptrIClientOptions == nil)//-100 is taken purposely.-100 does not exists.So something has to be selected from the list box.
			{
				//CAlert::ErrorAlert("Interface for IClientOptions not found.");
				break;
			}
			
			ptrIClientOptions->setdoRefreshFlag(kTrue);
			
			//CA("After refresh");
			TPLMediatorClass::loadData=kTrue;
			IsRefresh = kTrue;
			this->loadPaletteData();
			IsRefresh = kFalse;

		}

		if(theSelectedWidget==kTPLTableListDropDownWidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		{
			//CA("theSelectedWidget==kTPLTableListDropDownWidgetID");			
			IControlView* tableListdropdownCtrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTPLTableListDropDownWidgetID);
			if(tableListdropdownCtrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::tableListdropdownCtrlView == nil");
				return;
			}
			
			InterfacePtr<IDropDownListController> tableListDropDownController(tableListdropdownCtrlView, UseDefaultIID());
			if (tableListDropDownController == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::tableListDropDownController == nil");				
				break;		
			}

	 		InterfacePtr<IStringListControlData> tablelistDropDownData(tableListDropDownController, UseDefaultIID());
			if (tablelistDropDownData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::tablelistDropDownData invalid");
				break;
			}
			int32 index = tableListDropDownController->GetSelected(); 
			PMString tableName = tablelistDropDownData->GetString(index);
			
			if(SelectedRowNo == 4)
			{
				//CA("SelectedRowNo == 4");
				int32 itemListSize = static_cast<int32>(ItemData.returnListVectorSize(4));
				for(int32 i = 0; i < itemListSize ; i++)
				{
					
					TPLListboxInfo listInfo = ItemData.getData(4 , i);					 
					if(ITEMDataNodeList[i].getName() == tableName)
					{
						table_TypeID = listInfo.typeId;
					}
					else
						continue;					
				}
				if(tableName == "All")
					table_TypeID = -1;
			}
			break;
		}

		if(theSelectedWidget == kOutputAsItemListWidgetID && theChange == kTrueStateMessage)
		{
            itristatecontroldataForHorizontalFlow->Deselect();
			iHorizontalFlowControlView->Disable();
			itristatecontroldataForSprayItemPerFrame->Deselect();
			iSprayItemPerFrameCntrlView->Disable();                    // 1740 it is Disable();
			ISTabbedText = kTrue;

			IControlView* iIncludeHeaderCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsIncludeHeaderWidgetID);
			if(iIncludeHeaderCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iIncludeHeaderCntrlView == nil");
				return;
			}
			iIncludeHeaderCntrlView->Enable();

			
			InterfacePtr<ILoginHelper> ptrLogInHelper((static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss , IID_ILOGINHELPER /*ILoginHelper::kDefaultIID*/))));
			if(ptrLogInHelper == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::Pointer to ptrLogInHelper is nil.");
				return ;
			}
			LoginInfoValue cserverInfoValue ;
			bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue) ;
			if(result)
			{	
				ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();

				clientInfoObj.setisItemTablesasTabbedText(1) ;

				cserverInfoValue.setClientInfoValue(clientInfoObj);
				ptrLogInHelper->setEditedServerInfo(cserverInfoValue);
				double clientID = ptrIAppFramework->getClientID();
				result = ptrLogInHelper->editServerInfo(cserverInfoValue,clientID) ;
			}

			IControlView* iAttNameCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsAttributeNameWidgetID);
			if(iAttNameCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iAttNameCntrlView == nil");
				return;
			}

			InterfacePtr<ITriStateControlData> itristatecontroldata(iAttNameCntrlView, UseDefaultIID());
			if(itristatecontroldata==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata == nil");			
				return;
			}
			if(itristatecontroldata->IsSelected())
			{
				itristatecontroldata->Deselect();
			}
			//iAttNameCntrlView->Disable();	//---------
			iAttNameCntrlView->HideView();
////
			IControlView* iDelUnSprayedCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kDelUnSprayedFieldsWidgetID);
			if(iDelUnSprayedCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iDelUnSprayedCntrlView == nil");
				return;
			}
			InterfacePtr<ITriStateControlData> itristatecontroldata1(iDelUnSprayedCntrlView, UseDefaultIID());
			if(itristatecontroldata1==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata1 == nil");			
				return;
			}	
			if(itristatecontroldata1->IsSelected())
			{
				itristatecontroldata1->Deselect();
			}
			//iDelUnSprayedCntrlView->Disable();
			iDelUnSprayedCntrlView->HideView();

			IControlView* tableListdropdownCtrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTPLTableListDropDownWidgetID);
			if(tableListdropdownCtrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::tableListdropdownCtrlView == nil");
				return;
			}
			tableListdropdownCtrlView->ShowView();

			InterfacePtr<IDropDownListController> tableListDropDownController(tableListdropdownCtrlView, UseDefaultIID());
			if (tableListDropDownController == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::tableListDropDownController == nil");				
				break;		
			}

	 		InterfacePtr<IStringListControlData> tablelistDropDownData(tableListDropDownController, UseDefaultIID());
			if (tablelistDropDownData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::tablelistDropDownData invalid");
				break;
			}
			tablelistDropDownData->Clear(kFalse,kFalse);
		
			tablelistDropDownData->AddString("All", 0 );
			if(SelectedRowNo == 4)
			{
				int32 itemListSize = static_cast<int32>(ITEMDataNodeList.size());
				
				TPLListboxInfo list_Info;
				for(int32 i = 0; i < itemListSize ; i++)
				{
					list_Info = ItemData.getData(4 , i);
					if(ITEMDataNodeList[i].getHitCount() == 8 ) 
					{
						tablelistDropDownData->AddString(ITEMDataNodeList[i].getName(), i );
					}
					/*if(ITEMDataNodeList[i].getHitCount() == 2 ) 
					{
						tablelistDropDownData->AddString(ITEMDataNodeList[i].getName(), i );
					}*/
					
				}
				tableListDropDownController->Select(0);
			}
			else
			{
				tableListdropdownCtrlView->HideView();
				iAttNameCntrlView->ShowView();
				iDelUnSprayedCntrlView->ShowView();
			}

			
		}
		if(theSelectedWidget == kOutputAsItemListWidgetID && theChange != kTrueStateMessage)
		{
			itristatecontroldataForIncludeHeader->Deselect();
			iIncludeHeaderControlView->Disable();
			itristatecontroldataForHorizontalFlow->Deselect();
			iHorizontalFlowControlView->Disable();
			itristatecontroldataForSprayItemPerFrame->Deselect();
			iSprayItemPerFrameCntrlView->Enable();
			
			ISTabbedText =kFalse;
			isAddTableHeader = kFalse;
			
			//-----------
			IControlView* tableListdropdownCtrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTPLTableListDropDownWidgetID);
			if(tableListdropdownCtrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::tableListdropdownCtrlView == nil");
				return;
			}
			tableListdropdownCtrlView->HideView();

			IControlView* iIncludeHeaderCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsIncludeHeaderWidgetID);
			if(iIncludeHeaderCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iIncludeHeaderCntrlView == nil");
				return;
			}
			InterfacePtr<ITriStateControlData> itristatecontroldata(iIncludeHeaderCntrlView, UseDefaultIID());
			if(itristatecontroldata==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata == nil");			
				return;
			}
			if(itristatecontroldata->IsSelected())
			{
				itristatecontroldata->Deselect();
			}

			IControlView* iDelUnSprayedCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kDelUnSprayedFieldsWidgetID);
			if(iDelUnSprayedCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iDelUnSprayedCntrlView == nil");
				return;
			}
		
			//iDelUnSprayedCntrlView->Enable();	//---------
			iDelUnSprayedCntrlView->ShowView();

			
			InterfacePtr<ILoginHelper> ptrLogInHelper((static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss , IID_ILOGINHELPER /*ILoginHelper::kDefaultIID*/))));
			if(ptrLogInHelper == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::Pointer to ptrLogInHelper is nil.");
				return ;
			}
			LoginInfoValue cserverInfoValue ;
			bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue) ;
			if(result)
			{
				ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();
				clientInfoObj.setisItemTablesasTabbedText(0) ;
				clientInfoObj.setisAddTableHeaders(0) ;
				cserverInfoValue.setClientInfoValue(clientInfoObj);

				double clientID = ptrIAppFramework->getClientID();
				ptrLogInHelper->setEditedServerInfo(cserverInfoValue);
				result = ptrLogInHelper->editServerInfo(cserverInfoValue,clientID) ;
			}

			iIncludeHeaderCntrlView->Disable();

			IControlView* iAttNameCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsAttributeNameWidgetID);
			if(iAttNameCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iAttNameCntrlView == nil");
				return;
			}
			//iAttNameCntrlView->Enable(); //--------
			iAttNameCntrlView->ShowView();

		}

		if(theSelectedWidget == kOutputAsIncludeHeaderWidgetID && theChange == kTrueStateMessage)
		{
			isAddTableHeader = kTrue;	
			InterfacePtr<ILoginHelper> ptrLogInHelper((static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss , IID_ILOGINHELPER /*ILoginHelper::kDefaultIID*/))));
			if(ptrLogInHelper == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::Pointer to ptrLogInHelper is nil.");
				return ;
			}
			LoginInfoValue cserverInfoValue ;
	
			bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue) ;
			if(result)
			{
				ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();
				clientInfoObj.setisAddTableHeaders(1) ;
				cserverInfoValue.setClientInfoValue(clientInfoObj);
				
				ptrLogInHelper->setEditedServerInfo(cserverInfoValue);
				double clientID = ptrIAppFramework->getClientID();
				result = ptrLogInHelper->editServerInfo(cserverInfoValue,clientID) ;
			}
		}
		if(theSelectedWidget == kOutputAsIncludeHeaderWidgetID && theChange != kTrueStateMessage)
		{
			isAddTableHeader = kFalse;
			
			InterfacePtr<ILoginHelper> ptrLogInHelper((static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss , IID_ILOGINHELPER /*ILoginHelper::kDefaultIID*/))));
			if(ptrLogInHelper == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::Pointer to ptrLogInHelper is nil.");
				return ;
			}
			LoginInfoValue cserverInfoValue ;
	
			bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue) ;
			if(result)
			{
				ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();
				clientInfoObj.setisAddTableHeaders(0) ;
				cserverInfoValue.setClientInfoValue(clientInfoObj);
				
				ptrLogInHelper->setEditedServerInfo(cserverInfoValue);
				double clientID = ptrIAppFramework->getClientID();
				result = ptrLogInHelper->editServerInfo(cserverInfoValue,clientID) ;
			}
		}
		if(theSelectedWidget == kOutputAsAttributeNameWidgetID && theChange == kTrueStateMessage)
		{
			//CA("theSelectedWidget == kOutputAsAttributeNameWidgetID && theChange == kTrueStateMessage 1995");
			isAddAsDisplayName = kTrue;
			isAddImageDescription = kFalse;
            //isOutputAsSwatch = kFalse;
			IControlView* iItemListCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsItemListWidgetID);
			if(iItemListCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iItemListCntrlView == nil");
				return;
			}
			iItemListCntrlView->Disable();

			IControlView* iIncludeHeaderCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsIncludeHeaderWidgetID);
			if(iIncludeHeaderCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iIncludeHeaderCntrlView == nil");
				return;
			}
			iIncludeHeaderCntrlView->Disable();

		}
		if(theSelectedWidget == kOutputAsAttributeNameWidgetID && theChange != kTrueStateMessage)
		{
			//CA("theSelectedWidget == kOutputAsAttributeNameWidgetID && theChange != kTrueStateMessage 2017");
			isAddAsDisplayName = kFalse;

			IControlView* iItemListCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsItemListWidgetID);
			if(iItemListCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iItemListCntrlView == nil");
				return;
			}
			iItemListCntrlView->Disable();                       // it was enabled                   

		}

		if(theSelectedWidget == kOutputTableNameWidgetID && theChange == kTrueStateMessage)
		{
			
			isAddAsDisplayName = kTrue;
			isAddImageDescription = kFalse;
			ISTabbedText =kFalse;
			isAddTableHeader = kFalse;
            isAddListName = kFalse;
            isOutputAsSwatch = kFalse;
            

			
			InterfacePtr<ILoginHelper> ptrLogInHelper((static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss , IID_ILOGINHELPER /*ILoginHelper::kDefaultIID*/))));
			if(ptrLogInHelper == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::Pointer to ptrLogInHelper is nil.");
				return ;
			}
			LoginInfoValue cserverInfoValue ;
			bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue) ;
			if(result)
			{
				ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();
				clientInfoObj.setisItemTablesasTabbedText(0) ;
				clientInfoObj.setisAddTableHeaders(0) ;
				cserverInfoValue.setClientInfoValue(clientInfoObj);
		
				ptrLogInHelper->setEditedServerInfo(cserverInfoValue);
				double clientID = ptrIAppFramework->getClientID();
				result = ptrLogInHelper->editServerInfo(cserverInfoValue,clientID) ;
			}

			
			IControlView* iIncludeTableHeaderCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputIncludeTableHeaderWidgetID);
			if(iIncludeTableHeaderCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iIncludeTableHeaderCntrlView == nil");
				return;
			}
			InterfacePtr<ITriStateControlData> itristatecontroldata(iIncludeTableHeaderCntrlView, UseDefaultIID());
			if(itristatecontroldata==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata == nil");			
				return ;
			}
			if(itristatecontroldata->IsSelected())
				itristatecontroldata->Deselect(); 
			iIncludeTableHeaderCntrlView->Disable();
            
            IControlView* iIncludeListNameCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputIncludeListNameWidgetID);
			if(iIncludeListNameCntrlView==nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iIncludeListNameCntrlView == nil");
				return;
			}
			InterfacePtr<ITriStateControlData> itristatecontroldataListName(iIncludeListNameCntrlView, UseDefaultIID());
			if(itristatecontroldataListName==nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldataListName == nil");
				return ;
			}
			if(itristatecontroldataListName->IsSelected())
				itristatecontroldataListName->Deselect();
			iIncludeListNameCntrlView->Disable();
            

			IControlView* iTabbedTextCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsTabbedTextWidgetID);
			if(iTabbedTextCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iTabbedTextCntrlView == nil");
				return;
			}
			iTabbedTextCntrlView->Disable();

			IControlView* iTableCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsTableWidgetID);
			if(iTableCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iTableCntrlView == nil");
				return;
			}
			InterfacePtr<ITriStateControlData> itristatecontroldata2(iTableCntrlView, UseDefaultIID());
			if(itristatecontroldata2==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata2 == nil");			
				return ;
			}
			iTableCntrlView->Disable();
			itristatecontroldata2->Select();
			
		}
		if(theSelectedWidget == kOutputTableNameWidgetID && theChange != kTrueStateMessage)
		{

			isAddAsDisplayName = kFalse;
			ISTabbedText = kFalse;
			isAddTableHeader = kFalse;
            isAddListName = kFalse;
						
			IControlView* iIncludeTableHeaderCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputIncludeTableHeaderWidgetID);
			if(iIncludeTableHeaderCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iIncludeTableHeaderCntrlView == nil");
				return;
			}
			iIncludeTableHeaderCntrlView->Enable();

			IControlView* iTabbedTextCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsTabbedTextWidgetID);
			if(iTabbedTextCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iTabbedTextCntrlView == nil");
				return;
			}
		
			iTabbedTextCntrlView->Enable();
            
            IControlView* iIncludeListNameCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputIncludeListNameWidgetID);
			if(iIncludeListNameCntrlView==nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iIncludeTableHeaderCntrlView == nil");
				return;
			}
			iIncludeListNameCntrlView->Enable();
			
			IControlView* iTableCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsTableWidgetID);
			if(iTableCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iTableCntrlView == nil");
				return;
			}
			InterfacePtr<ITriStateControlData> itristatecontroldata1(iTableCntrlView, UseDefaultIID());
			if(itristatecontroldata1==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata1 == nil");			
				return ;
			}
			//CA("Enable");
			iTableCntrlView->Enable();
			itristatecontroldata1->Select(); 
			
		}
		if(theSelectedWidget == kOutputAsTabbedTextWidgetID && theChange == kTrueStateMessage) 
		{
			isAddAsDisplayName = kFalse;
            isAddListName = kFalse;
			
			InterfacePtr<ILoginHelper> ptrLogInHelper((static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss , IID_ILOGINHELPER /*ILoginHelper::kDefaultIID*/))));
			if(ptrLogInHelper == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::Pointer to ptrLogInHelper is nil.");
				return ;
			}
			LoginInfoValue cserverInfoValue ;
			bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue) ;
			if(result)
			{
				ISTabbedText =kTrue;
				ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();
				clientInfoObj.setisItemTablesasTabbedText(1) ;
				cserverInfoValue.setClientInfoValue(clientInfoObj);

				ptrLogInHelper->setEditedServerInfo(cserverInfoValue);
				double clientID = ptrIAppFramework->getClientID();
				result = ptrLogInHelper->editServerInfo(cserverInfoValue,clientID);
			}
            
            IControlView* iIncludeListNameCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputIncludeListNameWidgetID);
			if(iIncludeListNameCntrlView==nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iIncludeListNameCntrlView == nil");
				return;
			}
			InterfacePtr<ITriStateControlData> itristatecontroldataListName(iIncludeListNameCntrlView, UseDefaultIID());
			if(itristatecontroldataListName==nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldataListName == nil");
				return ;
			}
			if(itristatecontroldataListName->IsSelected())
				itristatecontroldataListName->Deselect();
			iIncludeListNameCntrlView->Disable();
            
		}
		if(theSelectedWidget == kOutputAsTableWidgetID && theChange == kTrueStateMessage) 
		{
			IControlView* iTableCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsTableWidgetID);
			if(iTableCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iTableCntrlView == nil");
				return;
			}
			if(iTableCntrlView->IsEnabled())
			{
				//CAlert::InformationAlert("iTableCntrlView->IsEnabled()");
				isAddAsDisplayName = kFalse;
			}
			
			InterfacePtr<ILoginHelper> ptrLogInHelper((static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss , IID_ILOGINHELPER /*ILoginHelper::kDefaultIID*/))));
			if(ptrLogInHelper == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::Pointer to ptrLogInHelper is nil.");
				return ;
			}
			LoginInfoValue cserverInfoValue ;
			bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue) ;
			if(result)
			{
				//CAlert::InformationAlert("True");
				ISTabbedText =kFalse;
				ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();
				clientInfoObj.setisItemTablesasTabbedText(0) ;
				cserverInfoValue.setClientInfoValue(clientInfoObj);

				ptrLogInHelper->setEditedServerInfo(cserverInfoValue);
				double clientID = ptrIAppFramework->getClientID();
				//CA_NUM("clientID: ",clientID);
				result = ptrLogInHelper->editServerInfo(cserverInfoValue,clientID);
				//if(result==kTrue)
					//CAlert::InformationAlert("TRUE");
			}
            
            IControlView* iIncludeListNameCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputIncludeListNameWidgetID);
			if(iIncludeListNameCntrlView==nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iIncludeListNameCntrlView == nil");
				return;
			}
            iIncludeListNameCntrlView->Enable();

		}

		if(theSelectedWidget == kOutputIncludeTableHeaderWidgetID && theChange == kTrueStateMessage) 
		{
			isAddTableHeader = kTrue;
			
			InterfacePtr<ILoginHelper> ptrLogInHelper((static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss , IID_ILOGINHELPER /*ILoginHelper::kDefaultIID*/))));
			if(ptrLogInHelper == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::Pointer to ptrLogInHelper is nil.");
				return ;
			}
			LoginInfoValue cserverInfoValue ;
			bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue) ;
			if(result)
			{									
				ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();
				clientInfoObj.setisAddTableHeaders(1) ;		
				cserverInfoValue.setClientInfoValue(clientInfoObj);

				ptrLogInHelper->setEditedServerInfo(cserverInfoValue);
				double clientID = ptrIAppFramework->getClientID();
				result = ptrLogInHelper->editServerInfo(cserverInfoValue,clientID);
			}
		}
		if(theSelectedWidget == kOutputIncludeTableHeaderWidgetID && theChange != kTrueStateMessage) 
		{
			isAddTableHeader = kFalse;
			
			InterfacePtr<ILoginHelper> ptrLogInHelper((static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss , IID_ILOGINHELPER /*ILoginHelper::kDefaultIID*/))));
			if(ptrLogInHelper == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLActionComponent::DoAction::Pointer to ptrLogInHelper is nil.");
				return ;
			}
			LoginInfoValue cserverInfoValue ;
			bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue) ;
			if(result)
			{	
				ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();
				clientInfoObj.setisAddTableHeaders(0) ;	
				cserverInfoValue.setClientInfoValue(clientInfoObj);

				ptrLogInHelper->setEditedServerInfo(cserverInfoValue);
				double clientID = ptrIAppFramework->getClientID();
				result = ptrLogInHelper->editServerInfo(cserverInfoValue,clientID);
			}
		}
        if(theSelectedWidget == kOutputIncludeListNameWidgetID && theChange == kTrueStateMessage)
			isAddListName = kTrue;
        
        if(theSelectedWidget == kOutputIncludeListNameWidgetID && theChange != kTrueStateMessage)
			isAddListName = kFalse;
        
        if(theSelectedWidget == kOutputAsSwatchWidgetID && theChange == kTrueStateMessage)
			isOutputAsSwatch = kTrue;
        
		if(theSelectedWidget == kOutputAsSwatchWidgetID && theChange != kTrueStateMessage)
			isOutputAsSwatch = kFalse;
        
		if(theSelectedWidget == kOutputAsImageDescriptionWidgetID && theChange == kTrueStateMessage) 
			isAddImageDescription = kTrue;
			
		if(theSelectedWidget == kOutputAsImageDescriptionWidgetID && theChange != kTrueStateMessage) 
			isAddImageDescription = kFalse;

		if(theSelectedWidget == kOutputAsFieldNameCheckBoxWidgetID && theChange == kTrueStateMessage) 
			isAddAsDisplayName = kTrue;
			
		if(theSelectedWidget == kOutputAsFieldNameCheckBoxWidgetID && theChange != kTrueStateMessage) 
			isAddAsDisplayName = kFalse;

		if(theSelectedWidget == kDelUnSprayedFieldsWidgetID && theChange == kTrueStateMessage) 
			deleteIfEmpty = kTrue;
		if(theSelectedWidget == kDelUnSprayedFieldsWidgetID && theChange != kTrueStateMessage) 
			deleteIfEmpty = kFalse;

		//---------------
		//if(theSelectedWidget == kHorizontalFlowCheckBoxWidgetID && theChange == kTrueStateMessage)
		//{
		//	CA("HorizontalFlow True"); 
		//	isHorizontalFlow=kTrue; 
		//}
		//if(theSelectedWidget == kHorizontalFlowCheckBoxWidgetID && theChange == kFalseStateMessage)
		//{
		//	CA("HorizontalFlow False");
		//	isHorizontalFlow=kFalse;  
		//}

				
		if(theSelectedWidget==kHorizontalFlowRadioButtonWidgetID && theChange==kTrueStateMessage )
		{
			isHorizontalFlow = kTrue;
		}
		else if(theSelectedWidget==kVerticalFlowRadioButtonWidgetID && theChange==kTrueStateMessage )
		{
			isHorizontalFlow = kFalse;
		}
		if(theSelectedWidget==kSpreadBasedLetterKeysWidgetID && theChange==kTrueStateMessage)
		{
			//CA("isSpreadBasedLetterKeys True");
			isSpreadBasedLetterKeys=kTrue;
		}
		if(theSelectedWidget==kSpreadBasedLetterKeysWidgetID && theChange==kFalseStateMessage)
		{
			//CA("isSpreadBasedLetterKeys False");
			isSpreadBasedLetterKeys=kFalse;
		}
	
	} while (kFalse);
}

void TPLSelectionObserver::UpdatePanel()
{
	//CA("updatepanel");
	do
	{
		InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
		if (iPanelControlData == nil)
		{
			//CA("iPanelControlData == nil");
			break;
		}

		IControlView* iControlView = iPanelControlData->FindWidget(kTPLDropDownWidgetID);
		if (iControlView == nil)
		{
			//CA("iControlView == nil");		
			break;
		}

		InterfacePtr<IDropDownListController> iDropDownListController(iControlView, UseDefaultIID());
		if (iDropDownListController == nil)
		{
			//CA("iDropDownListController == nil");		
			break;
		}
		InterfacePtr<IStringListControlData> iStringListControlData(iDropDownListController, UseDefaultIID());
		if (iStringListControlData == nil)
		{
			//CA("iStringListControlData == nil");		
			break;
		}
	}while(kFalse);
}

IPanelControlData* TPLSelectionObserver::QueryPanelControlData()
{
	//CA("QueryPanelControlData");
	IPanelControlData* iPanel = nil;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return iPanel;
	}
	do
	{
		InterfacePtr<IWidgetParent> iWidgetParent(this, UseDefaultIID());
		if (iWidgetParent == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::QueryPanelControlData::iWidgetParent == nil");
			break;
		}

		InterfacePtr<IPanelControlData> iPanelControlData(iWidgetParent->GetParent(), UseDefaultIID());
		if (iPanelControlData == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::QueryPanelControlData::iPanelControlData == nil");		
			break;
		}
		iPanelControlData->AddRef();
		iPanel = iPanelControlData;
	}
	while (false); 
	return iPanel;
}

void TPLSelectionObserver::AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	//CA("AttachWidget");
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil)
			break;
		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil)
			break;
		iSubject->AttachObserver(this, interfaceID);
	}
	while (false); 
}

void TPLSelectionObserver::DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	//CA("DetachWidget");
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil)
			break;
		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil)
			break;
		iSubject->DetachObserver(this, interfaceID);
	}
	while (false); 
}

void TPLSelectionObserver::HandleDropDownListClick
(const ClassID& theChange, 
ISubject* theSubject, 
const PMIID& protocol, 
void* changedBy)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return;
	}
	do
	{
		//CA("TPLSelectionObserver::HandleDropDownListClick");		
		InterfacePtr<IControlView> iControlView(theSubject, UseDefaultIID());
		if (iControlView == nil)
		{
			//ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPSelectionObserver::HandleDropDownListClick::iControlView == nil");
			break;
		}

		WidgetID widgetID = iControlView->GetWidgetID();

		InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
		if (iPanelControlData == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::HandleDropDownClick::iPanelControlData == nil");			
			break;
		}

		IControlView* iDropDownControlView = iPanelControlData->FindWidget(kTPLDropDownWidgetID);
		if(iDropDownControlView == nil)
		{
			//ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::HandleDropDownClick::iDropDownControlView == nil");
			break;
		}

		InterfacePtr<IDropDownListController> iDropDownListController(iDropDownControlView, UseDefaultIID());
		if (iDropDownListController == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::HandleDropDownClick::iDropDownListController == nil");
			break;
		}

		InterfacePtr<IStringListControlData> TabDropListData(iDropDownListController, UseDefaultIID());
		if (TabDropListData == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iStringListControlData invalid");
			break;
		}
		//TabDropListData->Clear(kFalse,kFalse);


		if (widgetID.Get() == kTPLDropDownWidgetID)
		{
			//CAlert::InformationAlert("widgetID.Get() == kTPLDropDownWidgetID");
			int32 selectedRowIndex=0;

			selectedRowIndex = iDropDownListController->GetSelected();

			IControlView* iItemListCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsItemListWidgetID);
			if(iItemListCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLCommonFunctions::attachAttributes::iItemListCntrlView == nil");
				return ;
			}
            
            InterfacePtr<ITriStateControlData> itristatecontroldata(iItemListCntrlView, UseDefaultIID());
			if(itristatecontroldata==nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata == nil");
				return;
			}
            
            
            
			IControlView * itemListTextControlView = TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kItemListTextWidgetID);
			if(itemListTextControlView == nil)
			{
				//CA("itemListTextControlView == nil");
				break;
			}
					
			IControlView* iTblGrpCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTableGroupPanelWidgetID);
			if(iTblGrpCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iTblGrpCntrlView == nil");
				return ;
			}
						
			IControlView* iAttrGrpCntrlView = TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kAttrGroupPanelWidgetID);
			if(iAttrGrpCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iAttrGrpCntrlView == nil");
				return ;
			}
			
			IControlView* iImgGrpCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kImageGroupPanelWidgetID);
			if(iImgGrpCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iImgGrpCntrlView == nil");
				return ;
			}

			IControlView* iSprayItemPerFrameCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kSprayItemPerFrameWidgetID);
			if(iSprayItemPerFrameCntrlView==nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::iSprayItemPerFrameCntrlView is nil");	
				return ;
			}

			InterfacePtr<ITriStateControlData> itristatecontroldata5(iSprayItemPerFrameCntrlView, UseDefaultIID());
			if(itristatecontroldata5==nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::itristatecontroldata5 is nil");		
				return ;
			}
		
			/*itristatecontroldata5->Deselect();
			iSprayItemPerFrameCntrlView->Enable();*/
			//--------------------------till here

			if(selectedRowIndex != 0)
			{
				//CAlert::InformationAlert("Default item list to disable line no. 2418 of selectionobserver");
				int32 selectedindex1 = iDropDownListController->GetSelected();
				PMString abc("selectedindex1: ");
				abc.AppendNumber(selectedindex1);
				//CA(abc);
			
				iTblGrpCntrlView->HideView();
				iTblGrpCntrlView->Disable();
				iImgGrpCntrlView->HideView();
				iImgGrpCntrlView->Disable();
				iAttrGrpCntrlView->ShowView();
				iAttrGrpCntrlView->Enable();

				iItemListCntrlView->Disable();
				itemListTextControlView->Disable();

				iSprayItemPerFrameCntrlView->Disable();                      // it is Disable();
				itristatecontroldata5->Deselect();

			

				/*AP*/
				/*if(PM_Product_Level ==3){
					selectedRowIndex +=0; 
				}
				else if(PM_Product_Level == 2){
					selectedRowIndex +=1;				
				}
				else if(PM_Product_Level == 1)*/
				{
					selectedRowIndex +=2;		// 3= Item Group, 4= Item, 0 = Event/Category			
					iItemListCntrlView->Enable();
					itemListTextControlView->Enable();
					
				}
				
				IControlView* iAttributeNameCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsAttributeNameWidgetID);
				if(iAttributeNameCntrlView==nil) 
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iAttributeNameCntrlView == nil");
					return ;
				}


				InterfacePtr<ITriStateControlData> itristatecontroldata2(iAttributeNameCntrlView, UseDefaultIID());
				if(itristatecontroldata2==nil) 
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata2 == nil");			
					return;
				}

				IControlView* iIncludeHeaderCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsIncludeHeaderWidgetID);
				if(iIncludeHeaderCntrlView==nil) 
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iIncludeHeaderCntrlView == nil");
					return ;
				}

				InterfacePtr<ITriStateControlData> itristatecontroldata1(iIncludeHeaderCntrlView, UseDefaultIID());
				if(itristatecontroldata1==nil) 
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata1 == nil");			
					return;
				}

				IControlView* iDelUnSprayedCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kDelUnSprayedFieldsWidgetID);
				if(iDelUnSprayedCntrlView==nil) 
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::iDelUnSprayedCntrlView == nil");
					return;
				}

				InterfacePtr<ITriStateControlData> itristatecontroldata3(iDelUnSprayedCntrlView, UseDefaultIID());
				if(itristatecontroldata1==nil) 
				{
					ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata1 == nil");			
					return;
				}
					
				//IControlView* iImgGrpCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kImageGroupPanelWidgetID);
				//if(iImgGrpCntrlView==nil) 
				//{
				//	ptrIAppFramework->LogDebug("AP7_TemplateBuilder::PnlTrvNodeEH::LButtonDn::iImgGrpCntrlView == nil");
				//	return ;
				//}

				//IControlView* iAttrGrpCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kAttrGroupPanelWidgetID);
				//if(iAttrGrpCntrlView==nil) 
				//{
				//	ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iAttrGrpCntrlView == nil");
				//	return ;
				//}

				//IControlView* iTblGrpCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTableGroupPanelWidgetID);
				//if(iTblGrpCntrlView==nil) 
				//{
				//	ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::iTblGrpCntrlView == nil");
				//	return ;
				//}
				
				if(iDropDownListController->GetSelected()==1) // item group
				{
					itristatecontroldata2->Deselect();
					iAttributeNameCntrlView->Enable();
					
					itristatecontroldata1->Deselect();
					iDelUnSprayedCntrlView->Enable();
                    
					itristatecontroldata->Deselect();
					iItemListCntrlView->Disable();
					itemListTextControlView->Disable();
				}

				else if(iDropDownListController->GetSelected()==2) // item
				{
					itristatecontroldata2->Deselect();
					iAttributeNameCntrlView->Enable();
					
					itristatecontroldata3->Deselect();
					iDelUnSprayedCntrlView->Enable();

					iItemListCntrlView->Enable();
					itemListTextControlView->Enable();
				}
				else
				{
					//CAlert::InformationAlert("else of iDropDownListController->GetSelected()");

					itristatecontroldata2->Deselect();
					iAttributeNameCntrlView->Enable();
					
					itristatecontroldata1->Deselect();
					iDelUnSprayedCntrlView->Enable();
				}


				//{
				//	CAlert::InformationAlert("iDropDownListController->Select(4)");
				//	itristatecontroldata2->Deselect();
				//	iAttributeNameCntrlView->Enable();
				//	
				//	itristatecontroldata1->Deselect();
				//	iDelUnSprayedCntrlView->Enable();

				//	iItemListCntrlView->Enable();
				//	itemListTextControlView->Enable();

				//}
				//else if(iDropDownListController->Select(3))
				//{
				//	CAlert::InformationAlert("iDropDownListController->Select(3)");
				//	itristatecontroldata2->Deselect();
				//	iAttributeNameCntrlView->Enable();
				//	
				//	itristatecontroldata1->Deselect();
				//	iDelUnSprayedCntrlView->Enable();
				//}
				//else if(iDropDownListController->Select(0))
				//{
				//	CAlert::InformationAlert("iDropDownListController->Select(0)");
				//	itristatecontroldata2->Deselect();
				//	iAttributeNameCntrlView->Enable();
				//	
				//	itristatecontroldata1->Deselect();
				//	iDelUnSprayedCntrlView->Enable();

				//}
				//else{}
				////-------------------------------------------
			}
			
			if(selectedRowIndex == 0)
			{
				iTblGrpCntrlView->HideView();
				iTblGrpCntrlView->Disable();
				iImgGrpCntrlView->HideView();
				iImgGrpCntrlView->Disable();
				iAttrGrpCntrlView->ShowView();
				iAttrGrpCntrlView->Enable();

                itristatecontroldata->Deselect();
				iItemListCntrlView->Disable();
				itemListTextControlView->Disable();

			//----------------------- mahesh added
				iSprayItemPerFrameCntrlView->Disable();                 // it is disable();
				itristatecontroldata5->Deselect();
			//--------------------------

			}
			SelectedRowNo = selectedRowIndex;
			//CA_NUM("selectedRowIndex  :  ",selectedRowIndex);
			IControlView* ProjectPanelCtrlView = iPanelControlData->FindWidget(kProjectTreeViewWidgetID);
			if (ProjectPanelCtrlView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::HandleDropDownListClick::ProjectPanelControlView == nil");
				break;
			}
			
		}
		InterfacePtr<ITreeViewMgr> treeViewMgr(TPLMediatorClass::ProjectLstboxCntrlView, UseDefaultIID());
		if(!treeViewMgr)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::HandleDropDownListClick::treeViewMgr is nil");
			return;
		}
			
		TPLTreeModel pModel;
		PMString pfName("Root");

		switch(SelectedRowNo)
		{
			case 0:
				pModel.setRoot(-1, pfName, 1);
				break;
			case 1:
				pModel.setRoot(-1, pfName, 2);
				break;
			case 2:
				pModel.setRoot(-1, pfName, 3);
				break;
			case 3:
				pModel.setRoot(-1, pfName, 4);
				break;
			case 4:
				pModel.setRoot(-1, pfName, 5);
				break;
		}

		treeViewMgr->ClearTree(kTrue);
		pModel.GetRootUID();
		treeViewMgr->ChangeRoot();

		if(SelectedRowNo != 4)
		{
			InterfacePtr<ICategoryBrowser> CatalogBrowserPtr((ICategoryBrowser*)::CreateObject(kCTBCategoryBrowserBoss, IID_ICATEGORYBROWSER));
			if(!CatalogBrowserPtr)
			{
				ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::HandleDropDownListClick::Pointre to TemplateBuilderPtr not found");
				return ;
			}
			CatalogBrowserPtr->CloseCategoryBrowser();
		}

		//-------
		if(ISTabbedText)
		{
			//CAlert::InformationAlert("ISTabbedText");
			//CA("ISTabbedText");
			IControlView* tableListdropdownCtrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTPLTableListDropDownWidgetID);
			if(tableListdropdownCtrlView==nil) 
			{
				CAlert::InformationAlert("tableListdropdownCtrlView==nil");
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::HandleDropDownListClick::tableListdropdownCtrlView == nil");
				return;
			}

			InterfacePtr<IDropDownListController> tableListDropDownController(tableListdropdownCtrlView, UseDefaultIID());
			if (tableListDropDownController == nil)
			{
				CAlert::InformationAlert("tableListDropDownController == nil");
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::HandleDropDownListClick::tableListDropDownController == nil");				
				break;		
			}

 			InterfacePtr<IStringListControlData> tablelistDropDownData(tableListDropDownController, UseDefaultIID());
			if (tablelistDropDownData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::HandleDropDownListClick::tablelistDropDownData invalid");
				break;
			}

			IControlView* iAttNameCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kOutputAsAttributeNameWidgetID);
			if(iAttNameCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::HandleDropDownListClick::iAttNameCntrlView == nil");
				return;
			}

			IControlView* iDelUnSprayedCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kDelUnSprayedFieldsWidgetID);
			if(iDelUnSprayedCntrlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::HandleDropDownListClick::iDelUnSprayedCntrlView == nil");
				return;
			}

			tablelistDropDownData->Clear(kFalse,kFalse);
		
			
			if(SelectedRowNo == 4)
			{
				//CA("Item");
				tablelistDropDownData->AddString("All", 0 );

				tableListdropdownCtrlView->ShowView();
				iAttNameCntrlView->HideView();
				iDelUnSprayedCntrlView->HideView();

				int32 itemListSize = static_cast<int32>(ITEMDataNodeList.size());
				
				for(int32 i = 0; i < itemListSize ; i++)
				{
					if(ITEMDataNodeList[i].getHitCount() == 8 ) 
					{
						tablelistDropDownData->AddString(ITEMDataNodeList[i].getName(), i );
					}
					/*if(ITEMDataNodeList[i].getHitCount() == 2 ) 
					{
						tablelistDropDownData->AddString(ITEMDataNodeList[i].getName(), i );
					}*/
					
				}
				tableListDropDownController->Select(0);
			}
			else
			{
				//CA("else");
				table_TypeID = -1;
				tableListdropdownCtrlView->HideView();
				iAttNameCntrlView->ShowView();
				iDelUnSprayedCntrlView->ShowView();
				iDelUnSprayedCntrlView->Enable();   // added 2nd april by mahesh
				
			}
			
		}
		else
		{
			//CA("!!!!!ISTabbedText  Drop Down");
		}


	}while(false); 
}

void TPLSelectionObserver::populatePFPanelLstbox()
{	//CA("Populate PF Panel list box");
	int32 count1=0;
	do
	{
		
//		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//		if(ptrIAppFramework == nil)
//			break;
//
//		if(TPLMediatorClass::iPanelCntrlDataPtr==nil)
//		{
//			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::populatePFPanelLstbox::Not getting iPanelCntrlDataPtr");
//			break;
//		}
//
//		PFDataNodeList.clear();
//		TPLDataNode PFListData;
///*
//public String PRODUCT_FAMILY_ELEMENT 	= "PF_ELEMENT";
//public String PRODUCT_GROUP_ELEMENT 	= "PG_ELEMENT";
//public String PRODUCT_ELEMENT 			= "PR_ELEMENT";
//public String PF_ASSETS = "PFTY"; // PF
//public String PG_ASSETS = "PGTY"; // PG
//public String PR_ASSETS = "PRTY"; // PR
//public String PRD_IMAGES = "PRIM"; // Item
//*/
//
//		//long typeId = ptrIAppFramework->TYPEMngr_getObjectTypeID("PRODUCT_FAMILY_ELEMENT");
//		//if(typeId==-1)
//		//{
//		//	//CA("Invalid typeid");
//		//	break;
//		//}
//				
//		VectorElementInfoPtr eleValObj = ptrIAppFramework->ElementCache_getCopyAttributesByIndex(1, TPLMediatorClass::CurrLanguageID );
//		if(eleValObj==nil)
//		{
//			ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::populatePFPanelLstbox::ElementCache_getCopyAttributesByIndex's eleValObj is nil");		
//			break;
//		}
//
//		VectorElementInfoValue::iterator it;
//		int i=0;
//		PFData.clearVector(1);
//		//PFData.setData
//		//	(1,
//		//	 i,
//		//	 -1,
//		//	 -1,
//		//	 "", 
//		//	 "PF Name",
//		//	 kTrue,
//		//	 "",
//		//	 kFalse,
//		//	 0	
//		//	 );
//
//		//PFListData.setAll("PF Name",-1,1,1,0,0);
//		//PFListData.setHitCount(0);
//		//PFDataNodeList.push_back(PFListData);
//		////listHelper.AddElement(lstboxControlView, "PF Name", kPFPanelTextWidgetID, i);
//		////listHelper.AddIcons(lstboxControlView, i, 0); 
//		//// 3rd param 0: for Text/Copy 1: for Image and 2: Table
//		//count1=i;
//		//i++;
//	
//        
//		for(it=eleValObj->begin();it!=eleValObj->end();it++)
//		{			
//			PFData.setData
//			(1,
//			 i,
//			 it->getElementId(),
//			 it->getElement_type_id(),
//			 it->getLanguage_id(), 
//			 it->getDisplay_name(),
//			 kTrue,
//			 "",
//			 kFalse,
//			 0
//			 );
//
//			PFListData.setAll(it->getDisplay_name(),it->getElementId(),1,1,0,0);
//			PFListData.setHitCount(0);
//			PFDataNodeList.push_back(PFListData);			
//			count1=i;
//			i++;			
//		}
//
////		delete eleValObj;
//
//		VectorTypeInfoPtr typeValObj = ptrIAppFramework->ElementCache_getImageAttributesByIndex(1);
//		if(typeValObj==NULL)
//			break;
//
//		if(typeValObj->size() ==0)
//		{
//			//ptrIAppFramework->LogInfo("AP7_TemplateBuilder::TPLSelectionObserver::populatePFPanelLstbox::ElementCache_getImageAttributesByIndex'stypeValObj->size = 0");
//			break;
//		}
//
//		VectorTypeInfoValue::iterator it1;
//
//		for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
//		{			
//			PFData.setData
//			(1,
//			 i,
//			 -1,
//			 it1->getType_id(),
//			 TPLMediatorClass::CurrLanguageID, 
//			 it1->getName(),
//			 kTrue,
//			 it1->getCode(),
//			 kTrue,
//			 0
//			 );
//
//			PFListData.setAll(it1->getName(),it1->getType_id(),1,1,0,1);
//			PFListData.setHitCount(1);
//			PFDataNodeList.push_back(PFListData);
//			count1=i;
//			i++;			
//		}
//		if(typeValObj)
//			delete typeValObj;
//		if(eleValObj)
//			delete eleValObj;
		
	}while(kFalse);
}

void TPLSelectionObserver::populatePGPanelLstbox()
{
	//CA("TPLSelectionObserver::populatePGPanelLstbox");
	int32 count2=0;
	do
	{
	//	if(TPLMediatorClass::iPanelCntrlDataPtr==nil)
	//	{
	//	//	CA("Not getting iPanelCntrlDataPtr");
	//		break;
	//	}

	//	/*SDKListBoxHelper listHelper(this, kTPLPluginID);
	//	listHelper.EmptyCurrentListBox(TPLMediatorClass::iPanelCntrlDataPtr, 2);

	//	IControlView* lstboxControlView = listHelper.FindCurrentListBox
	//		(TPLMediatorClass::iPanelCntrlDataPtr, 2);
	//	if(lstboxControlView==nil)
	//		break;
	//	
	//	TPLMediatorClass::PGLstboxCntrlView=lstboxControlView;
	//	lstboxControlView->Show(kTrue);*/

	//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//	if(ptrIAppFramework == nil)
	//		break;
	//	
	//	VectorElementInfoPtr eleValObj = ptrIAppFramework->ElementCache_getCopyAttributesByIndex(2, TPLMediatorClass::CurrLanguageID);
	//	if(eleValObj==nil)
	//	{
	//		ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::populatePGPanelLstbox::ElementCache_getCopyAttributesByIndex's eleValObj is nil");
	//		break;
	//	}

	//	PGDataNodeList.clear();
	//	TPLDataNode PGListData;

	//	VectorElementInfoValue::iterator it;
	//	int i=0;
	//	PGData.clearVector(2);
	//	/*PGData.setData
	//		(2,
	//		 i,
	//		 -1,
	//		 -1,
	//		 "", 
	//		 "PG Name",
	//		 kTrue,
	//		 "",
	//		 kFalse,
	//		 0
	//		 );

	//	PGListData.setAll("PG Name",-1,1,1,0,0);
	//	PGListData.setHitCount(0);
	//	PGDataNodeList.push_back(PGListData);*/

	//	//listHelper.AddElement(lstboxControlView, "PG Name", kPGPanelTextWidgetID, i);
	//	//listHelper.AddIcons(lstboxControlView, i, 0); 
	//	//count2=i;
	//	//i++;
	//	

	//	for(it=eleValObj->begin();it!=eleValObj->end();it++)
	//	{			
	//		PGData.setData
	//			(2,
	//			 i,
	//			 it->getElementId(),
	//			 it->getElement_type_id(),
	//			 it->getLanguage_id(), 
	//			 it->getDisplay_name(),
	//			 kTrue,
	//			 "",
	//			 kFalse,
	//			 0
	//			 );

	//		PGListData.setAll(it->getDisplay_name(),it->getElementId(),1,1,0,0);
	//		PGListData.setHitCount(0);
	//		PGDataNodeList.push_back(PGListData);			
	//		count2=i;
	//		i++;			
	//	}

	////	delete eleValObj;

	//	VectorTypeInfoPtr typeValObj = ptrIAppFramework->ElementCache_getImageAttributesByIndex(2);
	//	if(typeValObj==nil)
	//	{
	//		ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::populatePGPanelLstbox::ElementCache_getImageAttributesByIndex's typeValObj is nil");
	//		break;
	//	}

	//	VectorTypeInfoValue::iterator it1;

	//	for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
	//	{
	//		PGData.setData
	//		(2,
	//		 i,
	//		 -1,
	//		 it1->getType_id(),
	//		 TPLMediatorClass::CurrLanguageID, 
	//		 it1->getName(),
	//		 kTrue,
	//		 it1->getCode(),
	//		 kTrue,
	//		 0
	//		 );

	//		PGListData.setAll(it1->getName(),it1->getType_id(),1,1,0,1);
	//		PGListData.setHitCount(1);
	//		PGDataNodeList.push_back(PGListData);		
	//		count2=i;
	//		i++;			
	//	}
	//	if(eleValObj)
	//		delete eleValObj;
	//	if(typeValObj)
	//		delete typeValObj;
		
	}while(kFalse);
}


void TPLSelectionObserver::populatePRPanelLstbox()
{
	//CA("****TPLSelectionObserver::populatePRPanelLstbox**********");
	int32 count3=0;
	do
	{
		
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;

		if(TPLMediatorClass::iPanelCntrlDataPtr==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::populatePRPanelLstbox::Not getting iPanelCntrlDataPtr");
			break;
		}

		/*SDKListBoxHelper listHelper(this, kTPLPluginID);
		listHelper.EmptyCurrentListBox(TPLMediatorClass::iPanelCntrlDataPtr,3);

		IControlView* lstboxControlView=listHelper.FindCurrentListBox(TPLMediatorClass::iPanelCntrlDataPtr,3);
		if(lstboxControlView==nil)
			break;

		TPLMediatorClass::PRLstboxCntrlView=lstboxControlView;
		lstboxControlView->Show(kTrue);*/

		
/*
public String PRODUCT_FAMILY_ELEMENT 	= "PF_ELEMENT";
public String PRODUCT_GROUP_ELEMENT 	= "PG_ELEMENT";
public String PRODUCT_ELEMENT 			= "PR_ELEMENT";
*/		int i=0;
		PRDataNodeList.clear();
		TPLDataNode PRListData;
		PRData.clearVector(3);



		do
		{
			/*CA("Inside Pppulate PR LIST Box");
			PMString ASD("languageID : " );
			ASD.AppendNumber(TPLMediatorClass::CurrLanguageID);
			CA(ASD);*/
			//CA("first call");
			VectorElementInfoPtr eleValObj = ptrIAppFramework->StructureCache_getItemGroupCopyAttributesByLanguageId(TPLMediatorClass::CurrLanguageID);
			if(eleValObj==nil){
				ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::populatePRPanelLstbox::StructureCache_getItemGroupCopyAttributesByLanguageId's eleValObj==nil");
				break;
			}
			//CA("eleValObj== not nil ");
			VectorElementInfoValue::iterator it;
			
			for(it=eleValObj->begin();it!=eleValObj->end();it++)   //// changed By Rahul ...Nov17
			{	
				//getPicklistGroupId() > 0
				if(ptrIAppFramework->StructureCache_isElementMPV(it->getElementId()))
				{

					//CA("MPV AttributeFound");
					PRData.setData
					(3,
					i,
					it->getElementId(),
					-1/*it->getElement_type_id()*/,
					TPLMediatorClass::CurrLanguageID,  //it->getLanguage_id(), 
					it->getDisplayName() ,
					kTrue,
					"",
					kFalse,
					0,
                    kFalse,
                    -1,-1,-1,-1,3   // seting value 3 in SprayItemPerFrame to indicate PV attribute.
					);
					//CA(it->getDisplay_name());
					PRListData.setAll((it->getDisplayName()),it->getElementId(),1,1,0,0);
					PRListData.setHitCount(0);
					PRDataNodeList.push_back(PRListData);		
					i++;
					count3=i;

					/*PRData.setData
					(3,
					i,
					it->getElementId(),
					it->getElement_type_id(),
					TPLMediatorClass::CurrLanguageID,  //it->getLanguage_id(), 
					it->getDisplay_name() + " - Name",
					kTrue,
					"-201",
					kFalse,
					0
					);
					//CA(it->getDisplay_name());
					PRListData.setAll((it->getDisplay_name() + " - Name"),it->getElementId(),1,1,0,0);
					PRListData.setHitCount(0);
					PRDataNodeList.push_back(PRListData);		
					i++;
					count3=i;

					PRData.setData
					(3,
					i,
					it->getElementId(),
					it->getElement_type_id(),
					TPLMediatorClass::CurrLanguageID,  //it->getLanguage_id(), 
					it->getDisplay_name() + " - Abbvr",
					kTrue,
					"-202",
					kFalse,
					0
					);
					//CA(it->getDisplay_name());
					PRListData.setAll((it->getDisplay_name() + " - Abbvr"),it->getElementId(),1,1,0,0);
					PRListData.setHitCount(0);
					PRDataNodeList.push_back(PRListData);		
					i++;
					count3=i;

					PRData.setData
					(3,
					i,
					it->getElementId(),
					it->getElement_type_id(),
					TPLMediatorClass::CurrLanguageID,  //it->getLanguage_id(), 
					it->getDisplay_name() + " - Description",
					kTrue,
					"-203",
					kFalse,
					0
					);
					//CA(it->getDisplay_name());
					PRListData.setAll((it->getDisplay_name() + " - Description"),it->getElementId(),1,1,0,0);
					PRListData.setHitCount(0);
					PRDataNodeList.push_back(PRListData);		
					i++;
					count3=i;*/

				}
				//else if(it->getPub_Item())  /*AP*/
				//{
				//	PRData.setData
				//	(3,
				//	i,
				//	it->getElementId(),
				//	-1/*it->getElement_type_id()*/,
				//	TPLMediatorClass::CurrLanguageID,  //it->getLanguage_id(), 
				//	it->getDisplay_name(),
				//	kTrue,
				//	"",
				//	kFalse,
				//	0,
				//	kTrue
				//	);
				//	PRListData.setAll(it->getDisplay_name(),it->getElementId(),1,1,0,0);
				//	PRListData.setHitCount(6);
				//	PRDataNodeList.push_back(PRListData);		
				//	i++;
				//	count3=i;
				//}
				else
				{
					PRData.setData
					(3,
					i,
					it->getElementId(),
					-1,
					TPLMediatorClass::CurrLanguageID,   
					it->getDisplayName(),
					kTrue,
					"",
					kFalse,
					0
					);
					PRListData.setAll(it->getDisplayName(),it->getElementId(),1,1,0,0);
					PRListData.setHitCount(0);
					PRDataNodeList.push_back(PRListData);		
					i++;
					count3=i;


					if(it->getSysName() == "SUPPLIER_ID")
					{
						PMString dispName1("Vendor Description 1");
						dispName1.SetTranslatable(kFalse);
						PRData.setData
						(3,
						i,
						-225,
						-1,
						TPLMediatorClass::CurrLanguageID,  
						
						dispName1,
						kTrue,
						"",
						kFalse,
						0
						);
						PRListData.setAll(dispName1,-225,1,1,0,0);
						PRListData.setHitCount(0);
						PRDataNodeList.push_back(PRListData);		
						i++;
						count3=i;

						PMString dispName2("Vendor Description 2");
						dispName2.SetTranslatable(kFalse);
						PRData.setData
						(3,
						i,
						-226,
						-1,
						TPLMediatorClass::CurrLanguageID,  
						dispName2,
						kTrue,
						"",
						kFalse,
						0
						);
						PRListData.setAll(dispName2,-226,1,1,0,0);
						PRListData.setHitCount(0);
						PRDataNodeList.push_back(PRListData);		
						i++;
						count3=i;
					}
				}
			}
//16-may chetan
					////PRData.setData
					////(3,
					////i,
					////-801,//it->getElementId(),
					////-1,//it->getElement_type_id(),
					////TPLMediatorClass::CurrLanguageID,  //it->getLanguage_id(), 
					////"Alphabet Index",//it->getDisplay_name(),
					////kTrue,
					////"",
					////kFalse,
					////0
					////);
					////PRListData.setAll(/*it->getDisplay_name()*/"Alphabet Index",/*it->getElementId()*/-801,1,1,0,0);
					////PRListData.setHitCount(0);
					////PRDataNodeList.push_back(PRListData);		
					////i++;
					////count3=i;



			PRData.setData
			(3,
			i,
			-802,//it->getElementId(),
			-1,//it->getElement_type_id(),
			TPLMediatorClass::CurrLanguageID,  //it->getLanguage_id(), 
			"New Indicator",//it->getDisplay_name(),
			kTrue,
			"",
			kFalse,
			0
			);
			PRListData.setAll(/*it->getDisplay_name()*/"New Indicator",/*it->getElementId()*/-802,1,1,0,0);
			PRListData.setHitCount(0);
			PRDataNodeList.push_back(PRListData);		
			i++;
			count3=i;
//16-may 
			if(eleValObj)
				delete eleValObj;


			PRData.setData
			(	3,						// which List 4 = item List
				i,						// Index	
				-803,//it2->getAttribute_id(),// Attribute Id for Core element
				-1,					// type id ... here we are going to feed Item id
				TPLMediatorClass::CurrLanguageID,			
				"Letter Keys",//,it2->getDisplay_name(), 
				kTrue,
				"Letter Keys",// Code is replaced by Attribute Name
				kFalse,
				0           //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
			);
			

			PRListData.setAll("Letter Keys",/*it2->getAttribute_id()*/-803,1,1,0,0);
			PRListData.setHitCount(0);				
			PRDataNodeList.push_back(PRListData);
			i++;
			count3=i;


			//****************************Added New Here Date 9/09/09
			PRData.setData
			(3,
			i,
			-827,//it->getElementId(),
			-3,//it->getElement_type_id(),
			TPLMediatorClass::CurrLanguageID,  //it->getLanguage_id(), 
			"Number Keys",//it->getDisplay_name(),
			kTrue,
			"",
			kFalse,
			0,
			kFalse,
			-1,-1,-1,-1,-1,-1,-1,-1,4
			);
			//CA("Letter Number");
			PRListData.setAll("Number Keys",-827,1,1,0,0);
			PRListData.setHitCount(0);
			PRDataNodeList.push_back(PRListData);		
			i++;
			count3=i;
			//****** Up To Here

			
		//-----------------Table ---------------------
			PRData.setData
			(3,
			i,
			-121,//it->getElementId(),
			-3,//it->getElement_type_id(),
			TPLMediatorClass::CurrLanguageID,  //it->getLanguage_id(), 
			"List_Table Name",//it->getDisplay_name(),
			kTrue,
			"",
			kFalse,
			0,
			kFalse,
			-1,-1,-1,-1,-1,-1,-1,-1,4
			);

			PRListData.setAll("List/Table Name",-121,1,1,0,0);
			PRListData.setHitCount(0);
			PRDataNodeList.push_back(PRListData);		
			i++;
			count3=i;

		////--12Dec2011 List/Table Level Fields--ElementID=-983 to -975 ,Type_ID=-55---
			PRData.setData
			(3,
			i,
			-983,//it->getElementId(),
			-55,//it->getElement_type_id(),
			TPLMediatorClass::CurrLanguageID,  //it->getLanguage_id(), 
			"List Description",//it->getDisplay_name(),
			kTrue,
			"",
			kFalse,
			0,
			kFalse,
			-1,-1,-1,-1,-1,-1,-1,-1,5
			);

			PRListData.setAll("List Description",-983,1,1,0,0);
			PRListData.setHitCount(0);
			PRDataNodeList.push_back(PRListData);		
			i++;
			count3=i;	
		
			PRData.setData
			(3,
			i,
			-982,//it->getElementId(),
			-55,//it->getElement_type_id(),
			TPLMediatorClass::CurrLanguageID,  //it->getLanguage_id(), 
			"Stencil Name",//it->getDisplay_name(),
			kTrue,
			"",
			kFalse,
			0,
			kFalse,
			-1,-1,-1,-1,-1,-1,-1,-1,5
			);

			PRListData.setAll("Stencil Name",-982,1,1,0,0);
			PRListData.setHitCount(0);
			PRDataNodeList.push_back(PRListData);		
			i++;
			count3=i;

			//---List Level Fields "Note 1 to 5"***element_ID -980 to -976 ***
			PMString List_name = "";
			double ele_ID = -981;
			for(int32 x=1; x<=5 ; x++)
			{
				ele_ID = ele_ID + 1;
				List_name.Clear();
				List_name.Append("Note ");
				List_name.AppendNumber(PMReal(x));
				PRData.setData
				(3,
				i,
				ele_ID, //-981,//it->getElementId(),
				-56,//it->getElement_type_id(),
				TPLMediatorClass::CurrLanguageID,  //it->getLanguage_id(), 
				List_name,//	"Note ",//it->getDisplay_name(),
				kTrue,
				"",
				kFalse,
				0,
				kFalse,
				-1,-1,-1,-1,-1,-1,-1,-1,5
				);

				PRListData.setAll(List_name/*"Note"*/,ele_ID/*-981*/,1,1,0,0);
				PRListData.setHitCount(0);
				PRDataNodeList.push_back(PRListData);		
				i++;
				count3=i;
			}
            
            //---Advance Table Level Fields "Note 1 to 5"***element_ID -975 to -971 ***
			List_name = "";
			ele_ID = -976;
			for(int32 x=1; x<=5 ; x++)
			{
				ele_ID = ele_ID + 1;
				List_name.Clear();
				List_name.Append("Adv. Table Note ");
				List_name.AppendNumber(PMReal(x));
				PRData.setData
				(3,
                 i,
                 ele_ID, //it->getElementId(),
                 -58,//it->getElement_type_id(), //Adv. Table Notes Type
                 TPLMediatorClass::CurrLanguageID,  //it->getLanguage_id(),
                 List_name,//	"Note ",//it->getDisplay_name(),
                 kTrue,
                 "",
                 kFalse,
                 0,
                 kFalse,
                 -1,-1,-1,-1,-1,-1,-1,-1,5
                 );
                
				PRListData.setAll(List_name, ele_ID, 1, 1, 0, 0);
				PRListData.setHitCount(0);
				PRDataNodeList.push_back(PRListData);		
				i++;
				count3=i;
			}

			//--Pivot List ------
			PRData.setData
			(3,
			i,
			-786,
			-57,
			TPLMediatorClass::CurrLanguageID,
			"Pivot List",
			kTrue,
			"",
			kFalse,
			0,
			kFalse,
			-1,-1,-1,-1,-1,-1,-1,-1,5
			);

			PRListData.setAll("Pivot List",-786,1,1,0,0);
			PRListData.setHitCount(0);
			PRDataNodeList.push_back(PRListData);		
			i++;
			count3=i;
	


	//--------------------------------------

		}while(0);
	///////////////// Adding Table types here ////////////////
/////////////////////////////////////////////////////////////////////////////
			//PRData.setData
			//(	3,
			//	i,
			//	-1,
			//	-116,
			//	TPLMediatorClass::CurrLanguageID, 
			//	"All Standard Tables",//it1->getName(),
			//	kTrue,
			//	"",
			//	kFalse,
			//	1 // For Tables only
			//);
			//
			////CA(it1->getName());
			//PRListData.setAll("All Standard Tables",-116,1,1,0,2);
			//PRListData.setHitCount(2);	
			//PRDataNodeList.push_back(PRListData);					
			////i++;
			//count3=i;
			//i++;		


/////////////////////////////////////////////////////////////////////////////


		do
		{
			VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
			if(typeValObj==nil)
			{
				ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::populatePRPanelLstbox::ElementCache_getProductTableTypes's typeValObj is nil");
				break;
			}

			VectorTypeInfoValue::iterator it1;

			for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
			{	//CA("8.1");	

				PRData.setData
					(3,
					i,
					-1,
					it1->getTypeId(),
					TPLMediatorClass::CurrLanguageID, 
					it1->getName(),
					kTrue,
					"",
					kFalse,
					1 ,// For Tables only
					kFalse,//For Event Field
					-1,//For Header
					-1,//for childTag
					1//for TableType
					);
				
				//CA(it1->getName());
				//PRListData.setAll(it1->getName(),it1->getType_id(),1,1,0,2);
				PMString temp(it1->getName());
				temp.Append(" (List)"/*" : List"*/);
				PRListData.setAll(temp,it1->getTypeId(),1,1,0,8);
				//PRListData.setHitCount(2);	
				PRListData.setHitCount(8);				
				PRDataNodeList.push_back(PRListData);					
				//i++;
				count3=i;
				i++;			
			}
			if(typeValObj)
				delete typeValObj;
	
		}while(0);

		///hardcoded table type for medtronic
		//If ConfigCache contains entry of ItemCustomTable then below method will return kTrue and so TemplateBuilder will contain ItemCustomTable stencils,
		//If method returns kFalse then there is no ItemCustomTable stencils in the templateBuilder...
		//bool16 result = ptrIAppFramework->ConfigCache_getDisplayItemCustomTable();

		//if(result == kTrue)
		//{
		//	PRData.setData
		//		(	3,
		//			i,
		//			-1,
		//			-111,
		//			TPLMediatorClass::CurrLanguageID, 
		//			"Custom Table",
		//			kTrue,
		//			"",
		//			kFalse,
		//			1 // For Tables only
		//		);
		//		
		//		//CA(it1->getName());
		//	PRListData.setAll("Custom Table",-111,1,1,0,2);
		//	
		//	PRListData.setHitCount(2);				
		//	PRDataNodeList.push_back(PRListData);					
		//	//i++;
		//	count3=i;
		//	i++;
		//}
		
///////		For Hybrid Table
		bool16 result1= kTrue ; //ptrIAppFramework->ConfigCache_getDisplayItemHybridTable();
		if(result1 == kTrue)
		{
			PRData.setData
					(3,
					i,
					-1,
					-115,
					TPLMediatorClass::CurrLanguageID, 
					"Advanced Table",
					kTrue,
					"",
					kFalse,
					1,
					kFalse,//For Event Field
					-1,//For Header
					-1,//for childTag
					3  //for TableType
					);
				
				//CA(it1->getName());
			PRListData.setAll("Advanced Table",-115,1,1,0,2);
			
			PRListData.setHitCount(2);				
			PRDataNodeList.push_back(PRListData);					
			//i++;
			count3=i;	i++;
		}

		/*
		if(CurrentClassID == -1)
		{
			//CA("CurrentClassID == -1");
			VectorClassInfoPtr vectClassInfoValuePtr = ptrIAppFramework->ClassificationTree_getRoot(1); // 1 default for English
			if(vectClassInfoValuePtr==nil)
			{		
				ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::populateItemPanelLstbox::ClassificationTree_getRoot's vectClassInfoValuePtr==nil");
				break;
			}
			VectorClassInfoValue::iterator it;
			it=vectClassInfoValuePtr->begin();
			CurrentClassID = it->getClass_id();

			if(vectClassInfoValuePtr)
				delete vectClassInfoValuePtr;
		}
		*/

		/*AP*/
		//VectorTypeInfoPtr typeValObj = ptrIAppFramework->AttributeCache_getHybridTableTypesForClassAndParents(CurrentClassID);
		//if(typeValObj!=nil)
		//{
		//	VectorTypeInfoValue::iterator it1;					
		//	PMString Name("");
		//	for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
		//	{
		//		//CA(it1->getName()/*getName()*/);
		//		Name = it1->getName();
		//		//Name.Append("_Hybrid");
		//		PRData.setData
		//		(	3,
		//			i,
		//			-1,
		//			it1->getType_id(),//getId(),
		//			TPLMediatorClass::CurrLanguageID, 
		//			Name,
		//			kTrue,
		//			it1->getCode(),
		//			kFalse,
		//			1,
		//			-1,
		//			-1,
		//			-1,
		//			3
		//		);
		//		
		//		PMString temp(Name);
		//		temp.Append(" (Table)"/*" : Table"*/);
		//		PRListData.setAll(temp,it1->getType_id(),1,1,0,2);
		//		PRListData.setHitCount(2);				
		//		PRDataNodeList.push_back(PRListData);					
		//	//	listHelper.AddElement(lstboxControlView, it1->getName(), kItemPanelTextWidgetID, i);
		//	//	listHelper.AddIcons(lstboxControlView, i, 1); 
		//		count3=i;
		//		i++;
		//	}
		//}
		//
		//if(typeValObj)		
		//	delete typeValObj;

////////	End Hybrid Table

	/////hardcoded table type for Componenet Table
	//	bool16 returnResult= ptrIAppFramework->ConfigCache_getDisplayItemComponents();
	//	if(returnResult == kTrue)
	//	{
	//		PRData.setData
	//			(3,
	//			i,
	//			-1,
	//			-112,
	//			TPLMediatorClass::CurrLanguageID, 
	//			"Component Table",
	//			kTrue,
	//			"",
	//			kFalse,
	//			1 // For Tables only
	//			);
	//			
	//		//CA(it1->getName());
	//		PRListData.setAll("Component Table",-112,1,1,0,2);
	//		PRListData.setHitCount(2);				
	//		PRDataNodeList.push_back(PRListData);					
	//		//i++;
	//		count3=i;	i++;

	//	}

		do
		{
			PMString temp("Family"),insertString("");  //temp("Product")
			//temp = ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename(temp),TPLMediatorClass::CurrLanguageID);
			insertString.Append("All ");
			insertString.Append(temp);
			insertString.Append(" Images");

			PRData.setData
			(	3,
				i,
				-1,
				-96,
				TPLMediatorClass::CurrLanguageID, 
				insertString,//"All Product Images",
				kTrue,
				insertString,//"All Product Images",
				kTrue,
				0
			);

			PRListData.setAll(insertString/*"All Product Images"*/,-96,1,1,0,1);
			PRListData.setHitCount(1);
			PRDataNodeList.push_back(PRListData);		
			i++;
			count3=i;

			insertString.Clear();
			insertString.Append("First Available ");
			insertString.Append(temp);
			insertString.Append(" Image");

			PRData.setData
			(	3,
				i,
				-1,
				-98,
				TPLMediatorClass::CurrLanguageID, 
				insertString,//"First Available Product Image",//"Primary Product Image",
				kTrue,
				insertString,//"First Available Product Image",//"Primary Product Image",
				kTrue,
				0
			);

			//PRListData.setAll("Primary Product Image",-98,1,1,0,1);
			PRListData.setAll(insertString/*"First Available Product Image"*/,-98,1,1,0,1);
			PRListData.setHitCount(1);
			PRDataNodeList.push_back(PRListData);		
			i++;
			count3=i;
			insertString.Clear();

			VectorTypeInfoPtr typeValObj1= ptrIAppFramework->StructureCache_getItemGroupImageAttributes();
			//ptrIAppFramework->ElementCache_getImageAttributesByIndex(3);
			
			if(typeValObj1!=nil)
			{	
				VectorTypeInfoValue::iterator it2;
		
				for(it2=typeValObj1->begin();it2!=typeValObj1->end();it2++)
				{	
					PRData.setData
					(3,
					i,
					-1,
					it2->getTypeId(),
					TPLMediatorClass::CurrLanguageID, 
					it2->getName(),
					kTrue,
					it2->getCode(),
					kTrue,
					0
					);

					PRListData.setAll(it2->getName(),it2->getTypeId(),1,1,0,1);
					PRListData.setHitCount(1);
					PRDataNodeList.push_back(PRListData);		
					i++;
					count3=i;
				}
			}

/////////////////////	Added on 3-11-08
			//CA("Second call");
			VectorElementInfoPtr eleValObj = ptrIAppFramework->StructureCache_getItemGroupCopyAttributesByLanguageId(TPLMediatorClass::CurrLanguageID);
			if(eleValObj==nil){
				ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::populatePRPanelLstbox::ElementCache_getCopyAttributesByIndex's eleValObj==nil");
				break;
			}
//CA_NUM("size  :  ",eleValObj->size());
			//CA("eleValObj== not nil ");
			vector<double> ElementIDSWithPVMPVImgTyp;
			VectorElementInfoValue::iterator it4;
			for(it4=eleValObj->begin();it4!=eleValObj->end();it4++)
			{
//CA("it4->getDisplay_name()  :  "+it4->getDisplay_name());				
				if(it4->getPicklistGroupId() > 0) // "Multiple-Value Pick List"
				{

					for(int32 index = 1 ; index <=5 ; index++)
					{
						bool16 returnResult = kFalse;
						returnResult = ptrIAppFramework->CONFIGCACHE_getPVImageFileName(index);
						if(returnResult)
						{
							//PMString temp("");
							//temp.AppendNumber(it4->getPv_type_id());
							//temp.AppendNumber(index);
							//int32 Pv_type_id = temp.GetAsNumber();
							PMString displayName("");
							displayName.Append(it4->getDisplayName());
							displayName.Append(" ");
							displayName.AppendNumber(index);
							PRData.setData
							(	3,						// which List 3 
								i,						// Index	
								it4->getElementId(),// Element Id Id for Core element
								it4->getPicklistGroupId(),					// type id ... here we are going to feed Item id
								TPLMediatorClass::CurrLanguageID,			
								displayName,//it4->getDisplay_name(), 
								kTrue,
								displayName,//it4->getDisplay_name(),// Code is replaced by Attribute Name
								kTrue,
								0,           //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
								kFalse,
								-1,-1,-1,-1,-1,-1,index,-1,1
							);
							
							PRListData.setAll(/*it4->getDisplay_name()*/displayName,it4->getElementId(),1,1,0,1);
							PRListData.setHitCount(1);
							PRDataNodeList.push_back(PRListData);
							count3=i;
							i++;
						}
					}
					ElementIDSWithPVMPVImgTyp.push_back(it4->getElementId());
				}
			}
			


			/*AP*/
			//InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
			//if(ptrImageHelper != nil)
			//{
			//	//CA("ptrImageHelper != nil");
			//	ptrImageHelper->setattributesWithPVMPV(ElementIDSWithPVMPVImgTyp,kTrue);
			//	ElementIDSWithPVMPVImgTyp.clear();
			//}

			if(eleValObj)
				delete eleValObj;
////////////////////////////////// End on  3-11-08

/* No longer required now - Bugtracker - #375 - https://pub.plan.io/issues/375
			// Brand Manufacturer, Supplier/Vendor images...
			for(int32 j=0; j < 5; j ++)
			{
				insertString.Clear();
				insertString.Append("Brand Image ");
				insertString.AppendNumber( j +1);
				//insertString.Append(" Image");

				double assetTypeId = -207 - j;

				PRData.setData
				(	3,
					i,
					-1,
					assetTypeId,
					TPLMediatorClass::CurrLanguageID, 
					insertString,
					kTrue,
					insertString,
					kTrue,
					0
				);

				
				PRListData.setAll(insertString,assetTypeId,1,1,0,1);
				PRListData.setHitCount(1);
				PRDataNodeList.push_back(PRListData);		
				i++;
				count3=i;
				insertString.Clear();
			}

			for(int32 j=0; j < 5; j ++)
			{
				insertString.Clear();
				insertString.Append("Manufacturer Image ");
				insertString.AppendNumber( j +1);
				//insertString.Append(" Image");

				double assetTypeId = -212 - j;

				PRData.setData
				(	3,
					i,
					-1,
					assetTypeId,
					TPLMediatorClass::CurrLanguageID, 
					insertString,
					kTrue,
					insertString,
					kTrue,
					0
				);

				
				PRListData.setAll(insertString,assetTypeId,1,1,0,1);
				PRListData.setHitCount(1);
				PRDataNodeList.push_back(PRListData);		
				i++;
				count3=i;
				insertString.Clear();
			}
*/
 
 
			for(int32 j=0; j < 5; j ++)
			{
				insertString.Clear();
				insertString.Append("Org Image ");
				insertString.AppendNumber( j +1);
				//insertString.Append(" Image");

				double assetTypeId = -217 - j;

				PRData.setData
				(	3,
					i,
					-1,
					assetTypeId,
					TPLMediatorClass::CurrLanguageID, 
					insertString,
					kTrue,
					insertString,
					kTrue,
					0
				);

				
				PRListData.setAll(insertString,assetTypeId,1,1,0,1);
				PRListData.setHitCount(1);
				PRDataNodeList.push_back(PRListData);		
				i++;
				count3=i;
				insertString.Clear();
			}

//9-april
			// Added for Possible Value Box Attribute.	
		
/*	
			PRData.setData					//------------
			(	3,
				i,
				-1,
				-1001,
				TPLMediatorClass::CurrLanguageID, 
				"Possible Value Box",
				kTrue,
				"",
				kFalse,
				0
			);

			PRListData.setAll("Possible Value Box",-1001,1,1,0,5);
			PRListData.setHitCount(5);
			PRDataNodeList.push_back(PRListData);	
////////////////////////////////////////////////UPTO HERE
//Added on 10/01/07 By Dattatray
			PRData.setData
			(	3,
				i,
				-1002,
				-1,
				TPLMediatorClass::CurrLanguageID, 
				"Product Group Name",
				kTrue,
				"",
				kFalse,
				0
			);

			PRListData.setAll("Product Group Name",-1,1,1,0,7);
			PRListData.setHitCount(7);
			PRDataNodeList.push_back(PRListData);

			PRData.setData
			(	3,
				i,
				-1002,
				-1,
				TPLMediatorClass::CurrLanguageID, 
				"Product Group Abbvr",
				kTrue,
				"",
				kFalse,
				0
			);

			PRListData.setAll("Product Group Abbvr",-1,1,1,0,7);
			PRListData.setHitCount(7);
			PRDataNodeList.push_back(PRListData);

			PRData.setData
			(	3,
				i,
				-1002,
				-1,
				TPLMediatorClass::CurrLanguageID, 
				"Product Group Description",
				kTrue,
				"",
				kFalse,
				0
			);

			PRListData.setAll("Product Group Description",-1,1,1,0,7);
			PRListData.setHitCount(7);
			PRDataNodeList.push_back(PRListData);
*/
//Upto here 10/01/07
			if(typeValObj1)	
			  delete typeValObj1;
	
		}while(0);
		
	}while(kFalse);
}

void TPLSelectionObserver::populateItemPanelLstbox()
{
	//CA("TPLSelectionObserver::populateItemPanelLstbox");
	int32 count4=0;
	do
	{
		
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;
		//CAlert::InformationAlert(__FUNCTION__);
		if(TPLMediatorClass::iPanelCntrlDataPtr==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::populateItemPanelLstbox::Not getting iPanelCntrlDataPtr");
			break;
		}

		

		ITEMDataNodeList.clear();
		TPLDataNode ITEMListData;
		ItemData.clearVector(4);
		int i=0;

//A		do{

			VectorAttributeInfoPtr AttrInfoVectPtr = NULL;
			if(CurrentClassID == -1)
			{
				//CA("CurrentClassID == -1");
				VectorClassInfoPtr vectClassInfoValuePtr = ptrIAppFramework->ClassificationTree_getRoot(TPLMediatorClass::CurrLanguageID); // 1 default for English
				if(vectClassInfoValuePtr==nil)
				{		
					ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::populateItemPanelLstbox::ClassificationTree_getRoot's vectClassInfoValuePtr==nil");
					break;
				}
				VectorClassInfoValue::iterator it;
				it=vectClassInfoValuePtr->begin();
				CurrentClassID = it->getClass_id();		

				 if(vectClassInfoValuePtr)
					delete vectClassInfoValuePtr;
			}
        
            int32 field_5 = -1;

        

			AttrInfoVectPtr = ptrIAppFramework->StructureCache_getItemAttributesForClassAndParents(CurrentClassID, TPLMediatorClass::CurrLanguageID);
			if(AttrInfoVectPtr== NULL){
				ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::populateItemPanelLstbox::AttributeCache_getItemAttributesForClassAndParents's AttrInfoVectPtr is nil ");
				break;
			}
			
			VectorAttributeInfoValue::iterator it2;
			
			
			//CroosClientAttributeMap::iterator itr_Map;

			//map<int32,AttributeClientCodeModel> AttributeClient_MapByCode_Id; 

			//CroosClientAttributeMapPtr crssClientAttributeMap =  ptrIAppFramework->getAllCrossClientAttributeByClientId(ptrIAppFramework->getClientID() , AttributeClient_MapByCode_Id); 
			/*if(crssClientAttributeMap == NULL)
			{
				CA("crcsClientAttributeMap == NULL");
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::populateItemPanelLstbox::getAllCrossClientAttributeByClientId's crssClientAttributeMap==nil");
				break;
			}*/

			//AttributeClientCodeModel AttrClientCodeObj;
		
				for(it2=AttrInfoVectPtr->begin();it2!=AttrInfoVectPtr->end();it2++)
				{//CAlert::InformationAlert("4");
				//CA("it2->getDisplay_name()"+it2->getDisplay_name());
					//if(crssClientAttributeMap != NULL)
					//{
					//	itr_Map = crssClientAttributeMap->find(it2->getAttribute_id());
					//	if(itr_Map != crssClientAttributeMap->end())
					//	{
					//		AttrClientCodeObj = itr_Map->second ;
					//		field_5 = AttrClientCodeObj.getCode_Id();
					//	}
					//}
					//else
					//	field_5 = -1;
                    
                    if(it2->getDataType() == "ASSET")
                        continue;
				
					ItemData.setData
					(	4,						// which List 4 = item List
						i,						// Index	
						it2->getAttributeId(),// Attribute Id for Core element
						-1,					// type id ... here we are going to feed Item id
						TPLMediatorClass::CurrLanguageID,			
						it2->getDisplayName(), 
						kTrue,
						it2->getDisplayName(),// Code is replaced by Attribute Name
						kFalse,
						0,	//Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
						it2->getPubItem(),
						-1,-1,-1,-1,intSprItemPerFrameFlow,-1,-1,-1,-1,-1,field_5
					);

				
					//CA("it2->getDisplay_name :  "+it2->getDisplay_name());
					ITEMListData.setAll(it2->getDisplayName(),it2->getAttributeId(),1,1,0,0);
					if(it2->getPubItem())
					{
						//CA("it2->getPub_Item() == kTrue");
						ITEMListData.setHitCount(6);
					}
					else if(!it2->getisParametric())
						ITEMListData.setHitCount(0);				
					else
						ITEMListData.setHitCount(3);
					ITEMDataNodeList.push_back(ITEMListData);
					count4=i;
					i++;


					if(it2->getTableCol() == "SUPPLIER_ID")
					{
						PMString dispName1("Vendor Description 1");
						dispName1.SetTranslatable(kFalse);
						ItemData.setData
						(4,
						i,
						-225,
						-1,
						TPLMediatorClass::CurrLanguageID,						
						dispName1,
						kTrue,
						dispName1,
						kFalse,
						0,
						it2->getPubItem(),
						-1,-1,-1,-1,intSprItemPerFrameFlow,-1,-1,-1,-1,-1,field_5
						);
						ITEMListData.setAll(dispName1,-225,1,1,0,0);
						ITEMListData.setHitCount(0);
						ITEMDataNodeList.push_back(ITEMListData);							
						count4=i;
						i++;

						PMString dispName2("Vendor Description 2");
						dispName2.SetTranslatable(kFalse);
						ItemData.setData
						(4,
						i,
						-226,
						-1,
						TPLMediatorClass::CurrLanguageID,						
						dispName1,
						kTrue,
						dispName2,
						kFalse,
						0,
						it2->getPubItem(),
						-1,-1,-1,-1,intSprItemPerFrameFlow,-1,-1,-1,-1,-1,field_5
						);
						ITEMListData.setAll(dispName2,-226,1,1,0,0);
						ITEMListData.setHitCount(0);
						ITEMDataNodeList.push_back(ITEMListData);							
						count4=i;
						i++;
					}

			}
			
			//if(crssClientAttributeMap)
			//{
			//	delete crssClientAttributeMap;
			//	crssClientAttributeMap = NULL;

			//	AttributeClient_MapByCode_Id.clear();
			//}

			do{
			ItemData.setData
			(4,
			i,
			-703,//id hardcoded
			-1,
			TPLMediatorClass::CurrLanguageID, 
			"$ Off",
			kTrue,
			"$ Off",
			kFalse,
			0
			);

			ITEMListData.setAll("$ Off",-99,1,1,0,1);
			ITEMListData.setHitCount(6);					//setHitcount(5) changed by Tushar on 13/12/06	
			ITEMDataNodeList.push_back(ITEMListData);

			count4=i;
			i++;

		}while(0);
	

		do{
			ItemData.setData
			(4,
			 i,
			-704, //id hardcoded
			-1,
			TPLMediatorClass::CurrLanguageID, 
			"% Off",
			 kTrue,
			"% Off",
			 kFalse,
			 0
			 );
	
			ITEMListData.setAll("% Off",-99,1,1,0,1);
			ITEMListData.setHitCount(6);					//setHitcount(5) changed by Tushar on 13/12/06
			ITEMDataNodeList.push_back(ITEMListData);

			count4=i;
			i++;

		}while(0);

//16-may chetan
			ItemData.setData
			(	4,						// which List 4 = item List
				i,						// Index	
				-803,//it2->getAttribute_id(),// Attribute Id for Core element
				-1,					// type id ... here we are going to feed Item id
				TPLMediatorClass::CurrLanguageID,			
				"Letter Keys",//,it2->getDisplay_name(), 
				kTrue,
				"Letter Keys",// Code is replaced by Attribute Name
				kFalse,
				0,          //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
				kFalse,-1,-1,-1,-1,intSprItemPerFrameFlow	//-------
			);
			

			ITEMListData.setAll("Letter Keys",/*it2->getAttribute_id()*/-803,1,1,0,0);
			ITEMListData.setHitCount(0);				
			ITEMDataNodeList.push_back(ITEMListData);
			count4=i;
			i++;


			//******Added From Here 09/09/09
			ItemData.setData
			(	4,						// which List 4 = item List
				i,						// Index	
				-827,//it2->getAttribute_id(),// Attribute Id for Core element
				-1,					// type id ... here we are going to feed Item id
				TPLMediatorClass::CurrLanguageID,			
				"Number Keys",//,it2->getDisplay_name(), 
				kTrue,
				"Number Keys",// Code is replaced by Attribute Name
				kFalse,
				0,          //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
				kFalse,-1,-1,-1,-1,intSprItemPerFrameFlow	//-------
			);
			

			ITEMListData.setAll("Number Keys",/*it2->getAttribute_id()*/-827,1,1,0,0);
			ITEMListData.setHitCount(0);				
			ITEMDataNodeList.push_back(ITEMListData);
			count4=i;
			i++;
			//********

			ItemData.setData
			(	4,						// which List 4 = item List
				i,						// Index	
				-804,//it2->getAttribute_id(),// Attribute Id for Core element
				-1,					// type id ... here we are going to feed Item id
				TPLMediatorClass::CurrLanguageID,			
				"New Indicator",//,it2->getDisplay_name(), 
				kTrue,
				"New Indicator",// Code is replaced by Attribute Name
				kFalse,
				0,           //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
				kFalse,-1,-1,-1,-1,intSprItemPerFrameFlow	//-------
			);
			

			ITEMListData.setAll("New Indicator",/*it2->getAttribute_id()*/-804,1,1,0,0);
			ITEMListData.setHitCount(0);				
			ITEMDataNodeList.push_back(ITEMListData);
			count4=i;
			i++;
			
        
            ItemData.setData
                (4,
                 i,
                 -121,//it->getElementId(),
                 -3,//it->getElement_type_id(),
                 TPLMediatorClass::CurrLanguageID,  //it->getLanguage_id(),
                 "List_Table Name",//it->getDisplay_name(),
                 kTrue,
                 "",
                 kFalse,
                 0,
                 kFalse,
                 -1,-1,-1,-1,-1,-1,-1,-1,4
                 );
        
            ITEMListData.setAll("List/Table Name",-121,1,1,0,0);
            ITEMListData.setHitCount(0);
            ITEMDataNodeList.push_back(ITEMListData);
            i++;
            count4=i;
        
            ////--12Dec2011 List/Table Level Fields--ElementID=-983 to -975 ,Type_ID=-55---
            ItemData.setData
                (4,
                 i,
                 -983,//it->getElementId(),
                 -55,//it->getElement_type_id(),
                 TPLMediatorClass::CurrLanguageID,  //it->getLanguage_id(),
                 "List Description",//it->getDisplay_name(),
                 kTrue,
                 "",
                 kFalse,
                 0,
                 kFalse,
                 -1,-1,-1,-1,-1,-1,-1,-1,5
                 );
        
            ITEMListData.setAll("List Description",-983,1,1,0,0);
            ITEMListData.setHitCount(0);
            ITEMDataNodeList.push_back(ITEMListData);
            i++;
            count4=i;
		
            ItemData.setData
                (4,
                 i,
                 -982,//it->getElementId(),
                 -55,//it->getElement_type_id(),
                 TPLMediatorClass::CurrLanguageID,  //it->getLanguage_id(),
                 "Stencil Name",//it->getDisplay_name(),
                 kTrue,
                 "",
                 kFalse,
                 0,
                 kFalse,
                 -1,-1,-1,-1,-1,-1,-1,-1,5
                 );
        
            ITEMListData.setAll("Stencil Name",-982,1,1,0,0);
            ITEMListData.setHitCount(0);
            ITEMDataNodeList.push_back(ITEMListData);
            i++;
            count4=i;
        
            //---List Level Fields "Note 1 to 5"***element_ID -980 to -976 ***
            PMString List_name = "";
            double ele_ID = -981;
            for(int32 x=1; x<=5 ; x++)
            {
                ele_ID = ele_ID + 1;
                List_name.Clear();
                List_name.Append("Note ");
                List_name.AppendNumber(PMReal(x));
                ItemData.setData
                    (4,
                    i,
                     ele_ID, //-981,//it->getElementId(),
                     -56,//it->getElement_type_id(),
                     TPLMediatorClass::CurrLanguageID,  //it->getLanguage_id(),
                     List_name,//	"Note ",//it->getDisplay_name(),
                     kTrue,
                     "",
                     kFalse,
                     0,
                     kFalse,
                     -1,-1,-1,-1,-1,-1,-1,-1,5
                     );
            
                ITEMListData.setAll(List_name/*"Note"*/,ele_ID/*-981*/,1,1,0,0);
                ITEMListData.setHitCount(0);
                ITEMDataNodeList.push_back(ITEMListData);
                i++;
                count4=i;
            }
        
        //---Adv. Table Level Fields "Note 1 to 5"***element_ID -980 to -976 ***
        List_name = "";
        ele_ID = -976;
        for(int32 x=1; x<=5 ; x++)
        {
            ele_ID = ele_ID + 1;
            List_name.Clear();
            List_name.Append("Adv. Table Note ");
            List_name.AppendNumber(PMReal(x));
            ItemData.setData
            (4,
             i,
             ele_ID, //-981,//it->getElementId(),
             -58,//it->getElement_type_id(), // Adv. Table Notes Type
             TPLMediatorClass::CurrLanguageID,  //it->getLanguage_id(),
             List_name,//	"Note ",//it->getDisplay_name(),
             kTrue,
             "",
             kFalse,
             0,
             kFalse,
             -1,-1,-1,-1,-1,-1,-1,-1,5
             );
            
            ITEMListData.setAll(List_name, ele_ID, 1, 1, 0, 0);
            ITEMListData.setHitCount(0);
            ITEMDataNodeList.push_back(ITEMListData);
            i++;
            count4=i;
        }

        


			if(ptrIAppFramework->ConfigCache_getDisplayItemComponents())
			{
				ItemData.setData
				(	4,						// which List 4 = item List
					i,						// Index
					-805,//it2->getAttribute_id(),// Attribute Id for Core element
					-1,					// type id ... here we are going to feed Item id
					TPLMediatorClass::CurrLanguageID, 
					"Component Quantity",//,it2->getDisplay_name(), 
					kTrue,
					"Component Quantity",// Code is replaced by Attribute Name
					kFalse,
					0,           //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
					kFalse,-1,-1,-1,-1,intSprItemPerFrameFlow	//-------
				);

				ITEMListData.setAll("Component Quantity",/*it2->getAttribute_id()*/-805,1,1,0,0);
				ITEMListData.setHitCount(0);				
				ITEMDataNodeList.push_back(ITEMListData);
				count4=i;
				i++;
				
				
				ItemData.setData
				(	4,						// which List 4 = item List
					i,						// Index	
					-806,//it2->getAttribute_id(),// Attribute Id for Core element
					-1,					// type id ... here we are going to feed Item id
					TPLMediatorClass::CurrLanguageID, 
					"Component Availability",//,it2->getDisplay_name(), 
					kTrue,
					"Component Availability",// Code is replaced by Attribute Name
					kFalse,
					0,           //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
					kFalse,-1,-1,-1,-1,intSprItemPerFrameFlow	//-------
				);

				ITEMListData.setAll("Component Availability",/*it2->getAttribute_id()*/-806,1,1,0,0);
				ITEMListData.setHitCount(0);				
				ITEMDataNodeList.push_back(ITEMListData);
				count4=i;
				i++;
			}

			if(ptrIAppFramework->ConfigCache_getDisplayItemXref())
			{
				ItemData.setData
				(	4,						// which List 4 = item List
					i,						// Index	
					-807,//it2->getAttribute_id(),// Attribute Id for Core element
					-1,					// type id ... here we are going to feed Item id
					TPLMediatorClass::CurrLanguageID, 
					"X-Ref Type",//,it2->getDisplay_name(), 
					kTrue,
					"X-Ref Type",// Code is replaced by Attribute Name
					kFalse,
					0 ,          //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
					kFalse,-1,-1,-1,-1,intSprItemPerFrameFlow	//-------

				);

				ITEMListData.setAll("X-Ref Type",/*it2->getAttribute_id()*/-807,1,1,0,0);
				ITEMListData.setHitCount(0);				
				ITEMDataNodeList.push_back(ITEMListData);
				count4=i;
				i++;

			
				ItemData.setData
				(	4,						// which List 4 = item List
					i,						// Index	
					-808,//it2->getAttribute_id(),// Attribute Id for Core element
					-1,					// type id ... here we are going to feed Item id
					TPLMediatorClass::CurrLanguageID, 
					"X-Ref Rating",//,it2->getDisplay_name(), 
					kTrue,
					"X-Ref Rating ",// Code is replaced by Attribute Name
					kFalse,
					0,           //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
					kFalse,-1,-1,-1,-1,intSprItemPerFrameFlow	//-------
				);

				ITEMListData.setAll("X-Ref Rating",/*it2->getAttribute_id()*/-808,1,1,0,0);
				ITEMListData.setHitCount(0);				
				ITEMDataNodeList.push_back(ITEMListData);
				count4=i;
				i++;

				ItemData.setData
				(	4,						// which List 4 = item List
					i,						// Index	
					-809,//it2->getAttribute_id(),// Attribute Id for Core element
					-1,					// type id ... here we are going to feed Item id
					TPLMediatorClass::CurrLanguageID, 
					"X-Ref Alternate",//,it2->getDisplay_name(), 
					kTrue,
					"X-Ref Alternate",// Code is replaced by Attribute Name
					kFalse,
					0,           //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
					kFalse,-1,-1,-1,-1,intSprItemPerFrameFlow	//-------
				);

				ITEMListData.setAll("X-Ref Alternate",/*it2->getAttribute_id()*/-809,1,1,0,0);
				ITEMListData.setHitCount(0);				
				ITEMDataNodeList.push_back(ITEMListData);
				count4=i;
				i++;

				ItemData.setData
				(	4,						// which List 4 = item List
					i,						// Index	
					-810,//it2->getAttribute_id(),// Attribute Id for Core element
					-1,					// type id ... here we are going to feed Item id
					TPLMediatorClass::CurrLanguageID, 
					"X-Ref Comments",//,it2->getDisplay_name(), 
					kTrue,
					"X-Ref Comments",// Code is replaced by Attribute Name
					kFalse,
					0,           //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
					kFalse,-1,-1,-1,-1,intSprItemPerFrameFlow	//-------
				);
				
				ITEMListData.setAll("X-Ref Comments",/*it2->getAttribute_id()*/-810,1,1,0,0);
				ITEMListData.setHitCount(0);				
				ITEMDataNodeList.push_back(ITEMListData);
				count4=i;
				i++;

				
				ItemData.setData
				(	4,						// which List 4 = item List
					i,						// Index	
					-811,//it2->getAttribute_id(),// Attribute Id for Core element
					-1,					// type id ... here we are going to feed Item id
					TPLMediatorClass::CurrLanguageID,			
					"X-Ref Catalog Flag",//,it2->getDisplay_name(), 
					kTrue,
					"X-Ref Catalog Flag",// Code is replaced by Attribute Name
					kFalse,
					0,           //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
					kFalse,-1,-1,-1,-1,intSprItemPerFrameFlow	//-------
				);

				ITEMListData.setAll("X-Ref Catalog Flag",/*it2->getAttribute_id()*/-811,1,1,0,0);
				ITEMListData.setHitCount(0);				
				ITEMDataNodeList.push_back(ITEMListData);
				count4=i;
				i++;
			}

			if(ptrIAppFramework->ConfigCache_getDisplayItemAccessory())
			{
				ItemData.setData
				(	4,						// which List 4 = item List
					i,						// Index	
					-812,//it2->getAttribute_id(),// Attribute Id for Core element
					-1,					// type id ... here we are going to feed Item id
					TPLMediatorClass::CurrLanguageID, 
					"Accessory Quantity",//,it2->getDisplay_name(), 
					kTrue,
					"Accessory Quantity",// Code is replaced by Attribute Name
					kFalse,
					0,           //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
					kFalse,-1,-1,-1,-1,intSprItemPerFrameFlow	//-------
				);

				ITEMListData.setAll("Accessory Quantity",/*it2->getAttribute_id()*/-812,1,1,0,0);
				ITEMListData.setHitCount(0);				
				ITEMDataNodeList.push_back(ITEMListData);
				count4=i;
				i++;


				ItemData.setData
				(	4,						// which List 4 = item List
					i,						// Index	
					-813,//it2->getAttribute_id(),// Attribute Id for Core element
					-1,					// type id ... here we are going to feed Item id
					TPLMediatorClass::CurrLanguageID,			
					"Accessory Required",//,it2->getDisplay_name(), 
					kTrue,
					"Accessory Required",// Code is replaced by Attribute Name
					kFalse,
					0,           //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
					kFalse,-1,-1,-1,-1,intSprItemPerFrameFlow	//-------
				);
				
				ITEMListData.setAll("Accessory Required",/*it2->getAttribute_id()*/-813,1,1,0,0);
				ITEMListData.setHitCount(0);				
				ITEMDataNodeList.push_back(ITEMListData);
				count4=i;
				i++;


				ItemData.setData
				(	4,						// which List 4 = item List
					i,						// Index	
					-814,//it2->getAttribute_id(),// Attribute Id for Core element
					-1,					// type id ... here we are going to feed Item id
					TPLMediatorClass::CurrLanguageID, 
					"Accessory Comments",//,it2->getDisplay_name(), 
					kTrue,
					"Accessory Comments",// Code is replaced by Attribute Name
					kFalse,
					0,           //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
					kFalse,-1,-1,-1,-1,intSprItemPerFrameFlow	//-------
				);

				ITEMListData.setAll("Accessory Comments",/*it2->getAttribute_id()*/-814,1,1,0,0);
				ITEMListData.setHitCount(0);				
				ITEMDataNodeList.push_back(ITEMListData);
				count4=i;
				i++;
			}

	////---	For Make Model Year Attributes  ---

			ItemData.setData
			(	4,						// which List 4 = item List
				i,						// Index	
				-401,//it2->getAttribute_id(),// Attribute Id for Core element
				-1,					// type id ... here we are going to feed Item id
				TPLMediatorClass::CurrLanguageID, 
				"Make",//,it2->getDisplay_name(), 
				kTrue,
				"Make",// Code is replaced by Attribute Name
				kFalse,
				0,           //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
				kFalse,-1,-1,-1,-1,intSprItemPerFrameFlow	//-------
			);

			ITEMListData.setAll("Make",/*it2->getAttribute_id()*/-401,1,1,0,0);
			ITEMListData.setHitCount(0);				
			ITEMDataNodeList.push_back(ITEMListData);
			count4=i;
			i++;

			ItemData.setData
			(	4,						// which List 4 = item List
				i,						// Index	
				-402,//it2->getAttribute_id(),// Attribute Id for Core element
				-1,					// type id ... here we are going to feed Item id
				TPLMediatorClass::CurrLanguageID, 
				"Model",//,it2->getDisplay_name(), 
				kTrue,
				"Model",// Code is replaced by Attribute Name
				kFalse,
				0,           //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
				kFalse,-1,-1,-1,-1,intSprItemPerFrameFlow	//-------
			);

			ITEMListData.setAll("Model",/*it2->getAttribute_id()*/-402,1,1,0,0);
			ITEMListData.setHitCount(0);				
			ITEMDataNodeList.push_back(ITEMListData);
			count4=i;
			i++;


			ItemData.setData
			(	4,						// which List 4 = item List
				i,						// Index	
				-403,//it2->getAttribute_id(),// Attribute Id for Core element
				-1,					// type id ... here we are going to feed Item id
				TPLMediatorClass::CurrLanguageID, 
				"Year",//,it2->getDisplay_name(), 
				kTrue,
				"Year",// Code is replaced by Attribute Name
				kFalse,
				0,           //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
				kFalse,-1,-1,-1,-1,intSprItemPerFrameFlow	//-------
			);

			ITEMListData.setAll("Year",/*it2->getAttribute_id()*/-403,1,1,0,0);
			ITEMListData.setHitCount(0);				
			ITEMDataNodeList.push_back(ITEMListData);
			count4=i;
			i++;



//A			if(AttrInfoVectPtr)
//A				delete AttrInfoVectPtr;
//A		}while(0);

	//added on 13/12/06 by Tushar

	//	PMString temp = ptrIAppFramework->getPriceAttributePrefix(); commented on 22/12/06 by Tushar
	//	CA("temp : " + temp);
	//	PMString temp = "";
	
		/*VectorTypeInfoPtr TypeInfoVectorPtr = ptrIAppFramework->TypeCache_getEventPriceTypeList();
		if(TypeInfoVectorPtr != NULL)
		{
			VectorTypeInfoValue::iterator it3;
			PMString temp1 = "";
			PMString name = "";

			for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
			{
				temp1 = it3->getName();
				//CA("temp1 : " + temp1 );
				name = temp1; 
				//CA("name : "+ name);
				ItemData.setData
				(	4,
					i,
					-701,//-1,
					it3->getType_id(),
					TPLMediatorClass::CurrLanguageID, 
					name,
					kTrue,
					name,
					kFalse,
					0
				);

				ITEMListData.setAll(name,-99,1,1,0,1);
				ITEMListData.setHitCount(6);
				ITEMDataNodeList.push_back(ITEMListData);

				//temp.Clear();
				name.Clear();

				//temp1 = it3->getName();
				name = temp1 + " Suffix"; 

				ItemData.setData
				(	4,
					i,
					-702,//-1,
					it3->getType_id(),
					TPLMediatorClass::CurrLanguageID, 
					name,
					kTrue,
					name,
					kFalse,
					0
				);

				ITEMListData.setAll(name,-99,1,1,0,1);
				ITEMListData.setHitCount(6);
				ITEMDataNodeList.push_back(ITEMListData);

				//temp.Clear();
				name.Clear();
			}

				//temp1 = it3->getName();
			name = "$ Off"; 

			ItemData.setData
			(	4,
				i,
				-703,//-1,
				-1,//it3->getType_id(),
				TPLMediatorClass::CurrLanguageID, 
				name,
				kTrue,
				name,
				kFalse,
				0
			);

			ITEMListData.setAll(name,-99,1,1,0,1);
			ITEMListData.setHitCount(6);
			ITEMDataNodeList.push_back(ITEMListData);

			//temp.Clear();
			name.Clear();

			//temp1 = it3->getName();
			name = "% Off"; 
				
			ItemData.setData
			(	4,
				i,
				-704,//-1,
				-1,//it3->getType_id(),
				TPLMediatorClass::CurrLanguageID, 
				name,
				kTrue,
				name,
				kFalse,
				0
			);

			ITEMListData.setAll(name,-99,1,1,0,1);
			ITEMListData.setHitCount(6);
			ITEMDataNodeList.push_back(ITEMListData);

			//temp.Clear();
			name.Clear();

/////////////////////////// Added by Chetan Dogra on 02/08/2006 ////////////
////////////////////////////////  From Here   /////////////////////////////

		}
		if(TypeInfoVectorPtr)
			delete TypeInfoVectorPtr;*/
	//upto here added on 13/12/06 by Tushar

	//added by Tushar on 16/12/06
		/*for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
		{
			PMString temp1 = it3->getName();
			PMString name = temp + " " + temp1 + " Suffix"; 
				ItemData.setData
					(4,
					i,
			it3->getType_id(),
			-702,//-1,
					TPLMediatorClass::CurrLanguageID, 
			name,
					kTrue,
			name,
					kFalse,
			0
					);
				
			ITEMListData.setAll(name,-99,1,1,0,1);
			ITEMListData.setHitCount(6);
			ITEMDataNodeList.push_back(ITEMListData);
			}

		for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
		{
			PMString temp1 = it3->getName();
			PMString name = temp + " " + temp1 + " $ Off"; 
			ItemData.setData
				(4,
				i,
			it3->getType_id(),
			-703,//-1,
				TPLMediatorClass::CurrLanguageID, 
			name,
				kTrue,
			name,
				kFalse,
			0
				);
				
			ITEMListData.setAll(name,-99,1,1,0,1);
			ITEMListData.setHitCount(6);
			ITEMDataNodeList.push_back(ITEMListData);					
		}

		for(it3=TypeInfoVectorPtr->begin();it3!=TypeInfoVectorPtr->end();it3++)
		{  
			PMString temp1 = it3->getName();
			PMString name = temp + " " + temp1 + " % Off"; 
			ItemData.setData
				(4,
				i,
			it3->getType_id(),
			-704,//-1,
				TPLMediatorClass::CurrLanguageID, 
			name,
				kTrue,
			name,
			kFalse,
				0
				);

			ITEMListData.setAll(name,-99,1,1,0,1);
			ITEMListData.setHitCount(6);
				ITEMDataNodeList.push_back(ITEMListData);
		}*/
	//upto here added on 16/12/06 by Tushar

		////////////////////////////////////////////////////////////////////////////
		//following code is added by Chetan Dogra on 22-September-2006
		////////////////////////////////////////////////////////FROM HERE TO
		/*do{
			ItemData.setData
				(4,
				i,
			-701,//id hardcoded for event price item attributes.
				-1,
				TPLMediatorClass::CurrLanguageID, 
			"Event Price",
				kTrue,
			"Event Price",
			kFalse,
				0
				);

			ITEMListData.setAll("Event Price",-99,1,1,0,1);
			ITEMListData.setHitCount(5);
				ITEMDataNodeList.push_back(ITEMListData);
							
		}while(0);


		do{

					ItemData.setData
					(4,
					i,
			-702,//id hardcoded for event suffix item attributes.
					-1,
					TPLMediatorClass::CurrLanguageID, 
			"Event Suffix",
					kTrue,
			"Event Suffix",
			kFalse,
					0
					);

			ITEMListData.setAll("Event Suffix",-99,1,1,0,1);
			ITEMListData.setHitCount(6);					//setHitcount(5) changed by Tushar on 13/12/06
					ITEMDataNodeList.push_back(ITEMListData);

		}while(0);*/

	
		//do{
		//	ItemData.setData
		//	(4,
		//	i,
		//	-703,//id hardcoded
		//	-1,
		//	TPLMediatorClass::CurrLanguageID, 
		//	"$ Off",
		//	kTrue,
		//	"$ Off",
		//	kFalse,
		//	0
		//	);

		//	ITEMListData.setAll("$ Off",-99,1,1,0,1);
		//	ITEMListData.setHitCount(6);					//setHitcount(5) changed by Tushar on 13/12/06	
		//	ITEMDataNodeList.push_back(ITEMListData);

		//}while(0);
	

		//do{
		//	ItemData.setData
		//	(4,
		//	 i,
		//	-704, //id hardcoded
		//	-1,
		//	TPLMediatorClass::CurrLanguageID, 
		//	"% Off",
		//	 kTrue,
		//	"% Off",
		//	 kFalse,
		//	 0
		//	 );
	
		//	ITEMListData.setAll("% Off",-99,1,1,0,1);
		//	ITEMListData.setHitCount(6);					//setHitcount(5) changed by Tushar on 13/12/06
		//	ITEMDataNodeList.push_back(ITEMListData);

		//}while(0);
					
		///////////////////////////////////////////////////////UPTO HERE

		
		
/////////////////////////// Added by Chetan Dogra on 02/08/2006 ////////////
////////////////////////////////  From Here   /////////////////////////////

		do
		{	//CAlert::InformationAlert("Inside do");
			VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getListTableTypes();
			if(typeValObj==nil){
				ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::populateItemPanelLstbox::AttributeCache_getItemTableTypes's typeValObj is nil");	
				break;
			}
			VectorTypeInfoValue::iterator it1;
		
			for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
			{//CAlert::InformationAlert("8.1");	
				ItemData.setData
				(	4,
					i,
					-1,
					it1->getTypeId(),
					TPLMediatorClass::CurrLanguageID, 
					it1->getName(),
					kTrue,
					"",
					kFalse,
					1 ,// For Tables only
					kFalse,//For Event Field
					-1,//For Header
					-1,//for childTag
					1,//for TableType
					-1,
					intSprItemPerFrameFlow //----
				);

				//CA("it1->getName()  :  "+it1->getName());
				PMString temp(it1->getName());
				temp.Append(" (List)"/*" : List"*/);
				ITEMListData.setAll(temp,it1->getTypeId(),1,1,0,8);
				//ITEMListData.setAll(it1->getName(),it1->getType_id(),1,1,0,2);
				ITEMListData.setHitCount(8);	
				//ITEMListData.setHitCount(2);				
				ITEMDataNodeList.push_back(ITEMListData);//	CA("1");					
				count4=i;				
				i++;
			}
	
			if(typeValObj)
				delete typeValObj;	
		}while(0);

		///hardcoded table type for Componenet Table
		bool16 returnResult= ptrIAppFramework->ConfigCache_getDisplayItemComponents();
		if(returnResult == kTrue)
		{
			ItemData.setData
			(	4,
				i,
				-1,
				-112,
				TPLMediatorClass::CurrLanguageID, 
				"Component Table",
				kTrue,
				"",
				kFalse,
				1, // For Tables only
				kFalse,//For Event Field
				-1,//For Header
				-1,//for childTag
				4//for TableType
			);
		
			//CA(it1->getName());
			ITEMListData.setAll("Component Table",-112,1,1,0,2);
			ITEMListData.setHitCount(2);				
			ITEMDataNodeList.push_back(ITEMListData);					
			//i++;
			count4=i;
			i++;
		}

		bool16 returnResult1= ptrIAppFramework->ConfigCache_getDisplayItemXref();
		if(returnResult1 == kTrue)
		{
			ItemData.setData
			(	4,
				i,
				-1,
				-113,
				TPLMediatorClass::CurrLanguageID, 
				"XRef Table",
				kTrue,
				"",
				kFalse,
				1, // For Tables only
				kFalse,//For Event Field
				-1,//For Header
				-1,//for childTag
				6 //for TableType
			);

			//CA(it1->getName());
			ITEMListData.setAll("XRef Table",-113,1,1,0,2);
			ITEMListData.setHitCount(2);				
			ITEMDataNodeList.push_back(ITEMListData);					
			//i++;
			count4=i;
			i++;
		}

		bool16 returnResult2= ptrIAppFramework->ConfigCache_getDisplayItemAccessory();
		if(returnResult2 == kTrue)
		{
			ItemData.setData
			(	4,
				i,
				-1,
				-114,
				TPLMediatorClass::CurrLanguageID, 
				"Accessory Table",
				kTrue,
				"",
				kFalse,
				1 ,// For Tables only
				kFalse,//For Event Field
				-1,//For Header
				-1,//for childTag
				6//for TableType
			);

			//CA(it1->getName());
			ITEMListData.setAll("Accessory Table",-114,1,1,0,2);
			ITEMListData.setHitCount(2);				
			ITEMDataNodeList.push_back(ITEMListData);					
			//i++;
			count4=i;
			i++;
		}


		///////		For Hybrid Table
		bool16 result1= kTrue ; //ptrIAppFramework->ConfigCache_getDisplayItemHybridTable();
		if(result1 == kTrue)
		{
			ItemData.setData
					(4,
					i,
					-1,
					-115,
					TPLMediatorClass::CurrLanguageID, 
					"Advanced Table",
					kTrue,
					"",
					kFalse,
					1, // For Tables only
					kFalse,//For Event Field
					-1,//For Header
					-1,//for childTag
					3  //for TableType
					);
				
				//CA(it1->getName());
			ITEMListData.setAll("Advanced Table",-115,1,1,0,2);
			
			ITEMListData.setHitCount(2);				
			ITEMDataNodeList.push_back(ITEMListData);					
			count4=i;
			i++;
		}

/////////		For Hybrid Table
		/*AP*/
		//VectorTypeInfoPtr typeValObj = ptrIAppFramework->AttributeCache_getHybridTableTypesForClassAndParents(CurrentClassID);
		//if(typeValObj!=nil)
		//{
		//	VectorTypeInfoValue::iterator it1;					
		//	PMString Name("");
		//	for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
		//	{
		//		//CA(it1->getName());
		//		Name = it1->getName();
		//		//Name.Append("_Hybrid");
		//		ItemData.setData
		//		(	4,
		//			i,
		//			-1,
		//			it1->getType_id(),
		//			TPLMediatorClass::CurrLanguageID, 
		//			Name,
		//			kTrue,
		//			it1->getCode(),
		//			kFalse,
		//			1,
		//			kFalse,
		//			-1,
		//			-1,
		//			3
		//		);
		//		
		//		PMString temp(Name);
		//		temp.Append(" (Table)"/*" : Table"*/);
		//		ITEMListData.setAll(temp,it1->getType_id(),1,1,0,2);
		//		ITEMListData.setHitCount(2);				
		//		ITEMDataNodeList.push_back(ITEMListData);					
		//	//	listHelper.AddElement(lstboxControlView, it1->getName(), kItemPanelTextWidgetID, i);
		//	//	listHelper.AddIcons(lstboxControlView, i, 1); 
		//		count4=i;
		//		i++;
		//	}
		//}

		//if(typeValObj)		
		//	delete typeValObj;

//////		End

		do
		{
			

			PMString temp("Item"),insertString("");
			//temp = ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename(temp),TPLMediatorClass::CurrLanguageID);
			insertString.Append("All ");
			insertString.Append(temp);
			insertString.Append(" Images");

			ItemData.setData
			(	4,
				i,
				-1,
				-97,
				TPLMediatorClass::CurrLanguageID, 
				insertString,//"All Item Images",
				kTrue,
				insertString,//"All Item Images",
				kTrue,
				0,
				kFalse,-1,-1,-1,-1,intSprItemPerFrameFlow	//-------

			);
				
			ITEMListData.setAll(/*"All Item Images"*/insertString,-97,1,1,0,1);
			ITEMListData.setHitCount(1);
			ITEMDataNodeList.push_back(ITEMListData);

			insertString.Clear();
			insertString.Append("First Available ");
			insertString.Append(temp);
			insertString.Append(" Image");

			ItemData.setData
			(	4,
				i,
				-1,
				-99,
				TPLMediatorClass::CurrLanguageID, 
				insertString,//"First Available Item Image",//"Primary Item Image",
				kTrue,
				insertString,//"First Available Item Image",//"Primary Item Image",
				kTrue,
				0,
				kFalse,-1,-1,-1,-1,intSprItemPerFrameFlow	//-------
			);

			///ITEMListData.setAll("Primary Item Image",-99,1,1,0,1);
			ITEMListData.setAll(/*"First Available Item Image"*/insertString,-99,1,1,0,1);
			ITEMListData.setHitCount(1);
			ITEMDataNodeList.push_back(ITEMListData);
			insertString.Clear();
            
            
            VectorAttributeInfoValue::iterator it5;
            for(it5=AttrInfoVectPtr->begin();it5!=AttrInfoVectPtr->end();it5++)
            {//CAlert::InformationAlert("4");
                
                if(it5->getDataType() == "ASSET")
                {
                            PMString displayName("");
                            displayName.Append(it5->getDisplayName());

                            ItemData.setData
                            (
                             4,						// which List 4 = item List
                             i,						// Index
                             it5->getAttributeId(),// Attribute Id for Core element
                             -1,					// type id ... here we are going to feed Item id
                             TPLMediatorClass::CurrLanguageID,
                             displayName,//it4->getDisplay_name(),
                             kTrue,
                             "ASSET",//it4->getDisplay_name(),// Code is replaced by Attribute Name
                             kTrue,
                             0,           //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
                             kFalse,
                             -1,-1,-1,-1,intSprItemPerFrameFlow,-1,1,-1,1
                             );
                            
                            ITEMListData.setAll(displayName,it5->getAttributeId(),1,1,0,1);
                            ITEMListData.setHitCount(1);
                            ITEMDataNodeList.push_back(ITEMListData);
                            count4=i;
                            i++;
                
                }
            }
						
			VectorTypeInfoPtr typeValObj = ptrIAppFramework->StructureCache_getItemImagesForClassAndParents(CurrentClassID);
			if(typeValObj!=nil)
			{
				VectorTypeInfoValue::iterator it1;					

				for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
				{
					//CA(it1->getName());
					ItemData.setData
					(4,
					i,
					-1,
					it1->getTypeId(),
					TPLMediatorClass::CurrLanguageID, 
					it1->getName(),
					kTrue,
					it1->getCode(),
					kTrue,
					0,
					kFalse,-1,-1,-1,-1,intSprItemPerFrameFlow	//-------
					);

					ITEMListData.setAll(it1->getName(),it1->getTypeId(),1,1,0,1);
					ITEMListData.setHitCount(1);
					ITEMDataNodeList.push_back(ITEMListData);
				//	listHelper.AddElement(lstboxControlView, it1->getName(), kItemPanelTextWidgetID, i);
				//	listHelper.AddIcons(lstboxControlView, i, 1); 
					count4=i;
					i++;
				}
			}

			if(typeValObj)		
				delete typeValObj;

/////////////////////	Added on 23-10-08
			//CA("Now PV/MPV Images");
			vector<double> attrIDSWithPVMPVImgTyp;
			VectorAttributeInfoValue::iterator it4;
			for(it4=AttrInfoVectPtr->begin();it4!=AttrInfoVectPtr->end();it4++)
			{//CAlert::InformationAlert("4");

				if(it4->getPicklistGroupId() > 0)
				{
					//ptrIAppFramework->LogDebug(it4->getDisplayName());
					for(int32 index = 1 ; index <=5 ; index++)
					{
						bool16 returnResult = kFalse;
						returnResult = ptrIAppFramework->CONFIGCACHE_getPVImageFileName(index);
						if(returnResult)
						{
							//PMString temp("");
							//temp.AppendNumber(it4->getPv_type_id());
							//temp.AppendNumber(index);
							//int32 Pv_type_id = temp.GetAsNumber();
							PMString displayName("");
							displayName.Append(it4->getDisplayName());
							displayName.Append(" ");
							displayName.AppendNumber(index);
							ItemData.setData
							(	4,						// which List 4 = item List
								i,						// Index	
								it4->getAttributeId(),// Attribute Id for Core element
								it4->getPicklistGroupId(),					// type id ... here we are going to feed Item id
								TPLMediatorClass::CurrLanguageID,			
								displayName,//it4->getDisplay_name(), 
								kTrue,
								displayName,//it4->getDisplay_name(),// Code is replaced by Attribute Name
								kTrue,
								0,           //Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
								kFalse,
								-1,-1,-1,-1,intSprItemPerFrameFlow,-1,index,-1,1
							);
							
							ITEMListData.setAll(/*it4->getDisplay_name()*/displayName,it4->getAttributeId(),1,1,0,1);
							ITEMListData.setHitCount(1);
							ITEMDataNodeList.push_back(ITEMListData);
							count4=i;
							i++;
						}
					}
					attrIDSWithPVMPVImgTyp.push_back(it4->getAttributeId());					
				}
			}
            
            
            
            
            
			if(AttrInfoVectPtr)
					delete AttrInfoVectPtr;

/* No longer required now - Bugtracker - #375 - https://pub.plan.io/issues/375
            // Brand Manufacturer, Supplier/Vendor images...
			for(int32 j=0; j < 5; j ++)
			{
				insertString.Clear();
				insertString.Append("Brand Image ");
				insertString.AppendNumber( j +1);
				//insertString.Append(" Image");

				double assetTypeId = -207 - j;

				ItemData.setData
				(	4,
					i,
					-1,
					assetTypeId,
					TPLMediatorClass::CurrLanguageID, 
					insertString,
					kTrue,
					insertString,
					kTrue,
					0
				);

				
				ITEMListData.setAll(insertString,assetTypeId,1,1,0,1);
				ITEMListData.setHitCount(1);
				ITEMDataNodeList.push_back(ITEMListData);		
				i++;
				count4=i;
				insertString.Clear();
			}

			for(int32 j=0; j < 5; j ++)
			{
				insertString.Clear();
				insertString.Append("Manufacturer Image ");
				insertString.AppendNumber( j +1);
				//insertString.Append(" Image");

				double assetTypeId = -212 - j;

				ItemData.setData
				(	4,
					i,
					-1,
					assetTypeId,
					TPLMediatorClass::CurrLanguageID, 
					insertString,
					kTrue,
					insertString,
					kTrue,
					0
				);

				
				ITEMListData.setAll(insertString,assetTypeId,1,1,0,1);
				ITEMListData.setHitCount(1);
				ITEMDataNodeList.push_back(ITEMListData);		
				i++;
				count4=i;
				insertString.Clear();
			}
 */

			for(int32 j=0; j < 5; j ++)
			{
				insertString.Clear();
				insertString.Append("Org Image ");
				insertString.AppendNumber( j +1);
				//insertString.Append(" Image");

				double assetTypeId = -217 - j;

				ItemData.setData
				(	4,
					i,
					-1,
					assetTypeId,
					TPLMediatorClass::CurrLanguageID, 
					insertString,
					kTrue,
					insertString,
					kTrue,
					0
				);

				
				ITEMListData.setAll(insertString,assetTypeId,1,1,0,1);
				ITEMListData.setHitCount(1);
				ITEMDataNodeList.push_back(ITEMListData);		
				i++;
				count4=i;
				insertString.Clear();
			}

			/*AP*/
			//bool16 returnResult3= ptrIAppFramework->CONFIGCACHE_getShowWhirlPool();
			//if(returnResult3 == kTrue)
			//{
			//	insertString.Clear();
			//	insertString.Append("Event ");
			//	insertString.Append(temp);
			//	insertString.Append(" Swatch Image");

			//	ItemData.setData
			//	(	4,
			//		i,
			//		-95,
			//		-95,
			//		TPLMediatorClass::CurrLanguageID, 
			//		insertString,//"First Available Item Image",//"Primary Item Image",
			//		kTrue,
			//		insertString,//"First Available Item Image",//"Primary Item Image",
			//		kTrue,
			//		0,
			//		kFalse,-1,-1,-1,-1,intSprItemPerFrameFlow	//-------
			//	);

			//	///ITEMListData.setAll("Primary Item Image",-99,1,1,0,1);
			//	ITEMListData.setAll(/*"First Available Item Image"*/insertString,-95,1,1,0,1);
			//	ITEMListData.setHitCount(1);
			//	ITEMDataNodeList.push_back(ITEMListData);
			//	insertString.Clear();
			//}
			//InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
			//if(ptrImageHelper != nil)
			//{
			//	//CA("ptrImageHelper != nil");
			//	ptrImageHelper->setattributesWithPVMPV(attrIDSWithPVMPVImgTyp,kFalse);
			//	attrIDSWithPVMPVImgTyp.clear();
			//}
			
////////////////////////////////// End on  23-10-08
            
            VectorAttributeGroupPtr attributeGroupVectPtr = NULL;
            attributeGroupVectPtr  = ptrIAppFramework->StructureCache_getItemAttributeGroups();
            if(attributeGroupVectPtr == NULL)
            {
                ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::populateItemPanelLstbox::StructureCache_getItemAttributeGroups VectorAttributeGroupPtr is nil ");
            }
            else
            {
                VectorAttributeGroup::iterator itAttriGrp;
                
                for(itAttriGrp=attributeGroupVectPtr->begin();itAttriGrp!=attributeGroupVectPtr->end();itAttriGrp++)
                {
                    
                    ItemData.setData
                    (	4,						// which List 4 = item List
                     i,						// Index
                     itAttriGrp->getAttributeGroupId(),
                     -1,					// type id ... here we are going to feed Item id
                     TPLMediatorClass::CurrLanguageID,
                     itAttriGrp->getName() + " (Group)",
                     kTrue,
                     itAttriGrp->getKey(),// Code is replaced by Attribute Name
                     kFalse,
                     0,	//Changed from "1" to "0" to obtain text frame on the Document, earlier it was giving table frame
                     0, //it2->getPubItem()
                     -1,-1,-1,-1,-1,-1,-1,-1,
                     6, // data Type for Attrribute Group
                     -1,field_5
                     );
                    
                    ITEMListData.setAll(itAttriGrp->getName() + " (Group)",itAttriGrp->getAttributeGroupId(),1,1,0,0);
                    ITEMListData.setHitCount(9);
                    
                    ITEMDataNodeList.push_back(ITEMListData);
                    count4=i;
                    i++;
                    
                }
            }


		}while(0);
	}while(kFalse);
}

void TPLSelectionObserver::populateProjectPanelLstbox()
{
	//CA("TPLSelectionObserver::populateProjectPanelLstbox");
	do
	{
//////////////////////////////////// for populating Project tree  /////////////////////
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;

		if(TPLMediatorClass::iPanelCntrlDataPtr==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::populateProjectPanelLstbox::Not getting iPanelCntrlDataPtr");
			break;
		}
			
		ProjectDataNodeList.clear();
		IControlView* ProjectPanelCtrlView = 
		TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kProjectTreeViewWidgetID);
		if (ProjectPanelCtrlView == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::populateProjectPanelLstbox::ProjectPanelCtrlView is nil");
			break;
		}
		TPLMediatorClass::ProjectLstboxCntrlView=ProjectPanelCtrlView;
		ProjectPanelCtrlView->ShowView(kTrue);

		int32 PM_Project_Level =  ptrIAppFramework->getPM_Project_Levels();

		VectorElementInfoPtr eleValObj = ptrIAppFramework->StructureCache_getSectionCopyAttributesByLanguageId( TPLMediatorClass::CurrLanguageID);
		if(eleValObj==nil)
		{
			ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::populateProjectPanelLstbox::ElementCache_getProjectSecSubSecAttributesByIndex's eleValObj is nil");
			break;
		}

		VectorTypeInfoPtr typeValObj1= ptrIAppFramework->StructureCache_getSectionImageAttributes();  // 4 for Category Images

		TPLDataNode ProjectListData;
		int i=0;
		ProjectData.clearVector(5);
		VectorElementInfoValue::iterator it;

		PMString temp1;
		PMString temp2;
		PMString Level;
		PMString temp3("Event");
		Level.AppendNumber(-1); //For Event Level is -1 and for section -2 onwards ,-3,-4 so on 

		{	// Event Name hardcoded Attribute				
			temp2 = temp3 + " ";
			temp2.Append("Name");
			temp2.Append(temp1);

			ProjectData.setData
			(	5,
				i,
				-1,
				-1, //it->getElement_type_id(),
				TPLMediatorClass::CurrLanguageID, 
				temp2, //it->getDisplayName(),
				kTrue,
				Level, // ""
				kFalse,
				0
			);
			ProjectListData.setAll(temp2,-1,1,1,0,0);
			ProjectListData.setHitCount(0);
			ProjectDataNodeList.push_back(ProjectListData);
		
			TPLMediatorClass::count5=i;
			i++;	
		}

		for(it=eleValObj->begin();it!=eleValObj->end();it++)
		{			
			//if(it->getLanguageId() == TPLMediatorClass::CurrLanguageID)
			{				
				temp2 = temp3 + " ";
				temp2.Append(it->getDisplayName());
				temp2.Append(temp1);

				ProjectData.setData
				(	5,
					 i,
					 it->getElementId(),
					 -1, //it->getElement_type_id(),
					 TPLMediatorClass::CurrLanguageID, //it->getLanguageId(), 
					 temp2, //it->getDisplayName(),
					 kTrue,
					 Level, // ""
					 kFalse,
					 0
				 );
				ProjectListData.setAll(/*it->getDisplayName()*/temp2,it->getElementId(),1,1,0,0);
				ProjectListData.setHitCount(0);
				ProjectDataNodeList.push_back(ProjectListData);

			//	listHelper.AddElement(lstboxControlView, "Publication Name", kProjectPanelTextWidgetID, i);
			//	listHelper.AddIcons(lstboxControlView, i, 0); 
				TPLMediatorClass::count5=i;
				i++;	
			}
		}

		/*PMString temp1;
		PMString temp2;
		PMString Level;
		PMString temp3("Event");*/
		//temp3 = ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename(temp3),TPLMediatorClass::CurrLanguageID);

		Level.AppendNumber(-1); //For Event Level is -1 and for section -2 onwards ,-3,-4 so on 
		if(typeValObj1!=nil)
		{	
			VectorTypeInfoValue::iterator it2;
	
			for(it2=typeValObj1->begin();it2!=typeValObj1->end();it2++)
			{	
				temp2 = temp3 + " ";
				temp2.Append(it2->getName());
				temp2.Append(temp1);

				ProjectData.setData
				(5,
				i,
				-1,
				it2->getTypeId(),
				TPLMediatorClass::CurrLanguageID, 
				temp2, //it2->getName(),
				kTrue,
				Level,
				kTrue,
				0
				);

				ProjectListData.setAll(temp2,it2->getTypeId(),1,1,0,1);
				ProjectListData.setHitCount(1);
				ProjectDataNodeList.push_back(ProjectListData);		
				i++;
				temp2.Clear();				
			}
		}


		for(it=eleValObj->begin();it!=eleValObj->end();it++)
		{
			//if(TPLMediatorClass::CurrLanguageID == it->getLanguageId())
				if(it->getPicklistGroupId() > 0)
				{
					for(int32 index = 1 ; index <=5 ; index++)
					{
						bool16 returnResult = kFalse;
						returnResult = ptrIAppFramework->CONFIGCACHE_getPVImageFileName(index);
						if(returnResult)
						{
							//PMString tempPv_type_id("");
							//tempPv_type_id.AppendNumber(it->getPv_type_id());
							//tempPv_type_id.AppendNumber(index);
							//int32 Pv_type_id = tempPv_type_id.GetAsNumber();
							PMString displayName("");
							displayName.Append(it->getDisplayName());
							displayName.Append(" ");
							displayName.AppendNumber(index);

							PMString temp("-");
							temp.AppendNumber(index/*it->getElement_type_id()*/);
							ProjectData.setData
							(	5,
								 i,
								 it->getElementId(),
								 it->getPicklistGroupId(),
								 TPLMediatorClass::CurrLanguageID, //it->getLanguageId(), 
								 displayName,//it->getDisplay_name(),
								 kTrue,
								 temp,
								 kTrue,
								 0,
								 kFalse,
								-1,-1,-1,-1,-1,-1,index-1,1
							 );

							ProjectListData.setAll(/*it->getDisplay_name()*/displayName,it->getElementId(),1,1,0,1);
							ProjectListData.setHitCount(1);
							ProjectDataNodeList.push_back(ProjectListData);
						//	TPLMediatorClass::count5=i;	
							i++;
						}
					}
				}								
		}		

		/*AP*/
		//int32 NumberOfSectionLevels = ptrIAppFramework->ProjectCache_getLevelForSection();
		int32 NumberOfSectionLevels = 5;

		/*PMString ASD("NumberOfSectionLevels : ");
		ASD.AppendNumber(NumberOfSectionLevels);
		CA(ASD);*/
		
		for(int32 levelNo = 1; levelNo <= NumberOfSectionLevels; levelNo++)
		{
			

			temp1.Clear();
			temp2.Clear();
			Level.Clear();
			temp3.Clear();

			temp1.Append(", Level ");
			Level.Clear();
			Level.AppendNumber(-1-levelNo); ////For Event Level is -1 and for section -2 onwards ,-3,-4 so on 
			temp1.AppendNumber(levelNo);

			temp3.Append("Section");
			//temp3 = ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename(temp3),TPLMediatorClass::CurrLanguageID);

			//CA("temp3 : " + temp3);
			//Level.AppendNumber(-2); //For Event Level is -1 and for section -2 onwards ,-3,-4 so on 

			eleValObj = ptrIAppFramework->StructureCache_getSectionCopyAttributesByLanguageId( TPLMediatorClass::CurrLanguageID);
			if(eleValObj==nil)
			{
				ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::populateProjectPanelLstbox::ElementCache_getProjectSecSubSecAttributesByIndex's eleValObj is nil");
				break;
			}
				
			{	// Section Name hardcoded Attribute					
				temp2 = temp3 + " ";
				temp2.Append("Name");
				temp2.Append(temp1);

				ProjectData.setData
				(	5,
					i,
					-1,
					-1, //it->getElement_type_id(),
					TPLMediatorClass::CurrLanguageID, 
					temp2, //it->getDisplayName(),
					kTrue,
					Level, // ""
					kFalse,
					0
				);
				ProjectListData.setAll(temp2,-1,1,1,0,0);
				ProjectListData.setHitCount(0);
				ProjectDataNodeList.push_back(ProjectListData);
		
				TPLMediatorClass::count5=i;
				i++;	
			}

			for(it=eleValObj->begin();it!=eleValObj->end();it++)
			{
				//if(it->getLanguageId() == TPLMediatorClass::CurrLanguageID)
				{
					temp2 = temp3 + " ";
					temp2.Append(it->getDisplayName());
					temp2.Append(temp1);

					//CA(temp2);

					ProjectData.setData
					(	5,
						i,
						it->getElementId(),
						-1, //it->getElement_type_id(),
						TPLMediatorClass::CurrLanguageID, //it->getLanguageId(), 
						temp2, // it->getDisplay_name(),
						kTrue,
						Level,
						kFalse,
						0
					);

				//	listHelper.AddElement(lstboxControlView, "Publication Description", kProjectPanelTextWidgetID, i);
				//	listHelper.AddIcons(lstboxControlView, i, 0); 
					ProjectListData.setAll(/*it->getDisplay_name()*/temp2,it->getElementId(),1,1,0,0);
					ProjectDataNodeList.push_back(ProjectListData);
					TPLMediatorClass::count5=i;
					i++;
					temp2.Clear();
				}
			}

			
			if(typeValObj1!=nil)
			{	
				VectorTypeInfoValue::iterator it2;
		
				for(it2=typeValObj1->begin();it2!=typeValObj1->end();it2++)
				{	
					temp2 = temp3 + " ";
					temp2.Append(it2->getName());
					temp2.Append(temp1);

					ProjectData.setData
					(5,
					i,
					-1,
					it2->getTypeId(),
					TPLMediatorClass::CurrLanguageID, 
					temp2, //it2->getName(),
					kTrue,
					Level,
					kTrue,
					0
					);

					ProjectListData.setAll(temp2,it2->getTypeId(),1,1,0,1);
					ProjectListData.setHitCount(1);
					ProjectDataNodeList.push_back(ProjectListData);		
					i++;
					temp2.Clear();				
				}
			}

			for(it=eleValObj->begin();it!=eleValObj->end();it++)
			{
				//if(TPLMediatorClass::CurrLanguageID == it->getLanguageId())
					if(it->getPicklistGroupId() > 0)
					{
						for(int32 index = 1 ; index <=5 ; index++)
						{
							bool16 returnResult = kFalse;
							returnResult = ptrIAppFramework->CONFIGCACHE_getPVImageFileName(index);
							if(returnResult)
							{
								//PMString tempPv_type_id("");
								//tempPv_type_id.AppendNumber(it->getPv_type_id());
								//tempPv_type_id.AppendNumber(index);
								//int32 Pv_type_id = tempPv_type_id.GetAsNumber();
								temp2 = temp3 + " ";
								PMString displayName("");
								displayName.Append(it->getDisplayName());
								displayName.Append(" ");
								displayName.AppendNumber(index);

								temp2.Append(displayName);
								temp2.Append(temp1);

								PMString temp("-");
								temp.AppendNumber(index/*it->getElement_type_id()*/);
								ProjectData.setData
								(	5,
									i,
									it->getElementId(),
									it->getPicklistGroupId(),
									TPLMediatorClass::CurrLanguageID, //it->getLanguageId(), 
									temp2, //displayName,//it->getDisplay_name(),
									kTrue,
									temp,
									kTrue,
									0,
									kFalse,
									-1,-1,-1,-1,-1,-1,index,-1,1
								);

								ProjectListData.setAll(/*it->getDisplay_name()*/temp2,it->getElementId(),1,1,0,1);
								ProjectListData.setHitCount(1);
								ProjectDataNodeList.push_back(ProjectListData);
								i++;
								temp2.Clear();
							}
						}
					}
			}
		}
		if(eleValObj)
			delete eleValObj;
	
		/*AP*/
		//if(PM_Project_Level == 3)
		//{
		//	//CA("PM_Project_Level == 3");
		//	eleValObj = ptrIAppFramework->ElementCache_getProjectSecSubSecAttributesByIndex(3, TPLMediatorClass::CurrLanguageID);
		//	if(eleValObj==nil)
		//	{
		//		ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::populateProjectPanelLstbox::ElementCache_getProjectSecSubSecAttributesByIndex's eleValObj is nil");
		//		break;
		//	}

		//	for(it=eleValObj->begin();it!=eleValObj->end();it++)
		//	{
		//		if(TPLMediatorClass::CurrLanguageID == it->getLanguage_id())
		//		{
		//			ProjectData.setData
		//				(5,
		//				i,
		//				it->getElementId(),
		//				it->getElement_type_id(),
		//				it->getLanguage_id(), 
		//				it->getDisplay_name(),
		//				kTrue,
		//				"",
		//				kFalse,
		//				0
		//				);

		//			ProjectListData.setAll(it->getDisplay_name(),it->getElementId(),1,1,0,0);
		//			ProjectDataNodeList.push_back(ProjectListData);
		//			TPLMediatorClass::count5=i;
		//			i++;
		//		}
		//	}

		//	for(it=eleValObj->begin();it!=eleValObj->end();it++)
		//	{
		//		if(TPLMediatorClass::CurrLanguageID == it->getLanguage_id())
		//			if(it->getPv_type_id() > 0)
		//			{
		//				for(int32 index = 1 ; index <=5 ; index++)
		//				{
		//					bool16 returnResult = kFalse;
		//					returnResult = ptrIAppFramework->CONFIGCACHE_getPVImageFileName(index);
		//					if(returnResult)
		//					{
		//						//PMString tempPv_type_id("");
		//						//tempPv_type_id.AppendNumber(it->getPv_type_id());
		//						//tempPv_type_id.AppendNumber(index);
		//						//int32 Pv_type_id = tempPv_type_id.GetAsNumber();
		//						PMString displayName("");
		//						displayName.Append(it->getDisplay_name());
		//						displayName.Append(" ");
		//						displayName.AppendNumber(index);

		//						ProjectData.setData
		//						(	5,
		//							i,
		//							it->getElementId(),
		//							it->getPv_type_id(),
		//							it->getLanguage_id(), 
		//							displayName,//it->getDisplay_name(),
		//							kTrue,
		//							"",
		//							kTrue,
		//							0,
		//							kFalse,
		//							-1,-1,-1,-1,-1,-1,index,-1,1
		//							);
		//						ProjectListData.setAll(/*it->getDisplay_name()*/displayName,it->getElementId(),1,1,0,1);
		//						ProjectListData.setHitCount(1);
		//						ProjectDataNodeList.push_back(ProjectListData);
		//						i++;
		//					}
		//				}
		//			}
		//	}
		//	if(eleValObj)
		//		delete eleValObj;
		//}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////Added by Chetan Dogra on 11-Dec-06 //////////// From here //////
		//*AP *//
		//do
		//{
		//	//VectorElementInfoPtr eleValObj = ptrIAppFramework->getCategoryCopyAttributesForPrint(TPLMediatorClass::CurrLanguageID);
		//	VectorElementInfoPtr eleValObj = ptrIAppFramework->StructureCache_getSectionCopyAttributesByLanguageId(TPLMediatorClass::CurrLanguageID);
		//	if(eleValObj==nil)
		//	{
		//		ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::populateProjectPanelLstbox::getCategoryCopyAttributesForPrint's eleValObj is nil ");
		//		break;
		//	}

		//	//CA("Category Image");
		//	//VectorTypeInfoPtr typeValObj1= ptrIAppFramework->ElementCache_getImageAttributesByIndex(4);  // 4 for Category Images
		//	VectorTypeInfoPtr typeValObj1= ptrIAppFramework->StructureCache_getSectionImageAttributes();

		//	//int i=0;
		//	VectorElementInfoValue::iterator it;

		//	int32 CatagoryRootLevel = -1;
		//	VectorClassInfoPtr vectClassInfoValuePtr = ptrIAppFramework->ClassificationTree_getRoot(TPLMediatorClass::CurrLanguageID);
		//	if(vectClassInfoValuePtr!=nil)
		//	{
		//		VectorClassInfoValue::iterator itrr = vectClassInfoValuePtr->begin();	

		//		CatagoryRootLevel = itrr->getClass_level();
		//		
		//		if(vectClassInfoValuePtr)
		//			delete vectClassInfoValuePtr;
		//	}

		//	PMString catagoryLevelAsPerDB("");		
		//	/*AP*/
		//	//int32 levels = ptrIAppFramework->getTreeLevelsIncludingMaster();
		//	int32 levels =4;


		//	PMString temp1;
		//	PMString temp2;
		//	PMString Level;
		//	PMString temp3("Category");
		//	//temp3 = ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename(temp3),TPLMediatorClass::CurrLanguageID);
		//	for(int32 j=1 ; j<=levels ;j++,CatagoryRootLevel++)
		//	{
		//		temp1.Append(", Level ");
		//		Level.Clear();
		//		Level.AppendNumber(j);
		//		temp1.AppendNumber(j);

		//		catagoryLevelAsPerDB.Clear();
		//		catagoryLevelAsPerDB.AppendNumber(CatagoryRootLevel);

		//		for(it=eleValObj->begin();it!=eleValObj->end();it++)
		//		{
		//			//if(TPLMediatorClass::CurrLanguageID == it->getLanguageId())
		//			{
		//				temp2 = temp3 + " ";
		//				temp2.Append(it->getDisplayName());
		//				temp2.Append(temp1);
		//				
		//				ProjectData.setData
		//				(	5,
		//					i,
		//					it->getElementId(),
		//					-1, //it->getElement_type_id(),
		//					TPLMediatorClass::CurrLanguageID, //it->getLanguageId(),
		//					temp2,//it->getDisplay_name(),
		//					kTrue,
		//					catagoryLevelAsPerDB, //Level, // Adding Level no
		//					kFalse,
		//					0
		//				);

		//				ProjectListData.setAll(temp2,it->getElementId(),1,1,0,0);
		//				ProjectListData.setHitCount(0);
		//				ProjectDataNodeList.push_back(ProjectListData);

		//				TPLMediatorClass::count5=i;
		//				i++;
		//				temp2.Clear();
		//				/*temp2.AppendNumber(it->getElement_type_id());
		//				temp2.Clear();*/
		//			}
		//		}

		//		if(typeValObj1!=nil)
		//		{	
		//			VectorTypeInfoValue::iterator it2;
		//	
		//			for(it2=typeValObj1->begin();it2!=typeValObj1->end();it2++)
		//			{	
		//				temp2 = temp3;
		//				temp2.Append(it2->getName());
		//				temp2.Append(temp1);

		//				ProjectData.setData
		//				(5,
		//				i,
		//				-1,
		//				it2->getTypeId(),
		//				TPLMediatorClass::CurrLanguageID, 
		//				temp2, //it2->getName(),
		//				kTrue,
		//				catagoryLevelAsPerDB, //Level,
		//				kTrue,
		//				0
		//				);

		//				ProjectListData.setAll(temp2,it2->getTypeId(),1,1,0,1);
		//				ProjectListData.setHitCount(1);
		//				ProjectDataNodeList.push_back(ProjectListData);		
		//				i++;
		//				temp2.Clear();				
		//			}
		//		}

		//		////	Added for PV/MPV Image Type on 61108
		//		for(it=eleValObj->begin();it!=eleValObj->end();it++)
		//		{
		//			//if(TPLMediatorClass::CurrLanguageID == it->getLanguageId())
		//				if(it->getPicklistGroupId() > 0)
		//				{
		//					//CA("Adding image type ");
		//					//CA(it->getDisplay_name());
		//					for(int32 index = 1 ; index <=5 ; index++)
		//					{
		//						bool16 returnResult = kFalse;
		//						returnResult = ptrIAppFramework->CONFIGCACHE_getPVImageFileName(index);
		//						if(returnResult)
		//						{
		//							//PMString tempPv_type_id("");
		//							//tempPv_type_id.AppendNumber(it->getPv_type_id());
		//							//tempPv_type_id.AppendNumber(index);
		//							//int32 Pv_type_id = tempPv_type_id.GetAsNumber();
		//							PMString displayName("");
		//							displayName.Append(it->getDisplayName());
		//							displayName.Append(" ");
		//							displayName.AppendNumber(index);

		//							temp2 = temp3;
		//							temp2.Append(displayName);//(it->getDisplay_name());
		//							temp2.Append(temp1);
		//							
		//							CatagoryData.setData
		//							(	5,
		//								i,
		//								it->getElementId(),
		//								it->getPicklistGroupId(),
		//								TPLMediatorClass::CurrLanguageID, //it->getLanguageId(),
		//								temp2,//it->getDisplay_name(),
		//								kTrue,
		//								catagoryLevelAsPerDB, //Level, // Adding Level no
		//								kTrue,
		//								0,
		//								kFalse,
		//								-1,-1,-1,-1,-1,-1,index,-1,1
		//							);

		//							ProjectListData.setAll(temp2,it->getElementId(),1,1,0,1);
		//							ProjectListData.setHitCount(1);
		//							ProjectDataNodeList.push_back(ProjectListData);
		//							i++;
		//							temp2.Clear();	
		//						}
		//					}
		//				}
		//		}
		//		///	END on 61108
		//		temp1.Clear();
		//	}

		//	/*AP*/
		//	//if(ptrIAppFramework->CONFIGCACHE_getIndesignSbShowPcAttribute()) // add these attributes Only when INDESIGN_SB_SHOW_PC_ATTRIBUTE CONFIG IS 1. 
		//	//{																// No need to show Product Catalog related attributes everytime, use above config for it					
		//	//	ProjectData.setData
		//	//	(5,
		//	//	i,
		//	//	-2001,
		//	//	-1,
		//	//	1, 
		//	//	"Job Name",
		//	//	kTrue,
		//	//	"-1",
		//	//	kFalse,
		//	//	0
		//	//	);

		//	//	ProjectListData.setAll("Job Name",-2001,1,1,0,0);
		//	//	ProjectDataNodeList.push_back(ProjectListData);

		//	//	TPLMediatorClass::count5=i;
		//	//	i++;

		//	//	ProjectData.setData
		//	//	(5,
		//	//	i,
		//	//	-2002,
		//	//	-1,
		//	//	1, 
		//	//	"Job Expiry Date",
		//	//	kTrue,
		//	//	"-1",
		//	//	kFalse,
		//	//	0
		//	//	);

		//	//	ProjectListData.setAll("Job Expiry Date",-2002,1,1,0,0);
		//	//	ProjectDataNodeList.push_back(ProjectListData);

		//	//	TPLMediatorClass::count5=i;
		//	//	i++;

		//	//	ProjectData.setData
		//	//	(5,
		//	//	i,
		//	//	-2003,
		//	//	-1,
		//	//	1, 
		//	//	"Job Index",
		//	//	kTrue,
		//	//	"-1",
		//	//	kFalse,
		//	//	0
		//	//	);

		//	//	ProjectListData.setAll("Job Index",-2003,1,1,0,0);
		//	//	ProjectDataNodeList.push_back(ProjectListData);

		//	//	TPLMediatorClass::count5=i;
		//	//	i++;
		//	//}

		//	if(eleValObj)
		//		delete eleValObj;

		//}while(kFalse);
//////////////////////////////////////// 11-Dec-06 ///////////////////// Till here ///////////
//////////////////////////////////////////////////////////////////////////////////////////////
////	For Hybrid Table
	/*AP*/
		//VectorTypeInfoPtr typeValObj = ptrIAppFramework->AttributeCache_getHybridTableTypesForClassAndParents(CurrentClassID);
		//if(typeValObj!=nil)
		//{
		//	VectorTypeInfoValue::iterator it1;					
		//	PMString Name("");
		//	for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
		//	{
		//		//CA(it1->getName());
		//		Name = it1->getName();
		//		//Name.Append("_Hybrid");
		//		ProjectData.setData
		//		(
		//			5,
		//			i,
		//			-1,
		//			it1->getType_id(),
		//			TPLMediatorClass::CurrLanguageID, 
		//			it1->getName(),
		//			kTrue,
		//			it1->getCode(),
		//			kFalse,
		//			1,-1,-1,-1,3
		//		);
		//		
		//		PMString temp(Name);
		//		temp.Append(" (Table)"/*" : Table"*/);
		//		ProjectListData.setAll(temp,it1->getType_id(),1,1,0,2);
		//		ProjectListData.setHitCount(2);				
		//		ProjectDataNodeList.push_back(ProjectListData);					
		//		//	listHelper.AddElement(lstboxControlView, it1->getName(), kItemPanelTextWidgetID, i);
		//		//	listHelper.AddIcons(lstboxControlView, i, 1); 
		//			//count3=i;
		//		i++;
		//	}
		//}

		//if(typeValObj)		
		//	delete typeValObj;

		//if(ptrIAppFramework->CONFIGCACHE_getIndesignSbShowPcAttribute()) // add these attributes Only when INDESIGN_SB_SHOW_PC_ATTRIBUTE CONFIG IS 1. 
		//{																// No need to show Product Catalog related attributes everytime, use above config for it					

		//	ProjectData.setData
		//	(	
		//		5,
		//		i,
		//		-1,
		//		-222,
		//		TPLMediatorClass::CurrLanguageID, 
		//		"Publication Logo 1",
		//		kTrue,
		//		"Publication Logo 1",
		//		kTrue,
		//		0
		//	);
	
		//	ProjectListData.setAll("Publication Logo 1",-222,1,1,0,1);
		//	ProjectListData.setHitCount(1);
		//	ProjectDataNodeList.push_back(ProjectListData);		
		//	i++;
		//	

		//	ProjectData.setData
		//	(	
		//		5,
		//		i,
		//		-1,
		//		-223,
		//		TPLMediatorClass::CurrLanguageID, 
		//		"Publication Logo 2",
		//		kTrue,
		//		"Publication Logo 2",
		//		kTrue,
		//		0
		//	);
	
		//	ProjectListData.setAll("Publication Logo 2",-223,1,1,0,1);
		//	ProjectListData.setHitCount(1);
		//	ProjectDataNodeList.push_back(ProjectListData);		
		//	i++;
		//	
		//}		
//////////	End
////////	Added for proofing

		/*ProjectData.setData
		(	
			5,
			i,
			-331,
			-1,
			TPLMediatorClass::CurrLanguageID, 
			"Proof Info",
			kTrue,
			"",
			kFalse,
			0
		);
		ProjectListData.setAll("Proof Info",-331,1,1,0,0);
		ProjectListData.setHitCount(0);
		ProjectDataNodeList.push_back(ProjectListData);		
		i++;

		ProjectData.setData
		(	
			5,
			i,
			-332,
			-1,
			TPLMediatorClass::CurrLanguageID, 
			"Proof Comment",
			kTrue,
			"Proof Comment",
			kFalse,
			0
		);

		ProjectListData.setAll("Proof Comment",-332,1,1,0,0);
		ProjectListData.setHitCount(0);
		ProjectDataNodeList.push_back(ProjectListData);		
		i++;

		ProjectData.setData
		(	
			5,
			i,
			-1,
			-333,
			TPLMediatorClass::CurrLanguageID, 
			"Proof Image",
			kTrue,
			"-1",
			kTrue,
			0,
			kFalse,
			-1,-1,-1,-1,-1,-1,-1,-1,1
		);

		ProjectListData.setAll("Proof Image",-333,1,1,0,1);
		ProjectListData.setHitCount(1);
		ProjectDataNodeList.push_back(ProjectListData);		
		i++;*/
///////////	END

	}while(kFalse);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
//Following method is added by vijay on 9-10-2006////////////////////////////////////////FROM HERE
void TPLSelectionObserver::populateCatagoryPanelLstbox()
{
	//CA("TPLSelectionObserver::populateCatagoryPanelLstbox");
	do
	{
		///////////////////////////// for populating Catagory tree  /////////////////////
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;

		if(TPLMediatorClass::iPanelCntrlDataPtr==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::populateCategoryPanelLstbox::iPanelCntrlDataPtr is nil");		
			break;
		}
			
		CatagoryDataNodeList.clear();
		IControlView* ProjectPanelCtrlView = TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kProjectTreeViewWidgetID);
		if (ProjectPanelCtrlView == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::populateCategoryPanelLstbox::ProjectPanelCtrlView is nil");
			break;
		}
		TPLMediatorClass::ProjectLstboxCntrlView=ProjectPanelCtrlView;
		ProjectPanelCtrlView->ShowView(kTrue);

		int32 PM_Project_Level =  ptrIAppFramework->getPM_Project_Levels();

		//VectorElementInfoPtr eleValObj = ptrIAppFramework->getCategoryCopyAttributesForPrint(TPLMediatorClass::CurrLanguageID);
		VectorElementInfoPtr eleValObj = ptrIAppFramework->StructureCache_getSectionCopyAttributesByLanguageId(TPLMediatorClass::CurrLanguageID);
		if(eleValObj==nil)
		{
			ptrIAppFramework->LogError("AP7_TemplateBuilder::TPLSelectionObserver::populateCategoryPanelLstbox::getCategoryCopyAttributesForPrint's eleValObj is nil ");
			break;
		}

		//CA("Category Image");
		//VectorTypeInfoPtr typeValObj1= ptrIAppFramework->ElementCache_getImageAttributesByIndex(4);  // 4 for Category Images
		VectorTypeInfoPtr typeValObj1= ptrIAppFramework->StructureCache_getSectionImageAttributes();  

		TPLDataNode CatagoryListData;
		int i=0;
		CatagoryData.clearVector(5);
		VectorElementInfoValue::iterator it;

		/*AP*/
		//int32 levels = ptrIAppFramework->getTreeLevelsIncludingMaster();
		int32 levels = 4;

		PMString temp1;
		PMString temp2;
		PMString Level;
		PMString temp3("Category ");
		//temp3 = ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename(temp3),TPLMediatorClass::CurrLanguageID);

		for(int32 j=1 ; j<=levels ;j++ )
		{
			temp1.Append(", Level ");
			Level.Clear();
			Level.AppendNumber(j);
			temp1.AppendNumber(j);

			for(it=eleValObj->begin();it!=eleValObj->end();it++)
			{
				//CA(it->getDisplay_name());
				temp2 = temp3;
				temp2.Append(it->getDisplayName());
				temp2.Append(temp1);
				
				CatagoryData.setData
					(5,
					i,
					it->getElementId(),
					-1, //it->getElement_type_id(),
					it->getLanguageId(),
					temp2,//it->getDisplay_name(),
					kTrue,
					Level, // Adding Level no
					kFalse,
					0	
					);

				CatagoryListData.setAll(temp2,it->getElementId(),1,1,0,0);
				CatagoryListData.setHitCount(0);
				CatagoryDataNodeList.push_back(CatagoryListData);

				TPLMediatorClass::count5=i;
				i++;
				temp2.Clear();
				/*temp2.AppendNumber(it->getElement_type_id());
				temp2.Clear();*/
			}

			if(typeValObj1!=nil)
			{	
				VectorTypeInfoValue::iterator it2;
		
				for(it2=typeValObj1->begin();it2!=typeValObj1->end();it2++)
				{	
					temp2 = temp3;
					temp2.Append(it2->getName());
					temp2.Append(temp1);

					CatagoryData.setData
					(5,
					i,
					-1,
					it2->getTypeId(),
					TPLMediatorClass::CurrLanguageID, 
					temp2, //it2->getName(),
					kTrue,
					Level,
					kTrue,
					0
					);

					CatagoryListData.setAll(temp2,it2->getTypeId(),1,1,0,1);
					CatagoryListData.setHitCount(1);
					CatagoryDataNodeList.push_back(CatagoryListData);		
					i++;
					temp2.Clear();				
				}
			}
////	Added for PV/MPV Image Type on 61108
			for(it=eleValObj->begin();it!=eleValObj->end();it++)
			{
				if(it->getPicklistGroupId() > 0)
				{
					for(int32 index = 1 ; index <=5 ; index++)
					{
						bool16 returnResult = kFalse;
						returnResult = ptrIAppFramework->CONFIGCACHE_getPVImageFileName(index);
						if(returnResult)
						{
							//CA("Adding image type ");
							//CA(it->getDisplay_name());
							//PMString tmp("");
							//tmp.AppendNumber(it->getPv_type_id());
							//tmp.AppendNumber(index);
							//int32 Pv_type_id = tmp.GetAsNumber();
							PMString displayName("");
							displayName.Append(it->getDisplayName());
							displayName.Append(" ");
							displayName.AppendNumber(index);

							temp2 = temp3;
							temp2.Append(displayName);//it->getDisplay_name());
							temp2.Append(temp1);
							
							CatagoryData.setData
							(	5,
								i,
								it->getElementId(),
								it->getPicklistGroupId(),
								it->getLanguageId(),
								temp2,//it->getDisplay_name(),
								kTrue,
								Level, // Adding Level no
								kTrue,
								0, 
								kFalse,
								-1,-1,-1,-1,-1,-1,index,-1,1
							);

							CatagoryListData.setAll(temp2,it->getElementId(),1,1,0,1);
							CatagoryListData.setHitCount(1);
							CatagoryDataNodeList.push_back(CatagoryListData);

							i++;
							temp2.Clear();
						}
					}
				}
			}

			temp1.Clear();
		}
///	END on 61108
		/*AP*/
//		if(ptrIAppFramework->CONFIGCACHE_getIndesignSbShowPcAttribute()) // add these attributes Only when INDESIGN_SB_SHOW_PC_ATTRIBUTE CONFIG IS 1. 
//		{																// No need to show Product Catalog related attributes everytime, use above config for it					
//
//				CatagoryData.setData
//				(5,
//				i,
//				-2001,
//				-1,
//				1, 
//				"Job Name",
//				kTrue,
//				"-1",
//				kFalse,
//				0
//				);
//
//				CatagoryListData.setAll("Job Name",-2001,1,1,0,0);
//				CatagoryDataNodeList.push_back(CatagoryListData);
//
//				TPLMediatorClass::count5=i;
//				i++;
/////////////////////////////////////////////////////////15-jan//////////////
//
////////////////////////////////////////////////////////////15-jan////////////
//				CatagoryData.setData
//				(5,
//				i,
//				-2002,
//				-1,
//				1, 
//				"Job Expiry Date",
//				kTrue,
//				"-1",
//				kFalse,
//				0
//				);
//
//				CatagoryListData.setAll("Job Expiry Date",-2002,1,1,0,0);
//				CatagoryDataNodeList.push_back(CatagoryListData);
//
//				TPLMediatorClass::count5=i;
//				i++;
/////////////////////////////////////////////////////////////15-jan///////////
//
////////////////////////////////////////////////////////////15-jan////////////
//				CatagoryData.setData
//				(5,
//				i,
//				-2003,
//				-1,
//				1, 
//				"Job Index",
//				kTrue,
//				"-1",
//				kFalse,
//				0
//				);
//
//				CatagoryListData.setAll("Job Index",-2003,1,1,0,0);
//				CatagoryDataNodeList.push_back(CatagoryListData);
//
//				TPLMediatorClass::count5=i;
//				i++;
//
//		}

				////	For Hybrid Table
//	bool16 result1 = ptrIAppFramework->ConfigCache_getDisplayItemHybridTable();
				//bool16 result1 = kTrue;
				//if(result1 == kTrue)
				//	{
				//		CatagoryData.setData
				//				(5,
				//				i,
				//				-115,
				//				-1,
				//				TPLMediatorClass::CurrLanguageID, 
				//				"Hybrid Table",
				//				kTrue,
				//				"",
				//				kFalse,
				//				1 // For Tables only
				//				);
				//			
				//		CatagoryListData.setAll("Hybrid Table",-115,1,1,0,2);
				//		
				//		CatagoryListData.setHitCount(2);				
				//		CatagoryDataNodeList.push_back(CatagoryListData);					
				//		i++;
				//		//count3=i;	i++;
				//	}
		/*AP*/
		//VectorTypeInfoPtr typeValObj = ptrIAppFramework->AttributeCache_getHybridTableTypesForClassAndParents(CurrentClassID);
		//if(typeValObj!=nil)
		//{
		//	VectorTypeInfoValue::iterator it1;					
		//	PMString Name("");
		//	for(it1=typeValObj->begin();it1!=typeValObj->end();it1++)
		//	{
		//		Name = it1->getName();
		//		//Name.Append("_Hybrid");
		//		//CA(it1->getName());
		//		CatagoryData.setData
		//			(5,
		//			i,
		//			-115,
		//			it1->/*getId()*/getType_id(),
		//			TPLMediatorClass::CurrLanguageID, 
		//			Name,
		//			kTrue,
		//			it1->getCode(),
		//			kFalse,
		//			1,
		//			kFalse,//For Event Field
		//			-1,//For Header
		//			-1,//for childTag
		//			3//for TableType
		//			);
		//		
		//		PMString temp(Name);
		//		temp.Append(" (Table)"/*" : Table"*/);
		//		CatagoryListData.setAll(temp,it1->/*getId()*/getType_id(),1,1,0,2);
		//		CatagoryListData.setHitCount(2);				
		//		CatagoryDataNodeList.push_back(CatagoryListData);					
		//	//	listHelper.AddElement(lstboxControlView, it1->getName(), kItemPanelTextWidgetID, i);
		//	//	listHelper.AddIcons(lstboxControlView, i, 1); 
		//		//count3=i;
		//		i++;
		//	}
		//}

		//if(typeValObj)		
		//	delete typeValObj;
//////////	End
		if(eleValObj)
			delete eleValObj;

	}while(kFalse);
}

////////////////////////////////////////////////////////////////////////////////////////UPTO HERE
//18-may chetan
PMString TPLSelectionObserver::GetSelectedLocaleName(IControlView* LocaleControlView, int32& selectedRow)
{	
	//CA("PubBrowseDlgObserver::GetSelectedLocaleName");
	PMString locName("");
	do{
		InterfacePtr<IStringListControlData> LocaleDropListData(LocaleControlView,UseDefaultIID());	
		if(LocaleDropListData == nil){
			//CAlert::InformationAlert("LocaleDropListData nil");
			break;
		}
		InterfacePtr<IDropDownListController> LocaleDropListController(LocaleDropListData, UseDefaultIID());
		if(LocaleDropListController == nil){
			//CAlert::InformationAlert("LocaleDropListController nil");
			break;
		}
		selectedRow = LocaleDropListController->GetSelected();
		if(selectedRow <0){
			//CAlert::InformationAlert("invalid row");
			break;
		}
		locName = LocaleDropListData->GetString(selectedRow);
//
		PMString sls;
		sls.AppendNumber (selectedRow );
		//CA("Selected row no ===== "+sls+" Name = "+locName );
//
		//if(selectedRow>=0){
		//	InterfacePtr<IPanelControlData> panelControlData(this,UseDefaultIID());
		//	if (panelControlData == nil){
		//		//CAlert::InformationAlert("panelControlData nil");
		//		break;
		//	}
		//	IControlView* controlView = panelControlData->FindWidget(kPubDropDownWidgetID);
		//	if (controlView == nil){
		//		//CAlert::InformationAlert("Publication Browse Button ControlView nil");
		//		break;
		//	}
		//	IControlView* controlView1 = panelControlData->FindWidget(kOKButtonWidgetID);
		//	if (controlView1 == nil){
		//		//CAlert::InformationAlert("Ok Button ControlView nil");
		//		break;
		//	}
		//	if(!controlView->GetEnableState()){
		//		controlView->Enable(kTrue);
		//		//controlView1->Enable(kTrue);
		//	}
		//}
	}while(0);	
	return locName;
} 


//18-may
//
