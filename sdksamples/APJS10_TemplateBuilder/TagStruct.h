#ifndef __TAGSTRUCT_H__
#define __TAGSTRUCT_H__	

#include "VCPluginHeaders.h"
#include "vector"
#include "IIDXMLElement.h"
#include "PMString.h"

#define NUM_TAGS_FIELDS 29

using namespace std;

class TagStruct
{
public:
	double elementId; // [Element ID] OR Attribute ID
	double typeId;	// Type ID of Element or Attribute field
	int32 header;		// if 1 indicates its a Header (Display Name ) field
	bool16 isEventField; //for Event level attribute if true its 
	int32 deleteIfEmpty;// -1 = not applied, 1= delete If Empty true (Delete empty tags after spray)
	int32 dataType;		// Type of Attribute -1= Normal Attributes, 1 = Possible Value, 2= Image Description , 3 = Image Name ,4 = Table name , 5 = Table Comments, 6= AttributeGroup
	int32 isAutoResize;
	double languageID;
	int16 whichTab; // current lstbox [index]	
	double pbObjectId;  // Pb object id for publication items and product in case of ONEsource its -1
	double parentId; // [parentID] Documnet tag's parent (in case of Table Product Id or Master Item Id)
	double childId;	// in case of Table Child Item's ID
	double sectionID; // storing subsection/ Section ID	
	double parentTypeID; // [parentTypeID]	
	int32 isSprayItemPerFrame; // -1 = not applied, 1= isSprayItemPerFrame with Horizontal Flow, 2 = isSprayItemPerFrame with Verticle flow	, 3 = Output as Swatch for PV images
	int32 catLevel;		// Category Level in case of Category Attributes otherwise -1
	
	int32 imgFlag; // image flag [imgFlag]  
	int32 imageIndex;	// image index for PV, Brand, Manufacturer & Supplier images.
	int32 flowDir;		// 0 = Horizontal Flow, 1= Verticle Flow, -1 = No flow is define its in case of all Images or PV images
	
	int32 childTag;		// if 1 indicates Child item tag 
	bool16 isTablePresent; // [tableflag]
	int32 tableType;	// displays different types of tables 1 = DB Table, 2 = Custom Table, 3 = Advance Table, 4 = Component , 5 = Accessaries, 6 XRef, 7 = All Standard Table, 8 = Table inside Table Cell, 9 = Attribute Group Table
	double tableId;		// Table ID after spray
	double rowno;
	double colno;  // -555 tabbed test table.

	double field1;	//-----It Contain TypeID of Selected List or Table(Only when Item List is Selected) ---
	double field2;
	double field3;
	double field4;
	double field5;	//-Attribute Code_Id

	bool16 isProcessed;
	int32 startIndex;
	int32 endIndex;
	IIDXMLElement *tagPtr;
	int numValidFields;
	
	UIDRef curBoxUIDRef;
	TagStruct();
    PMString groupKey;
};

typedef vector<TagStruct> TagList;

inline TagStruct::TagStruct()
{
	elementId=-1;
	typeId=-1;
	parentId=-1;
	childId = -1;
	whichTab=-1;
	imgFlag=-1;
	sectionID=-1;
	isProcessed=kFalse;
	parentTypeID=-1;
	startIndex=-1;
	endIndex=-1;
	tagPtr=NULL;
	numValidFields=0;
	rowno= -1;
	colno = -1;
	isAutoResize = -1;
	languageID = -1;
	isEventField = kFalse;

	pbObjectId = -1;
	header = -1;
	childTag = -1;
	tableType = -1;
	tableId = -1;
	flowDir = -1;
	isSprayItemPerFrame = -1;
	catLevel = -1;
	imageIndex = -1;
	deleteIfEmpty = -1;
	dataType = -1;

	field1 = -1;
	field2 = -1;
	field3 = -1;
	field4 = -1;
	field5 = -1;
    
    groupKey= "";

}
#endif
