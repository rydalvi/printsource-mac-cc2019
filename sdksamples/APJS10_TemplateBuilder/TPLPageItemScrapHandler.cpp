#include "VCPlugInHeaders.h"
#include "IDataExchangeHandler.h"
#include "ICommand.h"
#include "IDocument.h"
#include "INewPageItemCmdData.h"
#include "IPMDataObject.h"
#include "IPageItemScrapData.h"
#include "IPageItemLayerData.h"
#include "PMPoint.h"
#include "SDKUtilities.h"
#include "UIDList.h"
#include "CActionComponent.h"
#include "CAlert.h"
#include "CmdUtils.h"
#include "PageItemScrapID.h"
#include "TPLID.h"

class TPLPageItemScrapHandler : public IDataExchangeHandler
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		TPLPageItemScrapHandler(IPMUnknown* boss);
		/**
			dtor.
		*/
		virtual	~TPLPageItemScrapHandler();
		
		/**
			creates storage for the scrap.
		*/
		virtual void CreateScrapStorage();

		/**
			indicates the type of object this data exchange handler can deal with.
			@return the flavor supported by the handler
		*/
		virtual	PMFlavor GetFlavor() const;
		/**
			clears the scrap.
		*/
		virtual void				Clear();
		/**
			reports whether the scrap is empty or not.
			@return kTrue if the scrap is empty, kFalse otherwise.
		*/
		virtual bool16			IsEmpty() const;
				
		/** 
			reports whether the data exchange handler can internalise the data passed to it.
			@param whichItem IN the data needing internalized
			@return canDo if the flavor exists in the data object.
		*/
		virtual DataExchangeResponse CanInternalizeData(IPMDataObject* whichItem) const;
		/** 
			reports whether the handler can convert from a specific flavor. We only support our own flavor.
			@param fromWhichFlavor IN the flavor we wish to convert from
			@return ktrue if the flavor can be converted, kFalse otherwise.
		*/
		virtual bool16	 		CanConvertExternalFlavor(ExternalPMFlavor fromWhichFlavor) const;

		/** 
			reports whether the handler can externalise the object. We do not support this.
			@param whichItem IN the object requiring externaliation
			@return kFailure to indicate we do not externalise the type supported by this handler.
		*/
		virtual ErrorCode		ExternalizePromises(IPMDataObject* whichItem);
		/**
			Called to perform the externalisation. As we indicate we do not perform the externalisation (we do not promise anything)
			this method should never be called.
			@param whichItem IN the item to be externalised.
			@param toWhichFlavor IN the flavor the item is to be externalised as.
			@param s IN the stream the object is to be marshalled onto.
			@return kFailure, indicating we did not externalise anything.
		*/
		virtual ErrorCode		Externalize(IPMDataObject* whichItem, ExternalPMFlavor toWhichFlavor, IPMStream* s);
		/**
			Called to internalise the object.
			@param whichItem IN the item to be internalised.
			@return kFailure to indicate we did not internalise the object.
		*/
		virtual ErrorCode		Internalize(IPMDataObject* whichItem);
		/**
			Called to internalise the object.
			@param whichItem IN the item to be internalised.
			@param fromWhichFlavor IN the flavor we want to convert from.
			@param s IN the stream we can de-marshal the object from.
			@return kFailure to indicate we did not internalise the object.
		*/
		virtual ErrorCode		Internalize(IPMDataObject* whichItem, ExternalPMFlavor fromWhichFlavor, IPMStream* s);
		virtual void				AddExternalizeableFlavorsToList(OrderedFlavorList& flavorList) const{}
		virtual void				AddInternalizeableFlavorsToList(OrderedFlavorList& flavorList) const {}
	private:
		DECLARE_HELPER_METHODS()
};

CREATE_PMINTERFACE(TPLPageItemScrapHandler, kTPLPageItemScrapHandlerImpl)

DEFINE_HELPER_METHODS(TPLPageItemScrapHandler)

// Constructor.
TPLPageItemScrapHandler::TPLPageItemScrapHandler(IPMUnknown *boss) :
HELPER_METHODS_INIT(boss)
{
}

// dtor.
TPLPageItemScrapHandler::~TPLPageItemScrapHandler()
{
}

// creates storage for the scrap.
void 
TPLPageItemScrapHandler::CreateScrapStorage()
{
	// Get the IDocument, which is located on the scrap databases root boss
	IDataBase *db = ::GetDataBase(this);
	InterfacePtr<IDocument> doc(db, db->GetRootUID(), IID_IDOCUMENT);

	// Create a new page item, which will be the root node for the scrap.
	// Page items that get copied into the scrap will be children of this node.
	InterfacePtr<ICommand> createRootPageItem(CmdUtils::CreateCommand(kNewPageItemCmdBoss));
	if(createRootPageItem!=nil)
	{
		InterfacePtr<INewPageItemCmdData> cmdData(createRootPageItem, UseDefaultIID());
		PMPointList ptList(4, PMPoint());
		//-------------Indesign CS3 Change
		//ptList.Append(PMPoint(0, 0));
		ptList.push_back(PMPoint(0, 0));
		//ptList.Append(PMPoint(0, 0));
		ptList.push_back(PMPoint(0, 0));
		cmdData->Set(db, kPageItemScrapRootBoss, INewPageItemCmdData::kNoGraphicAttributes, kInvalidUID, ptList);
		CmdUtils::ProcessCommand(createRootPageItem);
		const UIDList *newItem = createRootPageItem->GetItemList();
		InterfacePtr<IPageItemScrapData> scrapData(this, UseDefaultIID());
		scrapData->SetRootNode(newItem->GetRef(0));
	}
}

//	indicates the type of object this data exchange handler can deal with.
PMFlavor 
TPLPageItemScrapHandler::GetFlavor() const
{
	return ourFlavor1;
}

//	clears the scrap.
void 
TPLPageItemScrapHandler::Clear()
{
	InterfacePtr<IPageItemScrapData> data(this, UseDefaultIID());
	data->Clear();
	InterfacePtr<IPageItemLayerData> layerData(this, IID_IPAGEITEMLAYERDATA);
	layerData->Clear();
}

//	reports whether the scrap is empty or not.
bool16 
TPLPageItemScrapHandler::IsEmpty() const
{
	InterfacePtr<IPageItemScrapData> data(this, UseDefaultIID());
	return data->IsEmpty();
}

// Can this handler convert clipboard or dragdrop data to it's Internal flavor?
DataExchangeResponse 
TPLPageItemScrapHandler::CanInternalizeData(IPMDataObject* whichItem) const
{
	return (whichItem->FlavorExistsWithPriority(ourFlavor1));
}

// reports whether the handler can convert from a specific flavor. We only support our own flavor.
bool16	 		
TPLPageItemScrapHandler::CanConvertExternalFlavor(ExternalPMFlavor fromWhichFlavor) const
{
	return (fromWhichFlavor == ourFlavor1);
}

// Make Externalization promises (for clipboard and drag/drop).
ErrorCode 
TPLPageItemScrapHandler::ExternalizePromises(IPMDataObject* whichItem)
{
	return kFailure;
}

// Externalize the contents into the given flavor on a stream (for clipboard and drag/drop).
ErrorCode 
TPLPageItemScrapHandler::Externalize(IPMDataObject* whichItem, ExternalPMFlavor toWhichFlavor, IPMStream* s)
{
	return kFailure;
}

// Internalize the data item (for clipboard or drag/drop).
ErrorCode 
TPLPageItemScrapHandler::Internalize(IPMDataObject* whichItem)
{
	return kFailure;
}

// Internalize the contents of a stream as the given flavor (for clipboard or drag/drop).
ErrorCode
TPLPageItemScrapHandler::Internalize(IPMDataObject* whichItem, ExternalPMFlavor fromWhichFlavor, IPMStream* s)
{
	return kFailure;
}





