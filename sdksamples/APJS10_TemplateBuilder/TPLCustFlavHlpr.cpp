#include "VCPlugInHeaders.h"
#include "CDragDropTargetFlavorHelper.h"
#include "IControlView.h"
#include "ILayoutUtils.h"
#include "ILayoutControlData.h"
#include "ICommand.h"
#include "IDataExchangeHandler.h"
#include "IGeometry.h"
#include "IPageItemLayerData.h"
#include "IPageItemScrapUtils.h"
#include "IPasteboardUtils.h"
#include "IDocument.h"
#include "IPageItemScrapData.h"
#include "ISpread.h"
//#include "IBoundsUtils.h"              //It is removed from CS3 API
//#include "IMoveRelativeCmdData.h"
#include "ILayoutCmdData.h"
#include "PersistUtils.h"
#include "ErrorUtils.h"
#include "Utils.h"
#include "DataObjectIterator.h"
#include "CmdUtils.h"
#include "LayerID.h"
#include "CAlert.h"
//#include "ISelection.h"
#include "ILayoutUIUtils.h"
#include "SystemUtils.h"
#include "InterfacePtr.h"
#include "ISpread.h"
#include "ISpreadList.h"
#include "UIDList.h"
#include "K2SmartPtr.h"
#include "SDKListBoxHelper.h"
#include "IControlView.h"
#include "IToolManager.h"
#include "IApplication.h"
#include "TPLListboxData.h"
#include "TPLID.h"
#include "TPLMediatorClass.h"
#include "TPLCommonFunctions.h"
#include "IToolCmdData.h"
#include "ITool.h"
#include "TextEditorID.h"// kIBeamToolBoss
#include "IFrameUtils.h"
//#include "ITextFrame.h"			//It is removed from CS3 API
#include "ITextFocusManager.h"
#include "ICommand.h"
#include "ISelectUtils.h"
#include "ITristateControlData.h"
#include "ISelectionUtils.h"
#include "ITextSelectionSuite.h"
#include "ILayoutSelectionSuite.h"
#include "ITextMiscellanySuite.h"
#include "ITableUtils.h"
#include "ITextEditSuite.h"
#include "ITransformCmdData.h"
//MessageServer
#include "CAlert.h"
//#include "IMessageServer.h"
//-------------CS3 Addition--------------------//
#include "ICopyCmdData.h"
#include "IGeometryFacade.h"
#include "IHierarchy.h"
#include "ITextFrameColumn.h"

#define FILENAME			PMString("TPLCustFlavHlpr.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAlert::InformationAlert(X)//CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

#define DRAGMODE		1
#define CONTENTSMODE	2

extern int32 SelectedRowNo;//Defined in the TPLSelectionObserver.cpp
extern UIDRef newPageItem;
extern UIDRef newPageItem1;

//ISTabbedText is defined in the TPLActionComponent.cpp
extern bool16 ISTabbedText;
extern bool16 isComponentAttr;
extern bool16 isXRefAttr;
extern bool16 isAccessoryAttr;
extern bool16 isMMYAttr;
extern bool16 isMMYSortAttr;
extern bool16 isAddAsDisplayName;
extern bool16 isAddImageDescription;
extern bool16 isOutputAsSwatch;

class TPLCustFlavHlpr : public CDragDropTargetFlavorHelper
{
public:
	TPLCustFlavHlpr(IPMUnknown *boss);
	virtual	~TPLCustFlavHlpr();
	/**
		Determines whether we can handle the flavors in the drag.
		@param target IN the target the mouse is currently over.
		@param dataIter IN iterator providing access to the data objects within the drag.
		@param fromSource IN the source of the drag.
		@param controller IN the drag drop controller mediating the drag.
		@return a target response (either won't accept or drop will copy).
	*/
	virtual DragDrop::TargetResponse	
						CouldAcceptTypes(const IDragDropTarget*, DataObjectIterator*, const IDragDropSource*, const IDragDropController*) const;

	/**
		performs the actual drag. Because we know that our custom flavor is masquerading and is 
		really a page item we must take a copy of the page item, then move this copy to the drop zone.
		We know we have been dropped on a widget (because we are called as part of that widget responding to
		a drop of the custom flavor).
		@param target IN the target for this drop.
		@param controller IN the drag drop controller that is mediating the drag.
		@param action IN what the drop means (i.e. copy, move etc)
		@return kSuccess if the drop is executed without error, kFailure, otherwise.
	*/
	virtual ErrorCode		ProcessDragDropCommand(IDragDropTarget*, IDragDropController*, DragDrop::eCommandType);

	DECLARE_HELPER_METHODS()
};


//--------------------------------------------------------------------------------------
// Class LayoutDDTargetFileFlavorHelper
//--------------------------------------------------------------------------------------
CREATE_PMINTERFACE(TPLCustFlavHlpr, kTPLCustFlavHlprImpl)
DEFINE_HELPER_METHODS(TPLCustFlavHlpr)

TPLCustFlavHlpr::TPLCustFlavHlpr(IPMUnknown *boss) :
	CDragDropTargetFlavorHelper(boss), HELPER_METHODS_INIT(boss)
{
}

TPLCustFlavHlpr::~TPLCustFlavHlpr()
{
}

/* Determine if we can handle the drop type */
DragDrop::TargetResponse 
TPLCustFlavHlpr::CouldAcceptTypes(const IDragDropTarget* target, DataObjectIterator* dataIter, const IDragDropSource* fromSource, const IDragDropController* controller) const
{//CA(__FUNCTION__);
	
	// Check the available external flavors to see if we can handle any of them
	if (dataIter != nil)
	{
		//CAlert::InformationAlert("CouldAcceptTypes");
		// Test for swatches in the drag
		DataExchangeResponse response;

	
//following change in if condition is made by vijay choudhari on 4-6-2006		
		if(SelectedRowNo == 4 ||SelectedRowNo == 3 ||SelectedRowNo == 2 ||SelectedRowNo == 1 ||SelectedRowNo == 0 || TPLMediatorClass::imageFlag==kTrue)
		{   
			
			
			response = dataIter->FlavorExistsWithPriorityInAllObjects(ourFlavor1);
			if (response.CanDo())
			{  				
				return DragDrop::TargetResponse(response, DragDrop::kDropWillCopy);
			}
		}
		/*else
		{
			response = dataIter->FlavorExistsWithPriorityInAllObjects(kPMTextFlavor);
			if (response.CanDo())
				return DragDrop::TargetResponse(response, DragDrop::kDropWillCopy);
		}*/
	}
	//CA("DragDrop::kWontAcceptTargetResponse");
	return DragDrop::kWontAcceptTargetResponse;
}
///////////////////////////////////////////////////////////////////////////////////////
//When we drag mouse on document and release left click ,Program flow comes into following function.
/* process the drop, this method called if CouldAcceptTypes returns valid response */
ErrorCode	
TPLCustFlavHlpr::ProcessDragDropCommand
	(
	IDragDropTarget* 			target, 
	IDragDropController* 		controller,
	DragDrop::eCommandType		action
	)
{	
	//CA(__FUNCTION__);
	//CA("1");	
	TPLMediatorClass::isCaratInsideTable = 0;
	TPLMediatorClass::checkForOverlapWithTextFrame = kFalse;

	ErrorCode stat = kFailure;
	do
	{		
		if (action != DragDrop::kDropCommand) 
		{
			break;
		}
		
		// Find the target view for the drop and the mouse position.
		InterfacePtr<IControlView> layoutView(target, UseDefaultIID());
		ASSERT(layoutView);
		if (!layoutView)
		{
			break;
		}
		
		InterfacePtr<ILayoutControlData> layoutData(target, UseDefaultIID());
		ASSERT(layoutData);
		if (!layoutData) 
		{
			break;
		}
		
//GetDragMouseLocation function returns coordinate of mouse position. It returns coordinates in 
//Global Coordinate System(returns GSysPoint).We want coordinates in terms of Pasteboard 
//coordinate system(PMPoint) so we need to convert it into PMPoint.
//To do so we are using GlobalToPasteboard function.-------------added by vijay.

		GSysPoint where = controller->GetDragMouseLocation();
		PMPoint currentPoint = Utils<ILayoutUIUtils>()->GlobalToPasteboard(layoutView, where);
	
//following code is added by vijay choudhari on 31-5-2006///////////////
//////////////////////////////////////////////////////////FROM HERE TO		
		//UIDRef ref1;
		//InterfacePtr<ISelectionManager>	iSelectionManager1 (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		//if(!iSelectionManager1)
		//{	CA("Selection Manager is Null ");
		//		return 1;
		//}	

		//InterfacePtr<ITextMiscellanySuite>txtSelectionSuite1(iSelectionManager1,UseDefaultIID());
		//if(txtSelectionSuite1)
		//{
		//	//CA("Text Selection Suite is not null");
		//	txtSelectionSuite1->GetFrameUIDRef(ref1);
		//}
		//
		
		
		TPLCommonFunctions comFunctions1;
		comFunctions1.checkForOverlapWithTextFrame( currentPoint);
////////////////////////////////////////////////////////UPTO HERE


// Determine the target spread to drop the items into.
//Now we need Spread which is nearest to the currentPoint (i.e. Mouse position after 
//drag and drop).For that we are using QueryNearestSpread function.After that we are
//storing UIDRef of that spread into targetSpreadUIDRef by calling global function
//GetUIDRef.-------------added by vijay.
		
		InterfacePtr<ISpread> targetSpread(Utils<IPasteboardUtils>()->QueryNearestSpread(layoutView, currentPoint));
		ASSERT(targetSpread);
		if (!targetSpread) 
		{
			break;
		}
		UIDRef targetSpreadUIDRef = ::GetUIDRef(targetSpread);

//If selected row of Drop-Down box is item,product or project respectively or 
//TPLMediatorClass::imageFlag==kTrue, Then program flow will enter into following 
//if block.

		if(SelectedRowNo == 4 ||SelectedRowNo == 3 ||SelectedRowNo == 2 ||SelectedRowNo == 1 ||SelectedRowNo == 0 || TPLMediatorClass::imageFlag==kTrue)
		{		
// Change spread if necessary.
//GetSpread() function is used to get current spread, the spread this view is 
//currently viewing. If targetSpreadUIDRef != UIDRef return by GetUIDRef function
//then program flow will enter into following if block.
				
			const UIDRef currentSpreadUIDRef = layoutData->GetSpreadRef();

			if (targetSpreadUIDRef != currentSpreadUIDRef)
			{				
				InterfacePtr<ICommand> setSpreadCmd(CmdUtils::CreateCommand(kSetSpreadCmdBoss));
				//ASSERT(setSpreadCmd);
				if (!setSpreadCmd)
				{
					break;
				}
		
				InterfacePtr<ILayoutCmdData> setSpreadLayoutCmdData(setSpreadCmd, UseDefaultIID());
				//ASSERT(setSpreadLayoutCmdData);
				if (!setSpreadLayoutCmdData) 
				{
					break;
				}
			
				setSpreadLayoutCmdData->Set(::GetUIDRef(layoutData->GetDocument()), layoutData);
				setSpreadCmd->SetItemList(UIDList(targetSpreadUIDRef));
				stat = CmdUtils::ProcessCommand(setSpreadCmd);
				
				//ASSERT_MSG(stat == kSuccess, "Failed to change spread");
			}
// Get a list of the page items in the drag.
			stat = controller->InternalizeDrag(kNoExternalFlavor, ourFlavor1);
			ASSERT_MSG(stat == kSuccess, "Cannot internalize the dragged item?\n");
			if (stat != kSuccess) 
			{
				break;
			}
		
			InterfacePtr<IDataExchangeHandler> handler(controller->QueryTargetHandler());
			ASSERT(handler);
			if (!handler) 
			{
				break; 
			}
			InterfacePtr<IPageItemScrapData> pageItemData(handler, UseDefaultIID());
			ASSERT(pageItemData);
			if (!pageItemData) 
			{
				break;
			}
			InterfacePtr<IPageItemLayerData> layerData(handler, IID_IPAGEITEMLAYERDATA);
			ASSERT(layerData);
			if (!layerData)
			{
				break;
			}
		
			UIDList* draggedItemList = pageItemData->CreateUIDList();		
		
// Duplicate the drag content in the target spread.
// Parent the duplicated page item in the active spread layer.
			UIDRef parent(targetSpreadUIDRef.GetDataBase(), layoutData->GetActiveLayerUID());

// See IPageItemLayerData if you want to maintain layer information across the drop.
			K2Vector<PMString>* layerNameList = nil;
			K2Vector<int32>* layerIndexList = nil;

//------------------------------CS3 Change-------------------------------------------------//
			/*InterfacePtr<ICommand>	duplicateCmd(Utils<IPageItemScrapUtils>()->CreateDuplicateCommand(draggedItemList, layerNameList, layerIndexList, kFalse, parent));	
			ASSERT(duplicateCmd);
			if (!duplicateCmd)
			{
				delete draggedItemList;
				break;
			}*/
			
			//ICommand* duplicateCmd;
			InterfacePtr<ICommand>	duplicateCmd (CmdUtils::CreateCommand( kDuplicateCmdBoss));
			InterfacePtr<ICopyCmdData> data( duplicateCmd, IID_ICOPYCMDDATA);
			data->Set( duplicateCmd, draggedItemList, parent);
			data->SetOffset( PMPoint(0,0));

			stat = CmdUtils::ProcessCommand(duplicateCmd);
			const UIDList* copiedItemList = duplicateCmd->GetItemList();
			ASSERT_MSG(stat == kSuccess && copiedItemList != nil, "Failed to duplicate dropped item");
			if (stat != kSuccess)
			{
				break;
			}
//---------------------CS3 Change-----------------------------------------------------------------------//

// Position the dropped items on the spread at the mouse position.
// Calculate the bounds of the items so we can center them on the cursor.
// You might want to extend the code below to constrain the dropped items
// position to intersect the bounds of the spread.
	
//---------------------------------CS3 Change-----------------------------------------------------//

			//	PMRect itemsBoundingBox = Utils<IBoundsUtils>()->GetStrokeBounds(*copiedItemList);

			const UIDRef itemUIDRef = copiedItemList->GetRef(0);
			PMRect itemsBoundingBox = Utils<Facade::IGeometryFacade>()->GetItemBounds(itemUIDRef,Transform::PasteboardCoordinates(), Geometry::OuterStrokeBounds() /*Geometry::PathBounds()*/);
			
			PMPoint itemsBoundsCenter = itemsBoundingBox.GetCenter();
			InterfacePtr<ICommand> moveRelativeCmd(CmdUtils::CreateCommand(kTransformPageItemsCmdBoss));
			ASSERT(moveRelativeCmd);
			if (!moveRelativeCmd) 
			{
				break;
			}
		
			/*InterfacePtr<IMoveRelativeCmdData> moveRelativeCmdData(moveRelativeCmd, UseDefaultIID());
			ASSERT(moveRelativeCmdData);
			if (!moveRelativeCmdData) 
			{
				break;
			}
			moveRelativeCmdData->Set(currentPoint - itemsBoundsCenter);*/

			PMPoint delta(currentPoint - itemsBoundsCenter);
			InterfacePtr<ITransformCmdData> moveRelativeCmdData (moveRelativeCmd, UseDefaultIID()); 
			if(moveRelativeCmdData == NULL)
			{
				//CA("moveRelativeCmdData is NULL");
				break;
			}
			moveRelativeCmdData->SetTransformData( Transform::PasteboardCoordinates(), Transform::CurrentOrigin()/*PMPoint(0,0)*/, Transform::TranslateBy(delta.X(), delta.Y())); 
			
			moveRelativeCmd->SetItemList(UIDList(*copiedItemList));
			stat = CmdUtils::ProcessCommand(moveRelativeCmd);
			ASSERT_MSG(stat == kSuccess, "Failed to position dropped items");
			if (stat != kSuccess) 
			{
				break;
			}
				
			
	/*		InterfacePtr<ISelection> isel(::QuerySelection());
			isel->Select(*(moveRelativeCmd->GetItemList()),ISelection::kReplace, kTrue);
	*/		
			InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
			InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
			if (!layoutSelectionSuite) 
			{
				break;
			}
			if (iSelectionManager->SelectionExists (kInvalidClass/*any CSB*/, ISelectionManager::kAnySelection)) 
			{
				// Clear the selection
				iSelectionManager->DeselectAll(nil);
			}
			//---------------CS3 Change-----------------------------------//	
		//	layoutSelectionSuite->Select(*(moveRelativeCmd->GetItemList()),Selection::kReplace,  Selection::kDontScrollLayoutSelection );
			layoutSelectionSuite->SelectPageItems(*(moveRelativeCmd->GetItemList()),Selection::kReplace,  Selection::kDontScrollLayoutSelection );
		
		}
		//else
		//{	CA("11");
		//	stat = controller->InternalizeDrag(kNoExternalFlavor, kPMTextFlavor);
		//	ASSERT_MSG(stat == kSuccess, "Cannot internalize the dragged item?\n");
		//	if (stat != kSuccess) { CA("stat != kSuccess");
		//		//break;
		//	}

		//}
	
			stat = kSuccess;
/*		InterfacePtr<ITextSelectionSuite> textSelectionSuite(iSelectionManager,UseDefaultIID());
		if(!textSelectionSuite)
		{
			//CA("Nil");
		}
*/		
//		textSelectionSuite->SetTextSelection(textModelRef,RangeData(0, RangeData::kLeanForward),Selection::kScrollIntoView, nil); 
			TPLCommonFunctions comFunctions;		
//		comFunctions.changeMode(DRAGMODE);
		//added by Premal///////////////////////
//		layoutSelectionSuite->Select(*(moveRelativeCmd->GetItemList()),Selection::kReplace,  Selection::kDontScrollSelection);
		////end of adding by Premal////////////////////////////////////////
	
		TPLListboxData lstboxData;
		/* Marking the dragged flag to true */

		lstboxData.setIsBoxDragged
			(TPLMediatorClass::curSelLstbox, 
			TPLMediatorClass::curSelRowIndex, kTrue);

/* Changing the radio btn selection if Image box */
		if(TPLMediatorClass::imageFlag==kTrue)
		{
			TPLMediatorClass::curSelRadBtn=2;
/* putting selection in 2nd radio explicitely */
			IControlView * iAppendCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kAppendRadBtnWidgetID);
			if(iAppendCntrlView==nil) 
				break;

			InterfacePtr<ITriStateControlData> iAppendtristatecontroldata(iAppendCntrlView, UseDefaultIID());
			if(iAppendtristatecontroldata==nil) 
				break;

			iAppendtristatecontroldata->SetState(ITriStateControlData::kUnselected,kTrue,kFalse);

			IControlView * iEmbedCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kEmbedRadBtnWidgetID);
			if(iEmbedCntrlView==nil) 
				break;

			InterfacePtr<ITriStateControlData> iEmbedtristatecontroldata(iEmbedCntrlView, UseDefaultIID());
			if(iEmbedtristatecontroldata==nil) 
				break;

			iEmbedtristatecontroldata->SetState(ITriStateControlData::kSelected,kTrue,kFalse);
			IControlView* iChkboxCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTagFrameWidgetID);
			if(iChkboxCntrlView==nil) 
				break;

			iChkboxCntrlView->HideView();                          // changed by mahesh it was disabled before.
		}
		
		
		
///////////////////////////////////////////////////////////
////////////////////// For Droping Table //////////////////
///////////////////////////////////////////////////////////
////Following code is added by vijay choudhari on 8-5-2006
////To provide Drag-Drop facility for table on Document.
///////////////////////////////////////////////////////////FROM HERE TO

//We want UIDRef of frame(frame which is there on page).for that we need to call
//method GetFrameUIDRef().For that we need Interfaceptr of ITextMiscellanySuite,
//We get it by using QuerySuite and passing it Interfaceptr of ISelectionManager.

//CA("Dropping table");
			InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
			if(!iSelectionManager)
			{	
				//CA("!iSelectionManager");
				return kFalse;
			}
			
			UIDRef ref;

			InterfacePtr<ITextMiscellanySuite> txtSelectionSuite(static_cast<ITextMiscellanySuite* >
			( Utils<ISelectionUtils>()->QuerySuite(/*ITextMiscellanySuite::*/IID_ITPLTEXTMISCELLANYSUITEE,iSelectionManager)));
		
		
			if(!txtSelectionSuite)
			{
				//CA("Please Select a Type Tool !txtSelectionSuite");
			}
			else
			{			
				TPLCommonFunctions tpl;	
//We are passing UIDRef ref as an out parameter to	GetFrameUIDRef() method.
//following method will return kTrue or kFalse depending upon whether that
//frame is TextFrame or not.
				
				bool16 ISTextFrame = txtSelectionSuite->GetFrameUIDRef(ref);					
				if(	ISTextFrame)
				{	
//CA("ISTextFrame");					

//Following function i.e.GetCaretPosition(start) gives current cursor position
//inside the frame. We are passing start as an out parameter to this method.
//Before that we are initializing it to '0'.
					int32 start=0;
					txtSelectionSuite->GetCaretPosition(start);

	//------------------------CS3 TextFrame Change---------------------------------------------------//

					//InterfacePtr<ITextFrame>txtFrame(ref,UseDefaultIID());
					//if(!txtFrame)
					//{
					//	//CA("Text Frame is null");				
					//	return kFalse;
					//}

					//InterfacePtr<ITextModel>txtModel(txtFrame->QueryTextModel(),UseDefaultIID());
					//if(!txtModel)
					//{
					//	//CA("Text Model is null");				
					//	return kFalse;
					//}

					
					InterfacePtr<IHierarchy> graphicFrameHierarchy(ref, UseDefaultIID());
					if (graphicFrameHierarchy == nil) 
					{
						//CA("graphicFrameHierarchy is NULL");
						return kFalse;
					}
									
					InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
					if (!multiColumnItemHierarchy) {
						//CA("multiColumnItemHierarchy is NULL");
						return kFalse;
					}

					InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
					if (!multiColumnItemTextFrame) {
						//CA("multiColumnItemTextFrame is NULL");
						return kFalse;
					}
					InterfacePtr<IHierarchy>
					frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
					if (!frameItemHierarchy) {
						//CA("frameItemHierarchy is NULL");
						return kFalse;
					}

					InterfacePtr<ITextFrameColumn>
					frameItemTFC(frameItemHierarchy, UseDefaultIID());
					if (!frameItemTFC) {
						//CA("!!!ITextFrameColumn");
						return kFalse;
					}

					InterfacePtr<ITextModel> txtModel(frameItemTFC->QueryTextModel());
					if(!txtModel)
					{
						//CA("!txtModel" );
						return kFalse;
					}

//----------------------------CS3 Change----------------------------------------------------------------//
					UIDRef ref1=::GetUIDRef(frameItemTFC);

//Now we want to check wheter cursor is inside table ,for that we are calling
//InsideTable() method.If it returns kTrue then program flow will enter into
//if block
					//bool16 isInsideTable = kFalse;

					TPLMediatorClass::isInsideTable=Utils<ITableUtils>()->InsideTable(txtModel, start);
					
					
//following code is added by vijay choudhari 					
					UIDList newList;
					UIDRef overlapBoxUIDRef;
					if(TPLMediatorClass::isInsideTable)
					{  	
						//CA("TPLMediatorClass::isInsideTable");
						TPLMediatorClass::isCaratInsideTable = 1;
					
						tpl.getSelectionFromLayout(newPageItem1);
						///if(tpl.checkForOverLaps(newPageItem1, 0, overlapBoxUIDRef, newList))  // this is old changed by Amit on 29/10/07
						if(tpl.checkForOverLaps(newPageItem1, 0, overlapBoxUIDRef, newList) && !(TPLMediatorClass::tableFlag == 1 && ISTabbedText == kFalse))
						{	
										
	//Presently we want to provide this Drag-Drop facility to only item ,For that reason 
	//we have included following if block                         
							//added by Yogesh to add the product_copy attributes inside 
							//the table
							
							
							if(SelectedRowNo !=4 )
							{
								TPLListboxData lstboxData;
								TPLListboxInfo lstboxInfo;
								lstboxInfo=lstboxData.getData(SelectedRowNo, TPLMediatorClass::curSelRowIndex);
						 		
								if(/*lstboxInfo.tableFlag == 1 || */lstboxInfo.isImageFlag == kTrue)
								{//if condition is modified on 11July.
								 //Make the items stencil droppable in table cell.
									//CA_NUM("SelectedRowNo ",SelectedRowNo);
									//CA_NUM("curSelectedRowIndex ",TPLMediatorClass::curSelRowIndex);
								tpl.getSelectionFromLayout(newPageItem1);						
								tpl.deleteThisBox(newPageItem1);	
								return kFalse;
							}
								
							}
	
							tpl.getSelectionFromLayout(newPageItem1);						
							tpl.deleteThisBox(newPageItem1);	

	//========================================================================
	//To get Tag name
							
							TPLListboxData lstboxData;
							TPLListboxInfo lstboxInfo;

//following code is added by vijay on 6-10-2006
							if(SelectedRowNo == 0)
							{
								lstboxInfo=lstboxData.getData(5, TPLMediatorClass::curSelRowIndex);
							}
							else
							{
								lstboxInfo=lstboxData.getData(SelectedRowNo, TPLMediatorClass::curSelRowIndex);
							}
							
							if(lstboxInfo.isImageFlag == kTrue)
							{		
								return kFalse;
							}
							
							PMString result = lstboxInfo.name; //tpl.prepareTagName(lstboxInfo.name);
							if(isComponentAttr && ( SelectedRowNo == 4) )
								result.Append("_Component");
							else if(isXRefAttr && (SelectedRowNo == 4))
								result.Append("_Xref") ; /// For XRef Table Attribute
							else if(isAccessoryAttr && (SelectedRowNo == 4))
								result.Append("_Accessory") ; /// For Accessory Table Attribute	
							else if(isMMYAttr && (SelectedRowNo == 4))
								result.Append("_MMY") ; /// For MMY Table Attribute	



	//=========================================================================	

							TextIndex TablStartIndex = Utils<ITableUtils>()->TableToPrimaryTextIndex(txtModel,start);
							UIDRef tableRef=( Utils<ITableUtils>()->GetTableModel(txtModel, TablStartIndex));
							InterfacePtr<ITextEditSuite> textEditSuite(iSelectionManager, UseDefaultIID());



							if (textEditSuite && textEditSuite->CanEditText())
							{
	//Following function (i.e.InsertText())is responsible for inserting text at current 
	//cursor position
                                result.ParseForEmbeddedCharacters();
								ErrorCode status = textEditSuite->InsertText(WideString(result));
								
							}
//							CA("Index "+lstboxInfo.index  );

	//prepareTagName() function	replaces blank spaces by '_'.					
							PMString CellTagName = tpl.prepareTagName(/*lstboxInfo.name*/result);
							XMLReference xmlRef;
	//TagTable() function is used to attach XML attributes.	
//////////////////////////////////////////////////////////////////////////////
							lstboxInfo.index = SelectedRowNo ;
//////////////////////////////////////////////////////////////////
							PMString temp;
							temp.AppendNumber(lstboxInfo.index);
							//CA("lstboxInfo.index in custflavhlper "+temp );
							tpl.TagTable(tableRef, ref1, "PSTable", CellTagName, xmlRef, lstboxInfo, start);						
	
							return kFalse;
						}			

						//following else if is added by Tushar for table inside another table cell
						else if(TPLMediatorClass::tableFlag==1 && ISTabbedText == kFalse)
						{
							TPLListboxData lstboxData;
							TPLListboxInfo lstboxInfo;

							//following code is added by Tushar on 4/1/07
							//i.e. if---else series
							/////////////////////////////from here to
							if( SelectedRowNo == 4)
							{
								lstboxInfo=lstboxData.getData(4, TPLMediatorClass::curSelRowIndex);
							}
							else if( SelectedRowNo == 3)
							{
								lstboxInfo=lstboxData.getData(3, TPLMediatorClass::curSelRowIndex);
								
							}
							else if( SelectedRowNo == 2)
							{
								lstboxInfo=lstboxData.getData(2, TPLMediatorClass::curSelRowIndex);
								
							}
							else if( SelectedRowNo == 1)
							{
								lstboxInfo=lstboxData.getData(1, TPLMediatorClass::curSelRowIndex);
								
							}		
							else if( SelectedRowNo == 0)
							{
								lstboxInfo=lstboxData.getData(5, TPLMediatorClass::curSelRowIndex);
							}
/////////////////////////		Commented by Amit on 29/10/07						
							//tpl.getSelectionFromLayout(newPageItem1);						
							//tpl.deleteThisBox(newPageItem1);
							//TPLCommonFunctions tp1;
							//TPLMediatorClass::tableFlag=lstboxInfo.tableFlag;
							//tp1.InsertTableInsideTableCellAndApplyTag(lstboxInfo, ref);
/////////////////////////		End			Amit

//////////////////		Added by Amit on 30/10/07
							if(tpl.checkForOverLaps(newPageItem1, 0, overlapBoxUIDRef, newList))
							{
								tpl.getSelectionFromLayout(newPageItem1);						
								tpl.deleteThisBox(newPageItem1);
								TPLCommonFunctions tp1;
								TPLMediatorClass::tableFlag=lstboxInfo.tableFlag;
								tp1.InsertTableInsideTableCellAndApplyTag(lstboxInfo, ref);
							}
							else
							{
								comFunctions.embedOrAppendTable
								(2,TPLMediatorClass::curSelRowIndex,TPLMediatorClass::curSelLstbox); 
							}
//////////////////		End			Amit
							break;
						}
///////////////////////////////////////////////////////////UPTO HERE...... 
						else
						{
						//	CA("222");
							//tpl.changeMode(DRAGMODE);
							//tpl.getSelectionFromLayout(newPageItem1);
						}
					}
				}
			}
///////////////////////////////////////////////////////////UPTO HERE...... 
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
		
		
		//////////////////// For Droping Table //////////////////
		//if((TPLMediatorClass::curSelLstbox==3 || TPLMediatorClass::curSelLstbox == 4) && TPLMediatorClass::tableFlag==1) //commented by Tushar on 4/1/07
		if((TPLMediatorClass::curSelLstbox==3 || TPLMediatorClass::curSelLstbox == 4 || TPLMediatorClass::curSelLstbox==5 ) && TPLMediatorClass::tableFlag==1 )
		{
			//CA("Going for drop table");	
			// Hardcoding the value for Embedding only. No Appending of Tables	
			if(isAddAsDisplayName == kTrue)
			{
				comFunctions.insertOrAppend
				(TPLMediatorClass::curSelRowIndex,
				TPLMediatorClass::curSelRowString,
				TPLMediatorClass::curSelRadBtn,
				TPLMediatorClass::curSelLstbox);
				//Following code is copied from the else block below as it is.
				//start copied code from else below
				IControlView* iChkboxCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTagFrameWidgetID);
				if(iChkboxCntrlView==nil) 
					break;
				
				if(iChkboxCntrlView->IsEnabled())
				{	
					InterfacePtr<ITriStateControlData> itristatecontroldata(iChkboxCntrlView, UseDefaultIID());
					if(itristatecontroldata==nil) 
						break;
					
					if(itristatecontroldata->IsSelected())
					{
						itristatecontroldata->SetState(ITriStateControlData::kSelected,kTrue,kFalse);
					}
					else
					{
						itristatecontroldata->SetState(ITriStateControlData::kUnselected,kTrue,kFalse);
					}					
				}
				break;
			}
			if(ISTabbedText == kFalse)
			{
				//CA("ISTabbedText == kFalse");
				//CA(TPLMediatorClass::curSelRowString);
				if(TPLMediatorClass::curSelRowString.Compare(kTrue,"All Standard Tables")==0)
				{
					//CA("Calling appendTableNameAndTable");
					comFunctions.appendTableNameAndTable
						(TPLMediatorClass::curSelRowIndex,
						TPLMediatorClass::curSelRowString,
						TPLMediatorClass::curSelLstbox);
				}
				else
				{
					comFunctions.embedOrAppendTable
						(2,TPLMediatorClass::curSelRowIndex,TPLMediatorClass::curSelLstbox); 
					return stat;
					//break;
					
				}
			}
			else
			{
				//CA("ISTabbedText == kTrue");
				comFunctions.insertOrAppend
					(TPLMediatorClass::curSelRowIndex,
					TPLMediatorClass::curSelRowString,
					TPLMediatorClass::curSelRadBtn,
					TPLMediatorClass::curSelLstbox);
					//Following code is copied from the else block below as it is.
					//start copied code from else below
					
				IControlView* iChkboxCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTagFrameWidgetID);
				if(iChkboxCntrlView==nil) 
					break;
				
				if(iChkboxCntrlView->IsEnabled())
				{	
					InterfacePtr<ITriStateControlData> itristatecontroldata(iChkboxCntrlView, UseDefaultIID());
					if(itristatecontroldata==nil) 
						break;
					
					if(itristatecontroldata->IsSelected())
					{
						itristatecontroldata->SetState(ITriStateControlData::kSelected,kTrue,kFalse);
					}
					else
					{
						itristatecontroldata->SetState(ITriStateControlData::kUnselected,kTrue,kFalse);
					}
					
				}
				//end copied code from else below
			}
			
		}

//following code is temporary commented by vijay choudhari on 17-6-2006
		else
		{	
//In drag-drop functionality following function plays important role
//Inside following function we are calling sequence of function to insert text 
//into box and add tag to text ,to check if frames are overlaping.
//CA("calling insertOrAppend");			

			TPLMediatorClass::curSelRowString.SetTranslatable(kFalse);

			comFunctions.insertOrAppend
			(TPLMediatorClass::curSelRowIndex,
			 TPLMediatorClass::curSelRowString,
			 TPLMediatorClass::curSelRadBtn,
			 TPLMediatorClass::curSelLstbox);
			
			IControlView* iChkboxCntrlView=TPLMediatorClass::iPanelCntrlDataPtr->FindWidget(kTagFrameWidgetID);
			if(iChkboxCntrlView==nil) 
				break;
			if(iChkboxCntrlView->IsEnabled())
			{	
				InterfacePtr<ITriStateControlData> itristatecontroldata(iChkboxCntrlView, UseDefaultIID());
				if(itristatecontroldata==nil) 
					break;
				
				if(itristatecontroldata->IsSelected())
				{
					itristatecontroldata->SetState(ITriStateControlData::kSelected,kTrue,kFalse);
				}
				else
				{
					itristatecontroldata->SetState(ITriStateControlData::kUnselected,kTrue,kFalse);
				}
				
			}
		}
		
	}
	while(false);
	
	return stat;
}

