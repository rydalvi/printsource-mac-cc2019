#include "VCPlugInHeaders.h"
#include "IListControlDataOf.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "IWidgetParent.h"
#include "IListBoxAttributes.h"
#include "IControlView.h"
#include "IApplication.h"
#include "PersistUtils.h"
#include "IPalettePanelUtils.h"
#include "CreateObject.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "CAlert.h"
#include "RsrcSpec.h"
#include "TPLID.h"
#include "SDKListBoxHelper.h"
#include "IListBoxController.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("SDKListBoxHelper.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

SDKListBoxHelper::SDKListBoxHelper(IPMUnknown * owner, int32 pluginId) : fOwner(owner), fOwnerPluginID(pluginId)
{
}

SDKListBoxHelper::~SDKListBoxHelper()
{
	fOwner=nil;
	fOwnerPluginID=0;
}
/* 
Here passing IPanelControlData of desired listbox got in ListboxObserver
*/
IControlView* SDKListBoxHelper ::FindCurrentListBox
(IPanelControlData* iPanelControlData, int i)//Give me the iControlView of the listbox. huh big deal!!
{
	if(!verifyState())
		return nil;

	IControlView * listBoxControlView2 = nil;

	do {
		if(iPanelControlData ==nil)
		{
			break;
		}
		if(i==1)
		{
			listBoxControlView2 = 
				iPanelControlData->FindWidget(kTPLPFPanelLstboxWidgetID);
		}
		else if(i==2)
		{
			listBoxControlView2 = 
				iPanelControlData->FindWidget(kTPLPGPanelLstboxWidgetID);
		}
		else if(i==3)
		{
			listBoxControlView2 = 
				iPanelControlData->FindWidget(kTPLPRPanelLstboxWidgetID);
		}
		else if(i==4)
		{
			listBoxControlView2 = 
				iPanelControlData->FindWidget(kTPLItemPanelLstboxWidgetID);
		}
		/*else if(i==5)
		{
			listBoxControlView2 = 
				iPanelControlData->FindWidget(kTPLProjectPanelLstboxWidgetID);
		}*/
		if(listBoxControlView2 == nil) 
		{
			break;
		}
	} while(0);
	return listBoxControlView2;
}

/* 
Here passing IControlView* of desired listbox returned from FindCurrentListBox()
*/
void SDKListBoxHelper::AddElement
(IControlView* lstboxControlView, const PMString & displayName, WidgetID updateWidgetId, int atIndex, int x)
{
	//CAlert::InformationAlert(__FUNCTION__);
	if(!verifyState())
		return;
	do	
	{
/*		IControlView * listBox = this->FindCurrentListBox(panel, x);
		if(listBox == nil) 
		{
			CAlert::InformationAlert("FindCurrentListBox nil");
			break;
		}*/
		InterfacePtr<IListBoxAttributes> listAttr(lstboxControlView, UseDefaultIID());
		if(listAttr == nil) 
			break;	

		RsrcID widgetRsrcID = listAttr->GetItemWidgetRsrcID();
		if (widgetRsrcID == 0)
			break;	

		RsrcSpec elementResSpec(LocaleSetting::GetLocale(), fOwnerPluginID, kViewRsrcType, widgetRsrcID);
		InterfacePtr<IControlView> newElView( (IControlView*) ::CreateObject(::GetDataBase(lstboxControlView), elementResSpec, IID_ICONTROLVIEW));
		ASSERT_MSG(newElView != nil, "SDKListBoxHelper::AddElement() Cannot create element");
		if(newElView == nil) 
			break;
		
		this->addListElementWidget(lstboxControlView, newElView, displayName, updateWidgetId, atIndex, x);
	}
	while (false);
}

/*
void SDKListBoxHelper::RemoveElementAt(int indexRemove, int x)
{
	if(!verifyState())
		return;
	do
	{
		IControlView * listBox = this->FindCurrentListBox(x);
		if(listBox == nil) 
		{
			break;
		}
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::RemoveElementAt() Found listbox but not control data?");
		if(listControlData==nil) 
		{
			break;
		}
		if(indexRemove < 0 || indexRemove >= listControlData->Length()) 
		{
			break;
		}
		listControlData->Remove(indexRemove, x);
		removeCellWidget(listBox, indexRemove);
	}
	while (false);
}

void SDKListBoxHelper::RemoveLastElement(int x)
{
	if(!verifyState())
		return;
	do
	{
		IControlView * listBox = this->FindCurrentListBox(x);
		if(listBox == nil) 
		{
			break;
		}
		
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::RemoveLastElement() Found listbox but not control data?");
		if(listControlData==nil) 
		{
			break;
		}
		int lastIndex = listControlData->Length()-1;
		if(lastIndex > 0) 
		{		
			listControlData->Remove(lastIndex, x);
			removeCellWidget(listBox, lastIndex);
		}
	}
	while (false);
}

int SDKListBoxHelper::GetElementCount(int x) 
{
	int retval=0;
	do {
		IControlView * listBox = this->FindCurrentListBox(x);
		if(listBox == nil) {
			break;
		}

		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::GetElementCount() Found listbox but not control data?");
		if(listControlData==nil) {
			break;
		}
		retval = listControlData->Length();
	} while(0);
	
	return retval;
}

void SDKListBoxHelper::removeCellWidget(IControlView * listBox, int removeIndex) {
	
	do {
		if(listBox==nil) break;

		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		ASSERT_MSG(panelData != nil, "SDKListBoxHelper::removeCellWidget()  Cannot get panelData");
		if(panelData == nil) 
		{
			break;
		}
		
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "SDKListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(cellControlView == nil) 
		{
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil,"SDKListBoxHelper::removeCellWidget() cellPanelData nil"); 
		if(cellPanelData == nil) 
		{
			break;
		}

		if(removeIndex < 0 || removeIndex >= cellPanelData->Length()) 
		{
			break;
		}
		cellPanelData->RemoveWidget(removeIndex);
	} while(0);
}
*/
/* 
Here passing IControlView* of desired listbox returned from FindCurrentListBox()
*/
void SDKListBoxHelper::addListElementWidget
(IControlView* lstboxControlView, InterfacePtr<IControlView> & elView, const PMString & displayName, WidgetID updateWidgetId, int atIndex, int x)
{
//	IControlView * listbox = this->FindCurrentListBox(panel, x);
	if(elView == nil || lstboxControlView == nil ) 
		return;

	do {
		InterfacePtr<IPanelControlData> newElPanelData (elView, UseDefaultIID());
		if (newElPanelData == nil) 
			break;
		IControlView* nameTextView = newElPanelData->FindWidget(updateWidgetId);
		if ( (nameTextView == nil)  ) 
			break;

		InterfacePtr<ITextControlData> newEltext (nameTextView,UseDefaultIID());
		if (newEltext == nil) 
			break;
		newEltext->SetString(displayName, kTrue, kTrue);
		
		InterfacePtr<IPanelControlData> panelData(lstboxControlView,UseDefaultIID());
		ASSERT_MSG(panelData != nil, "SDKListBoxHelper::addListElementWidget() Cannot get panelData");
		if(panelData == nil) 
			break;

		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "SDKListBoxHelper::addListElementWidget() cannot find cellControlView");
		if(cellControlView == nil) 
			break;

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil, "SDKListBoxHelper::addListElementWidget()  cellPanelData nil");
		if(cellPanelData == nil) 
			break;
		cellPanelData->AddWidget(elView);				

		InterfacePtr< IListControlDataOf<IControlView*> > listData(lstboxControlView, UseDefaultIID());		
		if(listData == nil)  
			break;
		listData->Add(elView);	
	} while(0);
}

void SDKListBoxHelper::CheckUncheckRow
(IControlView *listboxCntrlView, int32 index, bool checked)
{
	do
	{
/*		IControlView * listBox = this->FindCurrentListBox(x);
		if(listBox == nil)
		{
			CAlert::InformationAlert("FindCurrentListBox nil");
			break;
		}
*/
		InterfacePtr<IListBoxController> listCntl(listboxCntrlView,IID_ILISTBOXCONTROLLER);		
		if(listCntl == nil) 
			break;
		
		InterfacePtr<IPanelControlData> panelData(listboxCntrlView, UseDefaultIID());
		if (panelData == nil) 
			break;
		
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);		
		if(cellControlView == nil)
			break;

		InterfacePtr<IPanelControlData> cellPanelData(cellControlView, UseDefaultIID());		
		if(cellPanelData == nil)
			break;
		
		IControlView* nameTextView = cellPanelData->GetWidget(index);
		if ( nameTextView == nil ) 
			break;

		InterfacePtr<IPanelControlData> cellPanelDataChild(nameTextView, UseDefaultIID());		
		if(cellPanelDataChild == nil)
			break;

		int toHidden=0;
		int toShow=0;
		
		if(checked)
		{
			toHidden=0;
			toShow=1;
		}
		else
		{
			toHidden=1;
			toShow=0;
		}
		
		IControlView *childHideView=cellPanelDataChild->GetWidget(toHidden);
		if(childHideView==nil)
			break;

		IControlView *childShowView=cellPanelDataChild->GetWidget(toShow);
		if(childShowView==nil)
			break;

		childHideView->Hide();
		childShowView->Show();	
	}while(kFalse);
}

void SDKListBoxHelper::EmptyCurrentListBox
(IPanelControlData* panel, int x)
{
	do {
		IControlView* listBoxControlView = this->FindCurrentListBox(panel, x);
		if(listBoxControlView == nil) 
			break;
		InterfacePtr<IListControlData> listData (listBoxControlView, UseDefaultIID());
		if(listData == nil) 
			break;
		InterfacePtr<IPanelControlData> iPanelControlData(listBoxControlView, UseDefaultIID());
		if(iPanelControlData == nil) 
			break;
		IControlView* panelControlView = iPanelControlData->FindWidget(kCellPanelWidgetID);
		if(panelControlView == nil) 
			break;
		InterfacePtr<IPanelControlData> panelData(panelControlView, UseDefaultIID());
		if(panelData == nil) 
			break;
		listData->Clear(kFalse, kFalse);
		panelData->ReleaseAll();
		listBoxControlView->Invalidate();
	} while(0);
}

void SDKListBoxHelper::AddIcons(IControlView *listboxCntrlView, int32 index, int32 Check)
{
	do
	{
		InterfacePtr<IListBoxController> listCntl1(listboxCntrlView,IID_ILISTBOXCONTROLLER);		
		if(listCntl1 == nil){  
			break;
		}
		
		InterfacePtr<IPanelControlData> panelData1(listboxCntrlView, UseDefaultIID());
		if (panelData1 == nil) {  
			break;
		}

		IControlView* cellControlView1 = panelData1->FindWidget(kCellPanelWidgetID);		
		if(cellControlView1 == nil){ 
			break;
		}

		
		InterfacePtr<IPanelControlData> cellPanelData1(cellControlView1, UseDefaultIID());		
		if(cellPanelData1 == nil){ 
			break;
		}

		IControlView* nameTextView1 = cellPanelData1->GetWidget(index);
		if ( nameTextView1== nil ) { 
			break;
		}
	
		InterfacePtr<IPanelControlData> cellPanelDataChild1(nameTextView1, UseDefaultIID());		
		if(cellPanelDataChild1 == nil){  
			break;
		}
		
		IControlView *childTextView=cellPanelDataChild1->GetWidget(2);
		if(childTextView==nil){ 
			break;
		}	
		IControlView *childImageView=cellPanelDataChild1->GetWidget(3);
		if(childImageView==nil){  
			break;
		}
		IControlView *childTableView=cellPanelDataChild1->GetWidget(4);
		if(childTableView==nil){ 
			break;
		}
		if(Check == 0)				//Text
		{	
			childImageView->Hide();
			childTableView->Hide();
			childTextView->Show();
		}
		else if(Check == 1)			//Image
		{	
			childTextView->Hide();
			childTableView->Hide();
			childImageView->Show();
		}
		else if(Check == 2)			//Table
		{ 
			childTextView->Hide();
			childImageView->Hide();	
			childTableView->Show();
		}
			
	}while(kFalse);


}