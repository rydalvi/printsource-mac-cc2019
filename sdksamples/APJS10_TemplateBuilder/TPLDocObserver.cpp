#include "VCPlugInHeaders.h"
#include "K2Vector.tpp"

// Interface includes
#include "ITextControlData.h"
#include "IDocument.h"
#include "ISubject.h"
//#include "ISelection.h"
#include "IWidgetParent.h"
#include "IPanelControlData.h"
#include "IDropdownListController.h"
#include "IStringListControlData.h"
#include "IHierarchy.h"
#include "ISelectionUtils.h"

// API includes
#include "ILayoutUtils.h" //Cs4
#include "PageItemScrapID.h"
#include "StandoffID.h"
#include "CObserver.h"
#include "CmdUtils.h"

// PstLst includes
#include "TPLID.h"
#include "CAlert.h"
#include "TPLMediatorClass.h"
#include "IControlView.h"
#include "SDKListBoxHelper.h"
#include "TPLListboxData.h"
#include "TPLCommonFunctions.h"

TPLListboxData tempListboxData;

#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("TPLDocObserver.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAlert::InformationAlert(X)//CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
class TPLDocObserver : public CObserver
{
	public:
		TPLDocObserver(IPMUnknown* boss);
		virtual ~TPLDocObserver();
		void AutoAttach();
		void AutoDetach();
		virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy);
	protected:
		void AttachDocument(IDocument*	iDocument);
		void DetachDocument(IDocument*	iDocument);
};

CREATE_PMINTERFACE(TPLDocObserver, kTPLDocObserverImpl)

TPLDocObserver::TPLDocObserver(IPMUnknown* boss) :
	CObserver(boss, IID_IPSTLSTOBSERVER)
{
}

TPLDocObserver::~TPLDocObserver()
{
}

void TPLDocObserver::AutoAttach()
{	
	CObserver::AutoAttach();

	InterfacePtr<IDocument>	iDocument(this, UseDefaultIID());
	if (iDocument != nil)
		AttachDocument(iDocument);
}


void TPLDocObserver::AutoDetach()
{
	CObserver::AutoDetach();

	InterfacePtr<IDocument>	iDocument(this, UseDefaultIID());
	if (iDocument != nil)
		DetachDocument(iDocument);
}

void TPLDocObserver::Update(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy)
{
	
	do
	{
		if ((protocol == IID_IHIERARCHY_DOCUMENT) || (protocol == IID_IHIERARCHY))
		{
			if ((protocol == IID_IHIERARCHY)
			&&  (theChange == kAddToHierarchyCmdBoss || theChange == kRemoveFromHierarchyCmdBoss))
			{
				//	These commands double broadcast on both IID_IHIERARCHY & IID_IHIERARCHY_DOCUMENT
				//	Only react to the document broadcasts
				return;
			}
			
			if ((theChange == kDeletePageItemCmdBoss)
		/*	||  (theChange == kRemoveFromHierarchyCmdBoss)
			||  (theChange == kDeleteStandOffItemCmdBoss)
			||  (theChange == kDeleteInsetPageItemCmdBoss)*/)
			{
				ICommand* 				iCommand = (ICommand*)changedBy;
				const UIDList 			itemList = iCommand->GetItemListReference();
				ICommand::CommandState	cmdState = iCommand->GetCommandState();

				if (itemList.Length() && (cmdState == ICommand::kNotDone ))// || cmdState == ICommand::kNotRedone))
				{
					ICommand*	cmd = (ICommand*)changedBy;
					const UIDList	*selectUIDList =cmd->GetItemList();
					PreDirty();
					TPLCommonFunctions comnFuns;
					//CA("before refreshLstbox");
					comnFuns.refreshLstbox(selectUIDList->GetRef(0).GetUID());
					//CA("After refreshLstbox");
				}
			}
		}
	} while (kFalse);
}

void TPLDocObserver::AttachDocument(IDocument*	iDocument)
{
	do
	{	//CA("AttachDocument");
		if (iDocument == nil)
		{
			ASSERT_FAIL("no document to attach to");
			break;
		}
		
		InterfacePtr<ISubject> 		iDocSubject(iDocument, UseDefaultIID());
		if (iDocSubject == nil)
		{
			ASSERT_FAIL("no document subject to attach the observer");
			break;
		}
		
		//	Protocols used for page item model changes
		if (!iDocSubject->IsAttached(this, IID_IHIERARCHY_DOCUMENT, IID_IPSTLSTOBSERVER))
		{
			iDocSubject->AttachObserver(this, IID_IHIERARCHY_DOCUMENT, IID_IPSTLSTOBSERVER);
		}

		if (!iDocSubject->IsAttached(this, IID_IHIERARCHY, IID_IPSTLSTOBSERVER))
		{
			iDocSubject->AttachObserver(this, IID_IHIERARCHY, IID_IPSTLSTOBSERVER);
		}
	} while (kFalse);
}

void TPLDocObserver::DetachDocument(IDocument*	iDocument)
{
	do
	{
		if (iDocument == nil)
		{
			ASSERT_FAIL("no document to attach to");
			break;
		}
		
		InterfacePtr<ISubject> 		iDocSubject(iDocument, UseDefaultIID());
		if (iDocSubject == nil)
		{
			ASSERT_FAIL("no document subject to attach the observer");
			break;
		}

		if (iDocSubject->IsAttached(this, IID_IHIERARCHY_DOCUMENT, IID_IPSTLSTOBSERVER))
		{
			iDocSubject->DetachObserver(this, IID_IHIERARCHY_DOCUMENT, IID_IPSTLSTOBSERVER);
		}

		if (iDocSubject->IsAttached(this, IID_IHIERARCHY, IID_IPSTLSTOBSERVER))
		{
			iDocSubject->DetachObserver(this, IID_IHIERARCHY, IID_IPSTLSTOBSERVER);
		}
	} while (kFalse);
}

