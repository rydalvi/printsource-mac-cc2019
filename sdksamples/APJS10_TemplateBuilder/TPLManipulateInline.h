#ifndef __TPLMANIPULATE_INLINE_H__
#define __TPLMANIPULATE_INLINE_H__
// Framework includes:
//#include "SnpRunnable.h"
//#include "ISnipRunParameterUtils.h"
//#include "SnipRunLog.h"

/** 
	\li	How to create an inline within a story by processing kInsertILGCmdBoss.
	
  	@ingroup sdk_snippet
	@ingroup sdk_text

	@see kInsertILGCmdBoss
 */
class TPLManipulateInline 
{
	public:
		/** Constructor.
		 */
		TPLManipulateInline() {}

		/** Destructor.
		 */
		virtual			~TPLManipulateInline() {}
		/** Creates a new inline frame in the given story.
			@param storyUIDRef refers to the story in which the inline is to be created.
			@param whereTextIndex index where the inline should be inserted.
			@return kSuccess on success, kFailure otherwise.
		*/
		ErrorCode InsertInline(const UIDRef& storyUIDRef, const TextIndex& whereTextIndex);
	
	protected:
		/**	Create a frame. Note you can create a frame that contains
			any type of content, or no content at all, and make it into an inline.
			This method makes a graphic frame.
			@param database the frame should be created in.
			@param newFrameUIDRef OUT frame to be made into an inline. 
			@return kSuccess on success, other status otherwise.
			@postcondition the created frame is not parented on any hierarchy.
		*/
		ErrorCode CreateFrame(IDataBase* database, UIDRef& newFrameUIDRef);

		/**	Insert kTextChar_Inline into the text flow and
			process kChangeILGCmdBoss to make the given frame into an inline on
			the owned item strand(IItemStrand) of the text model(ITextModel).
			@param storyUIDRef refers to the story in which the inline is to be created.
			@param whereTextIndex index where the inline should be inserted.
			@param frameUIDRef frame to be made into an inline. Note this frame must not
			be parented on any hierarchy(IHierarchy). If you have a frame that is process 
			kRemoveFromHierarchyCmdBoss before calling this method.
			@return kSuccess on success, other status otherwise. 
		*/
		ErrorCode ChangeToInline(const UIDRef& storyUIDRef, const TextIndex& whereTextIndex, const UIDRef& frameUIDRef);

};

#endif