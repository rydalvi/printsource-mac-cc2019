
#include "VCPlugInHeaders.h"
#include "TPLMediatorClass.h"
#include "IControlView.h"

//UIDRef		TPLMediatorClass::curSelItemUIDRef=-1;
int32       TPLMediatorClass::count1=-1;
int32       TPLMediatorClass::count2=-1;
int32       TPLMediatorClass::count3=-1;
int32       TPLMediatorClass::count4=-1;
int32       TPLMediatorClass::count5=-1;
int32		TPLMediatorClass::curSelRowIndex=-1;
PMString	TPLMediatorClass::curSelRowString("");
int32		TPLMediatorClass::curSelRadBtn=1;	
int32		TPLMediatorClass::curSelLstbox=1;	
bool16		TPLMediatorClass::imageFlag=kFalse;
double		TPLMediatorClass::lastSelClasId=0;
PMString	TPLMediatorClass::lastSelClasName("");
PMString	TPLMediatorClass::lastSelClasHierarchy("");
double		TPLMediatorClass::lastSelClasIdForRefresh=0;
PMString	TPLMediatorClass::lastSelClasNameForRefresh("");
PMString	TPLMediatorClass::lastSelClasHierarchyForRefresh("");
IControlView* TPLMediatorClass::PFLstboxCntrlView=nil;
IControlView* TPLMediatorClass::PGLstboxCntrlView=nil;
IControlView* TPLMediatorClass::PRLstboxCntrlView=nil;
IControlView* TPLMediatorClass::ItemLstboxCntrlView=nil;
IControlView* TPLMediatorClass::ProjectLstboxCntrlView=nil;

// Added by Awasthi
IControlView* TPLMediatorClass::dropdownCtrlView1=nil;
IControlView* TPLMediatorClass::refreshBtnCtrlView1=nil;
IControlView* TPLMediatorClass::appendRadCtrlView1=nil;
IControlView* TPLMediatorClass::embedRadCtrlView1=nil;
IControlView* TPLMediatorClass::tagFrameChkboxCtrlView1=nil;
// End Awasthi

IPanelControlData* TPLMediatorClass::iPanelCntrlDataPtr=nil;
IPanelControlData* TPLMediatorClass::iPanelCntrlDataPtrTemp=nil;
int32 TPLMediatorClass::tableFlag=0;
bool16 TPLMediatorClass::loadData=kFalse;
//TPLMediaterClass::TPLListboxData

LanguageModelCache* TPLMediatorClass::cacheForLangModel=new LanguageModelCache();

CLanguageModel TPLMediatorClass::CurrentLanguageModel;
int32 TPLMediatorClass::LanguageModelCacheSize = -1;

ActionID TPLMediatorClass::CurrentLangActionID = -1;
double TPLMediatorClass::CurrLanguageID = -1;

bool16 TPLMediatorClass::doReloadFlag = kFalse;

//added by vijay choudhari on 24-04-2006 . int32 of the frame(Active frame which
//has cursor)is stored in following variable to make it persistent.
int32	TPLMediatorClass::initialSelItemUIDRef = 0;
int32	TPLMediatorClass::initialCaratPosition = 0;
int32   TPLMediatorClass::overLapingStatus = 0;
int32   TPLMediatorClass::addTagToTextDoubleClickVersion = 0;
int32   TPLMediatorClass::textSpanOFaddTagToTextDoubleClickVersion = 0;
int32   TPLMediatorClass::appendTextIntoSelectedAndOverlappedBox = 0;
int32	TPLMediatorClass::isCaratInsideTable = 0;
bool16  TPLMediatorClass::isInsideTable = kFalse;
bool16	TPLMediatorClass::checkForOverlapWithTextFrame = kFalse; 
bool16	TPLMediatorClass::checkForOverlapWithMasterframe = kFalse;
UIDList	TPLMediatorClass::UIDListofMasterPageItems;
//added by vijay on 9-9-2006
bool16	TPLMediatorClass::IsOneSourceMode = kFalse;
//added by vijay on 9-11-2006
bool16	TPLMediatorClass::IsTableInsideTableCell= kFalse;

//----
double TPLMediatorClass::currTempletSelectedClassID = -1 ;
int32 TPLMediatorClass::dataType = -1;
