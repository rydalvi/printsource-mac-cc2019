#include "VCPlugInHeaders.h"
#include "IPageItemScrapData.h"
#include "IPageItemLayerData.h"
#include "IPathUtils.h"
#include "IPMDataObject.h"
#include "IDragDropController.h"
#include "IGraphicAttributeUtils.h"
#include "IGraphicAttributeSuite.h"
#include "ISwatchUtils.h"
#include "ISwatchList.h"
#include "ILayoutUIUtils.h"
#include "IGeometry.h"
//#include "SDKUtilities.h"
#include "CAlert.h"
#include "CDragDropSource.h"
#include "UIDList.h"
#include "CmdUtils.h"
#include "UIDRef.h"
#include "K2Vector.h"
#include "SplineID.h"
#include "PMFlavorTypes.h"
#include "PMRect.h"
#include "SystemUtils.h"

#include "TPLID.h"
#include "CAlert.h"
#include "TPLMediatorClass.h"
#include "ITreeViewController.h"
#include "TPLDataNode.h"
#include "IntNodeID.h"
//#include "ITreeNodeIDData.h"
#include "TPLTreeDataCache.h"
#include "K2Vector.tpp" 
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
#include "TPLListboxData.h"
#include "TPLCommonFunctions.h"
#include "IAppFramework.h"


//MessageServer
//#include "IMessageServer.h"
#define FILENAME			PMString("TPLDragSource.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAlert::InformationAlert(X)//CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}


extern int32 SelectedRowNo;
extern bool16 isAddAsDisplayName;	
extern bool16 isAddImageDescription;
extern bool16 isOutputAsSwatch;

extern bool16 isSpreadBasedLetterKeys;
UIDRef newPageItem;
UIDRef newPageItem1;
class TPLDragSource : public CDragDropSource
{
	public:
		TPLDragSource(IPMUnknown* boss);
		/**
			we will only allow the drag if there is an existing document. We faux the creation of a page item,
			this simplifies the database requirements.
			@param e IN the event signifying the start of the drag.
			@return kTrue if we will drag from this location, otherwise kFalse.
		*/
		bool16 WillDrag(IEvent* e) const;
		/**
			we override this method to add the content we want to drag from this widget.
			@param DNDController IN the controller in charge of this widget's DND capabilities.
			@return kTrue if valid content has been added to the drag
		*/
		bool16 DoAddDragContent(IDragDropController* DNDController);

		/** In our case we have a custom drag item, so the target does not know how to draw feedback for this item.
			Therefore we assume responsibility for this.
			@return the SysRgn representing the feedback we want to convey.
		*/
		/* SysRgn DoMakeDragOutlineRegion() const; */ //APSCC2017_Comment

	private:
		/** 
			this method just creates a spline
			@param newPageItem OUT the UIDRef for the created page item.
			@param parent IN the UIDRef for the parent that the new item should be attached to.
			@param points IN the pair of points that define the spline.
			@param strokeWeight IN the initial stroke applied to the path for the new page item.
			@return kSuccess if the page item is created without error, kFailure otherwise.
		*/
		ErrorCode CreatePageItem(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight);
		ErrorCode CreatePicturebox(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight);
		ErrorCode CreateTextbox(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight);

		/** Cache the page item */
		UIDRef fPageItem;
};

CREATE_PMINTERFACE(TPLDragSource, kTPLDragSourceImpl)

TPLDragSource::TPLDragSource(IPMUnknown* boss)
: CDragDropSource(boss)
{
}

/*
	We indicate we are only interested in dragging if there is a document open. This is making the assumption that we
	want to drag the item onto the layout widget, but also allows us to create the page item within the context of that document.
*/
bool16
TPLDragSource::WillDrag(IEvent* e) const
{	
	//CA("I m in will drag function");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFalse;
	
	//ptrIAppFramework->LogError("TPLDragSource::WillDrag");
	
	InterfacePtr<ITreeViewController> treeViewMgr(TPLMediatorClass::ProjectLstboxCntrlView, UseDefaultIID());
	if(treeViewMgr==nil)
	{
		return kFalse;
	}

	NodeIDList selectedItem;
	treeViewMgr->GetSelectedItems(selectedItem);
	if(selectedItem.size()<=0)
	{
		return kFalse; 
	}
	
	NodeID nid=  selectedItem[0];
	TreeNodePtr<IntNodeID>  uidNodeIDTemp1(nid);
	//TreeNodePtr<NodeID>  uidNodeIDTemp(nid);
	const int32 uid = uidNodeIDTemp1->Get();
	//int32 uid32= (int32) uidNodeIDTemp->Get();
	int32 uid32= uid;

	TPLDataNode pNode;
	TPLTreeDataCache dc;

	//dc.isExist(uid, pNode);
	dc.isExist(uid32, pNode);
	PMString pfName = pNode.getName();
	//ptrIAppFramework->LogError("pfName = " + pfName);


	if(pfName == "Letter Keys")
	{
		//itristatecontroldata3->Deselect();
		//iSpreadBasedLetterKeysCntrlView->Enable();
	}
	else
	{
		//itristatecontroldata3->Deselect();
		//iSpreadBasedLetterKeysCntrlView->Disable();
		isSpreadBasedLetterKeys = kFalse;
	}


	TPLMediatorClass::imageFlag=kFalse;
    TPLMediatorClass::dataType=-1;
	
	if(SelectedRowNo == 0)  // Project tree is selected
	{
		//CA("0 selected");
		TPLMediatorClass::curSelLstbox=5;
		TPLMediatorClass::curSelRowIndex=pNode.getSequence();
		TPLMediatorClass::curSelRowString=pfName;
		TPLMediatorClass::tableFlag=0;
		///////	Added
		TPLListboxData ProjectListData;
		int32 vectSize=ProjectListData.returnListVectorSize(5);
		for(int y=0;y<vectSize;y++)
		{
			if(y==TPLMediatorClass::curSelRowIndex)
			{						
				TPLListboxInfo ProjectListboxInfo=ProjectListData.getData(5,y);
				//curSelRowStr=ItemListboxInfo.name;
				TPLMediatorClass::tableFlag=ProjectListboxInfo.tableFlag;
				TPLMediatorClass::imageFlag=ProjectListboxInfo.isImageFlag;
                TPLMediatorClass::dataType= ProjectListboxInfo.dataType;
				break;
			}
		}
		/////////	End
		/*pfName.AppendNumber(TPLMediatorClass::curSelRowIndex);
		CA(pfName);*/
	}
	else if(SelectedRowNo == 1)  // PF tree is selected
	{
		//CA("1 selected");
		TPLMediatorClass::curSelLstbox=1;
		TPLMediatorClass::curSelRowIndex=pNode.getSequence();
		TPLMediatorClass::curSelRowString=pfName;
		TPLMediatorClass::tableFlag=0;
        
		/*pfName.AppendNumber(TPLMediatorClass::curSelRowIndex);
		CA(pfName);*/
	}
	else if(SelectedRowNo == 2)  // PG tree is selected
	{
		//CA("2 selected");
		TPLMediatorClass::curSelLstbox=2;
		TPLMediatorClass::curSelRowIndex=pNode.getSequence();
		TPLMediatorClass::curSelRowString=pfName;
		TPLMediatorClass::tableFlag=0;

		/*pfName.AppendNumber(TPLMediatorClass::curSelRowIndex);
		CA(pfName);*/
	}
	else if(SelectedRowNo == 3)  // PR tree is selected
	{
		//CA("3 selected");
		
		TPLMediatorClass::curSelLstbox=3;
		TPLMediatorClass::curSelRowIndex=pNode.getSequence();
		TPLMediatorClass::curSelRowString=pfName;

		TPLListboxData PRLstboxData;
		int32 vectSize=PRLstboxData.returnListVectorSize(3);

		/* For getting data of the selected row of listbox */
		for(int y=0;y<vectSize;y++)
		{
			if(y==TPLMediatorClass::curSelRowIndex)
			{
				TPLListboxInfo PRListboxInfo=PRLstboxData.getData(3,y);
				//curSelRowStr=PRListboxInfo.name;
				TPLMediatorClass::tableFlag=PRListboxInfo.tableFlag;
				TPLMediatorClass::imageFlag=PRListboxInfo.isImageFlag;
                TPLMediatorClass::dataType= PRListboxInfo.dataType;

				if((PRListboxInfo.id ==  -1) && (PRListboxInfo.typeId == -1001))  //for Possible Value Box Attribute Drag is stopped here 
					return kFalse;
				break;
			}
		}
		

		/*pfName.AppendNumber(TPLMediatorClass::curSelRowIndex);
		CA(pfName);*/
	}
	
	
	else if(SelectedRowNo == 4) 
	{
		//CA("4 selected");
		
		TPLMediatorClass::curSelLstbox=4;
		TPLMediatorClass::curSelRowIndex=pNode.getSequence();
		TPLMediatorClass::curSelRowString=pfName;
		TPLMediatorClass::tableFlag=0;
		TPLListboxData ItemLstboxData;
		int32 vectSize=ItemLstboxData.returnListVectorSize(4);

				/* For getting data of the selected row of listbox */
				for(int y=0;y<vectSize;y++)
				{
					if(y==TPLMediatorClass::curSelRowIndex)
					{						
						TPLListboxInfo ItemListboxInfo=ItemLstboxData.getData(4,y);
						//curSelRowStr=ItemListboxInfo.name;
						TPLMediatorClass::tableFlag=ItemListboxInfo.tableFlag;
						TPLMediatorClass::imageFlag=ItemListboxInfo.isImageFlag;
                        TPLMediatorClass::dataType= ItemListboxInfo.dataType;
						break;
					}
				}

		/*pfName.AppendNumber(TPLMediatorClass::curSelRowIndex);
		CA(pfName);*/
	}
	
	IDocument * theFrontDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();
	if (theFrontDoc == nil)
		return kFalse;

	if(TPLMediatorClass::curSelRowIndex==-1)
	{
		return kFalse;
	}
/*	if(TmplDragEventFinder::selectedRowIndex==-1)
		return kFalse;
*/

	return kTrue;
}

/*
	We override the DoAddDragContent method to define the content for the drag, 
	in this case a simple spline item is added. Note, this is just a simple 
	implementation for demonstration purposes. 
*/
bool16
TPLDragSource::DoAddDragContent(IDragDropController* DNDController)
{	//CA(__FUNCTION__);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFalse;


	bool16 result = kFalse;
	// do while(false) loop for error control
	do 
	{
		// Get the dataexchangehandler for the flavor we want to add 
		// The method QueryHandler is on the dataexchangecontroller, which adds extra functionality
		// to the dragdropcontroller. I am assuming we drag from our custom source to custom target
		// via the layout widget, if we attempt to go directly, the handler will be nil.
		
		//IDataExchangeHandler* ourHandler = NULL;
		
//Change in if condition is made by vijay choudhari on 4-05-2006		
		if(SelectedRowNo == 4 ||SelectedRowNo == 3 ||SelectedRowNo == 2 ||SelectedRowNo == 1 ||SelectedRowNo == 0 || TPLMediatorClass::imageFlag==kTrue)
		{
			InterfacePtr<IDataExchangeHandler>ourHandler(DNDController->QueryHandler(ourFlavor1));		
		
			// if we cannot get the handler for our flavor, we are dead in the water.
			// The handler gives us the scrap to store our content on...
			if (ourHandler==nil)
			{
				//CA("Handler nil for our flavor?");
				break;
			}	
		
			// we need to place our content onto the scrap
			InterfacePtr<IPageItemScrapData> scrapData(ourHandler, UseDefaultIID());
			if (scrapData == nil)
			{
				//CA("No scrap data for DEHandler?");
				break;
			}
		
			// get the root of the scrap	
			UIDRef 	scrapRoot =	scrapData->GetRootNode();
			// and its associated database
			//IDataBase * scrapDB = scrapRoot.GetDataBase();
			
			// new page item
			newPageItem = UIDRef::gNull;
			newPageItem1 = UIDRef::gNull;
			// set up a list of points for the page item
			PMPointList points(2, PMPoint());
			// define the text bounding box using two pmpoints
			PMPoint startPoint(0,0);
			PMPoint endPoint(100,100);
//--------------CS3 Change------------------------//
			//points.Append( startPoint );
			points.push_back( startPoint );
			//points.Append( endPoint );
			points.push_back( endPoint );
			PMReal strokeWeight(0.0);
			
			if(TPLMediatorClass::imageFlag==kFalse) 
			{	
//CA("TPLMediatorClass::imageFlag==kFalse");				
				
//Change in if condition is made by vijay choudhari on 4-05-2006
				if(SelectedRowNo == 4 ||SelectedRowNo == 3 ||SelectedRowNo == 2 ||SelectedRowNo == 1 ||SelectedRowNo == 0 )
				{					
					if(CreateTextbox(newPageItem,scrapRoot,points,strokeWeight)==kFailure)
					{
						//CA("Failed to create page item?");
						break;
					}					
				}
			}
			if(TPLMediatorClass::imageFlag==kTrue)
			{
				if(isAddAsDisplayName == kTrue)
				{
					if(CreateTextbox(newPageItem,scrapRoot,points,strokeWeight)==kFailure)
					{

						//CA("Failed to create page item?");
						break;
					}
				}
				else if(isAddImageDescription == kTrue)
				{
					if(CreateTextbox(newPageItem,scrapRoot,points,strokeWeight)==kFailure)
					{

						//CA("Failed to create page item?");
						break;
					}
				}
				else
				{
					//CA("CreatePicturebox(newPageItem?");
					if(CreatePicturebox(newPageItem,scrapRoot,points,strokeWeight)==kFailure)
					{
							//CA("Failed to create page item?");
						break;
					}
				}
			}
			ourHandler->Clear();
			
//Change in if condition is made by vijay choudhari on 4-05-2006			
			if(SelectedRowNo == 4 ||SelectedRowNo == 3 ||SelectedRowNo == 0|| TPLMediatorClass::imageFlag==kTrue)
			{
				// Must manually clear the handler before adding any data...
				
				
				// we call replace to define the DB the item exists in. We add our new item to the scrap
				scrapData->Replace(UIDList(newPageItem));
				// Update the cached variable

				fPageItem = newPageItem;
				newPageItem1 = newPageItem;

		/*		InterfacePtr<IPageItemLayerData> layerData(scrapData,IID_IPAGEITEMLAYERDATA);
				// I faux the layer the item is on.
				K2Vector<int32> layerIndexList(1);
				layerIndexList.Append(0);

				// Make a list of the layer names.
				K2Vector<PMString> layerNameList;
				layerNameList.Preallocate(1);
				PMString layerName("Untitled");
				layerNameList.Append(layerName);

				layerData->SetPageItemList(UIDList(newPageItem));
				layerData->SetLayerNameList(layerNameList);
				layerData->SetLayerIndexList(layerIndexList);
				layerData->SetIsGuideLayer(kFalse);
		*/
			}
				// we point the controller at the pageitem handler 
				DNDController->SetSourceHandler(ourHandler);
					
				// we get the data object that represents the drag
				InterfacePtr<IPMDataObject> item(DNDController->AddDragItem(1));		
				// no flavor flags	
				PMFlavorFlags flavorFlags = 0;			
				// we set the type (flavour) in the drag object 
				item->PromiseFlavor(ourFlavor1, flavorFlags);
		}
		/*else
		{
	
			CA("kPMTextFlavor ");
			ourHandler =(DNDController->QueryHandler(kPMTextFlavor));
			
			InterfacePtr<IPMDataObject> item(DNDController->AddDragItem(1));

			PMFlavorFlags flavorFlags = kNormalFlavorFlag;

			item->PromiseFlavor(kPMTextFlavor, flavorFlags);



			InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
			if(!iSelectionManager)
			{	CA("!iSelectionManager");
				return kFalse;
			}
			UIDRef ref;
			InterfacePtr<ITextMiscellanySuite> txtSelectionSuite(static_cast<ITextMiscellanySuite* >
			( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager)));
			if(!txtSelectionSuite){
				CA("Please Select a Type Tool");
			}
			else
			{
				bool16 ISTextFrame = txtSelectionSuite->GetFrameUIDRef(ref);
			}
		}*/
		//CA("DoAddDragContent  END");
 		result = kTrue;
	} while (false);
	return result; 
}

/* As we have a custom flavour, the drag target is unlikely to know how to draw feedback, therfore we draw it in the source */
/* //APSCC2017_Comment
SysRgn
TPLDragSource::DoMakeDragOutlineRegion() const
{
//	CA(__FUNCTION__);
	//CA("I m in DOMakeDragOutlineRegion");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return nil;


	// do while(false) loop for error control
	do 
	{		
			// get the geometry of the page item
			InterfacePtr<IGeometry> iGeometry(fPageItem,UseDefaultIID());			
			if (iGeometry == nil)
			{				
				//CA("No geometry on scrap item!");
				break;
			}
			
			//// we will draw a rectangle around the bounding box
			//
			//PMRect thePIGeo = iGeometry->GetStrokeBoundingBox();
		 //   
			//// we need to draw the bounding box at the current mouse location
			//SysPoint currentMouse = GetMousePosition();
			//
			//PMPoint start(currentMouse);
			//
			//PMPoint lTop(thePIGeo.LeftTop());
			//PMPoint rBottom(thePIGeo.RightBottom());
	  //     
			//lTop+=start;
			//rBottom+=start;
			//// create a new pmrect based on the page item, offset from the mouse location
			//// On windows this feedback resorts to the default, on the mac it would be 
			//PMRect offsetRect(lTop,rBottom);
	  //     
			//SysRect windowRect = ::ToSys(offsetRect);
			//SysRgn origRgn = ::CreateRectSysRgn(windowRect);
	  //     
			//// Get the window region for the rectangle.
			//::InsetSysRect(windowRect,7,7);
			//
			//SysRgn windowRgn = ::CreateRectSysRgn(windowRect);
	  //     
			//DiffSysRgn(origRgn,windowRgn,origRgn);
			//return origRgn;

			// we will draw a rectangle around the bounding box
			PMRect thePIGeo = iGeometry->GetStrokeBoundingBox();
			// we need to draw the bounding box at the current mouse location
			SysPoint currentMouse = GetMousePosition();
			PMPoint start(currentMouse);
			PMPoint lTop(thePIGeo.LeftTop());
			PMPoint rBottom(thePIGeo.RightBottom());
			lTop+=(start-PMPoint(thePIGeo.Width()/2,thePIGeo.Height()/2));
			rBottom+=(start-PMPoint(thePIGeo.Width()/2,thePIGeo.Height()/2));
			// create a new pmrect based on the page item, offset from the mouse location
			// On windows this feedback resorts to the default, on the mac it would be 
			PMRect offsetRect(lTop,rBottom);
			SysRect windowRect = ::ToSys(offsetRect);
			SysWireframe origRgn = ::CreateRectSysWireframe(windowRect);
			// Get the window region for the rectangle.
			::InsetSysRect(windowRect,7,7);
			SysWireframe windowRgn = ::CreateRectSysWireframe(windowRect);
			DiffSysWireframe(origRgn,windowRgn,origRgn);
			::DeleteSysWireframe(windowRgn);
			return origRgn;
		
	}
	while (kFalse);
	return nil;
}
*/

/* this method takes in a points list, stroke weight and parent UIDRef and creates a spline with those points, the stroke weight
	and attaches it to the parent.
*/
ErrorCode TPLDragSource::CreatePageItem(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight)
{//CA(__FUNCTION__);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFailure;


	ErrorCode returnValue = kFailure;
	UIDRef newSplineItem = UIDRef(nil, kInvalidUID);
	do 
	{
		
		// wrap the commands in a sequence
		ICommandSequence *sequence = CmdUtils::BeginCommandSequence();
		//CA(" break 1  in CreatePageItem function");
		if (sequence == nil)
		{
			//CA("Cannot create command sequence?");
			break;
		}
       
		// create the page item
		newSplineItem = Utils<IPathUtils>()->CreateLineSpline(parent, points, INewPageItemCmdData::kDefaultGraphicAttributes);
		
		// put the new item into a splinelist
		const UIDList splineItemList(newSplineItem);
       
		// we apply the stroke to our item
		InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->CreateStrokeWeightCommand(strokeWeight,&splineItemList,kTrue,kTrue));		
		if(strokeSplineCmd==nil)
		{
			//CA("Cannot create the command to stroke the spline?");
			break;
		}
       
		if(CmdUtils::ProcessCommand(strokeSplineCmd)==kFailure)
		{
			//CA("Failed to stroke the spline?");
			break;
		}

		// we want to apply the stroke, color it black. We need to get the black swatch...		
		InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QuerySwatchList(splineItemList.GetDataBase()));		
		if(iSwatchList==nil)
		{
			//CA("Cannot get swatch list?");
			break;
		}
		
		UID blackUID = iSwatchList->/*GetRegistrationSwatchUID*/GetNoneSwatchUID (); /*GetPaperSwatchUID();*/ 
	
		// Now get the command to apply the stroke...
		InterfacePtr<ICommand> applyStrokeCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeRenderingCommand(blackUID,&splineItemList,kTrue,kTrue));
		
		if(applyStrokeCmd==nil)
		{
			//CA("Cannot create the command to render the stroke?");
			break;
		}
       
		if(CmdUtils::ProcessCommand(applyStrokeCmd)!=kSuccess)
		{
			//CA("Failed to render the stroke?");
			break;
		}
		
		// now we can end the command sequence, we are done.
		
		CmdUtils::EndCommandSequence(sequence);
		newPageItem = newSplineItem;
		returnValue=kSuccess;
		
	} while(false);
	return returnValue;
}

ErrorCode 
TPLDragSource::CreatePicturebox
(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight)
{
	//CA(__FUNCTION__);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFailure;

	ErrorCode returnValue = kFailure;
	do 
	{
		ICommandSequence *sequence = CmdUtils::BeginCommandSequence();
		if (sequence == nil)
			break;

		PMPoint topLeft(5,5);
		PMPoint bottomRight(100,100);
		PMRect rect(topLeft, bottomRight);
		UIDRef newSplineItem = Utils<IPathUtils>()->CreateRectangleSpline
		(parent, rect, INewPageItemCmdData::kGraphicFrameAttributes);

		const UIDList splineItemList(newSplineItem);

		InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->
		CreateStrokeWeightCommand(strokeWeight,&splineItemList,kTrue,kTrue));
		if(strokeSplineCmd==nil)
			break;
				
		if(CmdUtils::ProcessCommand(strokeSplineCmd)==kFailure)
			break;

	//	InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QueryActiveSwatchList());
	//	if(iSwatchList==nil)
	//		break;
	//	int32 blackUID = iSwatchList->GetRegistrationSwatchUID (); /*GetPaperSwatchUID();*/ //
	//	
	//	InterfacePtr<ICommand> applyStrokeCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeRenderingCommand(blackUID,&splineItemList,kTrue,kTrue));
	//	if(applyStrokeCmd==nil)
	//		break;

	//	if(CmdUtils::ProcessCommand(applyStrokeCmd)!=kSuccess)
	//		break;
		
		CmdUtils::EndCommandSequence(sequence);
		newPageItem = newSplineItem;
		returnValue=kSuccess;
	} while(false);
	return returnValue;
}


ErrorCode 
TPLDragSource::CreateTextbox(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight)
{
	//CA(__FUNCTION__);
	//CA("Inside Create Text Frame");

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFailure;

	

	ErrorCode returnValue=kFailure;
	PMString weight("strokeWeight::");
	weight.AppendNumber(strokeWeight);
	//CA(weight);

	
	do 
	{
		
		ICommandSequence *sequence=CmdUtils::BeginCommandSequence();
		if (sequence==nil)
			break;
       
		PMPoint topLeft(5,5);
		PMPoint bottomRight(100,100);
		PMRect rect(topLeft, bottomRight);
		 
		UIDRef newSplineItem = Utils<IPathUtils>()->CreateRectangleSpline
		(parent, rect, INewPageItemCmdData::kTextFrameAttributes,kTrue/*0x263*/);
		if(newSplineItem == UIDRef::gNull)
		{
			CA("Null uidref");
		}
		const UIDList splineItemList(newSplineItem);
  
		InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->
		CreateStrokeWeightCommand(strokeWeight,&splineItemList,kTrue,kTrue));
		if(strokeSplineCmd==nil)
			break;
       
		if(CmdUtils::ProcessCommand(strokeSplineCmd)==kFailure)
			break;
       
		InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QuerySwatchList(splineItemList.GetDataBase()));
		if(iSwatchList==nil)
			break;
	
		UID blackUID = iSwatchList->/*GetRegistrationSwatchUID*/GetNoneSwatchUID(); /*GetPaperSwatchUID();*/ //
		
		InterfacePtr<ICommand> applyStrokeCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeRenderingCommand(blackUID,&splineItemList,kTrue,kTrue));

		if(applyStrokeCmd==nil)
			break;
		
		if(CmdUtils::ProcessCommand(applyStrokeCmd)!=kSuccess)
			break;
      CmdUtils::EndCommandSequence(sequence);
		newPageItem=newSplineItem;
		returnValue=kSuccess;
		
	} while(false);
	//CA("Inside Create Text Frame End ");
	return returnValue;
}
// End, TPLDragSource.cpp.





