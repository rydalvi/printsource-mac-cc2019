//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/tablebasics/TblBscSuiteTextCSB.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rahul $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: 1.11 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ITextTarget.h"
#include "ITextModel.h"
#include "ITextStoryThreadDict.h"
#include "ITextStoryThread.h"
#include "ITextParcelList.h"
#include "ITextSelectionData.h"
#include "ITextFocus.h"
#include "IComposeScanner.h"
#include "ITableTarget.h"
#include "ICellContentMgr.h"	// for kTextContentType
#include "ICellContent.h"
#include "ITableModel.h"
#include "ITableCommands.h"
#include "ITableFrame.h"
#include "ITableSuite.h"
#include "ITableModelList.h"
#include "ISelectUtils.h"
#include "INewTableCmdData.h"
#include "ITextModelCmds.h"
#include "ITableTextSelection.h"

// General includes:
#include "TextIterator.h"
#include "ErrorUtils.h"
#include "CmdUtils.h"
#include "TablesUIMinMaxValues.h"
#include "RangeData.h"
#include "CPMUnknown.h"

#include "TblBscSuiteTextCSB.h"


/** Text selection boss ITblBscSuite implementation to create
	table content when a text selection is active.

	@author Seoras Ashby/Ian Paterson
	@see ITblBscSuite
	@ingroup tablebasics
*/


/*  Makes the implementation available to the application.
*/
CREATE_PMINTERFACE(TblBscSuiteTextCSB, kTPLTblBscSuiteTextCSBImpl)


/*
*/
TblBscSuiteTextCSB::TblBscSuiteTextCSB(IPMUnknown* boss) :
	CPMUnknown<ITblBscSuite>(boss)
{
}

/*
*/
TblBscSuiteTextCSB::~TblBscSuiteTextCSB(void)
{
}

/*
*/
bool16 TblBscSuiteTextCSB::CanInsertTable() const
{
	bool16 result = kFalse;
	do
	{
		InterfacePtr<ITableSuite> tableSuite(this, UseDefaultIID());
		ASSERT(tableSuite);
		if (!tableSuite) {
			break;
		}
		result = tableSuite->CanInsertTable();

	} while(false);
	return result;
}

/*
*/
ErrorCode TblBscSuiteTextCSB::InsertTable(int32 numRows, int32 numCols)
{
	ErrorCode result = kFailure;
	do
	{
		ASSERT(numRows>0 && numCols>0);
		if(numRows<=0 || numCols<=0) {
			break;
		}
		InterfacePtr<ITableSuite> tableSuite(this, UseDefaultIID());
		ASSERT(tableSuite);
		if (!tableSuite) {
			break;
		}

		const int32 headerRows = 0;
		const int32 footerRows = 0;
		tableSuite->InsertTable(numRows, numCols, headerRows, footerRows);

		result = kSuccess;

	} while(false);

	return result;
}

/*
*/
bool16 TblBscSuiteTextCSB::CanSetCellText(int32 row, int32 col) const
{
	bool16 result = kFalse;
	do
	{
		// If we can get a table model and the row and column are valid.
		InterfacePtr<ITableModel> tableModel(this->queryTableModelAndValidateRowCol(row, col));
		if (tableModel == nil)
		{
			break;
		}

		// And it is an anchor.
		bool16 isAnchor = tableModel->IsAnchor(GridAddress(row, col));
		if(!isAnchor) {
			break;
		}

		// We can set text for this cell.
		result = kTrue;

	} while(false);

	return result;
}

/*
*/
ErrorCode TblBscSuiteTextCSB::SetCellText
(
 int32 row, 
 int32 col, 
 const WideString& text
)
{
	ErrorCode result = kFailure;
	do
	{
		InterfacePtr<ITableModel> tableModel(this->queryTableModelAndValidateRowCol(row,col));
		if (tableModel == nil)
		{
			break;
		}

		// Set the text for the cell.
		result = setCellText(tableModel, row, col, text);

	} while(false);

	return result;
}


/*
*/
bool16 TblBscSuiteTextCSB::CanGetCellText(int32 row, int32 col) const
{
	bool16 result = kFalse;
	do
	{
		InterfacePtr<ITableModel> tableModel(this->queryTableModelAndValidateRowCol(row, col));
		if (tableModel == nil){
			break;
		}

		GridAddress targetCellGridAddress(row, col);
		GridID targetCellGridID = tableModel->GetGridID(targetCellGridAddress);
		if (targetCellGridID == kInvalidGridID) {
			break;
		}

		InterfacePtr<ITextStoryThreadDict> textStoryThreadDict(tableModel, UseDefaultIID());
		ASSERT(textStoryThreadDict);
		if (textStoryThreadDict == nil) {
			break;
		}

		InterfacePtr<ITextStoryThread> textStoryThread(textStoryThreadDict->QueryThread(targetCellGridID));
		ASSERT(textStoryThread);
		if (textStoryThread == nil){
			break;
		}
		
		result = kTrue;

	} while(false);

	return result;
}

/*
*/
void TblBscSuiteTextCSB::GetCellText(int32 row, int32 col, WideString& text) const
{
	do
	{
		InterfacePtr<ITableModel> tableModel(this->queryTableModelAndValidateRowCol(row, col));
		if (tableModel == nil) {
			break;
		}

		GridAddress targetCellGridAddress(row, col);
		GridID targetCellGridID = tableModel->GetGridID(targetCellGridAddress);
		if (targetCellGridID == kInvalidGridID) {
			break;
		}

		InterfacePtr<ITextStoryThreadDict> textStoryThreadDict(tableModel, UseDefaultIID());
		ASSERT(textStoryThreadDict);
		if (textStoryThreadDict == nil) {
			break;
		}

		InterfacePtr<ITextStoryThread> textStoryThread(textStoryThreadDict->QueryThread(targetCellGridID));
		ASSERT(textStoryThread);
		if (textStoryThread == nil){
			break;
		}

		this->getText(textStoryThread, text);

	} while(false);
}

/*
*/
ITableModel* TblBscSuiteTextCSB::queryTableModel() const
{
	ITableModel* result = nil;
	do
	{
		// Aquire the table model.
		InterfacePtr<ITableTarget> tableTarget(this,UseDefaultIID());
		ASSERT(tableTarget);
		if(!tableTarget) {
			break;
		}
		UIDRef tableUIDRef = tableTarget->GetModel();
		if (tableUIDRef == UIDRef(nil, kInvalidUID))
		{
			ASSERT_FAIL("invalid tableUIDRef");
			break;
		}

		InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		ASSERT(tableModel);
		if (tableModel == nil) {
			break;
		}

		result = tableModel;
		result->AddRef();
	} while(false);

	return result;
}

/*
*/
ITableModel* TblBscSuiteTextCSB::queryTableModelAndValidateRowCol(int32 row, int32 col) const
{
	ITableModel* result = nil;
	do
	{
		InterfacePtr<ITableModel> tableModel(this->queryTableModel());
		ASSERT(tableModel);
		if (tableModel == nil){
			break;
		}

		// Check the row and cell are valid for the table.
		if (tableModel->IsValidRow(row) == kFalse) {
			break;
		}

		if (tableModel->IsValidColumn(col) == kFalse) {
			break;
		}

		result = tableModel;
		result->AddRef();

	} while(false);

	return result;
}

/*
*/
ErrorCode TblBscSuiteTextCSB::setCellText(InterfacePtr<ITableModel>& tableModel, 
									int32 row, int32 col, const WideString& text)
{
	ErrorCode result = kFailure;
	do {
		ASSERT(tableModel);
		if(tableModel==nil) {
			break;
		}

		bool16 isAnchor = tableModel->IsAnchor(GridAddress(row, col));
		ASSERT(isAnchor);
		if(!isAnchor) {
			break;
		}

		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		ASSERT(tableCommands);
		if(tableCommands==nil) {
			break;
		}

		result = tableCommands->SetCellText(text, GridAddress(row, col));
		ASSERT(result == kSuccess);

	} while(false);
	return result;

}


/*
*/
void TblBscSuiteTextCSB::getText(const ITextStoryThread* textStoryThread, WideString& text) const
{
	do
	{
		InterfacePtr<ITextModel> textModel(textStoryThread->QueryTextModel());
		if (textModel == nil) {
			break;
		}
		
		int32 span=(-1);
		TextIndex at = textStoryThread->GetTextStart(&span);
		TextIterator beginTextChunk(textModel, at);
		TextIterator endTextChunk(textModel, at + span);
	
		std::copy(beginTextChunk, endTextChunk, std::back_inserter(text));
	
	} while(false);
}

bool16 TblBscSuiteTextCSB::GetCellRange(GridArea& gridArea) 
{
	InterfacePtr<ITableTarget>tableTarget(this,UseDefaultIID());
	if(!tableTarget)
	{
		//CA("Table Target is null");
		return kFalse;
	}
	gridArea=tableTarget->GetRange();
	return kTrue;
}
// End, TblBscSuiteTextCSB.cpp.

bool16 TblBscSuiteTextCSB:: queryTableModel(InterfacePtr<ITableModel> & iTableModel)
	{
		bool16 result = kFalse;	
	do
	{
		// Aquire the table model.
		InterfacePtr<ITableTarget> tableTarget(this,UseDefaultIID());
		ASSERT(tableTarget);
		if(!tableTarget) {
			break;
		}
		UIDRef tableUIDRef = tableTarget->GetModel();
		if (tableUIDRef == UIDRef(nil, kInvalidUID))
		{
			ASSERT_FAIL("invalid tableUIDRef");
			break;
		}

		InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		ASSERT(tableModel);
		if (tableModel == nil) {
			break;
		}


		iTableModel = tableModel;
		iTableModel->AddRef();
		result = kTrue;
	} while(false);

	return result;
		
	}



