#ifndef __TPLActionComponent_h__
#define __TPLActionComponent_h__

#include "CActionComponent.h"
//#include "IPaletteMgr.h"          //Removed From Indesign CS3 API

class TPLActionComponent:public CActionComponent
{
	public:
		TPLActionComponent(IPMUnknown* boss);
		~TPLActionComponent();
		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);
		//virtual void UpdateActionStates(IActionStateList* listToUpdate);
		virtual void UpdateActionStates(IActiveContext* ac, IActionStateList* iListPtr, GSysPoint mousePoint , IPMUnknown* widget );

		void CloseTemplatePalette();
		void DoPalette();

	private:
		void DoAbout();
		void DoItemOne();
		
//		static IPaletteMgr* palettePanelPtr;         //CS3 change
};

#endif