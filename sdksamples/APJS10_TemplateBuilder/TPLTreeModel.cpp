#include "VCPlugInHeaders.h"
#include "IHierarchy.h"	
#include "ISpreadlist.h"
#include "IDocument.h"
#include "ISession.h"
#ifdef	 DEBUG
#include "IPlugInList.h"
#include "IObjectModel.h"
#endif
#include "ILayoutUtils.h" //Cs4
#include "TPLID.h"
#include "TPLTreeModel.h"
#include "CAlert.h"
#include "TPLMediatorClass.h"


#include "TPLDataNode.h"
//#include "IClientOptions.h"
//#include "IAppFrameWork.h"
#include "TPLTreeDataCache.h"
//#include "ISpecialChar.h"

#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("TPLTreeModel.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

TPLTreeDataCache dc;

int32 TPLTreeModel::root=0;
PMString TPLTreeModel::publicationName;
double TPLTreeModel::classId=0;
extern TPLDataNodeList ProjectDataNodeList;
extern TPLDataNodeList PFDataNodeList;
extern TPLDataNodeList PGDataNodeList;
extern TPLDataNodeList PRDataNodeList;
extern TPLDataNodeList ITEMDataNodeList;
extern TPLDataNodeList CatagoryDataNodeList;
int32 RowCountForTree = -1;
extern int32 SelectedRowNo;


TPLTreeModel::TPLTreeModel()
{
	//root=-1;
	//classId=11002;
}

TPLTreeModel::~TPLTreeModel() 
{
}


int32 TPLTreeModel::GetRootUID() const
{	
	if(classId<=0)
		return 0;// kInvalidUID;
	
	TPLDataNode pNode;
	/*if(dc.isExist(root, pNode))
	{
		return root;
	}*/
	
	dc.clearMap();
	
	/*InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("Fail to CreateObject of Application FrameWork.");
		return kInvalidUID;
	}
	
	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				*/

	/*VectorPubObjectValuePPtr VectorFamilyInfoValuePtr = nil;
	VectorFamilyInfoValuePtr = ptrIAppFramework->PUBMngr_getPFTreesForSubSection(classId, kFalse, kFalse, kFalse);
	if(VectorFamilyInfoValuePtr == nil)
		return kInvalidUID;

	if(VectorFamilyInfoValuePtr->size()==0)
	{
		delete VectorFamilyInfoValuePtr;
		return kInvalidUID;
	}
	*/
	/*Adding root*/
	//CA("1");
	
	TPLDataNodeList CurrentNodeList;
	CurrentNodeList.clear();
	switch(SelectedRowNo)
	{
		case 0:
//following codn is added by vijay on 9-10-2006
			if(TPLMediatorClass::IsOneSourceMode)
			{
				CurrentNodeList = CatagoryDataNodeList;
			}
			else
			{
				CurrentNodeList = ProjectDataNodeList;
			}
			break;
		case 1:
			CurrentNodeList = PFDataNodeList;
			break;
		case 2:
			CurrentNodeList = PGDataNodeList;
			break;
		case 3:
			CurrentNodeList = PRDataNodeList;
			break;
		case 4:
			CurrentNodeList = ITEMDataNodeList;
			break;

	}

	RowCountForTree = 0;
	PMString name("Root");
	pNode.setPubId(root);
	pNode.setChildCount(static_cast<int32>(CurrentNodeList.size()));
	/*pNode.setLevel(0);*/
	pNode.setParentId(-2);
	pNode.setPublicationName(name);
	pNode.setSequence(0);
	dc.add(pNode);

	/*Adding kids*/
	/*VectorPubObjectValuePointer::iterator it1;
	VectorPubObjectValue::iterator it2;*/
	
	int count=0;
	for(int i =0 ; i<CurrentNodeList.size(); i++)
	{ //CAlert::InformationAlert(" 123");
			/*pNode.setLevel(1);*/
			
				pNode.setParentId(root);
				pNode.setSequence(i);
				count++;
				pNode.setPubId(count);
				PMString ASD(CurrentNodeList[i].getName());
				pNode.setPublicationName(ASD);
				//CA(ASD);
				pNode.setChildCount(0);
                pNode.setHitCount(CurrentNodeList[i].getHitCount());
			dc.add(pNode);
		}
	return root;
}


int32 TPLTreeModel::GetRootCount() const
{
	int32 retval=0;
	TPLDataNode pNode;
	if(dc.isExist(root, pNode))
	{
		return pNode.getChildCount();
	}
	return 0;
}

int32 TPLTreeModel::GetChildCount(const int32& uid) const
{
	TPLDataNode pNode;

	if(dc.isExist(uid, pNode))
	{
		return pNode.getChildCount();
	}
	return -1;
}

int32 TPLTreeModel::GetParentUID(const int32& uid) const
{
	TPLDataNode pNode;

	if(dc.isExist(uid, pNode))
	{
		if(pNode.getParentId()==-2)
			return 0; //kInvalidUID;
		return pNode.getParentId();
	}
	return 0; //kInvalidUID;
}


int32 TPLTreeModel::GetChildIndexFor(const int32& parentUID, const int32& childUID) const
{
	TPLDataNode pNode;
	int32 retval=-1;
	
	if(dc.isExist(childUID, pNode))
		return pNode.getSequence();
	return -1;
}


int32 TPLTreeModel::GetNthChildUID(const int32& parentUID, const int32& index) const
{
	TPLDataNode pNode;
	int32 retval=-1;

	if(dc.isExist(parentUID, index, pNode))
		return pNode.getPubId();
	return 0; // kInvalidUID;
}


int32 TPLTreeModel::GetNthRootChild(const int32& index) const
{
	TPLDataNode pNode;
	if(dc.isExist(root, index, pNode))
	{
		return pNode.getPubId();
	}
	return 0; //kInvalidUID;
}

int32 TPLTreeModel::GetIndexForRootChild(const int32& uid) const
{
	TPLDataNode pNode;
	if(dc.isExist(uid, pNode))
		return pNode.getSequence();
	return -1;
}

PMString TPLTreeModel::ToString(const int32& uid, int32 *Rowno) const
{
	TPLDataNode pNode;
	PMString name("");

	if(dc.isExist(uid, pNode))
	{	
		*Rowno= pNode.getSequence();
		//*Rowno = pNode.getHitCount();
		/*PMString ASD("getHitCount in ToolString : ");
		ASD.AppendNumber(pNode.getHitCount());
		CA(ASD);*/
		return pNode.getName();
	}
	return name;
}


int32 TPLTreeModel::GetNodePathLengthFromRoot(const int32& uid) 
{
	TPLDataNode pNode;
	if(uid==root)
		return 0;
	if(dc.isExist(uid, pNode))
		return (1);
	return -1;
}
