//========================================================================================
//  
//  $File: //depot/indesign_4.0/gm/source/sdksamples/wlistboxcomposite/WLBCmpEyeballObserver.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2005/03/08 13:31:35 $
//  
//  $Revision: #1 $
//  
//  $Change: 323509 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Implementation includes
#include "WidgetID.h"

// Interface includes
#include "ISubject.h"
#include "ITriStateControlData.h"
// Implem includes
#include "CAlert.h"
#include "CObserver.h"
#include "CMMID.h"


/**
	Observes the "eyeball" widget.
	
	@ingroup wlistboxcomposite
	
*/
class CMMMTemplateFileSelectButtonObserver : public CObserver
{
public:
	
	/**
		Constructor for WLBListBoxObserver class.
		@param interface ptr from boss object on which this interface is aggregated.
	*/
	CMMMTemplateFileSelectButtonObserver(IPMUnknown *boss);

	/**
		Destructor for WLBCmpEyeballObserver class - 
		performs cleanup 
	*/	
	~CMMMTemplateFileSelectButtonObserver();

	/**
		AutoAttach is only called for registered observers
		of widgets.  This method is called by the application
		core when the widget is shown.
	
	*/	
	virtual void AutoAttach();

	/**
		AutoDetach is only called for registered observers
		of widgets. Called when widget hidden.
	*/	
	virtual void AutoDetach();

	/**
		Update when there's been a change on the eyeball widget.
	
		@param theChange this is specified by the agent of change; it can be the class ID of the agent,
		or it may be some specialised message ID.
		@param theSubject this provides a reference to the object which has changed; in this case, the button
		widget boss object that is being observed.
		@param protocol the protocol along which the change occurred.
		@param changedBy this can be used to provide additional information about the change or a reference
		to the boss object that caused the change.
	*/	
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);

private:
	/**
		Helper method to change state of list-box, dependent on whether
		it is being shown or hidden.
		@param isAttaching specifies whether attaching (shown) or detaching (hidden)
	*/
	void updateListBox(bool16 isAttaching);

	
};

CREATE_PMINTERFACE(CMMMTemplateFileSelectButtonObserver, kCMMTemplateFileRollOverIconButtonObserverImpl)


CMMMTemplateFileSelectButtonObserver::CMMMTemplateFileSelectButtonObserver(IPMUnknown* boss)
: CObserver(boss)
{
	
}


CMMMTemplateFileSelectButtonObserver::~CMMMTemplateFileSelectButtonObserver()
{
	
}


void CMMMTemplateFileSelectButtonObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ITRISTATECONTROLDATA);
	}
	
}


void CMMMTemplateFileSelectButtonObserver::AutoDetach()
{
		InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this,IID_ITRISTATECONTROLDATA);
	}
}


void CMMMTemplateFileSelectButtonObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	if(theChange == kTrueStateMessage) {
		// Then the button has been activated.
		do {
				PMString dbgInfoString("WLBCmpEyeballObserver::Update() ");
				dbgInfoString.SetTranslatable(kFalse);	// only for debug- not real code
				CAlert::InformationAlert(dbgInfoString);
		
		} while(0);
	}
}




