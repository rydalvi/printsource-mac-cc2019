#include "VCPlugInHeaders.h"//This File should be included first and after that remaining header files be included
#include "IContentSprayer.h"
#include "CPMUnknown.h"
#include "CMMID.h"
#include "CAlert.h"
#include "IApplication.h"
#include "PaletteRefUtils.h"
#include "IPanelMgr.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "MediatorClass.h"
#include "IDropDownListController.h"
#include "IStringListControlData.h"
#include "SectionData.h"
#include "IClientOptions.h"
#include "CMMDialogController.h"
#include "ICMMInterface.h"
#include "CMMID.h"




#define CA(X)		CAlert::InformationAlert(X)

class CMMInterface :public CPMUnknown<ICMMInterface>
{
public:
    CMMInterface(IPMUnknown*);
    ~CMMInterface();

    void setEventInCMM(const double eventId, const double languageId);
};


CREATE_PMINTERFACE( CMMInterface, kCMMInterfaceImpl)

CMMInterface::CMMInterface(IPMUnknown* boss):CPMUnknown<ICMMInterface>(boss)
{}
CMMInterface::~CMMInterface()
{}


void CMMInterface::setEventInCMM(const double eventId, const double languageId)
{
	//CA(__FUNCTION__);
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return ;
	}
    CMMDialogController cmmDialogControllerObj(this);
    
    cmmDialogControllerObj.populatePublicationDropDownList( eventId, languageId);
    
}

