#ifndef __CMMDIALOGCONTROLLER_H__
#define __CMMDIALOGCONTROLLER_H__

#include "CDialogController.h"
// Interface includes:
#include "ISession.h"
#include "IActiveContext.h"
#include "IPanelControlData.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"

class CMMDialogController : public CDialogController
{
public:
    /** Constructor.
     @param boss interface ptr from boss object on which this interface is aggregated.
     */
    CMMDialogController(IPMUnknown* boss) : CDialogController(boss) {}
    
    /** Destructor.
     */
    virtual ~CMMDialogController() {}
    
    /** Initialize each widget in the dialog with its default value.
     Called when the dialog is opened.
     */
    virtual void InitializeDialogFields(IActiveContext* dlgContext);
    
    /** Validate the values in the widgets.
     By default, the widget with ID kOKButtonWidgetID causes
     ValidateFields to be called. When all widgets are valid,
     ApplyFields will be called.
     @return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.
     
     */
    virtual WidgetID ValidateDialogFields(IActiveContext* myContext);
    
    
    /** Retrieve the values from the widgets and act on them.
     @param widgetId identifies the widget on which to act.
     */
    virtual void ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId);
    //void updateListBox(bool16 isAttaching);
    
    virtual void DialogClosing();
    
    void populateSectionDropDownList(double rootId);
    void populatePublicationDropDownList(double defPubId, double SelectedLocaleID);
    
};
#endif