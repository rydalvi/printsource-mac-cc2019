//========================================================================================
//  
//  $File: $
//  
//  Owner: Catsy
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"
#include  "CMMDialogObserver.h"
#include "IDialogMgr.h"

// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"

#include "IWidgetParent.h" 
#include "ILayerUtils.h"
#include "IDocumentLayer.h"
#include "ProgressBar.h"
#include "ITextFrameColumn.h"

#include "IImportExportFacade.h"
#include "IURIUtils.h"


#include "IImportResourceCmdData.h"
#include "URI.h"
#include "IApplyMasterSignalData.h"
#include "IXMLUtils.h"
#include "IXMLReferenceData.h"
#include "CTBID.h"
#include "ICategoryBrowser.h"
#include "IPanelMgr.h"

//class RangeProgressBar;

extern ITagReader* itagReader;
extern IAppFramework* ptrIAppFramework;
extern ISpecialChar* iConverter;
extern IClientOptions* ptrIClientOptions;
extern IDataSprayer* DataSprayerPtr;


// Constants
const PlatformChar UNIXDELCHAR('/');
const PlatformChar MACDELCHAR(':');
const PlatformChar WINDELCHAR('\\');


CatPlanningDataList CPDataList; //7-may

VectorPubModelPtr pVectorPubModel = NULL;
VectorPubModelPtr pVectorPubModelForLevel4 = NULL;
VectorPubModelPtr pVectorPubModelForLevel5 = NULL;

 // Constants

#include "CAlert.h"

void CAMessage(const PMString & fileName,const PMString & functionName,const PMString & message,int32 line);
inline PMString numToPMString(int32 line)
{
	PMString num;
	num.AppendNumber(line);
	return num;
}

//#define CA(X) CAlert::InformationAlert(X);
	
#define CA(X) \
	CAlert::InformationAlert(PMString("CMMDialogObserver.cpp") + \
							PMString("\n") + \
							PMString(__FUNCTION__) +  \
							PMString("\n") + \
							numToPMString(__LINE__) + \
							PMString("\n") + \
							PMString("Message : ") + \
							X)


/** Implements IObserver based on the partial implementation CDialogObserver.

	
	@ingroup apjs10_createmediamaster
*/
//CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

//forward declarations
void DoCMMSpraySettingsDialog();//defined in CMMActionComponent.cpp
void fillpNodeDataList(double subSectionID, double sectionID, double publicationID,PMString subSectionName);//defined in CMMDialogObserver.cpp
void startCreatingMedia();//defined in startCreatingMedia();	
void startCreatingMediaForSpread();
void filterTheProductListAccordingToPageNo();
void fillpNodeDataListForSpread(double curSelSubecId, double sectionID, double publicationID,PMString subSectionName);
//27July.new approach to avoid crashing.
bool16 startCreatingMediaNew();
bool16 copyStencilsFromTheTemplateDocumentIntoScrapData(PMString & templateFilePath,InterfacePtr<IPageItemScrapData> & scrapData);
bool16 pasteTheItemsFromScrapDataOntoOpenDocument(InterfacePtr<IPageItemScrapData> & scrapData,UIDRef &documentDocUIDRef );

//Template..master..copy template..25July
 bool16 ScriptingVersionOfCopySelectedItems(PMString & filePathItemsToBeCopiedFrom,PMString & filePathItemsToBePastedIn,PMString & fileToBeSavedAs);
//30-april
void fillpNodeDataListForWhite();//defined in CMMDialogObserver.cpp 
//30-april
//6Jun
ErrorCode addDocumentsToBook(K2Vector<IDFile> & contentFileList);
//// Chetan -- 29-09
void startCreatingMediaNewForWBManualSpray();
//// Chetan -- 29-09
///////////////////////////////////
bool16 createNewLayer();
void createCommentsOnLayer();
//-void createRectangle(PMRect box,int32 layerNo,UIDRef& frameUIDRef ,int32 maxSpread, int32 pageWidth,APpubComment objAPpubComment);
void createOval(PMRect box,int32 layerNo,IDocument* document,UIDRef& frameUIDRef);
void createArrowGraphic(PMRect box,int32 layerNo,IDocument* document,UIDRef& frameUIDRef,PMPathPointList& pathPointList);
bool16 SelectFrame(const UIDRef& frameUIDRef);
void moveCreatedFrame(UIDRef frameUIDRef,PMRect box,bool16 isFromImport = kFalse);
bool16 getMaxLimitsOfBoxes(UIDList  TempUidList, PMRect& maxBounds,int32 halfOfPageHeight);
int32 getNoOfPagesfromCurrentSread();
void getRelativeMoveAmountOfxAndy(int32 &X,int32 &Y,int32 numberOfPagesInCurrentSpread,int32 CurrentX,int32 CurrentY,int32 pageWidth,int32 sideOfPage);
void addLayerIfNeeded();
bool16 convertBoxToTextBox(UIDRef& boxUIDRef);
void addTagToGraphicFrame(UIDRef& curBox);
XMLReference TagFrameElement(const XMLReference& newElementParent,UID frameUID,PMString& frameTagName);
void addTagToText(UIDRef& textFrameUIDRef,ITextModel* txtModel,PMString& displyName);
XMLReference tagFrameElement(const XMLReference& newElementParent,UID& frameUID,const PMString& frameTagName);
void attachAttributes(XMLReference* newTag,bool16 flag);
bool16 ImportFileInFrame(const UIDRef& imageBox, const PMString& fromPath);
void fitImageInBox(const UIDRef& boxUIDRef);
void AddOrDeleteSpreads(int32 maxSpreadNumber);
int32 getNoOfPagesfromSpreadNumber(int32 index);
//-void createRectangle(PMRect box,APpubComment objAPpubComment,UIDRef& frameUIDRef,int32 pageWidth);
bool16 copyStencilsFromTheTemplateDocumentIntoScrapData(PMString & templateFilePath,InterfacePtr<IPageItemScrapData> & scrapData,UIDList &itemList);
bool16 pasteTheItemsFromScrapDataOntoOpenDocument(InterfacePtr<IPageItemScrapData> & scrapData,UIDRef &documentDocUIDRef ,UIDRef &layerRef);
PMString prepareTagName(PMString name);
PMString keepOnlyAlphaNumeric(PMString name);
bool16 fileExists(PMString& path, PMString& name);
void resizeTextFrame(UIDRef itemRef,UIDList list, PMReal bottomX, PMReal bottomY,PMReal halfOfPageHeight,int32 topOfPage);
//-void createTableFrame(PMRect box,int32 layerNo,UIDRef& frameUIDRef, int32 spreadNumber ,int32 ObjectID, int32 tableTypeID ,APpubComment objAPpubComment);
ErrorCode CreateTable(const UIDRef& storyRef, const TextIndex at,const int32 numRows,const int32 numCols,const PMReal rowHeight,const PMReal colWidth,const CellType cellType = kTextContentType);
bool16 isTablePresent(const UIDRef&, UIDRef&, int32 tableNumber=1);

void startCreatingMediaForPriceBook();  //For Price Book on 27/12/07

//-bool16 ImportIndsFileInDocument(IDocument* frontDocument,  PMString IndsFilePath, PMRect Box, APpubComment& pubCommentObj, PMReal halfOfPageHeight,int32 topOfPage );

void AddOrDeleteSpreadsForWBPageBased(int32 SpreadCount, int32 PageCount, bool16 isLeftHandOddPageFlag);
void startCreatingMediaNewForWBManualSprayForPAGE_BASED();

void createCommentsOnLayerForWBPageBased(int32 currentSpreadNo,int32 currentPageNo);
bool16 startCreatingMediaForPriceBookNew();

///functions addded for spec Sheet
bool16 startCreatingMediaForSpecSheet();
void DoCMMExportOptionsDialog();

void changeStaticText(bool16 isItem);
///////////////////////////////////
//Global variables
//Note: Following variables are used only to use the existing subSectionSpraying functionality with
//minimum change.Ideally these variables should be stored inside a MediatorClass.
//9May PublicationNodeList pNodeDataList; 
CreateMediaPublicationNodeList pNodeDataList; 
double CurrentSelectedSection;
double CurrentSelectedPublicationID;
double CurrentSelectedSubSection;
int32 global_project_level ;
double global_lang_id ;
int no_of_lstboxElements=0;
int32 currentSelectedSpreadCount; //chetan
vector<WidgetID> vWidgetID;  ////  Chetan -- 28-09
//-extern vector<APpubComment> apPubCommentsBySpreadList;
int32 objIndex = 0; // -- Chetan
//-VectorAPpubCommentValue global_VectorAPpubCommentValue;//all comments are stored in this.
PMString folderPath;
int32 currentSpread;
int32 currentSectionIndex = 0;
int32 pageNumber  = 1; ///1 or 2 per spread
int32 previousSpreadNumber = 0;


/////////////////////////////////////////////////////////////////////
StencilFileInfoList stencilFileInfo;

extern vector<UID> pageUidList;
extern bool16 allProductSprayed;
extern int32 PageCount;

UIDList sectionStencilUIDListToDelete;	/// This is used to delete the section stencil When it is spray two times unnecessary
////////////////////////////////////////////////////////////////////
extern vectorCSprayStencilInfo CSprayStencilInfoVector;


/** Implements IObserver based on the partial implementation CDialogObserver.

	
	@ingroup createmediamaster
*/

//bool16 	closeTheDocument(UIDRef & docUIDRef);
//class CMMDialogObserver : public CDialogObserver
//{
//	public:
//		/**
//			Constructor.
//			@param boss interface ptr from boss object on which this interface is aggregated.
//		*/
//		CMMDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}
//
//		/** Destructor. */
//		virtual ~CMMDialogObserver() {}
//
//		/**
//			Called by the application to allow the observer to attach to the subjects
//			to be observed, in this case the dialog's info button widget. If you want
//			to observe other widgets on the dialog you could add them here.
//		*/
//		virtual void AutoAttach();
//
//		/** Called by the application to allow the observer to detach from the subjects being observed. */
//		virtual void AutoDetach();
//
//		/**
//			Called by the host when the observed object changes, in this case when
//			the dialog's info button is clicked.
//			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
//			@param theSubject points to the ISubject interface for the subject that has changed.
//			@param protocol specifies the ID of the changed interface on the subject boss.
//			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
//		*/
//		virtual void Update
//		(
//			const ClassID& theChange,
//			ISubject* theSubject,
//			const PMIID& protocol,
//			void* changedBy
//		);
//		void populatePublicationDropDownList(int32 SelectedLocaleID);
//		void populateSubSectionList(bool16 shall_I_Fill_Data_Into_ListBox,int32 rootId);
//
//		//1Nov...
//		void populateSubSectionListForSpread(bool16 shall_I_Fill_Data_Into_ListBox,int32 rootId);
//		//end 1Nov..
////17-april
//		void populateSubSectionListForWhiteboard(bool16 shall_I_Fill_Data_Into_ListBox,int32 rootId);
////17-april
//
//		//26-Dec
//		void populateSubSectionListForPriceBook(bool16 shall_I_Fill_Data_Into_ListBox,int32 rootId);
//
//};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(CMMDialogObserver, kCMMDialogObserverImpl)

/* AutoAttach
*/
void CMMDialogObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) 
		{
			break;
		}
		// Attach to other widgets you want to handle dynamically here.
		//widgets from kCMMFirstGroupPanelWidgetID
		//30Oct..AttachToWidget(kSelectCommonMasterForAllPublicationRadioWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		//30Oct..AttachToWidget(kSelectSeparateMasterForEachPublicationRadioWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kFileBrowseDialogButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		//30Oct..AttachToWidget(kAllSectionsCommonTemplateFileButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kOpenNextDialogButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kFirstGroupPanelCancelButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kPublicationDropDownWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);

		AttachToWidget(kFGPOneDocPerPageRadioWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kCMMSpecSheetRadioWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		
		//30Oct..AttachToWidget(kFirstGroupPanelBackButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		
		//widgets from kCMMSecondGroupPanelWidgetID
		AttachToWidget(kBackButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kSecondCancelButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kCreateMediaButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kInvokePublicationSelectionDialogWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
	/*	AttachToWidget(kSectionSelectionDropDownListWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		AttachToWidget(kSectionSelectionDropDownListLevel4WidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		AttachToWidget(kSectionSelectionDropDownListLevel5WidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
	*/	
		//30Oct..No use of CreateMediaButton on FirstGroupPanel
		//AttachToWidget(kFirstGroupPanelCreateMediaButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		
		//widgets from zerothgroup panel
		//30Oct..zerothGroupPanelWidget is to be removed.
		/*
		AttachToWidget(kRadioCreateMediaBySectionWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kRadioCreateMediaBySpreadWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kSpreadSelectionCheckBoxWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kZerothCancelButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		AttachToWidget(kZerothNextButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		AttachToWidget(kPageSelectionDropDownWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		*/
		//end 30Oct..
		
		//added on 6Jun
		AttachToWidget(kSelectAllCheckBoxWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
        AttachToWidget(kCMMShowPubTreeWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
        
		
		//ended on 6Jun

		InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
		if(dialogController == nil)
		{
			CA("dialogController == nil");
			break;
		}
		/* 30Oct..
		//Following are the widgets from the zerothGroupPanel and zerothGroupPanel is to be removed.
		dialogController->SetTriStateControlData(kRadioCreateMediaBySectionWidgetID,kTrue);
		MediatorClass ::radioCreateMediaBySectionWidgetSelected = kTrue;
		dialogController->SetTriStateControlData(kSpreadSelectionCheckBoxWidgetID,kFalse);
		MediatorClass ::IskeepSpreadsTogether = kFalse;

		InterfacePtr<IStringListControlData> pageOrderDropListData(
		dialogController->QueryListControlDataInterface(kPageSelectionDropDownWidgetID));
		if(pageOrderDropListData == nil)
		{
			CA("pageOrderDropListData nil");
			break;
		}
		InterfacePtr<IDropDownListController> pageOrderDropListController(
		pageOrderDropListData,UseDefaultIID());
		if(pageOrderDropListController == nil)
		{
			CA("pageOrderDropListController nil");
			break;
		}
		pageOrderDropListController->Select(0);
		MediatorClass :: pageOrderSelectedIndex = 0;
		//end 30Oct..
		*/
		
		

	} while (kFalse);
}

/* AutoDetach
*/
void CMMDialogObserver::AutoDetach()
{
	
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
		// Detach from other widgets you handle dynamically here.

		//30Oct..DetachFromWidget(kAllSectionsCommonTemplateFileButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kFirstGroupPanelCancelButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kOpenNextDialogButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kPublicationDropDownWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		DetachFromWidget(kFGPOneDocPerPageRadioWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);

		DetachFromWidget(kCMMSpecSheetRadioWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		//DetachFromWidget(kFirstCancelButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		//8th May 06
		//30Oct..DetachFromWidget(kFirstGroupPanelBackButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		//widgets from kCMMSecondGroupPanelWidgetID
		DetachFromWidget(kBackButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kSecondCancelButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kCreateMediaButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kInvokePublicationSelectionDialogWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		/*DetachFromWidget(kSectionSelectionDropDownListWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		DetachFromWidget(kSectionSelectionDropDownListLevel4WidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		DetachFromWidget(kSectionSelectionDropDownListLevel5WidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		*/
		//30Oct..No use of CreateMediaButton on FirstGroupPanel
		//DetachFromWidget(kFirstGroupPanelCreateMediaButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		
		//widgets from zerothgroup panel
		//30Oct..
		/*
		//zerothGroupPanelWidget is to be removed.
		DetachFromWidget(kRadioCreateMediaBySectionWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kRadioCreateMediaBySpreadWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kSpreadSelectionCheckBoxWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kZerothCancelButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kZerothNextButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kPageSelectionDropDownWidgetID,IID_ISTRINGLISTCONTROLDATA,panelControlData);
		//end 30Oct..
		*/

		//added on 6Jun
		DetachFromWidget(kSelectAllCheckBoxWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
        DetachFromWidget(kCMMShowPubTreeWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		
		
		//ended on 6Jun



	} while (kFalse);
}

/* Update
*/
void CMMDialogObserver::Update
(
	const ClassID& theChange,
	ISubject* theSubject,
	const PMIID& protocol,
	void* changedBy
)
{
	
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	//CDialogObserver::Update(theChange, theSubject, protocol, changedBy);
	do
	{
		InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
		if(dialogController == nil)
		{
			//CA("dialogController == nil");
			break;
		}
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
/////////////////////////////////
		PMString templateFolderPath("Whiteboard Media  :  ");
		templateFolderPath.SetTranslatable(kFalse);
		//templateFolderPath.Append(ptrIAppFramework->getCatalogPlanningTemplateFolderPath());//
		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			ptrIAppFramework->LogDebug("AP46_RefreshContent::Refresh::doRefresh::Interface for IClientOptions not found.");
			break;
		}		
		templateFolderPath.Append(ptrIClientOptions->getWhiteBoardMediaPath());//
		
		IControlView * folderPathtextView = panelControlData->FindWidget(kFolderPathWidgetID);
		if(folderPathtextView == nil)
		{
			//CA("folderPathtextView == nil");
			break;
		}
		//IControlView * SectionSelectionDropDownListView= panelControlData->FindWidget(kSectionSelectionDropDownListWidgetID);
		//if(SectionSelectionDropDownListView == nil)
		//{
		//	//CA("folderPathtextView == nil");
		//	break;
		//}
		InterfacePtr<ITextControlData> iTextControlData(folderPathtextView,UseDefaultIID());
		if(iTextControlData == nil)
		{
			
			//CA("iTextControlData == nil");
			break;
		}		
		iTextControlData->SetString(templateFolderPath);

		IControlView * firstSeparatorCtrlView = panelControlData->FindWidget(kSeparatorOnGroupPanel5WidgetID);
		if(!firstSeparatorCtrlView)
		{
			//CA("!firstSeparatorCtrlView");
			break;
		}
		IControlView * textForProCtrlView = panelControlData->FindWidget(kProductTemplateFileNameOnGroupPanelWidgetID);
		if(!textForProCtrlView)
		{
			//CA("!textForProCtrlView");
			break;
		}

		IControlView * textForSpecSheetStencilCtrlView = panelControlData->FindWidget(kTemplateFileNameOnGroupPanelForSpecSheetWidgetID);
		if(!textForSpecSheetStencilCtrlView)
		{
			//CA("!textForSpecSheetStencilCtrlView");
			break;
		}
		


		IControlView * secondSeparatorCtrlView = panelControlData->FindWidget(kSeparatorOnGrouPanel2WidgetID);
		if(!secondSeparatorCtrlView)
		{
			//CA("!secondSeparatorCtrlView");
			break;
		}
		IControlView * textForItemCtrlView = panelControlData->FindWidget(kItemTemplateFileNameOnGroupPanelWidgetID);
		if(!textForItemCtrlView)
		{
			//CA("!textForItemCtrlView");
			break;
		}
		IControlView * thirdSeparatorCtrlView = panelControlData->FindWidget(kSeparatorOnGrouPanel4WidgetID);
		if(!thirdSeparatorCtrlView)
		{
			//CA("!thirdSeparatorCtrlView");
			break;
		}
        //Vivekanand
		//IControlView * textForHybridCtrlView = panelControlData->FindWidget(kHybridTemplateFileNameOnGroupPanelWidgetID);
		//if(!textForHybridCtrlView)
		//{
		//	//CA("!textForHybridCtrlView");
		//	break;
		//}
		//IControlView * fourthSeparatorCtrlView = panelControlData->FindWidget(kSeparatorOnGrouPanel5WidgetID);
		//if(!fourthSeparatorCtrlView)
		//{
		//	//CA("!fourthSeparatorCtrlView");
		//	break;
		//}
		//Vivekanand11Nov2010
		IControlView * textForFlowCtrlView = panelControlData->FindWidget(kFlowFileNameOnGroupPanelWidgetID);
		if(!textForFlowCtrlView)
		{
			//CA("!textForFlowCtrlView");
			break;
		}

		IControlView * textForProdForManualCtrlView = panelControlData->FindWidget(kProductTemplateFileNameOnGroupPanelForManualWidgetID);
		if(!textForProdForManualCtrlView)
		{
			//CA("!textForProdForManualCtrlView");
			break;
		}
		IControlView * secondSeparatorForManualCtrlView = panelControlData->FindWidget(kSeparatorOnGrouPanel2ForManualWidgetID);
		if(!secondSeparatorForManualCtrlView)
		{
			//CA("!secondSeparatorForManualCtrlView");
			break;
		}
		IControlView * textForItemForManualCtrlView = panelControlData->FindWidget(kItemTemplateFileNameOnGroupPanelForManualWidgetID);
		if(!textForItemForManualCtrlView)
		{
			//CA("!textForItemForManualCtrlView");
			break;
		}

		/*IControlView * textForSectionCtrlView = panelControlData->FindWidget(kSectionTemplateFileNameOnGroupPanelWidgetID);
		if(!textForSectionCtrlView)
		{
			//CA("!textForSectionCtrlView");
			break;
		}*/
		//IControlView * fifthSeparatorCtrlView = panelControlData->FindWidget(kSeparatorOnGrouPanel7WidgetID);
		//if(!fifthSeparatorCtrlView)
		//{
		//	//CA("!fifthSeparatorCtrlView");
		//	break;
		//}
		IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kCMMFirstGroupPanelWidgetID);
		if(firstGroupPanelControlData == nil)
		{
			//CA("firstGroupPanelControlData == nil");
			break;
		}
		IControlView * secondGroupPanelControlData = panelControlData->FindWidget(kCMMSecondGroupPanelWidgetID);
		if(secondGroupPanelControlData == nil)
		{
			//CA("secondGroupPanelControlData == nil");
			break;
		}

/////////////////////////////////
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		ASSERT(controlView);
		if(!controlView) {
			break;
		}
		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();
		//first group panel widget
		if((theSelectedWidget == kPublicationDropDownWidgetID)&&(theChange == kPopupChangeStateMessage))
		{
         /*-
			IControlView * openNextDialogButtonControlView = panelControlData->FindWidget(kOpenNextDialogButtonWidgetID);
			if(openNextDialogButtonControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::Update::openNextDialogButtonControlView == nil");
				break;
			}
          -*/
            IControlView * CreateMediaButtonControlView = panelControlData->FindWidget(kCreateMediaButtonWidgetID);
            if(CreateMediaButtonControlView == nil)
            {
                //CA("CreateMediaButtonControlView == nil");
                break;
            }
//////////////////////////////////////////////////////////////20-jan start
			IControlView * firstGroupClusterPanelControlData = panelControlData->FindWidget(kCMMFirstGroupPanelClusterPanelWidgetID);
			if(firstGroupClusterPanelControlData == nil)
			{
				//CA("firstGroupClusterPanelControlData == nil");
				break;
			}
	

			IControlView * PlaceItemsOrProductsByPageAssignmentCheckBoxPanelControlData = panelControlData->FindWidget(kFGPPlaceItemsOrProductsByPageAssignmentCheckBoxWidgetID);
			if(PlaceItemsOrProductsByPageAssignmentCheckBoxPanelControlData == nil)
			{
				//CA("PlaceItemsOrProductsByPageAssignmentCheckBoxPanelControlData == nil");
				break;
			}
			
			IControlView * textWidgetCntrlView = panelControlData->FindWidget(kTextFirstPageWidgetID);
			if(textWidgetCntrlView == nil)
			{
				//CA("textWidgetCntrlView == nil");
				break;
			}
			
			IControlView * dropDownSelectionCntrlView = panelControlData->FindWidget(kFGPPageOrderSelectionDropDownWidgetID);
			if(dropDownSelectionCntrlView == nil)
			{
				//CA("dropDownSelectionCntrlView == nil");
				break;
			}
			
			IControlView * keepSpreadsTogetherCheckBoxCntrlView = panelControlData->FindWidget(kFGPKeepSpreadsTogetherCheckBoxWidgetID);
			if(keepSpreadsTogetherCheckBoxCntrlView == nil)
			{
				//CA("keepSpreadsTogetherCheckBoxCntrlView == nil");
				break;
			}
///////////////////////////////////////////////////20-jan end

			//30Oct..No use of CreateMediaButton on FirstGroupPanel
			/*
			IControlView * firstGroupPanelCreateMediaButtonControlView = panelControlData->FindWidget(kFirstGroupPanelCreateMediaButtonWidgetID);
			if(firstGroupPanelCreateMediaButtonControlView == nil)
			{
				CA("firstGroupPanelCreateMediaButtonControlView == nil");
				break;
			}
			//end 30Oct..
			*/
			
			IControlView * fileBrowseDialogButtonControlView = panelControlData->FindWidget(kFileBrowseDialogButtonWidgetID);
			if(fileBrowseDialogButtonControlView == nil)
			{
				CA("fileBrowseDialogButtonControlView == nil");
				break;
			}
            /*
			IControlView * allSectionsCommonTemplateFileButtonControlView = panelControlData->FindWidget(kAllSectionsCommonTemplateFileButtonWidgetID);
			if(allSectionsCommonTemplateFileButtonControlView == nil)
			{
				CA("allSectionsCommonTemplateFileButtonControlView == nil");
				break;
			}
			
			IControlView * templateFileTextEditBoxWidgetControlView = panelControlData->FindWidget(kTemplateFileTextEditBoxWidgetID);
			if(templateFileTextEditBoxWidgetControlView == nil)
			{
				CA("templateFileTextEditBoxWidgetControlView == nil");
				break;
			}
			//end comment 28Oct.
			*/

			InterfacePtr<IDropDownListController>dropDownListController (controlView,UseDefaultIID());
			if(dropDownListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::Update::dropDownListController == nil");
				break;
			}
			int32 dropDownIndexSelected =  dropDownListController->GetSelected(); 
			
			MediatorClass ::publicationDropDownIndexSelected = dropDownIndexSelected;

			InterfacePtr<IDialogController>dialogController(this,UseDefaultIID());
			if(dialogController == nil)
			{
				//CA("dialogController == nil");
				break;
			}
			ITriStateControlData :: TriState state = dialogController->GetTriStateControlData (kSelectSeparateMasterForEachPublicationRadioWidgetID);
			
			if(dropDownIndexSelected <= 0) 
			{
				// Commented on 28 Oct..Enhancement in CreateMedia refere file CreateMediaDoubts.doc in D:\27OctcreateMediaEnhancements
				//Disable CreateMediaButton	
				//this button is removed // firstGroupPanelCreateMediaButtonControlView->Disable();
				//-openNextDialogButtonControlView->Disable();
                CreateMediaButtonControlView->Disable();
				firstGroupClusterPanelControlData->Disable();	/*20-jan*/
				PlaceItemsOrProductsByPageAssignmentCheckBoxPanelControlData->Disable();  /*20-jan*/
				textWidgetCntrlView->Disable();  /*20-jan*/
				dropDownSelectionCntrlView->Disable();  /*20-jan*/
				keepSpreadsTogetherCheckBoxCntrlView->Disable();  /*20-jan*/
				//this button is removed //templateFileTextEditBoxWidgetControlView->Disable();
                fileBrowseDialogButtonControlView->Disable();
				//end comment 28Oct.
				break;
				
			}
			else
			{
				//-openNextDialogButtonControlView->Enable();
                CreateMediaButtonControlView->Enable();
				firstGroupClusterPanelControlData->Enable();
				PlaceItemsOrProductsByPageAssignmentCheckBoxPanelControlData->Enable();
				textWidgetCntrlView->Enable();
				dropDownSelectionCntrlView->Enable();
				keepSpreadsTogetherCheckBoxCntrlView->Enable();  
                fileBrowseDialogButtonControlView->Enable();

                for(int32 subSectionVectorElementIndex =0;subSectionVectorElementIndex < MediatorClass :: vector_subSectionSprayerListBoxParameters.size();subSectionVectorElementIndex++)
                {
                    if(subSectionVectorElementIndex == dropDownIndexSelected -1)
                    {
                        MediatorClass::vector_subSectionSprayerListBoxParameters[subSectionVectorElementIndex].isSelected = kTrue;
                        
                        //CA(MediatorClass::vector_subSectionSprayerListBoxParameters[subSectionVectorElementIndex].displayName);
                    }
                    else
                    {
                        MediatorClass::vector_subSectionSprayerListBoxParameters[subSectionVectorElementIndex].isSelected = kFalse;
                        
                        //CA("NotSelected: " +MediatorClass::vector_subSectionSprayerListBoxParameters[subSectionVectorElementIndex].displayName);
                    }
                }
                
				//---I 65-
				IControlView * oneDocPerSpreadRadioButtonControlView = panelControlData->FindWidget(kFGPOneDocPerSpreadRadioWidgetID);
				if(oneDocPerSpreadRadioButtonControlView == nil)
				{
					//CA("oneDocPerSpreadRadioButtonControlView == nil");
					break;
				}
				IControlView * multilineText2WidgetIDControlView = panelControlData->FindWidget(kTushar2MultilineTextWidgetID);
				if(multilineText2WidgetIDControlView == nil)
				{
					//CA("multilineText2WidgetIDControlView == nil");
					break;
				}
				IControlView * cmmWhiteBoardRadioRadioButtonControlView = panelControlData->FindWidget(kCMMWhiteBoardRadioWidgetID);
				if(cmmWhiteBoardRadioRadioButtonControlView == nil)
				{
					//CA("cmmWhiteBoardRadioRadioButtonControlView == nil");
					break;
				}
				IControlView * multilineText3WidgetIDControlView = panelControlData->FindWidget(kTushar_3MultilineTextWidgetID);
				if(multilineText3WidgetIDControlView == nil)
				{
					//CA("multilineText3WidgetIDControlView == nil");
					break;
				}

				oneDocPerSpreadRadioButtonControlView->Disable();
				multilineText2WidgetIDControlView->Disable();
				//cmmWhiteBoardRadioRadioButtonControlView->Disable();
				//multilineText3WidgetIDControlView->Disable();

                

				break;
			}

			//if createmedia by section is selected
			/* Commented on 28 Oct..Enhancement in CreateMedia refere file CreateMediaDoubts.doc in D:\27OctcreateMediaEnhancements
			if(MediatorClass :: radioCreateMediaBySectionWidgetSelected == kTrue)
			{
				if((state != ITriStateControlData ::kSelected))
				{
					//3Jun firstGroupPanelCreateMediaButtonControlView->Enable();
					firstGroupPanelCreateMediaButtonControlView->Disable();
					fileBrowseDialogButtonControlView->Enable();
					allSectionsCommonTemplateFileButtonControlView->Enable();
					templateFileTextEditBoxWidgetControlView->Enable();
					//3Jun openNextDialogButtonControlView->Disable();
					openNextDialogButtonControlView->Enable();
				}
				else
				{//if createmedia by spread is selected
					firstGroupPanelCreateMediaButtonControlView->Disable();
					fileBrowseDialogButtonControlView->Disable();
					allSectionsCommonTemplateFileButtonControlView->Disable();
					templateFileTextEditBoxWidgetControlView->Disable();
					openNextDialogButtonControlView->Enable();
				}
			}
			else
			{

				firstGroupPanelCreateMediaButtonControlView->Enable();
				fileBrowseDialogButtonControlView->Enable();
				allSectionsCommonTemplateFileButtonControlView->Enable();
				templateFileTextEditBoxWidgetControlView->Enable();
				//3Jun openNextDialogButtonControlView->Disable();
				openNextDialogButtonControlView->Disable();
			}
			//comment end 28Oct.
			*/
		}
		
		else if(theSelectedWidget == kFirstGroupPanelCancelButtonWidgetID && theChange == kTrueStateMessage)
		{
			CDialogObserver::CloseDialog();
			break;
		}

        if(theSelectedWidget==kCMMShowPubTreeWidgetID && theChange==kTrueStateMessage)
        {
            //CA(" show pub clicked  ");
            bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
            if(!isUserLoggedIn)
                return;
            
            InterfacePtr<IApplication> 	iApplication(GetExecutionContextSession()->QueryApplication()); //Cs4
            if(iApplication==nil)
            {
                //CA("iApplication==nil");
                break;
            }
            InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager());
            if(iPanelMgr == nil)
            {
                //CA("iPanelMgr == nil");
                break;
            }
            UID paletteUID = kInvalidUID;
            int32 TemplateTop	=0;
            int32 TemplateLeft	=0;
            int32 TemplateRight	=0;
            int32 TemplateBottom =0;
            
            const ActionID MyPalleteActionID = kCTBPanelWidgetActionID;
            
            IControlView* pnlControlView = iPanelMgr->GetPanelFromActionID(MyPalleteActionID);
            if(pnlControlView == NULL)
            {
                //CA("pnlControlView is NULL");
                break;
            }
            
            InterfacePtr<IWidgetParent> panelWidgetParent( pnlControlView, UseDefaultIID());
            if(panelWidgetParent == NULL)
            {
                //CA("panelWidgetParent is NULL");
                break;
            }
            InterfacePtr<IWindow> palette((IWindow*)panelWidgetParent->QueryParentFor(IWindow::kDefaultIID));
            if(palette == NULL)
            {
                //CA("palette is NULL");
                break;
            }
            
            if(palette)
            {
                
                TemplateTop	=0;
                TemplateLeft = 0;
                TemplateRight =150;	
                TemplateBottom	=300;
                
                InterfacePtr<ICategoryBrowser> CatalogBrowserPtr((ICategoryBrowser*)::CreateObject(kCTBCategoryBrowserBoss, IID_ICATEGORYBROWSER));
                if(!CatalogBrowserPtr)
                {
                    ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::Update::kSPSelectClassWidgetID--No CatalogBrowserPtr");
                    return ;
                }
                CatalogBrowserPtr->OpenCategoryBrowser(TemplateTop, TemplateLeft, TemplateBottom, TemplateRight , 1 , -1);
            }
            
        }
		else if(theSelectedWidget == kFGPOneDocPerPageRadioWidgetID)
		{
			//CA("theSelectedWidget == kFGPOneDocPerPageRadioWidgetID");
			IControlView * FGPKeepSpreadsTogetherCheckBoxWidgetControlView = panelControlData->FindWidget(kFGPKeepSpreadsTogetherCheckBoxWidgetID);
			if(FGPKeepSpreadsTogetherCheckBoxWidgetControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::Update::FGPKeepSpreadsTogetherCheckBoxWidgetControlView == nil");
				break;
			}
			if(theChange == kTrueStateMessage)
			{
				FGPKeepSpreadsTogetherCheckBoxWidgetControlView->Disable();
			}
			else
			{
				FGPKeepSpreadsTogetherCheckBoxWidgetControlView->Enable();
			}
			
		}
        


		else if(theSelectedWidget == kCMMSpecSheetRadioWidgetID)
		{
			//CA("theSelectedWidget == kCMMSpecSheetRadioWidgetID");

			IControlView * FGPPlaceItemsOrProductsByPageAssignmentCheckBoxWidgetControlView = panelControlData->FindWidget(kFGPPlaceItemsOrProductsByPageAssignmentCheckBoxWidgetID);
			if(FGPPlaceItemsOrProductsByPageAssignmentCheckBoxWidgetControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::Update::FGPPlaceItemsOrProductsByPageAssignmentCheckBoxWidgetControlView == nil");
				break;
			}
			if(theChange == kTrueStateMessage)
			{
				FGPPlaceItemsOrProductsByPageAssignmentCheckBoxWidgetControlView->Disable();
			}
			else
			{
				FGPPlaceItemsOrProductsByPageAssignmentCheckBoxWidgetControlView->Enable();
			}

			IControlView * TextFirstPageWidgetControlView = panelControlData->FindWidget(kTextFirstPageWidgetID);
			if(TextFirstPageWidgetControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::Update::TextFirstPageWidgetControlView == nil");
				break;
			}
			if(theChange == kTrueStateMessage)
			{
				TextFirstPageWidgetControlView->Disable();
			}
			else
			{
				TextFirstPageWidgetControlView->Enable();
			}

			
			IControlView * FGPPageOrderSelectionDropDownWidgetControlView = panelControlData->FindWidget(kFGPPageOrderSelectionDropDownWidgetID);
			if(FGPPageOrderSelectionDropDownWidgetControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::Update::FGPPageOrderSelectionDropDownWidgetControlView == nil");
				break;
			}
			if(theChange == kTrueStateMessage)
			{
				FGPPageOrderSelectionDropDownWidgetControlView->Disable();
			}
			else
			{
				FGPPageOrderSelectionDropDownWidgetControlView->Enable();
			}

			IControlView * FGPKeepSpreadsTogetherCheckBoxWidgetControlView = panelControlData->FindWidget(kFGPKeepSpreadsTogetherCheckBoxWidgetID);
			if(FGPKeepSpreadsTogetherCheckBoxWidgetControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::Update::FGPKeepSpreadsTogetherCheckBoxWidgetControlView == nil");
				break;
			}
			if(theChange == kTrueStateMessage)
			{
				FGPKeepSpreadsTogetherCheckBoxWidgetControlView->Disable();
			}
			else
			{
				FGPKeepSpreadsTogetherCheckBoxWidgetControlView->Enable();
			}			
		}

        if((theSelectedWidget == kFileBrowseDialogButtonWidgetID)&& (theChange == kTrueStateMessage))
        {
            
            SDKFileOpenChooser FileChooseDLG;
            FileChooseDLG.SetTitle("Select InDesign Template (*.indt) File.");
            //FileChooseDLG.AddFilter(kInCopyTemplateFileTypeInfoID, "Indt file(indt)");
            //FileChooseDLG.AddFilter(kTEXTFileTypeInfoID, PMString("Text file(text)").SetTranslatable(kFalse));
            FileChooseDLG.AddAllFiles();
            
            FileChooseDLG.ShowDialog();
            PMString pathOfFile("");
            
            if(!FileChooseDLG.IsChosen())
            {
                break;
            }
            
            pathOfFile = FileChooseDLG.GetPath();
            //CA(pathOfFile);
            //3Jun
            IDFile templateFile(pathOfFile);
            SDKUtilities sdkutils;
            PMString extension = sdkutils.GetExtension(templateFile);
            //CA("extension =" + extension);
            if(extension != "indt")
            {
                CAlert::InformationAlert("Please select valid Indesign Template File.\nTemplate files have .indt extension");
                break;
            }
            //CA(extension);
            //end 3Jun
            IControlView * templateFileTextEditButtonControlView = panelControlData->FindWidget(kTemplateFileTextEditBoxWidgetID);
            if(templateFileTextEditButtonControlView == nil)
            {
                CA("templateFileTextEditButtonControlView == nil");
                break;
            }
            InterfacePtr<ITextControlData>iTextControlData(templateFileTextEditButtonControlView,UseDefaultIID());
            if(iTextControlData == nil)
            {
                CA("iTextControlData == nil");
                break;
            }
            iTextControlData->SetString(pathOfFile);
            
            //subSectionListBoxParams.ProductStencilFilePath;
            MediatorClass::vector_subSectionSprayerListBoxParameters[MediatorClass::publicationDropDownIndexSelected -1].ProductStencilFilePath = pathOfFile;

            break;
            
        }
        
		//zeroth group panel widget
		//30Oct..
		//ZerothGroupPanelWidget is to be removed.
		/*
		if((theSelectedWidget == kPageSelectionDropDownWidgetID)&&(theChange == kPopupChangeStateMessage))
		{
			//CA("zeroth dropdown");
			InterfacePtr<IDropDownListController>dropDownListController (controlView,UseDefaultIID());
			if(dropDownListController == nil)
			{
				CA("dropDownListController == nil");
				break;
			}
			int32 dropDownIndexSelected =  dropDownListController->GetSelected(); 			
			MediatorClass :: pageOrderSelectedIndex = dropDownIndexSelected;

		}
		if((theSelectedWidget == kRadioCreateMediaBySectionWidgetID) && (theChange == kTrueStateMessage))
		{
			//CA("zeroth radiosec");
			MediatorClass :: radioCreateMediaBySectionWidgetSelected = kTrue;
			IControlView * pageSelectionGroupPanelControlData = panelControlData->FindWidget(kCMMPageSelectionGroupPanelWidgetID);
			if(pageSelectionGroupPanelControlData == nil)
			{
				CA("pageSelectionGroupPanelControlData == nil");
				break;
			}
			pageSelectionGroupPanelControlData->Disable();
			
			//added on 5Jun
			//Change the caption of the FirstGroupPanelCreateMedia push button to Cancel.
			IControlView * firstGroupPanelCreateMediaButtonControlView = panelControlData->FindWidget(kFirstGroupPanelCreateMediaButtonWidgetID);
			if(firstGroupPanelCreateMediaButtonControlView == nil)
			{
				CA("firstGroupPanelCreateMediaButtonControlView == nil");
				break;
			}
			firstGroupPanelCreateMediaButtonControlView->Hide();
			IControlView * firstGroupPanelCancelButtonControlView = panelControlData->FindWidget(kFirstGroupPanelCancelButtonWidgetID);
			if(firstGroupPanelCancelButtonControlView == nil)
			{
				CA("firstGroupPanelCancelButtonControlView == nil");
				break;
			}
			firstGroupPanelCancelButtonControlView->Show();

			IControlView * firstGroupPanelNextButtonControlView = panelControlData->FindWidget(kOpenNextDialogButtonWidgetID);
			if(firstGroupPanelNextButtonControlView == nil)
			{
				CA("firstGroupPanelNextButtonControlView == nil");
				break;
			}
			firstGroupPanelNextButtonControlView->Enable();			
			
			break;
			//ended on 5Jun
		}
		if((theSelectedWidget == kRadioCreateMediaBySpreadWidgetID) && (theChange == kTrueStateMessage))
		{
			//CA("zeroth radiospread");
			MediatorClass :: radioCreateMediaBySectionWidgetSelected = kFalse;
			IControlView * pageSelectionGroupPanelControlData = panelControlData->FindWidget(kCMMPageSelectionGroupPanelWidgetID);
			if(pageSelectionGroupPanelControlData == nil)
			{
				CA("pageSelectionGroupPanelControlData == nil");
				break;
			}
			pageSelectionGroupPanelControlData->Enable();

			//Change the caption of the FirstGroupPanelCreateMedia push button to Cancel.
			IControlView * firstGroupPanelCancelButtonControlView = panelControlData->FindWidget(kFirstGroupPanelCancelButtonWidgetID);
			if(firstGroupPanelCancelButtonControlView == nil)
			{
				CA("firstGroupPanelCancelButtonControlView == nil");
				break;
			}
			firstGroupPanelCancelButtonControlView->Hide();

			IControlView * firstGroupPanelCreateMediaButtonControlView = panelControlData->FindWidget(kFirstGroupPanelCreateMediaButtonWidgetID);
			if(firstGroupPanelCreateMediaButtonControlView == nil)
			{
				CA("firstGroupPanelCreateMediaButtonControlView == nil");
				break;
			}
			firstGroupPanelCreateMediaButtonControlView->Show();

			IControlView * firstGroupPanelNextButtonControlView = panelControlData->FindWidget(kOpenNextDialogButtonWidgetID);
			if(firstGroupPanelNextButtonControlView == nil)
			{
				CA("firstGroupPanelNextButtonControlView == nil");
				break;
			}
			firstGroupPanelNextButtonControlView->Disable();
			
			
			
			//ended on 5Jun
			break;
		}
		//keep spreads together check box
		if((theSelectedWidget == kSpreadSelectionCheckBoxWidgetID))
		{
			//CA("checked");
			if(theChange == kTrueStateMessage)
			{
				//keep the spreads together
				MediatorClass :: IskeepSpreadsTogether = kTrue;
			}
			else
			{
				//don't keep the spreads together
				MediatorClass :: IskeepSpreadsTogether = kFalse;
			}
			break;
		}
		if(((theSelectedWidget == kZerothCancelButtonWidgetID)||(theSelectedWidget == kFirstGroupPanelCancelButtonWidgetID)) && (theChange == kTrueStateMessage))
		{
			//CA("checked");
			//MediatorClass :: radioCreateMediaBySectionWidgetSelected = kFalse;
			CDialogObserver::CloseDialog();
			break;
		}
		if((theSelectedWidget == kZerothNextButtonWidgetID) && (theChange == kTrueStateMessage))
		{
			//CA("checked");
			//MediatorClass :: radioCreateMediaBySectionWidgetSelected = kFalse;
			IControlView * zerothGroupPanelControlData = panelControlData->FindWidget(kCMMZerothGroupPanelWidgetID);
			if(zerothGroupPanelControlData == nil)
			{
				CA("zerothGroupPanelControlData == nil");
				break;
			}
			zerothGroupPanelControlData->Hide();

			IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kCMMFirstGroupPanelWidgetID);
			if(firstGroupPanelControlData == nil)
			{
				CA("firstGroupPanelControlData == nil");
				break;
			}
			firstGroupPanelControlData->Show();
			
			if(MediatorClass :: IsCommonMasterForAllPublicationRadioWidgetSelected)
			{				
				dialogController->SetTriStateControlData(kSelectCommonMasterForAllPublicationRadioWidgetID,kTrue);
			}
			else
			{
				dialogController->SetTriStateControlData(kSelectSeparateMasterForEachPublicationRadioWidgetID,kTrue);
			}
			//fill the section dropdown list
			//InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
			if(ptrIClientOptions==nil)
			{
				CAlert::ErrorAlert("Interface for IClientOptions not found.");
				break;
			}
	
			PMString language_name("");	
			//define global_lang_id
			MediatorClass ::global_lang_id = ptrIClientOptions->getDefaultLocale(language_name);
			populatePublicationDropDownList(MediatorClass ::global_lang_id);
			
			//spray by spread is selected
			if(MediatorClass :: radioCreateMediaBySectionWidgetSelected == kFalse)
			{
				dialogController->SetTextControlData
					(  
						kSameTemplateTextWidgetID,  
						"Select same Master file for all spreads",  
						nil,  
						kTrue,  
						kFalse 
					);
				dialogController->SetTriStateControlData(kSelectCommonMasterForAllPublicationRadioWidgetID,kTrue);
				
				IControlView * differentTemplateTextWidgetControlView = panelControlData->FindWidget(kDifferentTemplateTextWidgetID);
				if(differentTemplateTextWidgetControlView == nil)
				{
					CA("differentTemplateTextWidgetControlView == nil");
					break;
				}
				differentTemplateTextWidgetControlView->Hide();

				IControlView * selectSeparateMasterForEachPublicationRadioWidgetControlView = panelControlData->FindWidget(kSelectSeparateMasterForEachPublicationRadioWidgetID);
				if(selectSeparateMasterForEachPublicationRadioWidgetControlView == nil)
				{
					CA("selectSeparateMasterForEachPublicationRadioWidgetControlView == nil");
					break;
				}
				selectSeparateMasterForEachPublicationRadioWidgetControlView->Hide();
				

			}
			else
			{
				dialogController->SetTextControlData
					(  
						kSameTemplateTextWidgetID,  
						"Select same Master/Stencil for all sections",  
						nil,  
						kTrue,  
						kFalse 
					);
				dialogController->SetTriStateControlData(kSelectCommonMasterForAllPublicationRadioWidgetID,kTrue);

				IControlView * differentTemplateTextWidgetControlView = panelControlData->FindWidget(kDifferentTemplateTextWidgetID);
				if(differentTemplateTextWidgetControlView == nil)
				{
					CA("differentTemplateTextWidgetControlView == nil");
					break;
				}
				differentTemplateTextWidgetControlView->Show();

				IControlView * selectSeparateMasterForEachPublicationRadioWidgetControlView = panelControlData->FindWidget(kSelectSeparateMasterForEachPublicationRadioWidgetID);
				if(selectSeparateMasterForEachPublicationRadioWidgetControlView == nil)
				{
					CA("selectSeparateMasterForEachPublicationRadioWidgetControlView == nil");
					break;
				}
				selectSeparateMasterForEachPublicationRadioWidgetControlView->Show();
				
			}
			break;
		}
		//end zeroth group panel widget
		
		//ZerothGroupPanelWidget is to be removed.
		//end 30Oct..
		*/

		//8th May 06
		else if((theSelectedWidget == kFirstGroupPanelBackButtonWidgetID) && (theChange == kTrueStateMessage))
		{
			/* 30Oct..
			//ZerothGroupPanelWidget is removed.There is no point in showing it on clicking of the back button.
			//In fact this button is also to be removed.
			IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kCMMFirstGroupPanelWidgetID);
			if(firstGroupPanelControlData == nil)
			{
				CA("firstGroupPanelControlData == nil");
				break;
			}
			firstGroupPanelControlData->Hide();

			IControlView * zerothGroupPanelControlData = panelControlData->FindWidget(kCMMZerothGroupPanelWidgetID);
			if(zerothGroupPanelControlData == nil)
			{
				CA("zerothGroupPanelControlData == nil");
				break;
			}
			zerothGroupPanelControlData->Show();

			
			
			if(MediatorClass :: IsCommonMasterForAllPublicationRadioWidgetSelected)
			{				
				dialogController->SetTriStateControlData(kSelectCommonMasterForAllPublicationRadioWidgetID,kTrue);
			}
			else
			{
				dialogController->SetTriStateControlData(kSelectSeparateMasterForEachPublicationRadioWidgetID,kTrue);
			}
			//end 30Oct..
			*/

		}		
		//end 8th May 06

		//first group panel widget
		//30Oct..
		//The following radio widgets are removed from the first group panel
		/*
		if((theSelectedWidget == kSelectCommonMasterForAllPublicationRadioWidgetID) && (theChange == kTrueStateMessage))
		{
			
		
			MediatorClass :: IsCommonMasterForAllPublicationRadioWidgetSelected = kTrue;
			
			//Enable CreateMediaButton
			
			//No use of CreateMedia on firstGroupPanel
			IControlView * createMediaButtonControlView = panelControlData->FindWidget(kFirstGroupPanelCreateMediaButtonWidgetID);
			if(createMediaButtonControlView == nil)
			{
				CA("createMediaButtonControlView == nil");
				break;
			}
			//Enable the browse button
			IControlView * browseButtonControlView = panelControlData->FindWidget(kFileBrowseDialogButtonWidgetID);
			if(browseButtonControlView == nil)
			{
				CA("browseButtonControlView == nil");
				break;
			}
			IControlView * allSectionsCommonTemplateFileButtonControlView = panelControlData->FindWidget(kAllSectionsCommonTemplateFileButtonWidgetID);
			if(allSectionsCommonTemplateFileButtonControlView == nil)
			{
				CA("allSectionsCommonTemplateFileButtonControlView == nil");
				break;
			}
			//browseButtonControlView->Enable();
			//Enable the kTemplateFileTextEditBoxWidgetID button
			IControlView * templateFileTextEditButtonControlView = panelControlData->FindWidget(kTemplateFileTextEditBoxWidgetID);
			if(templateFileTextEditButtonControlView == nil)
			{
				CA("templateFileTextEditButtonControlView == nil");
				break;
			}
			//templateFileTextEditButtonControlView->Enable();
			InterfacePtr<ITextControlData>iTextControlData(templateFileTextEditButtonControlView,UseDefaultIID());
			if(iTextControlData == nil)
			{
				CA("iTextControlData == nil");
				break;
			}
			//iTextControlData->SetString("");
			//disable the next button
			IControlView * openNextDialogButtonControlView = panelControlData->FindWidget(kOpenNextDialogButtonWidgetID);
			if(openNextDialogButtonControlView == nil)
			{
				CA("openNextDialogButtonControlView == nil");
				break;
			}
			//openNextDialogButtonControlView->Disable();
			
			if(MediatorClass ::publicationDropDownIndexSelected <= 0)
			{
				break;
			}
			
			browseButtonControlView->Enable();
			allSectionsCommonTemplateFileButtonControlView->Enable();
			templateFileTextEditButtonControlView->Enable();
			iTextControlData->SetString("");			
			break;
		}
		//first group panel widget
		if((theSelectedWidget == kSelectSeparateMasterForEachPublicationRadioWidgetID) &&(theChange == kTrueStateMessage))
		{
			MediatorClass :: IsCommonMasterForAllPublicationRadioWidgetSelected = kFalse;
					
			

			//Disable the browse button
			IControlView * browseButtonControlView = panelControlData->FindWidget(kFileBrowseDialogButtonWidgetID);
			if(browseButtonControlView == nil)
			{
				CA("browseButtonControlView == nil");
				break;
			}
			browseButtonControlView->Disable();
			IControlView * allSectionsCommonTemplateFileButtonControlView = panelControlData->FindWidget(kAllSectionsCommonTemplateFileButtonWidgetID);
			if(allSectionsCommonTemplateFileButtonControlView == nil)
			{
				CA("allSectionsCommonTemplateFileButtonControlView == nil");
				break;
			}
			allSectionsCommonTemplateFileButtonControlView->Disable();
			
			//Disable the kTemplateFileTextEditBoxWidgetID button
			IControlView * templateFileTextEditButtonControlView = panelControlData->FindWidget(kTemplateFileTextEditBoxWidgetID);
			if(templateFileTextEditButtonControlView == nil)
			{
				CA("templateFileTextEditButtonControlView == nil");
				break;
			}
			templateFileTextEditButtonControlView->Disable();

			
			//Check whether valid entry is selected from the publication dropdown
			//InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
			//if(dialogController == nil)
			//{
			//	CA("dialogController == nil");
			//	break;
			//}
			//InterfacePtr<IStringListControlData> PubDropListData(
			//dialogController->QueryListControlDataInterface(kPublicationDropDownWidgetID));
			//if(PubDropListData == nil)
			//{
			//	CA("PubDropListData nil");
			//	break;
			//}
			//InterfacePtr<IDropDownListController> PubDropListController(PubDropListData,UseDefaultIID());
			//if(PubDropListController == nil)
			//{
			//	CA("PubDropListController nil");
			//	break;
			//}
			//int32 selectedIndex = PubDropListController->GetSelected();
			

			if(MediatorClass ::publicationDropDownIndexSelected <= 0)
			{
				break;
			}


		//Enable the next button
			//5Jun 8
			
			//IControlView * openNextDialogButtonControlView = panelControlData->FindWidget(kOpenNextDialogButtonWidgetID);
			//if(openNextDialogButtonControlView == nil)
			//{
			//	CA("openNextDialogButtonControlView == nil");
			//	break;
			//}
			//openNextDialogButtonControlView->Enable();
			
			//5Jun 8
			break;
		}
		//first group panel widget
		if((theSelectedWidget == kFileBrowseDialogButtonWidgetID)&& (theChange == kTrueStateMessage))
		{
			
			SDKFileOpenChooser FileChooseDLG;
			FileChooseDLG.SetTitle("Select Template File.");
			//FileChooseDLG.AddFilter(kInCopyTemplateFileTypeInfoID, "Indt file(indt)");
			//FileChooseDLG.AddFilter(kTEXTFileTypeInfoID, PMString("Text file(text)").SetTranslatable(kFalse));
			
			FileChooseDLG.ShowDialog();
			PMString pathOfFile("");

			if(!FileChooseDLG.IsChosen())
			{ 
				break;
			}

			pathOfFile = FileChooseDLG.GetPath();
			//CA(pathOfFile);
			//3Jun
			IDFile templateFile(pathOfFile);
			SDKUtilities sdkutils;
			PMString extension = sdkutils.GetExtension(templateFile);
			//CA("extension =" + extension);
			if(extension != "indt")
			{
				CAlert::InformationAlert("Please select valid IndesignCS3 Master Template File.\nTemplate files have .indt extension");
				break;
			}
			//CA(extension);
			//end 3Jun
			IControlView * templateFileTextEditButtonControlView = panelControlData->FindWidget(kTemplateFileTextEditBoxWidgetID);
			if(templateFileTextEditButtonControlView == nil)
			{
				CA("templateFileTextEditButtonControlView == nil");
				break;
			}
			InterfacePtr<ITextControlData>iTextControlData(templateFileTextEditButtonControlView,UseDefaultIID());
			if(iTextControlData == nil)
			{
				CA("iTextControlData == nil");
				break;
			}
			iTextControlData->SetString(pathOfFile);
			break;

		}
		//first group panel widget
		if((theSelectedWidget == kAllSectionsCommonTemplateFileButtonWidgetID)&& (theChange == kTrueStateMessage))
		{
			MediatorClass ::Is_UseDifferentTemplateForEachSubSectionRadioSelected = kFalse;
			//CMMActionComponent cmmActn(MediatorClass :: CMMActionComponentIPMUnknown);
			//cmmActn.
				DoCMMSpraySettingsDialog();
			break;
		}
		//end 30Oct..
		*/
		//first group panel widget
		
		else if((theSelectedWidget == kOpenNextDialogButtonWidgetID /*-|| (theSelectedWidget == kOKButtonWidgetID && firstGroupPanelControlData->IsVisible())-*/)&& theChange == kTrueStateMessage)
		{
			//CA("theSelectedWidget == kOpenNextDialogButtonWidgetID || theSelectedWidget == kOKButtonWidgetID");
			if(MediatorClass ::publicationDropDownIndexSelected <= 0)
			{
				ptrIAppFramework->LogInfo("AP46CreateMedia::CMMDialogObserver::Update::MediatorClass ::publicationDropDownIndexSelected <= 0");
				break;
			}

			//Get publicationID corresponding to the MediatorClass ::publicationDropDownIndexSelected
			double selectedPublicationID = MediatorClass ::publicationVector[MediatorClass ::publicationDropDownIndexSelected - 1];

			MediatorClass :: currentSelectedPublicationID= selectedPublicationID;

			InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
			if(dialogController == nil)
			{
				//CA("dialogController == nil");
				break;
			}

			//Hide the firstGroupPanel
			IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kCMMFirstGroupPanelWidgetID);
			if(firstGroupPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::Update::firstGroupPanelControlData == nil");
				break;
			}
			firstGroupPanelControlData->HideView();
			
			//Show the secondGroupPanel
			IControlView * secondGroupPanelControlView = panelControlData->FindWidget(kCMMSecondGroupPanelWidgetID);
			if(secondGroupPanelControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::Update::secondGroupPanelControlView == nil");
				break;
			}
			secondGroupPanelControlView->ShowView();
			dialogController->SetTriStateControlData(kSelectAllCheckBoxWidgetID,kTrue , nil, kTrue, kFalse);//2.Parameter kTrue Modified.....15-06-2007-sachin..


			ITriStateControlData :: TriState state = dialogController->GetTriStateControlData(kFGPPlaceItemsOrProductsByPageAssignmentCheckBoxWidgetID);

			if(state == ITriStateControlData ::kSelected)
			{
				//selectInfo.Append("placeItemsOrProductsByPageAssignment = kTrue\n");                 
				MediatorClass :: placeItemsOrProductsByPageAssignment = kTrue;
			}
			else
			{
				//selectInfo.Append("placeItemsOrProductsByPageAssignment = kFalse\n");
				MediatorClass :: placeItemsOrProductsByPageAssignment = kFalse;
			}

			state = dialogController->GetTriStateControlData(kFGPKeepSpreadsTogetherCheckBoxWidgetID);
			
			if(state == ITriStateControlData ::kSelected)
			{
				//selectInfo.Append("IskeepSpreadsTogether = kTrue\n");
				MediatorClass :: IskeepSpreadsTogether = kTrue;
			}
			else
			{
				//selectInfo.Append("IskeepSpreadsTogether = kFalse\n");
				//MediatorClass :: IskeepSpreadsTogether = kFalse;
				MediatorClass :: IskeepSpreadsTogether = kTrue; //By Dattatray on 28/02/2007 for testing
			}
			
			IControlView* widget = panelControlData->FindWidget( kFGPPageOrderSelectionDropDownWidgetID );
	        InterfacePtr<IDropDownListController> dropDownListController(widget, UseDefaultIID());
		//	InterfacePtr<IDropDownListController> dropDownListController(dialogController->QueryListControlDataInterface(kFGPPageOrderSelectionDropDownWidgetID)/*,UseDefaultIID()*/);
			if(dropDownListController == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::Update::dropDownListController == nil");
				break;
			}
			MediatorClass :: pageOrderSelectedIndex  = dropDownListController->GetSelected();
			//selectInfo.Append("pageOrderSelectedIndex : ");
			//selectInfo.AppendNumber(MediatorClass :: pageOrderSelectedIndex);

			//CA(selectInfo);


			WidgetID selectedRadioWidgetID = dialogController->GetSelectedClusterWidget(kCMMFirstGroupPanelClusterPanelWidgetID);
			//viv1
			//------
			//InterfacePtr<IStringListControlData> PubDropListDataName(
			//dialogController->QueryListControlDataInterface(kSectionSelectionDropDownListWidgetID));
			//if(PubDropListDataName == nil)
			//{
			//	//CA("PubDropListData nil");
			//	break;
			//}
			//PubDropListDataName->Clear(kFalse,kFalse);	

			//InterfacePtr<IDropDownListController> PubDropListNameController(PubDropListDataName,UseDefaultIID());
			//if (PubDropListNameController == nil)
			//{
			//	//CA("PubDropListController nil");
			//	break;
			//}
			//
			////viv1
			////viv1
			//InterfacePtr<IStringListControlData> PubDropListDataNameForLevel4(
			//dialogController->QueryListControlDataInterface(kSectionSelectionDropDownListLevel4WidgetID));
			//if(PubDropListDataNameForLevel4 == nil)
			//{
			//	//CA("PubDropListData nil");
			//	break;
			//}
			//PubDropListDataNameForLevel4->Clear(kFalse,kFalse);	

			//InterfacePtr<IDropDownListController> PubDropListNameControllerForLevel4(PubDropListDataNameForLevel4,UseDefaultIID());
			//if (PubDropListNameControllerForLevel4 == nil)
			//{
			//	//CA("PubDropListController nil");
			//	break;
			//}
			//
			////viv1
			////viv1
			//InterfacePtr<IStringListControlData> PubDropListDataNameForLevel5(
			//dialogController->QueryListControlDataInterface(kSectionSelectionDropDownListLevel5WidgetID));
			//if(PubDropListDataNameForLevel5 == nil)
			//{
			//	//CA("PubDropListData nil");
			//	break;
			//}
			//PubDropListDataNameForLevel5->Clear(kFalse,kFalse);	

			//InterfacePtr<IDropDownListController> PubDropListNameControllerForLevel5(PubDropListDataNameForLevel5,UseDefaultIID());
			//if (PubDropListNameControllerForLevel5 == nil)
			//{
			//	//CA("PubDropListController nil");
			//	break;
			//}
			
			//viv1

			//PMString selectInfo;
			if(selectedRadioWidgetID == kFGPOneDocPerSectionRadioWidgetID)
			{
				folderPathtextView->HideView();
				//selectInfo.Append("Radio1 Selected\n");
				firstSeparatorCtrlView->ShowView();
				textForProCtrlView->ShowView();
				textForSpecSheetStencilCtrlView->HideView();
				secondSeparatorCtrlView->ShowView();
				textForItemCtrlView->ShowView();
				thirdSeparatorCtrlView->ShowView();
				//textForHybridCtrlView->Show();
				//fourthSeparatorCtrlView->Show();
				textForFlowCtrlView->ShowView();
				//textForSectionCtrlView->Show();
				//fifthSeparatorCtrlView->Show();

				MediatorClass :: createMediaRadioOption = 1;//per section.


                MediatorClass::sectionLevel =1; //-ptrIAppFramework->ProjectCache_getLevelForSection();
				if(MediatorClass::sectionLevel == 1 || MediatorClass::sectionLevel == -1){

					//CA("hide drop down here");
					//SectionSelectionDropDownListView->Hide();
					populateSubSectionList(kTrue,selectedPublicationID);
				}
				else
				{
					//CA("Show drop Down");
					//SectionSelectionDropDownListView->Show();
					
					//pVectorPubModel = ptrIAppFramework->GetProject_getAllSectionsByPublicationIdAndLevel(selectedPublicationID,3,MediatorClass::global_lang_id);
					//if(pVectorPubModel == NULL)
					//{
					//	//CA("Inside If");
					//	break;
					//}

					////CA("End of else");
					//
					//int32 PubModelIndexSize=static_cast<int32>(pVectorPubModel->size());
					///*PMString tempstr("pVectorPubModel->size = ");
					//tempstr.AppendNumber(PubModelIndexSize);
					//CA(tempstr);*/
					//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
					//if(!iConverter)
					//{
					//	//CA("!iConverter");
					//	
					//	return;
					//}					
					//	
					//VectorHtmlTrackerPtr vectorHtmlTrackerSPTBPtr = NULL;
					//VectorHtmlTrackerValue vectorObj ;
					//vectorHtmlTrackerSPTBPtr = &vectorObj;


					////PubDropListDataName->AddString("--Select--", 0, kFalse, kFalse);
					//for(int32 PubModelIndex=0; PubModelIndex < PubModelIndexSize; PubModelIndex++)
					//{
					//	PMString PubModelName("");
					//	PubModelName=pVectorPubModel->at(PubModelIndex).getName();

					//	vectorHtmlTrackerSPTBPtr->clear();
					//	PubModelName=iConverter->translateStringNew(PubModelName, vectorHtmlTrackerSPTBPtr);
					//	//CA(PubModelName);
					//	PubDropListDataName->AddString(PubModelName, PubModelIndex, kFalse, kFalse);
					//}

					////delete pVectorPubModel;
					//PubDropListNameController->Select(0);
					
					
					
					///////////////////for level4////////////////////////////////////

					/*pVectorPubModelForLevel4 = ptrIAppFramework->GetProject_getAllSectionsByPublicationIdAndLevel(pVectorPubModel->at(0).getPublicationID(),4,MediatorClass::global_lang_id);
					if(pVectorPubModelForLevel4  == NULL)
					{
						//CA("Inside If");
						break;
					}

					//CA("End of else");
					
					int32 PubModelIndexSizeForLevel4=static_cast<int32>(pVectorPubModelForLevel4->size());
					PMString tempstr1("PubModelIndexSizeForLevel4->size = ");
					tempstr1.AppendNumber(PubModelIndexSizeForLevel4);
					CA(tempstr1);
					
					//PubDropListDataName->AddString("--Select--", 0, kFalse, kFalse);
					for(int32 PubModelIndexForLevel4=0; PubModelIndexForLevel4 < PubModelIndexSizeForLevel4; PubModelIndexForLevel4++)
					{
						PMString PubModelNameForLevel4("");
						PubModelNameForLevel4=pVectorPubModelForLevel4->at(PubModelIndexForLevel4).getName();

						vectorHtmlTrackerSPTBPtr->clear();
						PubModelNameForLevel4=iConverter->translateStringNew(PubModelNameForLevel4, vectorHtmlTrackerSPTBPtr);
						//CA(PubModelName);
						PubDropListDataNameForLevel4->AddString(PubModelNameForLevel4, PubModelIndexForLevel4, kFalse, kFalse);
					}

					//delete pVectorPubModel;
					PubDropListNameControllerForLevel4->Select(0);



					///////////////////for level5////////////////////////////////////

					pVectorPubModelForLevel5 = ptrIAppFramework->GetProject_getAllSectionsByPublicationIdAndLevel(pVectorPubModelForLevel4->at(0).getPublicationID(),5,MediatorClass::global_lang_id);
					if(pVectorPubModelForLevel4  == NULL)
					{
						//CA("Inside If");
						break;
					}

					//CA("End of else");
					
					int32 PubModelIndexSizeForLevel5=static_cast<int32>(pVectorPubModelForLevel5->size());
					PMString tempstr2("PubModelIndexSizeForLevel5->size = ");
					tempstr2.AppendNumber(PubModelIndexSizeForLevel5);
					CA(tempstr2);
					
					//PubDropListDataName->AddString("--Select--", 0, kFalse, kFalse);
					for(int32 PubModelIndexForLevel5=0; PubModelIndexForLevel5 < PubModelIndexSizeForLevel5; PubModelIndexForLevel5++)
					{
						PMString PubModelNameForLevel5("");
						PubModelNameForLevel5=pVectorPubModelForLevel5->at(PubModelIndexForLevel5).getName();

						vectorHtmlTrackerSPTBPtr->clear();
						PubModelNameForLevel5=iConverter->translateStringNew(PubModelNameForLevel5, vectorHtmlTrackerSPTBPtr);
						//CA(PubModelName);
						PubDropListDataNameForLevel5->AddString(PubModelNameForLevel5, PubModelIndexForLevel5, kFalse, kFalse);
					}

					//delete pVectorPubModel;
					PubDropListNameControllerForLevel5->Select(0);

					int32 PubModelPublication_ID;
				
					PubModelPublication_ID=pVectorPubModelForLevel5->at(0).getPublicationID();
					//PMString selIndex1("PubModelPublication_ID  ");
					//selIndex1.AppendNumber(PubModelPublication_ID);
					//CA(selIndex1);
					
					WidgetID selectedRadioWidgetID = dialogController->GetSelectedClusterWidget(kCMMFirstGroupPanelClusterPanelWidgetID);
					if(selectedRadioWidgetID == kFGPOneDocPerSectionRadioWidgetID)
					{
						populateSubSectionListForSectionLevel(kTrue,PubModelPublication_ID);
					}
					//else if(selectedRadioWidgetID == kFGPOneDocPerSpreadRadioWidgetID)
					//{
					//	populateSubSectionListForSpreadForSectionLevel(kTrue,PubModelPublication_ID);
					//}
					*/

				}
				
				
//////////////////////
				
				IControlView * listBoxControlView =  panelControlData->FindWidget(kCMMMTemplateFileListBoxWidgetID);
				if(listBoxControlView == nil) 
				{
					//CA("listBoxControlView == nil");
					break;
				}

				InterfacePtr<IPanelControlData> panelData(listBoxControlView, UseDefaultIID());
				if(panelData == nil)
				{
					//CA("panelData == nil");
					break;
				}
				
				IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
				if(cellControlView == nil) 
				{
					//CA("cellControlView == nil");
					break;
				}

				InterfacePtr<IPanelControlData> cellPanelWidgetPanelControlData (cellControlView, UseDefaultIID());
				if(cellPanelWidgetPanelControlData == nil) 
				{
					//CA("cellPanelWidgetPanelControlData == nil");				
					break;
				}

				PMString insertItemText("Select ");
				insertItemText.Append("Item");
				insertItemText.Append(" Template File");
/*-				SDKListBoxHelper listHelper(nil, 0, 0, 0);
				int32 listBoxRowIndex = 0;
				while(listBoxRowIndex < no_of_lstboxElements)
				{
					listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxItemFileNameTextWidgetID,insertItemText,listBoxRowIndex);				
					listBoxRowIndex++;
				} 

				PMString insertProductText("Select ");
				insertProductText.Append("Family");
				insertProductText.Append(" File");
				listBoxRowIndex = 0;
				while(listBoxRowIndex < no_of_lstboxElements)
				{
					listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxStencilFileNameTextWidgetID,insertProductText,listBoxRowIndex);				
					listBoxRowIndex++;
				}
 -*/
				break;

/////////////////////////////////
			}
			else if(selectedRadioWidgetID == kFGPOneDocPerSpreadRadioWidgetID)
			{
				folderPathtextView->HideView();
				//selectInfo.Append("Radio2 Selected\n");
				firstSeparatorCtrlView->ShowView();
				textForProCtrlView->ShowView();
				textForSpecSheetStencilCtrlView->HideView();
				secondSeparatorCtrlView->ShowView();
				textForItemCtrlView->ShowView();
				thirdSeparatorCtrlView->ShowView();
				textForFlowCtrlView->ShowView();
			
				MediatorClass :: createMediaRadioOption = 2;//per spread.

                MediatorClass::sectionLevel = 1; //-ptrIAppFramework->ProjectCache_getLevelForSection();
				if(MediatorClass::sectionLevel == 1 || MediatorClass::sectionLevel == -1){

					//CA("hide drop down here");
					//SectionSelectionDropDownListView->Hide();
					populateSubSectionListForSpread(kTrue,selectedPublicationID);

				}
				else
				{
					//CA("Show drop Down");
					//SectionSelectionDropDownListView->Show();
					
					//pVectorPubModel = ptrIAppFramework->GetProject_getAllSectionsByPublicationIdAndLevel(selectedPublicationID,MediatorClass::sectionLevel+1,MediatorClass::global_lang_id);
					//if(pVectorPubModel == NULL)
					//{
					//	//CA("Inside If");
					//	break;
					//}

					////CA("End of else");
					//
					//int32 PubModelIndexSize=static_cast<int32>(pVectorPubModel->size());
					///*PMString tempstr("pVectorPubModel->size = ");
					//tempstr.AppendNumber(PubModelIndexSize);
					//CA(tempstr);*/
					//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
					//if(!iConverter)
					//{
					//	//CA("!iConverter");
					//	
					//	return;
					//}					
					//	
					//VectorHtmlTrackerPtr vectorHtmlTrackerSPTBPtr = NULL;
					//VectorHtmlTrackerValue vectorObj ;
					//vectorHtmlTrackerSPTBPtr = &vectorObj;


					//PubDropListDataName->AddString("--Select--", 0, kFalse, kFalse);
					//for(int32 PubModelIndex=0; PubModelIndex < PubModelIndexSize; PubModelIndex++)
					//{
					//	PMString PubModelName("");
					//	PubModelName=pVectorPubModel->at(PubModelIndex).getName();

					//	vectorHtmlTrackerSPTBPtr->clear();
					//	PubModelName=iConverter->translateStringNew(PubModelName, vectorHtmlTrackerSPTBPtr);
					//	//CA(PubModelName);
					//	PubDropListDataName->AddString(PubModelName, PubModelIndex+1, kFalse, kFalse);
					//}

					////delete pVectorPubModel;
					//PubDropListNameController->Select(0);
					
					

				}

				IControlView * listBox =  panelControlData->FindWidget(kCMMMTemplateFileListBoxWidgetID);
				if(listBox == nil) 
				{
					//CA("listBox == nil");
					break;
				}

				InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
				if(panelData == nil)
				{
					//CA("panelData == nil");
					break;
				}
				
				IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
				if(cellControlView == nil) 
				{
					//CA("cellControlView == nil");
					break;
				}

				InterfacePtr<IPanelControlData> cellPanelWidgetPanelControlData (cellControlView, UseDefaultIID());
				if(cellPanelWidgetPanelControlData == nil) 
				{
					//CA("cellPanelWidgetPanelControlData == nil");				
					break;
				}

				PMString insertItemText("Select ");
				insertItemText.Append("Item");
				insertItemText.Append(" Template File");
/*-				SDKListBoxHelper listHelper(nil, 0, 0, 0);
				int32 listBoxRowIndex = 0;
				while(listBoxRowIndex < no_of_lstboxElements)
				{
					listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxItemFileNameTextWidgetID,insertItemText,listBoxRowIndex);				
					listBoxRowIndex++;
				}

				PMString insertProductText("Select ");
				insertProductText.Append("Family");
				insertProductText.Append(" File");
				listBoxRowIndex = 0;
				while(listBoxRowIndex < no_of_lstboxElements)
				{
					listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxStencilFileNameTextWidgetID,insertProductText,listBoxRowIndex);				
					listBoxRowIndex++;
				}
-*/
			
			}
			else if(selectedRadioWidgetID == kFGPOneDocPerPageRadioWidgetID)
			{	
				folderPathtextView->HideView();
				//selectInfo.Append("Radio3 Selected\n");
				firstSeparatorCtrlView->ShowView();
				textForProCtrlView->ShowView();
				textForSpecSheetStencilCtrlView->HideView();
				secondSeparatorCtrlView->ShowView();
				textForItemCtrlView->ShowView();
				thirdSeparatorCtrlView->ShowView();
				textForFlowCtrlView->ShowView();

				MediatorClass :: createMediaRadioOption = 3;//per page.

				
                MediatorClass::sectionLevel = 1; //-ptrIAppFramework->ProjectCache_getLevelForSection();
				if(MediatorClass::sectionLevel == 1 || MediatorClass::sectionLevel == -1){

					//CA("hide drop down here");
					//SectionSelectionDropDownListView->Hide();
					populateSubSectionList(kTrue,selectedPublicationID);
					
				}
				else
				{
					//CA("Show drop Down");
					//SectionSelectionDropDownListView->Show();
					
					//pVectorPubModel = ptrIAppFramework->GetProject_getAllSectionsByPublicationIdAndLevel(selectedPublicationID,MediatorClass::sectionLevel+1,MediatorClass::global_lang_id);
					//if(pVectorPubModel == NULL)
					//{
					//	//CA("Inside If");
					//	break;
					//}

					////CA("End of else");
					//
					//int32 PubModelIndexSize=static_cast<int32>(pVectorPubModel->size());
					///*PMString tempstr("pVectorPubModel->size = ");
					//tempstr.AppendNumber(PubModelIndexSize);
					//CA(tempstr);*/
					//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
					//if(!iConverter)
					//{
					//	//CA("!iConverter");
					//	
					//	return;
					//}					
					//	
					//VectorHtmlTrackerPtr vectorHtmlTrackerSPTBPtr = NULL;
					//VectorHtmlTrackerValue vectorObj ;
					//vectorHtmlTrackerSPTBPtr = &vectorObj;


					//PubDropListDataName->AddString("--Select--", 0, kFalse, kFalse);
					//for(int32 PubModelIndex=0; PubModelIndex < PubModelIndexSize; PubModelIndex++)
					//{
					//	PMString PubModelName("");
					//	PubModelName=pVectorPubModel->at(PubModelIndex).getName();

					//	vectorHtmlTrackerSPTBPtr->clear();
					//	PubModelName=iConverter->translateStringNew(PubModelName, vectorHtmlTrackerSPTBPtr);
					//	//CA(PubModelName);
					//	PubDropListDataName->AddString(PubModelName, PubModelIndex+1, kFalse, kFalse);
					//}

					////delete pVectorPubModel;
					//PubDropListNameController->Select(0);
					
					

				}
				

				//kFGPKeepSpreadsTogetherCheckBoxWidgetID is disabled.So it should be treated as false.
				//MediatorClass :: IskeepSpreadsTogether = kFalse;
				MediatorClass :: IskeepSpreadsTogether = kTrue; //Added By Dattatray on 28/02/2007 For testing

				IControlView * listBox =  panelControlData->FindWidget(kCMMMTemplateFileListBoxWidgetID);
				if(listBox == nil) 
				{
					//CA("listBox == nil");
					break;
				}

				InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
				if(panelData == nil)
				{
					//CA("panelData == nil");
					break;
				}
				
				IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
				if(cellControlView == nil) 
				{
					//CA("cellControlView == nil");
					break;
				}

				InterfacePtr<IPanelControlData> cellPanelWidgetPanelControlData (cellControlView, UseDefaultIID());
				if(cellPanelWidgetPanelControlData == nil) 
				{
					//CA("cellPanelWidgetPanelControlData == nil");				
					break;
				}

				PMString insertItemText("Select ");
				insertItemText.Append("Item");
				insertItemText.Append(" Template File");
/*-				SDKListBoxHelper listHelper(nil, 0, 0, 0);
				int32 listBoxRowIndex = 0;
				while(listBoxRowIndex < no_of_lstboxElements)
				{
					listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxItemFileNameTextWidgetID,insertItemText,listBoxRowIndex);				
					listBoxRowIndex++;
				}

				PMString insertProductText("Select ");
				insertProductText.Append(""Family);
				insertProductText.Append(" File");
				listBoxRowIndex = 0;
				while(listBoxRowIndex < no_of_lstboxElements)
				{
					listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxStencilFileNameTextWidgetID,insertProductText,listBoxRowIndex);				
					listBoxRowIndex++;
				}
 -*/

			}
			else if(selectedRadioWidgetID == kCMMWhiteBoardRadioWidgetID)
			{  
				folderPathtextView->ShowView();
				MediatorClass :: createMediaRadioOption = 4;//per page.

				populateSubSectionListForWhiteboard (kTrue,selectedPublicationID);
				if(MediatorClass::isCommentsAutoSpray == kFalse)
				{
					firstSeparatorCtrlView->ShowView();
					textForProCtrlView->HideView();
					textForSpecSheetStencilCtrlView->HideView();
					secondSeparatorCtrlView->HideView();
					textForItemCtrlView->HideView();
					thirdSeparatorCtrlView->HideView();
					textForFlowCtrlView->HideView();
					textForProdForManualCtrlView->ShowView();
					secondSeparatorForManualCtrlView->ShowView();
					textForItemForManualCtrlView->ShowView();
				}
				else 
				{
					textForProCtrlView->ShowView();
					textForSpecSheetStencilCtrlView->HideView();
					secondSeparatorCtrlView->ShowView();
					textForItemCtrlView->ShowView();
					thirdSeparatorCtrlView->ShowView();
					textForFlowCtrlView->ShowView();
					textForProdForManualCtrlView->HideView();
					secondSeparatorForManualCtrlView->HideView();
					textForItemForManualCtrlView->HideView();
				}
			}

			else if(selectedRadioWidgetID == kCMMPriceBookRadioWidgetID)
			{
				//CA("kCMMPriceBookRadioWidgetID");	
				//folderPathtextView->Hide();
				//selectInfo.Append("Radio2 Selected\n");
				firstSeparatorCtrlView->ShowView();
				textForSpecSheetStencilCtrlView->HideView();
				MediatorClass :: createMediaRadioOption = 5;//per spread.
				populateSubSectionListForPriceBook(kTrue,selectedPublicationID);

				IControlView * listBox =  panelControlData->FindWidget(kCMMMTemplateFileListBoxWidgetID);
				if(listBox == nil) 
				{
					//CA("listBox == nil");
					break;
				}

				InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
				if(panelData == nil)
				{
					//CA("panelData == nil");
					break;
				}
				
				IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
				if(cellControlView == nil) 
				{
					//CA("cellControlView == nil");
					break;
				}

				InterfacePtr<IPanelControlData> cellPanelWidgetPanelControlData (cellControlView, UseDefaultIID());
				if(cellPanelWidgetPanelControlData == nil) 
				{
					//CA("cellPanelWidgetPanelControlData == nil");				
					break;
				}

				PMString insertItemText("Select ");
				insertItemText.Append("Item");
				insertItemText.Append(" Template File");
/*-				SDKListBoxHelper listHelper(nil, 0, 0, 0);
				int32 listBoxRowIndex = 0;
				while(listBoxRowIndex < no_of_lstboxElements)
				{
					listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxItemFileNameTextWidgetID,insertItemText,listBoxRowIndex);				
					listBoxRowIndex++;
				}

				PMString insertProductText("Select ");
				insertProductText.Append("Family");
				insertProductText.Append(" File");
				listBoxRowIndex = 0;
				while(listBoxRowIndex < no_of_lstboxElements)
				{
					listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxStencilFileNameTextWidgetID,insertProductText,listBoxRowIndex);				
					listBoxRowIndex++;
				}
 -*/
			
			}

			else if(selectedRadioWidgetID == kCMMSpecSheetRadioWidgetID)
			{  
				//CA("selectedRadioWidgetID == kCMMSpecSheetRadioWidgetID");
				//folderPathtextView->Show();
				MediatorClass :: createMediaRadioOption = 6;//spac sheet.

				populateSubSectionListForSpecSheet(kTrue,selectedPublicationID);
								
				folderPathtextView->HideView();
				firstSeparatorCtrlView->ShowView();
				textForProCtrlView->HideView();
				textForSpecSheetStencilCtrlView->ShowView();
				secondSeparatorCtrlView->HideView();
				textForItemCtrlView->HideView();
				thirdSeparatorCtrlView->HideView();
				textForFlowCtrlView->HideView();
				textForProdForManualCtrlView->HideView();
				secondSeparatorForManualCtrlView->HideView();
				textForItemForManualCtrlView->HideView();
			}
			dialogController->SetTriStateControlData(kSelectAllCheckBoxWidgetID, kFalse , nil, kTrue);

			/* Commented on 28Oct..New Enhancement..refer CreateMediaDoubts.doc in D:\27OctcreateMediaEnhancements.
			//start 5Jun
			PMString templateFilePath;
			if(MediatorClass :: IsCommonMasterForAllPublicationRadioWidgetSelected)
			{
				templateFilePath = dialogController->GetTextControlData(kTemplateFileTextEditBoxWidgetID);
				//CA(templateFilePath);
				SDKFileHelper templateFile(templateFilePath);
				if(templateFilePath == "" || templateFile.IsExisting() != kTrue)
				{
					break;					
				}

			}
			//end 5Jun			
			//backButtonClicked = kFalse;
			//nextButtonClicked = kTrue;
			// Comment end on 28Oct
			*/
			
			/*31Oct..
			//3Jun
			InterfacePtr<IPanelControlData> secondGroupPanelControlData(secondGroupPanelControlView,UseDefaultIID());
			if(secondGroupPanelControlData == nil)
			{
				CA("secondGroupPanelControlData == nil");
				break;
			}
			IControlView * selectAllCheckBoxControlView = secondGroupPanelControlData->FindWidget(kSelectAllCheckBoxWidgetID);
			if(selectAllCheckBoxControlView == nil)
			{
				CA("selectAllCheckBoxControlView == nil");
				break;
			}
			
			IControlView * separatorWidgetControlView = secondGroupPanelControlData->FindWidget(kSeparatorOnGrouPanelWidgetID);
			if(separatorWidgetControlView == nil)
			{
				CA("separatorWidgetControlView == nil");
				break;
			}
			IControlView * templateFileNameWidgetControlView = secondGroupPanelControlData->FindWidget(kTemplateFileNameOnGroupPanelWidgetID);
			if(templateFileNameWidgetControlView == nil)
			{
				CA("templateFileNameWidgetControlView == nil");
				break;
			}
			
			if(kFalse)//28Oct..modification.MediatorClass :: IsCommonMasterForAllPublicationRadioWidgetSelected
			{

				MediatorClass ::Is_UseDifferentTemplateForEachSubSectionRadioSelected = kFalse;
				selectAllCheckBoxControlView->Show();				
				separatorWidgetControlView->Hide();
				templateFileNameWidgetControlView->Hide();


			}
			else
			{
				MediatorClass ::Is_UseDifferentTemplateForEachSubSectionRadioSelected = kTrue;
				selectAllCheckBoxControlView->Hide();
				separatorWidgetControlView->Show();
				templateFileNameWidgetControlView->Show();

			}
			//end 31Oct...
			*/
			//end 3Jun			
			////Get the selected index from the publication drop down
			////list box addition
			//if(MediatorClass ::publicationDropDownIndexSelected <= 0)
			//{
			//	break;
			//}

			////Get publicationID corresponding to the MediatorClass ::publicationDropDownIndexSelected
			//int32 selectedPublicationID = MediatorClass ::publicationVector[MediatorClass ::publicationDropDownIndexSelected - 1];

			//MediatorClass :: currentSelectedPublicationID= selectedPublicationID;

			////3Jun			
			////MediatorClass ::Is_UseDifferentTemplateForEachSubSectionRadioSelected = kTrue;
			//populateSubSectionList(kTrue,selectedPublicationID);
			/*
			//Commented  on 28Oct..New Enhancement..refere CreateMediaDoubts.doc in D:\27OctcreateMediaEnhancements.
			
			if(MediatorClass :: IsCommonMasterForAllPublicationRadioWidgetSelected)
			{
				vector<WidgetID> vWidgetID;
				vWidgetID.push_back(kListBoxSeparatorWidgetID);
				vWidgetID.push_back(kListBoxMasterFileNameTextWidgetID);
				vWidgetID.push_back(kListBoxSelectTemplateFileButtonWidgetID);
				vWidgetID.push_back(kInvokeSubSectionOptionsDialogRollOverButtonButtonWidgetID);

				SDKListBoxHelper listHelper(this, kCMMPluginID, kCMMMTemplateFileListBoxWidgetID, kCMMDialogWidgetID);

				IControlView * listBoxControlView = listHelper.FindCurrentListBox();
				if(listBoxControlView == nil)
				{
					CA("listBoxControlView == nil");
					break;
				}
				InterfacePtr<IListControlData>listcntldata(listBoxControlView,UseDefaultIID());
				if(listcntldata == nil)
				{
					CA("listcntldata == nil");
					break;
				}
				for(int32 listIndex=0;listIndex < listcntldata->Length();listIndex++)
				{
					listHelper.HideWidgets(listBoxControlView,vWidgetID,listIndex);
					MediatorClass::vector_subSectionSprayerListBoxParameters[listIndex].masterFileWithCompletePath = templateFilePath;
					MediatorClass::vector_subSectionSprayerListBoxParameters[listIndex].ssss.AllStencilFilePath = MediatorClass ::subSecSpraySttngs.AllStencilFilePath;
					MediatorClass::vector_subSectionSprayerListBoxParameters[listIndex].ssss.ItemStencilFilePath = MediatorClass ::subSecSpraySttngs.ItemStencilFilePath;
					MediatorClass::vector_subSectionSprayerListBoxParameters[listIndex].ssss.ProductStencilFilePath = MediatorClass ::subSecSpraySttngs.ProductStencilFilePath;
					MediatorClass::vector_subSectionSprayerListBoxParameters[listIndex].ssss.isSingleStencilFileForProductAndItem = MediatorClass ::subSecSpraySttngs.isSingleStencilFileForProductAndItem;
					//CA(MediatorClass::vector_subSectionSprayerListBoxParameters[listIndex].masterFileWithCompletePath);
				}

			}
			//Comment end  on 28Oct..New Enhancement..refere CreateMediaDoubts.doc in D:\27OctcreateMediaEnhancements.
			*/
			//SDKListBoxHelper listHelper(this, kCMMPluginID, kCMMMTemplateFileListBoxWidgetID, kCMMDialogWidgetID);
			//listHelper.EmptyCurrentListBox();
			//MediatorClass ::publicationDropDownIndexSelected
			//end list box addition
			break;
		}
		//First Group Panel widget
		
		/*
		//30Oct..
		//No use of CreateMediaButton on FirstGroupPanel
		if(theSelectedWidget == kFirstGroupPanelCreateMediaButtonWidgetID && theChange == kTrueStateMessage)
		{
			AcquireWaitCursor awc;
			awc.Animate(); 
		
			//CA("1");
			if(MediatorClass :: radioCreateMediaBySectionWidgetSelected)
			{
				CDialogObserver::CloseDialog();
				break;
			}

			if(MediatorClass ::publicationDropDownIndexSelected <= 0)
			{
				break;			
			}
			//Get publicationID corresponding to the MediatorClass ::publicationDropDownIndexSelected
			int32 selectedPublicationID = MediatorClass ::publicationVector[MediatorClass ::publicationDropDownIndexSelected - 1];
			MediatorClass :: currentSelectedPublicationID= selectedPublicationID;
			
			MediatorClass ::Is_UseDifferentTemplateForEachSubSectionRadioSelected = kFalse;

			populateSubSectionList(kFalse,selectedPublicationID);

			if(MediatorClass :: vector_subSectionSprayerListBoxParameters.size() <= 0)
			{
				CA("subSectionVectorSize is 0");
				break;
			}
			InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
			if(dialogController == nil)
			{
				CA("dialogController == nil");
				break;
			}
			PMString templateFilePath = dialogController->GetTextControlData(kTemplateFileTextEditBoxWidgetID);
			SDKFileHelper templateFile(templateFilePath);
			if(templateFilePath != "" && templateFile.IsExisting() == kTrue)
			{
				
				for(int32 subSectionVectorElementIndex =0;subSectionVectorElementIndex < MediatorClass :: vector_subSectionSprayerListBoxParameters.size();subSectionVectorElementIndex++)
				{
					MediatorClass :: vector_subSectionSprayerListBoxParameters[subSectionVectorElementIndex].masterFileWithCompletePath = templateFilePath;
				}
				//if spray by spread is selected then
				if(MediatorClass ::radioCreateMediaBySectionWidgetSelected == kFalse)
				{
					MediatorClass :: templateFilePathForSpreadSpray = templateFilePath;
					//CA("before startCreatingMediaForSpread()");
					startCreatingMediaForSpread();
				}
				else
				{
					
					//CA("before startCreatingMedia()");
					startCreatingMediaNew();
					//CA("after startCreatingMedia()");
				}
				CDialogObserver::CloseDialog();
			//else
			//{
				//startCreatingMediaForSpread();
			//}
			}
			//Following function uses variables from the MediatorClass.So no need to pass parameters to this function.
			//startCreatingMedia();	
			//CA("after startCreatingMedia()");
			//CDialogObserver::CloseDialog();
			//CA("after CloseDialog()");
			
			break;
		}
		//end 30Oct..
		*/
		//second group panel widget
		else if(theSelectedWidget == kBackButtonWidgetID && theChange == kTrueStateMessage)
		{//CA("Back button");

			if(pVectorPubModel != NULL)
			{
				delete pVectorPubModel;
				pVectorPubModel = NULL;
			}
			if(pVectorPubModelForLevel4 != NULL)
			{
				delete pVectorPubModelForLevel4;
				pVectorPubModelForLevel4 = NULL;
			}
			if(pVectorPubModelForLevel5 != NULL)
			{
				delete pVectorPubModelForLevel5;
				pVectorPubModelForLevel5 = NULL;
			}


			if(MediatorClass::isCommentsAutoSpray == kFalse)
			{
				textForProCtrlView->ShowView();
				secondSeparatorCtrlView->ShowView();
				textForItemCtrlView->ShowView();
				thirdSeparatorCtrlView->ShowView();
				textForFlowCtrlView->ShowView();
				textForProdForManualCtrlView->HideView();
				secondSeparatorForManualCtrlView->HideView();
				textForItemForManualCtrlView->HideView();

			}

			//backButtonClicked = kTrue;
			//nextButtonClicked = kFalse;
			//Show the firstGroupPanel
			IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kCMMFirstGroupPanelWidgetID);
			if(firstGroupPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::Update::firstGroupPanelControlData == nil");
				break;
			}
			firstGroupPanelControlData->ShowView();
			
			//Hide the secondGroupPanel
			IControlView * secondGroupPanelControlData = panelControlData->FindWidget(kCMMSecondGroupPanelWidgetID);
			if(secondGroupPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::Update::secondGroupPanelControlData == nil");
				break;
			}
			secondGroupPanelControlData->HideView();



			break;
		}
		//second group panel widget
		else if(theSelectedWidget == kSecondCancelButtonWidgetID && theChange == kTrueStateMessage)
		{
			if(pVectorPubModel != NULL)
			{
				delete pVectorPubModel;
				pVectorPubModel = NULL;
			}
			if(pVectorPubModelForLevel4 != NULL)
			{
				delete pVectorPubModelForLevel4;
				pVectorPubModelForLevel4 = NULL;
			}
			if(pVectorPubModelForLevel5 != NULL)
			{
				delete pVectorPubModelForLevel5;
				pVectorPubModelForLevel5 = NULL;
			}

			CDialogObserver::CloseDialog();
			break;
		}
		//second group panel widget
		else if((theSelectedWidget == kCreateMediaButtonWidgetID || (theSelectedWidget == kOKButtonWidgetID && /*secondGroupPanelControlData*/firstGroupPanelControlData->IsVisible())) && theChange == kTrueStateMessage)
            
		{//if 1
			
			AcquireWaitCursor awc;
			awc.Animate(); 

			ptrIAppFramework->clearAllStaticObjects();
            
            WidgetID selectedRadioWidgetID = dialogController->GetSelectedClusterWidget(kCMMFirstGroupPanelClusterPanelWidgetID);
            
            if(selectedRadioWidgetID == kFGPOneDocPerSectionRadioWidgetID)
            {
                MediatorClass ::radioCreateMediaBySectionWidgetSelected = kTrue;
                MediatorClass :: createMediaRadioOption = 1;//per section.
            }
            else if(selectedRadioWidgetID == kFGPOneDocPerSpreadRadioWidgetID)
            {
                MediatorClass ::radioCreateMediaBySectionWidgetSelected = kTrue;
                MediatorClass :: createMediaRadioOption = 2;//per spread.
            }
            else if(selectedRadioWidgetID == kFGPOneDocPerPageRadioWidgetID)
            {
                MediatorClass ::radioCreateMediaBySectionWidgetSelected = kTrue;
                MediatorClass :: createMediaRadioOption = 3;//per page.
            }
            else if(selectedRadioWidgetID == kCMMSpecSheetRadioWidgetID)
            {
                MediatorClass ::radioCreateMediaBySectionWidgetSelected = kTrue;
                MediatorClass :: createMediaRadioOption = 6;//spac sheet.
            }
            
			//Following function uses variables from the MediatorClass.So no need to pass parameters to this function.
			bool16 dialogClose = kFalse;
			if(MediatorClass ::radioCreateMediaBySectionWidgetSelected == kTrue)
			{
				//CA("beforrrrrre");
				//for(int32 listIndex = 0;listIndex <MediatorClass:: vector_subSectionSprayerListBoxParameters.size();listIndex++)
				// CA(MediatorClass::vector_subSectionSprayerListBoxParameters[listIndex].masterFileWithCompletePath);
				if( (MediatorClass::isCommentsAutoSpray == kFalse)  && (MediatorClass::createMediaRadioOption == 4))
				{
					CPubModel pubModelObj= ptrIAppFramework->getpubModelByPubID(MediatorClass::currentSelectedPublicationID, 1);

/*-					if( !(pubModelObj.getIspage_based())){
						MediatorClass::IsPageBased = kFalse;					
						startCreatingMediaNewForWBManualSpray();		
					}
					else
					{
						MediatorClass::IsPageBased = kTrue;
						startCreatingMediaNewForWBManualSprayForPAGE_BASED();
					}
 -*/
					dialogClose = kTrue;
				}
				else if(MediatorClass::createMediaRadioOption == 5){
                    //CA("before startCreatingMediaForPriceBookNew");
					dialogClose = startCreatingMediaForPriceBookNew();//startCreatingMediaForPriceBook();
				}
				else if(MediatorClass::createMediaRadioOption == 6){
					//CA("before startCreatingMediaForSpecSheet");
					dialogClose = startCreatingMediaForSpecSheet();
					//CA("after startCreatingMediaForSpecSheet");
				}
				else{
                    //CA("before startCreatingMediaNew");
					dialogClose = startCreatingMediaNew();
				}
			}
			if(dialogClose == kTrue){
				//CA("before CloseDialog()");
				CDialogObserver::CloseDialog();
				//CA("after CloseDialog()");
				
				if(pVectorPubModel != NULL)
				{
					delete pVectorPubModel;
					pVectorPubModel = NULL;
				}
				if(pVectorPubModelForLevel4 != NULL)
				{
					delete pVectorPubModelForLevel4;
					pVectorPubModelForLevel4 = NULL;
				}
				if(pVectorPubModelForLevel5 != NULL)
				{
					delete pVectorPubModelForLevel5;
					pVectorPubModelForLevel5 = NULL;
				}
			}
			//CA("after CloseDialog()");

			break;
		}//end if 1

		//added on 6Jun
								//Modified..
		if(theSelectedWidget == kSelectAllCheckBoxWidgetID )// && theChange == kTrueStateMessage)
		{//if 1
			
			//This checkbox is shown only when the commonmasterforallsection radio is selected.
			//CA("if(theSelectedWidget == kSelectAllCheckBoxWidgetID )Line..1479");

			if(MediatorClass :: IsCommonMasterForAllPublicationRadioWidgetSelected)
			{//if x1
				//CA("if(theSelectedWidget == kSelectAllCheckBoxWidgetID )Line..1483");
/*-				SDKListBoxHelper listHelper(this, kCMMPluginID, kCMMMTemplateFileListBoxWidgetID, kCMMDialogWidgetID);
				IControlView * listBox = MediatorClass ::PublicationTemplateMappingListBox;
				if(listBox == nil) 
				{//if x2
					ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::Update::listBox == nil");
					break;
				}//end if x2
				if(theChange == kTrueStateMessage)
				{//if x3
						//CA("true...1489");
						//CA("CmmDialogObserver.cpp...(theChange == kTrueStateMessage)..Line 1493");
					    for(int32 listIndex=0;listIndex<MediatorClass :: vector_subSectionSprayerListBoxParameters.size();listIndex++)
						{
							MediatorClass :: vector_subSectionSprayerListBoxParameters[listIndex].isSelected = kTrue;
							//CA("if(theChange == kTrueStateMessage)..Line1498");
							listHelper.ToggleCheckUnchekIconOfSelectedRow
							(
								listBox,
								listIndex,
								MediatorClass :: vector_subSectionSprayerListBoxParameters[listIndex].isSelected
							);
					}
				}//end if x3
				else if(MediatorClass ::IsEventSourceInListBox == kFalse)
				{//if x4
					//CA(" kSelectAllCheckBoxWidgetID false");
					for(int32 listIndex=0;listIndex<MediatorClass :: vector_subSectionSprayerListBoxParameters.size();listIndex++)
					{
						MediatorClass :: vector_subSectionSprayerListBoxParameters[listIndex].isSelected = kFalse; //kTrue Modified Sachin..
						listHelper.ToggleCheckUnchekIconOfSelectedRow
						(
							listBox,
							listIndex,
							MediatorClass :: vector_subSectionSprayerListBoxParameters[listIndex].isSelected
						);
					}
				}//end if x4
-*/
			}///end if x1
		}//end if 1
		

		/////////////////////////////////////////////////////////////////////
//		if(theSelectedWidget == kSelectAllCheckBoxWidgetID )//&& theChange == kFalseStateMessage)
//		{//if 1
//			//This checkbox is shown only when the commonmasterforallsection radio is selected.
//CA("false");
//		if(MediatorClass :: IsCommonMasterForAllPublicationRadioWidgetSelected)
//		{
//			SDKListBoxHelper listHelper(this, kCMMPluginID, kCMMMTemplateFileListBoxWidgetID, kCMMDialogWidgetID);
//			IControlView * listBox = MediatorClass ::PublicationTemplateMappingListBox;
//			if(listBox == nil) 
//			{
//				CA("listBox == nil");
//				break;
//			}
//			if(theChange == kTrueStateMessage)
//			{
//				for(int32 listIndex=0;listIndex<MediatorClass :: vector_subSectionSprayerListBoxParameters.size();listIndex++)
//				{
//					MediatorClass :: vector_subSectionSprayerListBoxParameters[listIndex].isSelected = kFalse;
//					listHelper.ToggleCheckUnchekIconOfSelectedRow
//					(
//						listBox,
//						listIndex,
//						MediatorClass :: vector_subSectionSprayerListBoxParameters[listIndex].isSelected
//					);
//
//				}
//			}
//			else if(MediatorClass ::IsEventSourceInListBox = kFalse
//
//		}
//
//			
//		}//end if 1
		////////////////////////////////////////////////////////////////////////
		//ended on 6Jun		
		// TODO: process this

		
		///--------------
		//else if((theSelectedWidget == kSectionSelectionDropDownListWidgetID)&&(theChange == kPopupChangeStateMessage))
		//{
		//	//CA("Inside 1");
		//	InterfacePtr<IStringListControlData> PubDropListDataName(
		//	dialogController->QueryListControlDataInterface(kSectionSelectionDropDownListWidgetID));
		//	if(PubDropListDataName == nil)
		//	{
		//		//CA("PubDropListData nil");
		//		break;
		//	}
		//		

		//	InterfacePtr<IDropDownListController> PubDropListNameController(PubDropListDataName,UseDefaultIID());
		//	if (PubDropListNameController == nil)
		//	{
		//		//CA("PubDropListController nil");
		//		break;
		//	}
		//	
		//	int32 Selected_Index=PubDropListNameController->GetSelected();
		//	/*PMString selIndex("Selected_Index   ");
		//	selIndex.AppendNumber(Selected_Index);
		//	CA(selIndex);*/
		//	//viv1
		//	InterfacePtr<IStringListControlData> PubDropListDataNameForLevel4(
		//	dialogController->QueryListControlDataInterface(kSectionSelectionDropDownListLevel4WidgetID));
		//	if(PubDropListDataNameForLevel4 == nil)
		//	{
		//		//CA("PubDropListData nil");
		//		break;
		//	}
		//	PubDropListDataNameForLevel4->Clear(kFalse,kFalse);	

		//	InterfacePtr<IDropDownListController> PubDropListNameControllerForLevel4(PubDropListDataNameForLevel4,UseDefaultIID());
		//	if (PubDropListNameControllerForLevel4 == nil)
		//	{
		//		//CA("PubDropListController nil");
		//		break;
		//	}
		//	//viv1
		//	//viv1
		//	//InterfacePtr<IStringListControlData> PubDropListDataNameForLevel5(dialogController->QueryListControlDataInterface(kSectionSelectionDropDownListLevel5WidgetID));
		//	//if(PubDropListDataNameForLevel5== nil)
		//	//{
		//	//	//CA("PubDropListData nil");
		//	//	break;
		//	//}
		//	//PubDropListDataNameForLevel5->Clear(kFalse,kFalse);	

		//	//InterfacePtr<IDropDownListController> PubDropListNameControllerForLevel5(PubDropListDataNameForLevel5,UseDefaultIID());
		//	//if (PubDropListNameControllerForLevel5 == nil)
		//	//{
		//	//	//CA("PubDropListController nil");
		//	//	break;
		//	//}
		//	////viv1
		//	if(Selected_Index >=0)
		//	{
		//		int32 PubModelPublication_ID;
		//		
		//		PubModelPublication_ID=(pVectorPubModel->at(Selected_Index)).getPublicationID();
		//		/*PMString selIndex1("PubModelPublication_ID  ");
		//		selIndex1.AppendNumber(PubModelPublication_ID);
		//		CA(selIndex1);*/

		//		if(pVectorPubModelForLevel4 != NULL)
		//		{
		//			delete pVectorPubModelForLevel4;
		//			pVectorPubModelForLevel4 = NULL;
		//		}

		//		pVectorPubModelForLevel4 = ptrIAppFramework->GetProject_getAllSectionsByPublicationIdAndLevel(PubModelPublication_ID,4,MediatorClass::global_lang_id);
		//		if(pVectorPubModelForLevel4 == NULL)
		//		{
		//			//CA("Inside If");
		//			break;
		//		}
		//		int32 PubModelIndexSizeForLevel4=static_cast<int32>(pVectorPubModelForLevel4->size());
		//		/*PMString tempstr1("PubModelIndexSizeForLevel4->size = ");
		//		tempstr1.AppendNumber(PubModelIndexSizeForLevel4);
		//		CA(tempstr1);*/
		//		VectorHtmlTrackerPtr vectorHtmlTrackerSPTBPtr = NULL;
		//		VectorHtmlTrackerValue vectorObj ;
		//		vectorHtmlTrackerSPTBPtr = &vectorObj;
		//	
		//		//PubDropListDataName->AddString("--Select--", 0, kFalse, kFalse);
		//		for(int32 PubModelIndexForLevel4=0; PubModelIndexForLevel4 < PubModelIndexSizeForLevel4; PubModelIndexForLevel4++)
		//		{
		//			PMString PubModelNameForLevel4("");
		//			PubModelNameForLevel4=pVectorPubModelForLevel4->at(PubModelIndexForLevel4).getName();

		//			vectorHtmlTrackerSPTBPtr->clear();
		//			PubModelNameForLevel4=iConverter->translateStringNew(PubModelNameForLevel4, vectorHtmlTrackerSPTBPtr);
		//			//CA(PubModelName);
		//			PubDropListDataNameForLevel4->AddString(PubModelNameForLevel4, PubModelIndexForLevel4, kFalse, kFalse);
		//		}

		//		//delete pVectorPubModel;
		//		PubDropListNameControllerForLevel4->Select(0);

		//		//pVectorPubModelForLevel5 = ptrIAppFramework->GetProject_getAllSectionsByPublicationIdAndLevel(pVectorPubModelForLevel4->at(0).getPublicationID(),5,MediatorClass::global_lang_id);
		//		//if(pVectorPubModelForLevel5 == NULL)
		//		//{
		//		//	//CA("Inside If");
		//		//	break;
		//		//}
		//		//int32 PubModelIndexSizeForLevel5=static_cast<int32>(pVectorPubModelForLevel5->size());
		//		///*PMString tempstr1("PubModelIndexSizeForLevel4->size = ");
		//		//tempstr1.AppendNumber(PubModelIndexSizeForLevel4);
		//		//CA(tempstr1);*/
		//		//
		//		////PubDropListDataName->AddString("--Select--", 0, kFalse, kFalse);
		//		//for(int32 PubModelIndexForLevel5=0; PubModelIndexForLevel5 < PubModelIndexSizeForLevel5; PubModelIndexForLevel5++)
		//		//{
		//		//	PMString PubModelNameForLevel5("");
		//		//	PubModelNameForLevel5=pVectorPubModelForLevel5->at(PubModelIndexForLevel5).getName();

		//		//	vectorHtmlTrackerSPTBPtr->clear();
		//		//	PubModelNameForLevel5=iConverter->translateStringNew(PubModelNameForLevel5, vectorHtmlTrackerSPTBPtr);
		//		//	//CA(PubModelName);
		//		//	PubDropListDataNameForLevel5->AddString(PubModelNameForLevel5, PubModelIndexForLevel5, kFalse, kFalse);
		//		//}

		//		////delete pVectorPubModel;
		//		//PubDropListNameControllerForLevel5->Select(0);
		//	}
		//}

		//else if((theSelectedWidget == kSectionSelectionDropDownListLevel4WidgetID)&&(theChange == kPopupChangeStateMessage))
		//{
		//	//CA("Inside_Second_DropDown");
		//	InterfacePtr<IStringListControlData> PubDropListDataName(
		//	dialogController->QueryListControlDataInterface(kSectionSelectionDropDownListLevel4WidgetID));
		//	if(PubDropListDataName == nil)
		//	{
		//		//CA("PubDropListData nil");
		//		break;
		//	}
		//		

		//	InterfacePtr<IDropDownListController> PubDropListNameController(PubDropListDataName,UseDefaultIID());
		//	if (PubDropListNameController == nil)
		//	{
		//		//CA("PubDropListController nil");
		//		break;
		//	}
		//	
		//	int32 Selected_Index=PubDropListNameController->GetSelected();
		//	/*PMString selIndex("Selected_Index   ");
		//	selIndex.AppendNumber(Selected_Index);
		//	CA(selIndex);*/
		//	
		//	//viv1
		//	//viv1
		//	InterfacePtr<IStringListControlData> PubDropListDataNameForLevel5(dialogController->QueryListControlDataInterface(kSectionSelectionDropDownListLevel5WidgetID));
		//	if(PubDropListDataNameForLevel5== nil)
		//	{
		//		//CA("PubDropListData nil");
		//		break;
		//	}
		//	PubDropListDataNameForLevel5->Clear(kFalse,kFalse);	

		//	InterfacePtr<IDropDownListController> PubDropListNameControllerForLevel5(PubDropListDataNameForLevel5,UseDefaultIID());
		//	if (PubDropListNameControllerForLevel5 == nil)
		//	{
		//		//CA("PubDropListController nil");
		//		break;
		//	}
		//	//viv1
		//	if(Selected_Index >=0)
		//	{
		//		int32 PubModelPublication_ID;
		//		
		//		PubModelPublication_ID=(pVectorPubModelForLevel4->at(Selected_Index)).getPublicationID();
		//		/*PMString selIndex1("PubModelPublication_ID  ");
		//		selIndex1.AppendNumber(PubModelPublication_ID);
		//		CA(selIndex1);*/

		//		if(pVectorPubModelForLevel5 != NULL)
		//		{
		//			delete pVectorPubModelForLevel5;
		//			pVectorPubModelForLevel5 = NULL;
		//		}

		//		pVectorPubModelForLevel5 = ptrIAppFramework->GetProject_getAllSectionsByPublicationIdAndLevel(pVectorPubModelForLevel4->at(Selected_Index).getPublicationID(),5,MediatorClass::global_lang_id);
		//		if(pVectorPubModelForLevel5 == NULL)
		//		{
		//			//CA("Inside If");
		//			break;
		//		}
		//		int32 PubModelIndexSizeForLevel5=static_cast<int32>(pVectorPubModelForLevel5->size());
		//		/*PMString tempstr1("PubModelIndexSizeForLevel4->size = ");
		//		tempstr1.AppendNumber(PubModelIndexSizeForLevel4);
		//		CA(tempstr1);*/
		//		
		//		//PubDropListDataName->AddString("--Select--", 0, kFalse, kFalse);
		//		VectorHtmlTrackerPtr vectorHtmlTrackerSPTBPtr = NULL;
		//		VectorHtmlTrackerValue vectorObj ;
		//		vectorHtmlTrackerSPTBPtr = &vectorObj;
		//		for(int32 PubModelIndexForLevel5=0; PubModelIndexForLevel5 < PubModelIndexSizeForLevel5; PubModelIndexForLevel5++)
		//		{
		//			PMString PubModelNameForLevel5("");
		//			PubModelNameForLevel5=pVectorPubModelForLevel5->at(PubModelIndexForLevel5).getName();

		//			vectorHtmlTrackerSPTBPtr->clear();
		//			PubModelNameForLevel5=iConverter->translateStringNew(PubModelNameForLevel5, vectorHtmlTrackerSPTBPtr);
		//			//CA(PubModelName);
		//			PubDropListDataNameForLevel5->AddString(PubModelNameForLevel5, PubModelIndexForLevel5, kFalse, kFalse);
		//		}

		//		//delete pVectorPubModel;
		//		PubDropListNameControllerForLevel5->Select(0);
		//	}
		//}

		//else if((theSelectedWidget == kSectionSelectionDropDownListLevel5WidgetID)&&(theChange == kPopupChangeStateMessage))
		//{
		//	//CA("Inside 1");
		//	InterfacePtr<IStringListControlData> PubDropListDataName(dialogController->QueryListControlDataInterface(kSectionSelectionDropDownListLevel5WidgetID));
		//	if(PubDropListDataName == nil)
		//	{
		//		//CA("PubDropListData nil");
		//		break;
		//	}
		//		

		//	InterfacePtr<IDropDownListController> PubDropListNameController(PubDropListDataName,UseDefaultIID());
		//	if (PubDropListNameController == nil)
		//	{
		//		//CA("PubDropListController nil");
		//		break;
		//	}
		//	
		//	int32 Selected_Index=PubDropListNameController->GetSelected();
		//	int32 PubModelPublication_ID;
		//		
		//	PubModelPublication_ID=pVectorPubModelForLevel5->at(Selected_Index).getPublicationID();
		//	/*PMString selIndex1("PubModelPublication_ID  ");
		//	selIndex1.AppendNumber(PubModelPublication_ID);
		//	CA(selIndex1);*/
		//	
		//	WidgetID selectedRadioWidgetID = dialogController->GetSelectedClusterWidget(kCMMFirstGroupPanelClusterPanelWidgetID); 
		//	if(selectedRadioWidgetID == kFGPOneDocPerSectionRadioWidgetID)
		//	{
		//		populateSubSectionListForSectionLevel(kTrue,PubModelPublication_ID);
		//	}

		//	IControlView * listBox =  panelControlData->FindWidget(kCMMMTemplateFileListBoxWidgetID);
		//	if(listBox == nil) 
		//	{
		//		//CA("listBox == nil");
		//		break;
		//	}

		//	InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		//	if(panelData == nil)
		//	{
		//		//CA("panelData == nil");
		//		break;
		//	}
		//	
		//	IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		//	if(cellControlView == nil) 
		//	{
		//		//CA("cellControlView == nil");
		//		break;
		//	}

		//	InterfacePtr<IPanelControlData> cellPanelWidgetPanelControlData (cellControlView, UseDefaultIID());
		//	if(cellPanelWidgetPanelControlData == nil) 
		//	{
		//		//CA("cellPanelWidgetPanelControlData == nil");				
		//		break;
		//	}

		//	PMString insertItemText("Select ");
		//	insertItemText.Append(ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename("Item"),MediatorClass::global_lang_id));
		//	insertItemText.Append(" Template File");
		//	SDKListBoxHelper listHelper(nil, 0, 0, 0);
		//	int32 listBoxRowIndex = 0;
		//	while(listBoxRowIndex < no_of_lstboxElements)
		//	{
		//		listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxItemFileNameTextWidgetID,insertItemText,listBoxRowIndex);				
		//		listBoxRowIndex++;
		//	}

		//	PMString insertProductText("Select ");
		//	insertProductText.Append(ptrIAppFramework->LABELCACHE_getLabel(ptrIAppFramework->getCodeToRename("Product"),MediatorClass::global_lang_id));
		//	insertProductText.Append(" File");
		//	listBoxRowIndex = 0;
		//	while(listBoxRowIndex < no_of_lstboxElements)
		//	{
		//		listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kListBoxStencilFileNameTextWidgetID,insertProductText,listBoxRowIndex);				
		//		listBoxRowIndex++;
		//	}

		//}



	
	} while (kFalse);

}

//  Code generated by DollyXs code generator

void CMMDialogObserver :: populatePublicationDropDownList(double SelectedLocaleID)
{
	do
	{
		//InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
		{
			//CA("ptrIAppFramework == nil");
			break;
		}

		if(SelectedLocaleID == -1)
			break;

		InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
		if(dialogController == nil)
		{
			//CA("dialogController == nil");
			break;
		}

		InterfacePtr<IStringListControlData> PubDropListData(
		dialogController->QueryListControlDataInterface(kPublicationDropDownWidgetID));
		if(PubDropListData == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::populatePublicationDropDownList::PubDropListData nil");
			break;
		}
		PubDropListData->Clear(kFalse,kFalse);	

		InterfacePtr<IDropDownListController> PubDropListController(PubDropListData,UseDefaultIID());
		if (PubDropListController == nil)
		{
			//CA("PubDropListController nil");
			break;
		}



		ptrIAppFramework->EventCache_clearInstance();
		
		VectorPubModelPtr VectorPublInfoValuePtr = ptrIAppFramework->ProjectCache_getAllProjects(SelectedLocaleID);
		
		
		if(VectorPublInfoValuePtr == nil){
			ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::populatePublicationDropDownList::PUBMgr_getAllProjects::There are no Publications!");
			break;
		}

		
		VectorPubModel::iterator it;
		int32 dropDownListRowIndex = 0;
		MediatorClass ::publicationVector.clear();
		
		for(it = VectorPublInfoValuePtr->begin(); it != VectorPublInfoValuePtr->end(); it++)
		{	
			//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			//CA("FOR");
			PMString CatalogName;
			CatalogName.SetTranslatable(kFalse);
			PMString TempBuffer = it->getName();
			TempBuffer.SetTranslatable(kFalse);
			if(iConverter)
			{ 
				CatalogName=iConverter->translateString(TempBuffer);
			}
			else
			{ 				
				CatalogName = TempBuffer;
			}
			
			MediatorClass ::publicationVector.push_back(it->getEventId());
					
			if(dropDownListRowIndex == 0)
			{
				PubDropListData->AddString("--Select--", dropDownListRowIndex, kFalse, kFalse);
				dropDownListRowIndex++;
			}
			PubDropListData->AddString(CatalogName, dropDownListRowIndex, kFalse, kFalse);
			dropDownListRowIndex++;
		}
		PubDropListController->Select(0);
		if(VectorPublInfoValuePtr)
			delete VectorPublInfoValuePtr;
	}while(kFalse);

	/*
	for(int32 i=0;i<MediatorClass ::publicationVector.size();i++)
	{
		PMString n(" number : ");
		n.AppendNumber
		CA(MediatorClass ::publicationVector[i]);
	}
	*/
}

void CMMDialogObserver::populateSubSectionListForSpecSheet(bool16 shall_I_Fill_Data_Into_ListBox,double rootId)
{
/*-
	no_of_lstboxElements=0;
	//CA("1 Inside CMMDialogObserver::populateSubSectionListForSpecSheet");
	//noOfRows = 0;	
	do
	{
		SDKListBoxHelper listHelper(this, kCMMPluginID, kCMMMTemplateFileListBoxWidgetID, kCMMDialogWidgetID);
		if(shall_I_Fill_Data_Into_ListBox)
		{

			listHelper.EmptyCurrentListBox();
		}
		//yet to define
		MediatorClass :: vector_subSectionSprayerListBoxParameters.clear();
		//MediatorClass ::PublicationTemplateMappingListBox

		
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
		{
			//CA("ptrIAppFramework == nil");
			break;
		}

		MediatorClass :: global_project_level = ptrIAppFramework->getPM_Project_Levels();
		
		SectionData publdata;
		int32 sectionID = -1;
		publdata.clearSectionVector();
		publdata.clearSubsectionVector();
		//define global_vector_pubmodel
		MediatorClass:: global_vector_pubmodel.clear();
		int32 language_id = -1 ;
		//define global_lang_id
		language_id = MediatorClass :: global_lang_id ; //ptrIClientOptions->getDefaultLocale(language_name);
		VectorPubModelPtr vector_pubmodel = NULL;

		if( MediatorClass :: global_project_level == 3)
		{	
			//CA("Level 3");	
			vector_pubmodel = ptrIAppFramework->getAllSectionsByPubIdAndLanguageId(rootId , language_id);
			if(vector_pubmodel == nil)
			{
				ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::populateSubsectionList::getAllSectionsByPubIdAndLanguageId::vector_pubmodel == nil");
				break;
			}
			VectorPubModel::iterator it;
			for(it=vector_pubmodel->begin(); it!=vector_pubmodel->end(); it++)
			{
					int32 sectionID=it->getPublicationID();
					PMString sectionName = it->getName();
					
					VectorPubModelPtr vectorSubSectionModel = ptrIAppFramework->getAllSubsectionsBySectionIdAndLanguageId(sectionID , language_id);
					if(vectorSubSectionModel == nil)
					{
						
						ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::populateSubsectionList::getAllSubsectionsBySectionIdAndLanguageId::vectorSubSectionModel == nil");
						break;
					}
					
					VectorPubModel::iterator it_subsection;

					for(it_subsection=vectorSubSectionModel->begin(); it_subsection!=vectorSubSectionModel->end(); it_subsection++)
					{

						CPubModel model = *(it_subsection);
						MediatorClass:: global_vector_pubmodel.push_back(model);
						PMString subSectionName(iConverter->translateString(model.getName()));

						subSectionSprayerListBoxParameters tempClass;

						subSectionData ssd;
						ssd.subSectionName = subSectionName;
						ssd.subSectionID = model.getPublicationID();
						ssd.sectionID = sectionID;
						ssd.sectionName = sectionName;
						ssd.number = model.getNumber();						

						tempClass.displayName = subSectionName;
						tempClass.isSelected = kTrue;
						tempClass.vec_ssd.push_back(ssd);

						if(shall_I_Fill_Data_Into_ListBox)
						{
							//CA("adding elements into listbox");
							subSectionName.SetTranslatable(kFalse);
							listHelper.AddElement(subSectionName,kPublicationNameTextWidgetID);
						}				
						
						MediatorClass :: vector_subSectionSprayerListBoxParameters.push_back(tempClass);									
					}					
			}	

			if(vector_pubmodel)
				delete vector_pubmodel;

		}
		else if( MediatorClass :: global_project_level ==2)
		{
			//CA("Level 2");				
			vector_pubmodel = ptrIAppFramework->getAllSubsectionsByPubIdAndLanguageId(rootId , language_id);
			if(vector_pubmodel == nil)
			{
				ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::populateSubsectionList::getAllSubsectionsByPubIdAndLanguageId::vector_pubmodel == nil");
				break;
			}
					
//CA("now");
			VectorPubModel::iterator it;

			for(it=vector_pubmodel->begin(); it!=vector_pubmodel->end(); it++,no_of_lstboxElements++)
			{

				CPubModel model = (*it);
				MediatorClass:: global_vector_pubmodel.push_back(model);
				PMString subSectionName(iConverter->translateString(model.getName()));
								
				subSectionSprayerListBoxParameters tempClass;

				subSectionData ssd;
				ssd.subSectionName = subSectionName;
				ssd.subSectionID = model.getPublicationID();
				ssd.sectionID = model.getPublicationID();  // For Project Level 2 fill section id same as Subsection id.
				ssd.number = model.getNumber();	

				tempClass.displayName = subSectionName;
				tempClass.isSelected = kTrue;
				tempClass.vec_ssd.push_back(ssd);
				//No sectionID and sectionName information.Default will be assigned.	

				if(shall_I_Fill_Data_Into_ListBox)
				{
					subSectionName.SetTranslatable(kFalse);
					listHelper.AddElement(subSectionName,kPublicationNameTextWidgetID);
				}
///////////////////////////// For hiding extra widgets used for manual spray in white board    from here
				vWidgetID.clear();
				vWidgetID.push_back(kListBoxStencilFileNameTextWidgetID);
				vWidgetID.push_back(kSeparatorOnGrouPanel1WidgetID);
				vWidgetID.push_back(kListBoxItemFileNameTextWidgetID);
				vWidgetID.push_back(kListBoxSelectItemStencilRollOverButtonWidgetID);
				vWidgetID.push_back(kListBoxApplyItemStencilUnderneathElementsRollOverButtonWidgetID);
				vWidgetID.push_back(kSeparatorOnGrouPanel6WidgetID);
				vWidgetID.push_back(kInvokeSubSectionOptionsDialogRollOverButtonGreenButtonWidgetID);
				vWidgetID.push_back(kListBoxApplySpraySettingsUnderneathElementsRollOverButtonWidgetID);
				vWidgetID.push_back(kInvokeSubSectionOptionsDialogRollOverButtonButtonWidgetID);
				
				vWidgetID.push_back(kListBoxProductStencilFileNameTextForManualWidgetID);
				vWidgetID.push_back(kSeparatorOnGrouPanel1ForManualWidgetID);
				vWidgetID.push_back(kListBoxItemStencilFileNameTextForManualWidgetID);
				
				listHelper.HideWidgets(MediatorClass::PublicationTemplateMappingListBox, vWidgetID, no_of_lstboxElements);
////////////////////////////////////////////////////////////////////////////////////////////     upto here
				MediatorClass :: vector_subSectionSprayerListBoxParameters.push_back(tempClass);

			}
			if(vector_pubmodel)
				delete vector_pubmodel;
		

		}
		
	}while(kFalse);
-*/
}


void CMMDialogObserver::populateSubSectionList(bool16 shall_I_Fill_Data_Into_ListBox,double rootId)
{
/*-
	no_of_lstboxElements=0;// declared globally on 03-october
	//CA("1 Inside SPSelectionObserver::populateSectionDropDownList");
	//noOfRows = 0;	
	do
	{
		SDKListBoxHelper listHelper(this, kCMMPluginID, kCMMMTemplateFileListBoxWidgetID, kCMMDialogWidgetID);
		if(shall_I_Fill_Data_Into_ListBox)
		{

			listHelper.EmptyCurrentListBox();
		}
		//yet to define
		MediatorClass :: vector_subSectionSprayerListBoxParameters.clear();
		//MediatorClass ::PublicationTemplateMappingListBox

		
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
		{
			//CA("ptrIAppFramework == nil");
			break;
		}
		//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(!iConverter)
		{
			//CA("iConverter == nil");
			break;
		}

		MediatorClass :: global_project_level = ptrIAppFramework->getPM_Project_Levels();
		
		SectionData publdata;
		int32 sectionID = -1;
		publdata.clearSectionVector();
		publdata.clearSubsectionVector();
		//define global_vector_pubmodel
		MediatorClass:: global_vector_pubmodel.clear();

		int32 language_id = -1 ;
		//define global_lang_id
		language_id = MediatorClass :: global_lang_id ; //ptrIClientOptions->getDefaultLocale(language_name);
		VectorPubModelPtr vector_pubmodel = NULL;

		if( MediatorClass :: global_project_level == 3)
		{	
//CA("Level 3");	
			vector_pubmodel = ptrIAppFramework->getAllSectionsByPubIdAndLanguageId(rootId , language_id);
			if(vector_pubmodel == nil)
			{
				ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::populateSubsectionList::getAllSectionsByPubIdAndLanguageId::vector_pubmodel == nil");
				break;
			}
			VectorPubModel::iterator it;
			for(it=vector_pubmodel->begin(); it!=vector_pubmodel->end(); it++)
			{
				int32 sectionID=it->getPublicationID();
				PMString sectionName = it->getName();
				
				VectorPubModelPtr vectorSubSectionModel = ptrIAppFramework->getAllSubsectionsBySectionIdAndLanguageId(sectionID , language_id);
				if(vectorSubSectionModel == nil)
				{
					
					ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::populateSubsectionList::getAllSubsectionsBySectionIdAndLanguageId::vectorSubSectionModel == nil");
					break;
				}
				
				VectorPubModel::iterator it_subsection;

				for(it_subsection=vectorSubSectionModel->begin(); it_subsection!=vectorSubSectionModel->end(); it_subsection++)
				{

					CPubModel model = *(it_subsection);
					MediatorClass:: global_vector_pubmodel.push_back(model);
					PMString subSectionName(iConverter->translateString(model.getName()));

					subSectionSprayerListBoxParameters tempClass;

					subSectionData ssd;
					ssd.subSectionName = subSectionName;
					ssd.subSectionID = model.getPublicationID();
					ssd.sectionID = sectionID;
					ssd.sectionName = sectionName;
					ssd.number = model.getNumber();						

					tempClass.displayName = subSectionName;
					tempClass.isSelected = kTrue;
					tempClass.vec_ssd.push_back(ssd);

					if(shall_I_Fill_Data_Into_ListBox)
					{
						//CA("adding elements into listbox");
						subSectionName.SetTranslatable(kFalse);
						listHelper.AddElement(subSectionName,kPublicationNameTextWidgetID);
					}							
					MediatorClass :: vector_subSectionSprayerListBoxParameters.push_back(tempClass);									
				}					
			}	

			if(vector_pubmodel)
				delete vector_pubmodel;

		}
		else if( MediatorClass :: global_project_level ==2)
		{
//CA("Level 2");				
			vector_pubmodel = ptrIAppFramework->getAllSubsectionsByPubIdAndLanguageId(rootId , language_id);
			if(vector_pubmodel == nil)
			{
				ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::populateSubsectionList::getAllSubsectionsByPubIdAndLanguageId::vector_pubmodel == nil");
				break;
			}
					
//CA("now");
			VectorPubModel::iterator it;
		
			for(it=vector_pubmodel->begin(); it!=vector_pubmodel->end(); it++,no_of_lstboxElements++)
			{
	
				CPubModel model = (*it);
				MediatorClass:: global_vector_pubmodel.push_back(model);
				PMString subSectionName(iConverter->translateString(model.getName()));
								
				subSectionSprayerListBoxParameters tempClass;

				subSectionData ssd;
				ssd.subSectionName = subSectionName;
				ssd.subSectionID = model.getPublicationID();
				ssd.sectionID = model.getPublicationID();  // For Project Level 2 fill section id same as Subsection id.
				ssd.number = model.getNumber();	

				tempClass.displayName = subSectionName;
				tempClass.isSelected = kTrue;
				tempClass.vec_ssd.push_back(ssd);
				//No sectionID and sectionName information.Default will be assigned.	

				if(shall_I_Fill_Data_Into_ListBox)
				{
					subSectionName.SetTranslatable(kFalse);
					listHelper.AddElement(subSectionName,kPublicationNameTextWidgetID);
				}
///////////////////////////// For hiding extra widgets used for manual spray in white board    from here
				vWidgetID.clear();
				vWidgetID.push_back(kListBoxProductStencilFileNameTextForManualWidgetID);
				vWidgetID.push_back(kSeparatorOnGrouPanel1ForManualWidgetID);
				vWidgetID.push_back(kListBoxItemStencilFileNameTextForManualWidgetID);
				vWidgetID.push_back(kListBoxStencilFileNameForSpecSheetTextWidgetID);//16-10-8
				listHelper.HideWidgets(MediatorClass::PublicationTemplateMappingListBox, vWidgetID, no_of_lstboxElements);
////////////////////////////////////////////////////////////////////////////////////////////     upto here
				MediatorClass :: vector_subSectionSprayerListBoxParameters.push_back(tempClass);
////////////

			}
			if(vector_pubmodel)
				delete vector_pubmodel;
		
				//PMString listSize;
				//listSize.AppendNumber(no_lstbox_elements);
				//CA(listSize);
		}
		
	}while(kFalse);
-*/
}

//aded on 31Oct...
void CMMDialogObserver::populateSubSectionListForSpread(bool16 shall_I_Fill_Data_Into_ListBox,double rootId)
{
/*-
	no_of_lstboxElements=0;// declared globally on 03-october
	//CA("1 Inside SPSelectionObserver::populateSubSectionListForSpread");
	//noOfRows = 0;	
	do
	{
		SDKListBoxHelper listHelper(this, kCMMPluginID, kCMMMTemplateFileListBoxWidgetID, kCMMDialogWidgetID);
		listHelper.EmptyCurrentListBox();
		
		//yet to define
		MediatorClass :: vector_subSectionSprayerListBoxParameters.clear();
		//MediatorClass ::PublicationTemplateMappingListBox

		
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
		{
			//CA("ptrIAppFramework == nil");
			break;
		}
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(!iConverter)
		{
			//CA("iConverter == nil");
			break;
		}

		MediatorClass :: global_project_level = ptrIAppFramework->getPM_Project_Levels();
		
		SectionData publdata;
		double sectionID = -1;
		publdata.clearSectionVector();
		publdata.clearSubsectionVector();
		//define global_vector_pubmodel
		MediatorClass:: global_vector_pubmodel.clear();

		double language_id = -1 ;
		//define global_lang_id
		language_id = MediatorClass :: global_lang_id ; //ptrIClientOptions->getDefaultLocale(language_name);
		VectorPubModelPtr vector_pubmodel = NULL;

		if( MediatorClass :: global_project_level == 3)

		{	
            //CA("Level 3");
			vector_pubmodel = ptrIAppFramework->getAllSectionsByPubIdAndLanguageId(rootId , language_id);
			if(vector_pubmodel == nil)
			{
				ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::populateSubsectionListForSpread::getAllSectionsByPubIdAndLanguageId::vector_pubmodel == nil");
				break;
			}
			VectorPubModel::iterator it;

			
			
			for(it=vector_pubmodel->begin(); it!=vector_pubmodel->end(); it++)
			{
					double sectionID=it->getPublicationID();
					PMString sectionName = it->getName();
					
					VectorPubModelPtr vectorSubSectionModel = ptrIAppFramework->getAllSubsectionsBySectionIdAndLanguageId(sectionID , language_id);
					if(vectorSubSectionModel == nil)
					{
						
						ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::populateSubsectionListSpread::getAllSubsectionsBySectionIdAndLanguageId::vectorSubSectionModel == nil");
						break;
					}
					
					VectorPubModel::iterator it_subsection;

					bool16 isFirstIteration = kTrue;

					subSectionSprayerListBoxParameters tempClass;
					tempClass.isSelected = kTrue;
					tempClass.vec_ssd.clear();
					tempClass.displayName = "";
					
					//whene pageOrder is left you have to add two subsection data in a vector.
					bool16 insertElementNow = kFalse;
					vector<CPubModel> :: iterator end = vectorSubSectionModel->end();
					for(it_subsection=vectorSubSectionModel->begin(); it_subsection!=end; it_subsection++)
					{

						CPubModel model = *(it_subsection);
						MediatorClass:: global_vector_pubmodel.push_back(model);
						PMString subSectionName(iConverter->translateString(model.getName()));

						subSectionData ssd;
						ssd.subSectionName = subSectionName;
						ssd.subSectionID = model.getPublicationID();
						ssd.sectionID = sectionID;
						ssd.sectionName = sectionName;						
						ssd.number = model.getNumber();

						if(isFirstIteration == kTrue)
						{
							isFirstIteration = kFalse;
							if(MediatorClass ::pageOrderSelectedIndex == 1)
							{
									tempClass.displayName = subSectionName;
									tempClass.vec_ssd.push_back(ssd);
									listHelper.AddElement(tempClass.displayName,kPublicationNameTextWidgetID);

									MediatorClass :: vector_subSectionSprayerListBoxParameters.push_back(tempClass);
									
									tempClass.vec_ssd.clear();
									tempClass.displayName.Clear();
									continue;
							}
						}

						if(insertElementNow == kFalse)
						{
							//Last subSection..it has no pair.We have to display it alone.
							if(it_subsection == end-1)
							{
								tempClass.displayName.Append(subSectionName);
								tempClass.vec_ssd.push_back(ssd);
								insertElementNow = kFalse;

								listHelper.AddElement(tempClass.displayName,kPublicationNameTextWidgetID);

								MediatorClass :: vector_subSectionSprayerListBoxParameters.push_back(tempClass);
								

								tempClass.vec_ssd.clear();
								tempClass.displayName.Clear();
							}
							else 
							{
								tempClass.displayName.Append(subSectionName);
								tempClass.vec_ssd.push_back(ssd);
								insertElementNow = kTrue;
							}
						}
						else
						{
							tempClass.displayName.Append("-" + subSectionName);
							tempClass.vec_ssd.push_back(ssd);
							insertElementNow = kFalse;

							listHelper.AddElement(tempClass.displayName,kPublicationNameTextWidgetID);

							MediatorClass :: vector_subSectionSprayerListBoxParameters.push_back(tempClass);
							

							tempClass.vec_ssd.clear();
							tempClass.displayName.Clear();

						}
							
			
					}
					
					
			}	
			if(vector_pubmodel)
				delete vector_pubmodel;

		}
		else if( MediatorClass :: global_project_level ==2)
		{
//CA("Level 2");				
			vector_pubmodel = ptrIAppFramework->getAllSubsectionsByPubIdAndLanguageId(rootId , language_id);
			if(vector_pubmodel == nil)
			{
				
				ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::populateSubSectionListForSpread::getAllSubsectionsByPubIdAndLanguageId::vector_pubmodel == nil");
				break;
			}
					
//CA("now");
			VectorPubModel::iterator it;
		
			bool16 isFirstIteration = kTrue;

			subSectionSprayerListBoxParameters tempClass;
			tempClass.isSelected = kTrue;
			tempClass.vec_ssd.clear();
			tempClass.displayName = "";
			
			//whene pageOrder is left you have to add two subsection data in a vector.
			bool16 insertElementNow = kFalse;
			vector<CPubModel> :: iterator end = vector_pubmodel->end();

			for(it=vector_pubmodel->begin(); it!=end; it++)
				{
	
					CPubModel model = (*it);
					MediatorClass:: global_vector_pubmodel.push_back(model);
					PMString subSectionName(iConverter->translateString(model.getName()));
									
						subSectionData ssd;
						ssd.subSectionName = subSectionName;
						ssd.subSectionID = model.getPublicationID();
						ssd.sectionID = model.getPublicationID();  // For Level 2 fill sectionId same as SubSection id 
						//ssd.sectionName = sectionName;						
						ssd.number = model.getNumber();

						if(isFirstIteration == kTrue)
						{
							isFirstIteration = kFalse;
							if(MediatorClass ::pageOrderSelectedIndex == 1)
							{
									tempClass.displayName = subSectionName;
									tempClass.vec_ssd.push_back(ssd);

									listHelper.AddElement(tempClass.displayName,kPublicationNameTextWidgetID);
///////////////////////////// For hiding extra widgets used for manual spray in white board    from here
									vWidgetID.clear();
									vWidgetID.push_back(kListBoxProductStencilFileNameTextForManualWidgetID);
									vWidgetID.push_back(kSeparatorOnGrouPanel1ForManualWidgetID);
									vWidgetID.push_back(kListBoxItemStencilFileNameTextForManualWidgetID);

									listHelper.HideWidgets(MediatorClass::PublicationTemplateMappingListBox, vWidgetID, no_of_lstboxElements);
////////////////////////////////////////////////////////////////////////////////////////////     upto here
									no_of_lstboxElements++;
									MediatorClass :: vector_subSectionSprayerListBoxParameters.push_back(tempClass);
									
									tempClass.vec_ssd.clear();
									tempClass.displayName.Clear();
									continue;
							}
						}

						if(insertElementNow == kFalse)
						{
							//Last subSection..it has no pair.We have to display it alone.
							if(it == end-1)
							{
								tempClass.displayName.Append(subSectionName);
								tempClass.vec_ssd.push_back(ssd);
								insertElementNow = kFalse;

								listHelper.AddElement(tempClass.displayName,kPublicationNameTextWidgetID);
///////////////////////////// For hiding extra widgets used for manual spray in white board    from here
								vWidgetID.clear();
								vWidgetID.push_back(kListBoxProductStencilFileNameTextForManualWidgetID);
								vWidgetID.push_back(kSeparatorOnGrouPanel1ForManualWidgetID);
								vWidgetID.push_back(kListBoxItemStencilFileNameTextForManualWidgetID);
								vWidgetID.push_back(kListBoxStencilFileNameForSpecSheetTextWidgetID);//16-10-8
								listHelper.HideWidgets(MediatorClass::PublicationTemplateMappingListBox, vWidgetID, no_of_lstboxElements);
////////////////////////////////////////////////////////////////////////////////////////////     upto here
								no_of_lstboxElements++;
								MediatorClass :: vector_subSectionSprayerListBoxParameters.push_back(tempClass);
								

								tempClass.vec_ssd.clear();
								tempClass.displayName.Clear();
							}
							else 
							{
								tempClass.displayName.Append(subSectionName);
								tempClass.vec_ssd.push_back(ssd);
								insertElementNow = kTrue;
							}
						}
						else
						{
							tempClass.displayName.Append("-" + subSectionName);
							tempClass.vec_ssd.push_back(ssd);
							insertElementNow = kFalse;

							listHelper.AddElement(tempClass.displayName,kPublicationNameTextWidgetID);
///////////////////////////// For hiding extra widgets used for manual spray in white board    from here
							vWidgetID.clear();
							vWidgetID.push_back(kListBoxProductStencilFileNameTextForManualWidgetID);
							vWidgetID.push_back(kSeparatorOnGrouPanel1ForManualWidgetID);
							vWidgetID.push_back(kListBoxItemStencilFileNameTextForManualWidgetID);
							vWidgetID.push_back(kListBoxStencilFileNameForSpecSheetTextWidgetID);//16-10-8
							listHelper.HideWidgets(MediatorClass::PublicationTemplateMappingListBox, vWidgetID, no_of_lstboxElements);
////////////////////////////////////////////////////////////////////////////////////////////     upto here
							no_of_lstboxElements++;
							MediatorClass :: vector_subSectionSprayerListBoxParameters.push_back(tempClass);
							

							tempClass.vec_ssd.clear();
							tempClass.displayName.Clear();

						}
							
										
				}

		}
			
		if(vector_pubmodel)
			delete vector_pubmodel;

		
	}while(kFalse);
-*/
}
//ended on 31Oct...
void fillpNodeDataList(double subSectionID,double sectionID,double publicationID,PMString subSectionName)//int32 curSelSubecId)
 {
	 do
	{	
		//The pNodeDataList is a global vector.
		pNodeDataList.clear();

		//9May PublicationNode pNode;	
		CreateMediaPublicationNode pNode;	

		//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			CA("ptrIAppFramework is nil");
			break;
		}

		//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		//int32 ParentTypeID = ptrIAppFramework->TYPEMngr_getObjectTypeID("PRODUCT_LEVEL");

		VectorPubObjectValuePtr VectorFamilyInfoValuePtr = nil;
		VectorFamilyInfoValuePtr = ptrIAppFramework->getProductsAndItemsForSection(subSectionID, MediatorClass ::global_lang_id);
				
		if(VectorFamilyInfoValuePtr == nil)
		{
			ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::fillpNodeDataList::GetProjectProducts_getProductsAndItemsForSubSection::VectorFamilyInfoValuePtr == nil");
			break;
		}

		if(VectorFamilyInfoValuePtr->size()==0)
		{
			ptrIAppFramework->LogInfo("AP46CreateMedia::CMMDialogObserver::fillpNodeDataList::VectorFamilyInfoValuePtr->size() == 0");
			delete VectorFamilyInfoValuePtr;
			break;
		}
	
		int32/*size_t*/ size =static_cast<int32> (VectorFamilyInfoValuePtr->size());		
		//VectorPubObjectValuePointer::iterator it1;
		VectorPubObjectValue::iterator it2;
		
		MediatorClass :: vecPageNo.clear();
		
			for(it2=VectorFamilyInfoValuePtr->begin(); it2 !=VectorFamilyInfoValuePtr->end(); it2++)
			{	

				
				pNode.setPBObjectID(it2->getPub_object_id());
				pNode.setSequence(it2->getSeq_order());
				pNode.setIsProduct(it2->getisProduct());
				int32 pageNo =it2->getPage_no();
				pNode.pageno = pageNo;
				if(it2->getNew_product())
					pNode.setNewProduct(1);
				else
					pNode.setNewProduct(0);
                //CA_NUM("pageNum :",pNode.pageno);
				if(it2->getisProduct() == 1)
				{					
					it2->getObjectValue().getName();				
					//ASD.AppendNumber(it2->getObjectValue().getLevel_no());
					//CA(it2->getObjectValue().getName());
					//-pNode.setLevel(it2->getObjectValue().getLevel_no());
					pNode.setParentId(it2->getObjectValue().getParent_id());					
					pNode.setPubId(it2->getObjectValue().getObject_id());

					PMString pName1 = it2->getName();
                    pNode.setPublicationName(pName1);
										
					pNode.setChildCount(it2->getObjectValue().getChildCount());
					//-pNode.setReferenceId(it2->getObjectValue().getRef_id());
					pNode.setTypeId(it2->getObjectValue().getObject_type_id());
                    pNode.setIsProduct(1);
								
				}
				if(it2->getisProduct() == 0)
				{
					pNode.setPubId(it2->getItemModel().getItemID());

					if(!iConverter)
					{
						PMString ItemNO = (it2->getName());
						//PMString ItemDesp(it2->getItemModel().getItemDesc());
						//if(ItemDesp != "")
						//{
						//	ItemNO.Append(" : "+ ItemDesp);
						//}
						pNode.setPublicationName(ItemNO);
                        pNode.setIsProduct(0);
                        pNode.setChildCount(0);
						//CA(ItemNO);
					}
					else
					{
						PMString ItemNO(iConverter->translateString(it2->getName()));
						//PMString ItemDesp(iConverter->translateString(it2->getItemModel().getItemDesc()));
						//if(ItemDesp != "")
						//{
						//	ItemNO.Append(" : "+ ItemDesp);
						//}
						pNode.setPublicationName(ItemNO);
                        pNode.setIsProduct(0);
                        pNode.setChildCount(0);
					}
					pNode.setTypeId(it2->getobject_type_id());
				}

				if(it2->getisProduct() == 2)
				{					
					it2->getObjectValue().getName();				
					//ASD.AppendNumber(it2->getObjectValue().getLevel_no());
					//CA(it2->getObjectValue().getName());
					//-pNode.setLevel(it2->getObjectValue().getLevel_no());
					pNode.setParentId(it2->getObjectValue().getParent_id());					
					pNode.setPubId(it2->getObjectValue().getObject_id());

					PMString pName1 = it2->getObjectValue().getName();
                    pNode.setPublicationName(pName1);
									
					pNode.setChildCount(it2->getObjectValue().getChildCount());
					//-pNode.setReferenceId(it2->getObjectValue().getRef_id());
					pNode.setTypeId(it2->getObjectValue().getObject_type_id());			
								
				}


				pNode.subSectionID = subSectionID;
				pNode.sectionID = sectionID;
				pNode.publicationID = publicationID;
				pNode.subSectionName = subSectionName;	

				pNodeDataList.push_back(pNode);	

			}

			if(VectorFamilyInfoValuePtr)
				delete VectorFamilyInfoValuePtr;

	}while(kFalse);
 }

 void fillpNodeDataListForSpread(double subSectionID,double sectionID,double publicationID,PMString subSectionName)
 {
	 do
	{	
		//The pNodeDataList is a global vector.
		//pNodeDataList.clear();
		CreateMediaPublicationNode pNode;	

		//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			CA("ptrIAppFramework is nil");
			break;
		}

		VectorPubObjectValuePtr VectorFamilyInfoValuePtr = nil;
		//-VectorFamilyInfoValuePtr = ptrIAppFramework->GetProjectProducts_getProductsAndItemsForSubSection(subSectionID, MediatorClass ::global_lang_id);
        VectorFamilyInfoValuePtr = ptrIAppFramework->getProductsAndItemsForSection(subSectionID, MediatorClass ::global_lang_id);
        
				
		if(VectorFamilyInfoValuePtr == nil)
		{
			ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::fillpNodeDataListForSpread::GetProjectProducts_getProductsAndItemsForSubSection::VectorFamilyInfoValuePtr == nil");
			break;
		}

		if(VectorFamilyInfoValuePtr->size()==0)
		{
			ptrIAppFramework->LogInfo("AP46CreateMedia::CMMDialogObserver::fillpNodeDataListForSpread::VectorFamilyInfoValuePtr->size() == 0");
			delete VectorFamilyInfoValuePtr;
			break;
		}
	
		int32 size =(int32) VectorFamilyInfoValuePtr->size();		//Cs4
		//VectorPubObjectValuePointer::iterator it1;
		VectorPubObjectValue::iterator it2;
		

			for(it2=VectorFamilyInfoValuePtr->begin(); it2 !=VectorFamilyInfoValuePtr->end(); it2++)
			{

				pNode.setPBObjectID(it2->getPub_object_id());
				pNode.setSequence(it2->getIndex());	
				pNode.setIsProduct(it2->getisProduct());
				if(it2->getNew_product())
					pNode.setNewProduct(1);
				else
					pNode.setNewProduct(0);

				int32 pageNo =1;//it2->getPage_no();
				pNode.pageno = pageNo;


				if(it2->getisProduct() ==1)
				{					
					it2->getObjectValue().getName();				
					//ASD.AppendNumber(it2->getObjectValue().getLevel_no());
					//CA(it2->getObjectValue().getName());
					//-pNode.setLevel(it2->getObjectValue().getLevel_no());
					pNode.setParentId(it2->getObjectValue().getParent_id());					
					pNode.setPubId(it2->getObjectValue().getObject_id());

					PMString pName1 = it2->getObjectValue().getName();
                    pNode.setPublicationName(pName1);
					
					pNode.setChildCount(it2->getObjectValue().getChildCount());
					//-pNode.setReferenceId(it2->getObjectValue().getRef_id());
					pNode.setTypeId(it2->getObjectValue().getObject_type_id());			
								
				}
				if(it2->getisProduct() ==0)
				{
					pNode.setPubId(it2->getItemModel().getItemID());

					if(!iConverter)
					{
						PMString ItemNO(it2->getItemModel().getItemNo());
						PMString ItemDesp(it2->getItemModel().getItemDesc());
						if(ItemDesp != "")
						{
							ItemNO.Append(" : "+ ItemDesp);
						}
						pNode.setPublicationName(ItemNO);
						//CA(ItemNO);
					}
					else
					{
						PMString ItemNO(iConverter->translateString(it2->getItemModel().getItemNo()));
						PMString ItemDesp(iConverter->translateString(it2->getItemModel().getItemDesc()));
						if(ItemDesp != "")
						{
							ItemNO.Append(" : "+ ItemDesp);
						}
						pNode.setPublicationName(ItemNO);						
					}
					pNode.setTypeId(it2->getobject_type_id());
				}

				if(it2->getisProduct() ==2)
				{					
					it2->getObjectValue().getName();				
					//ASD.AppendNumber(it2->getObjectValue().getLevel_no());
					//CA(it2->getObjectValue().getName());
					//-pNode.setLevel(it2->getObjectValue().getLevel_no());
					pNode.setParentId(it2->getObjectValue().getParent_id());					
					pNode.setPubId(it2->getObjectValue().getObject_id());

					PMString pName1 = it2->getObjectValue().getName();
                    pNode.setPublicationName(pName1);
					
					pNode.setChildCount(it2->getObjectValue().getChildCount());
					//-pNode.setReferenceId(it2->getObjectValue().getRef_id());
					pNode.setTypeId(it2->getObjectValue().getObject_type_id());			
								
				}
				
				pNode.subSectionID = subSectionID;
				pNode.sectionID = sectionID;
				pNode.publicationID = publicationID;
				pNode.subSectionName = subSectionName;						

				bool16 isPageNoInTheVector = kFalse;
				for(int32 index = 0;index<MediatorClass :: vecPageNo.size();index++)
				{
					if(pageNo == MediatorClass :: vecPageNo[index])
					{
						isPageNoInTheVector = kTrue;
						break;
					}
				}
				if(isPageNoInTheVector == kFalse)
				{
					MediatorClass :: vecPageNo.push_back(pageNo);
				}							
				pNodeDataList.push_back(pNode);				
			}	

			if(VectorFamilyInfoValuePtr)
				delete VectorFamilyInfoValuePtr;
        
	}while(kFalse);
 }

 void startCreatingMedia()
 {
//	CA("startCreatingMedia.......");
		InterfacePtr<IBookManager> bookManager(GetExecutionContextSession(), UseDefaultIID());
		if (bookManager == nil) 
		{ 
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMedia::bookManager == nil");
			return; 
		}
		IBook * activeBook = bookManager->GetCurrentActiveBook();
		if(activeBook == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMedia::activeBook == nil");
			return;			
		}
		
		//-IDFile file = activeBook->GetBookFileSpec();
        SDKFileHelper activeBookFileHelper(activeBook->GetBookFileSpec());
        PMString inddfilepath = activeBookFileHelper.GetPath();

		SDKUtilities sdkutils;
		

		sdkutils.RemoveLastElement(inddfilepath);
		sdkutils.AppendPathSeparator(inddfilepath);
		//CA(path);

		K2Vector<IDFile> contentFileList;
		//code start
			vector<subSectionSprayerListBoxParameters> & subSectionVector = MediatorClass ::vector_subSectionSprayerListBoxParameters;
			int32 subSectionsToBeSrpaydVectorSize =static_cast<int32> (subSectionVector.size()); //CS4
			
			if(subSectionsToBeSrpaydVectorSize > 0)
			{//if 2

				//CA("subSectionsToBeSprayedVectorSize > 0");
				//ICommandSequence *piCmdSequence =CmdUtils::BeginCommandSequence();

				for(int32 vectorElementIndex = 0;vectorElementIndex < subSectionsToBeSrpaydVectorSize;vectorElementIndex++)
				{//for 1
					
					//CA_NUM("vectorElementIndex : ",vectorElementIndex);
					subSectionSprayerListBoxParameters & subSectionListBoxParams = subSectionVector[vectorElementIndex];
					if(subSectionListBoxParams.isSelected == kTrue)
					{// 3

						if(vectorElementIndex > 0)
							CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 

						PMString templateFilePath = subSectionListBoxParams.masterFileWithCompletePath;
						//CA(templateFilePath);
						SDKFileHelper templateFile(templateFilePath);
						if(templateFilePath == "" || templateFile.IsExisting() == kFalse)
						{
							//CA(templateFilePath + " file doesn't exist");
							continue;
						}
						//1Nov..check this later
						double Nov1TempSubSecID = subSectionListBoxParams.vec_ssd[0].subSectionID;
						double Nov1TempSecID = subSectionListBoxParams.vec_ssd[0].sectionID;
						PMString Nov1TempSubSecName =subSectionListBoxParams.vec_ssd[0].subSectionName;
						//end 1Nov..check this later

						fillpNodeDataList(Nov1TempSubSecID,Nov1TempSecID,MediatorClass::currentSelectedPublicationID,Nov1TempSubSecName);//subSectionListBoxParams.subSectionID);						
						if(pNodeDataList.size() <= 0)
						{
							continue;
						}
						//////////25 july....copy templates to master code///////////////
						//27July//
						PMString path(inddfilepath);					
						//27July//

						//1Nov..Check this later.
						PMString tempSubSecName = subSectionListBoxParams.vec_ssd[0].subSectionName;
						//1Nove..Check this later.
						path.Append(tempSubSecName);
						//27July//
						path.Append(".indd");

						//27July//
                        PMString temp1("c:\\a.indd");
						bool16 result = ScriptingVersionOfCopySelectedItems(templateFilePath ,temp1 ,path);
						//27July//				
						if(result == kFalse)
							//27July//							
							continue;
						//27July//						
						templateFilePath = path;

						///////////25 july ..copy templates to master code//////////////
						SDKLayoutHelper sdklhelp;
						IDFile templateIDFile(templateFilePath);
						UIDRef templateDocUIDRef = sdklhelp.OpenDocument(templateIDFile);
						if(templateDocUIDRef == UIDRef ::gNull)
						{
							ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMedia::uidref of templatedoc is invalid");
							continue;
						}						
						MediatorClass ::currentProcessingDocUIDRef = templateDocUIDRef;

						ErrorCode err = sdklhelp.OpenLayoutWindow(templateDocUIDRef);
						if(err == kFailure)
						{
							CA("Error occured while opening the layoutwindow of the template");
							continue;
						}

						InterfacePtr<IDocument> doc(templateDocUIDRef,UseDefaultIID());
						if(doc == nil)
						{
							CA("doc == nil");
							continue;
						}
						InterfacePtr<ISpreadList>spreadList(doc,UseDefaultIID());
						if(spreadList == nil)
						{
							CA("spreadList == nil");
							continue;
						}
						IDataBase * database = templateDocUIDRef.GetDataBase();
						if(database == nil)
						{
							CA("database == nil");
							continue;
						}

						UIDRef firstSreadUIDRef(database, spreadList->GetNthSpreadUID(0)); 
						InterfacePtr<ISpread> spread(firstSreadUIDRef, IID_ISPREAD); 
						if(spread == nil)
						{
							CA("spread == nil");
							continue;
						}
						UIDList templateFrameUIDList(database);
						if(spread->GetNthPageUID(0)== kInvalidUID) 
						{
							CA("pageUID is invalid");
							continue;
						}		
							
						spread->GetItemsOnPage(0,&templateFrameUIDList,kFalse,kTrue);			
							
						
						InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection());
						if(iSelectionManager == nil)
						{
							ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMedia::iSelectionManger == nil");
							continue;
						}
						InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
						//InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(Utils<ISelectionUtils>()->QuerySuite(IID_ILAYOUTSELECTION_ISUITE ),UseDefaultIID());
						if (layoutSelectionSuite == nil) 
						{
							CA("layoutSelectionSuite == nil");
							continue;
						}						
						layoutSelectionSuite->/*Select*/SelectPageItems(templateFrameUIDList,Selection::kReplace,Selection::kAlwaysCenterInView);
						//global variable value assignments

						//1Nov..Check this later.
						double Nov1tempSecID = subSectionListBoxParams.vec_ssd[0].sectionID;
						double Nov1tempSubSecID = subSectionListBoxParams.vec_ssd[0].subSectionID;
						//1Nov..Check this later.
						CurrentSelectedSection = Nov1tempSecID;
						CurrentSelectedPublicationID = MediatorClass :: currentSelectedPublicationID;
						CurrentSelectedSubSection = Nov1tempSubSecID;
						global_project_level = MediatorClass :: global_project_level;
						global_lang_id =MediatorClass ::global_lang_id;

						if(MediatorClass ::Is_UseDifferentTemplateForEachSubSectionRadioSelected)
						{
							MediatorClass ::subSecSpraySttngs = subSectionListBoxParams.ssss;
						}
						
						SubSectionSprayer SSsp;
						//1Nov..Check this later.
						//PMString Nov1TempSubSecName = subSectionListBoxParams.vec_ssd[0].subSectionName;
						//end 1Nov..Check this later.
						SSsp.selectedSubSection = Nov1TempSubSecName;
						bool16 isCancelHit = kFalse;
						SSsp.startSprayingSubSection(isCancelHit);

						//continue;
						//start commented temporariliy on 25 july..copy template to master code
						//PMString path(inddfilepath);					
						//path.Append(SSsp.selectedSubSection);
						//path.Append(".indd");
						
						//CA(path);

						SDKUtilities sdkutil;
						IDFile docFile = sdkutil.PMStringToSysFile(&path);
			   		    ErrorCode status = sdklhelp.SaveDocumentAs(templateDocUIDRef,docFile);
						if(status == kFailure)
						{
							CA("document can not be saved");
							continue;						
						}
						//CA("1");
						
							//end commented temporariliy on 25 july..copy template to master code
						//27July//SDKUtilities sdkutil;
						//27July//IDFile docFile = sdkutil.PMStringToSysFile(&path);
						sdklhelp.CloseDocument(templateDocUIDRef,kTrue);
						contentFileList.push_back(docFile);
						
						if(isCancelHit == kTrue)
							break;


					}//end if 3

				}//end for 1
				//CA("latest 1");
				if(contentFileList.size()>0)
				{
					//CA("before addDocumentsToBook");
					addDocumentsToBook(contentFileList);
					//CA("after addDocumentsToBook");
				}
				//CA("latest 2");
//				CmdUtils::EndCommandSequence(piCmdSequence);	
			
			}//end if 2
			//code end
 }
void startCreatingMediaForSpread()
 {
		// CA("Inside startCreatingMediaForSpread ");
		InterfacePtr<IBookManager> bookManager(/*gSession*/GetExecutionContextSession(), UseDefaultIID()); //Cs4
		if (bookManager == nil) 
		{ 
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaForSpread::bookManager == nil");
			return; 
		}
		IBook * activeBook = bookManager->GetCurrentActiveBook();
		if(activeBook == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMedia::activeBook == nil");
			return;			
		}
		
		//IDFile file = activeBook->GetBookFileSpec();

		SDKUtilities sdkutils;
        SDKFileHelper activeBookFileHelper(activeBook->GetBookFileSpec());
        PMString inddfilepath = activeBookFileHelper.GetPath();

		sdkutils.RemoveLastElement(inddfilepath);
		sdkutils.AppendPathSeparator(inddfilepath);
		

		K2Vector<IDFile> contentFileList;
		contentFileList.clear();
		//code start
		vector<subSectionSprayerListBoxParameters> & subSectionVector = MediatorClass ::vector_subSectionSprayerListBoxParameters;
		int32 subSectionsToBeSrpaydVectorSize = static_cast<int32>(subSectionVector.size()); //CS4
		
			if(subSectionsToBeSrpaydVectorSize > 0)
			{//if 2
				//ICommandSequence *piCmdSequence =CmdUtils::BeginCommandSequence();
				pNodeDataList.clear();
				MediatorClass :: vecPageNo.clear();
				for(int32 vectorElementIndex = 0;vectorElementIndex < subSectionsToBeSrpaydVectorSize;vectorElementIndex++)
				{//for 1
					subSectionSprayerListBoxParameters & subSectionListBoxParams = subSectionVector[vectorElementIndex];
					
					//1Nov..Check this later.
					double Nov1TempSubSecID =subSectionListBoxParams.vec_ssd[0].subSectionID;
					double Nov1TempSecID =subSectionListBoxParams.vec_ssd[0].sectionID;
					PMString Nov1TempSubSecName = subSectionListBoxParams.vec_ssd[0].subSectionName;
					//end 1Nov..Check this later.
					fillpNodeDataListForSpread(Nov1TempSubSecID,Nov1TempSecID,MediatorClass :: currentSelectedPublicationID,Nov1TempSubSecName);
				}//end for 1
				filterTheProductListAccordingToPageNo();


			for(int32 pNodeListVectorIndex = 0;pNodeListVectorIndex < MediatorClass::vectorOfpNodeDataList.size();pNodeListVectorIndex++)
				{//for 2		
					if(kTrue)
					{//if 3
	//CA("5");			
						if(pNodeListVectorIndex > 0 )
							CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 

						UIDRef dataToBeSpreadDocumentUIDRef = UIDRef ::gNull;
						SDKLayoutHelper sdklhelp;
						pNodeDataList = MediatorClass::vectorOfpNodeDataList[pNodeListVectorIndex];
						if(pNodeDataList.size() <= 0){ CA("pNodeDataList.size() <= 0");
							continue;
						}
		//CA("6");						//
						if(MediatorClass ::isSingleStencilFileForProductAndItem)
						{
	
							PMString templateFilePath = MediatorClass :: templateFilePathForSpreadSpray;
							SDKFileHelper masterFile(templateFilePath);
							if(templateFilePath == "" || masterFile.IsExisting() == kFalse)
							{
								CA(templateFilePath + " file doesn't exist");
								continue;
							}
			//CA("7");				
							InterfacePtr<IPageItemScrapData>  scrapData;
							bool16 result =  copyStencilsFromTheTemplateDocumentIntoScrapData(MediatorClass :: AllStencilFilePath, scrapData);
			
							if(result == kFalse)
							{
								ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMedia::copyStencilsFromTheTemplateDocumentIntoScrapData::result == kFalse while copying");
								continue;
							}
				//CA("8");	
							PMString filePathItemsToBePastedIn(templateFilePath);
							IDFile dataToBeSpreadDocumentIDFile(filePathItemsToBePastedIn);
							dataToBeSpreadDocumentUIDRef = sdklhelp.OpenDocument(dataToBeSpreadDocumentIDFile);
							if(dataToBeSpreadDocumentUIDRef == UIDRef ::gNull)
							{
								CA("uidref of dataToBeSpreadDocumentUIDRef is invalid");
								continue;
							}						
					//CA("9");		
							ErrorCode err = sdklhelp.OpenLayoutWindow(dataToBeSpreadDocumentUIDRef);
							if(err == kFailure)
							{
								CA("Error occured while opening the layoutwindow of the dataToBeSprayedDocumentFileLayout");
								continue;
							}						
							result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapData,dataToBeSpreadDocumentUIDRef );
					//CA("10");					
		///////////////////////////////////////////////////////////////////////////////////////////////////					
		//following code is added to collect uidlist from master page for allStencilUIDList-----------from here	
							InterfacePtr<IDocument> dataToBeSprayedDocument1(dataToBeSpreadDocumentUIDRef,UseDefaultIID());
							if(dataToBeSprayedDocument1 == nil)
							{
								CA("dataToBeSprayedDocument1 == nil");
								break;
							}
		
							InterfacePtr<ISpreadList>dataToBeSprayedDocumentSpreadList1(dataToBeSprayedDocument1,UseDefaultIID());
							if(dataToBeSprayedDocumentSpreadList1 == nil)
							{
								CA("dataToBeSprayedDocumentSpreadList1 == nil");
								continue;
							}
		
							IDataBase * dataToBeSprayedDocumentDatabase1 = dataToBeSpreadDocumentUIDRef.GetDataBase();
							if(dataToBeSprayedDocumentDatabase1 == nil)
							{
								CA("dataToBeSprayedDocumentDatabase1 == nil");
								continue;
							}
		
							UIDRef dataToBeSprayedDocumentFirstSpreadUIDRef1(dataToBeSprayedDocumentDatabase1, dataToBeSprayedDocumentSpreadList1->GetNthSpreadUID(0)); 

							InterfacePtr<ISpread> dataToBeSprayedDocumentFirstSpread1(dataToBeSprayedDocumentFirstSpreadUIDRef1, IID_ISPREAD); 
							if(dataToBeSprayedDocumentFirstSpread1 == nil)
							{
								CA("spread == nil");
								continue;
							}
			
							UIDList dataToBeSprayedDocumentFrameUIDList1(dataToBeSprayedDocumentDatabase1);
							if(dataToBeSprayedDocumentFirstSpread1->GetNthPageUID(0)== kInvalidUID) 
							{
								CA("pageUID is invalid");
								continue;
							}		
				
		//In following code we are getting UIDList for all items on master page and storing it in mediator class
		//variable MediatorClass ::allStencilUIDList
							UIDList tempUIDListForALL(dataToBeSprayedDocumentDatabase1);
							dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&tempUIDListForALL,kFalse,kTrue);

							
							MediatorClass ::allStencilUIDList = tempUIDListForALL;
							
		//////////////////////////////-------------------------------------------------------------upto here					
						}
						else
						{

							PMString templateFilePath = MediatorClass :: templateFilePathForSpreadSpray;
							SDKFileHelper masterFile(templateFilePath);
							if(templateFilePath == "" || masterFile.IsExisting() == kFalse)
							{
								CA(templateFilePath + " file doesn't exist");
								continue;
							}

										
							//CA("subSectionListBoxParams.ssss.ItemStencilFilePath : " +subSectionListBoxParams.ssss.ItemStencilFilePath);
							InterfacePtr<IPageItemScrapData>  scrapData;
							bool16 result =  copyStencilsFromTheTemplateDocumentIntoScrapData(MediatorClass :: ItemStencilFilePath, scrapData);
								
							if(result == kFalse)
							{
								CA("result == kFalse while copying");
								continue;
							}
				
							PMString filePathItemsToBePastedIn(templateFilePath);
							IDFile dataToBeSpreadDocumentIDFile(templateFilePath);
							dataToBeSpreadDocumentUIDRef = sdklhelp.OpenDocument(dataToBeSpreadDocumentIDFile);
							if(dataToBeSpreadDocumentUIDRef == UIDRef ::gNull)
							{
								CA("uidref of dataToBeSpreadDocumentUIDRef is invalid");
								continue;
							}						
						
							ErrorCode err = sdklhelp.OpenLayoutWindow(dataToBeSpreadDocumentUIDRef);

							if(err == kFailure)
							{
									CA("Error occured while opening the layoutwindow of the dataToBeSprayedDocumentFileLayout");
									continue;
							}						
							result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapData,dataToBeSpreadDocumentUIDRef );
							

		////////////////////////////////////////////////////////////////////////////////////////////////////////
		//code to colloect list of uidref on master document for items added by vijay on 14/8/2006
							InterfacePtr<IDocument> dataToBeSprayedDocument1(dataToBeSpreadDocumentUIDRef,UseDefaultIID());
							if(dataToBeSprayedDocument1 == nil)
							{
								CA("dataToBeSprayedDocument1 == nil");
								break;
							}

							InterfacePtr<ISpreadList>dataToBeSprayedDocumentSpreadList1(dataToBeSprayedDocument1,UseDefaultIID());
							if(dataToBeSprayedDocumentSpreadList1 == nil)
							{
								CA("dataToBeSprayedDocumentSpreadList1 == nil");
								continue;
							}

							IDataBase * dataToBeSprayedDocumentDatabase1 = dataToBeSpreadDocumentUIDRef.GetDataBase();
							if(dataToBeSprayedDocumentDatabase1 == nil)
							{
								CA("dataToBeSprayedDocumentDatabase1 == nil");
								continue;
							}

							UIDRef dataToBeSprayedDocumentFirstSpreadUIDRef1(dataToBeSprayedDocumentDatabase1, dataToBeSprayedDocumentSpreadList1->GetNthSpreadUID(0)); 

							InterfacePtr<ISpread> dataToBeSprayedDocumentFirstSpread1(dataToBeSprayedDocumentFirstSpreadUIDRef1, IID_ISPREAD); 
							if(dataToBeSprayedDocumentFirstSpread1 == nil)
							{
								CA("spread == nil");
								continue;
							}

							UIDList dataToBeSprayedDocumentFrameUIDList1(dataToBeSprayedDocumentDatabase1);
							if(dataToBeSprayedDocumentFirstSpread1->GetNthPageUID(0)== kInvalidUID) 
							{
								CA("pageUID is invalid");
								continue;
							}		
								
							UIDList tempUIDListForItem(dataToBeSprayedDocumentDatabase1);
							dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&tempUIDListForItem,kFalse);
						
							InterfacePtr<IPageItemScrapData>  scrapDataForProduct;

							result =  copyStencilsFromTheTemplateDocumentIntoScrapData(MediatorClass :: ProductStencilFilePath, scrapDataForProduct);
							if(result == kFalse)
							{
								ptrIAppFramework->LogDebug("aP46CreateMedia::CMMDialogObserver::startCreatingMediaForSpread::copyStencilsFromTheTemplateDocumentIntoScrapData::result == kFalse while copying");
								continue;
							}

							result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapDataForProduct,dataToBeSpreadDocumentUIDRef );
							
							dataToBeSprayedDocumentFrameUIDList1.Clear();

							UIDList temp2UIDList(dataToBeSprayedDocumentDatabase1);
							dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&temp2UIDList,kFalse);			
											
		/////////////////////////////////////////////////////////////////////////////////////////////
		//following code is added by vijay choudhari on 14/8/2006 to separate product list from 
		//itemAndProductStencilUIDList.........................................from here
					
							

							UIDList tempUIDListForProduct(dataToBeSprayedDocumentDatabase1); 
						
							for( int32 i = 0 ; i < temp2UIDList.Length() ; i++)
							{
								bool16 isUIDFound = kFalse;
								for(int32 j = 0 ; j < tempUIDListForItem.Length() ; j++)
								{
									if(temp2UIDList[i] == tempUIDListForItem[j])
									{
										isUIDFound = kTrue;
										break;
									}
								}
								if(isUIDFound == kFalse)
									tempUIDListForProduct.Append(temp2UIDList[i]);
							}

							MediatorClass ::productStencilUIDList = tempUIDListForProduct;
							MediatorClass ::itemStencilUIDList =  tempUIDListForItem;
					
					
						} // end else
						
										
				
						InterfacePtr<IDocument> dataToBeSprayedDocument(dataToBeSpreadDocumentUIDRef,UseDefaultIID());
						if(dataToBeSprayedDocument == nil)
						{
							CA("dataToBeSprayedDocument == nil");
							break;
						}
						InterfacePtr<ISpreadList>dataToBeSprayedDocumentSpreadList(dataToBeSprayedDocument,UseDefaultIID());
						if(dataToBeSprayedDocumentSpreadList == nil)
						{
							CA("dataToBeSprayedDocumentSpreadList == nil");
							continue;
						}
						IDataBase * dataToBeSprayedDocumentDatabase = dataToBeSpreadDocumentUIDRef.GetDataBase();
						if(dataToBeSprayedDocumentDatabase == nil)
						{
							CA("dataToBeSprayedDocumentDatabase == nil");
							continue;
						}

						UIDRef dataToBeSprayedDocumentFirstSpreadUIDRef(dataToBeSprayedDocumentDatabase, dataToBeSprayedDocumentSpreadList->GetNthSpreadUID(0)); 
						InterfacePtr<ISpread> dataToBeSprayedDocumentFirstSpread(dataToBeSprayedDocumentFirstSpreadUIDRef, IID_ISPREAD); 
						if(dataToBeSprayedDocumentFirstSpread == nil)
						{
							CA("spread == nil");
							continue;
						}
						UIDList dataToBeSprayedDocumentFrameUIDList(dataToBeSprayedDocumentDatabase);
						if(dataToBeSprayedDocumentFirstSpread->GetNthPageUID(0)== kInvalidUID) 
						{
							CA("pageUID is invalid");
							continue;
						}		
						dataToBeSprayedDocumentFirstSpread->GetItemsOnPage(0,&dataToBeSprayedDocumentFrameUIDList,kFalse);			
						
						InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection());
						if(iSelectionManager == nil)
						{
							CA("iSelectionManger == nil");
							continue;
						}
						InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
						//InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(Utils<ISelectionUtils>()->QuerySuite(IID_ILAYOUTSELECTION_ISUITE ),UseDefaultIID());
						if (layoutSelectionSuite == nil) 
						{
							CA("layoutSelectionSuite == nil");
							continue;
						}	

						layoutSelectionSuite->/*Select*/SelectPageItems(dataToBeSprayedDocumentFrameUIDList,Selection::kReplace,Selection::kAlwaysCenterInView);
				
						global_project_level = MediatorClass :: global_project_level;
						global_lang_id =MediatorClass ::global_lang_id;
						
						MediatorClass ::currentProcessingDocUIDRef = dataToBeSpreadDocumentUIDRef;
						
						SubSectionSprayer SSsp;							
							
						PMString path(inddfilepath);		
							
						bool16 isCancelHit = kFalse;
						SSsp.startSprayingSubSection(isCancelHit);									
						
						 
						int32 firstProductPageNo = pNodeDataList[0].pageno;
						int32 lastProductPageNo = pNodeDataList[pNodeDataList.size() - 1].pageno;
						if(firstProductPageNo == lastProductPageNo)
						{
							path.AppendNumber(firstProductPageNo);
						}
						else
						{
						 	path.AppendNumber(firstProductPageNo);
						 	path.Append("-");
						 	path.AppendNumber(lastProductPageNo);
						 }
						 path.Append(".indd");
					 //CA(path);

						SDKUtilities sdkutil;
						IDFile docFile = sdkutil.PMStringToSysFile(&path);
			   		    ErrorCode status = sdklhelp.SaveDocumentAs(dataToBeSpreadDocumentUIDRef,docFile);
						if(status == kFailure)
						{
							CA("document can not be saved");
							continue;						
						}
						//CA("1");
						sdklhelp.CloseDocument(dataToBeSpreadDocumentUIDRef);
						contentFileList.push_back(docFile);
						
						if(isCancelHit == kTrue)
							break;


						/* 6Jun commented
						int32  firstPageNum =-1;
						PMString pageRangeString;
						
						

						//contentFileList.clear();
						

						
						IBook * thisBook = activeBook;
						IDFile activeBookIDFile = thisBook->GetBookFileSpec();
						
						
						piCmdSequence->SetName("Add Document to Book");

						InterfacePtr<ICommand>piCreateContentCmd(CmdUtils::CreateCommand(kConstructContentCmdBoss) );
						if (!piCreateContentCmd)
						{
							CA("!piCreateContentCmd");
							break;
						}

						InterfacePtr<IBookContentCmdData>
						piBookContentCmdData(piCreateContentCmd, IID_IBOOKCONTENTCMDDATA);
						if(piBookContentCmdData == nil)
						{
							CA("piBookContentCmdData == nil");
							break;
						}
						piBookContentCmdData->SetTargetBook(activeBookIDFile);
						piBookContentCmdData->SetContentFile(contentFileList);
						
						ErrorCode result;
						//CA("before 1");
						if ((result = CmdUtils::ProcessCommand(piCreateContentCmd)) !=
						kSuccess)
						break;
						//CA("after 1");
						//test 2
						
						const UIDList *list = piCreateContentCmd->GetItemList();
						if(list == nil)
						{
							CA("list == nil"); 
							break;
						}
						if(list->Length()<= 0)
						{
							CA("list->Length()<= 0");
							break;
						}

						UIDList* nonConstList = const_cast<UIDList*>(list);
						

						InterfacePtr<ICommand> addDocToBookCmd(CmdUtils::CreateCommand(kAddDocToBookCmdBoss));
						if(addDocToBookCmd == nil)
						{
							CA("addDocToBookCmd == nil");
							continue;
						}
						InterfacePtr<IBookContentCmdData> bookContentCmdData(addDocToBookCmd, UseDefaultIID());
						if(bookContentCmdData == nil)
						{
							CA("bookContentCmdData == nil");
							continue;
						}						
						bookContentCmdData->SetTargetBook(activeBookIDFile);
						//CA("upto last 2");
						bookContentCmdData->SetContentFile(contentFileList);
						//CA("upto last 2.1");
						bookContentCmdData->SetContentList(nonConstList);
						//CA("upto last 2.2");
						InterfacePtr<IBookContentMgr> contentMgr(thisBook, UseDefaultIID());
						if(contentMgr == nil)
						{
							CA("contentMgr == nil");
							break;
						}
						//CA("upto last 3.3");
						int32 destPosition = contentMgr->GetContentCount() - 1; 
						PMString dest("The destPosition is ");
						dest.AppendNumber(destPosition);
						//CA(dest);
						//CA("upto last 3.4");
						bookContentCmdData->SetDestPosition(destPosition); 
						//CA("upto last 3.5");
						status = CmdUtils::ProcessCommand(addDocToBookCmd); 
						
						
			//CA("6");
			const UIDList *addList = addDocToBookCmd->GetItemList();
			if (!(result == kSuccess && addList->Length() > 0))
			{
				CA("!(result == kSuccess && addList->Length() > 0)");
				break;
			}
		//CA("7");


			InterfacePtr<ICommand>
			piRepaginateCmd(CmdUtils::CreateCommand(kSetRepaginationCmdBoss));
			if (!piRepaginateCmd)
			{
				CA("!piRepaginateCmd");
				break;
			}

			// Get repagination options from the book and apply the same to
			//the command
			InterfacePtr<IBookPaginateOptions> piPaginateOptions(thisBook,IID_IBOOKPAGINATEOPTIONS);
			IBookPaginateOptions::Options options =	piPaginateOptions->GetPaginateOptions();
			bool16 bInsert = piPaginateOptions->GetInsertBlankPage();
			bool16 bAutoRepaginate =piPaginateOptions->GetAutoRepaginateFlag();
			bool16 bForceToRepaginate = kTrue;

			InterfacePtr<IBookPaginateOptions>piPaginateCmdData(piRepaginateCmd, IID_IBOOKPAGINATEOPTIONS);
			piPaginateCmdData->SetPaginateOptions(options);
			piPaginateCmdData->SetInsertBlankPage(bInsert);
			piPaginateCmdData->SetAutoRepaginateFlag(bAutoRepaginate);
			UIDRef bookUIDRef = ::GetUIDRef(activeBook);
			piPaginateCmdData->SetCurrentBookUIDRef(bookUIDRef);

			InterfacePtr<IIntData> piPosition(piRepaginateCmd,IID_IINTDATA);
			piPosition->Set(destPosition);

			InterfacePtr<IBoolData> piForceRepaginate(piRepaginateCmd,IID_IBOOLDATA);
			piForceRepaginate->Set(bForceToRepaginate);

			result = CmdUtils::ProcessCommand(piRepaginateCmd);
			

		//CA("upto last 3.5");
						
						//CA("upto last 4");
						InterfacePtr<ICommand>saveBookCmd(CmdUtils::CreateCommand(kSaveBookCmdBoss));
						if(saveBookCmd == nil)
						{
							CA("saveBookCmd == nil");
							break;
						}
		//CA("upto last 3.6");
						bookUIDRef = ::GetUIDRef(activeBook);
						UIDList bookUIDList(bookUIDRef);
						saveBookCmd->SetItemList(bookUIDList);
						//CA("5");
		//CA("upto last 3.7");
						status = CmdUtils::ProcessCommand(saveBookCmd);

				//6Jun end comment
				*/

											
					}//end if 3


				}//end for 1
				if(contentFileList.size()>0)
				{
					//CA("before addDocumentsToBook");
					addDocumentsToBook(contentFileList);
					//CA("after addDocumentsToBook");
				}
				//CmdUtils::EndCommandSequence(piCmdSequence);	
				
			}//end if 2
			//code end
	//CA("end");
 }

 void filterTheProductListAccordingToPageNo()
 {

	 sort(MediatorClass ::vecPageNo.begin(),MediatorClass ::vecPageNo.end());
	
	 //for CAlert
	 PMString data;
	 for(int32 i = 0;i<MediatorClass ::vecPageNo.size();i++)
	 {
		 data.AppendNumber(MediatorClass ::vecPageNo[i]);
		 data.Append("\n");
	 }
	//CA(data);
	 //end for CAlert
	 MediatorClass ::vectorOfpNodeDataList.clear();
	 for(int32 pagevectorIndex = 0;pagevectorIndex <MediatorClass ::vecPageNo.size();pagevectorIndex++)
	 {//for 1
		//9May PublicationNodeList tempPublicationNodeList;
		 CreateMediaPublicationNodeList tempPublicationNodeList;
		 for(int32 index = 0;index < pNodeDataList.size();index++)
			{//for 1.1
				if(pNodeDataList[index].pageno == MediatorClass ::vecPageNo[pagevectorIndex])
				{
					tempPublicationNodeList.push_back(pNodeDataList[index]);
				}				 
			}//end for 1.1
		
		 if((MediatorClass :: pageOrderSelectedIndex == 0)
			 ||
			 ((MediatorClass :: pageOrderSelectedIndex == 1) && (pagevectorIndex != 0))
			 )
		 {//if 1
			pagevectorIndex = pagevectorIndex + 1;
			if(pagevectorIndex <MediatorClass ::vecPageNo.size())
			{//if 1.1
			
				for(int32 index = 0;index < pNodeDataList.size();index++)
				{//for					
					if(pNodeDataList[index].pageno == MediatorClass ::vecPageNo[pagevectorIndex])
					{
						tempPublicationNodeList.push_back(pNodeDataList[index]);
					}
				}//end for
			}//end if 1.1
		 
		 }//end if 1	
		
		 MediatorClass ::vectorOfpNodeDataList.push_back(tempPublicationNodeList);
	 }//end for 1

	 PMString data1;
	 for(int32 index = 0;index < MediatorClass ::vectorOfpNodeDataList.size();index++)
	 {
		 CreateMediaPublicationNodeList & tempPublicationNodeList = MediatorClass ::vectorOfpNodeDataList[index];
		 for(int32 innerIndex=0;innerIndex < tempPublicationNodeList.size();innerIndex++)
		 {
			 CreateMediaPublicationNode & pNode = tempPublicationNodeList[innerIndex];
			 data1.Append(pNode.strPublicationName);
			 data1.Append(" : ");
			 data1.AppendNumber(pNode.pageno);
		 }
		
		 data1.Append("\n------\n");
		 
	 }
	 //CA(data1);

 }

 //void appendpNodeDataListForSpread()
 //{
	 //for(int32 index = 0;index < pNodeDataList.size();index++)
	// {
		 //MediatorClass :: spreadpNodeDataList.push_back(pNodeDataList[index]);
	// }
// }
/*
//This function is not required as the SDKLayoutHelper's CloseDocument is working fine
bool16 closeTheDocument(UIDRef & docUIDRef)
 {
	 
	InterfacePtr<ICommand>closeDocCmd(CmdUtils :: CreateCommand(kCloseDocCmdBoss));
	if(closeDocCmd == nil)
	{
		CA("closeDocCmd == nil");
		return kFalse;
	}
	UIDList docUIDList(docUIDRef);
	closeDocCmd->SetItemList(docUIDList);
	ErrorCode status = CmdUtils :: ProcessCommand(closeDocCmd);
	if(status == kFailure)
		return kFalse;

	return kTrue;

 }
*/

ErrorCode addDocumentsToBook(K2Vector<IDFile> & contentFileList)
 {
	
	ErrorCode status = kFailure;
	InterfacePtr<IBookManager> bookManager(/*gSession*/GetExecutionContextSession(), UseDefaultIID()); //Cs4
	if (bookManager == nil) 
	{ 
		ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::addDocumentsToBook::bookManager == nil");
		return status; 
	}
	IBook * currentActiveBook = bookManager->GetCurrentActiveBook();
	if(currentActiveBook == nil)
	{
		ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::addDocumentsToBook::currentActiveBook == nil");
		return status;			
	}
	 int32  firstPageNum =-1;
	 PMString pageRangeString;
	 //contentFileList.clear();
	 //contentFileList.push_back(docFile);	
	 IDFile activeBookIDFile = currentActiveBook->GetBookFileSpec();
						
							
						//piCmdSequence->SetName("Add Document to Book");
	//ICommandSequence *piCmdSequence =CmdUtils::BeginCommandSequence();
	//piCmdSequence->SetName("Add Document to Book");
	do
	{
		InterfacePtr<ICommand>piCreateContentCmd(CmdUtils::CreateCommand(kConstructContentCmdBoss) );
		if (!piCreateContentCmd)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::addDocumentsToBook::!piCreateContentCmd");
			break;
		}	
		InterfacePtr<IBookContentCmdData>
		piBookContentCmdData(piCreateContentCmd, IID_IBOOKCONTENTCMDDATA);
		if(piBookContentCmdData == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::addDocumentsToBook::piBookContentCmdData == nil");
			break;
		}
		piBookContentCmdData->SetTargetBook(activeBookIDFile);
		piBookContentCmdData->SetContentFile(contentFileList);
		
		
		//CA("before 1");
		if ((status = CmdUtils::ProcessCommand(piCreateContentCmd)) !=kSuccess)
		break;
		//CA("after 1");
		//test 2
		
		const UIDList *list = piCreateContentCmd->GetItemList();
		if(list == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::addDocumentsToSpread::list == nil"); 
			break;
		}
		if(list->Length()<= 0)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::addDocumentsToBook::list->Length()<= 0");
			break;
		}

		UIDList* nonConstList = const_cast<UIDList*>(list);
		

		InterfacePtr<ICommand> addDocToBookCmd(CmdUtils::CreateCommand(kAddDocToBookCmdBoss));
		if(addDocToBookCmd == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::addDocumentsToBook::addDocToBookCmd == nil");
			break;
		}
		InterfacePtr<IBookContentCmdData> bookContentCmdData(addDocToBookCmd, UseDefaultIID());
		if(bookContentCmdData == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::addDocumentsToBook::bookContentCmdData == nil");
			break;
		}						
		bookContentCmdData->SetTargetBook(activeBookIDFile);
		//CA("upto last 2");
		bookContentCmdData->SetContentFile(contentFileList);
		//CA("upto last 2.1");
		bookContentCmdData->SetContentList(nonConstList);
		//CA("upto last 2.2");
		InterfacePtr<IBookContentMgr> contentMgr(currentActiveBook, UseDefaultIID());
		if(contentMgr == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::addDocumentsToBook::contentMgr == nil");
			break;
		}
		//CA("upto last 3.3");
		int32 destPosition = contentMgr->GetContentCount() - 1; 

		/*	PMString dest("The destPosition is ");
		dest.AppendNumber(destPosition);
		CA(dest);*/
		//CA("upto last 3.4");
		bookContentCmdData->SetDestPosition(destPosition); 
		//CA("upto last 3.5");
		status = CmdUtils::ProcessCommand(addDocToBookCmd); 
					
					
		//CA("6");
		const UIDList *addList = addDocToBookCmd->GetItemList();
		if (!(status == kSuccess && addList->Length() > 0))
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::addDocumentsToBook::!(status == kSuccess && addList->Length() > 0)");
			break;
		}
		

		UIDRef bookUIDRef = ::GetUIDRef(currentActiveBook);

		//CA("7");
		{
			InterfacePtr<ICommand>saveBookCmd(CmdUtils::CreateCommand(kSaveBookCmdBoss));
			if(saveBookCmd == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::addDocumentsToSpread::saveBookCmd == nil");
				break;
			}

			bookUIDRef = ::GetUIDRef(currentActiveBook);
			UIDList bookUIDList(bookUIDRef);
			saveBookCmd->SetItemList(bookUIDList);
			status = CmdUtils::ProcessCommand(saveBookCmd); 

		}
// commented by rahul for avoiding crash
		InterfacePtr<ICommand>
		piRepaginateCmd(CmdUtils::CreateCommand(kSetRepaginationCmdBoss));
		if (!piRepaginateCmd)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::addDocumentsToBook::!piRepaginateCmd");
			break;
		}

		// Get repagination options from the book and apply the same to
		//the command
		InterfacePtr<IBookPaginateOptions> piPaginateOptions(currentActiveBook,IID_IBOOKPAGINATEOPTIONS);
		IBookPaginateOptions::Options options =	piPaginateOptions->GetPaginateOptions();
		bool16 bInsert = piPaginateOptions->GetInsertBlankPage();
		bool16 bAutoRepaginate =piPaginateOptions->GetAutoRepaginateFlag();
		bool16 bForceToRepaginate = kTrue;

		InterfacePtr<IBookPaginateOptions>piPaginateCmdData(piRepaginateCmd, IID_IBOOKPAGINATEOPTIONS);
		piPaginateCmdData->SetPaginateOptions(/*options*/IBookPaginateOptions::kNextPage );
		piPaginateCmdData->SetInsertBlankPage(/*bInsert*/kFalse);
		piPaginateCmdData->SetAutoRepaginateFlag(/*bAutoRepaginate*/kTrue);
			
		piPaginateCmdData->SetCurrentBookUIDRef(bookUIDRef);

		InterfacePtr<IIntData> piPosition(piRepaginateCmd,IID_IINTDATA);
		piPosition->Set(destPosition);

		InterfacePtr<IBoolData> piForceRepaginate(piRepaginateCmd,IID_IBOOLDATA);
		piForceRepaginate->Set(bForceToRepaginate);
//CA("8");
		status = CmdUtils::ProcessCommand(piRepaginateCmd);
//CA("8.5");
		InterfacePtr<ICommand>saveBookCmd(CmdUtils::CreateCommand(kSaveBookCmdBoss));
		if(saveBookCmd == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::addDocumentsToSpread::saveBookCmd == nil");
			break;
		}

		bookUIDRef = ::GetUIDRef(currentActiveBook);
		UIDList bookUIDList(bookUIDRef);
		saveBookCmd->SetItemList(bookUIDList);
		status = CmdUtils::ProcessCommand(saveBookCmd); 

	}while(kFalse);
	//CmdUtils::EndCommandSequence(piCmdSequence);	
	return status;
	//Comment end on 6Jun
	
 }


 ////////////////////////////////////////////////////////////////////////////
 bool16 ScriptingVersionOfCopySelectedItems(PMString & filePathItemsToBeCopiedFrom,PMString & filePathItemsToBePastedIn,PMString & fileToBeSavedAs)
{
	bool16 result = kFalse;
	do
	{
		SDKLayoutHelper sdklhelp;
		IDFile templateIDFile(filePathItemsToBeCopiedFrom);
		UIDRef templateDocUIDRef = sdklhelp.OpenDocument(templateIDFile);
		if(templateDocUIDRef == UIDRef ::gNull)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::uidref of templatedoc is invalid");
			continue;
		}						
		
		ErrorCode err = sdklhelp.OpenLayoutWindow(templateDocUIDRef);
		if(err == kFailure)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::Error occured while opening the layoutwindow of the template");
			continue;
		}

		InterfacePtr<IDocument> doc(templateDocUIDRef,UseDefaultIID());
		if(doc == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::doc == nil");
			continue;
		}
		InterfacePtr<ISpreadList>spreadList(doc,UseDefaultIID());
		if(spreadList == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::spreadList == nil");
			continue;
		}
		IDataBase * database = templateDocUIDRef.GetDataBase();
		if(database == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::database == nil");
			continue;
		}

		UIDRef firstSreadUIDRef(database, spreadList->GetNthSpreadUID(0)); 
		InterfacePtr<ISpread> spread(firstSreadUIDRef, IID_ISPREAD); 
		if(spread == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::spread == nil");
			continue;
		}
		UIDList templateFrameUIDList(database);
		if(spread->GetNthPageUID(0)== kInvalidUID) 
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::pageUID is invalid");
			continue;
		}		
			
		spread->GetItemsOnPage(0,&templateFrameUIDList,kFalse);			
			
			

		InterfacePtr<ICommand> copyStencilsCMD(CmdUtils::CreateCommand(kCopyCmdBoss));
		if(copyStencilsCMD == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::copyStencilsCMD == nil");
			break;

		}
		InterfacePtr<ICopyCmdData> cmdData(copyStencilsCMD, IID_ICOPYCMDDATA);
		if(cmdData == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::cmdData == nil");
			break;
		}
		// Copy cmd will own this list
		UIDList* listCopy = new UIDList(templateFrameUIDList);
		InterfacePtr<IClipboardController> clipboardController(/*gSession*/GetExecutionContextSession(),UseDefaultIID());//Cs4
		if(clipboardController == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::clipboardController == nil");
			break;
			
		}
		ErrorCode status = clipboardController->PrepareForCopy();
		if(status == kFailure)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::status == kFailure");
			break;
		}
		InterfacePtr<IDataExchangeHandler> scrapHandler(clipboardController->QueryHandler(kPageItemFlavor));
		if(scrapHandler == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::scrapHandler == nil");
			break;
		}
		clipboardController->SetActiveScrapHandler(scrapHandler);

		InterfacePtr<IPageItemScrapData> scrapData(scrapHandler, UseDefaultIID());
		if(scrapData == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::scrapData == nil");
			break;
		}
		// specify where to copy item
		UIDRef parent = scrapData->GetRootNode();
		cmdData->Set(copyStencilsCMD, listCopy, parent, scrapHandler);
		status = CmdUtils::ProcessCommand(copyStencilsCMD);
		if(status == kFailure)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::status == kFailure");
			break;
		}
		sdklhelp.CloseDocument(templateDocUIDRef);
		////////////////////				
		IDFile documentIDFile(filePathItemsToBePastedIn);
		UIDRef documentDocUIDRef = sdklhelp.OpenDocument(documentIDFile);
		if(documentDocUIDRef == UIDRef ::gNull)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::uidref of documentDocUIDRef is invalid");
			continue;
		}						
		
		err = sdklhelp.OpenLayoutWindow(documentDocUIDRef);
		if(err == kFailure)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::Error occured while opening the layoutwindow of the dataToBeSprayedDocumentFileLayout");
			continue;
		}

		InterfacePtr<IDocument> dataToBeSprayedDocument(documentDocUIDRef,UseDefaultIID());
		if(dataToBeSprayedDocument == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::dataToBeSprayedDocument == nil");
			continue;
		}
		InterfacePtr<ISpreadList>dataToBeSprayedDocumentSpreadList(dataToBeSprayedDocument,UseDefaultIID());
		if(dataToBeSprayedDocumentSpreadList == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::dataToBeSprayedDocumentSpreadList == nil");
			continue;
		}
		IDataBase * dataToBeSprayedDocDatabase = documentDocUIDRef.GetDataBase();
		if(dataToBeSprayedDocDatabase == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::dataToBeSprayedDocDatabase == nil");
			continue;
		}

		UIDRef dataToBeSprayedDocFirstSpreadUIDRef(dataToBeSprayedDocDatabase, dataToBeSprayedDocumentSpreadList->GetNthSpreadUID(0)); 
	
		////////////////////


// sample code to paste items from scrap
		if (scrapData->IsEmpty()==kFalse)
		{
			InterfacePtr<IPageItemScrapData> scrapData(scrapHandler, UseDefaultIID());
			if(scrapData == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::scrapData == nil");
				break;
			}
			//This will give the list of items present on the scrap
			UIDList* scrapContents = scrapData->CreateUIDList();
			if (scrapContents->Length() >= 1)
			{
//CA("copying the scrapContents to the copiedUIDList");

//copiedUIDList = (*scrapContents);
	

				UIDRef spreadUIDRef;
				//ScriptingVersionOfGetUIDRefOfLastSpread(spreadUIDRef);
				spreadUIDRef = dataToBeSprayedDocFirstSpreadUIDRef;
				//SDKLayoutHelper sdkLayoutHelper;
				//get UIDRef of layer you want to paste to
				UIDRef parentLayerUIDRef =sdklhelp.GetSpreadLayerRef(spreadUIDRef);
				if(parentLayerUIDRef.GetUID() == kInvalidUID)
				{
					ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::parentLayerUIDRef.GetUID() == kInvalidUID");
					break;
				}
				InterfacePtr<ICommand> pasteToClipBoardCMD (CmdUtils::CreateCommand(kPasteCmdBoss));
				if(pasteToClipBoardCMD == nil)
				{
					ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::pasteToClipBoardCMD == nil");
					break;
				}
				InterfacePtr<ICopyCmdData> cmdData(pasteToClipBoardCMD, UseDefaultIID());
				if(cmdData == nil)
				{
					ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::cmdData == nil");
					break;
				}
				if(scrapContents == nil)
				{
					ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::ScriptingVersionOfCopySelectedItems::scrapContents == nil");
					break;
				}
				if(scrapContents == nil || parentLayerUIDRef == UIDRef::gNull || pasteToClipBoardCMD == nil)
				{	
					break;
				}
				cmdData->Set(pasteToClipBoardCMD, scrapContents, parentLayerUIDRef);
				//PMRect offset = Ö; // pasteboard coordinates of paste destination
				//cmdData->SetOffset(offset);
				status = CmdUtils::ProcessCommand(pasteToClipBoardCMD); 
				if(status == kSuccess)
				{
					//CA("operation successful");						
					//const PBPMPoint moveByPoints(0.0, 0.0);
					//for(int32 i = /*copiedBoxUIDList*/(*scrapContents).Length() - 1; i >= 0; --i)
					//{
						//CA("inside for loop");
						//UIDRef boxUIDRef = (*scrapContents).GetRef(i);
						//InterfacePtr<ITransform> transform(boxUIDRef, IID_ITRANSFORM);
						//MovePageItemRelative(transform, moveByPoints);
					//}
					result = kTrue;
				}
				SDKUtilities sdkutil;					
				IDFile docFile = sdkutil.PMStringToSysFile(&fileToBeSavedAs);
				sdklhelp.SaveDocumentAs(documentDocUIDRef,docFile);
				sdklhelp.CloseDocument(documentDocUIDRef);
			} 
		}
	}while(kFalse);
return result;
}
//=========================================================================================================
bool16 startCreatingMediaNew()
 {//start function startCreatingMediaNew 
	
	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
		return kFalse;

	InterfacePtr<IBookManager> bookManager(/*gSession*/GetExecutionContextSession(), UseDefaultIID()); //Cs4
	if (bookManager == nil) 
	{ 
		ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaNew::bookManager == nil");
		return kFalse; 
	}
	
	IBook * activeBook = bookManager->GetCurrentActiveBook();
	if(activeBook == nil)
	{
		ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaNew::activeBook == nil");
		return kFalse;			
	}
	//To get Actual File name in the file system.
	//-IDFile file = activeBook->GetBookFileSpec();

	SDKUtilities sdkutils;
	//-PMString inddfilepath = file.GetString();
    SDKFileHelper activeBookFileHelper(activeBook->GetBookFileSpec());
    PMString inddfilepath = activeBookFileHelper.GetPath();

	sdkutils.RemoveLastElement(inddfilepath);
	sdkutils.AppendPathSeparator(inddfilepath);	
//CA("inddfilepath : " + inddfilepath);

	K2Vector<IDFile> contentFileList;
		
	vector<subSectionSprayerListBoxParameters> & subSectionVector = MediatorClass ::vector_subSectionSprayerListBoxParameters;
	int32 subSectionsToBeSrpaydVectorSize = static_cast<int32>(subSectionVector.size()); //Cs4
//CA("B");
	//PMString AllStencilFilePath,ItemStencilFilePath,ProductStencilFilePath,HybridTableStencilFilePath;


	//30Oct..temporarily commented
	/*
	if(MediatorClass ::Is_UseDifferentTemplateForEachSubSectionRadioSelected == kFalse)
	{
			AllStencilFilePath = MediatorClass ::subSecSpraySttngs.AllStencilFilePath;
			ItemStencilFilePath = MediatorClass ::subSecSpraySttngs.ItemStencilFilePath;
			ProductStencilFilePath = MediatorClass ::subSecSpraySttngs.ProductStencilFilePath;
			
	}
	*/
	
			
	if(subSectionsToBeSrpaydVectorSize > 0)
	{//start if subSectionsToBeSrpaydVectorSize > 0


		for(int32 vectorElementIndex = 0;vectorElementIndex < subSectionsToBeSrpaydVectorSize;vectorElementIndex++)
		{//start for z

			
			subSectionSprayerListBoxParameters & subSectionListBoxParams = subSectionVector[vectorElementIndex];
			
			if(subSectionListBoxParams.isSelected == kTrue)
			{
				//CA("subSectionListBoxParams.isSelected == kTrue");	
				//start if x
				//following is the path of master file i.e. templateFilePath
				PMString masterFilePath = subSectionListBoxParams.masterFileWithCompletePath;
				//added by Tushar on 25/01/07
				PMString productStencilFilePath = subSectionListBoxParams.ProductStencilFilePath;
				PMString itemStencilFilePath = subSectionListBoxParams.ItemStencilFilePath;
				
				PMString hybridTableStencilFilePath = subSectionListBoxParams.HybridTableStencilFilePath;
				
				PMString sectionStencilFilePath = subSectionListBoxParams.SectionStencilFilePath;

				//CA("productStencilFilePath :" + productStencilFilePath);
				//CA("itemStencilFilePath :" + itemStencilFilePath);
				//CA("hybridTableStencilFilePath : " + hybridTableStencilFilePath);

				SDKFileHelper productStencilFile(productStencilFilePath);
				SDKFileHelper itemStencilFile(itemStencilFilePath);
				SDKFileHelper hybridTableStencilFile(hybridTableStencilFilePath); /////////	Added by Amit on 4/9/07 for Hybrid Table
				
				if(masterFilePath == "")
				{
					CAlert::InformationAlert("Select master file.");
					return kFalse;
				}
				
				if(productStencilFilePath == "" && itemStencilFilePath == "" && hybridTableStencilFilePath == "")
				{
					CAlert::InformationAlert("Select template files to spray.");
					return kFalse;
				}
				
				
				
				//if(productStencilFilePath == "" && itemStencilFilePath != "")
				//{
				//	//CAlert::InformationAlert("stencil for item is selected.");
				//	if(itemStencilFile.IsExisting() == kFalse)
				//	{
				//		CAlert::InformationAlert("item stencil file : " + itemStencilFilePath + " not found.");
				//		continue;
				//	}
				//	subSectionListBoxParams.AllStencilFilePath = itemStencilFilePath;
				//	subSectionListBoxParams.isSingleStencilFileForProductAndItem = kTrue;
				//}
				//
				//if(productStencilFilePath != "" && itemStencilFilePath == "")
				//{
				//	//CAlert::InformationAlert("stencil for product is selected.");
				//	if(productStencilFile.IsExisting() == kFalse)
				//	{
				//		CAlert::InformationAlert("product stencil file : " + productStencilFilePath + " not found.");
				//		continue;
				//	}
				//	subSectionListBoxParams.AllStencilFilePath = productStencilFilePath;
				//	subSectionListBoxParams.isSingleStencilFileForProductAndItem = kTrue;
				//}
				//upto here 25/01/07

				////////////////		Added By Amit on 13/9/07

////////////////	if only one File path is selected
				
				if(itemStencilFilePath != "" && productStencilFilePath == ""  && hybridTableStencilFilePath == "" )
				{
					//CAlert::InformationAlert("stencil for item is selected.");
					if(itemStencilFile.IsExisting() == kFalse)
					{
						CAlert::InformationAlert("item template file : " + itemStencilFilePath + " not found.");
						continue;
					}
					subSectionListBoxParams.AllStencilFilePath = itemStencilFilePath;
					subSectionListBoxParams.isSingleStencilFileForProductAndItem = kTrue;
				}
				
				if(productStencilFilePath != "" && itemStencilFilePath == "" && hybridTableStencilFilePath == "")
				{
					//CAlert::InformationAlert("stencil for product is selected.");
					if(productStencilFile.IsExisting() == kFalse)
					{
						CAlert::InformationAlert("product template file : " + productStencilFilePath + " not found.");
						continue;
					}
					subSectionListBoxParams.AllStencilFilePath = productStencilFilePath;
					subSectionListBoxParams.isSingleStencilFileForProductAndItem = kTrue;
				}
				
				if(hybridTableStencilFilePath != "" && productStencilFilePath == ""  && itemStencilFilePath == "" )
				{
					//CAlert::InformationAlert("stencil for Hybid Table is selected.");
					if(hybridTableStencilFile.IsExisting() == kFalse)
					{
						CAlert::InformationAlert("hybridTable template file : " + hybridTableStencilFilePath + " not found.");
						continue;
					}
					subSectionListBoxParams.AllStencilFilePath = hybridTableStencilFilePath;
					subSectionListBoxParams.isSingleStencilFileForProductAndItem = kTrue;
				}
//////////		end		on 13/9/07		
				if(itemStencilFilePath != "" && productStencilFilePath != ""  && hybridTableStencilFilePath == "" )
				{
					//CAlert::InformationAlert("stencil of product is selected for hybridTable");
					if(itemStencilFile.IsExisting() == kFalse)
					{
						CAlert::InformationAlert("item template file : " + itemStencilFilePath + " not found.");
						continue;
					}
					
					if(productStencilFile.IsExisting() == kFalse)
					{
						CAlert::InformationAlert("product template file : " + productStencilFilePath + " not found.");
						continue;
					}
					
					subSectionListBoxParams.HybridTableStencilFilePath =productStencilFilePath;
					subSectionListBoxParams.isSingleStencilFileForProductAndItem = kFalse;
				}
				
				if(itemStencilFilePath == "" && productStencilFilePath != ""  && hybridTableStencilFilePath != "" )
				{
					//CAlert::InformationAlert("stencil of product is selected for item");
					if(hybridTableStencilFile.IsExisting() == kFalse)
					{
						CAlert::InformationAlert("hybridTable template file : " + hybridTableStencilFilePath + " not found.");
						continue;
					}
					
					if(productStencilFile.IsExisting() == kFalse)
					{
						CAlert::InformationAlert("product template file : " + productStencilFilePath + " not found.");
						continue;
					}
					
					subSectionListBoxParams.ItemStencilFilePath = productStencilFilePath;
					subSectionListBoxParams.isSingleStencilFileForProductAndItem = kFalse;
				}

				if(itemStencilFilePath != "" && productStencilFilePath == ""  && hybridTableStencilFilePath != "" )
				{
					//CAlert::InformationAlert("stencil of item is selected for product");
					if(productStencilFile.IsExisting() == kFalse)
					{
						CAlert::InformationAlert("product template file : " + productStencilFilePath + " not found.");
						continue;
					}
					
					if(itemStencilFile.IsExisting() == kFalse)
					{
						CAlert::InformationAlert("item template file : " + itemStencilFilePath + " not found.");
						continue;
					}
					
					subSectionListBoxParams.ProductStencilFilePath = itemStencilFilePath;
					subSectionListBoxParams.isSingleStencilFileForProductAndItem = kFalse;
				}
				
				if(sectionStencilFilePath !="")
					subSectionListBoxParams.SectionStencilFilePath = sectionStencilFilePath;

////////////////		End	

				
				if(vectorElementIndex > 0)
					CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 

				UIDRef dataToBeSpreadDocumentUIDRef	;			
				PMString path(inddfilepath);	
				//1Nov..Check this later.
				PMString Nov1TempDocSaveAsName = subSectionListBoxParams.displayName;
				//CA("Nov1TempDocSaveAsName : " + Nov1TempDocSaveAsName);
				//end 1Nov..Check this later.
				path.Append(Nov1TempDocSaveAsName);
				path.Append(".indd");				
				bool16 result;
				SDKLayoutHelper sdklhelp;
										
				if(subSectionListBoxParams.isSingleStencilFileForProductAndItem)
				{	//start if y
					//CA("subSectionListBoxParams.isSingleStencilFileForProductAndItem True");
					SDKFileHelper masterFile(masterFilePath);
					if(masterFilePath == "" || masterFile.IsExisting() == kFalse)
					{
						CAlert::InformationAlert("Master file : " + masterFilePath + " not found.");
						continue;
					}
		
					/*PMString ASD("Before Calling fillpNodeDataList in startCreateMediaNew : subSectionListBoxParams.vec_ssd[0].subSectionID : ");
					ASD.AppendNumber(subSectionListBoxParams.vec_ssd[0].subSectionID);
					ASD.Append(" \n   subSectionListBoxParams.vec_ssd[0].sectionID : ");
					ASD.AppendNumber(subSectionListBoxParams.vec_ssd[0].sectionID);
					ASD.Append(" \n   MediatorClass :: currentSelectedPublicationID : ");
					ASD.AppendNumber(MediatorClass :: currentSelectedPublicationID);
					ASD.Append(" \n " + subSectionListBoxParams.vec_ssd[0].subSectionName );
					CA(ASD);*/

					fillpNodeDataList(subSectionListBoxParams.vec_ssd[0].subSectionID,
						subSectionListBoxParams.vec_ssd[0].sectionID,
						MediatorClass :: currentSelectedPublicationID,
						subSectionListBoxParams.vec_ssd[0].subSectionName);//Nov1TempSubSecID);						
					
					if(pNodeDataList.size() <= 0)
					{
						continue;
					}
					InterfacePtr<IPageItemScrapData>  scrapData;
					result =  copyStencilsFromTheTemplateDocumentIntoScrapData(subSectionListBoxParams.AllStencilFilePath, scrapData);
					if(result == kFalse)
					{
						ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaNew::result == kFalse while copying");
						continue;
					}
		
					PMString filePathItemsToBePastedIn(masterFilePath);
					IDFile dataToBeSpreadDocumentIDFile(filePathItemsToBePastedIn);
					dataToBeSpreadDocumentUIDRef = sdklhelp.OpenDocument(dataToBeSpreadDocumentIDFile);
					if(dataToBeSpreadDocumentUIDRef == UIDRef ::gNull)
					{
						CA("uidref of dataToBeSpreadDocumentUIDRef is invalid");
						continue;
					}						
				
					ErrorCode err = sdklhelp.OpenLayoutWindow(dataToBeSpreadDocumentUIDRef);
					if(err == kFailure)
					{
						CA("Error occured while opening the layoutwindow of the dataToBeSprayedDocumentFileLayout");
						continue;
					}		

					result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapData,dataToBeSpreadDocumentUIDRef );

					///////////////////////////////////////////////////////////////////////////////////////////////////					
				//following code is added to collect uidlist from master page for allStencilUIDList-----------from here	
					InterfacePtr<IDocument> dataToBeSprayedDocument1(dataToBeSpreadDocumentUIDRef,UseDefaultIID());
					if(dataToBeSprayedDocument1 == nil)
					{
						//CA("dataToBeSprayedDocument1 == nil");
						break;
					}

					InterfacePtr<ISpreadList>dataToBeSprayedDocumentSpreadList1(dataToBeSprayedDocument1,UseDefaultIID());
					if(dataToBeSprayedDocumentSpreadList1 == nil)
					{
						//CA("dataToBeSprayedDocumentSpreadList1 == nil");
						continue;
					}

					IDataBase * dataToBeSprayedDocumentDatabase1 = dataToBeSpreadDocumentUIDRef.GetDataBase();
					if(dataToBeSprayedDocumentDatabase1 == nil)
					{
						//CA("dataToBeSprayedDocumentDatabase1 == nil");
						continue;
					}

					UIDRef dataToBeSprayedDocumentFirstSpreadUIDRef1(dataToBeSprayedDocumentDatabase1, dataToBeSprayedDocumentSpreadList1->GetNthSpreadUID(0)); 

					InterfacePtr<ISpread> dataToBeSprayedDocumentFirstSpread1(dataToBeSprayedDocumentFirstSpreadUIDRef1, IID_ISPREAD); 
					if(dataToBeSprayedDocumentFirstSpread1 == nil)
					{
						//CA("spread == nil");
						continue;
					}

					UIDList dataToBeSprayedDocumentFrameUIDList1(dataToBeSprayedDocumentDatabase1);
					if(dataToBeSprayedDocumentFirstSpread1->GetNthPageUID(0)== kInvalidUID) 
					{
						//CA("pageUID is invalid");
						continue;
					}		
		
					//In following code we are getting UIDList for all items on master page and storing it in mediator class
					//variable MediatorClass ::allStencilUIDList
					UIDList tempUIDListForALL(dataToBeSprayedDocumentDatabase1);
					dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&tempUIDListForALL,kFalse);
					MediatorClass ::allStencilUIDList = tempUIDListForALL;					
//////////////////////////////-------------------------------------------------------------upto here
				}//end if y
				else
				{
					//CA("else ..");
					SDKFileHelper masterFile(masterFilePath);
					if(masterFilePath == "" || masterFile.IsExisting() == kFalse)
					{
						CA(masterFilePath + " file doesn't exist");
						continue;
					}

				/*	PMString ASD("Before Calling fillpNodeDataList in startCreateMediaNew : subSectionListBoxParams.vec_ssd[0].subSectionID : ");
					ASD.AppendNumber(subSectionListBoxParams.vec_ssd[0].subSectionID);
					ASD.Append(" \n   subSectionListBoxParams.vec_ssd[0].sectionID : ");
					ASD.AppendNumber(subSectionListBoxParams.vec_ssd[0].sectionID);
					ASD.Append(" \n   MediatorClass :: currentSelectedPublicationID : ");
					ASD.AppendNumber(MediatorClass :: currentSelectedPublicationID);
					ASD.Append(" \n " + subSectionListBoxParams.vec_ssd[0].subSectionName );
					CA(ASD);*/

					fillpNodeDataList(subSectionListBoxParams.vec_ssd[0].subSectionID,
						subSectionListBoxParams.vec_ssd[0].sectionID,
						MediatorClass :: currentSelectedPublicationID,
						subSectionListBoxParams.vec_ssd[0].subSectionName);//Nov1TempSubSecID);						
					
					if(pNodeDataList.size() <= 0)
					{
						continue;
					}
					
					//CA("subSectionListBoxParams.ssss.ItemStencilFilePath : " +subSectionListBoxParams.ssss.ItemStencilFilePath);
					InterfacePtr<IPageItemScrapData>  scrapData;
				    result =  copyStencilsFromTheTemplateDocumentIntoScrapData(subSectionListBoxParams.ItemStencilFilePath, scrapData);
						
					if(result == kFalse)
					{
						ptrIAppFramework->LogDebug("result == kFalse while copying");
						continue;
					}
		
					PMString filePathItemsToBePastedIn(masterFilePath);
					IDFile dataToBeSpreadDocumentIDFile(masterFilePath);
					dataToBeSpreadDocumentUIDRef = sdklhelp.OpenDocument(dataToBeSpreadDocumentIDFile);
					if(dataToBeSpreadDocumentUIDRef == UIDRef ::gNull)
					{
						CA("uidref of dataToBeSpreadDocumentUIDRef is invalid");
						continue;
					}						
				
					ErrorCode err = sdklhelp.OpenLayoutWindow(dataToBeSpreadDocumentUIDRef);

					if(err == kFailure)
					{
						CA("Error occured while opening the layoutwindow of the dataToBeSprayedDocumentFileLayout");
						continue;
					}						
					result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapData,dataToBeSpreadDocumentUIDRef );
					

////////////////////////////////////////////////////////////////////////////////////////////////////////
//code to colloect list of uidref on master document for items added by vijay on 14/8/2006
					InterfacePtr<IDocument> dataToBeSprayedDocument1(dataToBeSpreadDocumentUIDRef,UseDefaultIID());
					if(dataToBeSprayedDocument1 == nil)
					{
						//CA("dataToBeSprayedDocument1 == nil");
						break;
					}

					InterfacePtr<ISpreadList>dataToBeSprayedDocumentSpreadList1(dataToBeSprayedDocument1,UseDefaultIID());
					if(dataToBeSprayedDocumentSpreadList1 == nil)
					{
						//CA("dataToBeSprayedDocumentSpreadList1 == nil");
						continue;
					}

					IDataBase * dataToBeSprayedDocumentDatabase1 = dataToBeSpreadDocumentUIDRef.GetDataBase();
					if(dataToBeSprayedDocumentDatabase1 == nil)
					{
						CA("dataToBeSprayedDocumentDatabase1 == nil");
						continue;
					}

					UIDRef dataToBeSprayedDocumentFirstSpreadUIDRef1(dataToBeSprayedDocumentDatabase1, dataToBeSprayedDocumentSpreadList1->GetNthSpreadUID(0)); 

					InterfacePtr<ISpread> dataToBeSprayedDocumentFirstSpread1(dataToBeSprayedDocumentFirstSpreadUIDRef1, IID_ISPREAD); 
					if(dataToBeSprayedDocumentFirstSpread1 == nil)
					{
						CA("spread == nil");
						continue;
					}

					UIDList dataToBeSprayedDocumentFrameUIDList1(dataToBeSprayedDocumentDatabase1);
					if(dataToBeSprayedDocumentFirstSpread1->GetNthPageUID(0)== kInvalidUID) 
					{
						CA("pageUID is invalid");
						continue;
					}

					UIDList tempUIDListForItem(dataToBeSprayedDocumentDatabase1);
					dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&tempUIDListForItem,kFalse,kTrue);

				
					InterfacePtr<IPageItemScrapData>  scrapDataForProduct;

					result =  copyStencilsFromTheTemplateDocumentIntoScrapData(subSectionListBoxParams.ProductStencilFilePath, scrapDataForProduct);
					if(result == kFalse)
					{
						//CA("result == kFalse while copying");
						continue;
					}

		
					result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapDataForProduct,dataToBeSpreadDocumentUIDRef );

					dataToBeSprayedDocumentFrameUIDList1.Clear();

					UIDList temp2UIDList(dataToBeSprayedDocumentDatabase1);
					dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&temp2UIDList,kFalse,kTrue);

					///////////////////		Added by Amit on 5/9/07 for Hybrid Table

					InterfacePtr<IPageItemScrapData> scrapDataForHybridTable;
					//CA("subSectionListBoxParams.HybridTableStencilFilePath	"+subSectionListBoxParams.HybridTableStencilFilePath);
					result =  copyStencilsFromTheTemplateDocumentIntoScrapData(subSectionListBoxParams.HybridTableStencilFilePath, scrapDataForHybridTable);
					if(result == kFalse)
					{
						//CA("result == kFalse while copying");
						continue;
					}

					result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapDataForHybridTable,dataToBeSpreadDocumentUIDRef );
					if(result == kFalse)
					{
			//			CA("result == kFalse while pasting");
						continue;
					}
					dataToBeSprayedDocumentFrameUIDList1.Clear();

					UIDList temp3UIDList(dataToBeSprayedDocumentDatabase1);
					dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&temp3UIDList,kFalse,kTrue);
					


					if(subSectionListBoxParams.SectionStencilFilePath != "")
					{
						InterfacePtr<IPageItemScrapData> scrapDataForSection;
						result =  copyStencilsFromTheTemplateDocumentIntoScrapData(subSectionListBoxParams.SectionStencilFilePath , scrapDataForSection);
						if(result == kFalse)
						{
							//CA("result == kFalse while copying");
							continue;
						}
	
						result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapDataForSection,dataToBeSpreadDocumentUIDRef );
						if(result == kFalse)
						{
							//CA("result == kFalse while pasting");
							continue;
						}
						dataToBeSprayedDocumentFrameUIDList1.Clear();
					}
					UIDList tempSectionStencilUIDList(dataToBeSprayedDocumentDatabase1);

					dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&tempSectionStencilUIDList,kFalse,kTrue);
					
///////////////////		End

/////////////////////////////////////////////////////////////////////////////////////////////
//following code is added by vijay choudhari on 14/8/2006 to separate product list from 
//itemAndProductStencilUIDList.........................................from here
					UIDList tempUIDListForProduct(dataToBeSprayedDocumentDatabase1); 
					UIDList tempUIDListForHybridTable(dataToBeSprayedDocumentDatabase1); 
					UIDList tempUIDListForSection(dataToBeSprayedDocumentDatabase1);

					for( int32 i = 0 ; i < tempSectionStencilUIDList.Length() ; i++)
					{
						bool16 isUIDFound = kFalse;
						for(int32 j = 0 ; j < temp3UIDList.Length() ; j++)
						{
							if(tempSectionStencilUIDList[i] == temp3UIDList[j])
							{
								isUIDFound = kTrue;
								break;
							}
						}
						if(isUIDFound == kFalse)
							tempUIDListForSection.Append(tempSectionStencilUIDList[i]);
					}

					for( int32 i = 0 ; i < temp3UIDList.Length() ; i++)
					{
						bool16 isUIDFound = kFalse;
						for(int32 j = 0 ; j < temp2UIDList.Length() ; j++)
						{
							if(temp3UIDList[i] == temp2UIDList[j])
							{
								isUIDFound = kTrue;
								break;
							}
							
						}
						if(isUIDFound == kFalse)
							tempUIDListForHybridTable.Append(temp3UIDList[i]);
					}
///////////////////		End  

					for( int32 i = 0 ; i < temp2UIDList.Length() ; i++)
					{
						bool16 isUIDFound = kFalse;
						for(int32 j = 0 ; j < tempUIDListForItem.Length() ; j++)
						{
							if(temp2UIDList[i] == tempUIDListForItem[j])
							{
								isUIDFound = kTrue;
								break;
							}
						}
						if(isUIDFound == kFalse)
							tempUIDListForProduct.Append(temp2UIDList[i]);
					}

					MediatorClass ::productStencilUIDList = tempUIDListForProduct;
					MediatorClass ::itemStencilUIDList =  tempUIDListForItem;
					MediatorClass ::hybridTableStencilUIDList =  tempUIDListForHybridTable;
					MediatorClass ::sectionStencilUIDList  = tempUIDListForSection;
					
					/*PMString siz("tempUIDListForItem.size = ");
					siz.AppendNumber(tempUIDListForItem.Length());
					siz.Append (" , tempUIDListForHybridTable.size = ");
					siz.AppendNumber(tempUIDListForHybridTable.Length());
					CA(siz);*/
				}




//////===========================================================================upto here 


//following code is temporarily commented by vijay choudhari on 12/8/2006
//for testing............................................................
		//				SDKFileHelper templateFile(templateFilePath);
		//				if(templateFilePath == "" || templateFile.IsExisting() == kFalse)
		//				{
		//					//CA(templateFilePath + " file doesn't exist");
		//					continue;
		//				}

		//				fillpNodeDataList(subSectionListBoxParams.subSectionID);						
		//				if(pNodeDataList.size() <= 0)
		//				{
		//					continue;
		//				}
		//				//////////25 july....copy templates to master code///////////////
		//				//27July//
		//				PMString path(inddfilepath);					
		//				//27July//
		//				path.Append(subSectionListBoxParams.subSectionName);
		//				//27July//
		//				path.Append(".indd");

		//				//27July//
		//				/////start copy the stencils from the template document////////
		//				InterfacePtr<IPageItemScrapData>  scrapData;
		//			    bool16 result =  copyStencilsFromTheTemplateDocumentIntoScrapData(templateFilePath, scrapData);
		//				if(result == kFalse)
		//				{
		//					CA("result == kFalse while copying");
		//					continue;
		//				}
		///////end copy stencils from the template document//////////////
		//				/////////////***********************start*****************//////////////
		//				SDKLayoutHelper sdklhelp;
		//				PMString filePathItemsToBePastedIn("c:\\test\\aa.indd");
		//				IDFile dataToBeSpreadDocumentIDFile(filePathItemsToBePastedIn);
		//				UIDRef dataToBeSpreadDocumentUIDRef = sdklhelp.OpenDocument(dataToBeSpreadDocumentIDFile);
		//				if(dataToBeSpreadDocumentUIDRef == UIDRef ::gNull)
		//				{
		//					CA("uidref of dataToBeSpreadDocumentUIDRef is invalid");
		//					continue;
		//				}						
		//		
		//				ErrorCode err = sdklhelp.OpenLayoutWindow(dataToBeSpreadDocumentUIDRef);
		//				if(err == kFailure)
		//				{
		//						CA("Error occured while opening the layoutwindow of the dataToBeSprayedDocumentFileLayout");
		//						continue;
		//				}						
		//				result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapData,dataToBeSpreadDocumentUIDRef );
		//				
//////////////////////////////--------------------------------------------------upto HERE
				if(result == kFalse)
					continue;

				masterFilePath = path;
				InterfacePtr<IDocument> dataToBeSprayedDocument(dataToBeSpreadDocumentUIDRef,UseDefaultIID());
				if(dataToBeSprayedDocument == nil)
				{
					CA("dataToBeSprayedDocument == nil");
					return kFalse;
				}
				InterfacePtr<ISpreadList>dataToBeSprayedDocumentSpreadList(dataToBeSprayedDocument,UseDefaultIID());
				if(dataToBeSprayedDocumentSpreadList == nil)
				{
					CA("dataToBeSprayedDocumentSpreadList == nil");
					return kFalse;
				}
				IDataBase * dataToBeSprayedDocumentDatabase = dataToBeSpreadDocumentUIDRef.GetDataBase();
				if(dataToBeSprayedDocumentDatabase == nil)
				{
					CA("dataToBeSprayedDocumentDatabase == nil");
					continue;
				}

				UIDRef dataToBeSprayedDocumentFirstSpreadUIDRef(dataToBeSprayedDocumentDatabase, dataToBeSprayedDocumentSpreadList->GetNthSpreadUID(0)); 
				InterfacePtr<ISpread> dataToBeSprayedDocumentFirstSpread(dataToBeSprayedDocumentFirstSpreadUIDRef, IID_ISPREAD); 
				if(dataToBeSprayedDocumentFirstSpread == nil)
				{
					CA("spread == nil");
					continue;
				}
				UIDList dataToBeSprayedDocumentFrameUIDList(dataToBeSprayedDocumentDatabase);
				if(dataToBeSprayedDocumentFirstSpread->GetNthPageUID(0)== kInvalidUID)
				{
					CA("pageUID is invalid");
					continue;
				}
				dataToBeSprayedDocumentFirstSpread->GetItemsOnPage(0,&dataToBeSprayedDocumentFrameUIDList,kFalse,kTrue);			
				
				InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection());
				if(iSelectionManager == nil)
				{
					CA("iSelectionManger == nil");
					continue;
				}


			//global variable value assignments
				//1Nov..Check this later.
				double Nov1TempSecID = subSectionListBoxParams.vec_ssd[0].sectionID;
				double Nov1TempSubSecID = subSectionListBoxParams.vec_ssd[0].subSectionID;
				PMString Nov1TempSubSecName = subSectionListBoxParams.vec_ssd[0].subSectionName;
				currentSelectedSpreadCount = subSectionListBoxParams .spreadCount ; //7-may chetan
				//end 1Nov..Check this later.
				CurrentSelectedSection = Nov1TempSecID;
				CurrentSelectedPublicationID = MediatorClass :: currentSelectedPublicationID;
				CurrentSelectedSubSection = Nov1TempSubSecID;
				global_project_level = MediatorClass :: global_project_level;
				global_lang_id =MediatorClass ::global_lang_id;

				//1Nov..if(MediatorClass ::Is_UseDifferentTemplateForEachSubSectionRadioSelected)
				//1Nov..{
				MediatorClass ::subSecSpraySttngs = subSectionListBoxParams.ssss;
				//1Nov..}
				//CA("before start spraying subsection");
				MediatorClass ::currentProcessingDocUIDRef = dataToBeSpreadDocumentUIDRef;
				SubSectionSprayer SSsp;
				SSsp.selectedSubSection = Nov1TempSubSecName;
				
				InterfacePtr<ISelectionManager> selectionManager(Utils<ISelectionUtils>()->QueryActiveSelection ());
				if (selectionManager == nil)
				{
					//CA("selectionManager == nil");
					break;
				}

				// Deselect everything.
				selectionManager->DeselectAll(nil); // deselect every active CSB
				// Make a layout selection.
				InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
				//InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(Utils<ISelectionUtils>()->QuerySuite(IID_ILAYOUTSELECTION_ISUITE ),UseDefaultIID());
				if (layoutSelectionSuite == nil) 
				{
					CA("layoutSelectionSuite == nil");
					continue; 
				}
				
				//CA("1");
				//SSsp.ScriptingVersionOfAddNewPage();//::


				layoutSelectionSuite->/*Select*/SelectPageItems(dataToBeSprayedDocumentFrameUIDList,Selection::kReplace,Selection::kAlwaysCenterInView);
				
				//Use in case of spray by spread where multiple sections are sprayed in single document.
				MediatorClass ::vec_SubSecData = subSectionListBoxParams.vec_ssd;

				bool16 isCancelHit = kFalse;
	
				SSsp.startSprayingSubSection(isCancelHit);
			
				//CA("2");
				//1Nov..Check this later.
				//PMString Nov1TempSubSecName = subSectionListBoxParams.subSectionName
				//end 1Nov..Check this later.

				//if(MediatorClass ::createMediaRadioOption == 2)//by spread
				//{
				//CA("3");
					//if(subSectionListBoxParams.vec_ssd.size()>1)
					//{
					/*	
						
						subSectionData ssdt = subSectionListBoxParams.vec_ssd[0];
						MediatorClass ::ssdt = ssdt;

						fillpNodeDataList(subSectionListBoxParams.vec_ssd[0].subSectionID,
						subSectionListBoxParams.vec_ssd[0].sectionID,
						MediatorClass :: currentSelectedPublicationID,
						subSectionListBoxParams.vec_ssd[0].subSectionName);//Nov1TempSubSecID);						
					
						//CA_NUM("pNodeDataList.size() : ",pNodeDataList.size());

						if(pNodeDataList.size()> 0)
						{
							//2Nov...For left right page thing
							
								//SSsp.ScriptingVersionOfAddNewPage();
							//::AddNewPage();
								//SSsp.ScriptingVersionOfAddNewPage();
							//::AddNewPage();
								//SSsp.ScriptingVersionOfAddNewPage();
							//::AddNewPage();								
							
							//end 2Nov..
							layoutSelectionSuite->DeselectAll();
							layoutSelectionSuite->Select(dataToBeSprayedDocumentFrameUIDList,Selection::kReplace,Selection::kAlwaysCenterInView);
							SSsp.sprayedProductIndex = 0;
							SSsp.selectedSubSection = subSectionListBoxParams.vec_ssd[1].subSectionName;
							SSsp.startSprayingSubSection();
						}
						*/
					//}
				//}
				

		
				SDKUtilities sdkutil;
				if((MediatorClass::createMediaRadioOption != 4))
				{
					IDFile docFile = sdkutil.PMStringToSysFile(&path);
			
					ErrorCode status = sdklhelp.SaveDocumentAs(dataToBeSpreadDocumentUIDRef/*templateDocUIDRef*/,docFile);				
					if(status == kFailure)
					{
						CA("document can not be saved");
						continue;
					}

					sdklhelp.CloseDocument(dataToBeSpreadDocumentUIDRef/*templateDocUIDRef*/,kTrue);
					contentFileList.push_back(docFile);
				}
				else
				{
//CA("2");
					UIDRef newUIDRef;
					newUIDRef = dataToBeSpreadDocumentUIDRef;
					PMString jpgPath;
					PMString folderPath;
					//folderPath = ptrIAppFramework->getCatalogPlanningTemplateFolderPath ();//					

					InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
					if(ptrIClientOptions==nil)
					{
						ptrIAppFramework->LogDebug("AP46_RefreshContent::Refresh::doRefresh::Interface for IClientOptions not found.");
						return kFalse;
					}
					folderPath =ptrIClientOptions->getWhiteBoardMediaPath();					
					if(folderPath == "")
					{
						CA("WhiteBoard Media Path not found. Please contact System Administrator! ");
						return kFalse;
					}
					
					
					folderPath.AppendNumber(/*"Output"*/CurrentSelectedPublicationID);
					sdkutils.AppendPathSeparator (folderPath);
					folderPath.AppendNumber(CurrentSelectedSubSection);
					sdkutils.AppendPathSeparator (folderPath);
					jpgPath = folderPath;
					FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&folderPath), kTrue);
					folderPath.Append(subSectionListBoxParams.displayName );
					folderPath.Append(".indd");
					IDFile templateFolderDocFile(folderPath);

					IDFile docFile = sdkutil.PMStringToSysFile(&folderPath/*path*/);

					ErrorCode status = sdklhelp.SaveDocumentAs(dataToBeSpreadDocumentUIDRef/*templateDocUIDRef*/,docFile);				
					if(status == kFailure)
					{
						CA("document can not be saved");
						continue;
					}

//CA(folderPath);
					bool16 status1 = FileUtils::CopyFile(docFile ,templateFolderDocFile);
					if(status1 == kFalse)
					{
						CAlert::InformationAlert("Unable to copy InDesign Documents to central repository folder");
					}

					//-CreateInddPreview instance;

					IDFile docIdFile(/*masterFilePath*/folderPath);
					PMString fileName("");
					fileName.AppendNumber(CurrentSelectedSubSection);
					fileName .Append(".jpeg");

					//PMString fileName = docIdFile.GetFileName();
					//int lastPos1 =0;
					//for (int i = 0 ; i< fileName.CharCount();i++)
					//	if ((fileName[i] == '.'))
					//		lastPos1 = i;
					//// At this point lastpos should point to the last delimeter, knock off the rest of the string.
					//fileName.Truncate(fileName.CharCount()-lastPos1);
					//fileName.Append(".jpeg");
					//CA(" fileName : " + fileName );
					//fileName .Append(".jpeg");

					// takes off the last element from the path. We search for the last delimeter and knock off the
					// stuff that comes after. We lose this data so beware!

					int lastpos = 0;
					for (int i = 0 ; i< path.CharCount();i++)
						if ((path[i] == MACDELCHAR) || (path[i] == UNIXDELCHAR) || (path[i] == WINDELCHAR))
							lastpos = i;
					// At this point lastpos should point to the last delimeter, knock off the rest of the string.
					path.Truncate(path.CharCount()-lastpos);

					int lastpost = 0;
					for (int i = 0 ; i< folderPath.CharCount();i++)
						if ((folderPath[i] == MACDELCHAR) || (folderPath[i] == UNIXDELCHAR) || (folderPath[i] == WINDELCHAR))
							lastpost = i;
					// At this point lastpos should point to the last delimeter, knock off the rest of the string.
					folderPath.Truncate(folderPath.CharCount()-lastpost);

					PMString OutPutFolderPath(path);

					PMString SUbDirectoryPath = "InDesign/CatPlanning/whiteboard";
					SUbDirectoryPath.Append("/");
					SUbDirectoryPath.AppendNumber(CurrentSelectedSubSection);


					#ifdef MACINTOSH
						/*path*/folderPath.Append(MACDELCHAR);
					#else
						/*path*/folderPath.Append(WINDELCHAR);
					#endif
					
					//CA("path : " + path);
					/*path*/folderPath.Append(fileName);
					ptrIAppFramework->LogInfo("startCreatingMediaNew::jpegFile Full Path : " + /*path*/folderPath);
					IDFile jpegFile(/*path*/folderPath);
//CA(path);
					// get resolution. 
					PMReal desiredRes = 72.0; // 72 Pixels per Inch
					// setup default scales
					PMReal xScale = 1.0, yScale = 1.0;
					IOpenFileCmdData::OpenFlags docOpenFlags = IOpenFileCmdData::kOpenDefault ;
					// create the preview
					// NOTE: See defaults on member declaration
/*-
					status = instance.CreatePagePreview(docIdFile, 
														docOpenFlags, 
														jpegFile, 
														xScale, 
														yScale, 
														desiredRes,
														OutPutFolderPath,
														SUbDirectoryPath,
														jpgPath);
					if (status != kSuccess) 
					{
						//CA("CreatePagePreview failed!");
					}
-*/
					//CA("After creating snapshot");
					////--
					//PMString asd,asd1;
					//asd.AppendNumber(CurrentSelectedSubSection );
					//asd1.AppendNumber(subSectionListBoxParams.whiteboard_Stage );
					//CA("before calling the func Section id = "+asd+" & Stage Id = "+asd1);
					////--
					int32 newStageId = 1;
/*-
					bool16 statuss = ptrIAppFramework ->GetProjectProducts_updateWhiteBoardStageIdForSection(CurrentSelectedSubSection, newStageId);
					if(statuss == kFalse)
					{
						ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaNew::result == kFalse while copying");
						return kFalse;

					}
-*/
					sdklhelp.CloseDocument(dataToBeSpreadDocumentUIDRef/*templateDocUIDRef*/,kTrue);
					contentFileList.push_back(docFile);
			    }
				
				if(isCancelHit == kTrue)	
				{
					//CA("Break");
					break;
				}
				
			}//end if x

		}//end for z
				
		if(contentFileList.size()>0)
		{
			//CA("addDocumentsToBook........");
			addDocumentsToBook(contentFileList);
		}


	}//end if subSectionsToBeSrpaydVectorSize > 0
	return kTrue;		
 }//end function startCreatingMediaNew 

//=========================================================================================================
 bool16 copyStencilsFromTheTemplateDocumentIntoScrapData(PMString & templateFilePath,InterfacePtr<IPageItemScrapData> & scrapData)
 {
	 bool16 result = kFalse;
	do
	{
		SDKLayoutHelper sdklhelp;

		PMString filePathItemsToBeCopiedFrom(templateFilePath);//"c:\\test\\aa.indt");
		filePathItemsToBeCopiedFrom.SetTranslatable(kFalse);
		IDFile templateIDFile(filePathItemsToBeCopiedFrom);
		UIDRef templateDocUIDRef = sdklhelp.OpenDocument(templateIDFile);
		if(templateDocUIDRef == UIDRef ::gNull)
		{
			CA("uidref of templatedoc is invalid");
			break;
		}

		ErrorCode err = sdklhelp.OpenLayoutWindow(templateDocUIDRef);
		if(err == kFailure)
		{
			//CA("Error occured while opening the layoutwindow of the template");
			break;
		}

		InterfacePtr<IDocument> templatedoc(templateDocUIDRef,UseDefaultIID());
		if(templatedoc == nil)
		{
			//CA("templatedoc == nil");
			break;
		}
		InterfacePtr<ISpreadList>templateSpreadUIDList(templatedoc,UseDefaultIID());
		if(templateSpreadUIDList == nil)
		{
			//CA("templateSpreadUIDList == nil");
			break;
		}
		IDataBase * templateDocDatabase = templateDocUIDRef.GetDataBase();
		if(templateDocDatabase == nil)
		{
			//CA("templateDocDatabase == nil");
			break;
		}

		UIDRef templateDocFirstSpreadUIDRef(templateDocDatabase, templateSpreadUIDList->GetNthSpreadUID(0)); 
		InterfacePtr<ISpread> templateSpread(templateDocFirstSpreadUIDRef, IID_ISPREAD); 
		if(templateSpread == nil)
		{
			//CA("templateSpread == nil");
			break;
		}
		UIDList templateFrameUIDList(templateDocDatabase);
		if(templateSpread->GetNthPageUID(0)== kInvalidUID)
		{
			//CA("pageUID is invalid");
			break;
		}
		
		templateSpread->GetItemsOnPage(0,&templateFrameUIDList,kFalse,kTrue);	

		InterfacePtr<ICommand> copyStencilsCMD(CmdUtils::CreateCommand(kCopyCmdBoss));
		if(copyStencilsCMD == nil)
		{
			//CA("copyStencilsCMD == nil");
			break;

		}
		InterfacePtr<ICopyCmdData> cmdData(copyStencilsCMD, IID_ICOPYCMDDATA);
		if(cmdData == nil)
		{
			//CA("cmdData == nil");
			break;
		}
		// Copy cmd will own this list

		UIDList* listCopy = new UIDList(templateFrameUIDList);
		InterfacePtr<IClipboardController> clipboardController(/*gSession*/GetExecutionContextSession(),UseDefaultIID());//Cs4
		if(clipboardController == nil)
		{
			//CA("clipboardController == nil");
			break;

		}
		ErrorCode status = clipboardController->PrepareForCopy();
		if(status == kFailure)
		{
			//CA("status == kFailure");
			break;
		}
		InterfacePtr<IDataExchangeHandler> scrapHandler(clipboardController->QueryHandler(kPageItemFlavor));
		if(scrapHandler == nil)
		{
			//CA("scrapHandler == nil");
			break;
		}

		clipboardController->SetActiveScrapHandler(scrapHandler);

		InterfacePtr<IPageItemScrapData> localScrapData(scrapHandler, UseDefaultIID());
		if(localScrapData == nil)
		{
			//CA("localScrapData == nil");
			break;
		}
		scrapData = localScrapData;
		// specify where to copy item

		UIDRef parent = scrapData->GetRootNode();

		cmdData->Set(copyStencilsCMD, listCopy, parent, scrapHandler);

		if(templateFrameUIDList.Length() == 0)
		{
			return kFalse;
		}
		else
		{
			status = CmdUtils::ProcessCommand(copyStencilsCMD);
		}

		if(status == kFailure)
		{
			//CA("status == kFailure");
			break;
		}

		//CA("closing");
		sdklhelp.CloseDocument(templateDocUIDRef,kFalse);




	}while(kFalse);

	result = kTrue;
	return result;

 }

//==========================================================================================================

 bool16 pasteTheItemsFromScrapDataOntoOpenDocument(InterfacePtr<IPageItemScrapData> & scrapData,UIDRef &documentDocUIDRef )
 {
	//CA("pasteTheItemsFromScrapDataOntoOpenDocument");
	bool16 result = kFalse;
	do
	{
		if(scrapData->IsEmpty()==kFalse)
		{//start if (scrapData->IsEmpty()==kFalse)
			//CA("scrapData->IsEmpty()==kFalse");
			//InterfacePtr<IPageItemScrapData> scrapData(scrapHandler, UseDefaultIID());
			//if(scrapData == nil)
			//{
				//CA("scrapData == nil");
				//break;
			//}
			//This will give the list of items present on the scrap
			UIDList* scrapContents = scrapData->CreateUIDList();
			if(scrapContents->Length() >= 1)
			{//start if (scrapContents->Length() >= 1)    
				//CA("scrapContents->Length() >= 1");
				InterfacePtr<IDocument> dataToBeSprayedDocument(documentDocUIDRef,UseDefaultIID());
				if(dataToBeSprayedDocument == nil)
				{
					CA("dataToBeSprayedDocument == nil");
					break;
				}
				InterfacePtr<ISpreadList>dataToBeSprayedDocumentSpreadList(dataToBeSprayedDocument,UseDefaultIID());
				if(dataToBeSprayedDocumentSpreadList == nil)
				{
					CA("dataToBeSprayedDocumentSpreadList == nil");
					break;
				}
				IDataBase * dataToBeSprayedDocDatabase = documentDocUIDRef.GetDataBase();
				if(dataToBeSprayedDocDatabase == nil)
				{
					CA("dataToBeSprayedDocDatabase == nil");
					break;
				}

				UIDRef dataToBeSprayedDocFirstSpreadUIDRef(dataToBeSprayedDocDatabase, dataToBeSprayedDocumentSpreadList->GetNthSpreadUID(0)); 
				UIDRef spreadUIDRef;
				spreadUIDRef = dataToBeSprayedDocFirstSpreadUIDRef;
				SDKLayoutHelper sdklhelp;
				UIDRef parentLayerUIDRef =sdklhelp.GetSpreadLayerRef(spreadUIDRef);
				if(parentLayerUIDRef.GetUID() == kInvalidUID)
				{
					CA("parentLayerUIDRef.GetUID() == kInvalidUID");
					break;
				}
				InterfacePtr<ICommand> pasteToClipBoardCMD (CmdUtils::CreateCommand(kPasteCmdBoss));
				if(pasteToClipBoardCMD == nil)
				{
					CA("pasteToClipBoardCMD == nil");
					break;
				}
				InterfacePtr<ICopyCmdData> cmdData(pasteToClipBoardCMD, UseDefaultIID());
				if(cmdData == nil)
				{
					CA("cmdData == nil");
					break;
				}
				if(scrapContents == nil)
				{
					CA("scrapContents == nil");
					break;
				}
				if(scrapContents == nil || parentLayerUIDRef == UIDRef::gNull || pasteToClipBoardCMD == nil)
				{
					break;
				}
				//PMPoint offset(-130.0, 0.0);
				PMPoint offset(0.0, 0.0);
				cmdData->SetOffset(offset);

				cmdData->Set(pasteToClipBoardCMD, scrapContents, parentLayerUIDRef);

				ErrorCode status = CmdUtils::ProcessCommand(pasteToClipBoardCMD); 
				if(status == kSuccess)
				{
					//CA("3");
					result = kTrue;
				}

			}//end if (scrapContents->Length() >= 1)
		}//end if (scrapData->IsEmpty()==kFalse)
	}while(kFalse);
	return result;
 }

 //added on 7Nov..
bool16 IsTheBookBlank()
 {

		bool16 status = kFalse;
		do
		{
			InterfacePtr<IBookManager> bookManager(/*gSession*/GetExecutionContextSession(), UseDefaultIID()); //Cs4
			if (bookManager == nil) 
			{ 
				ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::IsTheBookBlank::bookManager == nil");
				break; 
			}
			IBook * currentActiveBook = bookManager->GetCurrentActiveBook();
			if(currentActiveBook == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::IsTheBookBlank::currentActiveBook == nil");
				break;			
			}
			InterfacePtr<IBookContentMgr> iBookContentMgr(currentActiveBook,UseDefaultIID());
			if(iBookContentMgr == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::IsTheBookBlank::iBookContentMgr == nil");
				break;
			}

			int32 contentCount = iBookContentMgr->GetContentCount();
			if(contentCount <= 0)
			{
				status = kTrue;
			}
		}while(kFalse);

		return status;
 }
 //ended on 7Nov..

 //17-april chetan
 void CMMDialogObserver::populateSubSectionListForWhiteboard(bool16 shall_I_Fill_Data_Into_ListBox,double rootId)
{
/*-
	no_of_lstboxElements=0;// declared globally on 03-october
	//CA("1 Inside SPSelectionObserver::populateSectionDropDownList");
	//noOfRows = 0;	

	do
	{
		SDKListBoxHelper listHelper(this, kCMMPluginID, kCMMMTemplateFileListBoxWidgetID, kCMMDialogWidgetID);
		if(shall_I_Fill_Data_Into_ListBox)
		{
			listHelper.EmptyCurrentListBox();
		}
		//yet to define
		MediatorClass :: vector_subSectionSprayerListBoxParameters.clear();
		
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
		{
			CA("ptrIAppFramework == nil");
			break;
		}
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(!iConverter)
		{
			CA("iConverter == nil");
			break;
		}
		
		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			ptrIAppFramework->LogDebug("AP46_RefreshContent::Refresh::doRefresh::Interface for IClientOptions not found.");
			break;
		}	

		CPubModel pubicationModelObj = ptrIAppFramework->getpubModelByPubID(rootId, 1);

		MediatorClass :: global_project_level = ptrIAppFramework->getPM_Project_Levels();
		
		SectionData publdata;
		int32 sectionID = -1;
		publdata.clearSectionVector();
		publdata.clearSubsectionVector();
		//define global_vector_pubmodel
		MediatorClass:: global_vector_pubmodel.clear();
		
		int32 language_id = -1 ;
		//define global_lang_id
		language_id = MediatorClass :: global_lang_id ; //ptrIClientOptions->getDefaultLocale(language_name);
		VectorPubModelPtr vector_pubmodel = NULL;
		VectorAPpubCommentValuePtr vector_pubCommentModel = NULL;  //19-april

		if( MediatorClass :: global_project_level == 3)

		{	
//CA("Level 3");	
			vector_pubmodel = ptrIAppFramework->getAllSectionsByPubIdAndLanguageId(rootId , language_id);
			if(vector_pubmodel == nil)
			{
				ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::populateSubsectionList::getAllSectionsByPubIdAndLanguageId::vector_pubmodel == nil");
				break;
			}
			VectorPubModel::iterator it;
			for(it=vector_pubmodel->begin(); it!=vector_pubmodel->end(); it++)
			{
					int32 sectionID=it->getPublicationID();
					PMString sectionName = it->getName();
					
					VectorPubModelPtr vectorSubSectionModel = ptrIAppFramework->getAllSubsectionsBySectionIdAndLanguageId(sectionID , language_id);
					if(vectorSubSectionModel == nil)
					{
						
						ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::populateSubsectionList::getAllSubsectionsBySectionIdAndLanguageId::vectorSubSectionModel == nil");
						break;
					}
					
					VectorPubModel::iterator it_subsection;

					for(it_subsection=vectorSubSectionModel->begin(); it_subsection!=vectorSubSectionModel->end(); it_subsection++)
					{

						CPubModel model = *(it_subsection);
						MediatorClass:: global_vector_pubmodel.push_back(model);
						PMString subSectionName(iConverter->translateString(model.getName()));
			
						subSectionSprayerListBoxParameters tempClass;

						subSectionData ssd;
						ssd.subSectionName = subSectionName;
						ssd.subSectionID = model.getPublicationID();
						ssd.sectionID = sectionID;
						ssd.sectionName = sectionName;
						ssd.number = model.getNumber();

						tempClass.displayName = subSectionName;
						tempClass.isSelected = kTrue;
						tempClass.vec_ssd.push_back(ssd);
						tempClass.spreadCount = model .getSpreadCount();
//12-may global_project_level == 3
						tempClass.whiteboard_Stage = model.getWhiteboardStage ();
						tempClass .ssss.horizontalBoxSpacing = model.getHorizontalBoxSpacing() ;
						tempClass.ssss .verticalBoxSpacing = model.getVerticalBoxSpacing() ;
						tempClass.ssss .flowLeftToRight = model.getFlowDirection() ;
						tempClass.ssss .alternateForEachPage = model.getalternate() ;
						tempClass.ssss.flowHorizontalFlow = model.getFlowPreference() ;
						if((model.getFlowPreference() == kTrue) && (model.getalternate() == kTrue) && (model.getFlowDirection() == kTrue))
						{
							tempClass.ssss.AltVerticalFlowFlag = kFalse;
							tempClass.ssss.VerticalFlowFlag = kFalse;
							tempClass.ssss.AltHorizontalFlowFlag = kTrue;
							tempClass.ssss.HorizontalFlowFlag = kFalse;
						}
						else if((model.getFlowPreference() == kTrue) && (model.getalternate() == kFalse) && (model.getFlowDirection() == kTrue))
						{
							tempClass.ssss.AltVerticalFlowFlag = kFalse;
							tempClass.ssss.VerticalFlowFlag = kFalse;
							tempClass.ssss.AltHorizontalFlowFlag = kFalse;
							tempClass.ssss.HorizontalFlowFlag = kTrue;
						}
						else if((model.getFlowPreference() == kFalse) && (model.getalternate() == kTrue) && (model.getFlowDirection() == kTrue))
						{
							tempClass.ssss.AltVerticalFlowFlag = kTrue;
							tempClass.ssss.VerticalFlowFlag = kFalse;
							tempClass.ssss.AltHorizontalFlowFlag = kFalse;
							tempClass.ssss.HorizontalFlowFlag = kFalse;
						}
						else if((model.getFlowPreference() == kFalse) && (model.getalternate() == kFalse) && (model.getFlowDirection() == kTrue))
						{
							tempClass.ssss.AltVerticalFlowFlag = kFalse;
							tempClass.ssss.VerticalFlowFlag = kTrue;
							tempClass.ssss.AltHorizontalFlowFlag = kFalse;
							tempClass.ssss.HorizontalFlowFlag = kFalse;
						}

						if(shall_I_Fill_Data_Into_ListBox)
						{
//							CA("adding elements into listbox");
							listHelper.AddElement(subSectionName,kPublicationNameTextWidgetID);
						}
							
						
						MediatorClass :: vector_subSectionSprayerListBoxParameters.push_back(tempClass);

									
					}

					if(vectorSubSectionModel)
						delete vectorSubSectionModel;

			}		


			if(vector_pubmodel)
				delete vector_pubmodel;

		}
		else if( MediatorClass :: global_project_level ==2)
		{	
			MediatorClass :: vector_vector_apPubComment.clear ();				
			vector_pubmodel = ptrIAppFramework->getAllSubsectionsByPubIdAndLanguageId(rootId , language_id);
			if(vector_pubmodel == nil)
			{
				ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::populateSubsectionList::getAllSubsectionsByPubIdAndLanguageId::vector_pubmodel == nil");
				break;
			}
//
			PMString folderPath;
			//folderPath = ptrIAppFramework->getCatalogPlanningTemplateFolderPath ();//
			folderPath = ptrIClientOptions->getWhiteBoardMediaPath();//

			VectorPubModel::iterator it;

			for(it=vector_pubmodel->begin(); it!=vector_pubmodel->end(); it++)
			{
					CPubModel model = (*it);
///
					vector_pubCommentModel = ptrIAppFramework->getPubCommentsBySectionIdStageId (model.getPublicationID(),0);
					if(vector_pubCommentModel == nil)
					{
						//ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::populateSubsectionListForWhiteBoard::getPubCommentsBySectionIdStageId::vector_pubCommentModel == nil");
						if(!pubicationModelObj.getIspage_based())
							continue;
						
					}

					MediatorClass:: global_vector_pubmodel.push_back(model);
					PMString subSectionName(iConverter->translateString(model.getName()));

					subSectionSprayerListBoxParameters tempClass;

					subSectionData ssd;
					ssd.subSectionName = subSectionName;
					ssd.subSectionID = model.getPublicationID();
					ssd.number = model.getNumber();	
					ssd.sectionID = model.getPublicationID ();

					tempClass.displayName = subSectionName;
					tempClass.isSelected = kTrue;
					tempClass.vec_ssd.push_back(ssd);
					tempClass.spreadCount = model.getSpreadCount();
					tempClass.isCommentAutoSpray = model.getIsAutoSpray();
					MediatorClass::isCommentsAutoSpray = model.getIsAutoSpray();

						tempClass.whiteboard_Stage = model.getWhiteboardStage ();
						tempClass .ssss.horizontalBoxSpacing = model.getHorizontalBoxSpacing() ;
						tempClass.ssss .verticalBoxSpacing = model.getVerticalBoxSpacing() ;
						tempClass.ssss .flowLeftToRight = model.getFlowDirection() ;
						tempClass.ssss .alternateForEachPage = model.getalternate() ;
						tempClass.ssss.flowHorizontalFlow = model.getFlowPreference() ;
						if((model.getFlowPreference() == kTrue) && (model.getalternate() == kTrue) && (model.getFlowDirection() == kTrue))
						{
							tempClass.ssss.AltVerticalFlowFlag = kFalse;
							tempClass.ssss.VerticalFlowFlag = kFalse;
							tempClass.ssss.AltHorizontalFlowFlag = kTrue;
							tempClass.ssss.HorizontalFlowFlag = kFalse;
						}
						else if((model.getFlowPreference() == kTrue) && (model.getalternate() == kFalse) && (model.getFlowDirection() == kTrue))
						{
							tempClass.ssss.AltVerticalFlowFlag = kFalse;
							tempClass.ssss.VerticalFlowFlag = kFalse;
							tempClass.ssss.AltHorizontalFlowFlag = kFalse;
							tempClass.ssss.HorizontalFlowFlag = kTrue;
						}
						else if((model.getFlowPreference() == kFalse) && (model.getalternate() == kTrue) && (model.getFlowDirection() == kTrue))
						{
							tempClass.ssss.AltVerticalFlowFlag = kTrue;
							tempClass.ssss.VerticalFlowFlag = kFalse;
							tempClass.ssss.AltHorizontalFlowFlag = kFalse;
							tempClass.ssss.HorizontalFlowFlag = kFalse;
						}
						else if((model.getFlowPreference() == kFalse) && (model.getalternate() == kFalse) && (model.getFlowDirection() == kTrue))
						{
							tempClass.ssss.AltVerticalFlowFlag = kFalse;
							tempClass.ssss.VerticalFlowFlag = kTrue;
							tempClass.ssss.AltHorizontalFlowFlag = kFalse;
							tempClass.ssss.HorizontalFlowFlag = kFalse;
						}

					if( vector_pubCommentModel != NULL)
						MediatorClass::vector_vector_apPubComment.push_back (*vector_pubCommentModel );
					else
					{
						VectorAPpubCommentValuePtr vector_pubCommentModeltemp = new VectorAPpubCommentValue();  
						MediatorClass::vector_vector_apPubComment.push_back (*vector_pubCommentModeltemp );
						if(vector_pubCommentModeltemp)	//-------
							delete vector_pubCommentModeltemp;
					}


					MediatorClass:: vector_subSectionSprayerListBoxParameters.push_back(tempClass);

					PMString pathOfMasterFile(folderPath),pathOfProductStencil(folderPath),pathOfItemStencil(folderPath);
					pathOfMasterFile .Append("\\Master\\");
					pathOfItemStencil.Append("\\Item\\");
					pathOfProductStencil.Append("\\Product\\");
					//CA("Master file name : "+model.getMasterTemplateFilename());
					pathOfMasterFile.Append(model.getMasterTemplateFilename());
					pathOfProductStencil.Append( model.getProductStencil());
					pathOfItemStencil.Append(model.getItemStencil());

					IDFile templateFileMaster(pathOfMasterFile),templateFileProduct(pathOfProductStencil),templateFileItem(pathOfItemStencil);
					SDKUtilities sdkutils;
					PMString extension = sdkutils.GetExtension(templateFileMaster);
					if(extension != "indt" && extension != "indd")
					{						
						CAlert::InformationAlert("Please select valid IndesignCS3 Template Files.\nTemplate files have .indt extension");
						continue;
					}
					PMString masterFileName,productFileName,itemFileName;
					FileUtils::GetFileName(pathOfMasterFile,masterFileName);
					FileUtils::GetFileName(pathOfProductStencil ,productFileName );
					FileUtils::GetFileName(pathOfItemStencil ,itemFileName );
                    ///// to get product stencil for manual spray
					VectorAPpubCommentValue::iterator it3;
					PMString productStencils,itemStencils;
					int32 flagForProdStencil = 0;
					int32 flagForItemStencil = 0;

					if( vector_pubCommentModel != NULL)
					{
						for(it3=vector_pubCommentModel->begin(); it3!=vector_pubCommentModel->end() ; it3++)
						{
							if(it3->comment_type == 10)
							{
								if(!productStencils.Contains(it3->stencilPath))
								{
									if(flagForProdStencil)
									{
										productStencils.Append(",");
									}
									productStencils.Append(it3->stencilPath);
									flagForProdStencil++;
								}
							}
							if(it3->comment_type == 11)
							{
								if(!itemStencils.Contains(it3->stencilPath))
								{
									if(flagForItemStencil)
									{
										itemStencils.Append(",");
									}
									itemStencils.Append(it3->stencilPath);
									flagForItemStencil++;
								}
							}
						}
					}

					if(productStencils.IsEmpty())
					{
						productStencils.Append("N/A");
					}
					if(itemStencils.IsEmpty())
					{
						itemStencils.Append("N/A");
					}
                    ///// to get product stencil for manual spray
					if(shall_I_Fill_Data_Into_ListBox)
					{
						listHelper.AddElement(subSectionName,kPublicationNameTextWidgetID, masterFileName,
						kListBoxMasterFileNameTextWidgetID, productFileName, 
						kListBoxStencilFileNameTextWidgetID, itemFileName, 
						kListBoxItemFileNameTextWidgetID, productStencils, 
						kListBoxProductStencilFileNameTextForManualWidgetID, itemStencils,
						kListBoxItemStencilFileNameTextForManualWidgetID);
					}
//////////////////
					productStencils.Clear();
					itemStencils.Clear();

					if(!tempClass.isCommentAutoSpray)
					{
						vWidgetID.clear();
						vWidgetID.push_back(kListBoxSelectMasterTemplateRollOverButtonWidgetID);
						vWidgetID.push_back(kListBoxApplyMasterUnderneathElementsRollOverButtonWidgetID);
						vWidgetID.push_back(kListBoxStencilFileNameTextWidgetID);
						vWidgetID.push_back(kListBoxSelectStencilTemplateRollOverButtonWidgetID);
						vWidgetID.push_back(kListBoxApplyAllStencilUnderneathElementsRollOverButtonWidgetID);
						vWidgetID.push_back(kSeparatorOnGrouPanel1WidgetID);
						vWidgetID.push_back(kListBoxItemFileNameTextWidgetID);
						vWidgetID.push_back(kListBoxSelectItemStencilRollOverButtonWidgetID);
						vWidgetID.push_back(kListBoxApplyItemStencilUnderneathElementsRollOverButtonWidgetID);
						vWidgetID.push_back(kSeparatorOnGrouPanel6WidgetID);
						//vWidgetID.push_back(kListBoxHybridTableFileNameTextWidgetID);
						//vWidgetID.push_back(kListBoxSelectHybridTableStencilRollOverButtonWidgetID);
						//vWidgetID.push_back(kListBoxApplyHybridTableStencilUnderneathElementsRollOverButtonWidgetID);
						//vWidgetID.push_back(kSeparatorOnGrouPanel3WidgetID);
						vWidgetID.push_back(kInvokeSubSectionOptionsDialogRollOverButtonGreenButtonWidgetID);
						vWidgetID.push_back(kInvokeSubSectionOptionsDialogRollOverButtonButtonWidgetID);
						vWidgetID.push_back(kListBoxApplySpraySettingsUnderneathElementsRollOverButtonWidgetID);
						
						//vWidgetID.push_back(kListBoxSectionFileNameTextWidgetID);
						//vWidgetID.push_back(kListBoxSelectSectionStencilRollOverButtonWidgetID);
						//vWidgetID.push_back(kListBoxSectionStencilUnderneathElementsRollOverButtonWidgetID);
						//vWidgetID.push_back(kSeparatorOnPriceGrouPanel4WidgetID);

						listHelper.HideWidgets(MediatorClass::PublicationTemplateMappingListBox, vWidgetID, no_of_lstboxElements);
					}
					else
					{
						vWidgetID.clear();
						vWidgetID.push_back(kListBoxProductStencilFileNameTextForManualWidgetID);
						vWidgetID.push_back(kSeparatorOnGrouPanel1ForManualWidgetID);
						vWidgetID.push_back(kListBoxItemStencilFileNameTextForManualWidgetID);

						listHelper.HideWidgets(MediatorClass::PublicationTemplateMappingListBox, vWidgetID, no_of_lstboxElements);
					}
//////////////////
					MediatorClass::vector_subSectionSprayerListBoxParameters[no_of_lstboxElements].masterFileWithCompletePath=pathOfMasterFile;
					if(tempClass.isCommentAutoSpray)
					{
						MediatorClass::vector_subSectionSprayerListBoxParameters[no_of_lstboxElements].ProductStencilFilePath = pathOfProductStencil;
						MediatorClass::vector_subSectionSprayerListBoxParameters[no_of_lstboxElements].ItemStencilFilePath = pathOfItemStencil;
					}
					else
					{
						MediatorClass::vector_subSectionSprayerListBoxParameters[no_of_lstboxElements].ProductStencilFilePath = "";
						MediatorClass::vector_subSectionSprayerListBoxParameters[no_of_lstboxElements].ItemStencilFilePath = "";
					}
					MediatorClass::vector_subSectionSprayerListBoxParameters[no_of_lstboxElements].isSingleStencilFileForProductAndItem = kFalse;
					no_of_lstboxElements++;
			}

			if(vector_pubmodel)
				delete vector_pubmodel;
		}
	}while(kFalse);
-*/
}

 //17-april chetan

////////////////////////////////////////////////////////////////////////////////
///////////////////////////// 30-april chetan //////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void fillpNodeDataListForWhite()//int32 curSelSubecId)
{//CA("cmmdialogobserver::fillpNodeDataListForWhite");
/*-
    do
	{	
		//////////////////////////////////////////////////////////////////////////////////
		CreateMediaPublicationNodeList tempPNodeDataList;
		tempPNodeDataList = pNodeDataList;

		CatPlanningData CPDataObj;		

		//The pNodeDataList is a global vector.
		pNodeDataList.clear();

		//9May PublicationNode pNode;	
		CreateMediaPublicationNode pNode;	

		vector<vector<APpubComment> > apPubCommentVectorVector = MediatorClass::vector_vector_apPubComment;
		int32 sizeOfCommentVectorVector =static_cast<int32> (apPubCommentVectorVector.size());//cs4
		int32 sizeOfTempPNodeList = static_cast<int32>(tempPNodeDataList .size ());//cs4

		vector<APpubComment> ApPubComment;

		for(int32 i = 0;i<sizeOfCommentVectorVector ;i++)
		{//CA("second for");
			if(apPubCommentVectorVector[i][0].SECTION_NO == tempPNodeDataList[0].sectionID )
			{
				ApPubComment = apPubCommentVectorVector[i];
				break;
			}
		}

		MediatorClass::vector_apPubComment = ApPubComment;
		int32 sizeOfCommentVector = static_cast<int32>(MediatorClass::vector_apPubComment.size ()); //Cs4

		CPDataList .clear ();
		for(int32 j = 0 ; j< sizeOfTempPNodeList; j++)
		{//CA("for J");
			for(int32 i = 0; i< sizeOfCommentVector; i++)
			{//CA("for I");
				if((ApPubComment[i].comment.id == tempPNodeDataList[j].getPubId ()) && (ApPubComment[i].SECTION_NO == tempPNodeDataList[j].sectionID ))
				{//CA("apPubCommentVector[i].comment.id == tempPNodeDataList[j].sectionID () ");
					pNodeDataList.push_back(tempPNodeDataList[j]);
					CPDataObj.apPubCommentObj = ApPubComment[i];
					CPDataObj.createMediaPublicationNodeObj = tempPNodeDataList[j];
					CPDataList.push_back (CPDataObj );
					break;
				}
			}		
		}

		tempPNodeDataList.clear ();
	}while(kFalse);
-*/
 }

////////////////////////////////////////////////////////////////////////////////
///////////////////////////// 30-april chetan end //////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//// Chetan -- 29-09
void startCreatingMediaNewForWBManualSpray()
{//start function startCreatingMediaNew 

/*-
	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
		return;

	InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	if(ptrIClientOptions==nil)
	{
		ptrIAppFramework->LogDebug("AP46_RefreshContent::Refresh::doRefresh::Interface for IClientOptions not found.");
		return;
	}		
	
	InterfacePtr<IBookManager> bookManager(GetExecutionContextSession(), UseDefaultIID()); //Cs4
	if (bookManager == nil) 
	{ 
		ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaNew::bookManager == nil");
		return; 
	}
	
	IBook * activeBook = bookManager->GetCurrentActiveBook();
	if(activeBook == nil)
	{
		ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaNew::activeBook == nil");
		return;			
	}
		
	IDFile file = activeBook->GetBookFileSpec();

	SDKUtilities sdkutils;
	PMString inddfilepath = file.GetString();

	sdkutils.RemoveLastElement(inddfilepath);
	sdkutils.AppendPathSeparator(inddfilepath);	
	//CA("inddfilepath : " + inddfilepath);

	K2Vector<IDFile> contentFileList;
		
	vector<subSectionSprayerListBoxParameters> & subSectionVector = MediatorClass ::vector_subSectionSprayerListBoxParameters;
	int32 subSectionsToBeSrpaydVectorSize = static_cast<int32>(subSectionVector.size());//Cs4

	PMString AllStencilFilePath,ItemStencilFilePath,ProductStencilFilePath;
			
	if(subSectionsToBeSrpaydVectorSize > 0)
	{//start if subSectionsToBeSrpaydVectorSize > 0
		
		SDKUtilities sdkutil;
		//bool16 result;
		SubSectionSprayer SSsp;
		for(int32 vectorElementIndex = 0;vectorElementIndex < subSectionsToBeSrpaydVectorSize;vectorElementIndex++)
		{//start for z
			
			//global_VectorAPpubCommentValue = MediatorClass::vector_vector_apPubComment[vectorElementIndex];
			subSectionSprayerListBoxParameters & subSectionListBoxParams = subSectionVector[vectorElementIndex];
			vector<APpubComment> apPubCommentBySection = MediatorClass::vector_vector_apPubComment[vectorElementIndex];

			currentSelectedSpreadCount = subSectionListBoxParams.spreadCount;
			PMString masterFilePath = subSectionListBoxParams.masterFileWithCompletePath;
			PMString tempPath;

			folderPath.Clear();
			//folderPath = ptrIAppFramework->getCatalogPlanningTemplateFolderPath ();//
			folderPath = ptrIClientOptions->getWhiteBoardMediaPath();//

			UIDRef dataToBeSpreadDocumentUIDRef	;			
			PMString path(inddfilepath);

			PMString Nov1TempDocSaveAsName = subSectionListBoxParams.displayName;

			path.Append(Nov1TempDocSaveAsName);
			path.Append(".indd");
			SDKLayoutHelper sdklhelp;

			SDKFileHelper masterFile(masterFilePath);
			if(masterFilePath == "" || masterFile.IsExisting() == kFalse)
			{
				CA(masterFilePath + " file doesn't exist");
				continue;
			}

			if(subSectionListBoxParams.isSelected == kTrue)
			{
				if(vectorElementIndex > 0)
					CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 

				PMString filePathItemsToBePastedIn(masterFilePath);
				IDFile dataToBeSpreadDocumentIDFile(masterFilePath);
				dataToBeSpreadDocumentUIDRef = sdklhelp.OpenDocument(dataToBeSpreadDocumentIDFile);
				if(dataToBeSpreadDocumentUIDRef == UIDRef ::gNull)
				{
					CA("uidref of dataToBeSpreadDocumentUIDRef is invalid");
					continue;
				}						
			
				ErrorCode err = sdklhelp.OpenLayoutWindow(dataToBeSpreadDocumentUIDRef);

				AddOrDeleteSpreads(currentSelectedSpreadCount);

				int32 Nov1TempSecID = subSectionListBoxParams.vec_ssd[0].sectionID;
				int32 Nov1TempSubSecID = subSectionListBoxParams.vec_ssd[0].subSectionID;

				PMString Nov1TempSubSecName = subSectionListBoxParams.vec_ssd[0].subSectionName;
				currentSelectedSpreadCount = subSectionListBoxParams .spreadCount ; //7-may chetan
				//end 1Nov..Check this later.
				CurrentSelectedSection = Nov1TempSecID;
				CurrentSelectedPublicationID = MediatorClass :: currentSelectedPublicationID;
				CurrentSelectedSubSection = Nov1TempSubSecID;

				global_project_level = MediatorClass :: global_project_level;
				global_lang_id =MediatorClass ::global_lang_id;

				AcquireWaitCursor adb;					
				adb.Animate();

				for(currentSpread =1; currentSpread <= currentSelectedSpreadCount; currentSpread++)
				{
					tempPath.Clear();

					int32 numOfProductsForCurrentSpread=0;
					int32 sizeOfCommentsBySection =static_cast<int32> (apPubCommentBySection.size()); //Cs4
					int32sizeOfCommentListForSelectedSection = static_cast<int32>(MediatorClass::vector_vector_apPubComment[vectorElementIndex].size());  //CS4

					global_VectorAPpubCommentValue.clear ();
					for(int32 j=0; j<sizeOfCommentsBySection; j++)
					{
						if(apPubCommentBySection[j].spreadNumber == currentSpread )
						{
							numOfProductsForCurrentSpread++;							
							global_VectorAPpubCommentValue.push_back(apPubCommentBySection[j]);
						}
					}
					createCommentsOnLayer();


				}

				UIDRef newUIDRef;
				newUIDRef = dataToBeSpreadDocumentUIDRef;
				PMString jpgPath;
				//PMString folderPath;
				//folderPath = ptrIAppFramework->getCatalogPlanningTemplateFolderPath ();//
				PMString TempFolderPath = folderPath;

				//sdkutils.RemoveLastElement(TempFolderPath);
				sdkutil.AppendPathSeparator (TempFolderPath);
				TempFolderPath.AppendNumber(CurrentSelectedPublicationID);
				sdkutils.AppendPathSeparator (TempFolderPath);
				TempFolderPath.AppendNumber(CurrentSelectedSubSection);
				sdkutils.AppendPathSeparator (TempFolderPath);
				jpgPath = TempFolderPath;
				FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&TempFolderPath), kTrue);
				TempFolderPath.Append(subSectionListBoxParams.displayName );
				TempFolderPath.Append(".indd");
				IDFile templateFolderDocFile(TempFolderPath);
                    //CA("Folder Path = "+folderPath);
				IDFile docFile = sdkutil.PMStringToSysFile(&TempFolderPath);

				ErrorCode status = sdklhelp.SaveDocumentAs(dataToBeSpreadDocumentUIDRef,docFile);
				if(status == kFailure)
				{
					CA("document can not be saved");
					continue;
				}
//CA("1");
//CA(folderPath);
				bool16 status1 = FileUtils::CopyFile(docFile ,templateFolderDocFile);
				if(status1 == kFalse)
				{
					CAlert::InformationAlert("Unable to copy InDesign Documents to central repository folder");
				}

				CreateInddPreview instance;

				IDFile docIdFile(TempFolderPath);
				PMString fileName("");
				fileName.AppendNumber(CurrentSelectedSubSection);
				fileName.Append(".jpeg");


				int lastpos = 0;
				for (int i = 0 ; i< path.CharCount();i++)
					if ((path[i] == MACDELCHAR) || (path[i] == UNIXDELCHAR) || (path[i] == WINDELCHAR))
						lastpos = i;
				// At this point lastpos should point to the last delimeter, knock off the rest of the string.
				path.Truncate(path.CharCount()-lastpos);

				int lastpost = 0;
				for (int i = 0 ; i< TempFolderPath.CharCount();i++)
					if ((TempFolderPath[i] == MACDELCHAR) || (TempFolderPath[i] == UNIXDELCHAR) || (TempFolderPath[i] == WINDELCHAR))
						lastpost = i;
				// At this point lastpos should point to the last delimeter, knock off the rest of the string.
				TempFolderPath.Truncate(TempFolderPath.CharCount()-lastpost);

				PMString OutPutFolderPath(path);

				PMString SUbDirectoryPath = "InDesign/WhiteBoard";
				SUbDirectoryPath.Append("/");
				SUbDirectoryPath.AppendNumber(CurrentSelectedPublicationID);
				SUbDirectoryPath.Append("/");
				SUbDirectoryPath.AppendNumber(CurrentSelectedSubSection);


				#ifdef MACINTOSH
					TempFolderPath.Append(MACDELCHAR);
				#else
					TempFolderPath.Append(WINDELCHAR);
				#endif
				
				//CA("path : " + path);
				TempFolderPath.Append(fileName);
				ptrIAppFramework->LogInfo("startCreatingMediaNew::jpegFile Full Path : " + TempFolderPath);
				IDFile jpegFile(TempFolderPath);
//CA(path);
				// get resolution. 
				PMReal desiredRes = 72.0; // 72 Pixels per Inch
				// setup default scales
				PMReal xScale = 1.0, yScale = 1.0;
				IOpenFileCmdData::OpenFlags docOpenFlags = IOpenFileCmdData::kOpenDefault ;
				// create the preview
				// NOTE: See defaults on member declaration
				status = instance.CreatePagePreview(docIdFile, 
													docOpenFlags, 
													jpegFile, 
													xScale, 
													yScale, 
													desiredRes,
													OutPutFolderPath,
													SUbDirectoryPath,
													jpgPath);
				if (status != kSuccess) 
				{
					//CA("CreatePagePreview failed!");
				}
//CA("2");
				int32 newStageId = 1;
				bool16 statuss = ptrIAppFramework ->GetProjectProducts_updateWhiteBoardStageIdForSection(CurrentSelectedSubSection, newStageId);
				if(statuss == kFalse)
				{
					ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaNewForWBManualSpray::GetProjectProducts_updateWhiteBoardStageIdForSection == kFalse ");
					//return;

				}
	
				sdklhelp.CloseDocument(dataToBeSpreadDocumentUIDRef,kTrue);
				contentFileList.push_back(docFile);	
	
			}	
	}
	
	if(contentFileList.size()>0)
	{
		//CA("addDocumentsToBook........");
		addDocumentsToBook(contentFileList);
	}
 }
-*/
}


bool16 createNewLayer()
{
	bool16 result = kFalse;
	do
	{
		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutData == nil)
		{
			CA("layoutData == nil");
			break;
		}

		IDocument* document = layoutData->GetDocument();
		if (document == nil)
		{			
			CA("document == nil");
			break;
		}

		// Create the NewLayerCmd:
		InterfacePtr<ICommand> layerCmd(CmdUtils::CreateCommand(kNewLayerCmdBoss));

		// Set NewLayerCmd arguments:
		InterfacePtr<INewLayerCmdData> layerData(layerCmd, IID_INEWLAYERCMDDATA);
		if (layerData == nil)
		{
			CA("layerData is nil");
			break;
		}

		layerData->Set
		(
			GetUIDRef(document),	// const UIDRef& layerList
			nil,							// const PMString *name = nil
			kTrue,							// bool16 isVisible = kTrue, 
			kFalse,							// bool16 isLocked = kFalse, 
			kFalse,							// bool16 isGuideVisible = kTrue, 
			kFalse,							// bool16 isGuideLocked = kFalse, 
			kInvalidUID,					// UID layerColor = kInvalidUID, 
			kTrue,							// bool16 isUILayer = kTrue,
			kTrue							// bool16 isExpendable = kTrue;
		);	

		ErrorCode error = kFailure;
		// Process the command:
		error = CmdUtils::ProcessCommand(layerCmd);
		result = kTrue;
	}while(false);
	return result;
}

void createCommentsOnLayer()
{
/*-
	int32commentDataPtrVectorLength =static_cast<int32> (global_VectorAPpubCommentValue.size());  //Cs4
	
	if(commentDataPtrVectorLength  > 0)// if PubComments available for spray then go ahead 
	{
		//CA("Inside if");
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(!ptrIAppFramework)
		{
			//CA("ptrIAppFramework is NULL");		
			return;
		}
		
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(!iConverter)
		{
			//CA("!iConverter");		
			return;
		}

		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==NULL)
		{
			ptrIAppFramework->LogError("AP46_DataSprayerModel::CDataSprayer::fillImageInBox::!ptrIClientOptions");
			return;
		}

		///////////////// Adding Table types here ////////////////
		VectorTypeInfoPtr typeValObjForProductTable = ptrIAppFramework->ElementCache_getProductTableTypes();
		if(typeValObjForProductTable==nil)
		{
			ptrIAppFramework->LogError("AP46_CreateMediaMaster::CMMDialogObserver::createCommentsOnLayer::ElementCache_getProductTableTypes's typeValObj is nil");
			return;
		}

		VectorTypeInfoPtr typeValObjForItemTable = ptrIAppFramework->AttributeCache_getItemTableTypes();
		if(typeValObjForItemTable==nil){
			ptrIAppFramework->LogError("AP46_CreateMediaMaster::CMMDialogObserver::createCommentsOnLayer::AttributeCache_getItemTableTypes's typeValObj is nil");	
			return;
		}

		////////////////Adding Image Types here//////////////

		VectorTypeInfoPtr typeValObjForProductImage= ptrIAppFramework->ElementCache_getImageAttributesByIndex(3);
		if(typeValObjForProductImage == nil){
			ptrIAppFramework->LogError("AP46_CreateMediaMaster::CMMDialogObserver::createCommentsOnLayer::ElementCache_getImageAttributesByIndex's typeValObj is nil");	
			return;
		}
		//VectorTypeInfoPtr typeValObjForItemImage = ptrIAppFramework->AttributeCache_getItemImagesForClassAndParents(CurrentClassID);

		////////////////////////////////////////////////////////////////////////////////////
		
		
		IDocument* frontDocument = Utils<ILayoutUIUtils>()->GetFrontDocument();  //Cs4
		if(frontDocument == nil)
		{
			//CA("frontDocument is NULL");		
			return;
		}

		PMString imagePath=ptrIClientOptions->getImageDownloadPath();
		if(imagePath!="")
		{
			const char *imageP=  (imagePath.GrabCString());
			if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
				#ifdef MACINTOSH
					imagePath+="/";
				#else
					imagePath+="\\";
				#endif
		}
		if(imagePath=="")
		{
			ptrIAppFramework->LogError("SPSelectionObserver::createCommentsOnLayer::imagePath is blank");
			return;
		}

		int32 maxSpreadNumber = 0;
		for(int32 j = 0;j < commentDataPtrVectorLength ; j++)
		{
			APpubComment objAPpubComment = global_VectorAPpubCommentValue[j];
			if(objAPpubComment.spreadNumber > maxSpreadNumber)
				maxSpreadNumber = objAPpubComment.spreadNumber;
		}


		int32 currentSpreadNumber = 0;
		int32 i = 0;
		for(;i < commentDataPtrVectorLength ; i++)
		{
			
			ptrIAppFramework->clearAllStaticObjects();

			APpubComment objAPpubComment = global_VectorAPpubCommentValue[i];
			objIndex = i;
			currentSpreadNumber = objAPpubComment.spreadNumber;
			UIDRef frameUIDRef;
					
			//////////////////////////////

			InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
			if (layoutData == nil)
			{
				//CA("ILayoutControlData is nil");
				break;
			}

			IDocument* document = layoutData->GetDocument();
			if (document == nil)
			{
				//CA("document is nil");
				break;
			}
			
			IDataBase* database = ::GetDataBase(document);		
			if(database == nil)
			{
				//CA("database == nil");
				break;
			}

			InterfacePtr<ISpreadList>spreadList(document,UseDefaultIID());
			if(spreadList == nil)
			{
				//CA("spreadList == nil");
				break;
			}	

			UIDRef spreadUIDRef(database, spreadList->GetNthSpreadUID(objAPpubComment.spreadNumber-1));
			
			currentSpread = objAPpubComment.spreadNumber;


			InterfacePtr<ISpread> iSpread1(spreadUIDRef, IID_ISPREAD); 
			if(iSpread1 == nil)
			{
				//CA("spread1 == nil");
				//spreadNumber;
				break;
			}
	
			Transform::CoordinateSpace coordinateSpace_1 = Transform::PasteboardCoordinates() ;
			PMRect pageMargin = iSpread1->GetPagesBounds(coordinateSpace_1);//----CS5-----

			PMReal left3,top3,right3,bottom3;
			left3 = pageMargin.Left();
			top3 = pageMargin.Top();
			right3 = pageMargin.Right();
			bottom3 = pageMargin.Bottom();
			PMString Dimentions3;
			Dimentions3.Append(" LEFT3 = ");
			Dimentions3.AppendNumber(left3);
			Dimentions3.Append(", TOP3 = ");
			Dimentions3.AppendNumber(top3);
			Dimentions3.Append(", RIGHT3 = ");
			Dimentions3.AppendNumber(right3);
			Dimentions3.Append(", BOTTOM3 = ");
			Dimentions3.AppendNumber(bottom3);
			//CA(Dimentions3);

			int32 leftOfPage = ToInt32(left3);
			int32 rightOfPage = ToInt32(right3);
			int32 topOfPage = ToInt32(top3);
			int32 bottomOfPage = ToInt32(bottom3);
			int32 halfOfPageHeight = (bottomOfPage - topOfPage)/2;
			int32 halfOfPageWidth = ToInt32(right3);
			PMReal halfPage = (bottom3 - top3)/2;


			//////
			int numPages=iSpread1->GetNumPages();

			int32 pageWidth;
			int32 sideOfPage = 0; //0 for left & 1 for right
			if(numPages == 1)
			{
				if(leftOfPage == 0)
				{
					pageWidth = rightOfPage;
					sideOfPage = 1;
				}
				else
				{
					pageWidth = -leftOfPage;
					sideOfPage = 0;
				}
			}
			else if(numPages == 2)
			{
				pageWidth = (rightOfPage - leftOfPage)/2;
			}


			if(currentSectionIndex == 1 && numPages == 1)///this is only for first spread
			{
				if(sideOfPage == 0)
					pageNumber = 1;
				else if(sideOfPage == 1)
					pageNumber = 2;
			}
			else if(currentSectionIndex == 1 && numPages == 2)///this is only for first spread
			{				
				pageNumber = 1;					
			}
			else if(numPages == 2 && previousSpreadNumber == objAPpubComment.spreadNumber)
			{
				pageNumber = 2;	
			}
			else if(numPages == 1 && previousSpreadNumber != objAPpubComment.spreadNumber)
			{
				if(sideOfPage == 0)
					pageNumber = 1;
				else if(sideOfPage == 1)
					pageNumber = 2;
			}
			else if(numPages == 2 && previousSpreadNumber != objAPpubComment.spreadNumber)
			{
				pageNumber = 1;	
			}

			PMString temp;

			temp.Clear();

			PMPathPointList pathPointList;
			pathPointList.push_back(PMPathPoint(PMPoint(0.0, 0.0)));
			pathPointList.push_back(PMPathPoint(PMPoint(100.0, 100.0)));


			PMString IndsFileFolderPathForComment("");
			IndsFileFolderPathForComment.Append(imagePath);
			IndsFileFolderPathForComment.Append("InDesign");
			#ifdef MACINTOSH
				IndsFileFolderPathForComment+="/";
			#else
				IndsFileFolderPathForComment+="\\";
			#endif
			IndsFileFolderPathForComment.Append("WhiteBoard");
			#ifdef MACINTOSH
				IndsFileFolderPathForComment+="/";
			#else
				IndsFileFolderPathForComment+="\\";
			#endif

			IndsFileFolderPathForComment.AppendNumber(objAPpubComment.SECTION_NO);
			#ifdef MACINTOSH
				IndsFileFolderPathForComment+="/";
			#else
				IndsFileFolderPathForComment+="\\";
			#endif
			IndsFileFolderPathForComment.Append("PubComment");
			#ifdef MACINTOSH
				IndsFileFolderPathForComment+="/";
			#else
				IndsFileFolderPathForComment+="\\";
			#endif

			PMString IndsFileName("");
			IndsFileName.AppendNumber(objAPpubComment.APpubCommentID);
			IndsFileName.Append(".inds");

			//CA("IndsFileFolderPathForComment : " + IndsFileFolderPathForComment + IndsFileName);
			bool16 isIndsFileExist = fileExists(IndsFileFolderPathForComment, IndsFileName);
			if(isIndsFileExist)
			{
				//CA("isIndsFileExist");
				PMRect box;
				if(MediatorClass::IsPageBased)
				{
					if(pageNumber == 2)
					{
						box.Left(objAPpubComment.TOP_X_COORDINATE + pageWidth);
						box.Top(objAPpubComment.TOP_Y_COORDINATE);
						box.Right(objAPpubComment.BOT_X_COORDINATE + pageWidth);
						box.Bottom(objAPpubComment.BOT_Y_COORDINATE);
						
					}
					else
					{
						box.Left(objAPpubComment.TOP_X_COORDINATE);
						box.Top(objAPpubComment.TOP_Y_COORDINATE);
						box.Right(objAPpubComment.BOT_X_COORDINATE);
						box.Bottom(objAPpubComment.BOT_Y_COORDINATE);
					}
				}
				else
				{
					box.Left(objAPpubComment.TOP_X_COORDINATE);
					box.Top(objAPpubComment.TOP_Y_COORDINATE);
					box.Right(objAPpubComment.BOT_X_COORDINATE);
					box.Bottom(objAPpubComment.BOT_Y_COORDINATE);
				}
		
				PMString IndsFilePath = IndsFileFolderPathForComment;
				IndsFilePath.Append(IndsFileName);
				//CA("INDS File Exist at " + IndsFileFolderPathForComment + IndsFileName);
				bool16 result = ImportIndsFileInDocument(frontDocument,  IndsFilePath,  box,  objAPpubComment,halfPage,topOfPage);
				if(result){
					//CA("Import Successfull... continue");				
					continue;
				}
			}
			
			//CA("!isIndsFileExist");

			PMRect box;
			if(MediatorClass::IsPageBased)
			{
				if(pageNumber == 2)
				{
					box.Left(objAPpubComment.TOP_X_COORDINATE + pageWidth);
					box.Top(objAPpubComment.TOP_Y_COORDINATE + top3);
					box.Right(objAPpubComment.BOT_X_COORDINATE + pageWidth);
					box.Bottom(objAPpubComment.BOT_Y_COORDINATE + top3);
				}
				else
				{
					box.Left(objAPpubComment.TOP_X_COORDINATE);
					box.Top(objAPpubComment.TOP_Y_COORDINATE + top3);
					box.Right(objAPpubComment.BOT_X_COORDINATE);
					box.Bottom(objAPpubComment.BOT_Y_COORDINATE + top3);
				}
			}
			else
			{
				box.Left(objAPpubComment.TOP_X_COORDINATE);
				box.Top(objAPpubComment.TOP_Y_COORDINATE + top3);
				box.Right(objAPpubComment.BOT_X_COORDINATE);
				box.Bottom(objAPpubComment.BOT_Y_COORDINATE + top3);
			}
			
			CPbObjectValue* CPbObjectValueptr;
			switch(objAPpubComment.comment_type)
			{
				case 1:
					//CA("Case 1");
					createRectangle(box,0, frameUIDRef ,objAPpubComment.spreadNumber,pageWidth,objAPpubComment);
					break;

				case 2:
					//CA("Case 2");
					createRectangle(box,0, frameUIDRef ,objAPpubComment.spreadNumber,pageWidth,objAPpubComment);
					break;

				case 3:
					//CA("Case 3");
					createRectangle(box, 0, frameUIDRef ,objAPpubComment.spreadNumber,pageWidth,objAPpubComment);
					break;

				case 4:
					//CA("Case 4");
					createOval(box, 0,frontDocument, frameUIDRef);
					break;

				case 5:
					//CA("Case 5");
					createArrowGraphic(box, 0,frontDocument, frameUIDRef,pathPointList);
					break;

				case 6:
					//CA("Case 6");
					createRectangle(box,0, frameUIDRef ,objAPpubComment.spreadNumber,pageWidth,objAPpubComment);
					break;

				case 7:
					//CA("case 7");
					createRectangle(box, 0, frameUIDRef ,objAPpubComment.spreadNumber,pageWidth,objAPpubComment);
					break; 

				case 8:
					createTableFrame(box,objAPpubComment.stage, frameUIDRef ,objAPpubComment.spreadNumber,objAPpubComment.comment.id, objAPpubComment.assetId, objAPpubComment);
					break;

				case 9:
					//CA("case 9");
					CPbObjectValueptr = ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(objAPpubComment.SECTION_NO , objAPpubComment.comment.id);
					createTableFrame(box,objAPpubComment.stage, frameUIDRef ,objAPpubComment.spreadNumber, CPbObjectValueptr->getPub_object_id(), objAPpubComment.assetId, objAPpubComment);
					break;

				case 10:
					//CA("Case 10");
					createRectangle(box, objAPpubComment, frameUIDRef,pageWidth);
					break;

				case 11:
					//CA("case 11");
					createRectangle(box, objAPpubComment, frameUIDRef,pageWidth);
					break;
			}
	
			if((objAPpubComment.comment_type == 1 ||objAPpubComment.comment_type == 2 ||objAPpubComment.comment_type == 3 ||objAPpubComment.comment_type == 4||objAPpubComment.comment_type == 5 ))
			{
				InterfacePtr<IPMUnknown> unknown(frameUIDRef, IID_IUNKNOWN);
				if(!unknown)
					break;

				UID textFrameUID=Utils<IFrameUtils>()->GetTextFrameUID(unknown);
				if(textFrameUID==kInvalidUID )
				{
					convertBoxToTextBox(frameUIDRef);
					InterfacePtr<IPMUnknown> unknown(frameUIDRef, IID_IUNKNOWN);
					if(!unknown)
						break;
					textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);

					if(objAPpubComment.comment_type == 5)
					{
						addTagToGraphicFrame(frameUIDRef);
						objAPpubComment.isCommentPresentOnDocument = kTrue;
						continue;
					}
				}

				textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
				if(textFrameUID == kInvalidUID)
					break;

				
                ///Added From Here
				InterfacePtr<IHierarchy> graphicFrameHierarchy((frameUIDRef), UseDefaultIID());
				if (graphicFrameHierarchy == nil) 
				{
					//CA("graphicFrameHierarchy is NULL");
					
					break;
				}
								
				InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
				if (!multiColumnItemHierarchy) {
					//CA("multiColumnItemHierarchy is NULL");
					break;
				}

				InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
				if (!multiColumnItemTextFrame) {
					//CA("multiColumnItemTextFrame is NULL");
					break;
				}
				InterfacePtr<IHierarchy>
				frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
				if (!frameItemHierarchy) {
					//CA("frameItemHierarchy is NULL");
					break;
				}

				InterfacePtr<ITextFrameColumn>
				frameItemTFC(frameItemHierarchy, UseDefaultIID());
				if (!frameItemTFC) {
					//CA("!!ITextFrameColumn");
					break;
				}

				InterfacePtr<ITextModel> txtModel(frameItemTFC->QueryTextModel());
				if(!txtModel)
				{
					//CA("!textModel" );
					break;
				}
                //// Up To Here
				//		CA(commentDataPtrVector[i]->comment.commentString);
				PMString textToBeDisplayed(objAPpubComment.comment.commentString);
				if(objAPpubComment.comment_type == 1)//for product
				{
					textToBeDisplayed.Append("\n");
					CObjectValue cObjValue= ptrIAppFramework->GETProduct_getObjectElementValue(objAPpubComment.comment.id,objAPpubComment.languageId);
					PMString productName = iConverter->translateString(cObjValue.getName());
					textToBeDisplayed.Append(productName);
			//		CA(textToBeDisplayed);
					textToBeDisplayed.Append("\n");
					textToBeDisplayed.AppendNumber(objAPpubComment.comment.id);
			//		CA(textToBeDisplayed);
				}
				else if(objAPpubComment.comment_type == 2)//for item
				{
			//		CA("for item");
					textToBeDisplayed.Append("\n");
					CItemModel cItemModel= ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(objAPpubComment.comment.id,objAPpubComment.languageId);
					PMString itemName = iConverter->translateString(cItemModel.getItemDesc());
					textToBeDisplayed.Append(itemName);
			//		CA(textToBeDisplayed);
					textToBeDisplayed.Append("\n");
					textToBeDisplayed.AppendNumber(objAPpubComment.comment.id);
			//		CA(textToBeDisplayed);
				}

				WideString* displayString= &WideString(textToBeDisplayed);
				//.........
					int32 stringLength = displayString->Length();
				//.............
							//	
				txtModel->Insert(0,displayString);
			//	CA("b4 addTagToText"+ commentDataPtrVector[i]->comment.displayName);
				addTagToText(frameUIDRef,txtModel,objAPpubComment.comment.displayName);
			//	CA("after addTagToText");

				objAPpubComment.isCommentPresentOnDocument = kTrue;
			}
			if((objAPpubComment.comment_type == 6) || (objAPpubComment.comment_type == 7))//for product Image 
			{
				//CA("For Item & Product Image Comment Spray");
				PMString imagePathWithSubdir = imagePath;
				PMString typeCodeString;
				int32 ImageTypeId;
				int32 AssetParentTypeId = -1;

				do
				{
					//typeCodeString=ptrIAppFramework->ClientActionGetAssets_getProductAssetFolder(objAPpubComment.assetId);
					VectorAssetValuePtr AssetValuePtrObj = ptrIAppFramework->GETAssets_getAssetByAssetId(objAPpubComment.assetId);
					if(AssetValuePtrObj == NULL){
						//CA("Show Product images : AssetValuePtrObj == NULL");
						return ;
					}
		
					if(AssetValuePtrObj->size() ==0){
						//CA("AssetValuePtrObj->size() ==0");
						return ;			
					}

					PMString fileName("");
					VectorAssetValue::iterator it; // iterator of Asset value

				
					CAssetValue objCAssetvalue;
					for(it = AssetValuePtrObj->begin();it!=AssetValuePtrObj->end();it++)
					{
						objCAssetvalue = *it;
						fileName = objCAssetvalue.getFile_name();
						
						int32 assetID = objCAssetvalue.getAsset_id();					

						if(fileName=="")
							continue ;
						
						int32 image_mpv = objCAssetvalue.getMpv_value_id();
																
						AssetParentTypeId = objCAssetvalue.getParent_type_id();

						ImageTypeId = objCAssetvalue.getType_id();
						do{
							SDKUtilities::Replace(fileName,"%20"," ");
						}while(fileName.IndexOfString("%20") != -1);

					}

					if(objAPpubComment.comment_type == 6)
						typeCodeString = ptrIAppFramework->ClientActionGetAssets_getProductAssetFolder(objAPpubComment.assetId);
					if(objAPpubComment.comment_type == 7)
						typeCodeString = ptrIAppFramework->ClientActionGetAssets_getItemAssetFolder(objAPpubComment.assetId);

					if(typeCodeString.NumUTF16TextChars()!=0)
					{ 				
						PMString TempString = typeCodeString;		
						CharCounter ABC = 0;
						bool16 Flag = kTrue;
						bool16 isFirst = kTrue;
						do
						{					
							if(TempString.Contains("/", ABC))
							{
			 					ABC = TempString.IndexOfString("/"); 				
							
		 						PMString * FirstString = TempString.Substring(0, ABC);		 		
		 						PMString newsubString = *FirstString;		 	
										 		
		 						if(!isFirst)
		 						imagePathWithSubdir.Append("\\");
								
		 						imagePathWithSubdir.Append(newsubString);		 	
		 						FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir), kTrue);					 		
		 						isFirst = kFalse;					 		
		 						PMString * SecondString = TempString.Substring(ABC+1);
		 						PMString SSSTring =  *SecondString;		 		
		 						TempString = SSSTring;		
								if(FirstString)
									delete FirstString;
								if(SecondString)
									delete SecondString;
							}
							else
							{				
								if(!isFirst)
		 						imagePathWithSubdir.Append("\\");
								
		 						imagePathWithSubdir.Append(TempString);		 		
		 						FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir), kTrue);		 		
		 						isFirst = kFalse;				
								Flag= kFalse;							
							}			
						}while(Flag);
					}

					FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir));
					SDKUtilities ::AppendPathSeparator(imagePathWithSubdir);
									
					if(ptrIAppFramework->getAssetServerOption() == 0)
					{
						if(objAPpubComment.comment_type == 6)
						{
							if(!ptrIAppFramework->GETAsset_downLoadProductAsset(objAPpubComment.assetId,imagePathWithSubdir))
							{
								ptrIAppFramework->LogDebug("AP46_DataSprayerModel::CDataSprayer::fillImageInBox::!GETAsset_downLoadProductAsset");				
								continue;
							}
						}
						
						if(objAPpubComment.comment_type == 7)
						{
							if(!ptrIAppFramework->GETAsset_downLoadItemAsset(objAPpubComment.assetId,imagePathWithSubdir))
							{
								ptrIAppFramework->LogDebug("AP46_DataSprayerModel::CDataSprayer::fillImageInBox::!GETAsset_downLoadProductAsset");				
								continue;
							}
						}			
					}

					if(!fileExists(imagePathWithSubdir,fileName))	
					{
						ptrIAppFramework->LogDebug("CDataSprayer:: fillImageInBox: Image Not Found on Local Drive");
						continue; 
					}
					PMString total=imagePathWithSubdir+fileName;	
										
					if(ImportFileInFrame(frameUIDRef, total))
					{
						fitImageInBox(frameUIDRef);
					}
				}while(0);
				
				/// Creating and Adding tag to newly created Item image box
				InterfacePtr<IPMUnknown> unknown(frameUIDRef, IID_IUNKNOWN);
				if(unknown == NULL){
					ptrIAppFramework->LogDebug("CDataSprayer:: fillImageInBox: unknown NULL");
					return ;
				}
				InterfacePtr<IDocument> doc(frameUIDRef.GetDataBase(), frameUIDRef.GetDataBase()->GetRootUID(), UseDefaultIID());
				if(doc == NULL){
					ptrIAppFramework->LogDebug("CDataSprayer:: fillImageInBox: Document not found...");
					return ;
				}
				InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(doc));
				if (rootElement == NULL){			
					ptrIAppFramework->LogDebug("CDataSprayer:: fillImageInBox: IIDXMLElement NIL...");
					return ;
				}
				
				XMLReference parent = rootElement->GetXMLReference();

				PMString tagName("");
				
				VectorTypeInfoPtr typeValObjForProductImage = ptrIAppFramework->ElementCache_getImageAttributesByIndex(3);
				if(typeValObjForProductImage!=nil)
				{	
					VectorTypeInfoValue::iterator it2;
			
					for(it2=typeValObjForProductImage->begin();it2!=typeValObjForProductImage->end();it2++)
					{	
						int32 typeID = it2->getType_id();
						if(typeID == ImageTypeId)
						{
							//CA("typeID == ImageTypeId");
							tagName = it2->getName();
							break;
						}
					}
				}

				PMString TagName("");				
				
				if(objAPpubComment.comment_type == 7)
				{
					 tagName = ptrIAppFramework->TYPECACHE_getTypeNameById(ImageTypeId);
				}

				TagName = prepareTagName(tagName);

				CPbObjectValue* CPbObjectValueptr = ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(objAPpubComment.SECTION_NO , objAPpubComment.comment.id);

				XMLReference resultXMLRef=kInvalidXMLReference;
				do {
					// Acquire the IXLMElementCommands interface on the Utils boss, and use its method
					// to tag the frame.  Arbitrarily ask for the element to be the 0th child of it's parent.
					ErrorCode errCode = Utils<IXMLElementCommands>()->CreateElement(WideString(TagName), frameUIDRef.GetUID(), parent, 0, &resultXMLRef);  //Cs4
					// Verify the results: no errors, valid XMLRef returned, we can instantiate it.
					if (errCode != kSuccess)
					{
						//CA("ExpXMLActionComponent::TagFrameElement - CreateElement failed");
						break;
					}		
					if (resultXMLRef == kInvalidXMLReference)
					{
						//CA("ExpXMLActionComponent::TagFrameElement - Can't create new XMLReference");
						break;
					}		
					InterfacePtr<IIDXMLElement> newXMLElement (resultXMLRef.Instantiate());
					if (newXMLElement==NULL) 
					{
						//CA("newXMLElement==NULL");
						ptrIAppFramework->LogInfo("CDataSprayer:: fillImageInBox: ExpXMLActionComponent::TagFrameElement - Can't instantiate new XML element");
					}		
					
					PMString attribName("ID");
					PMString attribVal("");
					attribVal.AppendNumber(-1);							
					ErrorCode err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "typeId";
					attribVal.AppendNumber(ImageTypeId);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "index";

					if(objAPpubComment.comment_type == 6)
					{
						attribVal.AppendNumber(3);	
					}
					if(objAPpubComment.comment_type == 7)
					{
						attribVal.AppendNumber(4);	
					}				

					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal)); //CS4
					
					attribName.Clear();
					attribVal.Clear();
					attribName = "imgFlag";
					attribVal.AppendNumber(1);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal)); //Cs4
				
					attribName.Clear();
					attribVal.Clear();
					attribName = "parentID";
					attribVal.AppendNumber(objAPpubComment.comment.id);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
					
					attribName.Clear();
					attribVal.Clear();
					attribName = "parentTypeID";					
					attribVal.AppendNumber(AssetParentTypeId);					
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
					
					attribName.Clear();
					attribVal.Clear();
					attribName = "sectionID";
					attribVal.AppendNumber(CurrentSelectedSection);						
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
				
					attribName.Clear();
					attribVal.Clear();
					attribName = "tableFlag";
					attribVal.AppendNumber(0);						
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "LanguageID";								
					attribVal.AppendNumber(objAPpubComment.languageId);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "isAutoResize";							
					attribVal.AppendNumber(objAPpubComment.APpubCommentID);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "rowno";
					//attribVal.AppendNumber(-1);
					attribVal.AppendNumber(-1); // Do not set Pub commnet id for Images
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "colno";
					attribVal.AppendNumber(-1);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
				}while (false);
			}			


			if((objAPpubComment.comment_type == 8) || (objAPpubComment.comment_type == 9))//for product Table 
			{
				// For Item & Product Table Comment Spray
			
				int32 TableTypeID = objAPpubComment.assetId;

				PMString tagName("");
				if(objAPpubComment.comment_type == 8)
				{
					///////////////// Adding Table types here ////////////////
					VectorTypeInfoPtr typeValObjForProductTable = ptrIAppFramework->ElementCache_getProductTableTypes();
					if(typeValObjForProductTable==nil)
					{
						ptrIAppFramework->LogError("AP46_TemplateBuilder::TPLSelectionObserver::populatePRPanelLstbox::ElementCache_getProductTableTypes's typeValObj is nil");
						return;
					}

					if(typeValObjForProductTable!=nil)
					{	
						VectorTypeInfoValue::iterator it2;
				
						for(it2=typeValObjForProductTable->begin();it2!=typeValObjForProductTable->end();it2++)
						{	
							int32 typeID = it2->getType_id();
							if(typeID == TableTypeID)
							{
								//CA("typeID == ImageTypeId");
								tagName = it2->getName();
								break;
							}
						}
					}
				}
				if(objAPpubComment.comment_type == 9)
				{
					VectorTypeInfoPtr typeValObjForItemTable = ptrIAppFramework->AttributeCache_getItemTableTypes();
					if(typeValObjForItemTable==nil){
						ptrIAppFramework->LogError("AP46_TemplateBuilder::TPLSelectionObserver::populateItemPanelLstbox::AttributeCache_getItemTableTypes's typeValObj is nil");	
						return;
					}

					if(typeValObjForItemTable!=nil)
					{	
						VectorTypeInfoValue::iterator it2;
				
						for(it2=typeValObjForItemTable->begin();it2!=typeValObjForItemTable->end();it2++)
						{	
							int32 typeID = it2->getType_id();
							if(typeID == TableTypeID)
							{
								//CA("typeID == TableTypeID");
								tagName = it2->getName();
								break;
							}
						}
					}
				}

				//CA("tagName = " + tagName);

				/// Creating and Adding tag to newly created Item image box
				InterfacePtr<IPMUnknown> unknown(frameUIDRef, IID_IUNKNOWN);
				if(unknown == NULL){
					ptrIAppFramework->LogDebug("CDataSprayer:: fillImageInBox: unknown NULL");
					return ;
				}
				InterfacePtr<IDocument> doc(frameUIDRef.GetDataBase(), frameUIDRef.GetDataBase()->GetRootUID(), UseDefaultIID());
				if(doc == NULL){
					ptrIAppFramework->LogDebug("CDataSprayer:: fillImageInBox: Document not found...");
					return ;
				}
				InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(doc));
				if (rootElement == NULL){			
					ptrIAppFramework->LogDebug("CDataSprayer:: fillImageInBox: IIDXMLElement NIL...");
					return ;
				}
				
				XMLReference parent = rootElement->GetXMLReference();

				PMString TagName = prepareTagName(tagName);

				CPbObjectValueptr = ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(objAPpubComment.SECTION_NO , objAPpubComment.comment.id);
				//CPbObjectValue* CPbObjectValueptr = ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(objAPpubComment.SECTION_NO , objAPpubComment.comment.id);

				XMLReference resultXMLRef=kInvalidXMLReference;
				do {
					// Acquire the IXLMElementCommands interface on the Utils boss, and use its method
					// to tag the frame.  Arbitrarily ask for the element to be the 0th child of it's parent.
					ErrorCode errCode = Utils<IXMLElementCommands>()->CreateElement(WideString(TagName), frameUIDRef.GetUID(), parent, 0, &resultXMLRef);
					// Verify the results: no errors, valid XMLRef returned, we can instantiate it.
					if (errCode != kSuccess)
					{
						//CA("ExpXMLActionComponent::TagFrameElement - CreateElement failed");
						break;
					}		
					if (resultXMLRef == kInvalidXMLReference)
					{
						//CA("ExpXMLActionComponent::TagFrameElement - Can't create new XMLReference");
						break;
					}		
					InterfacePtr<IIDXMLElement> newXMLElement (resultXMLRef.Instantiate());
					if (newXMLElement==NULL) 
					{
						//CA("newXMLElement==NULL");
						ptrIAppFramework->LogInfo("CDataSprayer:: fillImageInBox: ExpXMLActionComponent::TagFrameElement - Can't instantiate new XML element");
					}		
					
					PMString attribName("ID");
					PMString attribVal("");
					attribVal.AppendNumber(-1);							
					ErrorCode err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "typeId";
					attribVal.AppendNumber(TableTypeID);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "index";

					if(objAPpubComment.comment_type == 8)
					{
						attribVal.AppendNumber(3);	
					}
					if(objAPpubComment.comment_type == 9)
					{
						attribVal.AppendNumber(4);	
					}
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
					
					attribName.Clear();
					attribVal.Clear();
					attribName = "imgFlag";
					attribVal.AppendNumber(0);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
				
					attribName.Clear();
					attribVal.Clear();
					attribName = "parentID";
					attribVal.AppendNumber(objAPpubComment.comment.id);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//cs4
					
					attribName.Clear();
					attribVal.Clear();
					attribName = "parentTypeID";
					attribVal.AppendNumber(CPbObjectValueptr->getobject_type_id());							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
					
					attribName.Clear();
					attribVal.Clear();
					attribName = "sectionID";
					attribVal.AppendNumber(CurrentSelectedSection);						
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
				
					attribName.Clear();
					attribVal.Clear();
					attribName = "tableFlag";
					attribVal.AppendNumber(1);						
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//CS4

					attribName.Clear();
					attribVal.Clear();
					attribName = "LanguageID";								
					attribVal.AppendNumber(objAPpubComment.languageId);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "isAutoResize";							
					attribVal.AppendNumber(objAPpubComment.APpubCommentID);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName = "rowno";
					attribVal.AppendNumber(-1);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "colno";
					attribVal.AppendNumber(-1);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
				}while (false);

				InterfacePtr<ITableUtility> iTableUtlObj
				((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
				if(!iTableUtlObj)
				{
					// CA("!iTableUtlObj");
					ptrIAppFramework->LogError("AP46_CreateMediaMaster::CMMDialogOserver::fillDataInTable::!iTableUtlObj");
					return ;
				}

				InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
				if(!DataSprayerPtr)
				{
                    ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogOserver::getAllBoxIds::Pointre to DataSprayerPtr not found");
					return;
				}

				InterfacePtr<ITagReader> itagReader
				((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
				if(!itagReader)
				{
					ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogOserver::createCommentsOnLayer:itagReader == nil");
					return;
				}

				IIDXMLElement* ptr = NULL;
				TagList tagList = itagReader->getFrameTags(frameUIDRef);

				TagStruct tagStruct = tagList[0];

				PublicationNode refpNode;
				PMString PubName("PubName");

				if(tagStruct.whichTab == 3)
					refpNode.setAll(PubName, tagStruct.parentId, CPbObjectValueptr->getPub_object_id(),tagStruct.sectionID,1,1,1,tagStruct.typeId,0,kTrue,kFalse);
				else if(tagStruct.whichTab == 4)
					refpNode.setAll(PubName, tagStruct.parentId, CPbObjectValueptr->getPub_object_id(),tagStruct.sectionID,1,1,1,tagStruct.typeId,0,kFalse,kFalse);
			
				DataSprayerPtr->FillPnodeStruct(refpNode, tagStruct.sectionID, 1, tagStruct.sectionID);
				
				DataSprayerPtr->getAllIds(tagStruct.parentId);
				iTableUtlObj->SetDBTableStatus(kTrue);
				
				int32 tableId=0;
				//CA("Going for DataSprayer fillDataInTable ");
				UIDRef tableUIDRef;
				if(isTablePresent(frameUIDRef, tableUIDRef))
				{
					if(objAPpubComment.comment_type == 8)
					{
						iTableUtlObj->fillDataInTable(tableUIDRef, refpNode, tagStruct, tableId, frameUIDRef);
					}
					if(objAPpubComment.comment_type == 9)
					{
						XMLReference boxXMLRef = tagStruct.tagPtr->GetXMLReference();
						iTableUtlObj->FillDataInsideItemTableOfItem(boxXMLRef ,tableUIDRef);
					}
				}			
			}


			if(objAPpubComment.comment_type == 10 || objAPpubComment.comment_type == 11)
			{
				// For Item & Product Copy Frame Comment Spray 
			//	CA("1");
				InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
				if(!DataSprayerPtr)
				{
                    ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogOserver::createCommentsOnLayer::Pointre to DataSprayerPtr not found");
					return;
				}

			//	CA("2");
				InterfacePtr<ITagReader> itagReader
				((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
				if(!itagReader)
				{
					ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogOserver::createCommentsOnLayer::itagReader == nil");
					return;
				}

				CPbObjectValue* CPbObjectValueptr = ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(objAPpubComment.SECTION_NO , objAPpubComment.comment.id);
				
			//	CA("3");
				PublicationNode pNode;
				
				//pNode.setPublicationName();
				pNode.setPubId(objAPpubComment.comment.id);
				pNode.setParentId(objAPpubComment.SECTION_NO);
				pNode.setLevel(CPbObjectValueptr->getLevel_no());
				pNode.setSequence(CPbObjectValueptr->getSeq_order());
				pNode.setChildCount(CPbObjectValueptr->getChildCount());
				//pNode.setHitCount();
				//pNode.setReferenceId();
				pNode.setTypeId(CPbObjectValueptr->getobject_type_id());
				//pNode.setDesignerAction();
				pNode.setNewProduct(CPbObjectValueptr->getNew_product());
				pNode.setPBObjectID(CPbObjectValueptr->getPub_object_id());
				pNode.setIsProduct(CPbObjectValueptr->getisProduct());
				pNode.setIsONEsource(kFalse);
				pNode.setIsStarred(CPbObjectValueptr->getStarredFlag1());
				DataSprayerPtr->FillPnodeStruct(pNode,objAPpubComment.SECTION_NO,CPbObjectValueptr->getPublication_id(),objAPpubComment.SECTION_NO);

				DataSprayerPtr->setFlow(kFalse);
				DataSprayerPtr->getAllIds(pNode.getPubId());//For these PF, PR, PG ITEM ID's we have to spray the data

				IIDXMLElement* ptr = NULL;
				TagList tagList = itagReader->getTagsFromBox(frameUIDRef, &ptr);
	
				if(tagList.size()>=0)
				{
					
					for(int32 tagCounter=0;tagCounter<tagList.size() ;tagCounter++)
					{
						XMLReference  childXMLReference = ptr->GetNthChild(tagCounter);
						//IIDXMLElement* chldPtr = childXMLReference.Instantiate();
						InterfacePtr<IIDXMLElement>chldPtr(childXMLReference.Instantiate());
						TagStruct tagInfoo;
						tagInfoo = tagList[tagCounter];
						PMString isAutoResize("");
						isAutoResize.AppendNumber(objAPpubComment.APpubCommentID);
						//CA(rowNo);
						chldPtr->SetAttributeValue(WideString("isAutoResize"),WideString(isAutoResize));//Cs4
						
					}

				}

				if(tagList.size()<=0)//This can be a Tagged Frame
				{
					if(DataSprayerPtr->isFrameTagged(frameUIDRef))
					{	
						//CA("isFrameTagged(selectUIDList.GetRef(i))");
						DataSprayerPtr->sprayForTaggedBox(frameUIDRef);				
					}
					continue;
				}
				else
					DataSprayerPtr->sprayForThisBox(frameUIDRef, tagList);
				

				// Added
				for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
				{
					tagList[tagIndex].tagPtr->Release();
				}
				//

				UIDList list(frameUIDRef);
				resizeTextFrame(frameUIDRef,list,objAPpubComment.BOT_X_COORDINATE - objAPpubComment.TOP_X_COORDINATE, objAPpubComment.BOT_Y_COORDINATE - objAPpubComment.TOP_Y_COORDINATE,halfPage,topOfPage);
				//DataSprayerPtr->sprayForThisBox(frameUIDRef,);
				//CA("5");
			}
		}
		objIndex = 0;
		previousSpreadNumber = currentSpreadNumber;
	}
-*/
}

/*-
 void createRectangle(PMRect box,int32 layerNo,UIDRef& frameUIDRef, int32 spreadNumber, int32 pageWidth,APpubComment objAPpubComment)
{
	UIDRef result = UIDRef::gNull;


	do
	{
		//addLayerIfNeeded();

		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutData == nil)
		{
			//CA("ILayoutControlData is nil");
			break;
		}

		IDocument* document = layoutData->GetDocument();
		if (document == nil)
		{
			//CA("document is nil");
			break;
		}
		
		IDataBase* database = ::GetDataBase(document);		
		if(database == nil)
		{
			//CA("database == nil");
			break;
		}

		InterfacePtr<ISpreadList>spreadList(document,UseDefaultIID());
		if(spreadList == nil)
		{
			//CA("spreadList == nil");
			break;
		}	

		UIDRef firstSreadUIDRef(database, spreadList->GetNthSpreadUID(spreadNumber-1)); 
		IControlView* fntView = Utils<ILayoutUIUtils>()->QueryFrontView();
		if (fntView == nil)
		{
			//CA("The front view is nil.");
			break;
		}

		InterfacePtr<IGeometry> spreadGeo(firstSreadUIDRef, UseDefaultIID());
		if (!spreadGeo) {
			//CA("!spreadGeo");
			break;
		}
		InterfacePtr<ICommand> showSprdCmd(Utils<ILayoutUIUtils>()->MakeScrollToSpreadCmd(fntView, spreadGeo, kTrue));
		CmdUtils::ProcessCommand(showSprdCmd);

		if (CmdUtils::ProcessCommand(showSprdCmd) != kSuccess) {
			//CA("MakeScrollToSpreadCmd failed");
			break;
		}


		InterfacePtr<ISpread> iSpread(firstSreadUIDRef, IID_ISPREAD); 
		if(iSpread == nil)
		{
			//CA("spread == nil");
			spreadNumber;
		}

		int numPages=iSpread->GetNumPages();

		// Get the list of document layers
		InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);
		if (layerList == nil)
		{
			//CA("layerList is nil");			
			break;
		}

		int32 layerCount = layerList->GetCount();
		
		//layerNo = 1;

		///Commented

		IDocumentLayer* documentLayer = Utils<ILayerUtils>()->QueryDocumentActiveLayer(document);//(layerList->QueryLayer(layerNo));////ADed
		if (documentLayer == nil)
		{
			//CA("documentLayer is nil");				
			break;
		}
		documentLayer->SetVisible(kTrue);

		InterfacePtr<ISpreadLayer> spreadLayer(iSpread->QueryLayer(documentLayer));
		if (spreadLayer == nil)
		{
			//CA("spreadLayer is nil");		
			break;
		}

		UIDRef layerRef = ::GetUIDRef(spreadLayer);

		//InterfacePtr<IGeometry> pageGeo(layerRef.GetDataBase(), iSpread->GetNthPageUID(pageNo-1) , UseDefaultIID());
		//if (pageGeo == nil )
		//{
		//	//CA("pageGeo is nil");
		//	break;
		//}

		frameUIDRef = Utils<IPathUtils>()->CreateRectangleSpline(layerRef,	box, 
													INewPageItemCmdData::kGraphicFrameAttributes,0x263);

		const UIDList splineItemList(frameUIDRef);

		PMReal strokeWeight(0.0);

		InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->
		CreateStrokeWeightCommand(strokeWeight,&splineItemList,kTrue,kTrue));

		if(strokeSplineCmd==nil)
		{
			//CA("strokeSplineCmd is nil");
			break;
		}

		if(CmdUtils::ProcessCommand(strokeSplineCmd)==kFailure)
		{
			//CA("command  is failed");		
			break;
		}

		InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QueryActiveSwatchList());
		if(iSwatchList==nil)
		{
			//CA("iSwatchList is nil");		
			break;
		}

		
		UID blackUID = iSwatchList->GetNoneSwatchUID(); //GetPaperSwatchUID(); 
		//UID blackUID = iSwatchList->GetBlackSwatchUID(); 
		InterfacePtr<ICommand> applyStrokeCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeRenderingCommand(blackUID,&splineItemList,kTrue,kTrue));

		if(applyStrokeCmd==nil)
		{
			//CA("applyStrokeCmd is nil");		
			break;
		}

		if(CmdUtils::ProcessCommand(applyStrokeCmd)!=kSuccess)
		{
			//CA("processCmd is failed");		
			break;
		}
 
		bool16 selectFrame = SelectFrame(frameUIDRef);

		//move newly created frame to specified cordinates
		PMRect box1;
		if(MediatorClass::IsPageBased)
		{
			if(pageNumber == 2)
			{
				if(numPages == 1)
				{
					box1.Left(objAPpubComment.TOP_X_COORDINATE);
					box1.Top(objAPpubComment.TOP_Y_COORDINATE);
					box1.Right(objAPpubComment.BOT_X_COORDINATE);
					box1.Bottom(objAPpubComment.BOT_Y_COORDINATE);
				}
				else
				{
				//PMRect box1(objAPpubComment.TOP_X_COORDINATE + pageWidth,objAPpubComment.TOP_Y_COORDINATE,objAPpubComment.BOT_X_COORDINATE + pageWidth, objAPpubComment.BOT_Y_COORDINATE);
				box1.Left(objAPpubComment.TOP_X_COORDINATE + pageWidth);
				box1.Top(objAPpubComment.TOP_Y_COORDINATE);
				box1.Right(objAPpubComment.BOT_X_COORDINATE + pageWidth);
				box1.Bottom(objAPpubComment.BOT_Y_COORDINATE);
			}
			}
			else
			{
				//PMRect box1(objAPpubComment.TOP_X_COORDINATE,objAPpubComment.TOP_Y_COORDINATE ,objAPpubComment.BOT_X_COORDINATE, objAPpubComment.BOT_Y_COORDINATE);
				box1.Left(objAPpubComment.TOP_X_COORDINATE - pageWidth);
				box1.Top(objAPpubComment.TOP_Y_COORDINATE);
				box1.Right(objAPpubComment.BOT_X_COORDINATE - pageWidth);
				box1.Bottom(objAPpubComment.BOT_Y_COORDINATE);
			}
		}
		else
		{
			//PMRect box1(objAPpubComment.TOP_X_COORDINATE,objAPpubComment.TOP_Y_COORDINATE ,objAPpubComment.BOT_X_COORDINATE, objAPpubComment.BOT_Y_COORDINATE);
			box1.Left(objAPpubComment.TOP_X_COORDINATE);
			box1.Top(objAPpubComment.TOP_Y_COORDINATE);
			box1.Right(objAPpubComment.BOT_X_COORDINATE);
			box1.Bottom(objAPpubComment.BOT_Y_COORDINATE);
		}
		moveCreatedFrame(frameUIDRef,box1);

//CA("createRectangle 11");
		
		//commented yto avoid crash
	//do
	//{
	//	
	//	UID pageUID = iSpread->GetNthPageUID(pageNumber - 1);
	//	InterfacePtr<IMasterSpreadList> iMasterSpreadList(document,UseDefaultIID());
	//	if(iMasterSpreadList == nil)
	//	{
	//		//CA("iMasterSpreadList == nil");
	//		break;
	//	}
		
	//	PMString MasterName = objAPpubComment.masterStencilPage;
	//	if(MasterName.NumUTF16TextChars() == 0)
	//	{
	//		//CA("MasterName.NumUTF16TextChars() == 0");
	//		break;
	//	}

	//	UID masterSpreadUID = kInvalidUID;
	//	for(int32 i=0; i<iMasterSpreadList->GetMasterSpreadCount(); i++) 
	//	{ 
	//		UIDRef masterspreadRef(database, iMasterSpreadList->GetNthMasterSpreadUID(i)); 
	//		InterfacePtr<IMasterSpread> masterSpread(masterspreadRef, UseDefaultIID());
	//					
	//		PMString CurrentMasterName("");
	//		masterSpread->GetName(&CurrentMasterName);
	//		//CA(CurrentMasterName);

	//		if(MasterName == CurrentMasterName)
	//		{
	//			CA("MasterName == CurrentMasterName  :: " + MasterName);
	//			masterSpreadUID = iMasterSpreadList->GetNthMasterSpreadUID(i);
	//			break;
	//		}
						
	//	}

	//	//CA("createRectangle 13");
	//	if(masterSpreadUID == kInvalidUID)
	//		break;

	//	//CA("createRectangle 13.5");	
	//	UIDRef masterSpreadUIDRef(database,masterSpreadUID);

	//	InterfacePtr<ICommand> command(CmdUtils::CreateCommand( kApplyMasterSpreadCmdBoss)); 
	//	if(command == nil)
	//	{
	//		//CA("command == nil");
	//		break;
	//	}

	//	//CA("createRectangle 13.6");			
	//	UIDList pageUIDList(database);
	//	pageUIDList.Insert(pageUID);
	//	command->SetItemList(pageUIDList);
	
	//	InterfacePtr<IApplyMasterCmdData>applyMasterCmd(command,UseDefaultIID());
	//	if(applyMasterCmd == nil)
	//	{			
	//		//CA("applyMasterCmd == nil");
	//		break;
	//	}
	//	//CA("createRectangle 13.7");	
	//	applyMasterCmd->Set 
	//		( 
	//			masterSpreadUIDRef, 
	//			masterSpreadUIDRef, 
	//			masterSpreadUIDRef 
	//		); 
	//	//CA("createRectangle 13.8");			
	//	ErrorCode status = CmdUtils::ProcessCommand(command); 

	//	//CA("createRectangle 14");	
	//	if(status == kFailure)
	//	{
	//		//CA("Master can not be applied to pages!!!");
	//		break;
	//	}
	//	//result = kTrue;
	//	break;



	//}while(kFalse);
		
	}while(false);
	//CA("Before return");
	return ;
}

-*/

//void SPSelectionObserver::createRectangle(PMRect box,int32 layerNo,UIDRef& frameUIDRef,int32 maxSpread)
//{
//	UIDRef result = UIDRef::gNull;
//	do
//	{
//		addLayerIfNeeded();
//
//		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
//		if (layoutData == nil)
//		{
//			CA("ILayoutControlData is nil");
//			break;
//		}
//
//		IDocument* document = layoutData->GetDocument();
//		if (document == nil)
//		{
//			CA("document is nil");
//			break;
//		}
//
//		InterfacePtr<ISpreadList>spreadList(document,UseDefaultIID());
//		if(spreadList == nil)
//		{
//			CA("spreadList == nil");
//			continue;
//		}
//
//		int32 spreadCount = spreadList->GetSpreadCount();
//
//		/*PMString totalSpreads("");
//		totalSpreads.AppendNumber(spreadCount);
//		CA("totalSpreads = " + totalSpreads);*/
//
//		IGeometry* spreadItem = layoutData->GetSpread();
//		if(spreadItem == nil)
//		{
//			CA("spreadItem is nil");		
//			break;
//		}
//
//		InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
//		if (iSpread == nil)
//		{
//			CA("iSpread is nil");				
//			break;
//		}
//
//		// Get the list of document layers
//		InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);
//		if (layerList == nil)
//		{
//			CA("layerList is nil");			
//			break;
//		}
//		
//		int32 layerCount = layerList->GetCount();
//		
//		/*PMString Temp("layerCount = ");
//		Temp.AppendNumber(layerCount);
//		Temp.Append(" , layerNo = ");
//		Temp.AppendNumber(layerNo);
//		CA(Temp);*/
//		/*bool16 status = kFalse;
//		while(1)
//		{
//			if(layerCount <= layerNo)
//			{
//				status = createNewLayer();
//				if(status == kFalse)
//					break;
//				layerCount++;
//			}
//			else 
//				break;
//		}*/
//
//		
//		layerNo++;
//		layerNo++;
//		IDocumentLayer* iDocumentLayer = layerList->GetLayer(layerNo);
//		if (iDocumentLayer == nil)
//		{
//			CA("iDocumentLayer is nil");				
//			break;
//		}
//
//		InterfacePtr<ISpreadLayer> spreadLayer(iSpread->QueryLayer(iDocumentLayer));
//		if (spreadLayer == nil)
//		{
//			CA("spreadLayer is nil");		
//			break;
//		}
//
//		UIDRef layerRef = ::GetUIDRef(spreadLayer);
//
//		InterfacePtr<IGeometry> pageGeo(layerRef.GetDataBase(), layoutData->GetPage(), UseDefaultIID());
//		if (pageGeo == nil )
//		{
//			//CA("pageGeo is nil");
//			break;
//		}
//		//CA("before CreateRectangleSpline");
//		frameUIDRef = Utils<IPathUtils>()->CreateRectangleSpline(layerRef,	box, 
//													INewPageItemCmdData::kGraphicFrameAttributes,0x263);
//		//CA("after CreateRectangleSpline");
//		const UIDList splineItemList(frameUIDRef);
//
//		ICommandSequence *sequence=CmdUtils::BeginCommandSequence();
//		if (sequence==nil)
//		{
//			CA("sequence is nil");		
//			break;
//		}
//
//		PMReal strokeWeight(0.0);
//
//		InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->
//		CreateStrokeWeightCommand(strokeWeight,&splineItemList,kTrue,kTrue));
//
//		if(strokeSplineCmd==nil)
//		{
//			CA("strokeSplineCmd is nil");
//			break;
//		}
//
//		if(CmdUtils::ProcessCommand(strokeSplineCmd)==kFailure)
//		{
//			//CA("command  is failed");		
//			break;
//		}
//
//		InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QueryActiveSwatchList());
//		if(iSwatchList==nil)
//		{
//			//CA("iSwatchList is nil");		
//			break;
//		}
//
//		//UID blackUID = iSwatchList->GetPaperSwatchUID(); 
//		UID blackUID = iSwatchList->GetBlackSwatchUID(); 
//		InterfacePtr<ICommand> applyStrokeCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeRenderingCommand(blackUID,&splineItemList,kTrue,kTrue));
//
//		if(applyStrokeCmd==nil)
//		{
//			//CA("applyStrokeCmd is nil");		
//			break;
//		}
//
//		if(CmdUtils::ProcessCommand(applyStrokeCmd)!=kSuccess)
//		{
//			//CA("processCmd is failed");		
//			break;
//		}
//
//		CmdUtils::EndCommandSequence(sequence);
//		//CA("before SelectFrame");
//		bool16 selectFrame = SelectFrame(frameUIDRef);
//		//CA("before moveCreatedFrame");
//		//*****************move newly created frame to specified cordinates
//		moveCreatedFrame(frameUIDRef,box);
//		//CA("After moveCreatedFrame");
//	}while(false);
//	return ;
//}

void createOval(PMRect box,int32 layerNo,IDocument* document,UIDRef& frameUIDRef)
{
	UIDRef result = UIDRef::gNull;
	do
	{
		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutData == nil)
			break;
		
		///***Commented
		/*IGeometry* spreadItem = layoutData->GetSpread();
		if(spreadItem == nil)
			break;*/
			
		UIDRef spreadRef=layoutData->GetSpreadRef();////@@@@Added	

		InterfacePtr<ISpread> iSpread(/*spreadItem*/spreadRef, UseDefaultIID());
		if (iSpread == nil)
			break;

		InterfacePtr <IDocument> myDoc(document, UseDefaultIID());
		if(!myDoc)
			break;

		// Get the list of document layers
		InterfacePtr<ILayerList> layerList(myDoc, IID_ILAYERLIST);
		if(!layerList)
			break;

		int32 layerCount = layerList->GetCount();

		///***Commeneted	
		//IDocumentLayer* iDocumentLayer = layerList->GetLayer(/*layerNo*/2);
		//if(!iDocumentLayer )
		//	break;
		InterfacePtr<IDocumentLayer> documentLayer(layerList->QueryLayer(/*documentLayerIndex*/2));///***Added

		InterfacePtr<ISpreadLayer> spreadLayer(iSpread->QueryLayer(documentLayer));
		if (spreadLayer == nil)
			break;

		UIDRef layerRef = ::GetUIDRef(spreadLayer);

		InterfacePtr<IGeometry> pageGeo(layerRef.GetDataBase(), layoutData->GetPage(), UseDefaultIID());
		if (pageGeo == nil )
			break;

		frameUIDRef = Utils<IPathUtils>()->CreateOvalSpline(layerRef,	box, 
													INewPageItemCmdData::kGraphicFrameAttributes,0x263);

		
		const UIDList splineItemList(frameUIDRef);

		/*ICommandSequence *sequence=CmdUtils::BeginCommandSequence();
		if (sequence==nil)
			break;*/

		PMReal strokeWeight(0.0);

		InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->
		CreateStrokeWeightCommand(strokeWeight,&splineItemList,kTrue,kTrue));

		if(strokeSplineCmd==nil)
			break;

		if(CmdUtils::ProcessCommand(strokeSplineCmd)==kFailure)
			break;

		InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QueryActiveSwatchList());
		if(iSwatchList==nil)
			break;

		UID blackUID = iSwatchList->GetNoneSwatchUID(); //GetNoneSwatchUID();

		InterfacePtr<ICommand> applyStrokeCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeRenderingCommand(blackUID,&splineItemList,kTrue,kTrue));

		if(applyStrokeCmd==nil)
			break;

		if(CmdUtils::ProcessCommand(applyStrokeCmd)!=kSuccess)
			break;

		//CmdUtils::EndCommandSequence(sequence);

		bool16 selectFrame = SelectFrame(frameUIDRef);

		//*****************move newly created frame to specified cordinates
		moveCreatedFrame(frameUIDRef,box);
		
	}while(false);
	return ;
}


void createArrowGraphic(PMRect box,int32 layerNo,IDocument* document,UIDRef& frameUIDRef,PMPathPointList& pathPointList)
{
	UIDRef result = UIDRef::gNull;
	SDKLayoutHelper layoutHelper;

	do
	{
		InterfacePtr<ILayoutControlData> layoutControlData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutControlData == nil)
			break;
			
		//***Commented	
		/*IGeometry* spreadItem = layoutControlData->GetSpread();
		if(spreadItem == nil)
			break;*/
		UIDRef spreadRef=layoutControlData->GetSpreadRef();////@@@@Added	
		
		InterfacePtr<ISpread> iSpread(/*spreadItem*/spreadRef, UseDefaultIID());
		if (iSpread == nil)
			break;
			
		
		// Get the list of document layers
		InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);

		int32 layerCount = layerList->GetCount();
		
		/*IDocumentLayer* iDocumentLayer = layerList->GetLayer(layerNo);*////**Commeneted
		InterfacePtr<IDocumentLayer> iDocumentLayer(layerList->QueryLayer(layerNo));///***Added

		InterfacePtr<ISpreadLayer> spreadLayer(iSpread->QueryLayer(iDocumentLayer));
		if (spreadLayer == nil)
			break;

		UIDRef parentUIDRef = ::GetUIDRef(spreadLayer);

		if (layoutControlData->GetPage() == kInvalidUID)
			break;
		
		UIDRef pageUIDRef(parentUIDRef.GetDataBase(), layoutControlData->GetPage());
		PMRect boundsInParentCoords = layoutHelper.PageToSpread(pageUIDRef, box);

		// Create a series of path segments to describe the spline.
		PMRect boundingBoxInnerCoords (0.0, 0.0, 100.0, 100.0);
			
		// Create the spline.
		frameUIDRef = layoutHelper.CreateSplineGraphic(parentUIDRef, box, boundingBoxInnerCoords, pathPointList, kFalse /*not a closed path*/);
		if (frameUIDRef == UIDRef::gNull)
		{
			break;
		}

		// Apply some graphic attributes to specify how the path should be drawn,
		SnpGraphicHelper graphicHelper(frameUIDRef);
		graphicHelper.AddStrokeWeight(5.0);
		graphicHelper.AddStrokeRendering(graphicHelper.GetNamedSwatch(PMString("Black"), frameUIDRef.GetDataBase()));
		graphicHelper.AddLineImplementation(kSolidPathStrokerBoss);//kDashedPathStrokerBoss,kSolidPathStrokerBoss,kCannedDotPathStrokerBoss,kCannedDash3x2PathStrokerBoss
		K2Vector<PMReal> dashAndGapValues;
		const PMReal dash0(32.0);
		const PMReal gap0(16.0);
		dashAndGapValues.push_back(dash0);
		dashAndGapValues.push_back(gap0);
		graphicHelper.AddDashedValues(dashAndGapValues);
		graphicHelper.AddJoinType(SnpGraphicHelper::kJTRound);
		graphicHelper.AddLineCap(SnpGraphicHelper::kLCRound);
		graphicHelper.AddLineEndEndImplementation(kBarbedArrowHeadBoss);//kBarbedArrowHeadBoss,kCircleSolidArrowHeadBoss,kTriangleArrowHeadBoss
		graphicHelper.ApplyAttributes();

		bool16 selectFrame = SelectFrame(frameUIDRef);

		//*****************move newly created frame to specified cordinates
		moveCreatedFrame(frameUIDRef,box);

	}while(false);
	
	return ;
}






bool16 SelectFrame(const UIDRef& frameUIDRef)
{
	bool16 result = kFalse;
	do 
	{
		//Select the frame.
		InterfacePtr<ISelectionManager> selectionManager(Utils<ISelectionUtils>()->QueryActiveSelection ());
		if (selectionManager == nil)
		{
			//CA("selectionManager == nil");
			break;
		}

		// Deselect everything.
		selectionManager->DeselectAll(nil); // deselect every active CSB
			
		// Make a layout selection.
		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
		if (!layoutSelectionSuite) {
			break;
		}
		layoutSelectionSuite->/*Select*/SelectPageItems(UIDList(frameUIDRef), 
			Selection::kReplace,  Selection::kDontScrollLayoutSelection);
				
		result = kTrue;
	} while(false);
	return result;
}

void moveCreatedFrame(UIDRef frameUIDRef,PMRect box,bool16 isFromImport)
{
	do
	{
		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{	
			CA("Selection Manager is Null");
			break;
		}

		UIDRef ref;
		UIDList  TempUidList(frameUIDRef.GetDataBase());

        TempUidList.Append(frameUIDRef.GetUID());
		
		//CA("GetUidList");

		InterfacePtr<ILayoutControlData> layoutData1(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutData1 == nil)
			break;

		IDocument* document = layoutData1->GetDocument();
		if (document == nil)
		{
			CA("document is nil");
			break;
		}
		IDataBase* database = ::GetDataBase(document);		
		if(database == nil)
		{
			CA("database == nil");
			break;
		}
		InterfacePtr<ISpreadList>spreadList(document,UseDefaultIID());
		if(spreadList == nil)
		{
			CA("spreadList == nil");
			break;
		}	

		UIDRef firstSreadUIDRef(database, spreadList->GetNthSpreadUID(currentSpread -1)); 
		InterfacePtr<ISpread> iSpread1(firstSreadUIDRef, IID_ISPREAD); 
		if(iSpread1 == nil)
		{
			CA("spread1 == nil");
			break;
			//spreadNumber;
		}
//////////////////////////

		int32 numPages = iSpread1->GetNumPages();

		Transform::CoordinateSpace coordinateSpace_1 = Transform::PasteboardCoordinates() ;
		PMRect pageMargin = iSpread1->GetPagesBounds(coordinateSpace_1);//----CS5-----

		PMReal left3,top3,right3,bottom3;
		left3 = pageMargin.Left();
		top3 = pageMargin.Top();
		right3 = pageMargin.Right();
		bottom3 = pageMargin.Bottom();
		/*PMString Dimentions3;
		Dimentions3.Append(" LEFT3 = ");
		Dimentions3.AppendNumber(left3);
		Dimentions3.Append(", TOP3 = ");
		Dimentions3.AppendNumber(top3);
		Dimentions3.Append(", RIGHT3 = ");
		Dimentions3.AppendNumber(right3);
		Dimentions3.Append(", BOTTOM3 = ");
		Dimentions3.AppendNumber(bottom3);
		CA(Dimentions3);*/

		//int32 pageWidth = ToInt32(right3);
		int32 leftOfPage = ToInt32(left3);
		int32 rightOfPage = ToInt32(right3);
		int32 topOfPage = ToInt32(top3);
		int32 bottomOfPage = ToInt32(bottom3);
		int32 halfOfPageHeight = (bottomOfPage - topOfPage)/2;

		PMRect maxLimitsOftextFrames;
		bool16 status = getMaxLimitsOfBoxes(TempUidList,maxLimitsOftextFrames,halfOfPageHeight);

		PMReal left2,top2,right2,bottom2;
		left2 = maxLimitsOftextFrames.Left();
		top2 = maxLimitsOftextFrames.Top() + halfOfPageHeight;
		right2 = maxLimitsOftextFrames.Right();
		bottom2 = maxLimitsOftextFrames.Bottom() + halfOfPageHeight;
	/*	PMString Dimentions2;
		Dimentions2.Append(" LEFT2 = ");
		Dimentions2.AppendNumber(left2);
		Dimentions2.Append(", TOP2 = ");
		Dimentions2.AppendNumber(top2);
		Dimentions2.Append(", RIGHT2 = ");
		Dimentions2.AppendNumber(right2);
		Dimentions2.Append(", BOTTOM2 = ");
		Dimentions2.AppendNumber(bottom2);
		CA(Dimentions2);*/

		int32 numberOfPagesInCurrentSpread = getNoOfPagesfromCurrentSread();

		int32 pageWidth=0;
		int32 sideOfPage = 0; //0 for left & 1 for right
		if(numberOfPagesInCurrentSpread == 1)
		{
			if(leftOfPage == 0)
			{
				pageWidth = rightOfPage;
				sideOfPage = 1;
			}
			else
			{
				pageWidth = -leftOfPage;
				sideOfPage = 0;
			}
		}
		else if(numberOfPagesInCurrentSpread == 2)
		{
			pageWidth = (rightOfPage - leftOfPage)/2;
		}
		PMString widthOfPage;
		widthOfPage.AppendNumber(pageWidth);
		//CA("widthOfPage = " + widthOfPage);

		int32 CurrentX = ToInt32(left2);
		int32 CurrentY = ToInt32(top2);
					
		PMReal left4,top4,right4,bottom4;
		left4 = box.Left();
		top4 = box.Top();
		right4 = box.Right();
		bottom4 = box.Bottom();
		
		int32 X = ToInt32(left4);
		int32 Y = ToInt32(top4);
				
		if(isFromImport)
		{
			if(CurrentX < 0 && numPages == 1 && sideOfPage == 1)
			{
				X = X + pageWidth;
			}
		}
		PMString temp1("X = ");
		temp1.AppendNumber(X);
		temp1.Append(  " , Y = " );
		temp1.AppendNumber(Y);
		temp1.Append( " , CerrentX = " );
		temp1.AppendNumber(CurrentX);
		temp1.Append( " , CurrentY = " );
		temp1.AppendNumber(CurrentY);
		//CA(temp1);
		getRelativeMoveAmountOfxAndy(X ,Y,numberOfPagesInCurrentSpread,CurrentX,CurrentY,pageWidth,sideOfPage);
		
		PMString XX;
		XX.AppendNumber(X);
		//CA("XX = " + XX);

		PMPoint temp(X,Y);
		//Utils<Facade::ITransformFacade>()->Move(TempUidList,temp,kFalse,ITransform::kTransformItemAndChildren);  ///Commented By Sachin Sharma
		///****Added
		ErrorCode errorCode;
		Transform::CoordinateSpace coordinateSpace = Transform::PasteboardCoordinates() ;
		PBPMPoint referencePoint(PMPoint(0,0));
		errorCode =  Utils<Facade::ITransformFacade>()->TransformItems( TempUidList, coordinateSpace, referencePoint, Transform::TranslateBy(temp.X(),temp.Y()));
		//**Up To Here
		}while(kFalse);

}

bool16 getMaxLimitsOfBoxes(UIDList  TempUidList, PMRect& maxBounds,int32 halfOfPageHeight)
{
	int32 length = TempUidList.Length();
	/*PMString Length = "";
	Length.AppendNumber(length);
	CA("Length : " + Length);*/
	
	UID Frame_uId = TempUidList[0];

	K2Vector<PMRect> boxMarginList;
	boxMarginList.clear();

	for(int32 i = 0 ; i < length ; i++)
	{

		UIDRef textFrameUIDRef=TempUidList.GetRef(i);
		
		PMRect textFrameBounds;
			
		InterfacePtr<IGeometry> textFrameGeometry(textFrameUIDRef, UseDefaultIID());
		if (textFrameGeometry == nil)
		{
			//CA("textFrameGeometry == nil ");
			break;
		}

		//CA("textFrameGeometry");
		textFrameBounds = textFrameGeometry->GetStrokeBoundingBox();
		
		PMReal left,top,right,bottom;
		left = textFrameBounds.Left();
		top = textFrameBounds.Top();
		right = textFrameBounds.Right();
		bottom = textFrameBounds.Bottom();
		/*PMString Dimentions;
		Dimentions.Append(" LEFT = ");
		Dimentions.AppendNumber(left);
		Dimentions.Append(", TOP = ");
		Dimentions.AppendNumber(top);
		Dimentions.Append(", RIGHT = ");
		Dimentions.AppendNumber(right);
		Dimentions.Append(", BOTTOM = ");
		Dimentions.AppendNumber(bottom);
		CA(Dimentions);*/

		InterfacePtr<ITransform> transform(textFrameGeometry, UseDefaultIID());
		if (transform == nil)
		{
			//CA("transform == nil ");
			break;
		}
		//CA("transform ");
		
		//PMRect* rectangle = &textFrameBounds;
		//transform->InverseTransform(&textFrameBounds);
		///transform->Transform(&textFrameBounds);  ///**Commented by Sachin Sharma

		::TransformInnerRectToParent((IPMUnknown *)transform,&textFrameBounds);////***Added
		

		PMReal left1,top1,right1,bottom1;
		left1 = textFrameBounds.Left();
		top1 = textFrameBounds.Top() + halfOfPageHeight;
		right1 = textFrameBounds.Right();
		bottom1 = textFrameBounds.Bottom() + halfOfPageHeight;

		/*PMString Dimentions1;
		Dimentions1.Append(" LEFT1 = ");
		Dimentions1.AppendNumber(left1);
		Dimentions1.Append(", TOP1 = ");
		Dimentions1.AppendNumber(top1);
		Dimentions1.Append(", RIGHT1 = ");
		Dimentions1.AppendNumber(right1);
		Dimentions1.Append(", BOTTOM1 = ");
		Dimentions1.AppendNumber(bottom1);
		CA(Dimentions +"\n" +Dimentions1);*/

		boxMarginList.push_back(textFrameBounds);
	}

	if(boxMarginList.size() == 0)
		return kFalse;

	PMReal minTop=0.0, minLeft=0.0 , maxBottom=0.0 , maxRight=0.0;

	PMReal top = boxMarginList[0].Top();
	PMReal left = boxMarginList[0].Left();
	PMReal bottom = boxMarginList[0].Bottom();
	PMReal right = boxMarginList[0].Right();

	/*PMString ASD("boxMarginList[0].Top() : ");
	ASD.AppendNumber((boxMarginList[0].Top()));
	ASD.Append("  boxMarginList[0].Left() : ");
	ASD.AppendNumber((boxMarginList[0].Left()));
	ASD.Append("  boxMarginList[0].Bottom() : ");
	ASD.AppendNumber((boxMarginList[0].Bottom()));
	ASD.Append("  boxMarginList[0].Right() : ");
	ASD.AppendNumber((boxMarginList[0].Right()));
	CA(ASD);*/
		
	minTop = top;
	minLeft = left;
	maxBottom = bottom;
	maxRight = right;

	for(int i=1;i<boxMarginList.size();i++)
	{
		
		top = boxMarginList[i].Top();
		left = boxMarginList[i].Left();
		bottom = boxMarginList[i].Bottom();
		right = boxMarginList[i].Right();

		/*PMString ASD("boxMarginList[i].Top() : ");
		ASD.AppendNumber((boxMarginList[i].Top()));
		ASD.Append("  boxMarginList[i].Left() : ");
		ASD.AppendNumber((boxMarginList[i].Left()));
		ASD.Append("  boxMarginList[i].Bottom() : ");
		ASD.AppendNumber((boxMarginList[i].Bottom()));
		ASD.Append("  boxMarginList[i].Right() : ");
		ASD.AppendNumber((boxMarginList[i].Right()));
		CA(ASD);*/

		if(top < minTop)
			minTop = top;
		if(left < minLeft)
			minLeft = left;
		if(bottom > maxBottom)
			maxBottom = bottom;
		if(right > maxRight)
			maxRight = right;
	}

	maxBounds.Top(minTop);
	maxBounds.Left(minLeft);
	maxBounds.Bottom(maxBottom);
	maxBounds.Right(maxRight);

	return kTrue;
}

int32 getNoOfPagesfromCurrentSread()
{
	InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
	if (layoutData == nil)
		return 0;
//////////////////////////
	IDocument* document = layoutData->GetDocument();
	if (document == nil)
	{
		CA("document is nil");
		return 0;
	}
	IDataBase* database = ::GetDataBase(document);		
	if(database == nil)
	{
		CA("database == nil");
		return 0;
	}
	InterfacePtr<ISpreadList>spreadList(document,UseDefaultIID());
	if(spreadList == nil)
	{
		CA("spreadList == nil");
		return 0;
	}	

	UIDRef firstSreadUIDRef(database, spreadList->GetNthSpreadUID(currentSpread -1)); 
	InterfacePtr<ISpread> iSpread(firstSreadUIDRef, IID_ISPREAD); 
	if(iSpread == nil)
	{
		CA("spread1 == nil");
		return 0;
	}
//////////////////////////

	int numPages=iSpread->GetNumPages();
	return numPages;
}

void getRelativeMoveAmountOfxAndy(int32 &X,int32 &Y,int32 numberOfPagesInCurrentSpread,int32 CurrentX,int32 CurrentY,int32 pageWidth,int32 sideOfPage)
{
	if(X == 0 && Y == 0 && CurrentX == 0 && CurrentY == 0)
	{
		//CA("dont do anything"); ///old
		if(numberOfPagesInCurrentSpread ==2)
		{
			X = - pageWidth;
		}
	}
	else if(X == 0 && Y == 0 && ((CurrentX != 0 && CurrentX > 0) || (CurrentY != 0 && CurrentY > 0 && CurrentX > 0)) && numberOfPagesInCurrentSpread == 1)
	{
		//CA("1");					//old
		X = X - CurrentX;
		Y = Y - CurrentY;
	}
	else if(X == 0 && Y == 0 && ((CurrentX != 0 && CurrentX > 0) || (CurrentY != 0 && CurrentY > 0 && CurrentX > 0)) && numberOfPagesInCurrentSpread == 2)
	{
		//CA("5");					
		X = -(pageWidth + CurrentX);
		Y = -CurrentY;
	}
	else if(X == 0 && Y == 0 && ((CurrentX != 0 && CurrentX < 0) || (CurrentY != 0 && CurrentY < 0)) && numberOfPagesInCurrentSpread == 2)
	{
		//CA("3");
		X = 0 - (CurrentX + pageWidth);
		Y = - CurrentY;
	}
	else if(X == 0 && Y == 0 && ((CurrentX != 0 && CurrentX < 0) || (CurrentY != 0 && CurrentY < 0)) && numberOfPagesInCurrentSpread == 1)
	{
		//CA(" for left page");
		X = - (pageWidth + CurrentX);
		Y = - CurrentY;
	}
	else if(X != 0 && Y != 0 && ((CurrentX != 0 && CurrentX < 0) || (CurrentY != 0 && CurrentY < 0)))
	{
		//CA("4");
		X = X - (CurrentX + pageWidth);
		Y = Y - CurrentY;
	}
	else if(X == 0 && Y != 0 && numberOfPagesInCurrentSpread == 1)
	{
		//CA("new one for left page == 1");
		if(sideOfPage == 0)
		{
			X = -(pageWidth + CurrentX);			
			Y = Y - CurrentY;
		}
		else
		{
			//CA("right page");
			X = X - CurrentX;			
			Y = Y - CurrentY;
		}

	}
	else if(X != 0 && Y == 0 && numberOfPagesInCurrentSpread == 1)
	{
		//CA("new one for left page ");
		if(sideOfPage == 0)
		{
			//CA("left page");
			X = X - (pageWidth + CurrentX); 
			Y = Y - CurrentY;
		}
		else
		{
			//CA("right page");
			X = X - CurrentX;			
			Y = Y - CurrentY;
		}
	}
	else if(numberOfPagesInCurrentSpread == 1)
	{	
		//CA("numberOfPagesInCurrentSpread == 1");
		X = X - CurrentX  - pageWidth;			//old (Added by Rahul (subtract Page width for Left page)
		Y = Y - CurrentY;
	}
	else if(numberOfPagesInCurrentSpread == 2)
	{
		//CA("numberOfPagesInCurrentSpread == 2");
		X = X - pageWidth - CurrentX;
		Y = Y - CurrentY;
	}
	return;
}

void addLayerIfNeeded()
{
	////CA(__FUNCTION__);
	//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//if(ptrIAppFramework == nil)
	//{
	//	//CA("ptrIAppFramework == nil");	
	//	return;
	//}

	//int32 numberOfStages=ptrIAppFramework->getStageBySectionId(Mediator::sectionID);
	////Default Document has 2 layers so 
	////numberOfStages += 2;
	//numberOfStages += 3;

	//IDocument * theFrontDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();

	//InterfacePtr<ILayerList> layerList(theFrontDoc, IID_ILAYERLIST);

	//int32 layerCount = layerList->GetCount();
	///*PMString lc("layerCount::");
	//lc.AppendNumber(layerCount);
	//lc.Append("____NumberOfStages");
	//lc.AppendNumber(numberOfStages);
	//CA(lc);*/
	//
	//if(numberOfStages == layerCount)
	//{
	//	//CA("numberOfStages == layerCount");		
	//	return;
	//}

	//else if(numberOfStages > layerCount)
	//{
	//	int32 noOfLayersToBeAdded = numberOfStages - layerCount;
	//	
	//	for(int32 i = 0; i <noOfLayersToBeAdded;i++)
	//	{
	//		IDataBase* database = ::GetDataBase(theFrontDoc);
	//												
	//		InterfacePtr<IDocument> document(database, database->GetRootUID(), UseDefaultIID());
	//		if (!document)
	//		{
	//			CA("!document");				
	//			return;
	//		}

	//		InterfacePtr<ILayerList> layerList(theFrontDoc, IID_ILAYERLIST);
	//		if (!layerList)
	//		{
	//			CA("!layerList");			
	//			return;
	//		}
	//				
	//		InterfacePtr<ICommand> newLayerCmd(CmdUtils::CreateCommand(kNewLayerCmdBoss));
	//		InterfacePtr<INewLayerCmdData> newLayerCmdData(newLayerCmd, IID_INEWLAYERCMDDATA);
	//		newLayerCmdData->Set(GetUIDRef(document));
	//		CmdUtils::ProcessCommand(newLayerCmd);
	//	}
	//}
}

bool16 convertBoxToTextBox(UIDRef& boxUIDRef)
{
		InterfacePtr<ICommand> command ( CmdUtils::CreateCommand(kConvertItemToTextCmdBoss));
		if(!command)
			return kFalse;
		command->SetItemList(UIDList(boxUIDRef));
		if(CmdUtils::ProcessCommand(command)!=kSuccess)
			return kFalse;

	return kTrue;

}

void addTagToGraphicFrame(UIDRef& curBox)
{
	//CA(__FUNCTION__);
/*-
	do{
		APpubComment objAPpubComment = global_VectorAPpubCommentValue[objIndex];
		
		InterfacePtr<IPMUnknown> unknown(curBox, IID_IUNKNOWN);
		if(unknown == nil){
			break;
		}
		InterfacePtr<IDocument> doc(curBox.GetDataBase(), curBox.GetDataBase()->GetRootUID(), UseDefaultIID());
		if(doc == nil)
			break;

		InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(doc));
		if (rootElement == nil)			
			break;

		XMLReference parent = rootElement->GetXMLReference();
		
		XMLReference storyXMLRef = TagFrameElement(parent, curBox.GetUID(),objAPpubComment.comment.displayName);
		if(storyXMLRef == kInvalidXMLReference)
		{
			break;
		}
		attachAttributes(&storyXMLRef,kTrue);
	}while(kFalse);
 -*/
}



///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
XMLReference TagFrameElement(const XMLReference& newElementParent,UID frameUID,PMString& frameTagName)
{
	//CA(__FUNCTION__);
	XMLReference resultXMLRef=kInvalidXMLReference;
	do {
		// Acquire the IXLMElementCommands interface on the Utils boss, and use its method
		// to tag the frame.  Arbitrarily ask for the element to be the 0th child of it's parent.
		ErrorCode errCode = Utils<IXMLElementCommands>()->CreateElement(WideString(frameTagName), frameUID, newElementParent, 0, &resultXMLRef);
		// Verify the results: no errors, valid XMLRef returned, we can instantiate it.
		if (errCode != kSuccess)
		{
			//CA("ExpXMLActionComponent::TagFrameElement - CreateElement failed");
			break;
		}		
		if (resultXMLRef == kInvalidXMLReference)
		{
			//CA("ExpXMLActionComponent::TagFrameElement - Can't create new XMLReference");
			break;
		}		
		InterfacePtr<IIDXMLElement> newXMLElement (resultXMLRef.Instantiate());
		if (newXMLElement==nil) 
		{
			//CA("ExpXMLActionComponent::TagFrameElement - Can't instantiate new XML element");
		}
	}while (false);

	return resultXMLRef;
}

void addTagToText(UIDRef& textFrameUIDRef,ITextModel* txtModel,PMString& displyName)
{
	do
	{
		//CA("Inside addTagToText ");
		UIDRef txtMdlUIDRef =::GetUIDRef(txtModel);
		if (txtMdlUIDRef.GetDataBase() == nil) 
			break;
 
		// Get the document's root element
		InterfacePtr<IDocument> doc(txtMdlUIDRef.GetDataBase(), txtMdlUIDRef.GetDataBase()->GetRootUID(), UseDefaultIID());
		InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(doc));

		if (rootElement == nil)
			break;
			
		// Before tagging the text itself, we have to verify the story itself is tagged.
		InterfacePtr<IXMLReferenceData> storyXMLRefData(txtMdlUIDRef, UseDefaultIID());
		
		if (storyXMLRefData == nil)
			break;
		
		XMLReference storyXMLRef = storyXMLRefData->GetReference();

		/// for attaching XMLtag to the selected range of text.. 
		//first we need to tag whole story innsdie the Text box
		// We can add the Text tag below the Frame Story Tag In our case the Story tag name  is 'PRINTsource'
		if (storyXMLRef == kInvalidXMLReference)   
		{							
			XMLReference parent = rootElement->GetXMLReference();
	
			PMString storyTagName("PRINTsource");
			
			//Here first time attachAttributes() function is called
            UID tempUID= txtMdlUIDRef.GetUID();
			storyXMLRef = tagFrameElement(parent, tempUID, storyTagName);
		
			if (storyXMLRef == kInvalidXMLReference)
			{				
				// We should have been able to create this element
				//CA("ExpXMLActionComponent::DoTagSelectedText - can't create XMLRef for story");
				break;
			}
		}	

		XMLReference* newTag = new XMLReference;
		
		int32 start = 0;
		int32 end=/*start+displyName.NumUTF16TextChars()*/ txtModel->TotalLength()-1 ;
		/*PMString ASD("End : ");
		ASD.AppendNumber(end);
		CA(ASD);
		*/
			
		//CA("Addd::"+displyName);
		Utils<IXMLElementCommands>()->CreateElement(WideString(displyName) , txtMdlUIDRef,start, end,kInvalidXMLReference, newTag);
		//CA("After creating Element ");
		attachAttributes(newTag,kTrue);
		//CA("After attach attributes");
		//delete newTag;

	}while(false);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

XMLReference tagFrameElement(const XMLReference& newElementParent,UID& frameUID,const PMString& frameTagName)
{
	XMLReference resultXMLRef=kInvalidXMLReference;
	do {
		// Acquire the IXLMElementCommands interface on the Utils boss, and use its method
		// to tag the frame.  Arbitrarily ask for the element to be the 0th child of it's parent.
		ErrorCode errCode = Utils<IXMLElementCommands>()->CreateElement(WideString(frameTagName), frameUID, newElementParent, 0, &resultXMLRef);
		// Verify the results: no errors, valid XMLRef returned, we can instantiate it.
		if (errCode != kSuccess)
			break;
		
		if (resultXMLRef == kInvalidXMLReference)
			break;
		
		InterfacePtr<IIDXMLElement> newXMLElement (resultXMLRef.Instantiate());
		if (newXMLElement==nil) 
		{
			CA("ExpXMLActionComponent::TagFrameElement - Can't instantiate new XML element");
		}
	}while (false);

	if(frameTagName == "PRINTsource")
	{
		attachAttributes(&resultXMLRef,kFalse);
	}
	return resultXMLRef;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void attachAttributes(XMLReference* newTag,bool16 flag)
{
/*-
	//CA(__FUNCTION__);
	APpubComment objAPpubComment = global_VectorAPpubCommentValue[objIndex];


	PMString attribName("ID");
	PMString attribVal("");
	attribVal.AppendNumber(objAPpubComment.APpubCommentID);
	if(!flag){
		attribVal="-1";
	}

	ErrorCode err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	attribName.Clear();
	attribVal.Clear();
	attribName = "typeId";
	attribVal.AppendNumber(objAPpubComment.comment_type);
	if(!flag){
		attribVal="-1";
	}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	attribName.Clear();
	attribVal.Clear();
	attribName = "index";
	attribVal.AppendNumber(6);
	if(!flag){
		attribVal="-1";
	}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	attribName.Clear();
	attribVal.Clear();
	attribName = "imgFlag";
	attribVal.AppendNumber(-1);
	if(!flag){
		attribVal="-1";
	}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));
	attribName.Clear();
	attribVal.Clear();
	attribName = "parentID";
	attribVal.AppendNumber(objAPpubComment.stage);
	if(!flag){
		attribVal="-1";
	}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	attribName.Clear();
	attribVal.Clear();
	attribName = "parentTypeID";
	attribVal.AppendNumber(-1);
	if(!flag){
		attribVal="-1";
	}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	attribName.Clear();
	attribVal.Clear();
	attribName = "sectionID";
	attribVal.AppendNumber(-1);
	if(!flag){
		attribVal="-1";
	}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	attribName.Clear();
	attribVal.Clear();
	attribName = "tableFlag";
	attribVal.AppendNumber(-1);
	if(!flag){
		attribVal="-1";
	}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	attribName.Clear();
	attribVal.Clear();
	attribName = "LanguageID";	
	attribVal.AppendNumber(1);
	if(!flag){
		attribVal="NULL";
	}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	attribName.Clear();
	attribVal.Clear();
	attribName = "isAutoResize";
	attribVal="0";
	
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4

	attribName.Clear();
	attribVal.Clear();
	attribName = "rowno";
	attribVal.AppendNumber(-1);
	if(flag){
		attribVal="-1";
	}
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
	attribName.Clear();
	attribVal.Clear();
	attribName = "colno";
	attribVal="-1";
	
	err = Utils<IXMLAttributeCommands>()->CreateAttribute(*newTag,WideString(attribName),WideString(attribVal));//Cs4
 -*/
}

bool16 ImportFileInFrame
(const UIDRef& imageBox, const PMString& fromPath)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}
	bool16 fileExists = SDKUtilities::FileExistsForRead(fromPath) == kSuccess;
	if(!fileExists) 
		return kFalse;	
	
	/*IDocument *docPtr=::GetFrontDocument();
	IDataBase* db = ::GetDataBase(docPtr);*/

	//IDocument* doc =::GetFrontDocument();  //Cs3
	IDocument* doc =Utils<ILayoutUIUtils>()->GetFrontDocument();  //cs4
	IDataBase* db = ::GetDataBase(doc); 
	if (db == NULL)
	{
		ptrIAppFramework->LogDebug("AP46_DataSprayerModel::CDataSprayer::ImportFileInFrame::db == NULL");																	
		return kFalse;
	}

	
	IDFile sysFile = SDKUtilities::PMStringToSysFile(const_cast<PMString* >(&fromPath));	
	InterfacePtr<ICommand> importCmd(CmdUtils::CreateCommand(kImportAndLoadPlaceGunCmdBoss));
	if(!importCmd) 
	{
		ptrIAppFramework->LogError("AP46_CreateMediaMaster::CMMDialogOserver::ImportFileInFrame::importCmd == NULL");
		return kFalse;
	}
	//InterfacePtr<IImportFileCmdData> importFileCmdData(importCmd, IID_IIMPORTFILECMDDATA); // no DefaultIID for this
	//if(!importFileCmdData)
	//{
	//	ptrIAppFramework->LogError("AP46_CreateMediaMaster::CMMDialogOserver::ImportFileInFrame::importFileCmdData == NULL");

	//	return kFalse;
	//}

	//importFileCmdData->Set(db, sysFile, kMinimalUI);

	URI tmpURI;
	Utils<IURIUtils>()->IDFileToURI(sysFile, tmpURI);
	InterfacePtr<IImportResourceCmdData> importFileCmdData(importCmd, IID_IIMPORTRESOURCECMDDATA); // no kDefaultIID	
	if (importFileCmdData == nil){
		CA(" importResourceCmdData == nil ");
		return kFalse;
	}	
	importFileCmdData->Set(db,tmpURI,kMinimalUI);


	
	ErrorCode err = CmdUtils::ProcessCommand(importCmd);
	if(err != kSuccess) 
		return kFalse;
	
	InterfacePtr<IPlaceGun> placeGun(db, db->GetRootUID(), UseDefaultIID());
	if(!placeGun)
	{
		ptrIAppFramework->LogError("AP46_CreateMediaMaster::CMMDialogOserver::ImportFileInFrame::!placeGun");
		return kFalse;
	}
	
	UIDRef placedItem(db, placeGun->/*GetItemUID*/GetFirstPlaceGunItemUID());
	
	InterfacePtr<ICommand> replaceCmd(CmdUtils::CreateCommand(kReplaceCmdBoss));
	if (replaceCmd == NULL)
	{
		ErrorUtils::PMSetGlobalErrorCode(kFailure);
		return kFalse;
	}
	
	InterfacePtr<IReplaceCmdData>iRepData(replaceCmd, IID_IREPLACECMDDATA);
	if(!iRepData)
	{
		ASSERT_FAIL("AbortPlaceGun failed");
		ErrorUtils::PMSetGlobalErrorCode(kFailure);
		return kFalse;
	}

	iRepData->Set(db, imageBox.GetUID(), placedItem.GetUID()/*newUID*/, kFalse);
	
	ErrorCode status = CmdUtils::ProcessCommand(replaceCmd);
	if(status==kFailure)
	{
		ptrIAppFramework->LogError("AP46_CreateMediaMaster::CMMDialogOserver::ImportFileInFrame::!status");
		return kFalse;
	}
	return kTrue;
}

void fitImageInBox(const UIDRef& boxUIDRef)
{
	do 
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			return;
		}
		InterfacePtr<ICommand> iAlignCmd(CmdUtils::CreateCommand(/*kFitContentPropCmdBoss*/kFitContentToFrameCmdBoss ));
		if(!iAlignCmd)
		{
			ptrIAppFramework->LogError("AP46_CreateMediaMaster::CMMDialogOserver::fitImageInBox::!iAlignCmd");		
			return;
		}

		InterfacePtr<IHierarchy> iHier(boxUIDRef, IID_IHIERARCHY);  // CS3 change
		if(!iHier)
		{
			ptrIAppFramework->LogError("AP46_CreateMediaMaster::CMMDialogOserver::fitImageInBox::!iHier");				
			return;
		}

		if(iHier->GetChildCount()==0)//There may not be any image at all????
		{
			ptrIAppFramework->LogError("AP46_CreateMediaMaster::CMMDialogOserver::fitImageInBox::There may not be any image");						
			return;
		}
		UID childUID=iHier->GetChildUID(0);

		UIDRef newChildUIDRef(boxUIDRef.GetDataBase(), childUID);
		//UIDList newUIDList1(boxUIDRef);
		//newUIDList1.Append(boxUIDRef.GetUID());

		iAlignCmd->SetItemList(UIDList(newChildUIDRef));
		//iAlignCmd->SetItemList(newUIDList1);

		CmdUtils::ProcessCommand(iAlignCmd);
	} while (false);
}

void AddOrDeleteSpreads(int32 maxSpreadNumber)
{
	InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
	if (layoutData == nil)
	{
		CA("ILayoutControlData is nil");
		return;
	}

	IDocument* document = layoutData->GetDocument();
	if (document == nil)
	{
		CA("document is nil");
		return;
	}

	InterfacePtr<ISpreadList>spreadList(document,UseDefaultIID());
	if(spreadList == nil)
	{
		CA("spreadList == nil");
		return;
	}

	int32 spreadCount = spreadList->GetSpreadCount();

	/*PMString totalSpreads(""); 
	totalSpreads.AppendNumber(spreadCount);
	CA("totalSpreads = " + totalSpreads);*/

	int32 addSpreadsCount = 0;
	if(spreadCount < maxSpreadNumber)
	{
		addSpreadsCount = maxSpreadNumber - spreadCount;
	}
	for(int32 spreadIndex = 0; spreadIndex < spreadCount; spreadIndex++)
	{
		int32 noOfPages = getNoOfPagesfromSpreadNumber(spreadIndex);

		/*PMString noOfPagesStr("");
		noOfPagesStr.AppendNumber(noOfPages);
		CA("noOfPages = " + noOfPagesStr);*/
		
		if(noOfPages == 1)
		{
			for(int32 i = 0; i<=addSpreadsCount;i++)
			{
				//::AddNewPage(); //Cs3
				//::AddNewPage(); //Cs3
				Utils<ILayoutUIUtils>()->AddNewPage(); //Cs4
				Utils<ILayoutUIUtils>()->AddNewPage(); //Cs4
			}
		
			////keepSpreadsTogether
			IDataBase* database = ::GetDataBase(document);
			if(database==nil)
			{
				CA("AP46_CreateMediaMaster::CMMDialogOserver::AddOrDeleteSpread::No database");
				return;
			}
			UIDList spreadUIDList(database);

			for(int32 spreadIndex = 0;spreadIndex < spreadList->GetSpreadCount();spreadIndex++)
			{
				UID spreadUID = spreadList->GetNthSpreadUID(spreadIndex);
				spreadUIDList.Append(spreadUID);
				
				
			}
			InterfacePtr<ICommand> islandCmd(CmdUtils::CreateCommand(kSetIslandSpreadCmdBoss)); 
			if(islandCmd == nil)
			{
				CA("islandCmd == nil");
				return ;
			}
			bool16 bAllIslands  = kFalse;//kFalse for normal spread
					
			InterfacePtr<IBoolData> boolData(islandCmd, UseDefaultIID()); 
			if(boolData == nil)
			{
				CA("boolData == nil");
				return ;
			}
			boolData->Set(!bAllIslands); 
			islandCmd->SetItemList(spreadUIDList); 
			
			ErrorCode status = CmdUtils::ProcessCommand(islandCmd); 

			/////Delete Page

			InterfacePtr<IPageList> iPageList(document,UseDefaultIID());
			if(iPageList == nil)
			{
				CA("iPageList == nil");
				return;
			}
			int32 pageCount = iPageList->GetPageCount();

			int32 indexOfLastPageDeleted = 0;//delete only the first page.

			UIDList toBeDeletedPagesUIDList(database);
			for(int32 pageIndex =0;pageIndex <=indexOfLastPageDeleted;pageIndex++)
			{
				toBeDeletedPagesUIDList.Append(iPageList->GetNthPageUID(pageIndex));
			}
			InterfacePtr<ICommand> iDeletePageCmd(CmdUtils::CreateCommand(kDeletePageCmdBoss));
			if (iDeletePageCmd == nil)
			{
				return;
			}
			
			InterfacePtr<IBoolData> iBoolData(iDeletePageCmd,UseDefaultIID());
			if (iBoolData == nil){
	            
				return;
			}
			iBoolData->Set(kFalse);
			
			iDeletePageCmd->SetItemList(toBeDeletedPagesUIDList);
			// process the command
			ErrorCode status1 = CmdUtils::ProcessCommand(iDeletePageCmd);

		}		
	}

}

int32 getNoOfPagesfromSpreadNumber(int32 spreadIndex)
{
	InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
	if (layoutData == nil)
		return 0;

	IDocument* document = layoutData->GetDocument();
	if (document == nil)
	{
		CA("document is nil");
		return 0;
	}

	InterfacePtr<ISpreadList>spreadList(document,UseDefaultIID());
	if(spreadList == nil)
	{
		CA("spreadList == nil");
		return 0;
	}

	IGeometry* spreadItem = spreadList->QueryNthSpread(spreadIndex);
	if(spreadItem == nil)
		return 0;

	InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
	if (iSpread == nil)
		return 0;

	int numPages=iSpread->GetNumPages();
	return numPages;
}

bool16 copyStencilsFromTheTemplateDocumentIntoScrapData(PMString & templateFilePath,InterfacePtr<IPageItemScrapData> & scrapData ,UIDList &itemList)
 {
	 bool16 result = kFalse;
	do
	{
		SDKLayoutHelper sdklhelp;

		PMString filePathItemsToBeCopiedFrom(templateFilePath);//"c:\\test\\aa.indt");
		IDFile templateIDFile(filePathItemsToBeCopiedFrom);
		UIDRef templateDocUIDRef = sdklhelp.OpenDocument(templateIDFile);
		if(templateDocUIDRef == UIDRef ::gNull)
		{
			CA("Template Document is invalid: " + filePathItemsToBeCopiedFrom);

			return result;;
		}						
					
		ErrorCode err = sdklhelp.OpenLayoutWindow(templateDocUIDRef);
		if(err == kFailure)
		{
			CA("Error occured while opening the layoutwindow of the template");
			return result;;
		}

		InterfacePtr<IDocument> templatedoc(templateDocUIDRef,UseDefaultIID());
		if(templatedoc == nil)
		{
			CA("templatedoc == nil");
			return result;;
		}
		InterfacePtr<ISpreadList>templateSpreadUIDList(templatedoc,UseDefaultIID());
		if(templateSpreadUIDList == nil)
		{
			CA("templateSpreadUIDList == nil");
			return result;;
		}
		IDataBase * templateDocDatabase = templateDocUIDRef.GetDataBase();
		if(templateDocDatabase == nil)
		{
			CA("templateDocDatabase == nil");
			return result;;
		}

		UIDRef templateDocFirstSpreadUIDRef(templateDocDatabase, templateSpreadUIDList->GetNthSpreadUID(0)); 
		InterfacePtr<ISpread> templateSpread(templateDocFirstSpreadUIDRef, IID_ISPREAD); 
		if(templateSpread == nil)
		{
			CA("templateSpread == nil");
			return result;;
		}
		UIDList templateFrameUIDList(templateDocDatabase);
		if(templateSpread->GetNthPageUID(0)== kInvalidUID) 
		{
			CA("pageUID is invalid");
			return result;;
		}		
		
		templateSpread->GetItemsOnPage(0,&templateFrameUIDList,kFalse,kTrue);	

		itemList = templateFrameUIDList;
		
		/*int32 lenOfUIDList = itemList.Length();
		PMString lenStr("");
		lenStr.AppendNumber(lenOfUIDList);
		CA("lenStr = " + lenStr);
		lenOfUIDList = templateFrameUIDList.Length();
		lenStr.Append(" , templateFrameUIDList.Length() =");
		lenStr.AppendNumber(lenOfUIDList);
		CA("lenStr = " + lenStr);*/

		InterfacePtr<ICommand> copyStencilsCMD(CmdUtils::CreateCommand(kCopyCmdBoss));
		if(copyStencilsCMD == nil)
		{
			CA("copyStencilsCMD == nil");
			break;

		}
		InterfacePtr<ICopyCmdData> cmdData(copyStencilsCMD, IID_ICOPYCMDDATA);
		if(cmdData == nil)
		{
			CA("cmdData == nil");
			break;
		}
		// Copy cmd will own this list

		UIDList* listCopy = new UIDList(itemList);
		InterfacePtr<IClipboardController> clipboardController(/*gSession*/GetExecutionContextSession(),UseDefaultIID()); //Cs4
		if(clipboardController == nil)
		{
			CA("clipboardController == nil");
			break;
			
		}
		ErrorCode status = clipboardController->PrepareForCopy();
		if(status == kFailure)
		{
			CA("status == kFailure");
			break;
		}
		InterfacePtr<IDataExchangeHandler> scrapHandler(clipboardController->QueryHandler(kPageItemFlavor));
		if(scrapHandler == nil)
		{
			CA("scrapHandler == nil");
			break;
		}

		clipboardController->SetActiveScrapHandler(scrapHandler);

		InterfacePtr<IPageItemScrapData> localScrapData(scrapHandler, UseDefaultIID());
		if(localScrapData == nil)
		{
			CA("localScrapData == nil");
			break;
		}
		scrapData = localScrapData;
		// specify where to copy item

		UIDRef parent = scrapData->GetRootNode();

		cmdData->Set(copyStencilsCMD, listCopy, parent, scrapHandler);

		if(templateFrameUIDList.Length() == 0)
		{
			//CA("templateFrameUIDList.Length() == 0");
			return kFalse;
		}
		else
		{
			status = CmdUtils::ProcessCommand(copyStencilsCMD);
		}

		if(status == kFailure)
		{
			CA("status == kFailure");
			break;
		}

		err = sdklhelp.CloseDocument(templateDocUIDRef,kFalse,K2::kSuppressUI);
		if(err == kFailure)
		{
			CA("Error occured while closing the document");
			break;
		}
		result = kTrue;

	}while(kFalse);

	
	return result;

 }

//==========================================================================================================
 bool16 pasteTheItemsFromScrapDataOntoOpenDocument(InterfacePtr<IPageItemScrapData> & scrapData,UIDRef &documentDocUIDRef ,UIDRef &layerRef)
 {
	bool16 result = kFalse;
	do
	{
		if (scrapData->IsEmpty()==kFalse)
		{//start if (scrapData->IsEmpty()==kFalse)
			//InterfacePtr<IPageItemScrapData> scrapData(scrapHandler, UseDefaultIID());
			//if(scrapData == nil)
			//{
				//CA("scrapData == nil");
				//break;
			//}
			//This will give the list of items present on the scrap
			UIDList* scrapContents = scrapData->CreateUIDList();
			if (scrapContents->Length() >= 1)
			{//start if (scrapContents->Length() >= 1)
				InterfacePtr<IDocument> dataToBeSprayedDocument(documentDocUIDRef,UseDefaultIID());
				if(dataToBeSprayedDocument == nil)
				{
					CA("dataToBeSprayedDocument == nil");
					break;
				}
				InterfacePtr<ISpreadList>dataToBeSprayedDocumentSpreadList(dataToBeSprayedDocument,UseDefaultIID());
				if(dataToBeSprayedDocumentSpreadList == nil)
				{
					CA("dataToBeSprayedDocumentSpreadList == nil");
					break;
				}
				IDataBase * dataToBeSprayedDocDatabase = documentDocUIDRef.GetDataBase();
				if(dataToBeSprayedDocDatabase == nil)
				{
					CA("dataToBeSprayedDocDatabase == nil");
					break;
				}

				UIDRef dataToBeSprayedDocFirstSpreadUIDRef(dataToBeSprayedDocDatabase, dataToBeSprayedDocumentSpreadList->GetNthSpreadUID(currentSpread-1)); 
				UIDRef spreadUIDRef;
				spreadUIDRef = dataToBeSprayedDocFirstSpreadUIDRef;
				SDKLayoutHelper sdklhelp;
				UIDRef parentLayerUIDRef =sdklhelp.GetSpreadLayerRef(spreadUIDRef);
				if(parentLayerUIDRef.GetUID() == kInvalidUID)
				{
					CA("parentLayerUIDRef.GetUID() == kInvalidUID");
					break;
				}
				InterfacePtr<ICommand> pasteToClipBoardCMD (CmdUtils::CreateCommand(kPasteCmdBoss));
				if(pasteToClipBoardCMD == nil)
				{
					CA("pasteToClipBoardCMD == nil");
					break;
				}
				InterfacePtr<ICopyCmdData> cmdData(pasteToClipBoardCMD, UseDefaultIID());
				if(cmdData == nil)
				{
					CA("cmdData == nil");
					break;
				}
				if(scrapContents == nil)
				{
					CA("scrapContents == nil");
					break;
				}
				if(scrapContents == nil || parentLayerUIDRef == UIDRef::gNull || pasteToClipBoardCMD == nil)
				{	
					break;
				}
				PMPoint offset(-130.0, 0.0);
				cmdData->SetOffset(offset);

				cmdData->Set(pasteToClipBoardCMD, scrapContents, layerRef/*parentLayerUIDRef*/);
				ErrorCode status = CmdUtils::ProcessCommand(pasteToClipBoardCMD); 
				if(status == kSuccess)
				{
					result = kTrue;
				}
			}//end if (scrapContents->Length() >= 1)
		}//end if (scrapData->IsEmpty()==kFalse)
	}while(kFalse);
	return result;
						
 }

/*-
void createRectangle(PMRect box,APpubComment objAPpubComment,UIDRef& frameUIDRef,int32 pageWidth)
{		//CA("case 10 Inside createRectangle ");
		
		SDKUtilities sdkutil;
		PMString tempPath;
		tempPath = folderPath;

	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}
		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutData == nil)
		{
			//CA("ILayoutControlData is nil");
			break;
		}

		IDocument* document = layoutData->GetDocument();
		if (document == nil)
		{
			//CA("document is nil");
			break;
		}
		
		IDataBase* database = ::GetDataBase(document);		
		if(database == nil)
		{
			//CA("database == nil");
			break;
		}

		UIDRef dataToBeSpreadDocumentUIDRef(nil, kInvalidUID);

        UID documentUID = database->GetRootUID();
		if (documentUID == kInvalidUID)
			break;
		dataToBeSpreadDocumentUIDRef = UIDRef(database, documentUID);

	//	addLayerIfNeeded();
			
	//	objAPpubComment.stencilPath;
	//	CA(objAPpubComment.stencilPath);
		
		tempPath = folderPath;
		if(objAPpubComment.comment_type == 10	||	objAPpubComment.comment_type == 13)
		{
			sdkutil.AppendPathSeparator(tempPath);
			tempPath.Append("Product");
		}
		if(objAPpubComment.comment_type == 11 || objAPpubComment.comment_type == 12)
		{
			sdkutil.AppendPathSeparator(tempPath);
			tempPath.Append("Item");
		}
		sdkutil.AppendPathSeparator(tempPath);
		if(objAPpubComment.comment_type == 10	||	objAPpubComment.comment_type == 11)
		tempPath.Append(objAPpubComment.stencilPath);
		else
			tempPath.Append("stencil2.indt");
		//CA("Check it ....");
		//CA(tempPath);

		PMString CopyStencilFromDocument(tempPath);

		UIDList itemList;

		InterfacePtr<IPageItemScrapData>  scrapData;
		bool16 result =  copyStencilsFromTheTemplateDocumentIntoScrapData(CopyStencilFromDocument, scrapData ,itemList);
		if(result == kFalse)
		{
			//CA("result == kFalse");
			//ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaNew::result == kFalse while copying");
			break;
		}

		InterfacePtr<ISpreadList>spreadList(document,UseDefaultIID());
		if(spreadList == nil)
		{
			//CA("spreadList == nil");
			break;
		}	

		UIDRef firstSreadUIDRef(database, spreadList->GetNthSpreadUID( objAPpubComment.spreadNumber -1));

		IControlView* fntView = Utils<ILayoutUIUtils>()->QueryFrontView();
		if (fntView == nil)
		{
			//CA("The front view is nil.");
			break;
		}
		InterfacePtr<IGeometry> spreadGeo(firstSreadUIDRef, UseDefaultIID());
		ASSERT(spreadGeo);
		if (!spreadGeo) {
		//	CA("!spreadGeo");
			break;
		}
		InterfacePtr<ICommand> showSprdCmd(Utils<ILayoutUIUtils>()->MakeScrollToSpreadCmd(fntView, spreadGeo, kTrue));
		CmdUtils::ProcessCommand(showSprdCmd);

		if (CmdUtils::ProcessCommand(showSprdCmd) != kSuccess) {
			//CA("MakeScrollToSpreadCmd failed");
			break;
		}

		InterfacePtr<ISpread> iSpread(firstSreadUIDRef, IID_ISPREAD); 
		if(iSpread == nil)
		{
			CA("spread == nil");
			//spreadNumber;
		}

		// Get the list of document layers
		InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);
		if (layerList == nil)
		{
			CA("layerList is nil");			
			break;
		}

		int32 layerNo = 1;
 
		InterfacePtr<IDocumentLayer> iDocumentLayer(layerList->QueryLayer(layerNo));///Added
		iDocumentLayer->SetVisible(kTrue);

		InterfacePtr<ISpreadLayer> spreadLayer(iSpread->QueryLayer(iDocumentLayer));
		if (spreadLayer == nil)
		{
			//CA("spreadLayer is nil");		
			break;
		}

		UIDRef layerRef = ::GetUIDRef(spreadLayer);
		

										
//Get the item UIDList before Paste 
		UIDList BeforePasteUIDList(database);

		int numPages=iSpread->GetNumPages();

		for(int i=0; i<numPages; i++)
		{
			UIDList tempList(database);
			iSpread->GetItemsOnPage(i, &tempList, kFalse,kTrue);
			BeforePasteUIDList.Append(tempList);
		}
												
		result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapData,dataToBeSpreadDocumentUIDRef,layerRef );
		//CA("After Paste");	
		scrapData->Clear();
		//break;
		//Get the item UIDList After Paste 
		UIDList AfterPasteUIDList(database);

		for(int i=0; i<numPages; i++)
		{
			UIDList tempList(database);
			iSpread->GetItemsOnPage(i, &tempList, kFalse,kTrue);
			AfterPasteUIDList.Append(tempList);
		}

		//Temp.Append("  , AfterPasteUIDList =  ");
		//Temp.AppendNumber(AfterPasteUIDList.Length());
		//CA(Temp);


		UIDList tempUIDList(database); 
		
		for(int32 i = 0 ; i < AfterPasteUIDList.Length() ; i++)
		{
			bool16 isUIDFound = kFalse;
			for(int32 j = 0 ; j < BeforePasteUIDList.Length() ; j++)
			{
				if(AfterPasteUIDList[i] == BeforePasteUIDList[j])
				{
					isUIDFound = kTrue;
					break;
				}					
			}
			if(isUIDFound == kFalse)
				tempUIDList.Append(AfterPasteUIDList[i]);
		}
		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(iSelectionManager == nil)
		{
			//CA("iSelectionManager == nil");
			return;
		}
		
		iSelectionManager->DeselectAll(nil);
		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
		//InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(Utils<ISelectionUtils>()->QuerySuite(IID_ILAYOUTSELECTION_ISUITE ),UseDefaultIID());
		if (!layoutSelectionSuite) 
		{	
			//CA("!layoutSelectionSuite");
			return;				
		}		
		layoutSelectionSuite->SelectPageItems(tempUIDList, Selection::kReplace,  Selection::kAlwaysCenterInView);
		
		//move newly created frame to specified cordinates
		frameUIDRef = tempUIDList.GetRef(0);
		
		//PMRect box1(objAPpubComment.TOP_X_COORDINATE,objAPpubComment.TOP_Y_COORDINATE ,objAPpubComment.BOT_X_COORDINATE, objAPpubComment.BOT_Y_COORDINATE);
		PMRect box1;
		if(MediatorClass::IsPageBased)
		{
			if(pageNumber == 2)
			{
				//PMRect box(objAPpubComment.TOP_X_COORDINATE + pageWidth,objAPpubComment.TOP_Y_COORDINATE + top3 ,objAPpubComment.BOT_X_COORDINATE + pageWidth, objAPpubComment.BOT_Y_COORDINATE + top3);
				box1.Left(objAPpubComment.TOP_X_COORDINATE + pageWidth);
				box1.Top(objAPpubComment.TOP_Y_COORDINATE);
				box1.Right(objAPpubComment.BOT_X_COORDINATE + pageWidth);
				box1.Bottom(objAPpubComment.BOT_Y_COORDINATE);
			}
			else
			{
				//PMRect box(objAPpubComment.TOP_X_COORDINATE,objAPpubComment.TOP_Y_COORDINATE + top3 ,objAPpubComment.BOT_X_COORDINATE, objAPpubComment.BOT_Y_COORDINATE + top3);
				box1.Left(objAPpubComment.TOP_X_COORDINATE);
				box1.Top(objAPpubComment.TOP_Y_COORDINATE);
				box1.Right(objAPpubComment.BOT_X_COORDINATE);
				box1.Bottom(objAPpubComment.BOT_Y_COORDINATE);
			}
		}
		else
		{
			//PMRect box(objAPpubComment.TOP_X_COORDINATE,objAPpubComment.TOP_Y_COORDINATE + top3 ,objAPpubComment.BOT_X_COORDINATE, objAPpubComment.BOT_Y_COORDINATE + top3);
			box1.Left(objAPpubComment.TOP_X_COORDINATE);
			box1.Top(objAPpubComment.TOP_Y_COORDINATE);
			box1.Right(objAPpubComment.BOT_X_COORDINATE);
			box1.Bottom(objAPpubComment.BOT_Y_COORDINATE);
		}


		moveCreatedFrame(frameUIDRef,box1);
//////////////////////////////////
	//do
	//{
	//	
	//	UID pageUID = iSpread->GetNthPageUID(pageNumber - 1);
	//	InterfacePtr<IMasterSpreadList> iMasterSpreadList(document,UseDefaultIID());
	//	if(iMasterSpreadList == nil)
	//	{
	//		//CA("iMasterSpreadList == nil");
	//		break;
	//	}
		
	//	PMString MasterName = objAPpubComment.masterStencilPage;
	//	if(MasterName.NumUTF16TextChars() == 0)
	//	{
	//		//CA("MasterName.NumUTF16TextChars() == 0");
	//		break;
	//	}

	//	UID masterSpreadUID = kInvalidUID;
	//	for(int32 i=0; i<iMasterSpreadList->GetMasterSpreadCount(); i++) 
	//	{ 
	//		UIDRef masterspreadRef(database, iMasterSpreadList->GetNthMasterSpreadUID(i)); 
	//		InterfacePtr<IMasterSpread> masterSpread(masterspreadRef, UseDefaultIID());
	//					
	//		PMString CurrentMasterName("");
	//		masterSpread->GetName(&CurrentMasterName);
	//		//CA(CurrentMasterName);

	//		if(MasterName == CurrentMasterName)
	//		{
	//			masterSpreadUID = iMasterSpreadList->GetNthMasterSpreadUID(i);
	//			break;
	//		}
						
	//	}

	//	if(masterSpreadUID == kInvalidUID)
	//		break;

	//
	//	UIDRef masterSpreadUIDRef(database,masterSpreadUID);

	//	InterfacePtr<ICommand> command(CmdUtils::CreateCommand( kApplyMasterSpreadCmdBoss)); 
	//	if(command == nil)
	//	{
	//		//CA("command == nil");
	//		break;
	//	}

	//	
	//	UIDList pageUIDList(database);
	//	pageUIDList.Insert(pageUID);
	//	command->SetItemList(pageUIDList);
	
	//	InterfacePtr<IApplyMasterCmdData>applyMasterCmd(command,UseDefaultIID());
	//	if(applyMasterCmd == nil)
	//	{
	//		//CA("applyMasterCmd == nil");
	//		break;
	//	}

	//	applyMasterCmd->Set 
	//		( 
	//			masterSpreadUIDRef, 
	//			masterSpreadUIDRef, 
	//			masterSpreadUIDRef 
	//		); 
	//	
	//	ErrorCode status = CmdUtils::ProcessCommand(command); 
	//
	//	if(status == kFailure)
	//	{
	//		//CA("Master can not be applied to pages!!!");
	//		break;
	//	}
	//	result = kTrue;
	//	break;
		

		
	//}while(kFalse);

//////////////////////////////////////////////////

	}while(false);

}
 -*/

PMString prepareTagName(PMString name)
{
	PMString tagName("");
	for(int i=0;i<name.NumUTF16TextChars(); i++)
	{
		if(name.GetChar(i) == '[' || name.GetChar(i) == ']' || name.GetChar(i) == '\n')
			continue;

		if(name.GetChar(i) ==' ')
		{
			tagName.Append("_");
		}
		else tagName.Append(name.GetChar(i));
	}
	PMString FinalTagName("");
	FinalTagName = keepOnlyAlphaNumeric(tagName); // removes the Special characters from the Name.

	return FinalTagName;
}
//Method Purpose: remove the Special character from the Name. 
PMString keepOnlyAlphaNumeric(PMString name)
{
	/*	Illegal filename characters:
		\ (backslash),			--> IsBackSlash
		/ (forward slash)		-->
		: (colon)				--> IsColon
		* (asterisk)			-->
		? (question mark)		-->
		" (double quotes)		-->
		< (left angle bracket)	-->
		> (right angle bracket)	-->
		| (pipe).				-->
		.. (Period)             --> IsPeriod
	*/
	
	PMString tagName("");

	//**** Checking For First Cahracter Is Not an Illegal Character if It is Then Delete It
	PlatformChar chFirst = name.GetChar(0);
	if(chFirst.IsBackSlash() || chFirst.IsColon() || chFirst.IsPeriod() || chFirst == '>' 
			|| chFirst == '<' || chFirst == '?' || chFirst == '|' || chFirst == '"' ||chFirst == '*' 
			|| chFirst == '/' )
		{		
			//CA("going to remove");
			name.Remove(0);	//**** It Removes The First Position	
		}
		
		
	for(int i=0;i<name.NumUTF16TextChars(); i++)
	{
		bool isAlphaNumeric = false ;

		PlatformChar ch = name.GetChar(i);		
		if(ch.IsAlpha() || ch.IsNumber())
			isAlphaNumeric = true ;

		if(ch.IsSpace())
		{
			isAlphaNumeric = true ;
			ch.Set('_') ;
		}
		//****Added By Sachin Sharma	
		if(ch.IsBackSlash() || ch.IsColon() || ch.IsPeriod() || ch == '>' 
			|| ch == '<' || ch == '?' || ch == '|' || ch == '"' ||ch == '*' 
			|| ch == '/' )
		{			
			isAlphaNumeric = true;
			ch.Set('_') ;		
		}

		if(ch == '_' || ch == '-')
			isAlphaNumeric = true ;
	
		if(isAlphaNumeric) 
			tagName.Append(ch);
	}
	return tagName ;
}

bool16 fileExists(PMString& path, PMString& name)
{
	PMString theName("");
	theName.Append(path);
	theName.Append(name);

	const char *file= (theName.GetPlatformString().c_str());
	FILE *fp=NULL;

	fp=std::fopen(file, "r");
	if(fp!=NULL)
	{
		std::fclose(fp);
		return kTrue;
	}
	return kFalse;
}

///////////////////////////////////////////////////////////
void resizeTextFrame(UIDRef itemRef,UIDList list, PMReal bottomX, PMReal bottomY,PMReal halfOfPageHeight,int32 topOfPage)
{
/*	InterfacePtr<ISpecifier> spec(::QueryActiveSpecifier());
	if(!spec)
		return 0;
*/
	//CA("resizeItem");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CA("ptrIAppFramework is nil");	
		return ;
	}

	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
	if(!iSelectionManager)
	{
		ptrIAppFramework->LogDebug("SPSelectionObserver::resizeTextFrame::!iSelectionManager");
		return;
	}
	InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
	( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
	
	 PMRect  maxBounds;
	bool16 res = getMaxLimitsOfBoxes(list,maxBounds,ToInt32(halfOfPageHeight));

	PBPMPoint referencePt(maxBounds.Left(),maxBounds.Top() + topOfPage+halfOfPageHeight);
	PMReal xFactor = bottomX;
	PMReal yFactor = bottomY;

	InterfacePtr<ICommand> cmd(CmdUtils::CreateCommand(kResizeItemsCmdBoss));
	if(!cmd)
	{
		ptrIAppFramework->LogDebug("SPSelectionObserver::resizeTextFrame::!icmd");	
		return ;
	}

	InterfacePtr<IResizeItemsCmdData> data(cmd, IID_IRESIZEITEMSCMDDATA);
	if(!data)
	{	
		ptrIAppFramework->LogDebug("SPSelectionObserver::resizeTextFrame::!data");	
		return ;
	}


	InterfacePtr<IBoundsData> bounds(cmd, IID_IBOUNDSDATA);
	if(!bounds)
	{
		ptrIAppFramework->LogDebug("SPSelectionObserver::resizeTextFrame::!ibounds");	
		return ;
	}

	cmd->SetItemList(UIDList(itemRef));

    Transform::CoordinateSpace coordinateSpace = Transform::PasteboardCoordinates() ;
	Geometry::BoundsKind boundsKind = Geometry::OuterStrokeBounds();

	data->SetResizeData(coordinateSpace,boundsKind,referencePt,Geometry::ResizeTo( bottomX, bottomY));
	ErrorCode result=CmdUtils::ProcessCommand(cmd);

	if(result==kFailure)
	{
	    CA("result==kFailure");
		ptrIAppFramework->LogDebug("AP46_TemplateBuilder::TPLCommonFunctions::resizeItem::result == kFailure");	
		return ;
	}
	
	return ;

}

/*-
void createTableFrame(PMRect box,int32 layerNo,UIDRef& frameUIDRef, int32 spreadNumber ,int32 ObjectID, int32 tableTypeID ,APpubComment objAPpubComment)
{
	UIDRef result = UIDRef::gNull;
	
	do
	{
		addLayerIfNeeded();

		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutData == nil)
		{
			CA("ILayoutControlData is nil");
			break;
		}

		IDocument* document = layoutData->GetDocument();
		if (document == nil)
		{
			CA("document is nil");
			break;
		}
		
		IDataBase* database = ::GetDataBase(document);		
		if(database == nil)
		{
			CA("database == nil");
			break;
		}

		InterfacePtr<ISpreadList>spreadList(document,UseDefaultIID());
		if(spreadList == nil)
		{
			CA("spreadList == nil");
			break;
		}	

		UIDRef firstSreadUIDRef(database, spreadList->GetNthSpreadUID(spreadNumber-1)); 
		InterfacePtr<ISpread> iSpread(firstSreadUIDRef, IID_ISPREAD); 
		if(iSpread == nil)
		{
			CA("spread == nil");
			spreadNumber;
		}

		// Get the list of document layers
		InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);
		if (layerList == nil)
		{
			CA("layerList is nil");			
			break;
		}

		layerNo = 1;

		IDocumentLayer* iDocumentLayer = layerList->QueryLayer(layerNo);
		if (iDocumentLayer == nil)
		{
			CA("iDocumentLayer is nil");				
			break;
		}
		iDocumentLayer->SetVisible(kTrue);

		InterfacePtr<ISpreadLayer> spreadLayer(iSpread->QueryLayer(iDocumentLayer));
		if (spreadLayer == nil)
		{
			CA("spreadLayer is nil");		
			break;
		}

		UIDRef layerRef = ::GetUIDRef(spreadLayer);

		InterfacePtr<IGeometry> pageGeo(layerRef.GetDataBase(), layoutData->GetPage(), UseDefaultIID());
		if (pageGeo == nil )
		{
			//CA("pageGeo is nil");
			break;
		}

		frameUIDRef = Utils<IPathUtils>()->CreateRectangleSpline(layerRef,	box, 
													INewPageItemCmdData::kGraphicFrameAttributes,0x263);

		const UIDList splineItemList(frameUIDRef);

		PMReal strokeWeight(0.0);

		InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->
		CreateStrokeWeightCommand(strokeWeight,&splineItemList,kTrue,kTrue));

		if(strokeSplineCmd==nil)
		{
			CA("strokeSplineCmd is nil");
			break;
		}

		if(CmdUtils::ProcessCommand(strokeSplineCmd)==kFailure)
		{
			CA("command  is failed");		
			break;
		}

		InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QueryActiveSwatchList());
		if(iSwatchList==nil)
		{
			CA("iSwatchList is nil");		
			break;
		}

		//UID blackUID = iSwatchList->GetPaperSwatchUID(); 
		UID blackUID = iSwatchList->GetNoneSwatchUID(); //GetBlackSwatchUID(); 
		InterfacePtr<ICommand> applyStrokeCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeRenderingCommand(blackUID,&splineItemList,kTrue,kTrue));

		if(applyStrokeCmd==nil)
		{
			CA("applyStrokeCmd is nil");		
			break;
		}

		if(CmdUtils::ProcessCommand(applyStrokeCmd)!=kSuccess)
		{
			CA("processCmd is failed");		
			break;
		}

	//	CmdUtils::EndCommandSequence(sequence);

		bool16 selectFrame = SelectFrame(frameUIDRef);

		//move newly created frame to specified cordinates
		moveCreatedFrame(frameUIDRef,box);

		
		InterfacePtr<IPMUnknown> unknown(frameUIDRef, IID_IUNKNOWN);
		if(!unknown)
			break;

		UID textFrameUID=Utils<IFrameUtils>()->GetTextFrameUID(unknown);
		if(textFrameUID==kInvalidUID )
		{
			convertBoxToTextBox(frameUIDRef);			
		}

		textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
		if(textFrameUID == kInvalidUID)
			break;


		InterfacePtr<IHierarchy> graphicFrameHierarchy((frameUIDRef), UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA("graphicFrameHierarchy is NULL");
			
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA("multiColumnItemHierarchy is NULL");
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA("multiColumnItemTextFrame is NULL");
			break;
		}
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA("frameItemHierarchy is NULL");
			break;
		}

		InterfacePtr<ITextFrameColumn>
		frameItemTFC(frameItemHierarchy, UseDefaultIID());
		if (!frameItemTFC) {
			//CA("!!ITextFrameColumn");
			break;
		}

		InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
		if(!textModel)
		{
			//CA("!textModel" );
			break;
		}
////Up To Here
		UIDRef textStoryUIDRef = ::GetUIDRef(textModel);

		int32 start=0;

		///get no of rows & cols
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}
		
		int32 numRows = 0; //Cs4
		int32 numCols = 0; //Cs4
		
		if(objAPpubComment.comment_type == 8)
		{
			VectorScreenTableInfoPtr tableInfo = NULL;
			tableInfo= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(CurrentSelectedSection, ObjectID, kTrue);
			if(tableInfo == NULL)
				break;

			CObjectTableValue oTableValue;
			VectorScreenTableInfoValue::iterator it;

			bool16 typeidFound=kFalse;
			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
			{
				oTableValue = *it;
				if(oTableValue.getTableTypeID() == tableTypeID)
				{   				
					typeidFound=kTrue;				
					break;
				}
			}
		
			if(tableInfo)
				delete tableInfo;

			if(!typeidFound){
				//CA("!typeidFound");
				break;
			}

			numRows =static_cast<int32> (oTableValue.getTableData().size());
			if(numRows<=0)
				break;
			numCols =static_cast<int32> (oTableValue.getTableHeader().size());
			if(numCols<=0)
				break;
		}
		if(objAPpubComment.comment_type == 9)
		{
			VectorScreenItemTableInfoPtr tableInfo=
						ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(ObjectID);
			if(tableInfo == NULL)
				break;

			CItemTableValue oTableValue;
			VectorScreenItemTableInfoValue::iterator it;

			bool16 typeidFound=kFalse;
			for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
			{
				//CA("Inside for");
				oTableValue = *it;
				if(oTableValue.getTableTypeID() == tableTypeID)
				{  // CA("oTableValue.getTableTypeID() == tableTypeID");				
					typeidFound=kTrue;				
					break;
				}
			}

			if(tableInfo)
				delete tableInfo;
		

			if(!typeidFound){
				//CA("!typeidFound");
				break;
			}

			numRows =static_cast<int32> (oTableValue.getTableData().size());
			if(numRows<=0)
				break;
			numCols =static_cast<int32> (oTableValue.getTableHeader().size());
			if(numCols<=0)
				break;
		}


		PMReal colWidth = (box.Right()- box.Left())/numCols;
		//PMReal rowHight = (box.Bottom()- box.Top())/numRows;
		//CA("create Table");
		CreateTable(textStoryUIDRef, start, numRows, numCols,	20,	colWidth);
		
	}while(false);
	//CA("Before return");
	return ;
}
-*/

ErrorCode CreateTable(const UIDRef& storyRef, 
											const TextIndex at,
											const int32 numRows,
											const int32 numCols,
											const PMReal rowHeight,
											const PMReal colWidth,
											const CellType cellType)
{
	ErrorCode status = kFailure;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CA("ptrIAppFramework is nil");		
		return status;
	}
	do {
		InterfacePtr<ITextModel> textModel(storyRef, UseDefaultIID());
		ASSERT(textModel);
		if(textModel == nil) 
		{
			ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogOserver::CreateTable::textModel == nil");		
			break;
		}

		// 1026750: Instead of processing kNewTableCmdBoss use the ITableUtils facade.
		Utils<ITableUtils> tableUtils;
		if (!tableUtils) 
		{
			ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogOserver::CreateTable::!tableUtils");
			break;
		}
		tableUtils->InsertTable (textModel, at, 0,
									 numRows, numCols,
									 rowHeight, colWidth,
									 cellType,
									 ITableUtils::eSetSelectionInFirstCell);
		status = kSuccess;

	} while (false);

	return status;

}

/////////////
bool16 isTablePresent(const UIDRef& boxUIDRef, UIDRef& tableUIDRef, int32 tableNumber)
{
	bool16 returnValue=kFalse;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	return kFalse;
	do
	{
		InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);
		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
		if (textFrameUID == kInvalidUID)
		{
			ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogOserver::isTablePresent::textFrameUID == kInvalidUID");		
			break;
		}

		/*InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		if (textFrame == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogOserver::isTablePresent::textFrame == nil");				
			break;
		}
		
		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
		if (textModel == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogOserver::isTablePresent::textModel == nil");						
			break;
		}*/

		///*****Added From Here
		InterfacePtr<IHierarchy> graphicFrameHierarchy((boxUIDRef/*.GetDataBase()*/), UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA("graphicFrameHierarchy is NULL");
			
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA("multiColumnItemHierarchy is NULL");
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA("multiColumnItemTextFrame is NULL");
			break;
		}
		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA("frameItemHierarchy is NULL");
			break;
		}

		InterfacePtr<ITextFrameColumn>
		frameItemTFC(frameItemHierarchy, UseDefaultIID());
		if (!frameItemTFC) {
			//CA("!!ITextFrameColumn");
			break;
		}

		InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
		if(!textModel)
		{
			//CA("!textModel" );
			break;
		}
////***** Up To Here

		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		if(tableList==nil)
		{
			ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogOserver::isTablePresent::tableList == nil");								
			break;
		}

		int32	tableIndex = tableList->GetModelCount() - 1;
		if(tableIndex<0)
		{
			break;
		}

		tableIndex=tableNumber-1;

		InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
		if(tableModel == nil) 
		{
			ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogOserver::isTablePresent::tableModel == nil");										
			break;
		}
		
		UIDRef tableRef(::GetUIDRef(tableModel));
		
		tableUIDRef=tableRef;

		returnValue=kTrue;
	}
	while(kFalse);

	return returnValue;
}



//Added on 26/12/07 By Dattatray for priceBook
void CMMDialogObserver::populateSubSectionListForPriceBook(bool16 shall_I_Fill_Data_Into_ListBox,double rootId)
{
/*-
		no_of_lstboxElements=0;// declared globally on 03-october
		//CA("1 Inside SPSelectionObserver::populateSubSectionListForPriceBook");
		//noOfRows = 0;	
	do
	{
		SDKListBoxHelper listHelper(this, kCMMPluginID, kCMMMTemplateFileListBoxWidgetID, kCMMDialogWidgetID);
		if(shall_I_Fill_Data_Into_ListBox)
		{
			listHelper.EmptyCurrentListBox();
		}

		
		//yet to define
		MediatorClass :: vector_subSectionSprayerListBoxParameters.clear();
	
	
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
		{
			CA("ptrIAppFramework == nil");
			break;
		}
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(!iConverter)
		{
			CA("iConverter == nil");
			break;
		}
		MediatorClass :: global_project_level = ptrIAppFramework->getPM_Project_Levels();
		SectionData publdata;
		int32 sectionID = -1;
		publdata.clearSectionVector();
		publdata.clearSubsectionVector();
		//define global_vector_pubmodel
		MediatorClass:: global_vector_pubmodel.clear();
		//---------------------------------------------------------------------------------------------------//
		int32 language_id = -1 ;
		//define global_lang_id
		language_id = MediatorClass :: global_lang_id ; //ptrIClientOptions->getDefaultLocale(language_name);
		VectorPubModelPtr vector_pubmodel = NULL;
		
//A		CPubModel objPubModel= ptrIAppFramework->getpubModelByPubID(rootId,language_id);
//A		//CA("Name::"+objPubModel.getName());
//A		listHelper.AddElement(objPubModel.getName(),kPublicationNameTextWidgetID);

		vector_pubmodel = ptrIAppFramework->getAllSubsectionsByPubIdAndLanguageId(rootId , language_id);
		if(vector_pubmodel == nil)
		{
			
			ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::populateSubSectionListForSpread::getAllSubsectionsByPubIdAndLanguageId::vector_pubmodel == nil");
			break;
		}
				
		VectorPubModel::iterator it;
		bool16 isFirstIteration = kTrue;
		subSectionSprayerListBoxParameters tempClass;
		tempClass.isSelected = kTrue;
		tempClass.vec_ssd.clear();
		tempClass.displayName = "";
			
			
//			//when pageOrder is left you have to add two subsection data in a vector.
			
		vector<CPubModel> :: iterator end = vector_pubmodel->end();
		for(it=vector_pubmodel->begin(); it!=end; it++)
		{
				
			CPubModel model = (*it);
			MediatorClass:: global_vector_pubmodel.push_back(model);
				
			PMString subSectionName(iConverter->translateString(model.getName()));
			//CA("subSectionName::"+subSectionName);
			subSectionData ssd;
			ssd.subSectionName = subSectionName;
			ssd.subSectionID = model.getPublicationID();
			ssd.sectionID = model.getEventId();  // For Level 2 fill sectionId same as SubSection id
			//ssd.sectionName = sectionName;						
			ssd.number = model.g();
			tempClass.displayName = subSectionName;
			tempClass.vec_ssd.push_back(ssd);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////			
			if(shall_I_Fill_Data_Into_ListBox)
			{
				listHelper.AddElement(subSectionName,kPublicationNameTextWidgetID);
			}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////// For hiding extra widgets used for manual spray in white board    from here
			vWidgetID.clear();
			if(it!=vector_pubmodel->begin())
			{
				vWidgetID.push_back(kListBoxSelectMasterTemplateRollOverButtonWidgetID);
			}				
			vWidgetID.push_back(kListBoxApplyMasterUnderneathElementsRollOverButtonWidgetID);
			
			vWidgetID.push_back(kListBoxProductStencilFileNameTextForManualWidgetID);
			vWidgetID.push_back(kSeparatorOnGrouPanel1ForManualWidgetID);
			vWidgetID.push_back(kListBoxItemStencilFileNameTextForManualWidgetID);
			vWidgetID.push_back(kListBoxStencilFileNameForSpecSheetTextWidgetID);///16-10-08
			listHelper.HideWidgets(MediatorClass::PublicationTemplateMappingListBox, vWidgetID, no_of_lstboxElements);
//////////////////////////////////////////////////////////////////////////////////////////////     upto here
			no_of_lstboxElements++;									
//A		}
			//PMString sz;
			//sz.AppendNumber(tempClass.vec_ssd.size());
			//CA("tempClass.vec_ssd.::"+sz);
	
			MediatorClass :: vector_subSectionSprayerListBoxParameters.push_back(tempClass);
			tempClass.vec_ssd.clear();
			tempClass.displayName.Clear();
		}
		if(vector_pubmodel)
			delete vector_pubmodel;
		
	}while(kFalse);
-*/
}

//New function for priceBook
void startCreatingMediaForPriceBook()
 {
	// CA("Inside startCreatingMediaForPriceBook");
	 InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	 if(ptrIAppFramework == nil)
		return;
	
	InterfacePtr<IBookManager> bookManager(GetExecutionContextSession(), UseDefaultIID()); //Cs4
	if (bookManager == nil) 
	{ 
		ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaNew::bookManager == nil");
		return; 
	}
	IBook * activeBook = bookManager->GetCurrentActiveBook();
	if(activeBook == nil)
	{
		ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaNew::activeBook == nil");
		return;			
	}
	
	//IDFile file = activeBook->GetBookFileSpec();
	SDKUtilities sdkutils;
	SDKLayoutHelper sdklhelp;
	UIDRef dataToBeSpreadDocumentUIDRef= UIDRef ::gNull;

    SDKFileHelper activeBookFileHelper(activeBook->GetBookFileSpec());
    PMString inddfilepath = activeBookFileHelper.GetPath();
	sdkutils.RemoveLastElement(inddfilepath);
	sdkutils.AppendPathSeparator(inddfilepath);	
//CA("inddfilepath : " + inddfilepath);
	
	K2Vector<IDFile> contentFileList;
	vector<subSectionSprayerListBoxParameters> & subSectionVector = MediatorClass ::vector_subSectionSprayerListBoxParameters;
	int32 subSectionsToBeSrpaydVectorSize =static_cast<int32> (subSectionVector.size()); //Cs4
	
	//PMString AllStencilFilePath,ItemStencilFilePath,ProductStencilFilePath,HybridTableStencilFilePath;
	
	if(subSectionsToBeSrpaydVectorSize > 0)
	{
		for(int32 vectorElementIndex = 0;vectorElementIndex < subSectionsToBeSrpaydVectorSize;vectorElementIndex++)
		{
			subSectionSprayerListBoxParameters & subSectionListBoxParams = subSectionVector[vectorElementIndex];
			if(subSectionListBoxParams.isSelected == kTrue)
			{
//CA("subSectionListBoxParams.isSelected == kTrue");	

				
				int32 subSectionDataVectorSize =static_cast<int32> (subSectionListBoxParams.vec_ssd.size()); //Cs4
//This loop is added for all the section spraying for pricebook

	//A			for(int32 vectorNumber=0;vectorNumber<subSectionDataVectorSize ;vectorNumber++)
	//A			{
				
					//following is the path of master file i.e. templateFilePath
					
					PMString masterFilePath = subSectionListBoxParams.masterFileWithCompletePath;
					PMString productStencilFilePath = subSectionListBoxParams.ProductStencilFilePath;
					PMString itemStencilFilePath = subSectionListBoxParams.ItemStencilFilePath;
					PMString hybridTableStencilFilePath = subSectionListBoxParams.HybridTableStencilFilePath;
	//New added for pricebook
					PMString sectionStencilFilePath = subSectionListBoxParams.SectionStencilFilePath;

					SDKFileHelper productStencilFile(productStencilFilePath);
					SDKFileHelper itemStencilFile(itemStencilFilePath);
					SDKFileHelper hybridTableStencilFile(hybridTableStencilFilePath); /////////	Added by Amit on 4/9/07 for Hybrid Table
					SDKFileHelper sectionStencilFile(sectionStencilFilePath);

					if(masterFilePath == "")
					{
						CAlert::InformationAlert("Select master file.");
						continue;
					}
					if(productStencilFilePath == "" && itemStencilFilePath == "" && hybridTableStencilFilePath == "" && sectionStencilFilePath == "" )
					{
						CAlert::InformationAlert("Select template files to spray.");
						continue;
					}
					
	////////////////if only one File path is selected

					if(itemStencilFilePath != "" && productStencilFilePath == ""  && hybridTableStencilFilePath == "")
					{
	//CAlert::InformationAlert("stencil for item is selected.");
						if(itemStencilFile.IsExisting() == kFalse)
						{
							CAlert::InformationAlert("item template file : " + itemStencilFilePath + " not found.");
							continue;
						}
						subSectionListBoxParams.AllStencilFilePath = itemStencilFilePath;
						subSectionListBoxParams.isSingleStencilFileForProductAndItem = kTrue;
					}
					
					if(productStencilFilePath != "" && itemStencilFilePath == "" && hybridTableStencilFilePath == "")
					{
	//CAlert::InformationAlert("stencil for product is selected.");
						if(productStencilFile.IsExisting() == kFalse)
						{
							CAlert::InformationAlert("product template file : " + productStencilFilePath + " not found.");
							continue;
						}
						subSectionListBoxParams.AllStencilFilePath = productStencilFilePath;
						subSectionListBoxParams.isSingleStencilFileForProductAndItem = kTrue;
					}
					
					if(hybridTableStencilFilePath != "" && productStencilFilePath == ""  && itemStencilFilePath == "")
					{
	//CAlert::InformationAlert("stencil for hybridTable is selected.");
						if(hybridTableStencilFile.IsExisting() == kFalse)
						{
							CAlert::InformationAlert("hybridTable template file : " + hybridTableStencilFilePath + " not found.");
							continue;
						}
						subSectionListBoxParams.AllStencilFilePath = hybridTableStencilFilePath;
						subSectionListBoxParams.isSingleStencilFileForProductAndItem = kTrue;
					}

					if(itemStencilFilePath != "" && productStencilFilePath != ""  && hybridTableStencilFilePath == "")
					{
	//CAlert::InformationAlert("stencil of product is selected for hybridTable");
						if(itemStencilFile.IsExisting() == kFalse)
						{
							CAlert::InformationAlert("item template file : " + itemStencilFilePath + " not found.");
							continue;
						}
						
						if(productStencilFile.IsExisting() == kFalse)
						{
							CAlert::InformationAlert("product template file : " + productStencilFilePath + " not found.");
							continue;
						}
						
						subSectionListBoxParams.HybridTableStencilFilePath =productStencilFilePath;
						subSectionListBoxParams.isSingleStencilFileForProductAndItem = kFalse;
					}
					
					if(itemStencilFilePath == "" && productStencilFilePath != ""  && hybridTableStencilFilePath != "" )
					{
	//CAlert::InformationAlert("stencil of product is selected for item");
						if(hybridTableStencilFile.IsExisting() == kFalse)
						{
							CAlert::InformationAlert("hybridTable template file : " + hybridTableStencilFilePath + " not found.");
							continue;
						}
						
						if(productStencilFile.IsExisting() == kFalse)
						{
							CAlert::InformationAlert("product template file : " + productStencilFilePath + " not found.");
							continue;
						}
						
						subSectionListBoxParams.ItemStencilFilePath = productStencilFilePath;
						subSectionListBoxParams.isSingleStencilFileForProductAndItem = kFalse;
					}

					if(itemStencilFilePath != "" && productStencilFilePath == ""  && hybridTableStencilFilePath != "" )
					{
	//CAlert::InformationAlert("stencil of item is selected for product");
						if(productStencilFile.IsExisting() == kFalse)
						{
							CAlert::InformationAlert("product template file : " + productStencilFilePath + " not found.");
							continue;
						}
						
						if(itemStencilFile.IsExisting() == kFalse)
						{
							CAlert::InformationAlert("item template file : " + itemStencilFilePath + " not found.");
							continue;
						}
						subSectionListBoxParams.ProductStencilFilePath = itemStencilFilePath;
						subSectionListBoxParams.isSingleStencilFileForProductAndItem = kFalse;
					}

	//New Added for priceBook on 3/1/08	
					if(sectionStencilFilePath !="")
						subSectionListBoxParams.SectionStencilFilePath = sectionStencilFilePath;
	//Done
					//UIDRef dataToBeSpreadDocumentUIDRef= UIDRef ::gNull;			
					PMString path(inddfilepath);	
					double tempsectionID = subSectionListBoxParams.vec_ssd[0].sectionID;

					CPubModel pubModel = ptrIAppFramework->getpubModelByPubID(tempsectionID ,MediatorClass :: global_lang_id);
					pubModel = ptrIAppFramework->getpubModelByPubID(pubModel.getRootID() ,MediatorClass :: global_lang_id);
					
					PMString Nov1TempDocSaveAsName = pubModel.getName();//subSectionListBoxParams.vec_ssd[0].sectionID//= subSectionListBoxParams.displayName;
					
					///		Nov1TempDocSaveAsName will give publication name	

					//PMString Nov1TempDocSaveAsName("");
					//Nov1TempDocSaveAsName.AppendNumber(subSectionListBoxParams.vec_ssd[vectorNumber].subSectionID );
	//CA("Nov1TempDocSaveAsName : " + Nov1TempDocSaveAsName);
					
					path.Append(Nov1TempDocSaveAsName);
					path.Append(".indd");				
					bool16 result;
					//SDKLayoutHelper sdklhelp;

					MediatorClass::sectionStencilUIDList.Clear();
					MediatorClass::itemStencilUIDList.Clear();
					MediatorClass::productStencilUIDList.Clear();
					MediatorClass::hybridTableStencilUIDList.Clear();
					MediatorClass ::allStencilUIDList.Clear();


					if(vectorElementIndex > 0)
						CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 

					if(subSectionListBoxParams.isSingleStencilFileForProductAndItem)
					{
	//CA("subSectionListBoxParams.isSingleStencilFileForProductAndItem True");
						SDKFileHelper masterFile(masterFilePath);
						if(masterFilePath == "" || masterFile.IsExisting() == kFalse)
						{
							CAlert::InformationAlert("Master file : " + masterFilePath + " not found.");
							continue;
						}
			
						fillpNodeDataList(subSectionListBoxParams.vec_ssd[0].subSectionID,
							subSectionListBoxParams.vec_ssd[0].sectionID,
							MediatorClass :: currentSelectedPublicationID,
							subSectionListBoxParams.vec_ssd[0].subSectionName);//Nov1TempSubSecID);
						
						if(pNodeDataList.size() <= 0)
						{
							continue;
						}
						InterfacePtr<IPageItemScrapData>  scrapData;
						result =  copyStencilsFromTheTemplateDocumentIntoScrapData(subSectionListBoxParams.AllStencilFilePath, scrapData);
						if(result == kFalse)
						{
							ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaNew::result == kFalse while copying");
							continue;
						}
			
						PMString filePathItemsToBePastedIn(masterFilePath);
						IDFile dataToBeSpreadDocumentIDFile(filePathItemsToBePastedIn);
						dataToBeSpreadDocumentUIDRef = sdklhelp.OpenDocument(dataToBeSpreadDocumentIDFile);
						if(dataToBeSpreadDocumentUIDRef == UIDRef ::gNull)
						{
							CA("uidref of dataToBeSpreadDocumentUIDRef is invalid");
							continue;
						}						
					
						ErrorCode err = sdklhelp.OpenLayoutWindow(dataToBeSpreadDocumentUIDRef);
						if(err == kFailure)
						{
							CA("Error occured while opening the layoutwindow of the dataToBeSprayedDocumentFileLayout");
							continue;
						}		

						result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapData,dataToBeSpreadDocumentUIDRef );

						///////////////////////////////////////////////////////////////////////////////////////////////////					
					//following code is added to collect uidlist from master page for allStencilUIDList-----------from here	
						InterfacePtr<IDocument> dataToBeSprayedDocument1(dataToBeSpreadDocumentUIDRef,UseDefaultIID());
						if(dataToBeSprayedDocument1 == nil)
						{
							//CA("dataToBeSprayedDocument1 == nil");
							break;
						}

						InterfacePtr<ISpreadList>dataToBeSprayedDocumentSpreadList1(dataToBeSprayedDocument1,UseDefaultIID());
						if(dataToBeSprayedDocumentSpreadList1 == nil)
						{
							//CA("dataToBeSprayedDocumentSpreadList1 == nil");
							continue;
						}

						IDataBase * dataToBeSprayedDocumentDatabase1 = dataToBeSpreadDocumentUIDRef.GetDataBase();
						if(dataToBeSprayedDocumentDatabase1 == nil)
						{
							//CA("dataToBeSprayedDocumentDatabase1 == nil");
							continue;
						}

						UIDRef dataToBeSprayedDocumentFirstSpreadUIDRef1(dataToBeSprayedDocumentDatabase1, dataToBeSprayedDocumentSpreadList1->GetNthSpreadUID(0)); 

						InterfacePtr<ISpread> dataToBeSprayedDocumentFirstSpread1(dataToBeSprayedDocumentFirstSpreadUIDRef1, IID_ISPREAD); 
						if(dataToBeSprayedDocumentFirstSpread1 == nil)
						{
							//CA("spread == nil");
							continue;
						}

						UIDList dataToBeSprayedDocumentFrameUIDList1(dataToBeSprayedDocumentDatabase1);
						if(dataToBeSprayedDocumentFirstSpread1->GetNthPageUID(0)== kInvalidUID) 
						{
							//CA("pageUID is invalid");
							continue;
						}		
			
	//In following code we are getting UIDList for all items on master page and storing it in mediator class
	//variable MediatorClass ::allStencilUIDList
						UIDList tempUIDListForALL(dataToBeSprayedDocumentDatabase1);
						dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&tempUIDListForALL,kFalse,kTrue);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						if(subSectionListBoxParams.SectionStencilFilePath != "")
						{
							InterfacePtr<IPageItemScrapData> scrapDataForSection;
							result =  copyStencilsFromTheTemplateDocumentIntoScrapData(subSectionListBoxParams.SectionStencilFilePath , scrapDataForSection);
							if(result == kFalse)
							{
								//CA("result == kFalse while copying");
								continue;
							}
		
							result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapDataForSection,dataToBeSpreadDocumentUIDRef );
							if(result == kFalse)
							{
								//CA("result == kFalse while pasting");
								continue;
							}
							dataToBeSprayedDocumentFrameUIDList1.Clear();

							UIDList tempSectionStencilUIDList(dataToBeSprayedDocumentDatabase1);

							dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&tempSectionStencilUIDList,kFalse,kTrue);
						
							UIDList tempUIDListForSection(dataToBeSprayedDocumentDatabase1);

							for( int32 i = 0 ; i < tempSectionStencilUIDList.Length() ; i++)
							{
								bool16 isUIDFound = kFalse;
								for(int32 j = 0 ; j < tempUIDListForALL.Length() ; j++)
								{
									if(tempSectionStencilUIDList[i] == tempUIDListForALL[j])
									{
										isUIDFound = kTrue;
										break;
									}
								}
								if(isUIDFound == kFalse)
									tempUIDListForSection.Append(tempSectionStencilUIDList[i]);
							}
							MediatorClass::sectionStencilUIDList = tempUIDListForSection;
						}
						
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						MediatorClass ::allStencilUIDList = tempUIDListForALL;					
	//////////////////////////////-------------------------------------------------------------upto here
					}//end if y 
					else
					{
	//CA("else ..");
						SDKFileHelper masterFile(masterFilePath);
						if(masterFilePath == "" || masterFile.IsExisting() == kFalse)
						{
							CA(masterFilePath + " file doesn't exist");
							continue;
						}
			//CA("Before fillpNodeDataList");
						fillpNodeDataList(subSectionListBoxParams.vec_ssd[0].subSectionID,
							subSectionListBoxParams.vec_ssd[0].sectionID,
							MediatorClass :: currentSelectedPublicationID,
							subSectionListBoxParams.vec_ssd[0].subSectionName);//Nov1TempSubSecID);
						
						if(pNodeDataList.size() <= 0)
						{
							//CA("pNodeDataList.size() <= 0");
							continue;
						}
			//CA("After fillpNodeDataList");			
	//CA("subSectionListBoxParams.ssss.ItemStencilFilePath : " +subSectionListBoxParams.ssss.ItemStencilFilePath);
						InterfacePtr<IPageItemScrapData>  scrapData;
						result =  copyStencilsFromTheTemplateDocumentIntoScrapData(subSectionListBoxParams.ItemStencilFilePath, scrapData);
							
						if(result == kFalse)
						{
							ptrIAppFramework->LogDebug("result == kFalse while copying");
							continue;
						}
			
						PMString filePathItemsToBePastedIn(masterFilePath);
						IDFile dataToBeSpreadDocumentIDFile(masterFilePath);
						if(dataToBeSpreadDocumentUIDRef == UIDRef ::gNull )
						{
							dataToBeSpreadDocumentUIDRef = sdklhelp.OpenDocument(dataToBeSpreadDocumentIDFile);
							if(dataToBeSpreadDocumentUIDRef == UIDRef ::gNull)
							{
								CA("uidref of dataToBeSpreadDocumentUIDRef is invalid");
								continue;
							}						
						
							ErrorCode err = sdklhelp.OpenLayoutWindow(dataToBeSpreadDocumentUIDRef);

							if(err == kFailure)
							{
									CA("Error occured while opening the layoutwindow of the dataToBeSprayedDocumentFileLayout");
									continue;
							}			
						}
						
						result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapData,dataToBeSpreadDocumentUIDRef );
						

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	//code to colloect list of uidref on master document for items added by vijay on 14/8/2006
						InterfacePtr<IDocument> dataToBeSprayedDocument1(dataToBeSpreadDocumentUIDRef,UseDefaultIID());
						if(dataToBeSprayedDocument1 == nil)
						{
							//CA("dataToBeSprayedDocument1 == nil");
							break;
						}

						InterfacePtr<ISpreadList>dataToBeSprayedDocumentSpreadList1(dataToBeSprayedDocument1,UseDefaultIID());
						if(dataToBeSprayedDocumentSpreadList1 == nil)
						{
							//CA("dataToBeSprayedDocumentSpreadList1 == nil");
							continue;
						}

						IDataBase * dataToBeSprayedDocumentDatabase1 = dataToBeSpreadDocumentUIDRef.GetDataBase();
						if(dataToBeSprayedDocumentDatabase1 == nil)
						{
							CA("dataToBeSprayedDocumentDatabase1 == nil");
							continue;
						}

						UIDRef dataToBeSprayedDocumentFirstSpreadUIDRef1(dataToBeSprayedDocumentDatabase1, dataToBeSprayedDocumentSpreadList1->GetNthSpreadUID(0)); 

						InterfacePtr<ISpread> dataToBeSprayedDocumentFirstSpread1(dataToBeSprayedDocumentFirstSpreadUIDRef1, IID_ISPREAD); 
						if(dataToBeSprayedDocumentFirstSpread1 == nil)
						{
							CA("spread == nil");
							continue;
						}

						UIDList dataToBeSprayedDocumentFrameUIDList1(dataToBeSprayedDocumentDatabase1);
						if(dataToBeSprayedDocumentFirstSpread1->GetNthPageUID(0)== kInvalidUID) 
						{
							CA("pageUID is invalid");
							continue;
						}

						UIDList tempUIDListForItem(dataToBeSprayedDocumentDatabase1);
						dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&tempUIDListForItem,kFalse,kTrue);
		//////////////////			
						InterfacePtr<IPageItemScrapData>  scrapDataForProduct;

						result =  copyStencilsFromTheTemplateDocumentIntoScrapData(subSectionListBoxParams.ProductStencilFilePath, scrapDataForProduct);
						if(result == kFalse)
						{
							//CA("result == kFalse while copying");
							continue;
						}

						result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapDataForProduct,dataToBeSpreadDocumentUIDRef );

						dataToBeSprayedDocumentFrameUIDList1.Clear();

						UIDList temp2UIDList(dataToBeSprayedDocumentDatabase1);
						dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&temp2UIDList,kFalse,kTrue);

	///////////////////		Added by Amit 

						InterfacePtr<IPageItemScrapData> scrapDataForHybridTable;
	//CA("subSectionListBoxParams.HybridTableStencilFilePath	"+subSectionListBoxParams.HybridTableStencilFilePath);
						result =  copyStencilsFromTheTemplateDocumentIntoScrapData(subSectionListBoxParams.HybridTableStencilFilePath, scrapDataForHybridTable);
						if(result == kFalse)
						{
							//CA("result == kFalse while copying");
							continue;
						}

						result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapDataForHybridTable,dataToBeSpreadDocumentUIDRef );
						if(result == kFalse)
						{
							//CA("result == kFalse while pasting");
							continue;
						}
						dataToBeSprayedDocumentFrameUIDList1.Clear();

						UIDList temp3UIDList(dataToBeSprayedDocumentDatabase1);
						dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&temp3UIDList,kFalse,kTrue);


						if(subSectionListBoxParams.SectionStencilFilePath != "")
						{
							InterfacePtr<IPageItemScrapData> scrapDataForSection;
							result =  copyStencilsFromTheTemplateDocumentIntoScrapData(subSectionListBoxParams.SectionStencilFilePath , scrapDataForSection);
							if(result == kFalse)
							{
								//CA("result == kFalse while copying");
								continue;
							}
		
							result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapDataForSection,dataToBeSpreadDocumentUIDRef );
							if(result == kFalse)
							{
								//CA("result == kFalse while pasting");
								continue;
							}
							dataToBeSprayedDocumentFrameUIDList1.Clear();
						}
						UIDList tempSectionStencilUIDList(dataToBeSprayedDocumentDatabase1);

						dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&tempSectionStencilUIDList,kFalse,kTrue);
	//////////////////////////	 
						UIDList tempUIDListForProduct(dataToBeSprayedDocumentDatabase1); 
						UIDList tempUIDListForHybridTable(dataToBeSprayedDocumentDatabase1); 
						UIDList tempUIDListForSection(dataToBeSprayedDocumentDatabase1);

						for( int32 i = 0 ; i < tempSectionStencilUIDList.Length() ; i++)
						{
							bool16 isUIDFound = kFalse;
							for(int32 j = 0 ; j < temp3UIDList.Length() ; j++)
							{
								if(tempSectionStencilUIDList[i] == temp3UIDList[j])
								{
									isUIDFound = kTrue;
									break;
								}
							}
							if(isUIDFound == kFalse)
								tempUIDListForSection.Append(tempSectionStencilUIDList[i]);
						}


						for( int32 i = 0 ; i < temp3UIDList.Length() ; i++)
						{
							bool16 isUIDFound = kFalse;
							for(int32 j = 0 ; j < temp2UIDList.Length() ; j++)
							{
								if(temp3UIDList[i] == temp2UIDList[j])
								{
									isUIDFound = kTrue;
									break;
								}
								
							}
							if(isUIDFound == kFalse)
								tempUIDListForHybridTable.Append(temp3UIDList[i]);
						}
	///////////////////		End  

	/////////////////////////////////////////////////////////////////////////////////////////////
	//following code is added by vijay choudhari on 14/8/2006 to separate product list from 
	//itemAndProductStencilUIDList.........................................from here

						for( int32 i = 0 ; i < temp2UIDList.Length() ; i++)
						{
							bool16 isUIDFound = kFalse;
							for(int32 j = 0 ; j < tempUIDListForItem.Length() ; j++)
							{
								if(temp2UIDList[i] == tempUIDListForItem[j])
								{
									isUIDFound = kTrue;
									break;
								}
							}
							if(isUIDFound == kFalse)
								tempUIDListForProduct.Append(temp2UIDList[i]);
						}
						MediatorClass ::productStencilUIDList = tempUIDListForProduct;
						MediatorClass ::itemStencilUIDList =  tempUIDListForItem;
						MediatorClass ::hybridTableStencilUIDList =  tempUIDListForHybridTable;
						MediatorClass ::sectionStencilUIDList  = tempUIDListForSection;
					}
					if(result == kFalse)
						continue;

					masterFilePath = path;
					InterfacePtr<IDocument> dataToBeSprayedDocument(dataToBeSpreadDocumentUIDRef,UseDefaultIID());
					if(dataToBeSprayedDocument == nil)
					{
						CA("dataToBeSprayedDocument == nil");
						break;
					}
					InterfacePtr<ISpreadList>dataToBeSprayedDocumentSpreadList(dataToBeSprayedDocument,UseDefaultIID());
					if(dataToBeSprayedDocumentSpreadList == nil)
					{
						CA("dataToBeSprayedDocumentSpreadList == nil");
						continue;
					}
					IDataBase * dataToBeSprayedDocumentDatabase = dataToBeSpreadDocumentUIDRef.GetDataBase();
					if(dataToBeSprayedDocumentDatabase == nil)
					{
						CA("dataToBeSprayedDocumentDatabase == nil");
						continue;
					}

					UIDRef dataToBeSprayedDocumentFirstSpreadUIDRef(dataToBeSprayedDocumentDatabase, dataToBeSprayedDocumentSpreadList->GetNthSpreadUID(0)); 
					InterfacePtr<ISpread> dataToBeSprayedDocumentFirstSpread(dataToBeSprayedDocumentFirstSpreadUIDRef, IID_ISPREAD); 
					if(dataToBeSprayedDocumentFirstSpread == nil)
					{
						CA("spread == nil");
						continue;
					}
					UIDList dataToBeSprayedDocumentFrameUIDList(dataToBeSprayedDocumentDatabase);
					if(dataToBeSprayedDocumentFirstSpread->GetNthPageUID(0)== kInvalidUID)
					{
						CA("pageUID is invalid");
						continue;
					}
					dataToBeSprayedDocumentFirstSpread->GetItemsOnPage(0,&dataToBeSprayedDocumentFrameUIDList,kFalse,kTrue);			
					InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection());
					if(iSelectionManager == nil)
					{
						CA("iSelectionManger == nil");
						continue;
					}
				//global variable value assignments
					//1Nov..Check this later.


					double Nov1TempSecID = subSectionListBoxParams.vec_ssd[0].sectionID;
					double Nov1TempSubSecID = subSectionListBoxParams.vec_ssd[0].subSectionID;
					PMString Nov1TempSubSecName = subSectionListBoxParams.vec_ssd[0].subSectionName;
					currentSelectedSpreadCount = subSectionListBoxParams .spreadCount ; //7-may chetan

					//end 1Nov..Check this later.
					CurrentSelectedSection = Nov1TempSecID;

					CurrentSelectedPublicationID = MediatorClass :: currentSelectedPublicationID;
					CurrentSelectedSubSection = Nov1TempSubSecID;
					global_project_level = MediatorClass :: global_project_level;
					global_lang_id =MediatorClass ::global_lang_id;

					//1Nov..if(MediatorClass ::Is_UseDifferentTemplateForEachSubSectionRadioSelected)
					//1Nov..{
						MediatorClass ::subSecSpraySttngs = subSectionListBoxParams.ssss;
					//1Nov..}
					//CA("before start spraying subsection");

					MediatorClass ::currentProcessingDocUIDRef = dataToBeSpreadDocumentUIDRef;
					SubSectionSprayer SSsp;
					SSsp.selectedSubSection = Nov1TempSubSecName;

					InterfacePtr<ISelectionManager> selectionManager(Utils<ISelectionUtils>()->QueryActiveSelection ());
					if (selectionManager == nil)
					{
						//CA("selectionManager == nil");
						break;
					}

					// Deselect everything.
					selectionManager->DeselectAll(nil); // deselect every active CSB
					// Make a layout selection.
					InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
					//InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(Utils<ISelectionUtils>()->QuerySuite(IID_ILAYOUTSELECTION_ISUITE ),UseDefaultIID());
					if (layoutSelectionSuite == nil) 
					{
						CA("layoutSelectionSuite == nil");
						continue; 
					}
					
					layoutSelectionSuite->SelectPageItems (dataToBeSprayedDocumentFrameUIDList,Selection::kReplace,Selection::kAlwaysCenterInView);
					
					//Use in case of spray by spread where multiple sections are sprayed in single document.
					MediatorClass ::vec_SubSecData = subSectionListBoxParams.vec_ssd;				
					
					bool16 isCancelHit = kFalse;
					SSsp.startSprayingSubSection(isCancelHit);

			
								
					SDKUtilities sdkutil;
					if((MediatorClass::createMediaRadioOption == 5))
					{
			
						IDFile docFile = sdkutil.PMStringToSysFile(&path);
						ErrorCode status = sdklhelp.SaveDocumentAs(dataToBeSpreadDocumentUIDRef,docFile);				
						if(status == kFailure)
						{
							CA("document can not be saved");
							continue;
						}
						sdklhelp.CloseDocument(dataToBeSpreadDocumentUIDRef,kTrue);
						contentFileList.push_back(docFile);
			
					}

					if(isCancelHit == kTrue)
						break;


			//A	}	//priceBook loop
			}	//end if x
		}	//end for z

		//sdklhelp.CloseDocument(dataToBeSpreadDocumentUIDRef,kTrue);
		if(contentFileList.size()>0)
		{
			//CA("addDocumentsToBook........");
			addDocumentsToBook(contentFileList);
		}
	}//end if subSectionsToBeSrpaydVectorSize > 0
 
 }//end function startCreatingMediaNew 

/*-
bool16 ImportIndsFileInDocument(IDocument* frontDocument,  PMString IndsFilePath, PMRect Box, APpubComment& pubCommentObj ,PMReal halfOfPageHeight,int32 topOfPage)
{
	bool16 Importresult = kFalse;
	do
	{		
		//CA("IndsFilePath : " + IndsFilePath);
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			return kFalse;

		SDKFileHelper fileHelper1(IndsFilePath);
		IDFile IndsIDFile = fileHelper1.GetIDFile();

		SDKFileHelper fileHelper(IndsIDFile);
		bool16 fileExists = fileHelper.IsExisting();
		if(!fileExists) {
			//CA("ERROR: file to import does not yet exist; create by ExportSpline");
			break;
		}
			
		InterfacePtr<ISpreadList> spreadList(frontDocument, UseDefaultIID());
		if(!spreadList) {//CA("ERROR:ImportIndsFileInDocument:!spreadList");
			break;
		}

		IDataBase* database = ::GetDataBase(frontDocument);
		UIDRef spreadUIDRef(database, spreadList->GetNthSpreadUID(pubCommentObj.spreadNumber-1));
				
		InterfacePtr<IPMStream> stream(static_cast<IPMStream*>(
		StreamUtil::CreateFileStreamRead(IndsIDFile, kOpenIn)));
		if(!stream) { //CA("ERROR:ImportIndsFileInDocument:!stream");
			break;
		}

		InterfacePtr<IDOMElement> xmlFragment(spreadUIDRef, UseDefaultIID());//// CS3 Change
		ASSERT(xmlFragment);
		if(!xmlFragment) { //CA("ERROR:ImportIndsFileInDocument:!xmlFragment");
			break;
		}

		UIDList UIDListBeforeImport(database);
		
		InterfacePtr<ISpread> spread(spreadUIDRef, IID_ISPREAD); 
		if(spread == nil)
		{
			//CA("spread == nil");
			break;;
		}

		int32 noOFPages = spread->GetNumPages();
		for(int32 p=0; p < noOFPages; p++)
		{
			UIDList UIDListPerPage(database);
			spread->GetItemsOnPage(p,&UIDListPerPage,kFalse,kTrue);	
			UIDListBeforeImport.Append(UIDListPerPage);
		}
		ErrorCode result = Utils<ISnippetImport>()->ImportFromStream(stream, xmlFragment);
		if(result == kFailure)
		{
			//CA("ERROR:ImportIndsFileInDocument: result == kFailure");
		}

		UIDList UIDListAfterImport(database);
		for(int32 p=0; p < noOFPages; p++)
		{
			UIDList UIDListPerPage(database);
			spread->GetItemsOnPage(p,&UIDListPerPage,kFalse,kTrue);	
			UIDListAfterImport.Append(UIDListPerPage);
		}
		
		UIDList ImportedFrameUIDList(database);
		for( int32 i = 0 ; i < UIDListAfterImport.Length() ; i++)
		{
			bool16 isUIDFound = kFalse;
			for(int32 j = 0 ; j < UIDListBeforeImport.Length() ; j++)
			{
				if(UIDListAfterImport[i] == UIDListBeforeImport[j])
				{
					isUIDFound = kTrue;
					break;
				}
				
			}
			if(isUIDFound == kFalse)
				ImportedFrameUIDList.Append(UIDListAfterImport[i]);
		}

		if(ImportedFrameUIDList.Length() == 0)
			break;

		Importresult = kTrue; // Import Successful

		PBPMPoint MoveToPoint( Box.Left(), Box.Top() );
		moveCreatedFrame(ImportedFrameUIDList.GetRef(0), Box, kTrue);
		//CA("8");	
		resizeTextFrame(ImportedFrameUIDList.GetRef(0),ImportedFrameUIDList,Box.Right() - Box.Left() , Box.Bottom() - Box.Top(), halfOfPageHeight,topOfPage);
		//CA("9");	
		InterfacePtr<ITagReader> itagReader
			((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader){
			break;
		}

		// Change sectionId, AutoResize = PubcommentID & rowno = PbObjectId for imported tags
		IIDXMLElement * xmlPtr = nil;
		TagList tagList = itagReader->getTagsFromBox(ImportedFrameUIDList.GetRef(0), &xmlPtr);
		if(tagList.size() > 0)
		{
			for(int32 q=0; q < tagList.size(); q++ )
			{
				PMString TagString =  tagList[q].tagPtr->GetTagString();
				//CA("TagString = " + TagString );
				PMString tagAttribute("");
				PMString tagAttributeValue("");				

				int elementCount1=tagList[q].tagPtr->GetChildCount();
					// these are Product Copy Level tags					
					tagAttribute.Clear();
					tagAttributeValue.Clear();
					tagAttribute.Append("isAutoResize");
					tagAttributeValue.AppendNumber(pubCommentObj.APpubCommentID);
					tagList[q].tagPtr->SetAttributeValue(WideString(tagAttribute) ,WideString(tagAttributeValue));//Cs4												
					
					tagAttribute.Clear();
					tagAttributeValue.Clear();						
					tagAttribute.Append("sectionID");
					tagAttributeValue.AppendNumber(pubCommentObj.SECTION_NO);
					
					tagList[q].tagPtr->SetAttributeValue(WideString(tagAttribute) ,WideString(tagAttributeValue));	//Cs4

					CPbObjectValue* pbObjectValueObj = ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(pubCommentObj.SECTION_NO, tagList[q].parentId );

					if((tagList[q].whichTab == 4) && ((tagList[q].elementId == -101) && (tagList[q].isTablePresent == kFalse) ) || (tagList[q].isTablePresent == kTrue))
					{
						//CA("Custom Tabbed Text OR Tabbed Text table Found");
						
						if(pbObjectValueObj != NULL)
						{
							tagAttribute.Clear();
							tagAttributeValue.Clear();
							
							tagAttribute.Append("rowno");
							tagAttributeValue.AppendNumber(pbObjectValueObj->getPub_object_id());
							tagList[q].tagPtr->SetAttributeValue(WideString(tagAttribute) ,WideString(tagAttributeValue));//Cs4
						}							

						if((tagList[q].isTablePresent == kTrue) )
						{
							do
							{
								XMLReference parentXMLRef = tagList[q].tagPtr->GetParent();
								//IIDXMLElement * parentXMLElement = parentXMLRef.Instantiate();
								InterfacePtr<IIDXMLElement>parentXMLElement(parentXMLRef.Instantiate());
								if(parentXMLElement == NULL)
									break;
								int32 attribCount=parentXMLElement->GetAttributeCount();
								if(attribCount > 0)
								{
									PMString value = parentXMLElement->GetAttributeValue(WideString("parentID"));//Cs4
									PMString value1 = parentXMLElement->GetAttributeValue(WideString("index"));//Cs4

									if((value == "-1") && (value1 == "-1"))
										break;
									
									tagAttribute.Clear();
									tagAttributeValue.Clear();
									
									tagAttribute.Append("rowno");
									tagAttributeValue.AppendNumber(pbObjectValueObj->getPub_object_id());
									parentXMLElement->SetAttributeValue(WideString(tagAttribute) ,WideString(tagAttributeValue));	//Cs4
								}				
							}while(0);
						}
					}

					
					if(elementCount1 > 0)
					{  // We got more tags under 2nd level tags these can be custom tabbed text(Attribute taags) or table(PSCell tags) 
						for(int k=0; k<elementCount1; k++)
						{
							XMLReference elementXMLref1=tagList[q].tagPtr->GetNthChild(k);
							//IIDXMLElement * childElement2=elementXMLref1.Instantiate();
							InterfacePtr<IIDXMLElement>childElement2(elementXMLref1.Instantiate());
							if(childElement2==nil)
							{	               
								continue;
							}
							else
							{
								int elementCount2=childElement2->GetChildCount();
								//if(elementCount2 == 0)
								{// Attributes of Custom Tabbed text tags
									PMString tagName=childElement2->GetTagString();
									//CA("tagName : " + tagName);		
									TagStruct tInfo;
									int32 attribCount=childElement2->GetAttributeCount();
									if(attribCount > 0)
									{
										tagAttribute.Clear();
										tagAttributeValue.Clear();
										tagAttribute.Append("isAutoResize");
										tagAttributeValue.AppendNumber(pubCommentObj.APpubCommentID);
										childElement2->SetAttributeValue(WideString(tagAttribute) ,WideString(tagAttributeValue));	//Cs4
										
										tagAttribute.Clear();
										tagAttributeValue.Clear();						
										tagAttribute.Append("sectionID");
										tagAttributeValue.AppendNumber(pubCommentObj.SECTION_NO);
										childElement2->SetAttributeValue(WideString(tagAttribute) ,WideString(tagAttributeValue));	//Cs4
																	
									}
								}
								if(elementCount2 > 0)
								{	// this can be PSCell tag and we are going for Tabel Cell Attributes
									for(int m=0; m<elementCount2; m++)
									{
										XMLReference elementXMLref1=childElement2->GetNthChild(m);
										//IIDXMLElement * childElement3=elementXMLref1.Instantiate();
										InterfacePtr<IIDXMLElement>childElement3(elementXMLref1.Instantiate());
										if(childElement3==nil)
										{	               
											continue;
										}
										else
										{
											int elementCount3=childElement3->GetChildCount();
											if(elementCount3 == 0)
											{  // getting Table cell Attribute tags here 
												PMString tagName=childElement3->GetTagString();
												//CA("tagName : " + tagName);		
												TagStruct tInfo;
												int32 attribCount=childElement3->GetAttributeCount();
												if(attribCount > 0)
												{
													tagAttribute.Clear();
													tagAttributeValue.Clear();
													tagAttribute.Append("isAutoResize");
													tagAttributeValue.AppendNumber(pubCommentObj.APpubCommentID);
													childElement3->SetAttributeValue(WideString(tagAttribute) ,WideString(tagAttributeValue));	//Cs4
													
													tagAttribute.Clear();
													tagAttributeValue.Clear();						
													tagAttribute.Append("sectionID");
													tagAttributeValue.AppendNumber(pubCommentObj.SECTION_NO);
													childElement3->SetAttributeValue(WideString(tagAttribute) ,WideString(tagAttributeValue));	//Cs4
																										
												}
											}
										}

									}
								}

							}
						}
					}

				///????///////
			}

		}
		//Added
		for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
		{
			tagList[tagIndex].tagPtr->Release();
		}
		//
		
		// Change Master Page according to pubcomment
		do
		{			
			UID pageUID = spread->GetNthPageUID(pageNumber - 1);
			if( pageUID == kInvalidUID)
				break;

			InterfacePtr<IMasterSpreadList> iMasterSpreadList(frontDocument,UseDefaultIID());
			if(iMasterSpreadList == nil)
			{				
				break;
			}
			PMString MasterName = pubCommentObj.masterStencilPage;
			if(MasterName.NumUTF16TextChars() == 0)
			{				
				break;
			}

			UID masterSpreadUID = kInvalidUID;
			for(int32 i=0; i<iMasterSpreadList->GetMasterSpreadCount(); i++) 
			{ 
				UIDRef masterspreadRef(database, iMasterSpreadList->GetNthMasterSpreadUID(i)); 
				InterfacePtr<IMasterSpread> masterSpread(masterspreadRef, UseDefaultIID());
							
				PMString CurrentMasterName("");
				masterSpread->GetName(&CurrentMasterName);
				//CA(CurrentMasterName);
				if(MasterName == CurrentMasterName)
				{
					masterSpreadUID = iMasterSpreadList->GetNthMasterSpreadUID(i);
					break;
				}
			}

			if(masterSpreadUID == kInvalidUID)
				break;
		
			UIDRef masterSpreadUIDRef(database,masterSpreadUID);
			InterfacePtr<ICommand> command(CmdUtils::CreateCommand( kApplyMasterSpreadCmdBoss)); 
			if(command == nil)
			{				
				break;
			}
			
			UIDList pageUIDList(database);
			
			pageUIDList.Append(pageUID);
			command->SetItemList(pageUIDList);

			
			InterfacePtr<IApplyMasterCmdData>applyMasterCmd(command,UseDefaultIID());
			if(applyMasterCmd == nil)
			{				
				break;
			}

			applyMasterCmd->SetApplyMasterCmdData(masterSpreadUIDRef , IApplyMasterCmdData::kKeepCurrentPageSize);//-now use in CS5-
			
			ErrorCode status = CmdUtils::ProcessCommand(command); 
		
			if(status == kFailure)
			{				
				break;
			}
			Importresult = kTrue;
			break;

		}while(kFalse);

		// Refresh Imorted frame
		InterfacePtr<IRefreshContent> RefreshContentPtr((IRefreshContent*)::CreateObject(kRefreshContentBoss, IID_IREFRESHCONTENT));
		if(RefreshContentPtr == NULL)
		{
			//CA("RefreshContentPtr == NULL");
			break;
		}
		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==NULL)
		{			
			break;
		}				
	
		PMString imagePath=ptrIClientOptions->getImageDownloadPath();
		if(imagePath!="")
		{
			const char *imageP=(imagePath.GrabCString());
			if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
				#ifdef MACINTOSH
					imagePath+="/";
				#else
					imagePath+="\\";
				#endif
		}
		if(imagePath=="")
		{			
			break;
		}
		//CA("Beore Refresh Box");
		RefreshContentPtr->refreshThisBox(ImportedFrameUIDList.GetRef(0), imagePath );
		//CA("After Refresh Box");
	//////////////////////////////////////////////////
	}while(0);
		
	return Importresult;
}
-*/



void CMMDialogObserver::refreshWhiteBoardMedia()		
 {
/*-
//	CA("refreshWhiteBoardMedia");
	SDKLayoutHelper sdklhelp;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CA("ptrIAppFramework == NULL");
		return ;
	}
	InterfacePtr<ITagReader> itagReader ((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader){
		ptrIAppFramework->LogDebug("AP46_RefreshContent::Refresh::getDocumentSelectedBoxIds::!itagReader");
		return ;
	}
	InterfacePtr<IBookManager> bookManager(GetExecutionContextSession(), UseDefaultIID()); //Cs5
	if (bookManager == nil) 
	{
		ptrIAppFramework->LogDebug("AP46_CatalogIndex::CMMDialogObserver::ApplyDialogFields::There is not book manager!");
		return;
	}
	IBook* book = bookManager->GetCurrentActiveBook();
	InterfacePtr<IBookContentMgr> bookContentMgr(book, UseDefaultIID());
	if (bookContentMgr == nil) 
	{
		ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogObserver::refreshWhiteBoardMedia::This book doesn't have a book content manager!  Something is wrong.");
		return;
	}
	int32 documentCount = bookContentMgr->GetContentCount();

//PMString d("documentCount	:	");
//d.AppendNumber(documentCount);
//CA(d);
	AcquireWaitCursor awc;
	awc.Animate(); 

	for(int32 docCnt = 0 ; docCnt < documentCount ; docCnt++)
	{
		int32 sectionID = -1;
		IDataBase* bookDB = ::GetDataBase(bookContentMgr);
		
		UID contentUID = bookContentMgr->GetNthContent(docCnt);
		if (contentUID == kInvalidUID) 
			continue; 

		IDFile CurrFile;
		bool16 IsMissingPluginFlag = kFalse;

		IDocument* CurrDoc = Utils<IBookUtils>()->FindDocFromContentUID
				(
					bookDB,
					contentUID,
					CurrFile,
					IsMissingPluginFlag
				);

		IDFile flName = CurrFile;
		UIDRef CurrDocRef = sdklhelp.OpenDocument(flName);
		ErrorCode err = sdklhelp.OpenLayoutWindow(CurrDocRef);
		if(err	== kFailure)
		{
			ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogObserver::refreshWhiteBoardMedia::Document is unable to open");
		}
//	
		//IDocument* fntDoc = ::GetFrontDocument();//Cs3
		IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument(); //CS4
		if(fntDoc==nil)
		{
			ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogObserver::refreshWhiteBoardMedia::fntDoc==nil");	
			return;
		}
		//
		//InterfacePtr<ILayoutControlData> layout(::QueryFrontLayoutData());//Cs3
		InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); //Cs4
		if (layout == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogObserver::refreshWhiteBoardMedia::layout == nil");		
			return;
		}
		IDataBase* database = ::GetDataBase(fntDoc);
		if(database==nil)
		{
			ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogObserver::refreshWhiteBoardMedia::database==nil");			
			return;
		}
		InterfacePtr<ISpreadList> spreadList((IPMUnknown*)fntDoc,UseDefaultIID());
		if (spreadList==nil)
		{
			ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogObserver::refreshWhiteBoardMedia::spreadList==nil");				
			return;
		}
		UIDList allPageItems(database);

//PMString spcount("Spread Count	:	");
//spcount.AppendNumber(spreadList->GetSpreadCount());
//CA(spcount);
		
		int32 spreadOnDocument = spreadList->GetSpreadCount();
		UID pageUID; 
		
		for(int32 spreadNo = 0 ; spreadNo < spreadOnDocument ; ++spreadNo)
		{
			UIDRef spreadUIDRef(database, spreadList->GetNthSpreadUID(spreadNo));
			InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
			if(!spread)
			{
				ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogObserver::refreshWhiteBoardMedia::!spread");						
				return;
			}
			
			int numPages=spread->GetNumPages();
			
			pageUID = spread->GetNthPageUID(numPages-1);		
								
//PMString pgcount("Page Count	:	");
//pgcount.AppendNumber(numPages);
//CA(pgcount);		
			for(int pageNo = 0; pageNo < numPages; pageNo++)
			{
				UIDList tempList(database);
				spread->GetItemsOnPage(pageNo, &tempList, kFalse,kTrue);
				allPageItems.Append(tempList);
			}
		}

		for(int i=0; i<allPageItems.Length(); i++)
		{
			TagList tList = itagReader->getTagsFromBox(allPageItems.GetRef(i));
			if(tList.size() > 0)
			{
				InterfacePtr<IPMUnknown> unknown(allPageItems.GetRef(i), IID_IUNKNOWN);
				if(!unknown)
				{
					CA("No unknown prt available");
					break;
				}

				sectionID =  tList[0].sectionID;
				
				//////
				//check here whether publication is pageBased or not 
				//if it is pageBased then call refreshWhiteBoardMediaForPageBased function....
				CPubModel CPubModelObj = ptrIAppFramework->getpubModelByPubID(sectionID,tList[0].languageID);

				if(CPubModelObj.getIspage_based())
				{
					MediatorClass::IsPageBased = kTrue;
					refreshWhiteBoardMediaForPageBased();
					return;
				}
				MediatorClass::IsPageBased = kFalse;
				///////

				IIDXMLElement * xmlElementPtr = tList[0].tagPtr;				
				UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
				if (textFrameUID == kInvalidUID)
				{
					XMLReference xmlRef = xmlElementPtr->GetXMLReference();
					Utils<IXMLElementCommands>()->DeleteElementAndContent(xmlRef,kTrue);
				}
				else
				{
					XMLReference xmlRef = xmlElementPtr->GetParent();
					Utils<IXMLElementCommands>()->DeleteElementAndContent(xmlRef,kTrue);
				}
			}

			//Added
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
			//
		}

/////////	Following code is for Delete UnUsed Tags	

		InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(fntDoc));
		if (rootElement == nil)
			break;
		XMLReference rootXMLReference = rootElement->GetXMLReference();
		Utils<IXMLElementCommands>()->DeleteElementAndContent(rootXMLReference,kTrue);

//////////		End	Deleting


//////////	Following code is for creating comments on layer	

		VectorAPpubCommentValuePtr vector_pubCommentModel = NULL;
		vector_pubCommentModel = ptrIAppFramework->getPubCommentsBySectionIdStageId(sectionID, 0);

		if(vector_pubCommentModel == nil)
		{
			ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::refreshWhiteBoardMedia::getPubCommentsBySectionId::vector_pubCommentModel == nil");
			continue;
		}
		
		vector<APpubComment> apPubCommentBySection = *vector_pubCommentModel;

		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			CAlert::ErrorAlert("Interface for IClientOptions not found.");
			break;
		}
		PMString language_name("");
	
		//define global_lang_id
		MediatorClass ::global_lang_id = ptrIClientOptions->getDefaultLocale(language_name);
	
		CPubModel pubModel = ptrIAppFramework->getpubModelByPubID(sectionID ,MediatorClass :: global_lang_id);
		
		int32 currentSelectedSpreadCount = pubModel.getSpreadCount();
		
		folderPath.Clear();
		folderPath = ptrIAppFramework->getCatalogPlanningTemplateFolderPath ();

		
		AddOrDeleteSpreads(currentSelectedSpreadCount);
		
		for(currentSpread =1; currentSpread <= currentSelectedSpreadCount; currentSpread++)
		{
			int32 sizeOfCommentsBySection =static_cast<int32> (apPubCommentBySection.size());//Cs4
			global_VectorAPpubCommentValue.clear ();
			for(int32 j=0; j<sizeOfCommentsBySection; j++)
			{
				if(apPubCommentBySection[j].spreadNumber == currentSpread )
				{
					global_VectorAPpubCommentValue.push_back(apPubCommentBySection[j]);
				}
			}
			createCommentsOnLayer();
		}
		
//////////////////		End Commenting

		sdklhelp.SaveDocumentAs(CurrDocRef,flName);
		sdklhelp.CloseDocument(CurrDocRef, kTrue);
	}
-*/
}

void CMMDialogObserver::refreshWhiteBoardMediaForPageBased()		
 {
/*-
	//CA("refreshWhiteBoardMediaForPageBased");
	SDKLayoutHelper sdklhelp;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CA("ptrIAppFramework == NULL");
		return ;
	}
	ptrIAppFramework->PROJECTCACHE_clearInstance();

	InterfacePtr<ITagReader> itagReader ((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader){
		ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogObserver::refreshWhiteBoardMediaForPageBased::!itagReader");
		return ;
	}
	InterfacePtr<IBookManager> bookManager(GetExecutionContextSession(), UseDefaultIID()); //Cs4
	if (bookManager == nil) 
	{
		ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogObserver::refreshWhiteBoardMediaForPageBased::There is not book manager!");
		return;
	}
	IBook* book = bookManager->GetCurrentActiveBook();
	InterfacePtr<IBookContentMgr> bookContentMgr(book, UseDefaultIID());
	if (bookContentMgr == nil) 
	{
		ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogObserver::refreshWhiteBoardMedia::This book doesn't have a book content manager!  Something is wrong.");
		return;
	}
	int32 documentCount = bookContentMgr->GetContentCount();

//PMString d("documentCount	:	");
//d.AppendNumber(documentCount);
//CA(d);
	AcquireWaitCursor awc;
	awc.Animate(); 

	int32 BookpublicationID = -1;
	VectorPubModelPtr pubModelPtr = NULL;

	folderPath.Clear();
	folderPath = ptrIAppFramework->getCatalogPlanningTemplateFolderPath ();
	
	int32 bookpageNo = 0;

	for(int32 docCnt = 0 ; docCnt < documentCount ; docCnt++)
	{
		int32 sectionID = -1;
		IDataBase* bookDB = ::GetDataBase(bookContentMgr);
		
		UID contentUID = bookContentMgr->GetNthContent(docCnt);
		if (contentUID == kInvalidUID) 
			continue; 

		IDFile CurrFile;
		bool16 IsMissingPluginFlag = kFalse;

		IDocument* CurrDoc = Utils<IBookUtils>()->FindDocFromContentUID
				(
					bookDB,
					contentUID,
					CurrFile,
					IsMissingPluginFlag
				);

		IDFile flName = CurrFile;
		UIDRef CurrDocRef = sdklhelp.OpenDocument(flName);
		ErrorCode err = sdklhelp.OpenLayoutWindow(CurrDocRef);
		if(err	== kFailure)
		{
			ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogObserver::refreshWhiteBoardMedia::Document is unable to open");
		}
		
		//IDocument* fntDoc = ::GetFrontDocument(); //CS3
		IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument(); //CS4
		if(fntDoc==nil)
		{
			ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogObserver::refreshWhiteBoardMedia::fntDoc==nil");	
			return;
		}
		//InterfacePtr<ILayoutControlData> layout(::QueryFrontLayoutData());//Cs3
		InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layout == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogObserver::refreshWhiteBoardMedia::layout == nil");		
			return;
		}
		IDataBase* database = ::GetDataBase(fntDoc);
		if(database==nil)
		{
			ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogObserver::refreshWhiteBoardMedia::database==nil");			
			return;
		}
		InterfacePtr<ISpreadList> spreadList((IPMUnknown*)fntDoc,UseDefaultIID());
		if (spreadList==nil)
		{
			ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogObserver::refreshWhiteBoardMedia::spreadList==nil");				
			return;
		}

		
		//PMString spcount("Spread Count	:	");
		//spcount.AppendNumber(spreadList->GetSpreadCount());
		//CA(spcount);
		
		//CA("refreshWhiteBoardMediaForPageBased 2");
		int32 spreadOnDocument = spreadList->GetSpreadCount();
		UID pageUID; 
		
		// Calulate Publicationid from document 
		if(BookpublicationID == -1)
		{
			UIDList allPageItemsFromDoc(database);
			for(int32 spreadNo = 0 ; spreadNo < spreadOnDocument ; spreadNo++)
			{
				UIDRef spreadUIDRef(database, spreadList->GetNthSpreadUID(spreadNo));
				InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
				if(!spread)
				{
					ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogObserver::refreshWhiteBoardMedia::!spread");						
					return;
				}

					UIDList tempList(database);
					spread->GetItemsOnPage(0, &tempList, kFalse,kTrue);
					allPageItemsFromDoc.Append(tempList);
			}

			if(allPageItemsFromDoc.Length() > 0)
			{
				
				int32 sectionid1 = -1;
				for(int i=0; i<allPageItemsFromDoc.Length(); i++)
				{
						//CA("refreshWhiteBoardMediaForPageBased 5 inside pageitem loop"); 
						TagList tList = itagReader->getTagsFromBox(allPageItemsFromDoc.GetRef(i));
						if(tList.size() > 0)
						{
							InterfacePtr<IPMUnknown> unknown(allPageItemsFromDoc.GetRef(i), IID_IUNKNOWN);
							if(!unknown)
							{
								//CA("No unknown prt available");
								break;
							}

							sectionid1 =  tList[0].sectionID;
							if(sectionid1 != -1)
							{
								CPubModel  pubModelObj = ptrIAppFramework->getpubModelByPubID(sectionid1 , 1);
								BookpublicationID = pubModelObj.getRootID();
								//CA("BookpublicationID :  ");
								//CAI(BookpublicationID);
								break;
							}
								
						}

						//
						for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
						{
							tList[tagIndex].tagPtr->Release();
						}
						//
						
				}
			}

			//CAI("BookpublicationID : " , BookpublicationID);
			pubModelPtr = ptrIAppFramework->getAllSubsectionsByPubIdAndLanguageId(BookpublicationID,1);
			if(pubModelPtr == NULL || (pubModelPtr->size() == 0))
			{
				//CA(" BookpublicationID = -1 .. size 0");
				BookpublicationID = -1;
				bookpageNo++;
				sdklhelp.SaveDocumentAs(CurrDocRef,flName);
				sdklhelp.CloseDocument(CurrDocRef, kTrue);
				continue;
			}

		}
		///

		for(int32 spreadNo = 0 ; spreadNo < spreadOnDocument ; spreadNo++ )
		{
			UIDRef spreadUIDRef(database, spreadList->GetNthSpreadUID(spreadNo));
			InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
			if(!spread)
			{
				ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogObserver::refreshWhiteBoardMedia::!spread");						
				return;
			}
			
			int numPages=spread->GetNumPages();
			
			pageUID = spread->GetNthPageUID(numPages-1);		
								
//PMString pgcount("Page Count	:	");
//pgcount.AppendNumber(numPages);
//CA(pgcount);		
			for(int pageNo = 0; pageNo < numPages; pageNo++)
			{
				sectionID = -1;
				//CAI("bookpageNo : ", bookpageNo)
				//CA("refreshWhiteBoardMediaForPageBased 4 inside page loop");
				UIDList tempList(database);
				UIDList allPageItemsperPage(database);
				spread->GetItemsOnPage(pageNo, &tempList, kFalse,kFalse);
				allPageItemsperPage.Append(tempList);

				bool16 isLocked = kFalse;
				for(int i=0; i<allPageItemsperPage.Length(); i++)
				{
					//CA("refreshWhiteBoardMediaForPageBased 5 inside pageitem loop"); 
					TagList tList = itagReader->getTagsFromBox(allPageItemsperPage.GetRef(i));
					if(tList.size() > 0)
					{
								InterfacePtr<IPMUnknown> unknown(allPageItemsperPage.GetRef(i), IID_IUNKNOWN);
						if(!unknown)
						{
									//CA("No unknown prt available");
							break;
						}

						sectionID =  tList[0].sectionID;

						if(pubModelPtr->size() >= bookpageNo +1)
						{
							//CA("pubModelPtr->size() > bookpageNo +1");
							int32 index11= 0; 
							VectorPubModel::iterator it;
							for(it = pubModelPtr->begin() ; it != pubModelPtr->end() ; it++)
							{	
								if(index11 == bookpageNo)
								{
									//CA("index11 == bookpageNo");
									//BookInfo boxInfo = bookInfoList[i];
									isLocked = it->getlocked();	
									//CAI("sectionID 11 : " , sectionID);
									break;
								}
								index11++;
							}							
						}
						if(isLocked)
						{
							//CA("break if locked True");
							break;
						}
						
						IIDXMLElement * xmlElementPtr = tList[0].tagPtr;
							
						UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
						if (textFrameUID == kInvalidUID)
						{
							XMLReference xmlRef = xmlElementPtr->GetXMLReference();
							Utils<IXMLElementCommands>()->DeleteElementAndContent(xmlRef,kTrue);
						}
						else
						{
							XMLReference xmlRef = xmlElementPtr->GetParent();
							Utils<IXMLElementCommands>()->DeleteElementAndContent(xmlRef,kTrue);
						}
								
					}
					

					//Added
					for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
					{
						tList[tagIndex].tagPtr->Release();
					}
					//

				}

				if(isLocked)
				{
					//CA("break if locked True with bookpageNo++");
					bookpageNo++;
					continue;
				}

				if(sectionID == -1)
				{
					//CA("sectionID == -1");
					//CAI("bookpageNo : ", bookpageNo);
					if(pubModelPtr->size() >= bookpageNo +1)
					{
						//CA("pubModelPtr->size() > bookpageNo +1");
						int32 index11= 0; 
						VectorPubModel::iterator it;
						for(it = pubModelPtr->begin() ; it != pubModelPtr->end() ; it++)
						{	
							if(index11 == bookpageNo)
							{
								//CA("index11 == bookpageNo");
								//BookInfo boxInfo = bookInfoList[i];
								sectionID = it->getPublicationID();	
								//CAI("sectionID 11 : " , sectionID);
								break;
							}
							index11++;
						}	

					}
				}
				//CA("refreshWhiteBoardMediaForPageBased 6 after deleting page items from page");
				//CAI("sectionID = ",sectionID);
		VectorAPpubCommentValuePtr vector_pubCommentModel = NULL;
			vector_pubCommentModel = ptrIAppFramework->getPubCommentsBySectionIdStageId(sectionID, 0);
			if(vector_pubCommentModel == nil)
			{
					//CA("vector_pubCommentModel is nil");
					bookpageNo++;
				continue;
			}

				//CA("refreshWhiteBoardMediaForPageBased 6 after deleting page items from page");

			global_VectorAPpubCommentValue.clear ();
			if(vector_pubCommentModel->size() <= 0)
			{
					//CA("vector_pubCommentModel->size() <= 0");
					bookpageNo++;
				continue;
			}
			vector <APpubComment> vec_ApPubComment = *vector_pubCommentModel;

				global_VectorAPpubCommentValue = vec_ApPubComment;			
				createCommentsOnLayerForWBPageBased(spreadNo +1 , pageNo +1);			
				bookpageNo++;
			}
		}

		sdklhelp.SaveDocumentAs(CurrDocRef,flName);
		sdklhelp.CloseDocument(CurrDocRef, kTrue);
	}
-*/
}

void startCreatingMediaNewForWBManualSprayForPAGE_BASED()
 {//start function startCreatingMediaNew
/*-
	//CA("Inside startCreatingMediaNewForWBManualSprayForPAGE_BASED");
	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
		return;

	InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	if(ptrIClientOptions==nil)
	{
		ptrIAppFramework->LogDebug("AP46_RefreshContent::Refresh::doRefresh::Interface for IClientOptions not found.");
		return;
	}		
		
	CPubModel currentPubModelObj = ptrIAppFramework->getpubModelByPubID(MediatorClass :: currentSelectedPublicationID, 1);

	InterfacePtr<IBookManager> bookManager(GetExecutionContextSession(), UseDefaultIID()); //Cs4
	if (bookManager == nil) 
	{ 
		ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaNew::bookManager == nil");
		return; 
	}
	
	IBook * activeBook = bookManager->GetCurrentActiveBook();
	if(activeBook == nil)
	{
		ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaNew::activeBook == nil");
		return;			
	}
		
	IDFile file = activeBook->GetBookFileSpec();

	SDKUtilities sdkutils;
	PMString inddfilepath = file.GetString();

	sdkutils.RemoveLastElement(inddfilepath);
	sdkutils.AppendPathSeparator(inddfilepath);	
	
	//CA("inddfilepath : " + inddfilepath);

	K2Vector<IDFile> contentFileList;
		
	vector<subSectionSprayerListBoxParameters> & subSectionVector = MediatorClass ::vector_subSectionSprayerListBoxParameters;
	int32 subSectionsToBeSrpaydVectorSize = static_cast<int32>(subSectionVector.size()); //Cs4

	PMString AllStencilFilePath,ItemStencilFilePath,ProductStencilFilePath;
			
	if(subSectionsToBeSrpaydVectorSize > 0)
	{	
		//start if subSectionsToBeSrpaydVectorSize > 0
		SDKUtilities sdkutil;
		//bool16 result;
		SubSectionSprayer SSsp;

		subSectionSprayerListBoxParameters & subSectionListBoxParamsOfFirst = subSectionVector[0];		

		currentSelectedSpreadCount = currentPubModelObj.getSpreadCount();
		PMString masterFilePath = subSectionListBoxParamsOfFirst.masterFileWithCompletePath;
		folderPath.Clear();
		folderPath = ptrIClientOptions->getWhiteBoardMediaPath();

		UIDRef dataToBeSpreadDocumentUIDRef	;			
		PMString path(inddfilepath);

		PMString Nov1TempDocSaveAsName = currentPubModelObj.getName();

		path.Append(Nov1TempDocSaveAsName);
		path.Append(".indd");
		SDKLayoutHelper sdklhelp;

		SDKFileHelper masterFile(masterFilePath);
		if(masterFilePath == "" || masterFile.IsExisting() == kFalse)
		{
			CA(masterFilePath + " file doesn't exist");
			return;
		}
		//CA("masterFilePath : " + masterFilePath);

		PMString filePathItemsToBePastedIn(masterFilePath);
		IDFile dataToBeSpreadDocumentIDFile(masterFilePath);
		dataToBeSpreadDocumentUIDRef = sdklhelp.OpenDocument(dataToBeSpreadDocumentIDFile);
		if(dataToBeSpreadDocumentUIDRef == UIDRef ::gNull)
		{
			CA("uidref of dataToBeSpreadDocumentUIDRef is invalid");
			return;
		}						
	
		ErrorCode err = sdklhelp.OpenLayoutWindow(dataToBeSpreadDocumentUIDRef);

		AddOrDeleteSpreadsForWBPageBased(currentSelectedSpreadCount, subSectionsToBeSrpaydVectorSize, currentPubModelObj.getLeftHandOddpages());

		currentSectionIndex = 0;
		MediatorClass::IsLeftHandOddPage = currentPubModelObj.getLeftHandOddpages();
				
		int32 spreadNo = 0;

		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutData == nil)
		{
			CA("ILayoutControlData is nil");
			return;
		}

		IDocument* document = layoutData->GetDocument();
		if (document == nil)
		{
			CA("document is nil");
			return;
		}

		InterfacePtr<ISpreadList>spreadList(document,UseDefaultIID());
		if(spreadList == nil)
		{
			CA("spreadList == nil");
			return;
		}

		int32 spreadCount = spreadList->GetSpreadCount();
	
		int32 pageNo = 0;
		for(spreadNo = 0; spreadNo<spreadCount ;spreadNo++ )
		{
			IGeometry* spreadItem = spreadList->QueryNthSpread(spreadNo);
			if(spreadItem == nil)
				return ;

			InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
			if (iSpread == nil)
				return ;

			int numPages=iSpread->GetNumPages();
			for(int32 i = 0 ;i < numPages;i++)
			{
				if(pageNo < subSectionsToBeSrpaydVectorSize)
				{
					subSectionSprayerListBoxParameters & subSectionListBoxParams = subSectionVector[pageNo];
					vector<APpubComment> apPubCommentBySection = MediatorClass::vector_vector_apPubComment[pageNo];
					
					if(subSectionListBoxParams.isSelected == kTrue)
					{
						int32 Nov1TempSecID = subSectionListBoxParams.vec_ssd[0].sectionID;
						int32 Nov1TempSubSecID = subSectionListBoxParams.vec_ssd[0].subSectionID;

						PMString Nov1TempSubSecName = subSectionListBoxParams.vec_ssd[0].subSectionName;
						int32 currentSelectedSpreadnumber = spreadNo ; //7-may chetan
						//end 1Nov..Check this later.
						CurrentSelectedSection = Nov1TempSecID;
						CurrentSelectedPublicationID = MediatorClass :: currentSelectedPublicationID;
						CurrentSelectedSubSection = Nov1TempSubSecID;
						
						global_project_level = MediatorClass :: global_project_level;
						global_lang_id =MediatorClass ::global_lang_id;
						AcquireWaitCursor adb;					
						adb.Animate();
						
				CPubModel currentSectionPubModelObj = ptrIAppFramework->getpubModelByPubID(Nov1TempSubSecID, 1);
						currentSpread = spreadNo;

					int32 numOfProductsForCurrentSpread=0;
					int32 sizeOfCommentsBySection =static_cast<int32> (apPubCommentBySection.size()); //Cs4
					int32 sizeOfCommentListForSelectedSection =static_cast<int32>(MediatorClass::vector_vector_apPubComment[pageNo].size());//Cs4

					global_VectorAPpubCommentValue.clear ();
					for(int32 j=0; j<sizeOfCommentsBySection; j++)
					{
							numOfProductsForCurrentSpread++;							
							global_VectorAPpubCommentValue.push_back(apPubCommentBySection[j]);
							
						}
						createCommentsOnLayerForWBPageBased(spreadNo +1  , i +1);
					}					
					}
				pageNo++;
			}

		}

		////for(int32 vectorElementIndex = 0;vectorElementIndex < subSectionsToBeSrpaydVectorSize;vectorElementIndex++)
		////{//start for z
		////	//CA("1");			
		////	//global_VectorAPpubCommentValue = MediatorClass::vector_vector_apPubComment[vectorElementIndex];
		////	currentSectionIndex++;
		////	subSectionSprayerListBoxParameters & subSectionListBoxParams = subSectionVector[vectorElementIndex];
		////	vector<APpubComment> apPubCommentBySection = MediatorClass::vector_vector_apPubComment[vectorElementIndex];

		////	//currentSelectedSpreadCount = subSectionListBoxParams.spreadCount;
		////	//PMString masterFilePath = subSectionListBoxParams.masterFileWithCompletePath;
		////	PMString tempPath;

		////	//folderPath.Clear();
		////	//folderPath = ptrIAppFramework->getCatalogPlanningTemplateFolderPath ();//
		////	//folderPath = ptrIClientOptions->getWhiteBoardMediaPath();//

		////	//CA("2");
		////	if(subSectionListBoxParams.isSelected == kTrue)
		////	{

		////		int32 Nov1TempSecID = subSectionListBoxParams.vec_ssd[0].sectionID;
		////		int32 Nov1TempSubSecID = subSectionListBoxParams.vec_ssd[0].subSectionID;

		////		PMString Nov1TempSubSecName = subSectionListBoxParams.vec_ssd[0].subSectionName;
		////		int32 currentSelectedSpreadnumber = subSectionListBoxParams.spreadCount ; //7-may chetan
		////		//end 1Nov..Check this later.
		////		CurrentSelectedSection = Nov1TempSecID;
		////		CurrentSelectedPublicationID = MediatorClass :: currentSelectedPublicationID;
		////		CurrentSelectedSubSection = Nov1TempSubSecID;


		////		global_project_level = MediatorClass :: global_project_level;
		////		global_lang_id =MediatorClass ::global_lang_id;

		////		AcquireWaitCursor adb;					
		////		adb.Animate();
		////		
		////		CPubModel currentSectionPubModelObj = ptrIAppFramework->getpubModelByPubID(Nov1TempSubSecID, 1);

		////		currentSpread = currentSectionPubModelObj.getspread_no();
		////		//for(currentSpread =1; currentSpread <= currentSelectedSpreadCount; currentSpread++)
		////		//{
		////		//	tempPath.Clear();
		////			int32 numOfProductsForCurrentSpread=0;
		////			int32 sizeOfCommentsBySection = apPubCommentBySection.size();
		////			int32 sizeOfCommentListForSelectedSection = MediatorClass::vector_vector_apPubComment[vectorElementIndex].size();

		////			global_VectorAPpubCommentValue.clear ();
		////			for(int32 j=0; j<sizeOfCommentsBySection; j++)
		////			{
		////				//if(apPubCommentBySection[j].spreadNumber == currentSpread )
		////				//{
		////					numOfProductsForCurrentSpread++;							
		////					global_VectorAPpubCommentValue.push_back(apPubCommentBySection[j]);
		////				//}
		////			}
		////			//CA("Before createCommentsOnLayer");
		////			
		////			//createCommentsOnLayer();
		////			///find out page side from currentSectionIndex
		////			int32 pageNumber = 0;
		////			int32 reminder = currentSectionIndex%2;



		////			if(reminder == 1 && MediatorClass::IsLeftHandOddPage)
		////			{
		////				pageNumber = 1;
		////				spreadNo++;
		////			}
		////			if(reminder == 1 && !MediatorClass::IsLeftHandOddPage)
		////				pageNumber = 2;
		////			if(reminder == 0 && MediatorClass::IsLeftHandOddPage)
		////				pageNumber = 2;
		////			if(reminder == 0 && !MediatorClass::IsLeftHandOddPage)
		////			{
		////				pageNumber = 1;
		////				spreadNo++;
		////			}
		////			
		////			createCommentsOnLayerForWBPageBased(spreadNo , pageNumber);
		////			//CA("After createCommentsOnLayer");
		////		//}
		////		//CA("After spraying all comments");
		////	}

		////}

		UIDRef newUIDRef;
		newUIDRef = dataToBeSpreadDocumentUIDRef;
		PMString jpgPath;
		//PMString folderPath;
		//folderPath = ptrIAppFramework->getCatalogPlanningTemplateFolderPath ();//
		PMString TempFolderPath = folderPath;

		//sdkutils.RemoveLastElement(TempFolderPath);
		sdkutil.AppendPathSeparator (TempFolderPath);
		TempFolderPath.AppendNumber(CurrentSelectedPublicationID);
		sdkutils.AppendPathSeparator (TempFolderPath);
		//TempFolderPath.AppendNumber(CurrentSelectedSubSection);
		//sdkutils.AppendPathSeparator (TempFolderPath);
		jpgPath = TempFolderPath;
		FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&TempFolderPath), kTrue);
		TempFolderPath.Append( currentPubModelObj.getName());
		TempFolderPath.Append(".indd");
		IDFile templateFolderDocFile(TempFolderPath);

		//CA("TempFolderPath = " + TempFolderPath);
		//CA("Folder Path = "+folderPath);

		IDFile docFile = sdkutil.PMStringToSysFile(&TempFolderPath);

		ErrorCode status = sdklhelp.SaveDocumentAs(dataToBeSpreadDocumentUIDRef,docFile);
		if(status == kFailure)
		{
			CA("document can not be saved");
			//continue;
		}
	
		//CA(folderPath);
		bool16 status1 = FileUtils::CopyFile(docFile ,templateFolderDocFile);
		if(status1 == kFalse)
		{
			CAlert::InformationAlert("Unable to copy InDesign Documents to central repository folder");
		}

		CreateInddPreview instance;

		IDFile docIdFile(TempFolderPath);
		PMString fileName("");
		fileName.AppendNumber(CurrentSelectedSubSection);
		fileName.Append(".jpeg");

				//PMString fileName = docIdFile.GetFileName();
				//int lastPos1 =0;
				//for (int i = 0 ; i< fileName.CharCount();i++)
				//	if ((fileName[i] == '.'))
				//		lastPos1 = i;
				//// At this point lastpos should point to the last delimeter, knock off the rest of the string.
				//fileName.Truncate(fileName.CharCount()-lastPos1);
				//fileName.Append(".jpeg");
				//CA(" fileName : " + fileName );
				//fileName .Append(".jpeg");

				// takes off the last element from the path. We search for the last delimeter and knock off the
				// stuff that comes after. We lose this data so beware!

		int lastpos = 0;
		for (int i = 0 ; i< path.CharCount();i++)
			if ((path[i] == MACDELCHAR) || (path[i] == UNIXDELCHAR) || (path[i] == WINDELCHAR))
				lastpos = i;
		// At this point lastpos should point to the last delimeter, knock off the rest of the string.
		path.Truncate(path.CharCount()-lastpos);

		int lastpost = 0;
		for (int i = 0 ; i< TempFolderPath.CharCount();i++)
			if ((TempFolderPath[i] == MACDELCHAR) || (TempFolderPath[i] == UNIXDELCHAR) || (TempFolderPath[i] == WINDELCHAR))
				lastpost = i;
		// At this point lastpos should point to the last delimeter, knock off the rest of the string.
		TempFolderPath.Truncate(TempFolderPath.CharCount()-lastpost);

		//CA("path : " + path);
		PMString OutPutFolderPath(TempFolderPath);

		PMString SUbDirectoryPath = "InDesign/WhiteBoard";
		SUbDirectoryPath.Append("/");
		SUbDirectoryPath.AppendNumber(CurrentSelectedPublicationID);


		#ifdef MACINTOSH
			TempFolderPath.Append(MACDELCHAR);
		#else
			TempFolderPath.Append(WINDELCHAR);
		#endif
				
		//CA("path : " + path);
		TempFolderPath.Append(fileName);
		ptrIAppFramework->LogInfo("startCreatingMediaNew::jpegFile Full Path : " + TempFolderPath);
		IDFile jpegFile(TempFolderPath);
//CA(path);
				// get resolution. 
		PMReal desiredRes = 72.0; // 72 Pixels per Inch
		// setup default scales
		PMReal xScale = 1.0, yScale = 1.0;
		IOpenFileCmdData::OpenFlags docOpenFlags = IOpenFileCmdData::kOpenDefault ;
		// create the preview
		// NOTE: See defaults on member declaration
		status = instance.CreatePagePreviewForPAGE_BASED_WB(docIdFile, 
											docOpenFlags, 
											jpegFile, 
											xScale, 
											yScale, 
											desiredRes,
											OutPutFolderPath,
											SUbDirectoryPath,
											jpgPath);
		if (status != kSuccess) 
				{
					//CA("CreatePagePreview failed!");
				}
//CA("2");
		int32 newStageId = 1;
		bool16 statuss = ptrIAppFramework ->GetProjectProducts_updateWhiteBoardStageIdForSection(CurrentSelectedSubSection, newStageId);
		if(statuss == kFalse)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaNewForWBManualSpray::GetProjectProducts_updateWhiteBoardStageIdForSection == kFalse ");
			//return;

		}

		sdklhelp.CloseDocument(dataToBeSpreadDocumentUIDRef,kTrue);
		contentFileList.push_back(docFile);	
		
		if(contentFileList.size()>0)
		{
			//CA("addDocumentsToBook........");
			addDocumentsToBook(contentFileList);
		}

	}
-*/
}

void AddOrDeleteSpreadsForWBPageBased(int32 maxSpreadNumber, int32 PageCount, bool16 isLeftHandOddPage)
{
/*-
//	CAI("maxSpreadNumber : ", maxSpreadNumber);
//	CAI("PageCount : ", PageCount);
	
	InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
	if (layoutData == nil)
	{
		CA("ILayoutControlData is nil");
		return;
	}

	IDocument* document = layoutData->GetDocument();
	if (document == nil)
	{
		CA("document is nil");
		return;
	}

	InterfacePtr<ISpreadList>spreadList(document,UseDefaultIID());
	if(spreadList == nil)
	{
		CA("spreadList == nil");
		return;
	}

	int32 spreadCount = spreadList->GetSpreadCount();

	int32 addSpreadsCount = 0;
	if(spreadCount < maxSpreadNumber)
	{
		addSpreadsCount = maxSpreadNumber - spreadCount;
	}
	//for(int32 spreadIndex = 0; spreadIndex < spreadCount; spreadIndex++)
	//{
		int32 noOfPages = getNoOfPagesfromSpreadNumber(0);
		
		if((noOfPages == 1) && (isLeftHandOddPage))
		{
			for(int32 i = 0; i< PageCount;i++)
			{
				//::AddNewPage();	//Cs3 Deprecated
				Utils<ILayoutUIUtils>()->AddNewPage(); //Cs4
			}
		
			////keepSpreadsTogether
			IDataBase* database = ::GetDataBase(document);
			if(database==nil)
			{
				CA("AP46_CreateMediaMaster::CMMDialogOserver::AddOrDeleteSpread::No database");
				return;
			}
			UIDList spreadUIDList(database);

			for(int32 spreadIndex = 0;spreadIndex < spreadList->GetSpreadCount();spreadIndex++)
			{
				UID spreadUID = spreadList->GetNthSpreadUID(spreadIndex);
				spreadUIDList.Append(spreadUID);				
				
			}
			InterfacePtr<ICommand> islandCmd(CmdUtils::CreateCommand(kSetIslandSpreadCmdBoss)); 
			if(islandCmd == nil)
			{
				CA("islandCmd == nil");
				return ;
			}
			bool16 bAllIslands  = kFalse;//kFalse for normal spread
					
			InterfacePtr<IBoolData> boolData(islandCmd, UseDefaultIID()); 
			if(boolData == nil)
			{
				CA("boolData == nil");
				return ;
			}
			boolData->Set(!bAllIslands); 
			islandCmd->SetItemList(spreadUIDList); 
			
			ErrorCode status = CmdUtils::ProcessCommand(islandCmd); 

			/////Delete Page

			InterfacePtr<IPageList> iPageList(document,UseDefaultIID());
			if(iPageList == nil)
			{
				CA("iPageList == nil");
				return;
			}
			int32 pageCount = iPageList->GetPageCount();

			int32 indexOfLastPageDeleted = 0;//delete only the first page.

			UIDList toBeDeletedPagesUIDList(database);
			for(int32 pageIndex =0;pageIndex <=indexOfLastPageDeleted;pageIndex++)
			{
				toBeDeletedPagesUIDList.Append(iPageList->GetNthPageUID(pageIndex));
			}
			InterfacePtr<ICommand> iDeletePageCmd(CmdUtils::CreateCommand(kDeletePageCmdBoss));
			if (iDeletePageCmd == nil)
			{
				return;
			}
			
			InterfacePtr<IBoolData> iBoolData(iDeletePageCmd,UseDefaultIID());
			if (iBoolData == nil){
	            
				return;
			}
			iBoolData->Set(kFalse);
			
			iDeletePageCmd->SetItemList(toBeDeletedPagesUIDList);
			// process the command
			ErrorCode status1 = CmdUtils::ProcessCommand(iDeletePageCmd);
		}		
		else if((noOfPages == 1) && (!isLeftHandOddPage))
		{
			for(int32 i = 0; i < PageCount-1; i++)
			{
				//::AddNewPage(); //Cs3 Deprictaed
				Utils<ILayoutUIUtils>()->AddNewPage(); //Cs4
			}

			//keepSpreadsTogether
			IDataBase* database = ::GetDataBase(document);
			if(database==nil)
			{
				CA("AP46_CreateMediaMaster::CMMDialogOserver::AddOrDeleteSpread::No database");
				return;
			}
			UIDList spreadUIDList(database);

			for(int32 spreadIndex = 0;spreadIndex < spreadList->GetSpreadCount();spreadIndex++)
			{
				UID spreadUID = spreadList->GetNthSpreadUID(spreadIndex);
				spreadUIDList.Append(spreadUID);
				
				
			}
			InterfacePtr<ICommand> islandCmd(CmdUtils::CreateCommand(kSetIslandSpreadCmdBoss)); 
			if(islandCmd == nil)
			{
				CA("islandCmd == nil");
				return ;
			}
			bool16 bAllIslands  = kTrue;//kFalse for normal spread
					
			InterfacePtr<IBoolData> boolData(islandCmd, UseDefaultIID()); 
			if(boolData == nil)
			{
				CA("boolData == nil");
				return ;
			}
			boolData->Set(bAllIslands); 
			islandCmd->SetItemList(spreadUIDList); 
			
			ErrorCode status = CmdUtils::ProcessCommand(islandCmd); 

		}
	//}
-*/
}




void createCommentsOnLayerForWBPageBased(int32 currentSpreadNo,int32 currentPageNo)
{
/**-
	int32 commentDataPtrVectorLength = static_cast<int32>(global_VectorAPpubCommentValue.size());//Cs4
	
	if(commentDataPtrVectorLength  > 0)// if PubComments available for spray then go ahead 
	{
		//CA("Inside if");
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(!ptrIAppFramework)
		{
			//CA("ptrIAppFramework is NULL");		
			return;
		}
		
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(!iConverter)
		{
			//CA("!iConverter");		
			return;
		}

		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==NULL)
		{
			ptrIAppFramework->LogError("AP46_DataSprayerModel::CDataSprayer::fillImageInBox::!ptrIClientOptions");
			return;
		}

		///////////////// Adding Table types here ////////////////
		VectorTypeInfoPtr typeValObjForProductTable = ptrIAppFramework->ElementCache_getProductTableTypes();
		if(typeValObjForProductTable==nil)
		{
			ptrIAppFramework->LogError("AP46_CreateMediaMaster::CMMDialogObserver::createCommentsOnLayer::ElementCache_getProductTableTypes's typeValObj is nil");
			return;
		}

		VectorTypeInfoPtr typeValObjForItemTable = ptrIAppFramework->AttributeCache_getItemTableTypes();
		if(typeValObjForItemTable==nil){
			ptrIAppFramework->LogError("AP46_CreateMediaMaster::CMMDialogObserver::createCommentsOnLayer::AttributeCache_getItemTableTypes's typeValObj is nil");	
			return;
		}

		////////////////Adding Image Types here//////////////

		VectorTypeInfoPtr typeValObjForProductImage= ptrIAppFramework->ElementCache_getImageAttributesByIndex(3);
		if(typeValObjForProductImage == nil){
			ptrIAppFramework->LogError("AP46_CreateMediaMaster::CMMDialogObserver::createCommentsOnLayer::ElementCache_getImageAttributesByIndex's typeValObj is nil");	
			return;
		}
		//VectorTypeInfoPtr typeValObjForItemImage = ptrIAppFramework->AttributeCache_getItemImagesForClassAndParents(CurrentClassID);

		////////////////////////////////////////////////////////////////////////////////////
		
		
		IDocument* frontDocument = Utils<ILayoutUIUtils>()->GetFrontDocument();  //CS4
		if(frontDocument == nil)
		{
			//CA("frontDocument is NULL");		
			return;
		}

		PMString imagePath=ptrIClientOptions->getImageDownloadPath();
		if(imagePath!="")
		{
			const char *imageP=  (imagePath.GrabCString());
			if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
				#ifdef MACINTOSH
					imagePath+="/";
				#else
					imagePath+="\\";
				#endif
		}
		if(imagePath=="")
		{
			ptrIAppFramework->LogError("SPSelectionObserver::createCommentsOnLayer::imagePath is blank");
			return;
		}

		int32 currentSpreadNumber = 0;
		int32 i = 0;
		for(;i < commentDataPtrVectorLength ; i++)
		{
			
			ptrIAppFramework->clearAllStaticObjects();

			APpubComment objAPpubComment = global_VectorAPpubCommentValue[i];
			objIndex = i;
			
			objAPpubComment.spreadNumber = currentSpreadNo; 

			currentSpreadNumber = objAPpubComment.spreadNumber;
			UIDRef frameUIDRef;

			InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
			if (layoutData == nil)
			{
				//CA("ILayoutControlData is nil");
				break;
			}

			IDocument* document = layoutData->GetDocument();
			if (document == nil)
			{
				//CA("document is nil");
				break;
			}
			
			IDataBase* database = ::GetDataBase(document);		
			if(database == nil)
			{
				//CA("database == nil");
				break;
			}

			InterfacePtr<ISpreadList>spreadList(document,UseDefaultIID());
			if(spreadList == nil)
			{
				//CA("spreadList == nil");
				break;
			}	

			InterfacePtr<IPageList> iPageList(document,UseDefaultIID());
			if(iPageList == nil)
			{
				//CA("iPageList == nil");
				break;
			}

			UIDRef spreadUIDRef(database, spreadList->GetNthSpreadUID(objAPpubComment.spreadNumber-1));
			
			currentSpread = objAPpubComment.spreadNumber;


			InterfacePtr<ISpread> iSpread1(spreadUIDRef, IID_ISPREAD); 
			if(iSpread1 == nil)
			{
				//CA("spread1 == nil");
				//spreadNumber;
				break;
			}
	
			//PMRect pageMargin = iSpread1->GetPagesBounds();
			Transform::CoordinateSpace coordinateSpace_1 = Transform::PasteboardCoordinates() ;
			PMRect pageMargin = iSpread1->GetPagesBounds(coordinateSpace_1);//----CS5-----

			PMReal left3,top3,right3,bottom3;
			left3 = pageMargin.Left();
			top3 = pageMargin.Top();
			right3 = pageMargin.Right();
			bottom3 = pageMargin.Bottom();
			//CA(Dimentions3);

			int32 leftOfPage = ToInt32(left3);
			int32 rightOfPage = ToInt32(right3);
			int32 topOfPage = ToInt32(top3);
			int32 bottomOfPage = ToInt32(bottom3);
			int32 halfOfPageHeight = (bottomOfPage - topOfPage)/2;
			int32 halfOfPageWidth = ToInt32(right3);
			PMReal halfPage = (bottom3 - top3)/2;


			//////
			int numPages=iSpread1->GetNumPages();
			UID pageUID = iSpread1->GetNthPageUID(currentPageNo -1);
			PMString ptype("");
			PageType pagetype = iPageList->GetPageType(pageUID);
			if(pagetype == kLeftPage )
			{
				pageNumber = 1;
				ptype = "kLeftPage";
			}
			else if(pagetype == kRightPage)
			{
				pageNumber =2;
				ptype = "kRightPage";
				if(numPages == 1)
					pageNumber =1;
			}
			else if(pagetype == kUnisexPage )
			{
				pageNumber = 1;
				ptype = "kUnisexPage";
			}

			int32 pageWidth;
			int32 sideOfPage = 0; //0 for left & 1 for right
			if(numPages == 1)
			{
				if(leftOfPage == 0)
				{
					pageWidth = rightOfPage;
					sideOfPage = 1;
					
				}
				else
				{
					pageWidth = -leftOfPage;
					sideOfPage = 0;
				}
			}
			else if(numPages == 2)
			{
				pageWidth = (rightOfPage - leftOfPage)/2;
			}
 
			////
			//////////////////////////////
		//	PMRect box(objAPpubComment.TOP_X_COORDINATE,objAPpubComment.TOP_Y_COORDINATE + top3 ,objAPpubComment.BOT_X_COORDINATE, objAPpubComment.BOT_Y_COORDINATE + top3);
//=================	
			PMString temp;
			//CA(temp);
			temp.Clear();
//=================
			PMPathPointList pathPointList;
			pathPointList.push_back(PMPathPoint(PMPoint(0.0, 0.0)));
			pathPointList.push_back(PMPathPoint(PMPoint(100.0, 100.0)));


			PMString IndsFileFolderPathForComment("");
			IndsFileFolderPathForComment.Append(imagePath);
			IndsFileFolderPathForComment.Append("InDesign");
			#ifdef MACINTOSH
				IndsFileFolderPathForComment+="/";
			#else
				IndsFileFolderPathForComment+="\\";
			#endif
			IndsFileFolderPathForComment.Append("WhiteBoard");
			#ifdef MACINTOSH
				IndsFileFolderPathForComment+="/";
			#else
				IndsFileFolderPathForComment+="\\";
			#endif
		//	IndsFileFolderPathForComment.AppendNumber(objAPpubComment.PUBLICATION_ID);
		//	#ifdef MACINTOSH
		//		IndsFileFolderPathForComment+="/";
		//	#else
		//		IndsFileFolderPathForComment+="\\";
		//	#endif
			IndsFileFolderPathForComment.AppendNumber(objAPpubComment.SECTION_NO);
			#ifdef MACINTOSH
				IndsFileFolderPathForComment+="/";
			#else
				IndsFileFolderPathForComment+="\\";
			#endif
			IndsFileFolderPathForComment.Append("PubComment");
			#ifdef MACINTOSH
				IndsFileFolderPathForComment+="/";
			#else
				IndsFileFolderPathForComment+="\\";
			#endif

			PMString IndsFileName("");
			IndsFileName.AppendNumber(objAPpubComment.APpubCommentID);
			IndsFileName.Append(".inds");

			//CA("IndsFileFolderPathForComment : " + IndsFileFolderPathForComment + IndsFileName);
			bool16 isIndsFileExist = fileExists(IndsFileFolderPathForComment, IndsFileName);
			if(isIndsFileExist)
			{
				//CA("isIndsFileExist");
				PMRect box;
				if(MediatorClass::IsPageBased)
				{
					if(pageNumber == 2)
					{
						//PMRect box(objAPpubComment.TOP_X_COORDINATE + pageWidth,objAPpubComment.TOP_Y_COORDINATE ,objAPpubComment.BOT_X_COORDINATE + pageWidth, objAPpubComment.BOT_Y_COORDINATE );
						box.Left(objAPpubComment.TOP_X_COORDINATE + pageWidth);
						box.Top(objAPpubComment.TOP_Y_COORDINATE);
						box.Right(objAPpubComment.BOT_X_COORDINATE + pageWidth);
						box.Bottom(objAPpubComment.BOT_Y_COORDINATE);
						
					}
					else
					{
					//	PMRect box(objAPpubComment.TOP_X_COORDINATE,objAPpubComment.TOP_Y_COORDINATE ,objAPpubComment.BOT_X_COORDINATE, objAPpubComment.BOT_Y_COORDINATE);
						box.Left(objAPpubComment.TOP_X_COORDINATE);
						box.Top(objAPpubComment.TOP_Y_COORDINATE);
						box.Right(objAPpubComment.BOT_X_COORDINATE);
						box.Bottom(objAPpubComment.BOT_Y_COORDINATE);
					}
				}
				else
				{
					//PMRect box(objAPpubComment.TOP_X_COORDINATE,objAPpubComment.TOP_Y_COORDINATE,objAPpubComment.BOT_X_COORDINATE, objAPpubComment.BOT_Y_COORDINATE );
					box.Left(objAPpubComment.TOP_X_COORDINATE);
					box.Top(objAPpubComment.TOP_Y_COORDINATE);
					box.Right(objAPpubComment.BOT_X_COORDINATE);
					box.Bottom(objAPpubComment.BOT_Y_COORDINATE);
				}
		
				PMString IndsFilePath = IndsFileFolderPathForComment;
				IndsFilePath.Append(IndsFileName);
				//CA("INDS File Exist at " + IndsFileFolderPathForComment + IndsFileName);
				bool16 result = kFalse;
				if(!(objAPpubComment.comment_type == 6 || objAPpubComment.comment_type == 7))
				{
					result = ImportIndsFileInDocument(frontDocument,  IndsFilePath,  box,  objAPpubComment,halfPage,topOfPage);
				if(result){
					//CA("Import Successfull... continue");				
					continue;
				}
			}
			
			}
			
			//CA("!isIndsFileExist");

			PMRect box;
			if(MediatorClass::IsPageBased)
			{
				
				//CA("MediatorClass::IsPageBased");
				if(/pageNumber == 2 || (pageNumber == 1 && pagetype == kRightPage))
				{
					pageNumber = 2;
					//CA("pageNumber == 2");
					//PMRect box(objAPpubComment.TOP_X_COORDINATE + pageWidth,objAPpubComment.TOP_Y_COORDINATE + top3 ,objAPpubComment.BOT_X_COORDINATE + pageWidth, objAPpubComment.BOT_Y_COORDINATE + top3);
					box.Left(objAPpubComment.TOP_X_COORDINATE + pageWidth);
					box.Top(objAPpubComment.TOP_Y_COORDINATE + top3);
					box.Right(objAPpubComment.BOT_X_COORDINATE + pageWidth);
					box.Bottom(objAPpubComment.BOT_Y_COORDINATE + top3);
				}
				else
				{
					//CA("pageNumber == 1");
					//PMRect box(objAPpubComment.TOP_X_COORDINATE,objAPpubComment.TOP_Y_COORDINATE + top3 ,objAPpubComment.BOT_X_COORDINATE, objAPpubComment.BOT_Y_COORDINATE + top3);
					box.Left(objAPpubComment.TOP_X_COORDINATE);
					box.Top(objAPpubComment.TOP_Y_COORDINATE + top3);
					box.Right(objAPpubComment.BOT_X_COORDINATE);
					box.Bottom(objAPpubComment.BOT_Y_COORDINATE + top3);

 
				}
			}
			else
			{
				//CA("Else pageNumber");
				//PMRect box(objAPpubComment.TOP_X_COORDINATE,objAPpubComment.TOP_Y_COORDINATE + top3 ,objAPpubComment.BOT_X_COORDINATE, objAPpubComment.BOT_Y_COORDINATE + top3);
				box.Left(objAPpubComment.TOP_X_COORDINATE);
				box.Top(objAPpubComment.TOP_Y_COORDINATE + top3);
				box.Right(objAPpubComment.BOT_X_COORDINATE);
				box.Bottom(objAPpubComment.BOT_Y_COORDINATE + top3);
			}
			
			CPbObjectValue* CPbObjectValueptr;
			//CA("objAPpubComment.comment_type : ");
			//CAI(objAPpubComment.comment_type);

			switch(objAPpubComment.comment_type)
			{
				case 1:
					//CA("Case 1");
					createRectangle(box,0, frameUIDRef ,objAPpubComment.spreadNumber,pageWidth,objAPpubComment);
					break;

				case 2:
					//CA("Case 2");
					createRectangle(box,0, frameUIDRef ,objAPpubComment.spreadNumber,pageWidth,objAPpubComment);
					break;

				case 3:
					//CA("Case 3");
					createRectangle(box,0, frameUIDRef ,objAPpubComment.spreadNumber,pageWidth,objAPpubComment);
					break;

				case 4:
					//CA("Case 4");
					createOval(box,0,frontDocument, frameUIDRef);
					break;

				case 5:
					//CA("Case 5");
					createArrowGraphic(box,0,frontDocument, frameUIDRef,pathPointList);
					break;

				case 6:
					//CA("Case 6");
					createRectangle(box,0, frameUIDRef ,objAPpubComment.spreadNumber,pageWidth,objAPpubComment);
					break;

				case 7:
					//CA("case 7");
					createRectangle(box,0, frameUIDRef ,objAPpubComment.spreadNumber,pageWidth,objAPpubComment);
					break; 

				case 8:
					//CA("case 8");
					//objAPpubComment.assetId = 1220;
					//objAPpubComment.comment.id = 30022350;
					//CPbObjectValueptr = ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(objAPpubComment.SECTION_NO , objAPpubComment.comment.id);
					//objAPpubComment.comment_type = 9;
					createTableFrame(box,objAPpubComment.stage, frameUIDRef ,objAPpubComment.spreadNumber,objAPpubComment.comment.id, objAPpubComment.assetId, objAPpubComment);
					break;

				case 9:
					//CA("case 9");
					CPbObjectValueptr = ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(objAPpubComment.SECTION_NO , objAPpubComment.comment.id);
					createTableFrame(box,objAPpubComment.stage, frameUIDRef ,objAPpubComment.spreadNumber,CPbObjectValueptr->getPub_object_id(), objAPpubComment.assetId, objAPpubComment);
					break;

				case 10:
					//CA("Case 10");
					createRectangle(box, objAPpubComment, frameUIDRef,pageWidth);
					break;

				case 11:
					//CA("case 11");
					createRectangle(box, objAPpubComment, frameUIDRef,pageWidth);
					break;
////////////////////////////////////////////////////////////////////////		//A
				case 12:
					//CA("case 12");
					createRectangle(box, objAPpubComment, frameUIDRef,pageWidth);
					break;
				case 13:
					//CA("case 13");
					createRectangle(box, objAPpubComment, frameUIDRef,pageWidth);
					break;
/////////////////////////////////////////////////////////////////////////////////
			}
	
			if((objAPpubComment.comment_type == 1 ||objAPpubComment.comment_type == 2 ||objAPpubComment.comment_type == 3 ||objAPpubComment.comment_type == 4||objAPpubComment.comment_type == 5 ))
			{
				//CA("(objAPpubComment.comment_type == 1 ||objAPpubComment.comment_type == 2 ||objAPpubComment.comment_type == 3 ||objAPpubComment.comment_type == 4||objAPpubComment.comment_type == 5 )");
				InterfacePtr<IPMUnknown> unknown(frameUIDRef, IID_IUNKNOWN);
				if(!unknown)
					break;

				UID textFrameUID=Utils<IFrameUtils>()->GetTextFrameUID(unknown);
				if(textFrameUID==kInvalidUID )
				{
					convertBoxToTextBox(frameUIDRef);
					InterfacePtr<IPMUnknown> unknown(frameUIDRef, IID_IUNKNOWN);
					if(!unknown)
						break;
					textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);

					if(objAPpubComment.comment_type == 5)
					{
						addTagToGraphicFrame(frameUIDRef);
						objAPpubComment.isCommentPresentOnDocument = kTrue;
						continue;
					}
				}

				textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
				if(textFrameUID == kInvalidUID)
					break;

							
///Added From Here
				InterfacePtr<IHierarchy> graphicFrameHierarchy((frameUIDRef), UseDefaultIID());
				if (graphicFrameHierarchy == nil) 
				{
					//CA("graphicFrameHierarchy is NULL");
					
					break;
				}
								
				InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
				if (!multiColumnItemHierarchy) {
					//CA("multiColumnItemHierarchy is NULL");
					break;
				}

				InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
				if (!multiColumnItemTextFrame) {
					//CA("multiColumnItemTextFrame is NULL");
					break;
				}
				InterfacePtr<IHierarchy>
				frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
				if (!frameItemHierarchy) {
					//CA("frameItemHierarchy is NULL");
					break;
				}

				InterfacePtr<ITextFrameColumn>
				frameItemTFC(frameItemHierarchy, UseDefaultIID());
				if (!frameItemTFC) {
					//CA("!!ITextFrameColumn");
					break;
				}

				InterfacePtr<ITextModel> txtModel(frameItemTFC->QueryTextModel());
				if(!txtModel)
				{
					//CA("!textModel" );
					break;
				}
//// Up To Here
				//		CA(commentDataPtrVector[i]->comment.commentString);
				PMString textToBeDisplayed(objAPpubComment.comment.commentString);
				if(objAPpubComment.comment_type == 1)//for product
				{
					textToBeDisplayed.Append("\n");
					CObjectValue cObjValue= ptrIAppFramework->GETProduct_getObjectElementValue(objAPpubComment.comment.id,objAPpubComment.languageId);
					PMString productName = iConverter->translateString(cObjValue.getName());
					textToBeDisplayed.Append(productName);
			//		CA(textToBeDisplayed);
					textToBeDisplayed.Append("\n");
					textToBeDisplayed.AppendNumber(objAPpubComment.comment.id);
			//		CA(textToBeDisplayed);
				}
				else if(objAPpubComment.comment_type == 2)//for item
				{
			//		CA("for item");
					textToBeDisplayed.Append("\n");
					CItemModel cItemModel= ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(objAPpubComment.comment.id,objAPpubComment.languageId);
					PMString itemName = iConverter->translateString(cItemModel.getItemDesc());
					textToBeDisplayed.Append(itemName);
			//		CA(textToBeDisplayed);
					textToBeDisplayed.Append("\n");
					textToBeDisplayed.AppendNumber(objAPpubComment.comment.id);
			//		CA(textToBeDisplayed);
				}

				WideString* displayString= &WideString(textToBeDisplayed);
				//.........
					int32 stringLength = displayString->Length();
				//.............
							//	
				txtModel->Insert(0,displayString);
			//	CA("b4 addTagToText"+ commentDataPtrVector[i]->comment.displayName);
				addTagToText(frameUIDRef,txtModel,objAPpubComment.comment.displayName);
			//	CA("after addTagToText");

				objAPpubComment.isCommentPresentOnDocument = kTrue;
			}
			if((objAPpubComment.comment_type == 6) || (objAPpubComment.comment_type == 7))//for product Image 
			{
				//CA("For Item & Product Image Comment Spray");
				PMString imagePathWithSubdir = imagePath;
				PMString typeCodeString;
				int32 ImageTypeId;
				int32 AssetParentTypeId = -1;

				do
				{
					//typeCodeString=ptrIAppFramework->ClientActionGetAssets_getProductAssetFolder(objAPpubComment.assetId);
					VectorAssetValuePtr AssetValuePtrObj = ptrIAppFramework->GETAssets_getAssetByAssetId(objAPpubComment.assetId);
					if(AssetValuePtrObj == NULL){
						//CA("Show Product images : AssetValuePtrObj == NULL");
						return ;
					}
		
					if(AssetValuePtrObj->size() ==0){
						//CA("AssetValuePtrObj->size() ==0");
						return ;			
					}

					PMString fileName("");
					VectorAssetValue::iterator it; // iterator of Asset value

				
					CAssetValue objCAssetvalue;
					for(it = AssetValuePtrObj->begin();it!=AssetValuePtrObj->end();it++)
					{
						objCAssetvalue = *it;
						fileName = objCAssetvalue.getFile_name();
						
						int32 assetID = objCAssetvalue.getAsset_id();					

						if(fileName=="")
							continue ;
						
						int32 image_mpv = objCAssetvalue.getMpv_value_id();
																
						AssetParentTypeId = objCAssetvalue.getParent_type_id();

						ImageTypeId = objCAssetvalue.getType_id();
						do{
							SDKUtilities::Replace(fileName,"%20"," ");
						}while(fileName.IndexOfString("%20") != -1);

					}
					//CA("For Item & Product Image Comment Spray  2");
					if(objAPpubComment.comment_type == 6)
						typeCodeString = ptrIAppFramework->ClientActionGetAssets_getProductAssetFolder(objAPpubComment.assetId);
					if(objAPpubComment.comment_type == 7)
						typeCodeString = ptrIAppFramework->ClientActionGetAssets_getItemAssetFolder(objAPpubComment.assetId);

					if(typeCodeString.NumUTF16TextChars()!=0)
					{ 				
						PMString TempString = typeCodeString;		
						CharCounter ABC = 0;
						bool16 Flag = kTrue;
						bool16 isFirst = kTrue;
						do
						{					
							if(TempString.Contains("/", ABC))
							{
			 					ABC = TempString.IndexOfString("/"); 				
							
		 						PMString * FirstString = TempString.Substring(0, ABC);		 		
		 						PMString newsubString = *FirstString;		 	
										 		
		 						if(!isFirst)
		 						imagePathWithSubdir.Append("\\");
								
		 						imagePathWithSubdir.Append(newsubString);		 	
		 						FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir), kTrue);					 		
		 						isFirst = kFalse;					 		
		 						PMString * SecondString = TempString.Substring(ABC+1);
		 						PMString SSSTring =  *SecondString;		 		
		 						TempString = SSSTring;
								if(FirstString)
									delete FirstString;

								if(SecondString)
									delete SecondString;
							}	
							else
							{				
								if(!isFirst)
		 						imagePathWithSubdir.Append("\\");
								
		 						imagePathWithSubdir.Append(TempString);		 		
		 						FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir), kTrue);		 		
		 						isFirst = kFalse;				
								Flag= kFalse;							
							}			
						}while(Flag);
					}
					//CA("For Item & Product Image Comment Spray 3 ");
					FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir));
					SDKUtilities ::AppendPathSeparator(imagePathWithSubdir);
					//CA(imagePathWithSubdir)	;								
					if(ptrIAppFramework->getAssetServerOption() == 0)
					{
						if(objAPpubComment.comment_type == 6)
						{
							if(!ptrIAppFramework->GETAsset_downLoadProductAsset(objAPpubComment.assetId,imagePathWithSubdir))
							{
								ptrIAppFramework->LogDebug("AP46_DataSprayerModel::CDataSprayer::fillImageInBox::!GETAsset_downLoadProductAsset");				
								continue;
							}
						}
						
						if(objAPpubComment.comment_type == 7)
						{
							if(!ptrIAppFramework->GETAsset_downLoadItemAsset(objAPpubComment.assetId,imagePathWithSubdir))
							{
								ptrIAppFramework->LogDebug("AP46_DataSprayerModel::CDataSprayer::fillImageInBox::!GETAsset_downLoadProductAsset");				
								continue;
							}
						}			
					}
					
					if(!fileExists(imagePathWithSubdir,fileName))	
					{						
						ptrIAppFramework->LogDebug("CDataSprayer:: fillImageInBox: Image Not Found on Local Drive");
						continue; 
					}
					PMString total=imagePathWithSubdir+fileName;	
					
					if(ImportFileInFrame(frameUIDRef, total))
					{
						fitImageInBox(frameUIDRef);
					}
				}while(0);

				InterfacePtr<IPMUnknown> unknown(frameUIDRef, IID_IUNKNOWN);
				if(unknown == NULL){
					ptrIAppFramework->LogDebug("CDataSprayer:: fillImageInBox: unknown NULL");
					return ;
				}
				InterfacePtr<IDocument> doc(frameUIDRef.GetDataBase(), frameUIDRef.GetDataBase()->GetRootUID(), UseDefaultIID());
				if(doc == NULL){
					ptrIAppFramework->LogDebug("CDataSprayer:: fillImageInBox: Document not found...");
					return ;
				}
				InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(doc));
				if (rootElement == NULL){			
					ptrIAppFramework->LogDebug("CDataSprayer:: fillImageInBox: IIDXMLElement NIL...");
					return ;
				}
				
				XMLReference parent = rootElement->GetXMLReference();

				PMString tagName("");
				
				VectorTypeInfoPtr typeValObjForProductImage = ptrIAppFramework->ElementCache_getImageAttributesByIndex(3);
				if(typeValObjForProductImage!=nil)
				{	
					VectorTypeInfoValue::iterator it2;
			
					for(it2=typeValObjForProductImage->begin();it2!=typeValObjForProductImage->end();it2++)
					{	
						int32 typeID = it2->getType_id();
						if(typeID == ImageTypeId)
						{
							//CA("typeID == ImageTypeId");
							tagName = it2->getName();
							break;
						}
					}
				}

				PMString TagName("");				
				
				if(objAPpubComment.comment_type == 7)
				{
					 tagName = ptrIAppFramework->TYPECACHE_getTypeNameById(ImageTypeId);
				}

				TagName = prepareTagName(tagName);

				CPbObjectValue* CPbObjectValueptr = ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(objAPpubComment.SECTION_NO , objAPpubComment.comment.id);

				XMLReference resultXMLRef=kInvalidXMLReference;
				do {
					// Acquire the IXLMElementCommands interface on the Utils boss, and use its method
					// to tag the frame.  Arbitrarily ask for the element to be the 0th child of it's parent.
					ErrorCode errCode = Utils<IXMLElementCommands>()->CreateElement(WideString(TagName), frameUIDRef.GetUID(), parent, 0, &resultXMLRef);
					// Verify the results: no errors, valid XMLRef returned, we can instantiate it.
					if (errCode != kSuccess)
					{
						//CA("ExpXMLActionComponent::TagFrameElement - CreateElement failed");
						break;
					}		
					if (resultXMLRef == kInvalidXMLReference)
					{
						//CA("ExpXMLActionComponent::TagFrameElement - Can't create new XMLReference");
						break;
					}		
					InterfacePtr<IIDXMLElement> newXMLElement (resultXMLRef.Instantiate());
					if (newXMLElement==NULL) 
					{
						//CA("newXMLElement==NULL");
						ptrIAppFramework->LogInfo("CDataSprayer:: fillImageInBox: ExpXMLActionComponent::TagFrameElement - Can't instantiate new XML element");
					}		
					
					PMString attribName("ID");
					PMString attribVal("");
					attribVal.AppendNumber(-1);							
					ErrorCode err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));

					attribName.Clear();
					attribVal.Clear();
					attribName = "typeId";
					attribVal.AppendNumber(ImageTypeId);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "index";

					if(objAPpubComment.comment_type == 6)
					{
						attribVal.AppendNumber(3);	
					}
					if(objAPpubComment.comment_type == 7)
					{
						attribVal.AppendNumber(4);	
					}				

					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "imgFlag";
					attribVal.AppendNumber(1);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
				
					attribName.Clear();
					attribVal.Clear();
					attribName = "parentID";
					attribVal.AppendNumber(objAPpubComment.comment.id);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
					
					attribName.Clear();
					attribVal.Clear();
					attribName = "parentTypeID";					
					attribVal.AppendNumber(AssetParentTypeId);					
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));
					
					attribName.Clear();
					attribVal.Clear();
					attribName = "sectionID";
					attribVal.AppendNumber(objAPpubComment.SECTION_NO);						
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
				
					attribName.Clear();
					attribVal.Clear();
					attribName = "tableFlag";
					attribVal.AppendNumber(0);						
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "LanguageID";								
					attribVal.AppendNumber(objAPpubComment.languageId);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "isAutoResize";							
					attribVal.AppendNumber(objAPpubComment.APpubCommentID);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "rowno";
					//attribVal.AppendNumber(-1);
					attribVal.AppendNumber(-1); // Do not set Pub commnet id for Images
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "colno";
					attribVal.AppendNumber(-1);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
				}while (false);
			}			


			if((objAPpubComment.comment_type == 8) || (objAPpubComment.comment_type == 9))//for product Table 
			{
				//CA("(objAPpubComment.comment_type == 8) || (objAPpubComment.comment_type == 9)");
				// For Item & Product Table Comment Spray

				//CA("objAPpubComment.comment_type == 9");
				

			
				int32 TableTypeID = objAPpubComment.assetId;

				PMString tagName("");
				if(objAPpubComment.comment_type == 8)
				{
					///////////////// Adding Table types here ////////////////
					VectorTypeInfoPtr typeValObjForProductTable = ptrIAppFramework->ElementCache_getProductTableTypes();
					if(typeValObjForProductTable==nil)
					{
						ptrIAppFramework->LogError("AP46_TemplateBuilder::TPLSelectionObserver::populatePRPanelLstbox::ElementCache_getProductTableTypes's typeValObj is nil");
						return;
					}

					if(typeValObjForProductTable!=nil)
					{	
						VectorTypeInfoValue::iterator it2;
				
						for(it2=typeValObjForProductTable->begin();it2!=typeValObjForProductTable->end();it2++)
						{	
							int32 typeID = it2->getType_id();
							if(typeID == TableTypeID)
							{
								//CA("typeID == ImageTypeId");
								tagName = it2->getName();
								break;
							}
						}
					}
				}
				if(objAPpubComment.comment_type == 9)
				{
					VectorTypeInfoPtr typeValObjForItemTable = ptrIAppFramework->AttributeCache_getItemTableTypes();
					if(typeValObjForItemTable==nil){
						ptrIAppFramework->LogError("AP46_TemplateBuilder::TPLSelectionObserver::populateItemPanelLstbox::AttributeCache_getItemTableTypes's typeValObj is nil");	
						return;
					}

					if(typeValObjForItemTable!=nil)
					{	
						VectorTypeInfoValue::iterator it2;
				
						for(it2=typeValObjForItemTable->begin();it2!=typeValObjForItemTable->end();it2++)
						{	
							int32 typeID = it2->getType_id();
							if(typeID == TableTypeID)
							{
								//CA("typeID == TableTypeID");
								tagName = it2->getName();
								break;
							}
						}
					}
				}

				//CA("tagName = " + tagName);

				/// Creating and Adding tag to newly created Item image box
				InterfacePtr<IPMUnknown> unknown(frameUIDRef, IID_IUNKNOWN);
				if(unknown == NULL){
					ptrIAppFramework->LogDebug("CDataSprayer:: fillImageInBox: unknown NULL");
					return ;
				}
				InterfacePtr<IDocument> doc(frameUIDRef.GetDataBase(), frameUIDRef.GetDataBase()->GetRootUID(), UseDefaultIID());
				if(doc == NULL){
					ptrIAppFramework->LogDebug("CDataSprayer:: fillImageInBox: Document not found...");
					return ;
				}
				InterfacePtr<IIDXMLElement> rootElement(Utils<IXMLUtils>()->QueryRootElement(doc));
				if (rootElement == NULL){			
					ptrIAppFramework->LogDebug("CDataSprayer:: fillImageInBox: IIDXMLElement NIL...");
					return ;
				}
				
				XMLReference parent = rootElement->GetXMLReference();

				PMString TagName = prepareTagName(tagName);

				CPbObjectValueptr = ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(objAPpubComment.SECTION_NO , objAPpubComment.comment.id);
				//CPbObjectValue* CPbObjectValueptr = ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(objAPpubComment.SECTION_NO , objAPpubComment.comment.id);

				XMLReference resultXMLRef=kInvalidXMLReference;
				do {
					// Acquire the IXLMElementCommands interface on the Utils boss, and use its method
					// to tag the frame.  Arbitrarily ask for the element to be the 0th child of it's parent.
					ErrorCode errCode = Utils<IXMLElementCommands>()->CreateElement(WideString(TagName), frameUIDRef.GetUID(), parent, 0, &resultXMLRef);
					// Verify the results: no errors, valid XMLRef returned, we can instantiate it.
					if (errCode != kSuccess)
					{
						//CA("ExpXMLActionComponent::TagFrameElement - CreateElement failed");
						break;
					}		
					if (resultXMLRef == kInvalidXMLReference)
					{
						//CA("ExpXMLActionComponent::TagFrameElement - Can't create new XMLReference");
						break;
					}		
					InterfacePtr<IIDXMLElement> newXMLElement (resultXMLRef.Instantiate());
					if (newXMLElement==NULL) 
					{
						//CA("newXMLElement==NULL");
						ptrIAppFramework->LogInfo("CDataSprayer:: fillImageInBox: ExpXMLActionComponent::TagFrameElement - Can't instantiate new XML element");
					}		
					
					PMString attribName("ID");
					PMString attribVal("");
					attribVal.AppendNumber(-1);							
					ErrorCode err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "typeId";
					attribVal.AppendNumber(TableTypeID);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "index";

					if(objAPpubComment.comment_type == 8)
					{
						attribVal.AppendNumber(3);	
					}
					if(objAPpubComment.comment_type == 9)
					{
						attribVal.AppendNumber(4);	
					}
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
					
					attribName.Clear();
					attribVal.Clear();
					attribName = "imgFlag";
					attribVal.AppendNumber(0);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
				
					attribName.Clear();
					attribVal.Clear();
					attribName = "parentID";
					attribVal.AppendNumber(objAPpubComment.comment.id);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
					
					attribName.Clear();
					attribVal.Clear();
					attribName = "parentTypeID";
					attribVal.AppendNumber(CPbObjectValueptr->getobject_type_id());							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
					
					attribName.Clear();
					attribVal.Clear();
					attribName = "sectionID";
					attribVal.AppendNumber(CurrentSelectedSection);						
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
				
					attribName.Clear();
					attribVal.Clear();
					attribName = "tableFlag";
					attribVal.AppendNumber(1);						
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "LanguageID";								
					attribVal.AppendNumber(objAPpubComment.languageId);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "isAutoResize";							
					attribVal.AppendNumber(objAPpubComment.APpubCommentID);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "rowno";
					attribVal.AppendNumber(-1);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4

					attribName.Clear();
					attribVal.Clear();
					attribName = "colno";
					attribVal.AppendNumber(-1);							
					err = Utils<IXMLAttributeCommands>()->CreateAttribute(resultXMLRef,WideString(attribName),WideString(attribVal));//Cs4
				}while (false);

				InterfacePtr<ITableUtility> iTableUtlObj
				((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
				if(!iTableUtlObj)
				{
					// CA("!iTableUtlObj");
					ptrIAppFramework->LogError("AP46_CreateMediaMaster::CMMDialogOserver::fillDataInTable::!iTableUtlObj");
					return ;
				}

				InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
				if(!DataSprayerPtr)
				{
                    ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogOserver::getAllBoxIds::Pointre to DataSprayerPtr not found");
					return;
				}

				InterfacePtr<ITagReader> itagReader
				((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
				if(!itagReader)
				{
					ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogOserver::createCommentsOnLayer:itagReader == nil");
					return;
				}

				IIDXMLElement* ptr = NULL;
				TagList tagList = itagReader->getFrameTags(frameUIDRef);


				TagStruct tagStruct = tagList[0];

				PublicationNode refpNode;
				PMString PubName("PubName");

				if(tagStruct.whichTab == 3)
					refpNode.setAll(PubName, tagStruct.parentId, CPbObjectValueptr->getPub_object_id(),tagStruct.sectionID,1,1,1,tagStruct.typeId,0,kTrue,kFalse);
				else if(tagStruct.whichTab == 4)
					refpNode.setAll(PubName, tagStruct.parentId, CPbObjectValueptr->getPub_object_id(),tagStruct.sectionID,1,1,1,tagStruct.typeId,0,kFalse,kFalse);
			
				DataSprayerPtr->FillPnodeStruct(refpNode, tagStruct.sectionID, 1, tagStruct.sectionID);
				
				DataSprayerPtr->getAllIds(tagStruct.parentId);
				iTableUtlObj->SetDBTableStatus(kTrue);
				
				int32 tableId=0;
				//CA("Going for DataSprayer fillDataInTable ");
				UIDRef tableUIDRef;
				if(isTablePresent(frameUIDRef, tableUIDRef))
				{
					if(objAPpubComment.comment_type == 8)
					{
						iTableUtlObj->fillDataInTable(tableUIDRef, refpNode, tagStruct, tableId, frameUIDRef);
					}
					if(objAPpubComment.comment_type == 9)
					{
						XMLReference boxXMLRef = tagStruct.tagPtr->GetXMLReference();
						iTableUtlObj->FillDataInsideItemTableOfItem(boxXMLRef ,tableUIDRef);
					}
				}			
			}


			if(objAPpubComment.comment_type == 10 || objAPpubComment.comment_type == 11	|| objAPpubComment.comment_type == 12 || objAPpubComment.comment_type == 13)
			{
				// For Item & Product Copy Frame Comment Spray 
				//CA("objAPpubComment.comment_type == 10 || objAPpubComment.comment_type == 11 ||	objAPpubComment.comment_type == 12");
				InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
				if(!DataSprayerPtr)
				{
                    ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogOserver::createCommentsOnLayer::Pointre to DataSprayerPtr not found");
					return;
				}
				
				InterfacePtr<ITagReader> itagReader
				((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
				if(!itagReader)
				{
					ptrIAppFramework->LogDebug("AP46_CreateMediaMaster::CMMDialogOserver::createCommentsOnLayer::itagReader == nil");
					return;
				}

				CPbObjectValue* CPbObjectValueptr = ptrIAppFramework->GetProjectProducts_findByObjectIdPubId(objAPpubComment.SECTION_NO , objAPpubComment.comment.id);
				
				PublicationNode pNode;
				
				//pNode.setPublicationName();
				pNode.setPubId(objAPpubComment.comment.id);
				pNode.setParentId(objAPpubComment.SECTION_NO);
				pNode.setLevel(CPbObjectValueptr->getLevel_no());
				pNode.setSequence(CPbObjectValueptr->getSeq_order());
				pNode.setChildCount(CPbObjectValueptr->getChildCount());
				//pNode.setHitCount();
				//pNode.setReferenceId();
				pNode.setTypeId(CPbObjectValueptr->getobject_type_id());
				//pNode.setDesignerAction();
				pNode.setNewProduct(CPbObjectValueptr->getNew_product());
				pNode.setPBObjectID(CPbObjectValueptr->getPub_object_id());
				pNode.setIsProduct(CPbObjectValueptr->getisProduct());
				pNode.setIsONEsource(kFalse);
				pNode.setIsStarred(CPbObjectValueptr->getStarredFlag1());
				//CA("before fill node list");
				DataSprayerPtr->FillPnodeStruct(pNode,objAPpubComment.SECTION_NO,CPbObjectValueptr->getPublication_id(),objAPpubComment.SECTION_NO);

				DataSprayerPtr->setFlow(kFalse);
				DataSprayerPtr->getAllIds(pNode.getPubId());//For these PF, PR, PG ITEM ID's we have to spray the data

				//CA("after fill node list");

				IIDXMLElement* ptr = NULL;
				TagList tagList = itagReader->getTagsFromBox(frameUIDRef, &ptr);
 
//15/11/07 For WhiteBoard to change rowNo with pubCommentID		
				if(tagList.size()>=0)
				{					
					for(int32 tagCounter=0;tagCounter<tagList.size() ;tagCounter++)
					{
						XMLReference  childXMLReference = ptr->GetNthChild(tagCounter);
						//IIDXMLElement* chldPtr =  childXMLReference.Instantiate();
						InterfacePtr<IIDXMLElement>chldPtr(childXMLReference.Instantiate());
						TagStruct tagInfoo;
						tagInfoo = tagList[tagCounter];
						PMString isAutoResize("");
						isAutoResize.AppendNumber(objAPpubComment.APpubCommentID);
						//CA(rowNo);
						chldPtr->SetAttributeValue(WideString("isAutoResize"),WideString(isAutoResize));//Cs4
					}
				}
							
				if(tagList.size()<=0)//This can be a Tagged Frame
				{
					if(DataSprayerPtr->isFrameTagged(frameUIDRef))
					{	
						//CA("isFrameTagged(selectUIDList.GetRef(i))");
						//CA("before sprayForTaggedBox");						
						DataSprayerPtr->sprayForTaggedBox(frameUIDRef);
						//CA("After sprayForTaggedBox");
					}
					continue;
				}
				else
				{
					//CA("before sprayForThisBox");
					DataSprayerPtr->sprayForThisBox(frameUIDRef, tagList);
					//CA("After sprayForThisBox");
				}

				//Added
				for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
				{
					tagList[tagIndex].tagPtr->Release();
				}
				//
				

				UIDList list(frameUIDRef);
				resizeTextFrame(frameUIDRef,list,objAPpubComment.BOT_X_COORDINATE - objAPpubComment.TOP_X_COORDINATE, objAPpubComment.BOT_Y_COORDINATE - objAPpubComment.TOP_Y_COORDINATE,halfPage,topOfPage);
				//DataSprayerPtr->sprayForThisBox(frameUIDRef,);				
			}
		}
		objIndex = 0;
		previousSpreadNumber = currentSpreadNumber;
	}
 -*/
}


bool16 startCreatingMediaForPriceBookNew()
{
	// CA("Inside startCreatingMediaForPriceBookNew");
	 InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	 if(ptrIAppFramework == nil)
		return kFalse;
	
	InterfacePtr<IBookManager> bookManager(/*gSession*/GetExecutionContextSession(), UseDefaultIID()); //Cs4
	if (bookManager == nil) 
	{ 
		ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaNew::bookManager == nil");
		return kFalse; 
	}
	IBook * activeBook = bookManager->GetCurrentActiveBook();
	if(activeBook == nil)
	{
		ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaNew::activeBook == nil");
		return kFalse;			
	}
	
	//IDFile file = activeBook->GetBookFileSpec();
	SDKUtilities sdkutils;
	SDKLayoutHelper sdklhelp;
	UIDRef dataToBeSpreadDocumentUIDRef= UIDRef ::gNull;

    SDKFileHelper activeBookFileHelper(activeBook->GetBookFileSpec());
    PMString inddfilepath = activeBookFileHelper.GetPath();
	sdkutils.RemoveLastElement(inddfilepath);
	sdkutils.AppendPathSeparator(inddfilepath);	
//CA("inddfilepath : " + inddfilepath);
	
	K2Vector<IDFile> contentFileList;
	vector<subSectionSprayerListBoxParameters> & subSectionVector = MediatorClass ::vector_subSectionSprayerListBoxParameters;
	int32/*int64*/ subSectionsToBeSrpaydVectorSize =static_cast<int32> (subSectionVector.size()); //Cs4


	stencilFileInfo.clear();
	
	if(subSectionsToBeSrpaydVectorSize > 0)
	{
		vector<UID> allUIDList;
		int32 sectionloopIndex = 0;
		for(int32 vectorElementIndex = 0;vectorElementIndex < subSectionsToBeSrpaydVectorSize;vectorElementIndex++)
		{
			subSectionSprayerListBoxParameters & subSectionListBoxParams = subSectionVector[vectorElementIndex];
			
			if(subSectionListBoxParams.isSelected == kTrue)
			{
//CA("subSectionListBoxParams.isSelected == kTrue");	
				
				sectionloopIndex++;
				//This loop is added for all the section spraying for pricebook
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				if(sectionloopIndex == 1 )
				{
					PMString masterFilePath = subSectionListBoxParams.masterFileWithCompletePath;
					if(masterFilePath == "")
					{
						CAlert::InformationAlert("Select master file.");
						return kFalse;
					}
					SDKFileHelper masterFile(masterFilePath);
					if(masterFilePath == "" || masterFile.IsExisting() == kFalse)
					{
						CA(masterFilePath + " file doesn't exist");
						return kFalse;
					}
					
					PMString filePathItemsToBePastedIn(masterFilePath);
					IDFile dataToBeSpreadDocumentIDFile(masterFilePath);
					if(dataToBeSpreadDocumentUIDRef == UIDRef ::gNull )
					{
						if(vectorElementIndex > 0)
							CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority); 

						dataToBeSpreadDocumentUIDRef = sdklhelp.OpenDocument(dataToBeSpreadDocumentIDFile);
						if(dataToBeSpreadDocumentUIDRef == UIDRef ::gNull)
						{
							CA("uidref of dataToBeSpreadDocumentUIDRef is invalid");
							return kFalse;
						}						
						ErrorCode err = sdklhelp.OpenLayoutWindow(dataToBeSpreadDocumentUIDRef);
						if(err == kFailure)
						{
							CA("Error occured while opening the layoutwindow of the dataToBeSprayedDocumentFileLayout");
							return kFalse;
						}			
					}
				}	
				InterfacePtr<IDocument> dataToBeSprayedDocument1(dataToBeSpreadDocumentUIDRef,UseDefaultIID());
				if(dataToBeSprayedDocument1 == nil)
				{
					//CA("dataToBeSprayedDocument1 == nil");
					return kFalse;
				}

				InterfacePtr<ISpreadList>dataToBeSprayedDocumentSpreadList1(dataToBeSprayedDocument1,UseDefaultIID());
				if(dataToBeSprayedDocumentSpreadList1 == nil)
				{
					//CA("dataToBeSprayedDocumentSpreadList1 == nil");
					continue;
				}

				IDataBase * dataToBeSprayedDocumentDatabase1 = dataToBeSpreadDocumentUIDRef.GetDataBase();
				if(dataToBeSprayedDocumentDatabase1 == nil)
				{
					//CA("dataToBeSprayedDocumentDatabase1 == nil");
					continue;
				}

				UIDRef dataToBeSprayedDocumentFirstSpreadUIDRef1(dataToBeSprayedDocumentDatabase1, dataToBeSprayedDocumentSpreadList1->GetNthSpreadUID(0)); 

				InterfacePtr<ISpread> dataToBeSprayedDocumentFirstSpread1(dataToBeSprayedDocumentFirstSpreadUIDRef1, IID_ISPREAD); 
				if(dataToBeSprayedDocumentFirstSpread1 == nil)
				{
					//CA("spread == nil");
					continue;
				}

				UIDList dataToBeSprayedDocumentFrameUIDList1(dataToBeSprayedDocumentDatabase1);
				if(dataToBeSprayedDocumentFirstSpread1->GetNthPageUID(0)== kInvalidUID) 
				{
					//CA("pageUID is invalid");
					continue;
				}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//		int32 subSectionDataVectorSize = subSectionListBoxParams.vec_ssd.size();
		
		//		for(int32 vectorNumber=0;vectorNumber<subSectionDataVectorSize ;vectorNumber++)	
		//		{																							
					//following is the path of master file i.e. templateFilePath
					
					//PMString masterFilePath = subSectionListBoxParams.masterFileWithCompletePath;
					PMString productStencilFilePath = subSectionListBoxParams.ProductStencilFilePath;
					PMString itemStencilFilePath = subSectionListBoxParams.ItemStencilFilePath;
					PMString hybridTableStencilFilePath = subSectionListBoxParams.HybridTableStencilFilePath;
	//New added for pricebook
					PMString sectionStencilFilePath = subSectionListBoxParams.SectionStencilFilePath;

					SDKFileHelper productStencilFile(productStencilFilePath);
					SDKFileHelper itemStencilFile(itemStencilFilePath);
					SDKFileHelper hybridTableStencilFile(hybridTableStencilFilePath); 
					SDKFileHelper sectionStencilFile(sectionStencilFilePath);


					if(productStencilFilePath == "" && itemStencilFilePath == "" && hybridTableStencilFilePath == "" && sectionStencilFilePath == "" )
					{
						CAlert::InformationAlert("Select template files to spray.");
						return kFalse;
					}
					
////////////////if only one File path is selected

					if(itemStencilFilePath != "" && productStencilFilePath == ""  && hybridTableStencilFilePath == "")
					{
	//CAlert::InformationAlert("stencil for item is selected.");
						if(itemStencilFile.IsExisting() == kFalse)
						{
							CAlert::InformationAlert("item template file : " + itemStencilFilePath + " not found.");
							continue;
						}
						subSectionListBoxParams.AllStencilFilePath = itemStencilFilePath;
						subSectionListBoxParams.isSingleStencilFileForProductAndItem = kTrue;
					}
					
					if(productStencilFilePath != "" && itemStencilFilePath == "" && hybridTableStencilFilePath == "")
					{
	//CAlert::InformationAlert("stencil for product is selected.");
						if(productStencilFile.IsExisting() == kFalse)
						{
							CAlert::InformationAlert("item group template file : " + productStencilFilePath + " not found.");
							continue;
						}
						subSectionListBoxParams.AllStencilFilePath = productStencilFilePath;
						subSectionListBoxParams.isSingleStencilFileForProductAndItem = kTrue;
					}
					
					if(hybridTableStencilFilePath != "" && productStencilFilePath == ""  && itemStencilFilePath == "")
					{
	//CAlert::InformationAlert("stencil for hybridTable is selected.");
						if(hybridTableStencilFile.IsExisting() == kFalse)
						{
							CAlert::InformationAlert("hybridTable template file : " + hybridTableStencilFilePath + " not found.");
							continue;
						}
						subSectionListBoxParams.AllStencilFilePath = hybridTableStencilFilePath;
						subSectionListBoxParams.isSingleStencilFileForProductAndItem = kTrue;
					}

					if(itemStencilFilePath != "" && productStencilFilePath != ""  && hybridTableStencilFilePath == "")
					{
	//CAlert::InformationAlert("stencil of product is selected for hybridTable");
						if(itemStencilFile.IsExisting() == kFalse)
						{
							CAlert::InformationAlert("item template file : " + itemStencilFilePath + " not found.");
							continue;
						}
						
						if(productStencilFile.IsExisting() == kFalse)
						{
							CAlert::InformationAlert("product template file : " + productStencilFilePath + " not found.");
							continue;
						}
						
						subSectionListBoxParams.HybridTableStencilFilePath =productStencilFilePath;
						subSectionListBoxParams.isSingleStencilFileForProductAndItem = kFalse;
					}
					
					if(itemStencilFilePath == "" && productStencilFilePath != ""  && hybridTableStencilFilePath != "" )
					{
	//CAlert::InformationAlert("stencil of product is selected for item");
						if(hybridTableStencilFile.IsExisting() == kFalse)
						{
							CAlert::InformationAlert("hybridTable template file : " + hybridTableStencilFilePath + " not found.");
							continue;
						}
						
						if(productStencilFile.IsExisting() == kFalse)
						{
							CAlert::InformationAlert("product template file : " + productStencilFilePath + " not found.");
							continue;
						}
						
						subSectionListBoxParams.ItemStencilFilePath = productStencilFilePath;
						subSectionListBoxParams.isSingleStencilFileForProductAndItem = kFalse;
					}

					if(itemStencilFilePath != "" && productStencilFilePath == ""  && hybridTableStencilFilePath != "" )
					{
	//CAlert::InformationAlert("stencil of item is selected for product");
						if(productStencilFile.IsExisting() == kFalse)
						{
							CAlert::InformationAlert("product template file : " + productStencilFilePath + " not found.");
							continue;
						}
						
						if(itemStencilFile.IsExisting() == kFalse)
						{
							CAlert::InformationAlert("item template file : " + itemStencilFilePath + " not found.");
							continue;
						}
						subSectionListBoxParams.ProductStencilFilePath = itemStencilFilePath;
						subSectionListBoxParams.isSingleStencilFileForProductAndItem = kFalse;
					}

	//New Added for priceBook on 3/1/08	
					if(sectionStencilFilePath !="")
					subSectionListBoxParams.SectionStencilFilePath = sectionStencilFilePath;
				
					bool16 result = kFalse;
			
					if(subSectionListBoxParams.isSingleStencilFileForProductAndItem)
					{
//CA("subSectionListBoxParams.isSingleStencilFileForProductAndItem	== kTrue");
						bool16 pathFound = kFalse;
						if(stencilFileInfo.size() == 0)
						{		
//CA(subSectionListBoxParams.AllStencilFilePath);
							StencilFileInfo stencilFileInfoObj;
							InterfacePtr<IPageItemScrapData>  scrapData;
							result =  copyStencilsFromTheTemplateDocumentIntoScrapData(subSectionListBoxParams.AllStencilFilePath, scrapData);
							if(result == kFalse)
							{
								ptrIAppFramework->LogDebug("result == kFalse while copying");
								continue;
							}
							
							result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapData,dataToBeSpreadDocumentUIDRef );
							
							UIDList tempUIDListForALL(dataToBeSprayedDocumentDatabase1);
							dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&tempUIDListForALL,kFalse,kTrue);
							
							stencilFileInfoObj.path = subSectionListBoxParams.AllStencilFilePath;
							stencilFileInfoObj.type = "A";
							stencilFileInfoObj.uidList = tempUIDListForALL;
							stencilFileInfo.push_back(stencilFileInfoObj);
						
							for(int uidItem = 0 ; uidItem < tempUIDListForALL.size() ; uidItem++)
								allUIDList.push_back(tempUIDListForALL[uidItem]);

						}
						if(subSectionListBoxParams.AllStencilFilePath != "")
						{
							pathFound = kFalse;
							for(int itemFileNo = 0 ; itemFileNo < stencilFileInfo.size() ; itemFileNo++)
							{
								if(stencilFileInfo[itemFileNo].path == subSectionListBoxParams.AllStencilFilePath )
								{
									pathFound = kTrue;
									break;
								}
							}
							if(pathFound == kFalse)
							{
//CA(subSectionListBoxParams.AllStencilFilePath);
								InterfacePtr<IPageItemScrapData>  scrapData;
								result =  copyStencilsFromTheTemplateDocumentIntoScrapData(subSectionListBoxParams.AllStencilFilePath, scrapData);
								if(result == kFalse)
								{
									ptrIAppFramework->LogDebug("result == kFalse while copying");
									continue;
								}
								result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapData,dataToBeSpreadDocumentUIDRef );
										
								UIDList tempUIDListForALL(dataToBeSprayedDocumentDatabase1);
								dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&tempUIDListForALL,kFalse,kTrue);
								
								UIDList allitemprotabUIDList(dataToBeSprayedDocumentDatabase1);
								for(int32 i = 0; i < tempUIDListForALL.size() ; i++)
								{
									bool16 found = kFalse;
									for(int32 j = 0 ; j < allUIDList.size() ; j++)
									{
										if(tempUIDListForALL[i] == allUIDList[j])
										{
											found = kTrue;
											break;
										}
									}
									if(found == kFalse)
									{
										allUIDList.push_back(tempUIDListForALL[i]);
										allitemprotabUIDList.push_back(tempUIDListForALL[i]);
									}
								}
								StencilFileInfo stencilFileInfoObj;
								stencilFileInfoObj.path = subSectionListBoxParams.AllStencilFilePath;
								stencilFileInfoObj.type = "A";
								stencilFileInfoObj.uidList = allitemprotabUIDList ;
								stencilFileInfo.push_back(stencilFileInfoObj);
							}
						}	
					
						if(subSectionListBoxParams.SectionStencilFilePath != "")
						{
							pathFound = kFalse;
							for(int itemFileNo = 0 ; itemFileNo < stencilFileInfo.size() ; itemFileNo++)
							{
								if(stencilFileInfo[itemFileNo].path == subSectionListBoxParams.SectionStencilFilePath )
								{
									pathFound = kTrue;
									break;
								}
							}
							if(pathFound == kFalse)
							{
//CA(subSectionListBoxParams.SectionStencilFilePath);
								InterfacePtr<IPageItemScrapData> scrapDataForSection;
								result =  copyStencilsFromTheTemplateDocumentIntoScrapData(subSectionListBoxParams.SectionStencilFilePath , scrapDataForSection);
								if(result == kFalse)
								{
									//CA("result == kFalse while copying");
									continue;
								}
			
								result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapDataForSection,dataToBeSpreadDocumentUIDRef );
								if(result == kFalse)
								{
									//CA("result == kFalse while pasting");
									continue;
								}
								dataToBeSprayedDocumentFrameUIDList1.Clear();
								UIDList tempSectionStencilUIDList(dataToBeSprayedDocumentDatabase1);
								dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&tempSectionStencilUIDList,kFalse,kTrue);
								
								UIDList sectionUIDList(dataToBeSprayedDocumentDatabase1);
								for(int32 i = 0; i < tempSectionStencilUIDList.size() ; i++)
								{
									bool16 found = kFalse;
									for(int32 j = 0 ; j < allUIDList.size() ; j++)
									{
										if(tempSectionStencilUIDList[i] == allUIDList[j])
										{
											found = kTrue;
											break;
										}
									}
									if(found == kFalse)
									{
										allUIDList.push_back(tempSectionStencilUIDList[i]);
										sectionUIDList.push_back(tempSectionStencilUIDList[i]);
									}
								}

								StencilFileInfo stencilFileInfoObj;
								stencilFileInfoObj.path = subSectionListBoxParams.SectionStencilFilePath;
								stencilFileInfoObj.type = "S";
								stencilFileInfoObj.uidList = sectionUIDList;
								stencilFileInfo.push_back(stencilFileInfoObj);
							}
						}
					}
					else
					{
//CA("subSectionListBoxParams.isSingleStencilFileForProductAndItem	== kFalse");
						
						
						if(stencilFileInfo.size() == 0)
						{		
//CA(subSectionListBoxParams.ItemStencilFilePath);
							StencilFileInfo stencilFileInfoObj;
							InterfacePtr<IPageItemScrapData>  scrapData;
							result =  copyStencilsFromTheTemplateDocumentIntoScrapData(subSectionListBoxParams.ItemStencilFilePath, scrapData);
							if(result == kFalse)
							{
								ptrIAppFramework->LogDebug("result == kFalse while copying");
								continue;
							}
							
							result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapData,dataToBeSpreadDocumentUIDRef );
							
							UIDList tempUIDListForItem(dataToBeSprayedDocumentDatabase1);
							dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&tempUIDListForItem,kFalse,kTrue);
							
							stencilFileInfoObj.path = subSectionListBoxParams.ItemStencilFilePath;
							stencilFileInfoObj.type = "I";
							stencilFileInfoObj.uidList = tempUIDListForItem;
							stencilFileInfo.push_back(stencilFileInfoObj);
						
							for(int uidItem = 0 ; uidItem < tempUIDListForItem.size() ; uidItem++)
								allUIDList.push_back(tempUIDListForItem[uidItem]);

						}

						bool16 pathFound = kFalse;
						if(subSectionListBoxParams.ItemStencilFilePath != "")
						{
							pathFound = kFalse;
							for(int itemFileNo = 0 ; itemFileNo < stencilFileInfo.size() ; itemFileNo++)
							{
								if(stencilFileInfo[itemFileNo].path == subSectionListBoxParams.ItemStencilFilePath )
								{
									pathFound = kTrue;
									break;
								}
							}
							if(pathFound == kFalse)
							{
//CA(subSectionListBoxParams.ItemStencilFilePath);
								
								
								InterfacePtr<IPageItemScrapData>  scrapData;
								result =  copyStencilsFromTheTemplateDocumentIntoScrapData(subSectionListBoxParams.ItemStencilFilePath, scrapData);
								if(result == kFalse)
								{
									ptrIAppFramework->LogDebug("result == kFalse while copying");
									continue;
								}
								result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapData,dataToBeSpreadDocumentUIDRef );
										
								UIDList tempUIDListForItem(dataToBeSprayedDocumentDatabase1);
								dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&tempUIDListForItem,kFalse,kTrue);
								
								UIDList itemUIDList(dataToBeSprayedDocumentDatabase1);
								for(int32 i = 0; i < tempUIDListForItem.size() ; i++)
								{
									bool16 found = kFalse;
									for(int32 j = 0 ; j < allUIDList.size() ; j++)
									{
										if(tempUIDListForItem[i] == allUIDList[j])
										{
											found = kTrue;
											break;
										}
									}
									if(found == kFalse)
									{
										allUIDList.push_back(tempUIDListForItem[i]);
										itemUIDList.push_back(tempUIDListForItem[i]);
									}
								}
								StencilFileInfo stencilFileInfoObj;
								stencilFileInfoObj.path = subSectionListBoxParams.ItemStencilFilePath;
								stencilFileInfoObj.type = "I";
								stencilFileInfoObj.uidList = itemUIDList ;
								stencilFileInfo.push_back(stencilFileInfoObj);
							}
						}	
						if(subSectionListBoxParams.ProductStencilFilePath != "")
						{
							pathFound = kFalse;
							for(int itemFileNo = 0 ; itemFileNo < stencilFileInfo.size() ; itemFileNo++)
							{
								if(stencilFileInfo[itemFileNo].path ==  subSectionListBoxParams.ProductStencilFilePath)
								{
									pathFound = kTrue;
									break;
								}
							}
							if(pathFound == kFalse)
							{
//CA(subSectionListBoxParams.ProductStencilFilePath);							
								

								InterfacePtr<IPageItemScrapData>  scrapDataForProduct;

								result =  copyStencilsFromTheTemplateDocumentIntoScrapData(subSectionListBoxParams.ProductStencilFilePath, scrapDataForProduct);
								if(result == kFalse)
								{
									//CA("result == kFalse while copying");
									continue;
								}

								result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapDataForProduct,dataToBeSpreadDocumentUIDRef );

								dataToBeSprayedDocumentFrameUIDList1.Clear();

								UIDList temp2UIDList(dataToBeSprayedDocumentDatabase1);
								dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&temp2UIDList,kFalse,kTrue);

								UIDList productUIDList(dataToBeSprayedDocumentDatabase1);
								for(int32 i = 0; i < temp2UIDList.size() ; i++)
								{
									bool16 found = kFalse;
									for(int32 j = 0 ; j < allUIDList.size() ; j++)
									{
										if(temp2UIDList[i] == allUIDList[j])
										{
											found = kTrue;
											break;
										}
									}
									if(found == kFalse)
									{
										allUIDList.push_back(temp2UIDList[i]);
										productUIDList.push_back(temp2UIDList[i]);
									}
								}
								StencilFileInfo stencilFileInfoObj;
								stencilFileInfoObj.path = subSectionListBoxParams.ProductStencilFilePath;
								stencilFileInfoObj.type = "P";
								stencilFileInfoObj.uidList = productUIDList;
								stencilFileInfo.push_back(stencilFileInfoObj);
							}
						}
						if(subSectionListBoxParams.HybridTableStencilFilePath != "")
						{
							pathFound = kFalse;
							for(int itemFileNo = 0 ; itemFileNo < stencilFileInfo.size() ; itemFileNo++)
							{
								if(stencilFileInfo[itemFileNo].path == subSectionListBoxParams.HybridTableStencilFilePath )
								{
									pathFound = kTrue;
									break;
								}
							}
							if(pathFound == kFalse)
							{
//CA(subSectionListBoxParams.HybridTableStencilFilePath);
								InterfacePtr<IPageItemScrapData> scrapDataForHybridTable;
			//CA("subSectionListBoxParams.HybridTableStencilFilePath	"+subSectionListBoxParams.HybridTableStencilFilePath);
								result =  copyStencilsFromTheTemplateDocumentIntoScrapData(subSectionListBoxParams.HybridTableStencilFilePath, scrapDataForHybridTable);
								if(result == kFalse)
								{
									//CA("result == kFalse while copying");
									continue;
								}

								result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapDataForHybridTable,dataToBeSpreadDocumentUIDRef );
								if(result == kFalse)
								{
									//CA("result == kFalse while pasting");
									continue;
								}
								dataToBeSprayedDocumentFrameUIDList1.Clear();

								UIDList temp3UIDList(dataToBeSprayedDocumentDatabase1);
								dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&temp3UIDList,kFalse,kTrue);
								
								UIDList hybridUIDList(dataToBeSprayedDocumentDatabase1);
								for(int32 i = 0; i < temp3UIDList.size() ; i++)
								{
									bool16 found = kFalse;
									for(int32 j = 0 ; j < allUIDList.size() ; j++)
									{
										if(temp3UIDList[i] == allUIDList[j])
										{
											found = kTrue;
											break;
										}
									}
									if(found == kFalse)
									{
										
										allUIDList.push_back(temp3UIDList[i]);
										hybridUIDList.push_back(temp3UIDList[i]);
									}
								}

								StencilFileInfo stencilFileInfoObj;
								stencilFileInfoObj.path = subSectionListBoxParams.HybridTableStencilFilePath;
								stencilFileInfoObj.type = "H";
								stencilFileInfoObj.uidList = hybridUIDList;
								stencilFileInfo.push_back(stencilFileInfoObj);
							}
						}
						if(subSectionListBoxParams.SectionStencilFilePath != "")
						{
							pathFound = kFalse;
							for(int itemFileNo = 0 ; itemFileNo < stencilFileInfo.size() ; itemFileNo++)
							{
								if(stencilFileInfo[itemFileNo].path == subSectionListBoxParams.SectionStencilFilePath )
								{
									pathFound = kTrue;
									break;
								}
							}
							if(pathFound == kFalse)
							{
//CA(subSectionListBoxParams.SectionStencilFilePath);
								InterfacePtr<IPageItemScrapData> scrapDataForSection;
								result =  copyStencilsFromTheTemplateDocumentIntoScrapData(subSectionListBoxParams.SectionStencilFilePath , scrapDataForSection);
								if(result == kFalse)
								{
									//CA("result == kFalse while copying");
									continue;
								}
			
								result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapDataForSection,dataToBeSpreadDocumentUIDRef );
								if(result == kFalse)
								{
									//CA("result == kFalse while pasting");
									continue;
								}
								dataToBeSprayedDocumentFrameUIDList1.Clear();
								UIDList tempSectionStencilUIDList(dataToBeSprayedDocumentDatabase1);
								dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&tempSectionStencilUIDList,kFalse,kTrue);
								
								UIDList sectionUIDList(dataToBeSprayedDocumentDatabase1);
								for(int32 i = 0; i < tempSectionStencilUIDList.size() ; i++)
								{
									bool16 found = kFalse;
									for(int32 j = 0 ; j < allUIDList.size() ; j++)
									{
										if(tempSectionStencilUIDList[i] == allUIDList[j])
										{
											found = kTrue;
											break;
										}
									}
									if(found == kFalse)
									{
										allUIDList.push_back(tempSectionStencilUIDList[i]);
										sectionUIDList.push_back(tempSectionStencilUIDList[i]);
									}
								}

								StencilFileInfo stencilFileInfoObj;
								stencilFileInfoObj.path = subSectionListBoxParams.SectionStencilFilePath;
								stencilFileInfoObj.type = "S";
								stencilFileInfoObj.uidList = sectionUIDList;
								stencilFileInfo.push_back(stencilFileInfoObj);
							}
						}
					}//else end
				//}
			}
		}
/*PMString size("stencilFileInfo.size()");
size.AppendNumber(stencilFileInfo.size());
CA(size);*/
/////////////////////////////////////////////////////////////////////////////////////////////////////
		bool16 toggleFlag = kTrue;

		UIDRef originalPageUIDRef, originalSpreadUIDRef;
		bool16 result = kFalse;
		pageUidList.clear();
		allProductSprayed = kFalse;

		
		//CA_NUM("size : " ,pNodeDataList.size());
		
		
		//get current page UIDRef
		SubSectionSprayer subSectionSprayerObj;
		result = subSectionSprayerObj.getCurrentPage(originalPageUIDRef, originalSpreadUIDRef);
		if(result == kFalse){ 
			ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::getCurrentPage returns kFlase");
			return kFalse;;
		}
		
		PageCount = 1;

		UID originalPageUID = originalPageUIDRef.GetUID();
		pageUidList.push_back(originalPageUID);
		
		if(MediatorClass ::pageOrderSelectedIndex == 1) //spray should start from right page.
		{
			//::AddNewPage(); //Cs3
			Utils<ILayoutUIUtils>()->AddNewPage(); //Cs4
			PageCount = PageCount+ 1;
			UIDRef pageUIDRef;
			UIDRef spreadUIDRef;
			bool16 result = subSectionSprayerObj.getCurrentPage(pageUIDRef, spreadUIDRef);
			if(result == kFalse){
				ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::getCurrentPage returns kFalse");			
				return kFalse;;
			}

			pageUidList.push_back(pageUIDRef.GetUID());			
		}
		

		PMRect marginBoxBounds;
		result = subSectionSprayerObj.getMarginBounds(originalPageUIDRef, marginBoxBounds);
		if(result == kFalse)
		{
			result = subSectionSprayerObj.getPageBounds(originalPageUIDRef, marginBoxBounds);
			if(result == kFalse)
			{
				ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::getPageBounds returns kFalse");
				return kFalse;;
			}
		}
		
		PMString path(inddfilepath);
		sectionloopIndex = 0;
///////////////////////////////////////////////////////////////////////////////////////////////////
		sectionStencilUIDListToDelete.Clear();	//This is used to delete the section stencil When it is spray two times unnecessary

		for(int32 vectorElementIndex = 0;vectorElementIndex < subSectionsToBeSrpaydVectorSize;vectorElementIndex++)
		{
			subSectionSprayerListBoxParameters & subSectionListBoxParams = subSectionVector[vectorElementIndex];
			
			MediatorClass::sectionStencilUIDList.Clear();
			MediatorClass::itemStencilUIDList.Clear();
			MediatorClass::productStencilUIDList.Clear();
			MediatorClass::hybridTableStencilUIDList.Clear();
			MediatorClass ::allStencilUIDList.Clear();
	
			if(subSectionListBoxParams.isSelected == kTrue)
			{
				sectionloopIndex++;
				for(int sectionFileInfoIndex = 0 ; sectionFileInfoIndex < stencilFileInfo.size() ; sectionFileInfoIndex++)
				{
					if(subSectionListBoxParams.isSingleStencilFileForProductAndItem)
					{
						if(stencilFileInfo[sectionFileInfoIndex].path == subSectionListBoxParams.AllStencilFilePath /*&&  stencilFileInfoForItem[sectionFileInfoIndex].type == "S"*/)
							MediatorClass::allStencilUIDList = stencilFileInfo[sectionFileInfoIndex].uidList;
						else if(stencilFileInfo[sectionFileInfoIndex].path == subSectionListBoxParams.SectionStencilFilePath /*&&  stencilFileInfoForItem[sectionFileInfoIndex].type == "S"*/)
							MediatorClass::sectionStencilUIDList = stencilFileInfo[sectionFileInfoIndex].uidList;
					}
					else
					{
						if(stencilFileInfo[sectionFileInfoIndex].path == subSectionListBoxParams.ItemStencilFilePath/* &&  stencilFileInfoForItem[sectionFileInfoIndex].type == "I"*/)
							MediatorClass::itemStencilUIDList = stencilFileInfo[sectionFileInfoIndex].uidList;
						else if(stencilFileInfo[sectionFileInfoIndex].path == subSectionListBoxParams.ProductStencilFilePath /* &&  stencilFileInfoForItem[sectionFileInfoIndex].type == "P"*/)
							MediatorClass::productStencilUIDList = stencilFileInfo[sectionFileInfoIndex].uidList;
						else if(stencilFileInfo[sectionFileInfoIndex].path == subSectionListBoxParams.HybridTableStencilFilePath /*&&  stencilFileInfoForItem[sectionFileInfoIndex].type == "H"*/)
							MediatorClass::hybridTableStencilUIDList = stencilFileInfo[sectionFileInfoIndex].uidList;
						else if(stencilFileInfo[sectionFileInfoIndex].path == subSectionListBoxParams.SectionStencilFilePath /*&&  stencilFileInfoForItem[sectionFileInfoIndex].type == "S"*/)
							MediatorClass::sectionStencilUIDList = stencilFileInfo[sectionFileInfoIndex].uidList;
					}	
				}	

/*PMString SizeOfUIDLists("MediatorClass::itemStencilUIDList.size()");
SizeOfUIDLists.AppendNumber(MediatorClass::itemStencilUIDList.size());
SizeOfUIDLists.Append("\r");
SizeOfUIDLists.Append("MediatorClass::productStencilUIDList.size()");
SizeOfUIDLists.AppendNumber(MediatorClass::productStencilUIDList.size());
SizeOfUIDLists.Append("\r");
SizeOfUIDLists.Append("MediatorClass::hybridTableStencilUIDList.size()");
SizeOfUIDLists.AppendNumber(MediatorClass::hybridTableStencilUIDList.size());
SizeOfUIDLists.Append("\r");
SizeOfUIDLists.Append("MediatorClass::sectionStencilUIDList.size()");
SizeOfUIDLists.AppendNumber(MediatorClass::sectionStencilUIDList.size());
SizeOfUIDLists.Append("\r");
SizeOfUIDLists.Append("MediatorClass::allStencilUIDList.size()");
SizeOfUIDLists.AppendNumber(MediatorClass::allStencilUIDList.size());
CA(SizeOfUIDLists);
*/

				InterfacePtr<ISelectionManager> selectionManager(Utils<ISelectionUtils>()->QueryActiveSelection ());
				if (selectionManager == nil)
				{
					//CA("selectionManager == nil");
					break;
				}

				// Make a layout selection.
				InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
				//InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(Utils<ISelectionUtils>()->QuerySuite(IID_ILAYOUTSELECTION_ISUITE ),UseDefaultIID());
				if (layoutSelectionSuite == nil) 
				{
					//CA("layoutSelectionSuite == nil");
					return kFalse;; 
				}
			
				IDataBase * dataToBeSprayedDocumentDatabase = dataToBeSpreadDocumentUIDRef.GetDataBase();

				UIDList frameForThisSectionUIDList(dataToBeSprayedDocumentDatabase);
				if(MediatorClass::itemStencilUIDList.size()>0)
				{
					for(int32 indx =0 ; indx < MediatorClass::itemStencilUIDList.size() ; indx++ )
					{
						frameForThisSectionUIDList.push_back(MediatorClass::itemStencilUIDList[indx]);					
					}
				}
				if(MediatorClass::productStencilUIDList.size()>0)
				{
					for(int32 indx =0 ; indx < MediatorClass::productStencilUIDList.size() ; indx++ )
					{
						frameForThisSectionUIDList.push_back(MediatorClass::productStencilUIDList[indx]);					
					}
				}
				if(MediatorClass::hybridTableStencilUIDList.size()>0)
				{
					for(int32 indx =0 ; indx < MediatorClass::hybridTableStencilUIDList.size() ; indx++ )
					{
						frameForThisSectionUIDList.push_back(MediatorClass::hybridTableStencilUIDList[indx]);					
					}

				}
				if(MediatorClass::sectionStencilUIDList.size()>0)
				{
					for(int32 indx =0 ; indx < MediatorClass::sectionStencilUIDList.size() ; indx++ )
					{
						frameForThisSectionUIDList.push_back(MediatorClass::sectionStencilUIDList[indx]);					
					}
				}
				if(MediatorClass::allStencilUIDList.size()>0)
				{
					for(int32 indx =0 ; indx < MediatorClass::allStencilUIDList.size() ; indx++ )
					{
						frameForThisSectionUIDList.push_back(MediatorClass::allStencilUIDList[indx]);					
					}
				}
				layoutSelectionSuite->DeselectAllPageItems ();

				layoutSelectionSuite->/*Select*/SelectPageItems (frameForThisSectionUIDList,Selection::kReplace,Selection::kAlwaysCenterInView);
				
				fillpNodeDataList(subSectionListBoxParams.vec_ssd[0/*vectorNumber*/].subSectionID,
					subSectionListBoxParams.vec_ssd[0/*vectorNumber*/].sectionID,
					MediatorClass :: currentSelectedPublicationID,
					subSectionListBoxParams.vec_ssd[0/*vectorNumber*/].subSectionName);//Nov1TempSubSecID);						
							
				if(pNodeDataList.size() <= 0)
				{
					//CA("pNodeDataList.size() <= 0");
					continue;
				}


				double Nov1TempSecID = subSectionListBoxParams.vec_ssd[0/*vectorNumber*/].sectionID;
				double Nov1TempSubSecID = subSectionListBoxParams.vec_ssd[0/*vectorNumber*/].subSectionID;
				PMString Nov1TempSubSecName = subSectionListBoxParams.vec_ssd[0/*vectorNumber*/].subSectionName;
				currentSelectedSpreadCount = subSectionListBoxParams .spreadCount ; //7-may chetan

				CurrentSelectedSection = Nov1TempSecID;

				CurrentSelectedPublicationID = MediatorClass :: currentSelectedPublicationID;
				CurrentSelectedSubSection = Nov1TempSubSecID;
				global_project_level = MediatorClass :: global_project_level;
				global_lang_id =MediatorClass ::global_lang_id;

				MediatorClass ::subSecSpraySttngs = subSectionListBoxParams.ssss;
				
				MediatorClass ::currentProcessingDocUIDRef = dataToBeSpreadDocumentUIDRef;
				
				SubSectionSprayer SSsp;
				SSsp.selectedSubSection = Nov1TempSubSecName;

				MediatorClass ::vec_SubSecData = subSectionListBoxParams.vec_ssd;	
				
				bool16 isCancelHit = kFalse;
				SSsp.startSprayingSubSectionNew(toggleFlag,marginBoxBounds,isCancelHit);

				if(sectionloopIndex == 1)
				{
					double tempsectionID = subSectionListBoxParams.vec_ssd[0].sectionID;

					CPubModel pubModel = ptrIAppFramework->getpubModelByPubID(tempsectionID ,MediatorClass :: global_lang_id);
					pubModel = ptrIAppFramework->getpubModelByPubID(pubModel.getRootID() ,MediatorClass :: global_lang_id);
				
					PMString Nov1TempDocSaveAsName = pubModel.getName();//subSectionListBoxParams.vec_ssd[0/*vectorNumber*/].sectionID//= subSectionListBoxParams.displayName;
					
					path.Append(Nov1TempDocSaveAsName);
					path.Append(".indd");
				}
				
				if(isCancelHit == kTrue)
					break;
			}
		}	
		
		deleteStartPageOfTheDocument(originalPageUIDRef);

		SDKUtilities sdkutil;
	
		IDFile docFile = sdkutil.PMStringToSysFile(&path);
		ErrorCode status = sdklhelp.SaveDocumentAs(dataToBeSpreadDocumentUIDRef,docFile);				
		if(status == kFailure)
		{
			CA("document can not be saved");
			return kTrue;
		}
		sdklhelp.CloseDocument(dataToBeSpreadDocumentUIDRef,kTrue);

		contentFileList.push_back(docFile);
		if(contentFileList.size()>0)
		{
			addDocumentsToBook(contentFileList);
		}
	}
	return kTrue;
}



bool16 startCreatingMediaForSpecSheet()
{
	//CA("startCreatingMediaForSpecSheet");

	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
		return kFalse;


	InterfacePtr<IBookManager> bookManager(/*gSession*/GetExecutionContextSession(), UseDefaultIID()); //Cs4
	if (bookManager == nil) 
	{ 
		ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaForSpecSheet::bookManager == nil");
		return kFalse; 
	}
	
	IBook * activeBook = bookManager->GetCurrentActiveBook();
	if(activeBook == nil)
	{
		ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaForSpecSheet::activeBook == nil");
		return kFalse;			
	}
	//To get Actual File name in the file system.
	//IDFile file = activeBook->GetBookFileSpec();

	SDKUtilities sdkutils;
    SDKFileHelper activeBookFileHelper(activeBook->GetBookFileSpec());
    PMString inddfilepath = activeBookFileHelper.GetPath();

	sdkutils.RemoveLastElement(inddfilepath);
	sdkutils.AppendPathSeparator(inddfilepath);	
	//CA("inddfilepath : " + inddfilepath);

	K2Vector<IDFile> contentFileList;
		
	vector<subSectionSprayerListBoxParameters> & subSectionVector = MediatorClass ::vector_subSectionSprayerListBoxParameters;
	int32/*int64*/ subSectionsToBeSrpaydVectorSize =static_cast<int32> (subSectionVector.size()); //Cs4
	
	/*PMString subSectionsToBeSrpaydVectorSizeStr("subSectionsToBeSrpaydVectorSize = ");
	subSectionsToBeSrpaydVectorSizeStr.AppendNumber(subSectionsToBeSrpaydVectorSize);
	CA(subSectionsToBeSrpaydVectorSizeStr);*/

	//DoCMMExportOptionsDialog();

	if(subSectionsToBeSrpaydVectorSize > 0)
	{//start if subSectionsToBeSrpaydVectorSize > 0

		bool16 isCanceled = kFalse;
		SubSectionSprayer SSsp;

		for(int32 vectorElementIndex = 0;vectorElementIndex < subSectionsToBeSrpaydVectorSize;vectorElementIndex++)
		{//start for z
			subSectionSprayerListBoxParameters & subSectionListBoxParams = subSectionVector[vectorElementIndex];
			
			if(subSectionListBoxParams.isSelected == kTrue)
			{
				//CA("subSectionListBoxParams.isSelected == kTrue");	
				//start if x
				//following is the path of master file i.e. templateFilePath
                
				//-PMString masterFilePath = subSectionListBoxParams.masterFileWithCompletePath;
				//-masterFilePath.SetTranslatable(kFalse);
				PMString productStencilFilePath = subSectionListBoxParams.ProductStencilFilePath;
				productStencilFilePath.SetTranslatable(kFalse);

				//-if(masterFilePath == "")
				//-{
				//-	CAlert::InformationAlert("Select master file.");
				//-	return kFalse;
				//-}

				if(productStencilFilePath == "")
				{
					CAlert::InformationAlert("Select template files to spray.");
					return kFalse;
				}

				///added by Tushar on 12/03/2009

				SDKLayoutHelper sdklhelp;
				PMString StencilFilePath(productStencilFilePath);
				StencilFilePath.SetTranslatable(kFalse);
				IDFile templateIDFile(StencilFilePath);
				UIDRef templateDocUIDRef = sdklhelp.OpenDocument(templateIDFile);
				if(templateDocUIDRef == UIDRef ::gNull)
				{
					CA("uidref of templatedoc is invalid");
					break;
				}

				ErrorCode err = sdklhelp.OpenLayoutWindow(templateDocUIDRef);
				if(err == kFailure)
				{
					//CA("Error occured while opening the layoutwindow of the template");
					break;
				}

				
				InterfacePtr<IDocument> templatedoc(templateDocUIDRef,UseDefaultIID());
				if(templatedoc == nil)
				{
					//CA("templatedoc == nil");
					break;
				}
                
                IDataBase * templateDocDatabase = templateDocUIDRef.GetDataBase();
                if(templateDocDatabase == nil)
                {
                    //CA("templateDocDatabase == nil");
                    break;
                }

                
				InterfacePtr<ISpreadList>templateSpreadUIDList(templatedoc,UseDefaultIID());
				if(templateSpreadUIDList == nil)
				{
					//CA("templateSpreadUIDList == nil");
					break;
				}
               
				
                int32 spreadCount = templateSpreadUIDList->GetSpreadCount();
                UIDList templateFrameUIDList(templateDocDatabase);
                
                for(int32 spreadIndex = 0; spreadIndex < spreadCount; spreadIndex++)
                {
                
                    UIDRef templateDocFirstSpreadUIDRef(templateDocDatabase, templateSpreadUIDList->GetNthSpreadUID(spreadIndex));
                    InterfacePtr<ISpread> templateSpread(templateDocFirstSpreadUIDRef, IID_ISPREAD);
                    if(templateSpread == nil)
                    {
                        //CA("templateSpread == nil");
                        break;
                    }
                    
                    if(templateSpread->GetNthPageUID(0)== kInvalidUID)
                    {
                        //CA("pageUID is invalid");
                        break;
                    }
                    int numPages=templateSpread->GetNumPages();
                    for(int32 pageIndex = 0; pageIndex < numPages; pageIndex++)
                    {
                        if(templateSpread->GetNthPageUID(pageIndex)== kInvalidUID)
                        {
                            //CA("pageUID is invalid");
                            continue;
                        }
                        UIDList templatePageFrameUIDList(templateDocDatabase);
                        templateSpread->GetItemsOnPage(pageIndex,&templatePageFrameUIDList,kFalse,kTrue);
                        templateFrameUIDList.Append(templatePageFrameUIDList);
                    }
                }
				
				CSprayStencilInfoVector.clear();
				CSprayStencilInfo objCSprayStencilInfo;

				UIDList temptemplateFrameUIDList(templateFrameUIDList.GetDataBase());
				temptemplateFrameUIDList = templateFrameUIDList;

				SSsp.getAllBoxIdsForGroupFrames(temptemplateFrameUIDList);


				bool16 result1 = SSsp.getStencilInfo(/*templateFrameUIDList*/temptemplateFrameUIDList,objCSprayStencilInfo);
				if(result1 == kFalse)
					break;

				CSprayStencilInfoVector.push_back(objCSprayStencilInfo);
				
				//og was commented
				//sdklhelp.CloseDocument(templateDocUIDRef,kFalse,K2::kSuppressUI, kFalse);
				
				GetSectionData getSectionData;
                PMString itemFieldIds("");
                PMString itemAssetTypeIds("");
                PMString itemGroupFieldIds("");
                PMString itemGroupAssetTypeIds("");
                PMString listTypeIds("");
                PMString listItemFieldIds("");
                PMString itemAttributeGroupIds("");
                bool16 isSprayItemPerFrameFlag = kFalse;
                
                double langId = ptrIAppFramework->getLocaleId();
		
				vectorCSprayStencilInfo::iterator itr;
				for(itr = CSprayStencilInfoVector.begin();itr != CSprayStencilInfoVector.end(); itr++)
				{
					if(itr->isCopy)
						getSectionData.addCopyFlag = kTrue;
					if(itr->isProductCopy)
						getSectionData.addProductCopyFlag = kTrue;
					if(itr->isSectionCopy)
					{
						getSectionData.addSectionLevelCopyAttrFlag = kTrue;
						getSectionData.addPublicationLevelCopyAttrFlag = kTrue;
						getSectionData.addCatagoryLevelCopyAttrFlag = kTrue;
					}

					if(itr->isAsset)
						getSectionData.addImageFlag = kTrue;
					if(itr->isProductAsset)
						getSectionData.addProductImageFlag = kTrue;
					
					if(itr->isAsset && !itr->isCopy)
						getSectionData.addCopyFlag = kTrue;
			
					if(itr->isProductAsset && !itr->isProductCopy)
						getSectionData.addProductCopyFlag = kTrue;

					if(itr->isBMSAssets)
					{
						getSectionData.addImageFlag = kTrue;
						getSectionData.addItemBMSAssetsFlag = kTrue;
					}
					if(itr->isProductBMSAssets)
					{
						getSectionData.addImageFlag = kTrue;
						getSectionData.addProductBMSAssetsFlag = kTrue;
					}
					if(itr->isSectionLevelBMSAssets)
						getSectionData.addPubLogoAssetFlag = kTrue;

				
					if(itr->isDBTable)
					{
						getSectionData.addDBTableFlag = kTrue;
						//getSectionData.addProductDBTableFlag = kTrue; //for Item Group Lists spray
						getSectionData.addChildCopyAndImageFlag = kTrue;		
					}
					if(itr->isProductDBTable)
					{
						getSectionData.addProductDBTableFlag = kTrue;
						getSectionData.addChildCopyAndImageFlag = kTrue;		
					}
					if(itr->isCustomTablePresent)
						getSectionData.addCustomTablePresentFlag = kTrue;
							
					
					if(itr->isItemPVMPVAssets)
					{
						getSectionData.addItemPVMPVAssetFlag = kTrue;
						if(itr->itemPVAssetIdList.size()>0)
							for(int32 i = 0; i < itr->itemPVAssetIdList.size(); i++)
								getSectionData.itemPVAssetIdList.push_back(itr->itemPVAssetIdList[i]);				
					}
					if(itr->isProductPVMPVAssets)
					{
						getSectionData.addProductPVMPVAssetFlag = kTrue;
						if(itr->productPVAssetIdList.size()>0)
							for(int32 i = 0; i < itr->productPVAssetIdList.size(); i++)
								getSectionData.productPVAssetIdList.push_back(itr->productPVAssetIdList[i]);	
					}
					if(itr->isSectionPVMPVAssets)
					{
						getSectionData.addSectionPVMPVAssetFlag = kTrue;
						if(itr->sectionPVAssetIdList.size()>0)
							for(int32 i = 0; i < itr->sectionPVAssetIdList.size(); i++)
								getSectionData.sectionPVAssetIdList.push_back(itr->sectionPVAssetIdList[i]);	
					}
					if(itr->isPublicationPVMPVAssets)
					{
						getSectionData.addPublicationPVMPVAssetFlag = kTrue;
						if(itr->publicationPVAssetIdList.size()>0)
							for(int32 i = 0; i < itr->publicationPVAssetIdList.size(); i++)
								getSectionData.publicationPVAssetIdList.push_back(itr->publicationPVAssetIdList[i]);	
					}
					if(itr->isCatagoryPVMPVAssets)
					{
						getSectionData.addCatagoryPVMPVAssetFlag = kTrue;
						if(itr->catagoryPVAssetIdList.size()>0)
							for(int32 i = 0; i < itr->catagoryPVAssetIdList.size(); i++)
								getSectionData.catagoryPVAssetIdList.push_back(itr->catagoryPVAssetIdList[i]);	
					}
					
					if(itr->isCategoryImages)
					{
						getSectionData.addCategoryImages = kTrue;
						if(itr->categoryAssetIdList.size()>0)
							for(int32 i = 0; i < itr->categoryAssetIdList.size(); i++)
								getSectionData.categoryAssetIdList.push_back(itr->categoryAssetIdList[i]);	
					}

					if(itr->isEventSectionImages)
					{
						getSectionData.addEventSectionImages = kTrue;
						if(itr->eventSectionAssetIdList.size()>0)
							for(int32 i = 0; i < itr->eventSectionAssetIdList.size(); i++)
								getSectionData.eventSectionAssetIdList.push_back(itr->eventSectionAssetIdList[i]);	
					}
							
					if(itr->isHyTable)
						getSectionData.addHyTableFlag = kTrue;
					if(itr->isProductHyTable)
						getSectionData.addHyTableFlag = kTrue;
					if(itr->isSectionLevelHyTable)
						getSectionData.addHyTableFlag = kTrue;

					if(itr->isChildTag)
						getSectionData.addChildCopyAndImageFlag = kTrue;

					if(itr->isProductChildTag)
						getSectionData.addProductChildCopyAndImageFlag = kTrue;

					if(itr->isEventField)
						getSectionData.isEventField = kTrue;
                    
                    // Added by Apsiva for New JSON section Call.
                    // collecting listTypes for Item Group and iTem
                    if(  itr->dBTypeIds.size() > 0)
                    {
                        for(int ct =0; ct < itr->dBTypeIds.size(); ct++ )
                        {
                            listTypeIds.AppendNumber(PMReal(itr->dBTypeIds.at(ct)));
                            if(ct != (itr->dBTypeIds.size() -1) )
                            {
                                listTypeIds.Append(",");
                            }
                        }
                    }
                    // collecting Copy Attributes for Item Group
                    if(  itr->ProductAttributeIds.size() > 0)
                    {
                        for(int ct =0; ct < itr->ProductAttributeIds.size(); ct++ )
                        {
                            itemGroupFieldIds.AppendNumber(PMReal(itr->ProductAttributeIds.at(ct)));
                            if(ct != (itr->ProductAttributeIds.size() -1) )
                            {
                                itemGroupFieldIds.Append(",");
                            }
                        }
                    }
                    
                    if(itr->productPVAssetIdList.size()>0)
                    {
                        if(itemGroupFieldIds.NumUTF16TextChars () > 0)
                        {
                            itemGroupFieldIds.Append(",");
                        }
                        
                        for(int32 i = 0; i < itr->productPVAssetIdList.size(); i++)
                        {
                            itemGroupFieldIds.AppendNumber(PMReal(itr->productPVAssetIdList[i]));
                            if( i < itr->productPVAssetIdList.size() -1 )
                                itemGroupFieldIds.Append(",");
                        }
                    }
                    
                    // collecting Copy Attributes for Item
                    if(  itr->itemAttributeIds.size() > 0)
                    {
                        for(int ct =0; ct < itr->itemAttributeIds.size(); ct++ )
                        {
                            itemFieldIds.AppendNumber(PMReal(itr->itemAttributeIds.at(ct)));
                            if(ct != (itr->itemAttributeIds.size() -1) )
                            {
                                itemFieldIds.Append(",");
                            }
                        }
                    }
                    
                    if(  itr->itemAttributeGroupIds.size() > 0)
                    {
                        for(int ct =0; ct < itr->itemAttributeGroupIds.size(); ct++ )
                        {
                            itemAttributeGroupIds.AppendNumber(PMReal(itr->itemAttributeGroupIds.at(ct)));
                            if(ct != (itr->itemAttributeGroupIds.size() -1) )
                            {
                                itemAttributeGroupIds.Append(",");
                            }
                        }
                    }
                    
                    if(itr->itemPVAssetIdList.size()>0)
                    {
                        if(itemFieldIds.NumUTF16TextChars () > 0)
                        {
                            itemFieldIds.Append(",");
                        }
                        
                        for(int32 i = 0; i < itr->itemPVAssetIdList.size(); i++)
                        {
                            itemFieldIds.AppendNumber(PMReal(itr->itemPVAssetIdList[i]));
                            if( i < itr->itemPVAssetIdList.size() -1 )
                                itemFieldIds.Append(",");	
                        }
                    }
                    // collecting Asset type Ids for Item Group
                    if(  itr->ProductAssetIds.size() > 0)
                    {
                        for(int ct =0; ct < itr->ProductAssetIds.size(); ct++ )
                        {
                            itemGroupAssetTypeIds.AppendNumber(PMReal(itr->ProductAssetIds.at(ct)));
                            if(ct != (itr->ProductAssetIds.size() -1) )
                            {
                                itemGroupAssetTypeIds.Append(",");
                            }
                        }
                    }
                    // collecting Asset type Ids for Item
                    if(  itr->itemAssetIds.size() > 0)
                    {
                        for(int ct =0; ct < itr->itemAssetIds.size(); ct++ )
                        {
                            itemAssetTypeIds.AppendNumber(PMReal(itr->itemAssetIds.at(ct)));
                            if(ct != (itr->itemAssetIds.size() -1) )
                            {
                                itemAssetTypeIds.Append(",");
                            }
                        }
                    }
                    
                    // collecting listItemFieldIds for Item
                    if(  itr->childItemAttributeIds.size() > 0)
                    {
                        for(int ct =0; ct < itr->childItemAttributeIds.size(); ct++ )
                        {
                            listItemFieldIds.AppendNumber(PMReal(itr->childItemAttributeIds.at(ct)));
                            if(ct != (itr->childItemAttributeIds.size() -1) )
                            {
                                listItemFieldIds.Append(",");
                            }
                        }
                    }
                    //langId = itr->langId;
                    
                    if(isSprayItemPerFrameFlag == kFalse)
                    {
                        isSprayItemPerFrameFlag = itr->isSprayItemPerFrame;
                    }


				}

				if(getSectionData.addChildCopyAndImageFlag)
				{
					getSectionData.addDBTableFlag = kTrue;
					getSectionData.addProductDBTableFlag = kTrue;
				}

				if(getSectionData.addProductChildCopyAndImageFlag)
				{
					getSectionData.addCustomTablePresentFlag = kTrue;
				}
						
				getSectionData.isOneSource = kFalse;
				
				getSectionData.isGetWholePublicationOrCatagoryDataFlag = kTrue;
				
				getSectionData.addComponentTableFlag = kFalse;
				getSectionData.addAccessoryTableFlag = kFalse;
				getSectionData.addXRefTableFlag = kFalse;
				
				/*getSectionData.addSectionLevelCopyAttrFlag = kFalse;
				getSectionData.addPublicationLevelCopyAttrFlag = kFalse;
				getSectionData.addCatagoryLevelCopyAttrFlag = kFalse;*/

		
				getSectionData.SectionId = subSectionListBoxParams.vec_ssd[0].subSectionID;
				getSectionData.PublicationId = MediatorClass::currentSelectedPublicationID ;

				InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
				if(ptrIClientOptions==nil)
				{
					//CAlert::ErrorAlert("Interface for IClientOptions not found.");
					break;
				}

				PMString language_name("");
				getSectionData.languageId = ptrIClientOptions->getDefaultLocale(language_name);

				//getSectionData.languageId = 1;
				getSectionData.CatagoryId = -1;


				
				getSectionData.itemIdList.push_back(1);
				getSectionData.productIdList.push_back(-1);
				getSectionData.hybridIdList.push_back(-1);


				UIDRef dataToBeSpreadDocumentUIDRef	;			
				PMString path(inddfilepath);	
				
				PMString sectionName = subSectionListBoxParams.displayName;
				sectionName.SetTranslatable(kFalse);
				//CA("sectionName : " + sectionName);
				
				path.Append(/*sectionName*/keepOnlyAlphaNumeric(sectionName));
				SDKUtilities sdkutils;
				sdkutils.AppendPathSeparator(path);
				//CA("Path  : " + path);
				FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&path), kTrue);

				bool16 result;
				//SDKLayoutHelper sdklhelp;

				//-SDKFileHelper masterFile(masterFilePath);
				//-if(masterFilePath == "" || masterFile.IsExisting() == kFalse)
				//-{
				//-	CAlert::InformationAlert("Master file : " + masterFilePath + " not found.");
				//-	continue;
				//-}

				fillpNodeDataList(subSectionListBoxParams.vec_ssd[0].subSectionID,
						subSectionListBoxParams.vec_ssd[0].sectionID,
						MediatorClass :: currentSelectedPublicationID,
						subSectionListBoxParams.vec_ssd[0].subSectionName);//Nov1TempSubSecID);						
					
				if(pNodeDataList.size() <= 0)
				{
					continue;
				}
				int32/*int64*/ prodCount = static_cast<int32>(pNodeDataList.size()); //Cs4

				PMString title("Spraying Section: ");
				title.SetTranslatable(kFalse);
				PMString temp = ("");
				temp.Append(subSectionListBoxParams.vec_ssd[0].subSectionName);
				title += temp;
				RangeProgressBar progressBar(title, 0, prodCount, kTrue);
				PMString str1("Spraying Families");
				str1.SetTranslatable(kFalse);

				PMString str2("Spraying Families");
				str2.SetTranslatable(kFalse);
				progressBar.SetTaskText(str2);

				PMString str3("Retriving Data From Server ...");
				str3.SetTranslatable(kFalse);
				progressBar.SetTaskText(str3);
				
				AcquireWaitCursor awc ; //og
				awc.Animate(); //og 

				//-ptrIAppFramework->GetSectionData_getDataForPubOrCat(getSectionData);
                
                ptrIAppFramework->clearAllStaticObjects();

                ptrIAppFramework->EventCache_setCurrentSectionData( subSectionListBoxParams.vec_ssd[0].subSectionID, langId , "" , "" , itemFieldIds, itemAssetTypeIds, itemGroupFieldIds,itemGroupAssetTypeIds,listTypeIds, listItemFieldIds, kFalse, isSprayItemPerFrameFlag, itemAttributeGroupIds);

				CSprayStencilInfoVector.clear();

				for(int32 prodIndex = 0; prodIndex < pNodeDataList.size(); prodIndex++)
				{
					/// Clear all scheduled commands before going to next document. **********************************************************************************
					if(prodIndex > 0)
						CmdUtils::ProcessScheduledCmds (ICommand::kLowestPriority);  

					PMString tempString("Spraying ");
					tempString +=pNodeDataList[prodIndex].getName();
					tempString += "...";
					tempString.SetTranslatable(kFalse);
					progressBar.SetTaskText(tempString);
					progressBar.SetPosition(prodIndex);
										
					PMString pubname(pNodeDataList[prodIndex].getName());
									
					PMString tempDocSaveAsName("");
					tempDocSaveAsName.SetTranslatable(kFalse);

                    /*-
					PMString * strptr; 
					if(pNodeDataList[prodIndex].getIsProduct() == 0)
					{
						strptr = pubname.Substring(0,pubname.IndexOfCharacter(':') - 1 );
						if(strptr == NULL)
							continue;
					}
					else
					{
						strptr = pubname.Substring(0,pubname.IndexOfCharacter(':'));
						if(strptr == NULL)
							continue;
					}					
                    
					tempDocSaveAsName = keepOnlyAlphaNumeric(*strptr);
                    
					if(strptr)
						delete strptr;
					//tempDocSaveAsName.Append(*strptr);
                     
                    -*/
                    
                    tempDocSaveAsName = keepOnlyAlphaNumeric(pubname);

                    //CA("tempDocSaveAsName = "+tempDocSaveAsName);

														
					PMString folderPath("");
					folderPath.SetTranslatable(kFalse);
					folderPath.Append(path);
					folderPath.Append(tempDocSaveAsName);
					folderPath.Append(".indd");	

					/*PMString pdfFfolderPath("");
					pdfFfolderPath.Append(path);
					pdfFfolderPath.Append(tempDocSaveAsName);
					pdfFfolderPath.Append(".pdf");*/	

					/*-
                    InterfacePtr<IPageItemScrapData>  scrapData;
					result =  copyStencilsFromTheTemplateDocumentIntoScrapData(subSectionListBoxParams.ProductStencilFilePath, scrapData);
					if(result == kFalse)
					{
						ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::startCreatingMediaNew::result == kFalse while copying");
						continue;
					}
                     -*/
	
					PMString filePathItemsToBePastedIn(/*-masterFilePath-*/ productStencilFilePath);
					filePathItemsToBePastedIn.SetTranslatable(kFalse);
					IDFile dataToBeSpreadDocumentIDFile(filePathItemsToBePastedIn);
					dataToBeSpreadDocumentUIDRef = sdklhelp.OpenDocument(dataToBeSpreadDocumentIDFile);
					if(dataToBeSpreadDocumentUIDRef == UIDRef ::gNull)
					{
						CA("uidref of dataToBeSpreadDocumentUIDRef is invalid");
						continue;
					}						
				
					ErrorCode err = sdklhelp.OpenLayoutWindow(dataToBeSpreadDocumentUIDRef);
					if(err == kFailure)
					{
						CA("Error occured while opening the layoutwindow of the dataToBeSprayedDocumentFileLayout");
						continue;
					}		

                    /*-
					result = pasteTheItemsFromScrapDataOntoOpenDocument(scrapData,dataToBeSpreadDocumentUIDRef );
                    -*/
				
					//following code is added to collect uidlist from master page for allStencilUIDList-----------from here	
					InterfacePtr<IDocument> dataToBeSprayedDocument1(dataToBeSpreadDocumentUIDRef,UseDefaultIID());
					if(dataToBeSprayedDocument1 == nil)
					{
						//CA("dataToBeSprayedDocument1 == nil");
						break;
					}

					InterfacePtr<ISpreadList>dataToBeSprayedDocumentSpreadList1(dataToBeSprayedDocument1,UseDefaultIID());
					if(dataToBeSprayedDocumentSpreadList1 == nil)
					{
						//CA("dataToBeSprayedDocumentSpreadList1 == nil");
						continue;
					}

					IDataBase * dataToBeSprayedDocumentDatabase1 = dataToBeSpreadDocumentUIDRef.GetDataBase();
					if(dataToBeSprayedDocumentDatabase1 == nil)
					{
						//CA("dataToBeSprayedDocumentDatabase1 == nil");
						continue;
					}
					
					MediatorClass::allStencilUIDList.Clear();
					
					UIDRef dataToBeSprayedDocumentFirstSpreadUIDRef1(dataToBeSprayedDocumentDatabase1, dataToBeSprayedDocumentSpreadList1->GetNthSpreadUID(0)); 

					InterfacePtr<ISpread> dataToBeSprayedDocumentFirstSpread1(dataToBeSprayedDocumentFirstSpreadUIDRef1, IID_ISPREAD); 
					if(dataToBeSprayedDocumentFirstSpread1 == nil)
					{
						//CA("spread == nil");
						continue;
					}

					UIDList dataToBeSprayedDocumentFrameUIDList1(dataToBeSprayedDocumentDatabase1);
					if(dataToBeSprayedDocumentFirstSpread1->GetNthPageUID(0)== kInvalidUID) 
					{
						//CA("pageUID is invalid");
						continue;
					}		
		
					//In following code we are getting UIDList for all items on master page and storing it in mediator class
					//variable MediatorClass ::allStencilUIDList
					UIDList tempUIDListForALL(dataToBeSprayedDocumentDatabase1);
					//-dataToBeSprayedDocumentFirstSpread1->GetItemsOnPage(0,&tempUIDListForALL,kFalse,kTrue);
                    
                    for(int numSp=0; numSp< dataToBeSprayedDocumentSpreadList1->GetSpreadCount(); numSp++)
                    {
                        UIDRef spreadUIDRef(dataToBeSprayedDocumentDatabase1, dataToBeSprayedDocumentSpreadList1->GetNthSpreadUID(numSp));
                        
                        InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
                        if(!spread)
                        {
                            ptrIAppFramework->LogDebug("AP7_RefreshContent::Refresh::getDocumentSelectedBoxIds::!spread");
                            return kFalse;
                        }
                        int numPages=spread->GetNumPages();
                        
                        for(int i=0; i<numPages; i++)
                        {
                            UIDList tempList(dataToBeSprayedDocumentDatabase1);
                            spread->GetItemsOnPage(i, &tempList, kFalse);
                            tempUIDListForALL.Append(tempList);
                        }
                    }
                    MediatorClass::allStencilUIDList.Clear();
					if(MediatorClass::allStencilUIDList.Length() == 0)
						MediatorClass::allStencilUIDList = tempUIDListForALL;
					else
					{
						MediatorClass::allStencilUIDList.Append(tempUIDListForALL);
					}
			
					/*PMString size("MediatorClass ::allStencilUIDList = ");
					size.AppendNumber(MediatorClass::allStencilUIDList.Length());
					CA(size);*/

                    /*-
					if(result == kFalse)
						continue;
                     -*/

					InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection());
					if(iSelectionManager == nil)
					{
						CA("iSelectionManger == nil");
						continue;
					}


					//global variable value assignments
					
					double TempSecID = subSectionListBoxParams.vec_ssd[0].sectionID;
					double TempSubSecID = subSectionListBoxParams.vec_ssd[0].subSectionID;
					PMString TempSubSecName = subSectionListBoxParams.vec_ssd[0].subSectionName;
					currentSelectedSpreadCount = subSectionListBoxParams.spreadCount;
					
					CurrentSelectedSection = TempSecID;
					CurrentSelectedPublicationID = MediatorClass::currentSelectedPublicationID;
					CurrentSelectedSubSection = TempSubSecID;
					global_project_level = MediatorClass :: global_project_level;
					global_lang_id =MediatorClass ::global_lang_id;

					MediatorClass ::currentProcessingDocUIDRef = dataToBeSpreadDocumentUIDRef;
			
					SubSectionSprayer SSsp;
					SSsp.selectedSubSection = TempSubSecName;
					SSsp.selectUIDList = MediatorClass::allStencilUIDList;
					SSsp.sprayedProductIndex = prodIndex;

					/*CSprayStencilInfo objCSprayStencilInfo;
					result = SSsp.getStencilInfo(MediatorClass::allStencilUIDList,objCSprayStencilInfo);
					if(result == kFalse)
						break;

					CSprayStencilInfoVector.push_back(objCSprayStencilInfo);*/

					InterfacePtr<ISelectionManager> selectionManager(Utils<ISelectionUtils>()->QueryActiveSelection ());
					if (selectionManager == nil)
					{
						//CA("selectionManager == nil");
						break;
					}

					// Deselect everything.
					selectionManager->DeselectAll(nil); // deselect every active CSB
					// Make a layout selection.
					InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
				//	InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(Utils<ISelectionUtils>()->QuerySuite(IID_ILAYOUTSELECTION_ISUITE ),UseDefaultIID());
					if (layoutSelectionSuite == nil) 
					{
						CA("layoutSelectionSuite == nil");
						continue; 
					}
			
					layoutSelectionSuite->SelectPageItems(MediatorClass::allStencilUIDList,Selection::kReplace,Selection::kAlwaysCenterInView);
					


					SSsp.startSprayingSubSectionForSpecSheet();

					SDKUtilities sdkutils;
					IDFile docFile = sdkutils.PMStringToSysFile(&folderPath);
									
					progressBar.DisableChildProgressBars(kTrue);


					ErrorCode status = sdklhelp.SaveDocumentAs(dataToBeSpreadDocumentUIDRef,docFile);				
					if(status == kFailure)
					{
						CA("document can not be saved");
						continue;
					}

					status = kFailure;//og
					//status = sdklhelp.CloseDocument(dataToBeSpreadDocumentUIDRef,kTrue);//og
					status = sdklhelp.CloseDocument(dataToBeSpreadDocumentUIDRef,kFalse);//og
					//CA("CLOSED");
					if(status == kFailure)//og
					{//og
						CA("document can not closed");//og
						continue;//og
					}	//og									
					
					progressBar.DisableChildProgressBars(kFalse);

					contentFileList.push_back(docFile);	

					if(progressBar.WasCancelled(kFalse))
					{
						//CA("isCanceled");
						isCanceled = kTrue;
						break;
					}					
				}
				ptrIAppFramework->EventCache_clearCurrentSectionData();
				//sdklhelp.CloseDocument(templateDocUIDRef,kFalse,K2::kSuppressUI,kFalse); //og
				sdklhelp.CloseDocument(templateDocUIDRef,kFalse);
			}
			if(isCanceled == kTrue)
				break;
		}

		if(contentFileList.size()>0)
		{
			//CA("addDocumentsToBook........");
			addDocumentsToBook(contentFileList);
		}	
		
		return kTrue;
	}	

	return kFalse;
}

void DoCMMExportOptionsDialog()
{
	//CA("DoCMMExportOptionsDialog 1");
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if (application == nil) 
		{	
			CA("application == nil");
			break;
		}
		//CA("DoCMMExportOptionsDialog 2");
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		if (dialogMgr == nil) 
		{
			CA("dialogMgr == nil");
			break;
		}
		//CA("DoCMMExportOptionsDialog 3");
		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kCMMPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			kCMMExportOptionsDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);
		//CA("DoCMMExportOptionsDialog 4");
		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog * dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal,kTrue,kFalse);
		//CA("DoCMMExportOptionsDialog 4.1");		
		if (dialog == nil) 
		{
			CA("dialog == nil");
			break;
		}
		//CA("DoCMMExportOptionsDialog 5");
		// Open the dialog.
		dialog->Open();
	
	} while (false);
	//CA("DoCMMExportOptionsDialog 6");
}

void CMMDialogObserver::populateSubSectionListForSectionLevel(bool16 shall_I_Fill_Data_Into_ListBox,double sectionId)
{
/*-
	no_of_lstboxElements=0;// declared globally on 03-october
	//CA("1 Inside SPSelectionObserver::populateSectionDropDownList");
	//noOfRows = 0;	
	do
	{
		SDKListBoxHelper listHelper(this, kCMMPluginID, kCMMMTemplateFileListBoxWidgetID, kCMMDialogWidgetID);
		if(shall_I_Fill_Data_Into_ListBox)
		{

			listHelper.EmptyCurrentListBox();
		}
		//yet to define
		MediatorClass :: vector_subSectionSprayerListBoxParameters.clear();
		//MediatorClass ::PublicationTemplateMappingListBox

		
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
		{
			//CA("ptrIAppFramework == nil");
			break;
		}
		//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(!iConverter)
		{
			//CA("iConverter == nil");
			break;
		}

		MediatorClass :: global_project_level = ptrIAppFramework->getPM_Project_Levels();
		
		SectionData publdata;
		int32 sectionID = -1;
		publdata.clearSectionVector();
		publdata.clearSubsectionVector();
		//define global_vector_pubmodel
		MediatorClass:: global_vector_pubmodel.clear();
		//VectorPubInfoPtr pubSubSecInfoVectPtr=ptrIAppFramework->PUBMngr_findSectionById(sectionID, TRUE);
		//VectorPubInfoPtr pubSubSecInfoVectPtr = ptrIAppFramework->PBMngr_findSectionListByPublicationID(defPubId); // --- commented on 31 jan 4-16
		//---------------------------------------------------------------------------------------------------//
		int32 language_id = -1 ;
		//define global_lang_id
		language_id = MediatorClass :: global_lang_id ; //ptrIClientOptions->getDefaultLocale(language_name);
		VectorPubModelPtr vector_pubmodel = NULL;

		if( MediatorClass :: global_project_level == 3)
		{	
//CA("Level 3");	
			vector_pubmodel = ptrIAppFramework->getAllSectionsByPubIdAndLanguageId(sectionId , language_id);
			if(vector_pubmodel == nil)
			{
				ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::populateSubsectionList::getAllSectionsByPubIdAndLanguageId::vector_pubmodel == nil");
				break;
			}
			VectorPubModel::iterator it;
			for(it=vector_pubmodel->begin(); it!=vector_pubmodel->end(); it++)
			{
				int32 sectionID=it->getPublicationID();
				PMString sectionName = it->getName();
				
				//PMString pubname=it->getName();
				//int32 lvl= 0  ; //it->getLevel_no();
				//int32 rootid = it->getRootID();
				//int32 type_id = it->getTypeID();
				//commented temporarily as section are not directly used
 
				
				VectorPubModelPtr vectorSubSectionModel = ptrIAppFramework->getAllSubsectionsBySectionIdAndLanguageId(sectionID , language_id);
				if(vectorSubSectionModel == nil)
				{
					
					ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::populateSubsectionList::getAllSubsectionsBySectionIdAndLanguageId::vectorSubSectionModel == nil");
					break;
				}
				
				VectorPubModel::iterator it_subsection;

				for(it_subsection=vectorSubSectionModel->begin(); it_subsection!=vectorSubSectionModel->end(); it_subsection++)
				{
 
					CPubModel model = *(it_subsection);
					MediatorClass:: global_vector_pubmodel.push_back(model);
					PMString subSectionName(iConverter->translateString(model.getName()));

					subSectionSprayerListBoxParameters tempClass;

					subSectionData ssd;
					ssd.subSectionName = subSectionName;
					ssd.subSectionID = model.getPublicationID();
					ssd.sectionID = sectionID;
					ssd.sectionName = sectionName;
					ssd.number = model.getNumber();						

					tempClass.displayName = subSectionName;
					tempClass.isSelected = kTrue;
					tempClass.vec_ssd.push_back(ssd);

					if(shall_I_Fill_Data_Into_ListBox)
					{
						//CA("adding elements into listbox");
						listHelper.AddElement(subSectionName,kPublicationNameTextWidgetID);
					}							
					MediatorClass :: vector_subSectionSprayerListBoxParameters.push_back(tempClass);									
				}					
			}	

			if(vector_pubmodel)
				delete vector_pubmodel;

		}
		else if( MediatorClass :: global_project_level ==2)
		{
 
//CA("Level 2");				
			vector_pubmodel = ptrIAppFramework->ProjectCache_getAllChildren(sectionId , language_id);
			if(vector_pubmodel == nil)
			{
				ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::populateSubsectionList::getAllSubsectionsByPubIdAndLanguageId::vector_pubmodel == nil");
				break;
			}
					
//CA("now");
			VectorPubModel::iterator it;
		
			for(it=vector_pubmodel->begin(); it!=vector_pubmodel->end(); it++,no_of_lstboxElements++)
			{
				CPubModel model = (*it);
				MediatorClass:: global_vector_pubmodel.push_back(model);
				PMString subSectionName(iConverter->translateString(model.getName()));
								
				subSectionSprayerListBoxParameters tempClass;

				subSectionData ssd;
				ssd.subSectionName = subSectionName;
				ssd.subSectionID = model.getPublicationID();
				ssd.sectionID = model.getPublicationID();  // For Project Level 2 fill section id same as Subsection id.
				ssd.number = model.getNumber();	

				tempClass.displayName = subSectionName;
				tempClass.isSelected = kTrue;
				tempClass.vec_ssd.push_back(ssd);
				//No sectionID and sectionName information.Default will be assigned.	

				if(shall_I_Fill_Data_Into_ListBox)
				{
					listHelper.AddElement(subSectionName,kPublicationNameTextWidgetID);
				}
///////////////////////////// For hiding extra widgets used for manual spray in white board    from here
				vWidgetID.clear();
				vWidgetID.push_back(kListBoxProductStencilFileNameTextForManualWidgetID);
				vWidgetID.push_back(kSeparatorOnGrouPanel1ForManualWidgetID);
				vWidgetID.push_back(kListBoxItemStencilFileNameTextForManualWidgetID);
				vWidgetID.push_back(kListBoxStencilFileNameForSpecSheetTextWidgetID);//16-10-8
				listHelper.HideWidgets(MediatorClass::PublicationTemplateMappingListBox, vWidgetID, no_of_lstboxElements);
////////////////////////////////////////////////////////////////////////////////////////////     upto here
				MediatorClass :: vector_subSectionSprayerListBoxParameters.push_back(tempClass);
////////////
//////////////
	//		int32 rsrcID = 0;
	//InterfacePtr<IUIFontSpec> fontSpec(this, IID_IUIFONTSPEC); // no kDefaultIID
	//if(fontSpec==nil) {
	//	CA("CMMCustomIconWidgetEH::LButtonDn - No IUIFontSpec found on this boss.");
	//	break;
	//}
	//rsrcID =fontSpec->GetHiliteFontID(); 

	//InterfacePtr<IControlView> myView(this, UseDefaultIID());
	//if(myView == nil) {
	//	CA("CMMCustomIconWidgetEH::LButtonDn - No IControlView found on this boss.");
	//	break;
	//}
	//if(rsrcID != 0) {
	//	myView->SetRsrcID(rsrcID);
	//
	//}
/////////////
			}
			if(vector_pubmodel)
				delete vector_pubmodel;
		
				//PMString listSize;
				//listSize.AppendNumber(no_lstbox_elements);
				//CA(listSize);
		}
		
	

		//subSectionDropListCntrler->Select(0);
 
		
	}while(kFalse);
-*/
}


void CMMDialogObserver::populateSubSectionListForSpreadForSectionLevel(bool16 shall_I_Fill_Data_Into_ListBox,double sectionId)
{
/*-
    no_of_lstboxElements=0;// declared globally on 03-october
	//CA("1 Inside SPSelectionObserver::populateSectionDropDownList");
	//noOfRows = 0;	
	do
	{
		SDKListBoxHelper listHelper(this, kCMMPluginID, kCMMMTemplateFileListBoxWidgetID, kCMMDialogWidgetID);
		listHelper.EmptyCurrentListBox();
		
		//yet to define
		MediatorClass :: vector_subSectionSprayerListBoxParameters.clear();
		//MediatorClass ::PublicationTemplateMappingListBox

		
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
		{
			//CA("ptrIAppFramework == nil");
			break;
		}
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(!iConverter)
		{
			//CA("iConverter == nil");
			break;
		}

		MediatorClass :: global_project_level = ptrIAppFramework->getPM_Project_Levels();
		
		SectionData publdata;
		int32 sectionID = -1;
		publdata.clearSectionVector();
		publdata.clearSubsectionVector();
		//define global_vector_pubmodel
		MediatorClass:: global_vector_pubmodel.clear();
		//VectorPubInfoPtr pubSubSecInfoVectPtr=ptrIAppFramework->PUBMngr_findSectionById(sectionID, TRUE);
		//VectorPubInfoPtr pubSubSecInfoVectPtr = ptrIAppFramework->PBMngr_findSectionListByPublicationID(defPubId); // --- commented on 31 jan 4-16
		//---------------------------------------------------------------------------------------------------//
		int32 language_id = -1 ;
		//define global_lang_id
		language_id = MediatorClass :: global_lang_id ; //ptrIClientOptions->getDefaultLocale(language_name);
		VectorPubModelPtr vector_pubmodel = NULL;

		if( MediatorClass :: global_project_level == 3)

		{	
//CA("Level 3");	
			vector_pubmodel = ptrIAppFramework->getAllSectionsByPubIdAndLanguageId(sectionId , language_id);
			if(vector_pubmodel == nil)
			{
				ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::populateSubsectionListForSpread::getAllSectionsByPubIdAndLanguageId::vector_pubmodel == nil");
				break;
			}
			VectorPubModel::iterator it;

			
			
			for(it=vector_pubmodel->begin(); it!=vector_pubmodel->end(); it++)
			{
					int32 sectionID=it->getPublicationID();
					PMString sectionName = it->getName();
					
					//PMString pubname=it->getName();
					//int32 lvl= 0  ; //it->getLevel_no();
					//int32 rootid = it->getRootID();
					//int32 type_id = it->getTypeID();
					//commented temporarily as section are not directly used
 
					
					VectorPubModelPtr vectorSubSectionModel = ptrIAppFramework->getAllSubsectionsBySectionIdAndLanguageId(sectionID , language_id);
					if(vectorSubSectionModel == nil)
					{
						
						ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::populateSubsectionListSpread::getAllSubsectionsBySectionIdAndLanguageId::vectorSubSectionModel == nil");
						break;
					}
					
					VectorPubModel::iterator it_subsection;

					bool16 isFirstIteration = kTrue;

					subSectionSprayerListBoxParameters tempClass;
					tempClass.isSelected = kTrue;
					tempClass.vec_ssd.clear();
					tempClass.displayName = "";
					
					//whene pageOrder is left you have to add two subsection data in a vector.
					bool16 insertElementNow = kFalse;
					vector<CPubModel> :: iterator end = vectorSubSectionModel->end();
					for(it_subsection=vectorSubSectionModel->begin(); it_subsection!=end; it_subsection++)
					{
						
 
						CPubModel model = *(it_subsection);
						MediatorClass:: global_vector_pubmodel.push_back(model);
						PMString subSectionName(iConverter->translateString(model.getName()));

						

						subSectionData ssd;
						ssd.subSectionName = subSectionName;
						ssd.subSectionID = model.getPublicationID();
						ssd.sectionID = sectionID;
						ssd.sectionName = sectionName;						
						ssd.number = model.getNumber();

						
						
						if(isFirstIteration == kTrue)
						{
							isFirstIteration = kFalse;
							if(MediatorClass ::pageOrderSelectedIndex == 1)
							{
									tempClass.displayName = subSectionName;
									tempClass.vec_ssd.push_back(ssd);
									listHelper.AddElement(tempClass.displayName,kPublicationNameTextWidgetID);

									MediatorClass :: vector_subSectionSprayerListBoxParameters.push_back(tempClass);
									
									tempClass.vec_ssd.clear();
									tempClass.displayName.Clear();
									continue;
							}
						}

						if(insertElementNow == kFalse)
						{
							//Last subSection..it has no pair.We have to display it alone.
							if(it_subsection == end-1)
							{
								tempClass.displayName.Append(subSectionName);
								tempClass.vec_ssd.push_back(ssd);
								insertElementNow = kFalse;

								listHelper.AddElement(tempClass.displayName,kPublicationNameTextWidgetID);

								MediatorClass :: vector_subSectionSprayerListBoxParameters.push_back(tempClass);
								

								tempClass.vec_ssd.clear();
								tempClass.displayName.Clear();
							}
							else 
							{
								tempClass.displayName.Append(subSectionName);
								tempClass.vec_ssd.push_back(ssd);
								insertElementNow = kTrue;
							}
						}
						else
						{
							tempClass.displayName.Append("-" + subSectionName);
							tempClass.vec_ssd.push_back(ssd);
							insertElementNow = kFalse;

							listHelper.AddElement(tempClass.displayName,kPublicationNameTextWidgetID);

							MediatorClass :: vector_subSectionSprayerListBoxParameters.push_back(tempClass);
							

							tempClass.vec_ssd.clear();
							tempClass.displayName.Clear();

						}
							
			
					}
					
					
			}	
			if(vector_pubmodel)
				delete vector_pubmodel;

		}
		else if( MediatorClass :: global_project_level ==2)
		{
//CA("Level 2");				
			vector_pubmodel = ptrIAppFramework->ProjectCache_getAllChildren(sectionId , language_id);
			if(vector_pubmodel == nil)
			{
				
				ptrIAppFramework->LogError("AP46CreateMedia::CMMDialogObserver::populateSubSectionListForSpread::getAllSubsectionsByPubIdAndLanguageId::vector_pubmodel == nil");
				break;
			}
					
//CA("now");
			VectorPubModel::iterator it;
		
			bool16 isFirstIteration = kTrue;

			subSectionSprayerListBoxParameters tempClass;
			tempClass.isSelected = kTrue;
			tempClass.vec_ssd.clear();
			tempClass.displayName = "";
			
			//whene pageOrder is left you have to add two subsection data in a vector.
			bool16 insertElementNow = kFalse;
			vector<CPubModel> :: iterator end = vector_pubmodel->end();

			for(it=vector_pubmodel->begin(); it!=end; it++)
				{
	
					CPubModel model = (*it);
					MediatorClass:: global_vector_pubmodel.push_back(model);
					PMString subSectionName(iConverter->translateString(model.getName()));
									
						subSectionData ssd;
						ssd.subSectionName = subSectionName;
						ssd.subSectionID = model.getPublicationID();
						ssd.sectionID = model.getPublicationID();  // For Level 2 fill sectionId same as SubSection id 
						//ssd.sectionName = sectionName;						
						ssd.number = model.getNumber();

						if(isFirstIteration == kTrue)
						{
							isFirstIteration = kFalse;
							if(MediatorClass ::pageOrderSelectedIndex == 1)
							{
									tempClass.displayName = subSectionName;
									tempClass.vec_ssd.push_back(ssd);

									listHelper.AddElement(tempClass.displayName,kPublicationNameTextWidgetID);
///////////////////////////// For hiding extra widgets used for manual spray in white board    from here
									vWidgetID.clear();
									vWidgetID.push_back(kListBoxProductStencilFileNameTextForManualWidgetID);
									vWidgetID.push_back(kSeparatorOnGrouPanel1ForManualWidgetID);
									vWidgetID.push_back(kListBoxItemStencilFileNameTextForManualWidgetID);

									listHelper.HideWidgets(MediatorClass::PublicationTemplateMappingListBox, vWidgetID, no_of_lstboxElements);
////////////////////////////////////////////////////////////////////////////////////////////     upto here
									no_of_lstboxElements++;
									MediatorClass :: vector_subSectionSprayerListBoxParameters.push_back(tempClass);
									
									tempClass.vec_ssd.clear();
									tempClass.displayName.Clear();
									continue;
							}
						}

						if(insertElementNow == kFalse)
						{
							//Last subSection..it has no pair.We have to display it alone.
							if(it == end-1)
							{
								tempClass.displayName.Append(subSectionName);
								tempClass.vec_ssd.push_back(ssd);
								insertElementNow = kFalse;

								listHelper.AddElement(tempClass.displayName,kPublicationNameTextWidgetID);
///////////////////////////// For hiding extra widgets used for manual spray in white board    from here
								vWidgetID.clear();
								vWidgetID.push_back(kListBoxProductStencilFileNameTextForManualWidgetID);
								vWidgetID.push_back(kSeparatorOnGrouPanel1ForManualWidgetID);
								vWidgetID.push_back(kListBoxItemStencilFileNameTextForManualWidgetID);
								vWidgetID.push_back(kListBoxStencilFileNameForSpecSheetTextWidgetID);//16-10-8
								listHelper.HideWidgets(MediatorClass::PublicationTemplateMappingListBox, vWidgetID, no_of_lstboxElements);
////////////////////////////////////////////////////////////////////////////////////////////     upto here
								no_of_lstboxElements++;
								MediatorClass :: vector_subSectionSprayerListBoxParameters.push_back(tempClass);
								

								tempClass.vec_ssd.clear();
								tempClass.displayName.Clear();
							}
							else 
							{
								tempClass.displayName.Append(subSectionName);
								tempClass.vec_ssd.push_back(ssd);
								insertElementNow = kTrue;
							}
						}
						else
						{
							tempClass.displayName.Append("-" + subSectionName);
							tempClass.vec_ssd.push_back(ssd);
							insertElementNow = kFalse;

							listHelper.AddElement(tempClass.displayName,kPublicationNameTextWidgetID);
///////////////////////////// For hiding extra widgets used for manual spray in white board    from here
							vWidgetID.clear();
							vWidgetID.push_back(kListBoxProductStencilFileNameTextForManualWidgetID);
							vWidgetID.push_back(kSeparatorOnGrouPanel1ForManualWidgetID);
							vWidgetID.push_back(kListBoxItemStencilFileNameTextForManualWidgetID);
							vWidgetID.push_back(kListBoxStencilFileNameForSpecSheetTextWidgetID);//16-10-8
							listHelper.HideWidgets(MediatorClass::PublicationTemplateMappingListBox, vWidgetID, no_of_lstboxElements);
////////////////////////////////////////////////////////////////////////////////////////////     upto here
							no_of_lstboxElements++;
							MediatorClass :: vector_subSectionSprayerListBoxParameters.push_back(tempClass);
							

							tempClass.vec_ssd.clear();
							tempClass.displayName.Clear();

						}
							
										
				}

				//PMString listSize;
				//listSize.AppendNumber(no_lstbox_elements);
				//CA(listSize);
		}
			
		if(vector_pubmodel)
			delete vector_pubmodel;
		//subSectionDropListCntrler->Select(0);
		
	}while(kFalse);
 -*/
	
}

