//#ifndef __TableStyleUtils_h__
//#define __TableStyleUtils_h__

#include "VCPlugInHeaders.h"
#include "ITableModel.h"
#include "AttributeBossList.h"
class TableStyleUtils
{
	public:

		InterfacePtr<ITableModel>sourceTblModel;
		InterfacePtr<ITableModel>targetTblModel;
		//ITableModel* targetTblModel;

		TableStyleUtils(void);
		virtual ~TableStyleUtils();
		virtual bool16 GetCellTextStyleUID(InterfacePtr<ITableModel>tblModel,int32 row,int32 col,UID &textStyleUID,AttributeBossList* &ab);
		virtual bool16 SetCellTextStyleUID(InterfacePtr<ITableModel>tblModel,int row,int col,UID textStyleUID,AttributeBossList *ab);
		virtual bool16 GetCellColourUID(InterfacePtr<ITableModel>tblModel,int row,int col,UID &colorUID);
		virtual bool16 SetCellColourUID(InterfacePtr<ITableModel>tblModel,int row,int col,UID colorUID);
		virtual bool16 ApplyTableStyle();
		virtual void SetTableModel(bool16 isSource, int32 TableIndex=0);
		//virtual void SetSourceTableModel(UIDList uList);
		void setTableStyle();
		void ApplyStyleToSelectedFrame(PMString styleName,UIDRef &tempDoc);

		virtual int deleteThisBox(UIDRef frameRef);

		///////////Temp Testing//////////////////////////////////////////////////////
		void PlaceToHiddenDoc(UIDRef &doc);
		bool16  GetTableStyle();
		void getOverallTableStyle();


};
//
//#endif