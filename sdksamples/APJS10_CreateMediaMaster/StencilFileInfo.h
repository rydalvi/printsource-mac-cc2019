#ifndef __STENCILFILEINFO_H__
#define __STENCILFILEINFO_H__

#include "VCPluginHeaders.h"
#include "vector"

class StencilFileInfo
{
public:
	PMString path;
	UIDList uidList;
	PMString type;
};

typedef vector<StencilFileInfo> StencilFileInfoList, *StencilFileInfoListPtr;

#endif