//========================================================================================
//  
//  $File: $
//  
//  Owner: Catsy
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ISession.h"
#include "IActiveContext.h"
#include "IPanelControlData.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"

//common sdk files
//#include "SDKListBoxHelper.h"


// General includes:

// Project includes:

//PrintSource4.5 interfaces
#include "IAppFramework.h"
#include "IClientOptions.h"
#include "ISpecialChar.h"

//PrintSource4.5 id files
#include "OptnsDlgID.h"
#include "AFWJSID.h"
#include "CMMID.h"
//#include "SCTID.h"

//local class files
#include "MediatorClass.h"
#include "CDialogController.h"
#include "SectionData.h"
#include "ITextControlData.h"
#include "CMMDialogController.h"



#include "CAlert.h"
void CAMessage(const PMString & fileName,const PMString & functionName,const PMString & message,int32 line);
#define FILENAME			PMString("CMMDialogController.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

/** CMMDialogController
	Methods allow for the initialization, validation, and application of dialog widget
	values.
	Implements IDialogController based on the partial implementation CDialogController.

	
	@ingroup apjs10_createmediamaster
*/

//class CMMDialogController : public CDialogController
//{
//	public:
		/** Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
//		CMMDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** Destructor.
		*/
//		virtual ~CMMDialogController() {}

		/** Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
//	       virtual void InitializeDialogFields(IActiveContext* dlgContext);

		/** Validate the values in the widgets.
			By default, the widget with ID kOKButtonWidgetID causes
			ValidateFields to be called. When all widgets are valid,
			ApplyFields will be called.
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
//	       virtual WidgetID ValidateDialogFields(IActiveContext* myContext);


		/** Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
//		virtual void ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId);
		//void updateListBox(bool16 isAttaching);

//		virtual void DialogClosing();
//
//		void populateSectionDropDownList(double rootId);
//		void populatePublicationDropDownList(double defPubId, double SelectedLocaleID);

//};

//-*/

CREATE_PMINTERFACE(CMMDialogController, kCMMDialogControllerImpl)

/* ApplyFields
*/
void CMMDialogController::InitializeDialogFields(IActiveContext* dlgContext)
{
	//CA("CMMDialogController::InitializeDialogFields..Line 121");
	do
	{
		CDialogController::InitializeDialogFields(dlgContext);

/*-		SDKListBoxHelper listHelper(this, kCMMPluginID, kCMMMTemplateFileListBoxWidgetID, kCMMDialogWidgetID);
		IControlView * listBox = listHelper.FindCurrentListBox();
		if(listBox == nil)
		{
			//CAlert::InformationAlert("1 listBox == nil");
			break;
		}
		MediatorClass ::PublicationTemplateMappingListBox = listBox;
-*/
        
        InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
        if(ptrIAppFramework == nil)
        {
            //CA("ptrIAppFramework == nil");
            return;
        }
        
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if(!panelControlData) 
		{
			break;
		}
        MediatorClass ::iMainPanelCntrlDataPtr = panelControlData;

		InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
		if(dialogController == nil)
		{
			//CA("dialogController == nil");
			break;
		}
        
        MediatorClass::iMainDaialogControllerPtr =dialogController;
		//dialogController->SetTriStateControlData(kFGPOneDocPerSectionRadioWidgetID,kTrue);
        dialogController->SetTriStateControlData(kCMMSpecSheetRadioWidgetID,kTrue);

		InterfacePtr<IStringListControlData> pageOrderDropListData(
		dialogController->QueryListControlDataInterface(kFGPPageOrderSelectionDropDownWidgetID));
		if(pageOrderDropListData == nil)
		{
			//CA("pageOrderDropListData nil");
			break;
		}
		InterfacePtr<IDropDownListController> pageOrderDropListController(
		pageOrderDropListData,UseDefaultIID());
		if(pageOrderDropListController == nil)
		{
			//CA("pageOrderDropListController nil");
			break;
		}
		pageOrderDropListController->Select(0);
		MediatorClass :: pageOrderSelectedIndex = 0;

		//Hide the secondGroupPanel
		IControlView * secondGroupPanelControlData = panelControlData->FindWidget(kCMMSecondGroupPanelWidgetID);
		if(secondGroupPanelControlData == nil)
		{
			//CA("secondGroupPanelControlData == nil");
			break;
		}
		secondGroupPanelControlData->HideView();
		//Show the firstGroupPanel
		IControlView * firstGroupPanelControlData = panelControlData->FindWidget(kCMMFirstGroupPanelWidgetID);
		if(firstGroupPanelControlData == nil)
		{
			//CA("firstGroupPanelControlData == nil");
			break;
		}
		firstGroupPanelControlData->ShowView();//30Oct modification.ZerothGroupPanel removed.The dialogShouldDisplay the firstGroupPanel at startup.
	

		IControlView * firstGroupClusterPanelControlData = panelControlData->FindWidget(kCMMFirstGroupPanelClusterPanelWidgetID);
		if(firstGroupClusterPanelControlData == nil)
		{
			//CA("firstGroupClusterPanelControlData == nil");
			break;
		}
		firstGroupClusterPanelControlData->Disable();
        
        IControlView * FGPPlaceItemsOrProductsByPageAssignmentCheckBoxWidgetControlView = panelControlData->FindWidget(kFGPPlaceItemsOrProductsByPageAssignmentCheckBoxWidgetID);
        if(FGPPlaceItemsOrProductsByPageAssignmentCheckBoxWidgetControlView == nil)
        {
            ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::Update::FGPPlaceItemsOrProductsByPageAssignmentCheckBoxWidgetControlView == nil");
            break;
        }
        FGPPlaceItemsOrProductsByPageAssignmentCheckBoxWidgetControlView->Disable();

        ///////////////////////////////////

		//fill the section dropdown list
		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			//CAlert::ErrorAlert("Interface for IClientOptions not found.");
			break;
		}

		PMString language_name("");
		
		//define global_lang_id
		MediatorClass ::global_lang_id = ptrIClientOptions->getDefaultLocale(language_name);
        
        PMString defPubName("");
        double defPubId=-1;
        
        defPubId = ptrIClientOptions->getDefPublication(defPubName);
        if(defPubId <= 0)
        {
            //CA(" defPubId <= 0 ");
            break;
        }
        
        IControlView* pubNameControlView = panelControlData->FindWidget( kCMMPubNameWidgetID);
        if(pubNameControlView == NULL)
        {
            ptrIAppFramework->LogDebug("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::No pubNameControlView");
            break;
        }
        
        InterfacePtr<ITextControlData> pubNameTxtData(pubNameControlView,IID_ITEXTCONTROLDATA);
        if(pubNameTxtData == nil)
        {
            ptrIAppFramework->LogDebug("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::No pubNameTxtData");
            break;
        }
        
        PMString pubName("");
        
        pubName = "Event";
        pubName.Append(" : ");
        pubName.Append(defPubName);
        
        //CA("1");
        pubName.ParseForEmbeddedCharacters();
        pubNameTxtData->SetString(pubName);
        
		populatePublicationDropDownList(defPubId, MediatorClass ::global_lang_id);
        MediatorClass :: currentSelectedPublicationID= defPubId;

/*-
		IControlView * openNextDialogButtonControlView = panelControlData->FindWidget(kOpenNextDialogButtonWidgetID);
		if(openNextDialogButtonControlView == nil)
		{
			//CA("openNextDialogButtonControlView == nil");
			break;
		}
		openNextDialogButtonControlView->Disable();
-*/
        IControlView * CreateMediaButtonControlView = panelControlData->FindWidget(kCreateMediaButtonWidgetID);
        if(CreateMediaButtonControlView == nil)
        {
            //CA("CreateMediaButtonControlView == nil");
            break;
        }
        CreateMediaButtonControlView->Disable();
        
        IControlView * fileBrowseDialogButtonControlView = panelControlData->FindWidget(kFileBrowseDialogButtonWidgetID);
        if(fileBrowseDialogButtonControlView == nil)
        {
            CA("fileBrowseDialogButtonControlView == nil");
            break;
        }
        fileBrowseDialogButtonControlView->Disable();
        
        IControlView * specSheetStencilTextControlView = panelControlData->FindWidget(kTemplateFileTextEditBoxWidgetID);
        if(specSheetStencilTextControlView == nil)
        {
            //CA("productStencilTextControlView == nil");
            break;
        }
        
        InterfacePtr<ITextControlData>specSheetStencilTextControlData(specSheetStencilTextControlView,UseDefaultIID());
        if(specSheetStencilTextControlData == nil)
        {
            //CA("productStencilTextControlData == nil");
            break;
        }
        
        IControlView * TextFirstPageWidgetControlView = panelControlData->FindWidget(kTextFirstPageWidgetID);
        if(TextFirstPageWidgetControlView == nil)
        {
            ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::Update::TextFirstPageWidgetControlView == nil");
            break;
        }

        TextFirstPageWidgetControlView->Disable();

        
        
        IControlView * FGPPageOrderSelectionDropDownWidgetControlView = panelControlData->FindWidget(kFGPPageOrderSelectionDropDownWidgetID);
        if(FGPPageOrderSelectionDropDownWidgetControlView == nil)
        {
            ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::Update::FGPPageOrderSelectionDropDownWidgetControlView == nil");
            break;
        }

        FGPPageOrderSelectionDropDownWidgetControlView->Disable();

        
        IControlView * FGPKeepSpreadsTogetherCheckBoxWidgetControlView = panelControlData->FindWidget(kFGPKeepSpreadsTogetherCheckBoxWidgetID);
        if(FGPKeepSpreadsTogetherCheckBoxWidgetControlView == nil)
        {
            ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::Update::FGPKeepSpreadsTogetherCheckBoxWidgetControlView == nil");
            break;
        }

        FGPKeepSpreadsTogetherCheckBoxWidgetControlView->Disable();


        PMString insertText1("");
        PMString text5("Template (*.indt)");
        insertText1 = "Select Spec-Sheet ";
        insertText1.Append(text5);
        insertText1.SetTranslatable(kFalse);
        specSheetStencilTextControlData->SetString(insertText1);
        specSheetStencilTextControlView->Disable();

		
		IControlView * selectAllSectionCheckBoxControlView = panelControlData->FindWidget(kSelectAllCheckBoxWidgetID);
		if(selectAllSectionCheckBoxControlView == nil)
		{
			//CA("selectAllSectionCheckBoxControlView == nil");
			break;
		}
		InterfacePtr<ITriStateControlData>selectAllSectionCheckBoxTriState(selectAllSectionCheckBoxControlView,UseDefaultIID());
		if(selectAllSectionCheckBoxTriState == nil)
		{
			//CA("selectAllSectionCheckBoxTriState == nil");
			break;
		}

		MediatorClass ::selectAllSectionCheckBoxControlView =selectAllSectionCheckBoxControlView;
		////ended on 6Jun	
		
		

		IControlView * CreateDocPerItemControlView = panelControlData->FindWidget(kCreateDocPerItemTextWidgetID);
		if(CreateDocPerItemControlView == nil)
		{
			//CA("CreateDocPerItemControlView == nil");
			break;
		}
		InterfacePtr<ITextControlData>textControlData(CreateDocPerItemControlView,UseDefaultIID());
		if(textControlData == nil)
		{
			//CA("textControlData == nil");
			break;
		}
		PMString insertText("Create One Document Per ");
		insertText.SetTranslatable(kFalse);
		insertText.Append("Family or Item.");
		insertText.SetTranslatable(kFalse);
		textControlData->SetString(insertText);

		//---------------
		IControlView * itemsOrProductTextControlView = panelControlData->FindWidget(kItemsOrProductTextWidgetID);
		if(itemsOrProductTextControlView == nil)
		{
			//CA("itemsOrProductTextControlView == nil");
			break;
		}
		InterfacePtr<ITextControlData>itemsOrProductTextControlData(itemsOrProductTextControlView,UseDefaultIID());
		if(itemsOrProductTextControlData == nil)
		{
			//CA("itemsOrProductTextControlData == nil");
			break;
		}
		insertText.Clear();
		insertText="Place ";
		insertText.Append("Items ");
		insertText.Append("/");
		insertText.Append(" Families");
		insertText.Append(" by page assignment");
		insertText.SetTranslatable(kFalse);
		itemsOrProductTextControlData->SetString(insertText);
		//===================MANE=====================================
        itemsOrProductTextControlView->Disable();

		IControlView * PublicationTextControlView = panelControlData->FindWidget(kSelectPublicationWidgetID);
		if(PublicationTextControlView == nil)
		{
			CA("PublicationTextControlView == nil");
			break;
		}
		InterfacePtr<ITextControlData>PublicationTextControlData(PublicationTextControlView,UseDefaultIID());
		if(itemsOrProductTextControlData == nil)
		{
			CA("PublicationTextControlData == nil");
			break;
		}
		insertText.Clear();
		insertText="Select ";
		PMString publicationText = "Section: "; //"Event";
		insertText.Append(publicationText);
		insertText.SetTranslatable(kFalse);
		PublicationTextControlData->SetString(insertText);
		IControlView * mediapublicationTextControlView = panelControlData->FindWidget(kCreateMediaPublicationWidgetID);
		if(mediapublicationTextControlView == nil)
		{
			CA("mediapublicationTextControlView == nil");
			break;
		}
		InterfacePtr<ITextControlData>mediapublicationTextControlData(mediapublicationTextControlView,UseDefaultIID());
		if(mediapublicationTextControlData == nil)
		{
			CA("mediapublicationTextControlData == nil");
			break;
		}
		insertText.Clear();
		insertText="Create Media ";
		PMString mediapublicationText = "Event";
		insertText.Append(mediapublicationText);
		mediapublicationTextControlData->SetString(insertText);

		//===========================Up to Here=======================
		IControlView * productStencilTextControlView = panelControlData->FindWidget(kProductTemplateFileNameOnGroupPanelWidgetID);
		if(productStencilTextControlView == nil)
		{
			//CA("productStencilTextControlView == nil");
			break;
		}
		InterfacePtr<ITextControlData>productStencilTextControlData(productStencilTextControlView,UseDefaultIID());
		if(productStencilTextControlData == nil)
		{
			//CA("productStencilTextControlData == nil");
			break;
		}
		insertText.Clear();
		insertText="";
		PMString text1("Template (*.indt)");
		insertText = "Family";

		insertText.Append(kCMMBlankSpaceStringKey);
		insertText.Append(text1);
		insertText.SetTranslatable(kFalse);
		productStencilTextControlData->SetString(insertText);

		
		IControlView * itemStencilTextControlView = panelControlData->FindWidget(kItemTemplateFileNameOnGroupPanelWidgetID);
		if(itemStencilTextControlView == nil)
		{
			//CA("itemStencilTextControlView == nil");
			break;
		}
		InterfacePtr<ITextControlData>itemStencilTextControlData(itemStencilTextControlView,UseDefaultIID());
		if(itemStencilTextControlData == nil)
		{
			//CA("itemStencilTextControlData == nil");
			break;
		}
		insertText.Clear();
		insertText="";
		PMString text2("Template (*.indt)");
		insertText = "Item";
		insertText.Append(kCMMBlankSpaceStringKey);
		insertText.Append(text2);
		insertText.SetTranslatable(kFalse);
		itemStencilTextControlData->SetString(insertText);

		
		
		IControlView* oneDocProductStencilTextControlView = panelControlData->FindWidget(kProductTemplateFileNameOnGroupPanelForManualWidgetID);
		if(oneDocProductStencilTextControlView == nil)
		{
			//CA("oneDocProductStencilTextControlView == nil");
			break;
		}
		InterfacePtr<ITextControlData>oneDocProductStencilTextControlData(oneDocProductStencilTextControlView,UseDefaultIID());
		if(oneDocProductStencilTextControlData == nil)
		{
			//CA("oneDocProductStencilTextControlData == nil");
			break;
		}
		insertText.Clear();
		insertText="";
		PMString text3("Template (*.indt)");
		insertText = "Family";
		//insertText.Append(" ");
		insertText.Append(kCMMBlankSpaceStringKey);
		insertText.Append(text3);
		insertText.SetTranslatable(kFalse);
		oneDocProductStencilTextControlData->SetString(insertText);


		IControlView * oneDocItemStencilTextControlView = panelControlData->FindWidget(kItemTemplateFileNameOnGroupPanelForManualWidgetID);
		if(oneDocItemStencilTextControlView == nil)
		{
			//CA("oneDocItemStencilTextControlView == nil");
			break;
		}
		InterfacePtr<ITextControlData>oneDocItemStencilTextControlData(oneDocItemStencilTextControlView,UseDefaultIID());
		if(oneDocItemStencilTextControlData == nil)
		{
			//CA("oneDocItemStencilTextControlData == nil");
			break;
		}
		insertText.Clear();
		insertText="";
		PMString text4("Template (*.indt)");
		insertText = "Item";
		//insertText.Append(" ");
		insertText.Append(kCMMBlankSpaceStringKey);
		insertText.Append(text4);
		insertText.SetTranslatable(kFalse);
		oneDocItemStencilTextControlData->SetString(insertText);
	
		
		IControlView * multilineTextControlView = panelControlData->FindWidget(kTushar1MultilineTextWidgetID);
		if(multilineTextControlView == nil)
		{
			//CA("multilineTextControlView == nil");
			break;
		}
		InterfacePtr<ITextControlData>multilineTextControlData(multilineTextControlView,UseDefaultIID());
		if(multilineTextControlData == nil)
		{
			//CA("multilineTextControlData == nil");
			break;
		}
		insertText.Clear();
		insertText="Use this portion if you want each InDesign document to have all selected ";
		insertText.Append("Item");
		insertText.Append("(s) and/or ");
		insertText.Append("Family");
		insertText.Append("(s) as setup in the ");
		insertText.Append("Event");
		insertText.Append(".");
		insertText.SetTranslatable(kFalse);
		multilineTextControlData->SetString(insertText);

		IControlView * priceBookMultilineTextControlView = panelControlData->FindWidget(kPriceBookMultilineTextWidgetID);
		if(priceBookMultilineTextControlView == nil)
		{
			//CA("priceBookMultilineTextControlView == nil");
			break;
		}
		InterfacePtr<ITextControlData>priceBookMultilineTextControlData(priceBookMultilineTextControlView,UseDefaultIID());
		if(priceBookMultilineTextControlData == nil)
		{
			//CA("priceBookMultilineTextControlData == nil");
			break;
		}
		insertText.Clear();
		insertText="Use this portion if you want to create a price list for the ";
		insertText.Append("Event");
		insertText.Append(".");
		insertText.SetTranslatable(kFalse);
		priceBookMultilineTextControlData->SetString(insertText);
        
       	
	}while(kFalse);

	

	//listHelper.AddElement("1",kListBoxMasterFileNameTextWidgetID,0);
	// Put code to initialize widget values here.
}

/* ValidateFields
*/
WidgetID CMMDialogController::ValidateDialogFields(IActiveContext* myContext)
{
	WidgetID result = CDialogController::ValidateDialogFields(myContext);
	// Put code to validate widget values here.
	return result;
}

/* ApplyFields
*/
void CMMDialogController::ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId)
{
	// TODO add code that gathers widget values and applies them.
}


void CMMDialogController :: DialogClosing()
{
//CA("in dialog closing 1");
MediatorClass :: global_vector_pubmodel.clear() ;
//CA("in dialog closing 2");
MediatorClass :: publicationVector.clear();
//CA("in dialog closing 3");

MediatorClass :: vector_subSectionSprayerListBoxParameters.clear();

//int32 level = 5;
//CA("before purging memory");
//gSession->PurgeMemory(level);
//CA("after purging memory");
//CA("in dialog closing 4");

//if(MediatorClass ::PublicationTemplateMappingListBox !=nil)
//{
//	MediatorClass ::PublicationTemplateMappingListBoRelease();
//}
}

//Change implementation now populates childeren of defPubId
void CMMDialogController::populatePublicationDropDownList(double defPubId, double SelectedLocaleID)
{
	do
	{
        
		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
		{
			//CA("ptrIAppFramework == nil");
			break;
		}
        //ptrIAppFramework->LogDebug("CMMDialogController::populatePublicationDropDownList enter");

		if(SelectedLocaleID == -1)
			break;
         //ptrIAppFramework->LogDebug("CMMDialogController::populatePublicationDropDownList enter 2");
        
        InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
        if(ptrIClientOptions==nil)
        {
            CAlert::ErrorAlert("Interface for IClientOptions not found.");
            break;
        }
        
        PMString language_name("");
        
        //define global_lang_id
        MediatorClass ::global_lang_id = ptrIClientOptions->getDefaultLocale(language_name);
        
        PMString defPubName("");
        //double defPubId=-1;
        
        double pubParent_ID = -1;
        double rootPubID = -1;
        
        CPubModel pubModelValue = ptrIAppFramework->getpubModelByPubID(defPubId, MediatorClass::global_lang_id);
        pubParent_ID = pubModelValue.getParentId();
        rootPubID = pubModelValue.getRootID();
        //CA(pubModelValue.getName());
        
        CPubModel pubModelRootValue = ptrIAppFramework->getpubModelByPubID(rootPubID,MediatorClass::global_lang_id);
        PMString secName("");
        defPubName = pubModelRootValue.getName();
        
        //InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
        if(! MediatorClass::iMainPanelCntrlDataPtr)
        {
            ptrIAppFramework->LogDebug("CMMDialogController::populatePublicationDropDownList !panelControlData ");
            break;
        }
        
        IControlView* pubNameControlView =  MediatorClass::iMainPanelCntrlDataPtr->FindWidget( kCMMPubNameWidgetID);
        if(pubNameControlView == NULL)
        {
            ptrIAppFramework->LogDebug("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::No pubNameControlView");
            break;
        }
        
        InterfacePtr<ITextControlData> pubNameTxtData(pubNameControlView,IID_ITEXTCONTROLDATA);
        if(pubNameTxtData == nil)
        {
            ptrIAppFramework->LogDebug("AP7_ProductFinder::ContentSprayer::setClassNameAndClassID::No pubNameTxtData");
            break;
        }
        
        PMString pubName("");
        
        pubName = "Event";
        pubName.Append(" : ");
        pubName.Append(defPubName);
        
        //CA("1");
        pubName.ParseForEmbeddedCharacters();
        pubNameTxtData->SetString(pubName);

        //ptrIAppFramework->LogDebug("CMMDialogController::populatePublicationDropDownList enter 5");

		//InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
		if(MediatorClass::iMainDaialogControllerPtr == nil)
		{
			//CA("dialogController == nil");
			break;
		}
        
        //ptrIAppFramework->LogDebug("CMMDialogController::populatePublicationDropDownList enter 6");

		InterfacePtr<IStringListControlData> PubDropListData(
		MediatorClass::iMainDaialogControllerPtr->QueryListControlDataInterface(kPublicationDropDownWidgetID));
		if(PubDropListData == nil)
		{
			//CA("PubDropListData nil");
			break;
		}
        
        //ptrIAppFramework->LogDebug("CMMDialogController::populatePublicationDropDownList enter 7");
		
        PubDropListData->Clear(kFalse,kFalse);
        MediatorClass :: vector_subSectionSprayerListBoxParameters.clear();
        MediatorClass :: global_vector_pubmodel.clear() ;

		InterfacePtr<IDropDownListController> PubDropListController(PubDropListData,UseDefaultIID());
		if (PubDropListController == nil)
		{
			//CA("PubDropListController nil");
			break;
		}

        //ptrIAppFramework->LogDebug("CMMDialogController::populatePublicationDropDownList enter 8");
		VectorPubModelPtr VectorPublInfoValuePtr = ptrIAppFramework->ProjectCache_getAllChildren( defPubId, SelectedLocaleID);
		if(VectorPublInfoValuePtr == nil){
			//CAlert::InformationAlert("There are no Sections under selected Event!");
			
            VectorPublInfoValuePtr = ptrIAppFramework->ProjectCache_getAllChildren(pubParent_ID, SelectedLocaleID);
            if(VectorPublInfoValuePtr == nil){
                //CAlert::InformationAlert("There are no Sections under selected Event!...");
                break;
            }
            
		}

		
		VectorPubModel::iterator it;
		int32 dropDownListRowIndex = 0;
		MediatorClass ::publicationVector.clear();
		
		for(it = VectorPublInfoValuePtr->begin(); it != VectorPublInfoValuePtr->end(); it++)
		{
            //ptrIAppFramework->LogDebug("CMMDialogController::populatePublicationDropDownList enter 9");
			InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			PMString CatalogName;
			PMString TempBuffer = it->getName();
			TempBuffer.SetTranslatable(kFalse);
			if(iConverter)
			{ 
				CatalogName=iConverter->translateString(TempBuffer);
			}
			else
			{ 				
				CatalogName = TempBuffer;
			}
			
			MediatorClass ::publicationVector.push_back(it->getEventId());
					
			if(dropDownListRowIndex == 0)
			{
				PMString str;
				str.Append("--Select--");
				str.SetTranslatable(kFalse);
				PubDropListData->AddString(str, dropDownListRowIndex, kFalse, kFalse);
				dropDownListRowIndex++;
			}
			CatalogName.SetTranslatable(kFalse);
			PubDropListData->AddString(CatalogName.SetTranslatable(kFalse), dropDownListRowIndex, kFalse, kFalse);
			dropDownListRowIndex++;
            
            
            CPubModel model = (*it);
            MediatorClass:: global_vector_pubmodel.push_back(model);
            PMString subSectionName(iConverter->translateString(model.getName()));
            
            subSectionSprayerListBoxParameters tempClass;
            
            subSectionData ssd;
            ssd.subSectionName = subSectionName;
            ssd.subSectionID = model.getEventId();
            ssd.sectionID = model.getEventId();  // For Project Level 2 fill section id same as Subsection id.
            //ssd.number = model.getNumber();
            
            tempClass.displayName = subSectionName;
            tempClass.isSelected = kFalse;
            tempClass.vec_ssd.push_back(ssd);
            //No sectionID and sectionName information.Default will be assigned.
            
            MediatorClass::vector_subSectionSprayerListBoxParameters.push_back(tempClass);
            
		}
        
        //ptrIAppFramework->LogDebug("CMMDialogController::populatePublicationDropDownList enter 10");
		PubDropListController->Select(0);
		if(VectorPublInfoValuePtr)
			delete VectorPublInfoValuePtr;

	}while(kFalse);


}
//  Code generated by DollyXs code generator
