#ifndef __SDKListBoxHelper_h__
#define __SDKListBoxHelper_h__
#include "ISubject.h"

class IPMUnknown;
class IControlView;
class IPanelControlData;

class SDKListBoxHelper
{
public:
	SDKListBoxHelper(IPMUnknown * fOwner, int32 pluginId);
	virtual ~SDKListBoxHelper();
	void AddElement(IControlView* lstboxControlView, PMString & displayName,   WidgetID updateWidgetId, int atIndex = -2, int x=1, bool16 isObject=kTrue);
	void RemoveElementAt(int indexRemove, int x);
	void RemoveLastElement( int x);
	IControlView * FindCurrentListBox(InterfacePtr<IPanelControlData> panel, int x);
	void EmptyCurrentListBox(IControlView *listboxCntrlView);

	int GetElementCount(int x);
	void CheckUncheckRow(IControlView* listboxCntrlView, int32, bool);
	void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
private:
	bool16 verifyState() { return (fOwner!=nil) ? kTrue : kFalse; }
	void addListElementWidget(IControlView* lstboxControlView, InterfacePtr<IControlView> & elView, PMString & displayName, WidgetID updateWidgetId, int atIndex, int x, bool16);
	void removeCellWidget(IControlView * listBox, int removeIndex);
	IPMUnknown * fOwner;
	int32 fOwnerPluginID;
};

#endif 