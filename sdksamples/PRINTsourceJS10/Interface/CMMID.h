//========================================================================================
//  
//  $File: $
//  
//  Owner: Catsy
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __CMMID_h__
#define __CMMID_h__

#include "SDKDef.h"

// Company:
#define kCMMCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kCMMCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kCMMPluginName	 "Create Media" //"AP7_CreateMediaMaster"			// Name of this plug-in.
#define kCMMPrefixNumber	0xABA1F00 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kCMMVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kCMMAuthor		"Catsy"					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kCMMPrefixNumber above to modify the prefix.)
#define kCMMPrefix		RezLong(kCMMPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kCMMStringPrefix	SDK_DEF_STRINGIZE(kCMMPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kCMMMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kCMMMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kCMMPluginID, kCMMPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kCMMActionComponentBoss, kCMMPrefix + 0)
DECLARE_PMID(kClassIDSpace, kCMMDialogBoss, kCMMPrefix + 2)
DECLARE_PMID(kClassIDSpace, kCMMMTemplateFileListBoxWidgetBoss, kCMMPrefix + 3)
DECLARE_PMID(kClassIDSpace, kCMMRollOverIconButtonWidgetBoss, kCMMPrefix +4)
DECLARE_PMID(kClassIDSpace, kCMMInvokeSubSectionOptionsDialogButtonWidgetBoss, kCMMPrefix +5)
DECLARE_PMID(kClassIDSpace, kSpraySettingsDialogBoss, kCMMPrefix +6)
DECLARE_PMID(kClassIDSpace, kCMMIconSuiteWidgetBoss, kCMMPrefix +7) //13-april
DECLARE_PMID(kClassIDSpace, kCMMCustomIconSuiteWidgetBoss, kCMMPrefix +8) //13-april

//Spray Settings dialog
DECLARE_PMID(kClassIDSpace, kCMMSpraySettingsDialogBoss, kCMMPrefix + 9)
DECLARE_PMID(kClassIDSpace, kCMMExportOptionsDialogBoss, kCMMPrefix + 10)
DECLARE_PMID(kClassIDSpace, kCMMInterfaceBoss, kCMMPrefix +11)
//DECLARE_PMID(kClassIDSpace, kCMMBoss, kCMMPrefix + 9)
//DECLARE_PMID(kClassIDSpace, kCMMBoss, kCMMPrefix + 10)
//DECLARE_PMID(kClassIDSpace, kCMMBoss, kCMMPrefix + 11)
//DECLARE_PMID(kClassIDSpace, kCMMBoss, kCMMPrefix + 12)
//DECLARE_PMID(kClassIDSpace, kCMMBoss, kCMMPrefix + 13)
//DECLARE_PMID(kClassIDSpace, kCMMBoss, kCMMPrefix + 14)
//DECLARE_PMID(kClassIDSpace, kCMMBoss, kCMMPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kCMMBoss, kCMMPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kCMMBoss, kCMMPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kCMMBoss, kCMMPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kCMMBoss, kCMMPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kCMMBoss, kCMMPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kCMMBoss, kCMMPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kCMMBoss, kCMMPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kCMMBoss, kCMMPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kCMMBoss, kCMMPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kCMMBoss, kCMMPrefix + 25)


// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 0)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 1)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICMMINTERFACE, kCMMPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kCMMActionComponentImpl, kCMMPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kCMMDialogControllerImpl, kCMMPrefix + 1 )
DECLARE_PMID(kImplementationIDSpace, kCMMDialogObserverImpl, kCMMPrefix + 2 )
DECLARE_PMID(kImplementationIDSpace, kCMMTemplateFileListBoxObserverImpl, kCMMPrefix + 3)
DECLARE_PMID(kImplementationIDSpace, kCMMMTemplateFileSelectButtonObserverImpl, kCMMPrefix + 4)
DECLARE_PMID(kImplementationIDSpace, kCMMTemplateFileRollOverIconButtonObserverImpl, kCMMPrefix + 5)
DECLARE_PMID(kImplementationIDSpace, kCMMInvokeSubSectionOptionsDialogButtonObserverImpl, kCMMPrefix + 6)
DECLARE_PMID(kImplementationIDSpace, kSpraySettingsDialogControllerImpl, kCMMPrefix + 7)
DECLARE_PMID(kImplementationIDSpace, kSpraySettingsDialogObserverImpl, kCMMPrefix + 8)
//DECLARE_PMID(kImplementationIDSpace, kCMMImpl, kCMMPrefix + 9)
//DECLARE_PMID(kImplementationIDSpace, kCMMImpl, kCMMPrefix + 10)

//Spray Settings dialog
DECLARE_PMID(kImplementationIDSpace, kCMMSpraySettingsDialogControllerImpl,		kCMMPrefix + 11)
DECLARE_PMID(kImplementationIDSpace, kCMMSpraySettingsDialogObserverImpl,		kCMMPrefix + 12)
//added later
DECLARE_PMID(kImplementationIDSpace, kAllSectionsCommonTemplateFileButtonWidgetID, kCMMPrefix + 13)
DECLARE_PMID(kImplementationIDSpace, kCMMCustomIconSuitWidgetEHImpl,				kCMMPrefix + 14)
DECLARE_PMID(kImplementationIDSpace, kCMMExportOptionsDialogObserverImpl, kCMMPrefix + 15)
DECLARE_PMID(kImplementationIDSpace, kCMMInterfaceImpl, kCMMPrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kCMMImpl, kCMMPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kCMMImpl, kCMMPrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kCMMImpl, kCMMPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kCMMImpl, kCMMPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kCMMImpl, kCMMPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kCMMImpl, kCMMPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kCMMImpl, kCMMPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kCMMImpl, kCMMPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kCMMImpl, kCMMPrefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kCMMAboutActionID, kCMMPrefix + 0)

DECLARE_PMID(kActionIDSpace, kCMMDialogActionID, kCMMPrefix + 4)
DECLARE_PMID(kActionIDSpace, kCMMBookActionID, kCMMPrefix + 5)
DECLARE_PMID(kActionIDSpace, kWhiteBoardRefreshActionID, kCMMPrefix + 6)
DECLARE_PMID(kActionIDSpace, kCreatePDFsActionID, kCMMPrefix + 7)
//DECLARE_PMID(kActionIDSpace, kCMMActionID, kCMMPrefix + 8)
//DECLARE_PMID(kActionIDSpace, kCMMActionID, kCMMPrefix + 9)
//DECLARE_PMID(kActionIDSpace, kCMMActionID, kCMMPrefix + 10)
//DECLARE_PMID(kActionIDSpace, kCMMActionID, kCMMPrefix + 11)
//DECLARE_PMID(kActionIDSpace, kCMMActionID, kCMMPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kCMMActionID, kCMMPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kCMMActionID, kCMMPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kCMMActionID, kCMMPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kCMMActionID, kCMMPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kCMMActionID, kCMMPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kCMMActionID, kCMMPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kCMMActionID, kCMMPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kCMMActionID, kCMMPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kCMMActionID, kCMMPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kCMMActionID, kCMMPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kCMMActionID, kCMMPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kCMMActionID, kCMMPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kCMMActionID, kCMMPrefix + 25)


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kCMMDialogWidgetID, kCMMPrefix + 1)

//FirstGroupPanel Widget
DECLARE_PMID(kWidgetIDSpace, kPublicationDropDownWidgetID, kCMMPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kSelectCommonMasterForAllPublicationRadioWidgetID, kCMMPrefix + 3)
DECLARE_PMID(kWidgetIDSpace, kTemplateFileTextEditBoxWidgetID, kCMMPrefix + 4)
DECLARE_PMID(kWidgetIDSpace, kSelectSeparateMasterForEachPublicationRadioWidgetID, kCMMPrefix + 5)
DECLARE_PMID(kWidgetIDSpace, kFileBrowseDialogButtonWidgetID, kCMMPrefix + 6)
DECLARE_PMID(kWidgetIDSpace, kOpenNextDialogButtonWidgetID, kCMMPrefix + 7)
DECLARE_PMID(kWidgetIDSpace, kBackButtonWidgetID, kCMMPrefix + 8)
DECLARE_PMID(kWidgetIDSpace, kSecondCancelButtonWidgetID, kCMMPrefix + 9)
DECLARE_PMID(kWidgetIDSpace, kCreateMediaButtonWidgetID, kCMMPrefix + 10)
DECLARE_PMID(kWidgetIDSpace, kCMMMTemplateFileListBoxWidgetID, kCMMPrefix + 11)
//ListBoxElements
DECLARE_PMID(kWidgetIDSpace, kCMMPanelListParentWidgetId, kCMMPrefix + 12)
DECLARE_PMID(kWidgetIDSpace, kUnCheckIconWidgetID, kCMMPrefix + 13)
DECLARE_PMID(kWidgetIDSpace, kCheckIconWidgetID, kCMMPrefix + 14)
DECLARE_PMID(kWidgetIDSpace, kListBoxMasterFileNameTextWidgetID, kCMMPrefix + 15)
DECLARE_PMID(kWidgetIDSpace, kListBoxSelectTemplateFileButtonWidgetID, kCMMPrefix + 16)
DECLARE_PMID(kWidgetIDSpace, kPublicationNameTextWidgetID, kCMMPrefix + 17)
DECLARE_PMID(kWidgetIDSpace, kInvokeSubSectionOptionsDialogRollOverButtonButtonWidgetID, kCMMPrefix + 18)
DECLARE_PMID(kWidgetIDSpace, kCMMFirstGroupPanelWidgetID, kCMMPrefix + 19)
DECLARE_PMID(kWidgetIDSpace, kCMMSecondGroupPanelWidgetID, kCMMPrefix + 20)
DECLARE_PMID(kWidgetIDSpace, kInvokePublicationSelectionDialogWidgetID, kCMMPrefix + 21)
//spray settings dialog
DECLARE_PMID(kWidgetIDSpace, kCMMSpraySettingsDialogWidgetID, kCMMPrefix + 22)
DECLARE_PMID(kWidgetIDSpace, kVerticalSpacingWidgetID, kCMMPrefix + 23)
DECLARE_PMID(kWidgetIDSpace, kHorizontalSpacingWidgetID, kCMMPrefix + 24)
DECLARE_PMID(kWidgetIDSpace, kLeftToRightWidgetID, kCMMPrefix + 25)
DECLARE_PMID(kWidgetIDSpace, kAlternateValueWidgetID, kCMMPrefix + 26)
DECLARE_PMID(kWidgetIDSpace, kRightToLeftWidgetID, kCMMPrefix + 27)
DECLARE_PMID(kWidgetIDSpace, kVerticalFlowWidgetID, kCMMPrefix + 28)
DECLARE_PMID(kWidgetIDSpace, kHorizontalFlowWidgetID, kCMMPrefix + 29)
DECLARE_PMID(kWidgetIDSpace, kFirstGroupPanelCreateMediaButtonWidgetID, kCMMPrefix + 30)
//later additions
DECLARE_PMID(kWidgetIDSpace, kFirstCancelButtonWidgetID, kCMMPrefix + 31)
DECLARE_PMID(kWidgetIDSpace, kSpraySettingsDialogWidgetID, kCMMPrefix + 32)
//Zeroth Group Panel..added on 8 May 06
DECLARE_PMID(kWidgetIDSpace, kCMMZerothGroupPanelWidgetID, kCMMPrefix + 33)
DECLARE_PMID(kWidgetIDSpace, kRadioCreateMediaBySectionWidgetID, kCMMPrefix + 34)
DECLARE_PMID(kWidgetIDSpace, kRadioCreateMediaBySpreadWidgetID, kCMMPrefix + 35)
DECLARE_PMID(kWidgetIDSpace, kCMMPageSelectionGroupPanelWidgetID, kCMMPrefix + 36)
DECLARE_PMID(kWidgetIDSpace, kPageSelectionDropDownWidgetID, kCMMPrefix + 37)
DECLARE_PMID(kWidgetIDSpace, kCMMWidgetID, kCMMPrefix + 38)
DECLARE_PMID(kWidgetIDSpace, kSpreadSelectionCheckBoxWidgetID, kCMMPrefix + 39)
DECLARE_PMID(kWidgetIDSpace, kZerothCancelButtonWidgetID, kCMMPrefix + 40)
DECLARE_PMID(kWidgetIDSpace, kZerothNextButtonWidgetID, kCMMPrefix + 41)
//later 8th May 06
DECLARE_PMID(kWidgetIDSpace, kFirstGroupPanelBackButtonWidgetID, kCMMPrefix + 42)
DECLARE_PMID(kWidgetIDSpace, kSameTemplateTextWidgetID, kCMMPrefix + 43)
DECLARE_PMID(kWidgetIDSpace, kDifferentTemplateTextWidgetID, kCMMPrefix + 44)
//3Jun 06
DECLARE_PMID(kWidgetIDSpace,kSelectAllCheckBoxWidgetID, kCMMPrefix + 45)
DECLARE_PMID(kWidgetIDSpace, kSectionNameOnGroupPanelWidgetID, kCMMPrefix + 46)
DECLARE_PMID(kWidgetIDSpace, kSeparatorOnGrouPanelWidgetID, kCMMPrefix + 47)
DECLARE_PMID(kWidgetIDSpace, kTemplateFileNameOnGroupPanelWidgetID, kCMMPrefix + 48)
DECLARE_PMID(kWidgetIDSpace, kListBoxSeparatorWidgetID, kCMMPrefix + 49)
//added on 5Jun
DECLARE_PMID(kWidgetIDSpace, kFirstGroupPanelCancelButtonWidgetID, kCMMPrefix + 50)
//added by vijay on 8/8/2006
DECLARE_PMID(kWidgetIDSpace, kALLRadioButtonWidgetID, kCMMPrefix + 51)
DECLARE_PMID(kWidgetIDSpace, kItemStencilRadioButtonWidgetID, kCMMPrefix + 52)
DECLARE_PMID(kWidgetIDSpace, kALLRealEditBoxWidgetID, kCMMPrefix + 53)
DECLARE_PMID(kWidgetIDSpace, kItemStencilRealEditBoxWidgetID, kCMMPrefix + 54)
DECLARE_PMID(kWidgetIDSpace, kProductStencilRealEditBoxWidgetID, kCMMPrefix + 55)
DECLARE_PMID(kWidgetIDSpace, kALL1RollOverIconButtonWidgetID, kCMMPrefix + 56)
DECLARE_PMID(kWidgetIDSpace, kALL2RollOverIconButtonWidgetID, kCMMPrefix + 57)
DECLARE_PMID(kWidgetIDSpace, kItemStencil1RollOverIconButtonWidgetID, kCMMPrefix + 58)
DECLARE_PMID(kWidgetIDSpace, kItemStencil2RollOverIconButtonWidgetID, kCMMPrefix + 59)
DECLARE_PMID(kWidgetIDSpace, kProductStencil1RollOverIconButtonWidgetID, kCMMPrefix + 60)
DECLARE_PMID(kWidgetIDSpace, kProductStencil2RollOverIconButtonWidgetID, kCMMPrefix + 61)
//added on 27/10 by Tushar
DECLARE_PMID(kWidgetIDSpace, kFGPOneDocPerSectionRadioWidgetID, kCMMPrefix + 62)
DECLARE_PMID(kWidgetIDSpace, kFGPOneDocPerSpreadRadioWidgetID, kCMMPrefix + 63)
DECLARE_PMID(kWidgetIDSpace, kFGPOneDocPerPageRadioWidgetID, kCMMPrefix + 64)
DECLARE_PMID(kWidgetIDSpace, kTushar1MultilineTextWidgetID, kCMMPrefix + 65)
DECLARE_PMID(kWidgetIDSpace, kTushar2MultilineTextWidgetID, kCMMPrefix + 66)
DECLARE_PMID(kWidgetIDSpace, kTushar3MultilineTextWidgetID, kCMMPrefix + 67)
DECLARE_PMID(kWidgetIDSpace, kFGPPageOrderSelectionDropDownWidgetID, kCMMPrefix + 68)
DECLARE_PMID(kWidgetIDSpace, kFGPKeepSpreadsTogetherCheckBoxWidgetID, kCMMPrefix + 69)
DECLARE_PMID(kWidgetIDSpace, kFGPPlaceItemsOrProductsByPageAssignmentCheckBoxWidgetID, kCMMPrefix + 70)
DECLARE_PMID(kWidgetIDSpace, kTushar1GroupPanelWidgetID, kCMMPrefix + 71)
//added on 30/10 by Tushar
DECLARE_PMID(kWidgetIDSpace, kListBoxSelectMasterTemplateRollOverButtonWidgetID, kCMMPrefix + 72)
DECLARE_PMID(kWidgetIDSpace, kListBoxApplyMasterUnderneathElementsRollOverButtonWidgetID, kCMMPrefix + 73)
DECLARE_PMID(kWidgetIDSpace, kListBoxSelectStencilTemplateRollOverButtonWidgetID, kCMMPrefix + 74)
DECLARE_PMID(kWidgetIDSpace, kListBoxApplyAllStencilUnderneathElementsRollOverButtonWidgetID, kCMMPrefix + 75)
DECLARE_PMID(kWidgetIDSpace, kListBoxApplySpraySettingsUnderneathElementsRollOverButtonWidgetID, kCMMPrefix + 76)
DECLARE_PMID(kWidgetIDSpace, kListBoxStencilFileNameTextWidgetID, kCMMPrefix + 77)
DECLARE_PMID(kWidgetIDSpace, kFirstGroupPanelClusterPanelWidgetID, kCMMPrefix + 78)
//20/01/07...new additions for separate stencils for item & product.. 
DECLARE_PMID(kWidgetIDSpace, kListBoxItemFileNameTextWidgetID, kCMMPrefix + 79)
DECLARE_PMID(kWidgetIDSpace, kSeparatorOnGrouPanel1WidgetID, kCMMPrefix + 80)
DECLARE_PMID(kWidgetIDSpace, kListBoxSelectItemStencilRollOverButtonWidgetID, kCMMPrefix + 81)
DECLARE_PMID(kWidgetIDSpace, kSeparatorOnGrouPanel2WidgetID, kCMMPrefix + 82)
DECLARE_PMID(kWidgetIDSpace, kItemTemplateFileNameOnGroupPanelWidgetID, kCMMPrefix + 83)
DECLARE_PMID(kWidgetIDSpace, kInvokeSubSectionOptionsDialogRollOverButtonButton1WidgetID, kCMMPrefix + 84)
DECLARE_PMID(kWidgetIDSpace, kListBoxApplyItemStencilUnderneathElementsRollOverButtonWidgetID, kCMMPrefix + 85)
DECLARE_PMID(kWidgetIDSpace, kInvokeSubSectionOptionsDialogRollOverButtonGreenButtonWidgetID, kCMMPrefix + 86)//19-jan
DECLARE_PMID(kWidgetIDSpace, kTextFirstPageWidgetID, kCMMPrefix + 87)//20-jan 
DECLARE_PMID(kWidgetIDSpace, kCMMVerticalWidgetID, kCMMPrefix + 88)
DECLARE_PMID(kWidgetIDSpace, kCMMHorizontalWidgetID, kCMMPrefix + 89)
//DECLARE_PMID(kWidgetIDSpace, kSeparatorOnGrouPanel3WidgetID, kCMMPrefix + 90)
DECLARE_PMID(kWidgetIDSpace, kCMMWhiteBoardRadioWidgetID, kCMMPrefix + 91)
DECLARE_PMID(kWidgetIDSpace, kCMMVerticalFlowRadioButtonWidgetID,		kCMMPrefix + 92)
DECLARE_PMID(kWidgetIDSpace, kCMMAltVerticalFLowRadioButtonWidgetID,	kCMMPrefix + 93)
DECLARE_PMID(kWidgetIDSpace, kCMMHorzintalFlowRadioButtonWidgetID,		kCMMPrefix + 94)
DECLARE_PMID(kWidgetIDSpace, kCMMAltHorzintalFlowRadioButtonWidgetID,	kCMMPrefix + 95)
DECLARE_PMID(kWidgetIDSpace, kSeparatorOnGrouPanel4WidgetID,	kCMMPrefix + 96)
DECLARE_PMID(kWidgetIDSpace, kProductTemplateFileNameOnGroupPanelWidgetID,	kCMMPrefix + 97)
DECLARE_PMID(kWidgetIDSpace, kFlowFileNameOnGroupPanelWidgetID,	kCMMPrefix + 98)
DECLARE_PMID(kWidgetIDSpace, kSeparatorOnGroupPanel5WidgetID,	kCMMPrefix + 99)
DECLARE_PMID(kWidgetIDSpace, kProductTemplateFileNameOnGroupPanelForManualWidgetID,	kCMMPrefix + 100)
DECLARE_PMID(kWidgetIDSpace, kSeparatorOnGrouPanel2ForManualWidgetID,	kCMMPrefix + 101)
DECLARE_PMID(kWidgetIDSpace, kItemTemplateFileNameOnGroupPanelForManualWidgetID,	kCMMPrefix + 102)
DECLARE_PMID(kWidgetIDSpace, kListBoxProductStencilFileNameTextForManualWidgetID,	kCMMPrefix + 103)
DECLARE_PMID(kWidgetIDSpace, kSeparatorOnGrouPanel1ForManualWidgetID,	kCMMPrefix + 104)
DECLARE_PMID(kWidgetIDSpace, kListBoxItemStencilFileNameTextForManualWidgetID,	kCMMPrefix + 105)
DECLARE_PMID(kWidgetIDSpace, kFolderPathWidgetID,	kCMMPrefix + 106)
DECLARE_PMID(kWidgetIDSpace, kListBoxHybridTableFileNameTextWidgetID, kCMMPrefix + 107)
DECLARE_PMID(kWidgetIDSpace, kListBoxSelectHybridTableStencilRollOverButtonWidgetID, kCMMPrefix + 108)
DECLARE_PMID(kWidgetIDSpace, kListBoxApplyHybridTableStencilUnderneathElementsRollOverButtonWidgetID, kCMMPrefix + 109)
DECLARE_PMID(kWidgetIDSpace, kIsSprayItemPerFrameWidgetID, kCMMPrefix + 110)
DECLARE_PMID(kWidgetIDSpace, kIsHorizontalFlowForAllImageSprayWidgetID, kCMMPrefix + 111)
DECLARE_PMID(kWidgetIDSpace, kHybridTemplateFileNameOnGroupPanelWidgetID, kCMMPrefix + 112)
DECLARE_PMID(kWidgetIDSpace, kSeparatorOnGrouPanel5WidgetID, kCMMPrefix + 113)
DECLARE_PMID(kWidgetIDSpace, kSeparatorOnGrouPanel6WidgetID, kCMMPrefix + 114)
DECLARE_PMID(kWidgetIDSpace, kCMMPriceBookRadioWidgetID, kCMMPrefix + 115)
DECLARE_PMID(kWidgetIDSpace, kListBoxSectionFileNameTextWidgetID, kCMMPrefix + 116)
DECLARE_PMID(kWidgetIDSpace, kPriceBookMultilineTextWidgetID, kCMMPrefix + 117)
DECLARE_PMID(kWidgetIDSpace, kListBoxSelectSectionStencilRollOverButtonWidgetID, kCMMPrefix + 118)
DECLARE_PMID(kWidgetIDSpace, kListBoxSectionStencilUnderneathElementsRollOverButtonWidgetID, kCMMPrefix + 119)
//DECLARE_PMID(kWidgetIDSpace, kSeparatorOnPriceGrouPanel4WidgetID, kCMMPrefix + 120)
DECLARE_PMID(kWidgetIDSpace, kAddSectionStencilWidgetID, kCMMPrefix + 121)
DECLARE_PMID(kWidgetIDSpace, kAtTheStartOfSectionWidgetID, kCMMPrefix + 122)
DECLARE_PMID(kWidgetIDSpace, kAtTheStartOfEachPageWidgetID, kCMMPrefix + 123)
DECLARE_PMID(kWidgetIDSpace, kAtTheStartOfFirstPageAndSpreadWidgetID, kCMMPrefix + 124)
DECLARE_PMID(kWidgetIDSpace, kspraySectionWithOutPageBreakWidgetID, kCMMPrefix + 125)
DECLARE_PMID(kWidgetIDSpace, kSectionTemplateFileNameOnGroupPanelWidgetID, kCMMPrefix + 126)
DECLARE_PMID(kWidgetIDSpace, kSeparatorOnGrouPanel7WidgetID, kCMMPrefix + 127)
DECLARE_PMID(kWidgetIDSpace, kCMMSpecSheetRadioWidgetID, kCMMPrefix + 128)
DECLARE_PMID(kWidgetIDSpace, kSpecSheetMultilineTextWidgetID, kCMMPrefix + 129)
DECLARE_PMID(kWidgetIDSpace, kCMMExportOptionsDialogWidgetID, kCMMPrefix + 130)
DECLARE_PMID(kWidgetIDSpace, kAutomaticallyExportToPDFWidgetID, kCMMPrefix + 131)
DECLARE_PMID(kWidgetIDSpace, kTemplateFileNameOnGroupPanelForSpecSheetWidgetID, kCMMPrefix + 132)
DECLARE_PMID(kWidgetIDSpace, kListBoxStencilFileNameForSpecSheetTextWidgetID, kCMMPrefix + 133)
DECLARE_PMID(kWidgetIDSpace, kCreateDocPerItemTextWidgetID, kCMMPrefix + 134)
//--------
DECLARE_PMID(kWidgetIDSpace, kItemsOrProductTextWidgetID, kCMMPrefix + 135)
DECLARE_PMID(kWidgetIDSpace, kSectionSprayForProductOrItemTextWidgetID, kCMMPrefix + 136)
DECLARE_PMID(kWidgetIDSpace, kSprayPerItemFrameStaticTextWidgetID, kCMMPrefix + 137)
DECLARE_PMID(kWidgetIDSpace, kSelectPublicationWidgetID, kCMMPrefix + 138)
DECLARE_PMID(kWidgetIDSpace, kCreateMediaPublicationWidgetID, kCMMPrefix + 139)
DECLARE_PMID(kWidgetIDSpace, kCMMOKButtonWidgetID, kCMMPrefix + 140)
DECLARE_PMID(kWidgetIDSpace, kCMMCancelButton_WidgetID, kCMMPrefix + 141)

//DECLARE_PMID(kWidgetIDSpace, kSectionSelectionDropDownListWidgetID, kCMMPrefix + 142)
//DECLARE_PMID(kWidgetIDSpace, kSectionSelectionDropDownListLevel4WidgetID, kCMMPrefix + 143)
//DECLARE_PMID(kWidgetIDSpace, kSectionSelectionDropDownListLevel5WidgetID, kCMMPrefix + 144)

//DECLARE_PMID(kWidgetIDSpace, kLevel1TextWidgetID, kCMMPrefix + 145)
//DECLARE_PMID(kWidgetIDSpace, kLevel2TextWidgetID, kCMMPrefix + 146)
//DECLARE_PMID(kWidgetIDSpace, kLevel3TextWidgetID, kCMMPrefix + 147)
DECLARE_PMID(kWidgetIDSpace, kTushar_3MultilineTextWidgetID, kCMMPrefix + 148)
DECLARE_PMID(kWidgetIDSpace, kCMMPubNameWidgetID, kCMMPrefix + 149)
DECLARE_PMID(kWidgetIDSpace, kCMMShowPubTreeWidgetID, kCMMPrefix + 150)




// "About Plug-ins" sub-menu:
#define kCMMAboutMenuKey			kCMMStringPrefix "kCMMAboutMenuKey"
#define kCMMAboutMenuPath		kSDKDefStandardAboutMenuPath kCMMCompanyKey

// "Plug-ins" sub-menu:
#define kCMMPluginsMenuKey 		kCMMStringPrefix "kCMMPluginsMenuKey"
#define kCMMPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kCMMCompanyKey kSDKDefDelimitMenuPath kCMMPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kCMMAboutBoxStringKey	kCMMStringPrefix "kCMMAboutBoxStringKey"
#define kCMMTargetMenuPath kCMMPluginsMenuPath

//added by avinash
#define kCMMCreateMediaStringKey	kCMMStringPrefix "kCMMCreateMediaStringKey"
#define kCMMCreatePDFStringKey	kCMMStringPrefix "kCMMCreatePDFStringKey"
#define kCMMEmptyStringKey	kCMMStringPrefix "kCMMEmptyStringKey"
#define kCMMBySpreadsStringKey	kCMMStringPrefix "kCMMBySpreadsStringKey"
#define kCMMBySectionsStringKey	kCMMStringPrefix "kCMMBySectionsStringKey"
#define kCMMFirstPageStringKey	kCMMStringPrefix "kCMMFirstPageStringKey"
#define kCMMSpreadTogetherStringKey	kCMMStringPrefix "kCMMSpreadTogetherStringKey"
#define kCMMCancelStringKey	kCMMStringPrefix "kCMMCancelStringKey"
#define kCMMNextStringKey	kCMMStringPrefix "kCMMNextStringKey"
#define kCMMSelectPublicationStringKey	kCMMStringPrefix "kCMMSelectPublicationStringKey"
#define kCMMCreateMediaPublicationStringKey	kCMMStringPrefix "kCMMCreateMediaPublicationStringKey"
#define kCMM1docPerSectionStringKey	kCMMStringPrefix "kCMM1docPerSectionStringKey"
#define kCMMBlankSpaceStringKey	kCMMStringPrefix "kCMMBlankSpaceStringKey"
#define kCMM1DocPerSpreadStringKey	kCMMStringPrefix "kCMM1DocPerSpreadStringKey"
#define kCMMUseThisOptionStringKey	kCMMStringPrefix "kCMMUseThisOptionStringKey"
#define kCMM1docperpageStringKey	kCMMStringPrefix "kCMM1docperpageStringKey"
#define kCMMUseThisOptionFor1pageStringKey	kCMMStringPrefix "kCMMUseThisOptionFor1pageStringKey"
#define kCMMDocPerSectionOnWhiteboardStringKey	kCMMStringPrefix "kCMMDocPerSectionOnWhiteboardStringKey"
#define kCMMCustDesignedfromWhiteboardStringKey	kCMMStringPrefix "kCMMCustDesignedfromWhiteboardStringKey"
#define kCMMCreatePriceBookStringKey	kCMMStringPrefix "kCMMCreatePriceBookStringKey"
#define kCMMSpecSheetsinBulkStringKey	kCMMStringPrefix "kCMMSpecSheetsinBulkStringKey"
#define kCMMNext2StringKey	kCMMStringPrefix "kCMMNext2StringKey"
#define kCMMSelectallStringKey	kCMMStringPrefix "kCMMSelectallStringKey"
#define kCMMFolderpathStringKey	kCMMStringPrefix "kCMMFolderpathStringKey"
#define kCMMSectionStringKey	kCMMStringPrefix "kCMMSectionStringKey"
#define kCMMMasterpageStringKey	kCMMStringPrefix "kCMMMasterpageStringKey"
#define kCMMTemplateStringKey	kCMMStringPrefix "kCMMTemplateStringKey"
#define kCMMFlowStringKey	kCMMStringPrefix "kCMMFlowStringKey"
#define kCMMSpraySettingsStringKey	kCMMStringPrefix "kCMMSpraySettingsStringKey"
#define kCMMVerticalPointsStringKey	kCMMStringPrefix "kCMMVerticalPointsStringKey"
#define kCMMHorizontalPointsStringKey	kCMMStringPrefix "kCMMHorizontalPointsStringKey"
#define kCMMSpreadFlowStringKey	kCMMStringPrefix "kCMMSpreadFlowStringKey"
#define kCMMHorizontalFlowStringKey	kCMMStringPrefix "kCMMHorizontalFlowStringKey"
#define kCMMSpraysectionwithoutPageBreakStringKey	kCMMStringPrefix "kCMMSpraysectionwithoutPageBreakStringKey"
#define kCMMAtstartofsectionStringKey	kCMMStringPrefix "kCMMAtstartofsectionStringKey"
#define kCMMAtstartofEachPageStringKey	kCMMStringPrefix "kCMMAtstartofEachPageStringKey"
#define kCMMSelectMasterFileStringKey	kCMMStringPrefix "kCMMSelectMasterFileStringKey"
#define kCMMSelectProductFileNameStringKey	kCMMStringPrefix "kCMMSelectProductFileNameStringKey"
#define kCMMSelectTemplateFileNameStringKey	kCMMStringPrefix "kCMMSelectTemplateFileNameStringKey"
#define kCMMSelectExportToPDFStringKey	kCMMStringPrefix "kCMMSelectExportToPDFStringKey"

#define kCMMSelectMasterStringKey	kCMMStringPrefix "kCMMSelectMasterStringKey"
#define kCMMusethesamemasterStringKey	kCMMStringPrefix "kCMMusethesamemasterStringKey"
#define kCMMSelectProductTemplateStringKey	kCMMStringPrefix "kCMMSelectProductTemplateStringKey"




// till here

// Menu item positions:

#define kCMMDialogTitleKey kCMMStringPrefix "kCMMDialogTitleKey"
// "Plug-ins" sub-menu item key for dialog:
#define kCMMDialogMenuItemKey kCMMStringPrefix "kCMMDialogMenuItemKey"
// "Plug-ins" sub-menu item position for dialog:
#define kCMMDialogMenuItemPosition	12.0


// Initial data format version numbers
#define kCMMFirstMajorFormatNumber  RezLong(1)
#define kCMMFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kCMMCurrentMajorFormatNumber kCMMFirstMajorFormatNumber
#define kCMMCurrentMinorFormatNumber kCMMFirstMinorFormatNumber

#define kCMMPNGBackIconRsrcID              11111
#define kCMMPNGBackIconRollRsrcID          11111
#define kCMMPNGCancel1IconRsrcID           11112
#define kCMMPNGCancel1IconRollRsrcID       11112
#define kCMMPNGCreatemediaIconRsrcID       11113 
#define kCMMPNGCreatemediaIconRollRsrcID   11113
#define kCMMPNGCancelIconRsrcID			   11114
#define kCMMPNGCancelIconRollRsrcID		   11114
#define kCMMPNGNextIconRsrcID              11115
#define kCMMPNGNextIconRollRsrcID          11115
#define kCMMPNGOkIconRsrcID                11116  
#define kCMMPNGOkIconRollRsrcID            11116
#define kCMMPNGCancel2IconRsrcID           11117
#define kCMMPNGCancel2IconRollRsrcID       11117
#define kCMMPNGSelectEventRsrcID           11118
#define kCMMPNGSelectEventRollRsrcID       11118
#define kCMMPNGFileBrowserIconRsrcID       11119
#define kCMMPNGFileBrowserIconRollRsrcID   11119

//ResourceIDs
#define kCMMMTemplateFileListElementRsrcID 10000
#define kCMMExportOptionsDialogResourceID 10001

#define kCheckIconID						11000
#define kUncheckIconID						11001
#define kOpenfileIconID						11003
#define kFileBrowserIcon					11004

//added on 30/10 by Tushar
#define kApplyUnderneathElementsIconID		11005
#define kAppliedSettingsIconID				11006
//13-april
#define kCMMHorizontalWidgetIconID			11007
#define kRsrcCMMHorizontalWidgetIconID		11007

#define kCMMVerticalWidgetIconID			11008
#define kRsrcCMMVerticalWidgetIconID		11008
//13-april
#define kCMMVerticalWidget1IconID			11009
#define kRsrcCMMVerticalWidget1IconID		11009
#define kCMMHorizontalWidget1IconID			11010
#define kRsrcCMMHorizontalWidget1IconID		11010

#define kCMMSpraySettingsDialogResourceID	22222//88888
#define kSpraySettingsDialogResourceID		33333//95595

#endif // __CMMID_h__

//  Code generated by DollyXs code generator
