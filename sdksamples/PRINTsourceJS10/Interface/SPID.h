//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __SPID_h__
#define __SPID_h__

#include "SDKDef.h"

// Company:
#define kSPCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kSPCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kSPPluginName	"Content Sprayer"			// Name of this plug-in.
#define kSPPrefixNumber	0xDB2988 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kSPVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kSPAuthor		"Apsiva Inc"					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kSPPrefixNumber above to modify the prefix.)
#define kSPPrefix		RezLong(kSPPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kSPStringPrefix	SDK_DEF_STRINGIZE(kSPPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kSPMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kSPMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kSPPluginID, kSPPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kSPActionComponentBoss, kSPPrefix + 0)
DECLARE_PMID(kClassIDSpace, kSPPanelWidgetBoss, kSPPrefix + 1)
DECLARE_PMID(kClassIDSpace, kSPDialogBoss,	        		 kSPPrefix + 2)
DECLARE_PMID(kClassIDSpace, kSPListBoxWidgetBoss,			 kSPPrefix + 3)
DECLARE_PMID(kClassIDSpace, kSPLoginEventsHandler,		     kSPPrefix + 5)
DECLARE_PMID(kClassIDSpace, kSPPanelActionFilterBoss,	     kSPPrefix + 9)
DECLARE_PMID(kClassIDSpace, kPFTextWidgetBoss ,				 kSPPrefix + 10)
DECLARE_PMID(kClassIDSpace, kSPHTILstboxMultilineTxtWidgetBoss ,kSPPrefix + 11)
DECLARE_PMID(kClassIDSpace, kSPContentSprayerBoss ,			 kSPPrefix + 12)
DECLARE_PMID(kClassIDSpace, kSPCustomPictureWidgetBoss , kSPPrefix + 13)//----CS5---
//////////////////////// ThumbNail View /////////////////////////////////
DECLARE_PMID(kClassIDSpace, kSPXLibraryItemViewPanelBoss ,	 kSPPrefix + 14)
DECLARE_PMID(kClassIDSpace, kSPXLibraryItemGridBoss		 ,	 kSPPrefix + 15)
DECLARE_PMID(kClassIDSpace, kSPXLibraryItemButtonBoss	 ,	 kSPPrefix + 16)
DECLARE_PMID(kClassIDSpace, kSPProductImageIFaceBoss	 ,	 kSPPrefix + 17)
//////////////////////// ThumbNail View Ends /////////////////////////////
DECLARE_PMID(kClassIDSpace, kSPToolTipTextWidgetBoss	 ,	 kSPPrefix + 18)
DECLARE_PMID(kClassIDSpace, kSPDropDownListWidgetBoss	 ,	 kSPPrefix + 19)
DECLARE_PMID(kClassIDSpace, kSelectCustomerDialogBoss	 ,	 kSPPrefix + 20)
DECLARE_PMID(kClassIDSpace, kPicIcoPictureWidgetBoss	 ,	 kSPPrefix + 21)

DECLARE_PMID(kClassIDSpace, kSPTreeViewWidgetBoss		,	 kSPPrefix + 22)
DECLARE_PMID(kClassIDSpace, kSPTreeNodeWidgetBoss		,	 kSPPrefix + 23)



// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_ISPSPRAYER,					kSPPrefix + 0)
DECLARE_PMID(kInterfaceIDSpace, IID_ITEXTMISCELLANYSUITEE,		kSPPrefix + 1)
DECLARE_PMID(kInterfaceIDSpace, IID_ITBLBSCSUITE,				kSPPrefix + 2)
DECLARE_PMID(kInterfaceIDSpace, IID_ICONTENTSPRAYER,			kSPPrefix + 3)//Added By Dattatray on 20/09 to refresh productfinder plugin
//////////////////// For ThumbNail View //////////////////////////////////
DECLARE_PMID(kInterfaceIDSpace, IID_ISPXLIBRARYVIEWCONTROLLER , kSPPrefix + 4)
DECLARE_PMID(kInterfaceIDSpace, IID_ISPXLIBRARYBUTTONDATA,		kSPPrefix + 5)
DECLARE_PMID(kInterfaceIDSpace, IID_ISPPRODUCTIMAGEIFACE,		kSPPrefix + 6)
/////////////////////////////////////////////////////////////////////////
DECLARE_PMID(kInterfaceIDSpace, IID_ISPSHADOWEVENTHANDLER,		kSPPrefix + 7)



// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kSPDialogControllerImpl,				kSPPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kSPDialogObserverImpl,					kSPPrefix + 1 )
DECLARE_PMID(kImplementationIDSpace, kSPActionComponentImpl,				kSPPrefix + 2 )
DECLARE_PMID(kImplementationIDSpace, kSPSelectionObserverImpl,				kSPPrefix + 3 )
DECLARE_PMID(kImplementationIDSpace, kSPListBoxObserverImpl,				kSPPrefix + 4 )
DECLARE_PMID(kImplementationIDSpace, kSPSprayerInterfaceImpl,				kSPPrefix + 5)
DECLARE_PMID(kImplementationIDSpace, kSPLoginEventsHandlerImpl,				kSPPrefix + 6)
DECLARE_PMID(kImplementationIDSpace, kTextMiscellanySuiteASBImpl,			kSPPrefix + 8)
DECLARE_PMID(kImplementationIDSpace, kTextMiscellanySuiteLayoutCSBImpl,		kSPPrefix + 9)
DECLARE_PMID(kImplementationIDSpace, kTblBscSuiteASBImpl,					kSPPrefix +10)
DECLARE_PMID(kImplementationIDSpace, kTblBscSuiteTextCSBImpl,				kSPPrefix+ 11)
DECLARE_PMID(kImplementationIDSpace, kSPActionFilterImpl,					kSPPrefix+ 18)
DECLARE_PMID(kImplementationIDSpace, kPFSnipRunControlViewImpl ,			kSPPrefix+ 19)
DECLARE_PMID(kImplementationIDSpace, DoubleClickPFEventHandlerImpl ,		kSPPrefix+ 20)
DECLARE_PMID(kImplementationIDSpace, kContentSprayerImpl  ,					kSPPrefix+ 21)//Added By Dattatray on 20/09 to refresh productfinder plugin
DECLARE_PMID(kImplementationIDSpace, kSPCustomPictureWidgetEHImpl  ,		kSPPrefix+ 22)//Added By Rahul
////////////////// For ThumbNail View ///////////////////////
DECLARE_PMID(kImplementationIDSpace, kSPXLibraryItemViewPanelImpl  ,		kSPPrefix+ 23)
DECLARE_PMID(kImplementationIDSpace, kSPXLibraryItemGridPanelImpl  ,		kSPPrefix+ 24)
DECLARE_PMID(kImplementationIDSpace, kSPXLibraryItemGridEHImpl  ,			kSPPrefix+ 25)
DECLARE_PMID(kImplementationIDSpace, kSPXLibraryItemGridDDSourceImpl  ,		kSPPrefix+ 26)
DECLARE_PMID(kImplementationIDSpace, kSPXLibraryItemListBoxControllerImpl ,	kSPPrefix+ 27)
DECLARE_PMID(kImplementationIDSpace, kSPXLibraryItemGridControllerImpl  ,	kSPPrefix+ 28)
DECLARE_PMID(kImplementationIDSpace, kSPXLibraryItemGridPanoramaImpl  ,		kSPPrefix+ 29)
DECLARE_PMID(kImplementationIDSpace, kSPMyGridTipImpl  ,					kSPPrefix+ 30)
DECLARE_PMID(kImplementationIDSpace, kSPXLibraryItemButtonImpl  ,			kSPPrefix+ 31)
DECLARE_PMID(kImplementationIDSpace, kSPXLibraryButtonDataImpl  ,			kSPPrefix+ 32)
DECLARE_PMID(kImplementationIDSpace, kSPMyTipImpl  ,						kSPPrefix+ 33)
DECLARE_PMID(kImplementationIDSpace, kSPProductImageIFaceImpl  ,			kSPPrefix+ 34)
DECLARE_PMID(kImplementationIDSpace, kSelectCustomerDialogControllerImpl  ,	kSPPrefix+ 35)
DECLARE_PMID(kImplementationIDSpace, kSelectCustomerDialogObserverImpl  ,	kSPPrefix+ 36)
DECLARE_PMID(kImplementationIDSpace, kSPTreeViewHierarchyAdapterImpl  ,		kSPPrefix+ 37)
DECLARE_PMID(kImplementationIDSpace, kSPTreeViewWidgetMgrImpl  ,			kSPPrefix+ 38)
DECLARE_PMID(kImplementationIDSpace, kSPTreeObserverImpl  ,					kSPPrefix+ 39)
DECLARE_PMID(kImplementationIDSpace, kSPTreeNodeEHImpl  ,					kSPPrefix+ 40)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kSPAboutActionID,			kSPPrefix + 0)
DECLARE_PMID(kActionIDSpace, kSPPanelWidgetActionID,	kSPPrefix + 1)
DECLARE_PMID(kActionIDSpace, kSPSeparator1ActionID,		kSPPrefix + 2)
DECLARE_PMID(kActionIDSpace, kSPPopupAboutThisActionID,	kSPPrefix + 3)
DECLARE_PMID(kActionIDSpace, kSPDialogActionID,			kSPPrefix + 4)
DECLARE_PMID(kActionIDSpace, kSPMenuItem1ActionID,		kSPPrefix + (10 + 1))
DECLARE_PMID(kActionIDSpace, kSPPanelPSMenuActionID,	kSPPrefix + (10 + 2))
DECLARE_PMID(kActionIDSpace, kSPProductOnlyActionID,	kSPPrefix + (10 + 3))
DECLARE_PMID(kActionIDSpace, kSPItemOnlyActionID,		kSPPrefix + (10 + 4))
DECLARE_PMID(kActionIDSpace, kSPALLActionID,			kSPPrefix + (10 + 5))
DECLARE_PMID(kActionIDSpace, kSPSeparatorActionID,						kSPPrefix + (10 + 6))
DECLARE_PMID(kActionIDSpace, kSPIndividualItemStencilSprayActionID,		kSPPrefix + (10 + 7))
DECLARE_PMID(kActionIDSpace, kSPIndividualItemStencilHorizFlowActionID,	kSPPrefix + (10 + 8))
////////////////// For ThumbNail View ///////////////////////
DECLARE_PMID(kActionIDSpace, kSPSeparatorActionID1,	kSPPrefix + (10 + 9))
DECLARE_PMID(kActionIDSpace, kSPThumbViewActionID,	kSPPrefix + (10 + 10))
DECLARE_PMID(kActionIDSpace, kSPListViewActionID,	kSPPrefix + (10 + 11))
////////////////// ThumbNail View Ends //////////////////////
DECLARE_PMID(kActionIDSpace, kSPHybridTableOnlyActionID,	kSPPrefix + (10 + 12))
DECLARE_PMID(kActionIDSpace, kSPShowSearchPanelID,	kSPPrefix + (10 + 13))
DECLARE_PMID(kActionIDSpace, kSPLeadingItemActionID,	kSPPrefix + (10 + 14))

DECLARE_PMID(kActionIDSpace, kSPSingleItemActionID,	kSPPrefix + (10 + 15))

DECLARE_PMID(kActionIDSpace, kSPTemplateInfoActionID,	kSPPrefix + (10 + 16))//for ADLC
DECLARE_PMID(kActionIDSpace, kSPSeparatorActionID2,	kSPPrefix + (10 + 17))
DECLARE_PMID(kActionIDSpace, kSPSeparatorActionID3,	kSPPrefix + (10 + 18))


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kSPPanelWidgetID,					kSPPrefix + 0)
DECLARE_PMID(kWidgetIDSpace, kSPDialogWidgetID, 				kSPPrefix + 1)
DECLARE_PMID(kWidgetIDSpace, kSPSectionDropDownWidgetID,		kSPPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kSPPubNameWidgetID,				kSPPrefix +3)
DECLARE_PMID(kWidgetIDSpace, kSPShowPubTreeWidgetID,			kSPPrefix +4)
DECLARE_PMID(kWidgetIDSpace, kAssignButtonWidgetID,				kSPPrefix +5)
DECLARE_PMID(kWidgetIDSpace, kSPSubSectionDropDownWidgetID,		kSPPrefix +6) // added on jan 30 2006 ..
DECLARE_PMID(kWidgetIDSpace, kSPSubSectionSprayButtonWidgetID,	kSPPrefix +7)
DECLARE_PMID(kWidgetIDSpace, kSPShowWhiteBoardWidgetID,			kSPPrefix +8)
DECLARE_PMID(kWidgetIDSpace, kSPRefreshWidgetID,				kSPPrefix +9)
DECLARE_PMID(kWidgetIDSpace, kSPSprayButtonWidgetID,			kSPPrefix +10)
DECLARE_PMID(kWidgetIDSpace, kSPTreeListBoxWidgetID,				kSPPrefix +11)
DECLARE_PMID(kWidgetIDSpace, kSPListParentWidgetId,				kSPPrefix +12)
DECLARE_PMID(kWidgetIDSpace, kSPTextWidgetID,					kSPPrefix +13)
DECLARE_PMID(kWidgetIDSpace, kSecondScreenWidgetID,				kSPPrefix +14)
DECLARE_PMID(kWidgetIDSpace, kSDFilterOptionListWidgetID,		kSPPrefix +15)
DECLARE_PMID(kWidgetIDSpace, kFilterButtonWidgetID,				kSPPrefix +16)
DECLARE_PMID(kWidgetIDSpace, kSPRFltOPtListParentWidgetId,		kSPPrefix +17)
DECLARE_PMID(kWidgetIDSpace, kSPRUnCheckIconWidgetID,			kSPPrefix +18)
DECLARE_PMID(kWidgetIDSpace, kSPRCheckIconWidgetID,				kSPPrefix +19)
DECLARE_PMID(kWidgetIDSpace, kSPRFltOptTextWidgetID,			kSPPrefix +20)
DECLARE_PMID(kWidgetIDSpace, kNewProductsCheckWidgetID,			kSPPrefix +21)
DECLARE_PMID(kWidgetIDSpace, kUpdateCopyCheckWidgetID,			kSPPrefix +22)
DECLARE_PMID(kWidgetIDSpace, kUpdateArtCheckWidgetID,			kSPPrefix +23)
DECLARE_PMID(kWidgetIDSpace, kUpdateItemCheckWidgetID,			kSPPrefix +24)
DECLARE_PMID(kWidgetIDSpace, kAddToSpreadWidgetID,				kSPPrefix +25)
DECLARE_PMID(kWidgetIDSpace, kUpdateSpreadWidgetID,				kSPPrefix +26)
DECLARE_PMID(kWidgetIDSpace, kDeleteFromSpreadWidgetID,			kSPPrefix +27)
DECLARE_PMID(kWidgetIDSpace, kDialogClearButtonWidgetID,		kSPPrefix +28)

DECLARE_PMID(kWidgetIDSpace, kMerchItemsCompleteWidgetID,		kSPPrefix +29)
DECLARE_PMID(kWidgetIDSpace, kDesignCompleteWidgetID,			kSPPrefix +30)
DECLARE_PMID(kWidgetIDSpace, kCopyCompleteWidgetID,				kSPPrefix +31)
DECLARE_PMID(kWidgetIDSpace, kArtCompleteWidgetID,				kSPPrefix +32)
DECLARE_PMID(kWidgetIDSpace, kGreenFilterButtonWidgetID,		kSPPrefix +33)


DECLARE_PMID(kWidgetIDSpace, kFilterByMilestonesRadioWidgetID,			kSPPrefix +34)
DECLARE_PMID(kWidgetIDSpace, kFilterByDesignerActionsRadioWidgetID,		kSPPrefix +35)
DECLARE_PMID(kWidgetIDSpace, kShowAllProductsRadioWidgetID,				kSPPrefix +36)
DECLARE_PMID(kWidgetIDSpace, kSPSetDesignerMilestoneWidgetID,			kSPPrefix +37)
DECLARE_PMID(kWidgetIDSpace, kPRLPreviewButtonWidgetID,					kSPPrefix +38)
DECLARE_PMID(kWidgetIDSpace, kSPItemGroupStaticTextWidgetID,			kSPPrefix +39)
DECLARE_PMID(kWidgetIDSpace, kSPItemStaticTextWidgetID,			        kSPPrefix +40)
DECLARE_PMID(kWidgetIDSpace, kSPTableStaticTextWidgetID,			    kSPPrefix +41)
DECLARE_PMID(kWidgetIDSpace, kSPTableStaticTextThumbWidgetID,			    kSPPrefix +42)
DECLARE_PMID(kWidgetIDSpace, kSPItemGroupStaticTextThumbWidgetID,			    kSPPrefix +43)
DECLARE_PMID(kWidgetIDSpace, kSPItemStaticTextThumbWidgetID,			    kSPPrefix +44)

//New Addition
//DECLARE_PMID(kWidgetIDSpace,    kLocateButtonWidgetID,					kSPPrefix +39) //--COMMENTED ON 25 FEB 2006 --
DECLARE_PMID(kWidgetIDSpace,	kUserDropDownWidgetID,					kSPPrefix +50)
DECLARE_PMID(kWidgetIDSpace,	kRoleDropDownWidgetID,					kSPPrefix +51)
DECLARE_PMID(kWidgetIDSpace,	kCompleteRadioWidgetID,					kSPPrefix +52)
DECLARE_PMID(kWidgetIDSpace,    kDueRadioWidgetID,						kSPPrefix +53)
DECLARE_PMID(kWidgetIDSpace,    kClusterPanelWidgetID,					kSPPrefix +54)
DECLARE_PMID(kWidgetIDSpace,    kSPSectionDropDown_1WidgetID,			kSPPrefix +547)
DECLARE_PMID(kWidgetIDSpace,    kSectionTextWidgetID       ,			kSPPrefix +548)
DECLARE_PMID(kWidgetIDSpace,    kSubSectionTextWidgetID,			    kSPPrefix +549)
DECLARE_PMID(kWidgetIDSpace,    kSectionText_1WidgetID     ,			kSPPrefix +550)
DECLARE_PMID(kWidgetIDSpace,    kFirstGroupPanelWidgetID     ,			kSPPrefix +551)
DECLARE_PMID(kWidgetIDSpace,    kSecondGroupPanelWidgetID     ,			kSPPrefix +552)
DECLARE_PMID(kWidgetIDSpace,    kFilterByMilestoneTextWidgetID     ,	kSPPrefix +553)
DECLARE_PMID(kWidgetIDSpace,    kFilterByDesignerMilestoneTextWidgetID ,kSPPrefix +554)
DECLARE_PMID(kWidgetIDSpace,    kShowAllTextWidgetID ,                  kSPPrefix +555)
// ,

//
//added on 11.2.5  New Combinations of Icons.
/*DECLARE_PMID(kWidgetIDSpace,	kNewAddIcon,			kSPPrefix +34)
DECLARE_PMID(kWidgetIDSpace,	kNewUpdateIcon,			kSPPrefix +35)
DECLARE_PMID(kWidgetIDSpace,	kNewDeleteIcon,			kSPPrefix +36)
DECLARE_PMID(kWidgetIDSpace,	kCopyArtIcon,			kSPPrefix +37)
DECLARE_PMID(kWidgetIDSpace,	kCopyItemIcon,			kSPPrefix +38)
DECLARE_PMID(kWidgetIDSpace,	kArtItemIcon,			kSPPrefix +39)
DECLARE_PMID(kWidgetIDSpace,	kCopyArtItemIcon,		kSPPrefix +40)*/

//New Addition by Dattatray on 19/09
DECLARE_PMID(kWidgetIDSpace,    kSPOnePnlWidgetID,					kSPPrefix +556)
DECLARE_PMID(kWidgetIDSpace,    kSPMultilineTextWidgetID,			kSPPrefix +557)
DECLARE_PMID(kWidgetIDSpace,    kSPSelectClassWidgetID,				kSPPrefix +558)
DECLARE_PMID(kWidgetIDSpace,    kGreenStarButtonWidgetID,			kSPPrefix +559)
DECLARE_PMID(kWidgetIDSpace,    kWhiteStarButtonWidgetID,			kSPPrefix +560)
DECLARE_PMID(kWidgetIDSpace,    kStarCheckBoxWidgetID,				kSPPrefix +561)
DECLARE_PMID(kWidgetIDSpace,    kNewWhitePictureButtonWidgetID,		kSPPrefix +562)

////////////////////////////// For ThumbNail View //////////////////////////
DECLARE_PMID(kWidgetIDSpace,    kSPThumbPnlWidgetID,				kSPPrefix +563)
DECLARE_PMID(kWidgetIDSpace,    kSPXLibraryItemViewPanelWidgetId,	kSPPrefix +564)
DECLARE_PMID(kWidgetIDSpace,    kSPXLibraryItemScrollBarWidgetId,	kSPPrefix +565)
DECLARE_PMID(kWidgetIDSpace,    kSPXLibraryItemGridWidgetId,		kSPPrefix +566)
////////////////////////////// For ThumbNail View //////////////////////////
////////////
DECLARE_PMID(kWidgetIDSpace,    kBlankIconWidgetID,					kSPPrefix +567)
DECLARE_PMID(kWidgetIDSpace,    kUpdateProductIconWidgetID,			kSPPrefix +568)
DECLARE_PMID(kWidgetIDSpace,    kNewProductIconWidgetID,			kSPPrefix +569)
DECLARE_PMID(kWidgetIDSpace,    kAddToSpreadIconWidgetID,			kSPPrefix +570)
DECLARE_PMID(kWidgetIDSpace,    kUpdateCopyIconWidgetID,			kSPPrefix +571)
DECLARE_PMID(kWidgetIDSpace,    kUpdateArtIconWidgetID,				kSPPrefix +572)
DECLARE_PMID(kWidgetIDSpace,    kUpdateItemTableIconWidgetID,		kSPPrefix +573)
DECLARE_PMID(kWidgetIDSpace,    kDeleteFromSpreadIconWidgetID,		kSPPrefix +574)
DECLARE_PMID(kWidgetIDSpace,    kNewAddIconWidgetID,				kSPPrefix +575)
DECLARE_PMID(kWidgetIDSpace,    kNewUpdateIconWidgetID,				kSPPrefix +576)
DECLARE_PMID(kWidgetIDSpace,    kNewDeleteIconWidgetID,				kSPPrefix +577)
DECLARE_PMID(kWidgetIDSpace,    kCopyArtIconWidgetID,				kSPPrefix +579)
DECLARE_PMID(kWidgetIDSpace,    kCopyItemIconWidgetID,				kSPPrefix +580)
DECLARE_PMID(kWidgetIDSpace,    kArtItemIconWidgetID,				kSPPrefix +581)
DECLARE_PMID(kWidgetIDSpace,    kCopyArtItemIconWidgetID,			kSPPrefix +582)
DECLARE_PMID(kWidgetIDSpace,    kProductIconWidgetID,				kSPPrefix +583)
DECLARE_PMID(kWidgetIDSpace,    kItemIconWidgetID,					kSPPrefix +584)
DECLARE_PMID(kWidgetIDSpace,    kSPText2WidgetID,					kSPPrefix +585)
DECLARE_PMID(kWidgetIDSpace,    kWhiteStarButtonThumbWidgetID,		kSPPrefix +586)
DECLARE_PMID(kWidgetIDSpace,    kNewWhitePictureButtonThumbWidgetID,kSPPrefix +587)
DECLARE_PMID(kWidgetIDSpace,    kTableIconWidgetID,					kSPPrefix +588)
//DECLARE_PMID(kWidgetIDSpace,    kSPViewWhiteBoardWidgetID,		kSPPrefix +589)

////////////
DECLARE_PMID(kWidgetIDSpace,    kSPText3WidgetID,					kSPPrefix +590)
DECLARE_PMID(kWidgetIDSpace,    kSPListBoxWrapperGroupPanelWidgetID,kSPPrefix +591)
DECLARE_PMID(kWidgetIDSpace,    kSPSearchPnlWidgetID,				kSPPrefix +592)
DECLARE_PMID(kWidgetIDSpace,    kSPSearchTextWidgetID,				kSPPrefix +593)
DECLARE_PMID(kWidgetIDSpace,    kSPRevertSearchWidgetID,			kSPPrefix +594)
DECLARE_PMID(kWidgetIDSpace,    kSPPublicationGPWidgetID,			kSPPrefix +595)
DECLARE_PMID(kWidgetIDSpace,    kSPONEGPWidgetID,					kSPPrefix +596)									

//DECLARE_PMID(kWidgetIDSpace,    kHorizontalFlowCheckBoxWidgetID,	kSPPrefix +597)	//--LALIT---
//DECLARE_PMID(kWidgetIDSpace,    kSprayItemPerFrameCheckBoxWidgetID,	kSPPrefix +598)	
//DECLARE_PMID(kWidgetIDSpace,    kSPCheckBoxGroupPanelWidgetID,		kSPPrefix +599) //Commented on 29-0409
DECLARE_PMID(kWidgetIDSpace,    kSPNewCheckBoxGroupPanelWidgetID,	kSPPrefix +600)
DECLARE_PMID(kWidgetIDSpace,    kNewSprayItemPerFrameCheckBoxWidgetID,kSPPrefix +601)
DECLARE_PMID(kWidgetIDSpace,    kNewHorizontalFlowCheckBoxWidgetID,	kSPPrefix +602)
DECLARE_PMID(kWidgetIDSpace,    kSPShowTableSourceWidgetID,			kSPPrefix +603)
DECLARE_PMID(kWidgetIDSpace,    kSPSelectCustomerWidgetID,			kSPPrefix +604)
DECLARE_PMID(kWidgetIDSpace,    kSelectCustomerDialogWidgetID,		kSPPrefix +605)
DECLARE_PMID(kWidgetIDSpace,    kSelectCustomerTextWidgetID,		kSPPrefix +606)
DECLARE_PMID(kWidgetIDSpace,    kCustomerSelectionDropDownWidgetID,	kSPPrefix +607)
DECLARE_PMID(kWidgetIDSpace,    kOKSleCustButtonWidgetID,			kSPPrefix +608)
DECLARE_PMID(kWidgetIDSpace,    kDivisionSelectionDropDownWidgetID,	kSPPrefix +609)
DECLARE_PMID(kWidgetIDSpace,    kSelectDivisionTextWidgetID,		kSPPrefix +610)
DECLARE_PMID(kWidgetIDSpace,    kCancelButtonWidgetID,				kSPPrefix +611)
DECLARE_PMID(kWidgetIDSpace,    kSPsearchWidgetID,					kSPPrefix +612)
DECLARE_PMID(kWidgetIDSpace,    kSPLeadingItemStaticTextWidgetID,	kSPPrefix +613)
DECLARE_PMID(kWidgetIDSpace,    kSPLeadingItemStaticTextThumbWidgetID,	kSPPrefix +614)

DECLARE_PMID(kWidgetIDSpace,    kSPSingleItemStaticTextWidgetID,	kSPPrefix +615)
DECLARE_PMID(kWidgetIDSpace,    kSPSingleItemStaticTextThumbWidgetID,	kSPPrefix +616)
DECLARE_PMID(kWidgetIDSpace,    kSPcountStaticTextWidgetID,	kSPPrefix +617)
DECLARE_PMID(kWidgetIDSpace,    kSPTemplatePreviewWidgetID,	kSPPrefix +618)
DECLARE_PMID(kWidgetIDSpace,    kSPTreeNodeExpanderWidgetID,	kSPPrefix +619)




// "About Plug-ins" sub-menu:
#define kSPAboutMenuKey			kSPStringPrefix "kSPAboutMenuKey"
#define kSPAboutMenuPath		kSDKDefStandardAboutMenuPath kSPCompanyKey

// "Plug-ins" sub-menu:
#define kSPPluginsMenuKey 		kSPStringPrefix "kSPPluginsMenuKey"
#define kSPPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kSPCompanyKey kSDKDefDelimitMenuPath kSPPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kSPAboutBoxStringKey	kSPStringPrefix "kSPAboutBoxStringKey"
#define kSPPanelTitleKey					kSPStringPrefix	"kSPPanelTitleKey"
#define kSPStaticTextKey kSPStringPrefix	"kSPStaticTextKey"
#define kSPInternalPopupMenuNameKey kSPStringPrefix	"kSPInternalPopupMenuNameKey"
#define kSPTargetMenuPath kSPInternalPopupMenuNameKey

#define kSPHybridTableOnlyAboutMenuKey	kSPStringPrefix	"kSPHybridTableOnlyAboutMenuKey"
#define kSPProductOnlyAboutMenuKey	kSPStringPrefix	"kSPProductOnlyAboutMenuKey"
#define kSPItemOnlyAboutMenuKey kSPStringPrefix	"kSPItemOnlyAboutMenuKey"
#define kSPALLMenuKey	kSPStringPrefix		"kSPALLMenuKey"
#define kSPLeadingItemMenuKey kSPStringPrefix "kSPLeadingItemMenuKey"
#define kSPThumbViewMenuKey kSPStringPrefix	"kSPThumbViewMenuKey"
#define kSPListViewMenuKey	kSPStringPrefix "kSPListViewMenuKey"
#define kSPTemplateInfoMenuKey	kSPStringPrefix	 "kSPTemplateInfoMenuKey"
#define kPSPPluginsMenuPath kSPStringPrefix "kPSPPluginsMenuPath"
#define kSPBlankStringKey	kSPStringPrefix	"kSPBlankStringKey"	
#define	kSectionStringKey	kSPStringPrefix		"kSectionStringKey"
#define kSubSectionStringKey	kSPStringPrefix	"kSubSectionStringKey"
#define kSelectCategoy	kSPStringPrefix	"kSelectCategoy"
#define kSearchResultStringkey	kSPStringPrefix	"kSearchResultStringkey"
#define kSPSprayItemPerFrameStringKey		kSPStringPrefix		"kSPSprayItemPerFrameStringKey"
#define kSPHorizontalFlowStringKey	kSPStringPrefix		"kSPHorizontalFlowStringKey"
#define kLibraryItemViewStringKey	kSPStringPrefix	"kLibraryItemViewStringKey"
#define kSPTableStringKey	kSPStringPrefix	"kSPTableStringKey"
#define kItemGroupStringKey	kSPStringPrefix	"kItemGroupStringKey"
#define kItemStringkey	kSPStringPrefix	"kItemStringkey"
#define kLeadingItemStringKey	kSPStringPrefix "kLeadingItemStringKey"
#define kSingleItemStringKey	kSPStringPrefix	"kSingleItemStringKey"
#define kTwentyStringKey	kSPStringPrefix "kTwentyStringKey"
#define kSelectProductFilterCriteriaStringKey	kSPStringPrefix "kSelectProductFilterCriteriaStringKey"
#define kSPSelectStringKey	kSPStringPrefix		"kSPSelectStringKey"
#define kChangeEventStringKey	kSPStringPrefix "kChangeEventStringKey"
#define kSpraySectionStringKey kSPStringPrefix	"kSpraySectionStringKey"
#define kSprayStringKey	kSPStringPrefix	"kSprayStringKey"
#define kListTablesStringKey	kSPStringPrefix "kListTablesStringKey"
#define kRetrievingDataStringKey	kSPStringPrefix	"kRetrievingDataStringKey"
#define kSprayingProductsStringKey	kSPStringPrefix	"kSprayingProductsStringKey"
#define kSprayingSectionStringKey	kSPStringPrefix	"kSprayingSectionStringKey"
#define kSprayingSubsctionStringKey	kSPStringPrefix	"kSprayingSubsctionStringKey"
#define kTemplatepreviewStringKey	kSPStringPrefix	"kTemplatepreviewStringKey"
#define kSPPrintsourcetStringKey    kSPStringPrefix "kSPPrintsourcetStringKey"
// Menu item positions:

#define kSPMenuItem1MenuItemPosition 1
#define	kSPSeparator1MenuItemPosition		10.0
#define kSPAboutThisMenuItemPosition		11.0

#define kSPDialogTitleKey		kSPStringPrefix "kSPDialogTitleKey"
// "Plug-ins" sub-menu item key for dialog:
#define kSPDialogMenuItemKey		kSPStringPrefix "kSPDialogMenuItemKey"
// "Plug-ins" sub-menu item position for dialog:
#define kSPDialogMenuItemPosition	12.0

#define kFolderIcon							20000
//#define kPageIcon							20002
//#define kBookIcon							20003
//#define kRefreshIcon						20004
//#define kSprayIcon							20005
//#define kSubSectionSprayIcon				20011
#define kBlankIcon							20100 //kPRIcon
#define kItemIcon							20101
#define kUnChkIconID						21011	
#define kChkIconID							21012
//#define kFilterIcon							21013
#define kResetFilterIcon					21014
#define kSeparatorIconID					21015

#define kAddToSpreadIcon					21016
#define kUpdateCopyIcon						21017
#define kUpdateArtIcon						21018
#define kUpdateItemTableIcon				21019
#define kDeleteFromSpreadIcon				21020
#define kNewProductIcon						21021
#define kUpdateProductIcon					21022
#define kGreenFilterIcon					21024

#define kNewAddIcon				21025
#define kNewUpdateIcon			21026
#define kNewDeleteIcon			21027
#define kCopyArtIcon			21028
#define kCopyItemIcon			21029
#define kArtItemIcon			21030
#define kCopyArtItemIcon		21031

#define kDesignerMilestoneIcon  21032		
#define kPrvIcon				21035

//New Addition
#define	kLocateIcon				21036
#define kProductIcon			21037

#define GreenStarIcon			21038
#define kRsrcGreenStarIcon		21038	//----CS5------
#define WhiteStarIcon			21039
#define kRsrcWhiteStarIcon		21039	//----CS5------
#define kGreenStariconIcon		21040

#define	kNewRedPictureIcon		21041
#define kRsrcNewRedPictureIcon	21041	//----CS5------
#define kNewWhitePictureIcon	21042
#define kRsrcNewWhitePictureIcon 21042	//----CS5------

#define kTableIcon				21043
#define kviewWhiteboardIcon		21044 
//#define kTableSourceIcon		21045
#define kSelectCustomerIcon		21046

#define kSPListElementRsrcID			1000
#define kSPRFilterOptionElementRsrcID	1001
#define kSelectCustomerDialogResourceID	1100
#define kSPTreePanelNodeRsrcID			1200


// Initial data format version numbers
#define kSPFirstMajorFormatNumber  RezLong(1)
#define kSPFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kSPCurrentMajorFormatNumber kSPFirstMajorFormatNumber
#define kSPCurrentMinorFormatNumber kSPFirstMinorFormatNumber

// PNG specific ID
#define kSPPNGSprayRsrcID						30001
#define kSPPNGSprayRollRsrcID					30001

#define kSPPNGSubSectionSprayRsrcID				30002
#define kSPPNGSubSectionSprayRollRsrcID			30002

#define kSPPNGImageRsrcID						30003
#define kSPPNGImageRollRsrcID					30003

#define kSPPNGTableSourceRsrcID					30004
#define kSPPNGTableSourceRollRsrcID				30004

#define kSPPNGFilterRsrcID					    30005
#define kSPPNGFilterRollRsrcID				    30005

#define kSPPNGRefreshRsrcID					    30006
#define kSPPNGRefreshRollRsrcID				    30006

#define kSPPNGSelectEventRsrcID				    30007
#define kSPPNGSelectEventRollRsrcID			    30007

#define kSPPNGRevertSearchRsrcID                30008
#define kSPPNGRevertSearchRollRsrcID            30008

#define kSPPNGPreviewRsrcID                     30009
#define kSPPNGPreviewRollRsrcID                 30009

#define kGreenFilterRsrcID						30010
#define kGreenFilterRollRsrcID					30010

#define kPNGFilterOKIconRsrcID					30011 
#define kPNGFilterOKIconRollRsrcID				30011 

#define kPNGFilterClearIconRsrcID               30012 
#define kPNGFilterClearIconRollRsrcID           30012 

#define kPNGFilterCancelIconRsrcID              30013 
#define kPNGFilterCancelIconRollRsrcID          30013 

#define kSPPNGsearchRsrcID						30014
#define kSPPNGsearchRollRsrcID					30014

#define kSPPNGTemplatepreviewRsrcID				30015
#define kSPPNGTemplatepreviewRollRsrcID			30015

/////// For ThumbNail View //////
// some grid dimensions
#define kSPGridCellWidth				60//98//	64
#define kSPGridCellHeight				67//92 //91.65//	77
#define kSPGridDelta						(0.0)
#define kSPListViewCellHeight					21
////// ThumbNail View Ends //////


#endif // __SPID_h__
