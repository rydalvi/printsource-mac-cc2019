//========================================================================================
//  
//  $File: $
//  
//  Owner: Catsy
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __CTGID_h__
#define __CTGID_h__

#include "SDKDef.h"

// Company:
#define kCTGCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kCTGCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kCTGPluginName	"APJS10_CatsyTagger"			// Name of this plug-in.
#define kCTGPrefixNumber	0xeAB5100 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kCTGVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kCTGAuthor		"Catsy"					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kCTGPrefixNumber above to modify the prefix.)
#define kCTGPrefix		RezLong(kCTGPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kCTGStringPrefix	SDK_DEF_STRINGIZE(kCTGPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kCTGMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kCTGMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kCTGPluginID, kCTGPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kCTGActionComponentBoss,    kCTGPrefix + 0)
DECLARE_PMID(kClassIDSpace, kCTGPanelWidgetBoss,        kCTGPrefix + 1)
DECLARE_PMID(kClassIDSpace, kCTGTreeViewWidgetBoss,     kCTGPrefix + 3)
DECLARE_PMID(kClassIDSpace, kCTGTreeNodeWidgetBoss,     kCTGPrefix + 4)
DECLARE_PMID(kClassIDSpace, kCTGCatsyTaggerHelperBoss,  kCTGPrefix + 5)
//DECLARE_PMID(kClassIDSpace, kCTGBoss, kCTGPrefix + 6)
//DECLARE_PMID(kClassIDSpace, kCTGBoss, kCTGPrefix + 7)
//DECLARE_PMID(kClassIDSpace, kCTGBoss, kCTGPrefix + 8)
//DECLARE_PMID(kClassIDSpace, kCTGBoss, kCTGPrefix + 9)
//DECLARE_PMID(kClassIDSpace, kCTGBoss, kCTGPrefix + 10)
//DECLARE_PMID(kClassIDSpace, kCTGBoss, kCTGPrefix + 11)
//DECLARE_PMID(kClassIDSpace, kCTGBoss, kCTGPrefix + 12)
//DECLARE_PMID(kClassIDSpace, kCTGBoss, kCTGPrefix + 13)
//DECLARE_PMID(kClassIDSpace, kCTGBoss, kCTGPrefix + 14)
//DECLARE_PMID(kClassIDSpace, kCTGBoss, kCTGPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kCTGBoss, kCTGPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kCTGBoss, kCTGPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kCTGBoss, kCTGPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kCTGBoss, kCTGPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kCTGBoss, kCTGPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kCTGBoss, kCTGPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kCTGBoss, kCTGPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kCTGBoss, kCTGPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kCTGBoss, kCTGPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kCTGBoss, kCTGPrefix + 25)


// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_ICTGSHADOWEVENTHANDLER,     kCTGPrefix + 0)
DECLARE_PMID(kInterfaceIDSpace, IID_ICATSYTAGGERIFACE,          kCTGPrefix + 1)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICTGINTERFACE, kCTGPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kCTGActionComponentImpl,           kCTGPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kCTGSelectionObserverImpl,         kCTGPrefix + 1)
DECLARE_PMID(kImplementationIDSpace, kCTGTreeViewHierarchyAdapterImpl,  kCTGPrefix + 2)
DECLARE_PMID(kImplementationIDSpace, kCTGTreeViewWidgetMgrImpl,         kCTGPrefix + 3)
DECLARE_PMID(kImplementationIDSpace, kCTGTreeNodeEHImpl,                kCTGPrefix + 4)
DECLARE_PMID(kImplementationIDSpace, kCTGCatsyTaggerHelperImpl,         kCTGPrefix + 5)
//DECLARE_PMID(kImplementationIDSpace, kCTGImpl, kCTGPrefix + 6)
//DECLARE_PMID(kImplementationIDSpace, kCTGImpl, kCTGPrefix + 7)
//DECLARE_PMID(kImplementationIDSpace, kCTGImpl, kCTGPrefix + 8)
//DECLARE_PMID(kImplementationIDSpace, kCTGImpl, kCTGPrefix + 9)
//DECLARE_PMID(kImplementationIDSpace, kCTGImpl, kCTGPrefix + 10)
//DECLARE_PMID(kImplementationIDSpace, kCTGImpl, kCTGPrefix + 11)
//DECLARE_PMID(kImplementationIDSpace, kCTGImpl, kCTGPrefix + 12)
//DECLARE_PMID(kImplementationIDSpace, kCTGImpl, kCTGPrefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kCTGImpl, kCTGPrefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kCTGImpl, kCTGPrefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kCTGImpl, kCTGPrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kCTGImpl, kCTGPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kCTGImpl, kCTGPrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kCTGImpl, kCTGPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kCTGImpl, kCTGPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kCTGImpl, kCTGPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kCTGImpl, kCTGPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kCTGImpl, kCTGPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kCTGImpl, kCTGPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kCTGImpl, kCTGPrefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kCTGAboutActionID,             kCTGPrefix + 0)
DECLARE_PMID(kActionIDSpace, kCTGPanelWidgetActionID,       kCTGPrefix + 1)
DECLARE_PMID(kActionIDSpace, kCTGSeparator1ActionID,        kCTGPrefix + 2)
DECLARE_PMID(kActionIDSpace, kCTGPopupAboutThisActionID,    kCTGPrefix + 3)
//DECLARE_PMID(kActionIDSpace, kCTGActionID, kCTGPrefix + 5)
//DECLARE_PMID(kActionIDSpace, kCTGActionID, kCTGPrefix + 6)
//DECLARE_PMID(kActionIDSpace, kCTGActionID, kCTGPrefix + 7)
//DECLARE_PMID(kActionIDSpace, kCTGActionID, kCTGPrefix + 8)
//DECLARE_PMID(kActionIDSpace, kCTGActionID, kCTGPrefix + 9)
//DECLARE_PMID(kActionIDSpace, kCTGActionID, kCTGPrefix + 10)
//DECLARE_PMID(kActionIDSpace, kCTGActionID, kCTGPrefix + 11)
//DECLARE_PMID(kActionIDSpace, kCTGActionID, kCTGPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kCTGActionID, kCTGPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kCTGActionID, kCTGPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kCTGActionID, kCTGPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kCTGActionID, kCTGPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kCTGActionID, kCTGPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kCTGActionID, kCTGPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kCTGActionID, kCTGPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kCTGActionID, kCTGPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kCTGActionID, kCTGPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kCTGActionID, kCTGPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kCTGActionID, kCTGPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kCTGActionID, kCTGPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kCTGActionID, kCTGPrefix + 25)


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kCTGPanelWidgetID,                 kCTGPrefix + 0)
DECLARE_PMID(kWidgetIDSpace, kCTGSectionDropDownWidgetID,       kCTGPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kCTGItemFieldsDropDownWidgetID,    kCTGPrefix + 3)
DECLARE_PMID(kWidgetIDSpace, kCTGTreeWrapperGroupPanelWidgetID, kCTGPrefix + 4)
DECLARE_PMID(kWidgetIDSpace, kCTGTreeListBoxWidgetID,           kCTGPrefix + 5)
DECLARE_PMID(kWidgetIDSpace, kCTGTreeNodeExpanderWidgetID,      kCTGPrefix + 6)
DECLARE_PMID(kWidgetIDSpace, kCTGTreeParentWidgetId,            kCTGPrefix + 7)
DECLARE_PMID(kWidgetIDSpace, kCTGItemGroupStaticTextWidgetID,   kCTGPrefix + 8)
DECLARE_PMID(kWidgetIDSpace, kCTGItemStaticTextWidgetID,        kCTGPrefix + 9)
DECLARE_PMID(kWidgetIDSpace, kCTGChildItemStaticTextWidgetID,   kCTGPrefix + 10)
DECLARE_PMID(kWidgetIDSpace, kCTGTextWidgetID,                  kCTGPrefix + 11)
DECLARE_PMID(kWidgetIDSpace, kCTGAttributeValueTextWidgetID,    kCTGPrefix + 12)
DECLARE_PMID(kWidgetIDSpace, kCTGcountStaticTextWidgetID,       kCTGPrefix + 13)
DECLARE_PMID(kWidgetIDSpace, kCTGSeparatorWidgetID,             kCTGPrefix + 14)
DECLARE_PMID(kWidgetIDSpace, kCTGTreeCopyIconSuiteWidgetID,     kCTGPrefix + 15)
DECLARE_PMID(kWidgetIDSpace, kCTGTreeImageIconSuiteWidgetID,    kCTGPrefix + 16)
DECLARE_PMID(kWidgetIDSpace, kCTGListStaticTextWidgetID,        kCTGPrefix + 17)
DECLARE_PMID(kWidgetIDSpace, kCTGAttributeStaticTextWidgetID,   kCTGPrefix + 18)
DECLARE_PMID(kWidgetIDSpace, kCTGTreeListIconSuiteWidgetID,     kCTGPrefix + 19)
//DECLARE_PMID(kWidgetIDSpace, kCTGWidgetID, kCTGPrefix + 20)
//DECLARE_PMID(kWidgetIDSpace, kCTGWidgetID, kCTGPrefix + 21)
//DECLARE_PMID(kWidgetIDSpace, kCTGWidgetID, kCTGPrefix + 22)
//DECLARE_PMID(kWidgetIDSpace, kCTGWidgetID, kCTGPrefix + 23)
//DECLARE_PMID(kWidgetIDSpace, kCTGWidgetID, kCTGPrefix + 24)
//DECLARE_PMID(kWidgetIDSpace, kCTGWidgetID, kCTGPrefix + 25)


// "About Plug-ins" sub-menu:
#define kCTGAboutMenuKey			kCTGStringPrefix "kCTGAboutMenuKey"
#define kCTGAboutMenuPath		kSDKDefStandardAboutMenuPath kCTGCompanyKey

// "Plug-ins" sub-menu:
#define kCTGPluginsMenuKey 		kCTGStringPrefix "kCTGPluginsMenuKey"
#define kCTGPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kCTGCompanyKey kSDKDefDelimitMenuPath kCTGPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kCTGAboutBoxStringKey	kCTGStringPrefix "kCTGAboutBoxStringKey"
#define kCTGPanelTitleKey					kCTGStringPrefix	"kCTGPanelTitleKey"
#define kCTGStaticTextKey kCTGStringPrefix	"kCTGStaticTextKey"
#define kCTGInternalPopupMenuNameKey kCTGStringPrefix	"kCTGInternalPopupMenuNameKey"
#define kCTGTargetMenuPath kCTGInternalPopupMenuNameKey
#define	kCTGSectionStringKey	kCTGStringPrefix		"kCTGSectionStringKey"
#define	kCTGItemFieldsStringKey	kCTGStringPrefix		"kCTGItemFieldsStringKey"
#define	kCTGBlankStringKey      kCTGStringPrefix		"kCTGBlankStringKey"
#define	kFamilyStringKey        kCTGStringPrefix		"kFamilyStringKey"
#define	kCTGItemStringkey       kCTGStringPrefix		"kItemStringkey"
#define	kChildItemStringKey     kCTGStringPrefix		"kChildItemStringKey"
#define	kCTGSeparatorStringKey  kCTGStringPrefix		"kCTGSeparatorStringKey"
#define kListStringKey          kCTGStringPrefix        "ListStringKey"
#define kAtrributeStringKey     kCTGStringPrefix        "kAtrributeStringKey"



// Menu item positions:

#define	kCTGSeparator1MenuItemPosition		10.0
#define kCTGAboutThisMenuItemPosition		11.0


#define kCTGTreePanelNodeRsrcID			1000

#define kCTGCopyIcon					10002
#define kCTGImageIcon					10003
#define kCTGListIcon                    10004

// Initial data format version numbers
#define kCTGFirstMajorFormatNumber  RezLong(1)
#define kCTGFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kCTGCurrentMajorFormatNumber kCTGFirstMajorFormatNumber
#define kCTGCurrentMinorFormatNumber kCTGFirstMinorFormatNumber

#endif // __CTGID_h__
