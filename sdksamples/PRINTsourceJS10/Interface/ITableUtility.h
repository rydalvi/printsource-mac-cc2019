#ifndef _ITABLEUTILITY_H__
#define _ITABLEUTILITY_H__

class PublicationNode;
#include "vector"
#include "ITableModel.h"
#include "ITableTextSelection.h"
#include "XMLReference.h"
#include "SlugStructure.h"
#include "ITool.h"
#include "TagStruct.h"
#include "DSFID.h"
//#include "AdvanceTableScreenValue.h" 
#include "ITableCommands.h"



#define DRAGMODE		1
#define CONTENTSMODE	2
using namespace std;

class ITableUtility: public IPMUnknown
{
public:

	enum { kDefaultIID = IID_ITABLEUTILITY };

	virtual bool16 isTablePresent(const UIDRef&, UIDRef&)=0;
	virtual void fillDataInTable(const UIDRef&, PublicationNode&, TagStruct& , double&,const UIDRef&)=0;
	//virtual void fillDataInTable(const UIDRef , int32 objectId, int32 ,int32& )=0;
	virtual void resizeTable(const UIDRef&, const int32&, const int32&, bool16,const int32&,const int32&)=0;
	virtual void setTableRowColData(const UIDRef&, const PMString&, const int32&, const int32&)=0;
	virtual void putHeaderDataInTable(const UIDRef&, vector<double>, bool16, TagStruct& , UIDRef boxUIDRef)=0;
	virtual void SetSlug(const UIDRef&, vector<double>, vector<double>,bool8)=0;
	virtual void Slugreader(const UIDRef&, const int32&, const int32&,double&,double&)=0;
	virtual PMString TabbedTableData(const UIDRef&, PublicationNode&, double, double&, TagStruct&)=0;
	virtual void AddTexttoTabbedTable(const UIDRef&,PMString,vector<double>, vector<double>,bool8)=0;
	virtual void AddSlugtoText(double,double)=0;
	virtual void IterateThroughTable(InterfacePtr<ITableModel>&)=0;
	virtual void NewSlugreader(InterfacePtr<ITableModel> &tblModel, const int32& rowIndex, const int32& colIndex,double& Attr_id,double& Item_id,InterfacePtr<ITableTextSelection> &tblTxtSel)=0;
	virtual void TextSlugReader(const UIDRef& tableUIDRef)=0;
	virtual bool8 IsAttrInTextSlug(const UIDRef& tableUIDRef,double Attr_id)=0;
	virtual bool8 UpdateDataInTextSlug(const UIDRef& tableUIDRef,double Attr_id,PMString Table_col)=0;
	virtual ErrorCode ProcessSimpleCommand(const ClassID& commandClass, const UIDList& itemsIn, UIDList& itemsOut)=0;

	virtual void attachAttributes(XMLReference *newTag, bool16 IsPRINTsourceRootTag, SlugStruct& stencilInfoData, int rowno, int colno ,int ProductStandardTableCase = 0)=0;
	virtual ErrorCode TagTableCellText(InterfacePtr<ITableModel> &tableModel,XMLReference &parentXMLRef, GridID id,GridArea gridArea, XMLReference &CellTextxmlRef, SlugStruct& stencilInfoData)=0;
	virtual ErrorCode AddTableAndCellElements(const UIDRef& tableModelUIDRef, const UIDRef& BoxUIDRef,
								const PMString& tableTagName,
								const PMString& cellTagName,
								XMLReference& outCreatedXMLReference,
								double TableID, 
								SlugStruct& stencilInfoData								
								)=0;
	virtual ErrorCode TagStory(const PMString& tagName,const UIDRef& textModelUIDRef, const UIDRef& boxRef)=0;
	virtual ErrorCode TagTable(const UIDRef& tableModelUIDRef,UIDRef BoxRef,
								const PMString& tableTagName,
								const PMString& cellTagName,
								XMLReference& outCreatedXMLReference,
								SlugStruct& stencilInfoData,
								int32 rowno, int32 colno, double TableID)=0;
	virtual UIDRef AcquireTag(const UIDRef& boxUIDRef, const PMString& tagName, bool16 &ISTagPresent)=0;
	virtual int16 convertBoxToTextBox(UIDRef boxUIDRef)=0;
	virtual int changeMode(int whichMode)=0;
	//virtual ITool* queryTool(const ClassID& toolClass)=0;
	virtual void SetDBTableStatus(bool16 Status)=0;
	virtual ErrorCode SprayUserTableAsPerCellTagsForMultipleItem(const UIDRef& boxUIDRef, TagStruct& slugInfo, PublicationNode& pNode)=0;
	virtual ErrorCode CopyTables(const UIDRef& sourceStory, int32 StartIndexForNewTable, int32 repeteCopy)=0;
	virtual ErrorCode ProcessCopyStoryRangeCmd(
			const UIDRef& sourceStory, 
			TextIndex sourceStart, 
			int32 sourceLength, 
			const UIDRef& destStory,
			TextIndex destStart, 
			int32 destLength
		)=0;
	virtual void fillDataInHybridTable(const UIDRef&, PublicationNode&, TagStruct& ,double& ,const UIDRef& )=0;
	//virtual void putHeaderDataInHybridTable(const UIDRef&, vector<vector<PMString> >, bool16, TagStruct& , UIDRef boxUIDRef , vector<vector<CAdvanceTableCellValue> > &)=0;
	virtual bool16 FillDataInsideItemTableOfItem
			(
				XMLReference & boxXMLRef,
				UIDRef & tableModelUIDRef
			)=0;
	virtual void resizeTableForHybridTable(const UIDRef&, const int32&, const int32&, bool16,const int32&,const int32&,const int32&)=0;
	
};

#endif