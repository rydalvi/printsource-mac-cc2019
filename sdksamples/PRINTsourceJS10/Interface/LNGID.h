//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __LNGID_h__
#define __LNGID_h__

#include "SDKDef.h"

// Company:
#define kLNGCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kLNGCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kLNGPluginName	"APJS9_Login"			// Name of this plug-in.
#define kLNGPrefixNumber	0XDBe65BA 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kLNGVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kLNGAuthor		"Apsiva Inc."					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kLNGPrefixNumber above to modify the prefix.)
#define kLNGPrefix		RezLong(kLNGPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kLNGStringPrefix	SDK_DEF_STRINGIZE(kLNGPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

#define kLaunchpadDialogResourceID  800
#define kLoginDialogRsrcID			700
#define kPropDialogRsrcID			2000
#define kProp2DialogRsrcID			1900
#define kLNGListElementRsrcID		1000

// icons
#define kLNGClearIconID		10249
#define kLNGSaveIconID		10248
#define kLNGserverIconID	10247
#define kLNGnmsIconID		10246

// Missing plug-in: (see ExtraPluginInfo resource)
#define kLNGMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kLNGMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kLNGPluginID, kLNGPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kLNGActionComponentBoss, kLNGPrefix + 0)
DECLARE_PMID(kClassIDSpace, kLNGMenuRegisterBoss,			kLNGPrefix + 2)
DECLARE_PMID(kClassIDSpace, kLNGActionRegisterBoss,			kLNGPrefix + 3)
DECLARE_PMID(kClassIDSpace, kLNGLaunchpadDialogBoss,		kLNGPrefix + 4)
DECLARE_PMID(kClassIDSpace, kLNGListBoxWidgetBoss,			kLNGPrefix + 5)
DECLARE_PMID(kClassIDSpace, kLNGLoginDialogBoss,			kLNGPrefix + 6)
DECLARE_PMID(kClassIDSpace, kLNGPropDialogBoss,				kLNGPrefix + 7)
DECLARE_PMID(kClassIDSpace, kLNGTextWidgetBoss,				kLNGPrefix + 8)
DECLARE_PMID(kClassIDSpace, kLNGLoginEventsHandler,			kLNGPrefix + 9)
DECLARE_PMID(kClassIDSpace, kLNGListBox2WidgetBoss,			kLNGPrefix + 10)
DECLARE_PMID(kClassIDSpace, kLNGProp2DialogBoss,			kLNGPrefix + 11)
DECLARE_PMID(kClassIDSpace, kLNGLoginHelperBoss,			kLNGPrefix + 1277)
//6-march
DECLARE_PMID(kClassIDSpace, kLNGLstboxMultilineTxtWidgetBoss,			kLNGPrefix + 13)
//6-march
DECLARE_PMID(kClassIDSpace, kLNGTreeViewWidgetBoss,			kLNGPrefix + 14)
DECLARE_PMID(kClassIDSpace, kLNGTreeNodeWidgetBoss,			kLNGPrefix + 15)
DECLARE_PMID(kClassIDSpace, kLNGTreeTextBoxWidgetBoss,			kLNGPrefix + 16)

DECLARE_PMID(kClassIDSpace, kWFPDialogBoss,			kLNGPrefix + 17) //*** Versioning




// InterfaceIDs:  
DECLARE_PMID(kInterfaceIDSpace, IID_ILOGINEVENT ,			kLNGPrefix + 0)
DECLARE_PMID(kInterfaceIDSpace, IID_IREGISTERLOGINEVENT ,	kLNGPrefix + 1)
DECLARE_PMID(kInterfaceIDSpace, IID_ILOGINHELPER ,	        kLNGPrefix + 277)
DECLARE_PMID(kInterfaceIDSpace, IID_ILNGTRVSHADOWEVENTHANDLER ,	        kLNGPrefix + 278)

//

// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kLNGLaunchpadDlgActionCompImpl,kLNGPrefix + 0)
DECLARE_PMID(kImplementationIDSpace, kLNGLaunchpadDlgControllerImpl,kLNGPrefix + 1)
DECLARE_PMID(kImplementationIDSpace, kLNGLaunchpadDlgObserverImpl,	kLNGPrefix + 2)
DECLARE_PMID(kImplementationIDSpace, kLNGLaunchpadListBoxObserImpl,	kLNGPrefix + 3)
DECLARE_PMID(kImplementationIDSpace, kLNGLoginDlgControllerImpl,	kLNGPrefix + 4)
DECLARE_PMID(kImplementationIDSpace, kLNGLoginDlgObserverImpl,		kLNGPrefix + 5)
DECLARE_PMID(kImplementationIDSpace, kLNGPropDlgControllerImpl,		kLNGPrefix + 6)
DECLARE_PMID(kImplementationIDSpace, kLNGPropDlgObserverImpl,		kLNGPrefix + 7)
DECLARE_PMID(kImplementationIDSpace, DoubleClickEventHandlerImpl,	kLNGPrefix + 8)
DECLARE_PMID(kImplementationIDSpace, kCLoginEventImpl,				kLNGPrefix + 9)
DECLARE_PMID(kImplementationIDSpace, kLNGLoginEventsHandlerImpl,	kLNGPrefix + 10)
DECLARE_PMID(kImplementationIDSpace, kLNGProp2DlgControllerImpl,		kLNGPrefix + 11)
DECLARE_PMID(kImplementationIDSpace, kLNGProp2DlgObserverImpl,		kLNGPrefix + 12)
DECLARE_PMID(kImplementationIDSpace, kLNGLoginHelperImpl ,		    kLNGPrefix + 13)
DECLARE_PMID(kImplementationIDSpace, kLNGDialogObserverImpl ,		    kLNGPrefix + 14)
DECLARE_PMID(kImplementationIDSpace, kLNGDialogControllerImpl,		    kLNGPrefix + 15)
DECLARE_PMID(kImplementationIDSpace, kLNGTreeViewHierarchyAdapterImpl,		    kLNGPrefix + 16)
DECLARE_PMID(kImplementationIDSpace, kLNGTreeViewWidgetMgrImpl,		    kLNGPrefix + 17)
DECLARE_PMID(kImplementationIDSpace, kLNGTrvNodeEHImpl,		    kLNGPrefix + 18)

DECLARE_PMID(kImplementationIDSpace, kLNGVersionUpdaterDialogControllerImpl,		    kLNGPrefix + 19)//**** Versioning
DECLARE_PMID(kImplementationIDSpace, kLNGVersionUpdaterDialogObserverImpl,		    kLNGPrefix + 20)//**** Versioning


// ActionIDs:   
DECLARE_PMID(kActionIDSpace, kLNGAboutActionID, kLNGPrefix + 0)

DECLARE_PMID(kActionIDSpace, kLNGDialogActionID, kLNGPrefix + 4)
DECLARE_PMID(kActionIDSpace, kLNGResetActionID, kLNGPrefix + 5)
//DECLARE_PMID(kActionIDSpace, kLNGActionID, kLNGPrefix + 6)
//DECLARE_PMID(kActionIDSpace, kLNGActionID, kLNGPrefix + 7)
//DECLARE_PMID(kActionIDSpace, kLNGActionID, kLNGPrefix + 8)
//DECLARE_PMID(kActionIDSpace, kLNGActionID, kLNGPrefix + 9)
//DECLARE_PMID(kActionIDSpace, kLNGActionID, kLNGPrefix + 10)
//DECLARE_PMID(kActionIDSpace, kLNGActionID, kLNGPrefix + 11)
//DECLARE_PMID(kActionIDSpace, kLNGActionID, kLNGPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kLNGActionID, kLNGPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kLNGActionID, kLNGPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kLNGActionID, kLNGPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kLNGActionID, kLNGPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kLNGActionID, kLNGPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kLNGActionID, kLNGPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kLNGActionID, kLNGPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kLNGActionID, kLNGPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kLNGActionID, kLNGPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kLNGActionID, kLNGPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kLNGActionID, kLNGPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kLNGActionID, kLNGPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kLNGActionID, kLNGPrefix + 25)


// WidgetIDs:
// Launchpad Dialog WidgetIDs
DECLARE_PMID(kWidgetIDSpace, kLaunchpadDialogWidgetID,		kLNGPrefix + 0)
DECLARE_PMID(kWidgetIDSpace, kLaunchpadIconSuiteWidgetID,	kLNGPrefix + 1)
DECLARE_PMID(kWidgetIDSpace, kLaunchpadPropButtonWidgetID,	kLNGPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kLaunchpadNewButtonWidgetID,	kLNGPrefix + 3)
DECLARE_PMID(kWidgetIDSpace, kLaunchpadDeleteButtonWidgetID,kLNGPrefix + 4)

DECLARE_PMID(kWidgetIDSpace, kLaunchpadListBoxWidgetID,		  kLNGPrefix + 5)
DECLARE_PMID(kWidgetIDSpace, kLaunchpadEnvStaticTextWidgetID, kLNGPrefix + 6)
DECLARE_PMID(kWidgetIDSpace, kLaunchpadSerStaticTextWidgetID, kLNGPrefix + 7)
DECLARE_PMID(kWidgetIDSpace, kLaunchpadListBoxParentWidgetId, kLNGPrefix + 8)
DECLARE_PMID(kWidgetIDSpace, kLaunchpadListBoxEnvTextWidgetID,kLNGPrefix + 9)
DECLARE_PMID(kWidgetIDSpace, kLaunchpadListBoxSrvTextWidgetID,kLNGPrefix + 10)

DECLARE_PMID(kWidgetIDSpace, kLaunchpadGroupPanel1WidgetID,kLNGPrefix + 11)
DECLARE_PMID(kWidgetIDSpace, kLaunchpadSeparator1WidgetID, kLNGPrefix + 12)
DECLARE_PMID(kWidgetIDSpace, kLaunchpadGroupPanel2WidgetID,kLNGPrefix + 13)
DECLARE_PMID(kWidgetIDSpace, kLaunchpadStatusTextWidgetID, kLNGPrefix + 14)
DECLARE_PMID(kWidgetIDSpace, kLaunchpadGroupPanel3WidgetID,kLNGPrefix + 15)
DECLARE_PMID(kWidgetIDSpace, kLaunchpadGroupPanel4WidgetID,kLNGPrefix + 16)

// Login Dialog WidgetIDs
DECLARE_PMID(kWidgetIDSpace, kLoginDialogWidgetID,			   kLNGPrefix + 18)
DECLARE_PMID(kWidgetIDSpace, kLoginUserNameStaticTextWidgetID, kLNGPrefix + 19)
DECLARE_PMID(kWidgetIDSpace, kLoginPasswordStaticTextWidgetID, kLNGPrefix + 20)
DECLARE_PMID(kWidgetIDSpace, kLoginUserNameTextEditWidgetID,   kLNGPrefix + 21)
DECLARE_PMID(kWidgetIDSpace, kLoginPasswordTextEditWidgetID,   kLNGPrefix + 22)
DECLARE_PMID(kWidgetIDSpace, kLoginGroupPanelWidgetID,		   kLNGPrefix + 23)
DECLARE_PMID(kWidgetIDSpace, kLNGMultilineTextExpander1WidgetID,		kLNGPrefix + 24)
DECLARE_PMID(kWidgetIDSpace, kCancelButton1WidgetID,	kLNGPrefix + 25)
DECLARE_PMID(kWidgetIDSpace, kLoginGroupPanel1WidgetID,	kLNGPrefix + 26)
DECLARE_PMID(kWidgetIDSpace, kLoginGroupPanel2WidgetID,	kLNGPrefix + 27)
DECLARE_PMID(kWidgetIDSpace, kOptionsButtonWidgetID,	kLNGPrefix + 28)
DECLARE_PMID(kWidgetIDSpace, kHostDropDownWidgetID,	kLNGPrefix + 29)


// Properties Dialog WidgetIDs
DECLARE_PMID(kWidgetIDSpace, kPropertiesDialogWidgetID,		kLNGPrefix + 30)
DECLARE_PMID(kWidgetIDSpace, kPropEnvNameStaticTextWidgetID,kLNGPrefix + 31)
DECLARE_PMID(kWidgetIDSpace, kPropDbVendStaticTextWidgetID, kLNGPrefix + 32)
DECLARE_PMID(kWidgetIDSpace, kPropDbVerStaticTextWidgetID,	kLNGPrefix + 33)
DECLARE_PMID(kWidgetIDSpace, kPropDbSrvStaticTextWidgetID,  kLNGPrefix + 34)
DECLARE_PMID(kWidgetIDSpace, kPropDbPortStaticTextWidgetID, kLNGPrefix + 35)
DECLARE_PMID(kWidgetIDSpace, kPropDbSchemStaticTextWidgetID,kLNGPrefix + 36)
DECLARE_PMID(kWidgetIDSpace, kPropDbUnameStaticTextWidgetID,kLNGPrefix + 37)
DECLARE_PMID(kWidgetIDSpace, kPropDbPwordStaticTextWidgetID,kLNGPrefix + 38)
DECLARE_PMID(kWidgetIDSpace, kPropEnvTextEditWidgetID,	 kLNGPrefix + 39)
DECLARE_PMID(kWidgetIDSpace, kPropSrvTextEditWidgetID,	 kLNGPrefix + 40)
DECLARE_PMID(kWidgetIDSpace, kPropPrtTextEditWidgetID,	 kLNGPrefix + 41)
DECLARE_PMID(kWidgetIDSpace, kPropSchemaTextEditWidgetID,kLNGPrefix + 42)
DECLARE_PMID(kWidgetIDSpace, kPropUnameTextEditWidgetID, kLNGPrefix + 43)
DECLARE_PMID(kWidgetIDSpace, kPropPwordTextEditWidgetID, kLNGPrefix + 44)

DECLARE_PMID(kWidgetIDSpace, kPropVendorDropDownWidgetID, kLNGPrefix + 45)
DECLARE_PMID(kWidgetIDSpace, kPropVersionDropDownWidgetID,kLNGPrefix + 46)
DECLARE_PMID(kWidgetIDSpace, kPropGroupPanel1WidgetID,    kLNGPrefix + 47)
DECLARE_PMID(kWidgetIDSpace, kPropGroupPanel2WidgetID,	  kLNGPrefix + 48)
DECLARE_PMID(kWidgetIDSpace, kProp2GroupPanel1WidgetID,	  kLNGPrefix + 49)
DECLARE_PMID(kWidgetIDSpace, kProp2EnvNameStaticTextWidgetID,	  kLNGPrefix + 50)
DECLARE_PMID(kWidgetIDSpace, kProp2SrvTextEditWidgetID, kLNGPrefix + 51)
DECLARE_PMID(kWidgetIDSpace, kProp2EnvNameTextEditWidgetID , kLNGPrefix + 52)
DECLARE_PMID(kWidgetIDSpace, kProp2ServerUrlStaticTextWidgetID, kLNGPrefix + 53)
DECLARE_PMID(kWidgetIDSpace, kProperties2DialogWidgetID,		kLNGPrefix + 54)
DECLARE_PMID(kWidgetIDSpace, kProp2EnvTextEditWidgetID,	 kLNGPrefix + 55)

DECLARE_PMID(kWidgetIDSpace, kSelectModeWidgetID,	 kLNGPrefix + 56) //Cs4
DECLARE_PMID(kWidgetIDSpace, kPropConnNameStaticTextWidgetID,	 kLNGPrefix + 57)
DECLARE_PMID(kWidgetIDSpace, kPropConnModeDropDownWidgetID,	 kLNGPrefix + 58)
DECLARE_PMID(kWidgetIDSpace, kProp2ConnNameStaticTextWidgetID,	 kLNGPrefix + 59)
DECLARE_PMID(kWidgetIDSpace, kProp2ConnModeDropDownWidgetID,	 kLNGPrefix + 60)
DECLARE_PMID(kWidgetIDSpace, kPropServerUrlStaticTextWidgetID,	 kLNGPrefix + 61)
DECLARE_PMID(kWidgetIDSpace, kPropSrvURLTextEditWidgetID,		 kLNGPrefix + 62)
// new added vaibhav
DECLARE_PMID(kWidgetIDSpace, kPropImagePathStaticTextWidgetID,		 kLNGPrefix + 63)
DECLARE_PMID(kWidgetIDSpace, kPropDocPathStaticTextWidgetID,		 kLNGPrefix + 64)
DECLARE_PMID(kWidgetIDSpace, kPropImagePathTextEditWidgetID,		 kLNGPrefix + 65)
DECLARE_PMID(kWidgetIDSpace, kImgPathButtonWidgetID1,				 kLNGPrefix + 66)
DECLARE_PMID(kWidgetIDSpace, kPropDocPathTextEditWidgetID,			 kLNGPrefix + 67)
DECLARE_PMID(kWidgetIDSpace, kDocumentPathButtonWidgetID,			 kLNGPrefix + 68)
DECLARE_PMID(kWidgetIDSpace, kPropSameAsAssetWidgetID ,				 kLNGPrefix + 69)
DECLARE_PMID(kWidgetIDSpace, kPropLogLevelStaticTextWidgetID ,		 kLNGPrefix + 70)
DECLARE_PMID(kWidgetIDSpace, kPropLogLevelDropDownWidgetID ,		 kLNGPrefix + 71)
//
//6-march
DECLARE_PMID(kWidgetIDSpace, kSaveButtonWidgetID ,					 kLNGPrefix + 72)
DECLARE_PMID(kWidgetIDSpace, kScrollBarWidgetID ,					 kLNGPrefix + 73)
DECLARE_PMID(kWidgetIDSpace, kLNGMultilineTextExpanderWidgetID ,	 kLNGPrefix + 74)
DECLARE_PMID(kWidgetIDSpace, kLNGSaveRollOverWidgetID ,				 kLNGPrefix + 75)
DECLARE_PMID(kWidgetIDSpace, kLNGClearRollOverWidgetID ,			 kLNGPrefix + 76)
DECLARE_PMID(kWidgetIDSpace, kLogGroupPanelWidgetID ,				 kLNGPrefix + 77)
//added by nitin
DECLARE_PMID(kWidgetIDSpace, kPropGroupPanel3WidgetID,				 kLNGPrefix + 78)
DECLARE_PMID(kWidgetIDSpace, kPropClusterPanelWidgetID,			 kLNGPrefix + 79)
DECLARE_PMID(kWidgetIDSpace, kPropMaptoRepositoryWidgetID,					 kLNGPrefix + 80)// kPropMaptoRepositoryWidgetID,
DECLARE_PMID(kWidgetIDSpace, kPropDownloadImagestoWidgetID,					 kLNGPrefix + 81)// kPropDownloadImagestoWidgetID,
DECLARE_PMID(kWidgetIDSpace, kImgPathButtonWidgetID2,				 kLNGPrefix + 82)
DECLARE_PMID(kWidgetIDSpace, kPropDownloadImagePathStaticTextWidgetID,	 kLNGPrefix + 83)
DECLARE_PMID(kWidgetIDSpace, kPropDownloadImagePathTextEditWidgetID,	 kLNGPrefix + 84)
DECLARE_PMID(kWidgetIDSpace, kPropUseMissingFlagWidgetID ,				 kLNGPrefix + 85)


DECLARE_PMID(kWidgetIDSpace, kContinueButtonWidgetID ,				 kLNGPrefix + 86)
DECLARE_PMID(kWidgetIDSpace, kLNGTreeViewWidgetID ,				 kLNGPrefix + 87)
DECLARE_PMID(kWidgetIDSpace, kLNGTreePanelNodeWidgetID ,				 kLNGPrefix + 88)
DECLARE_PMID(kWidgetIDSpace, kLNGTreeNodeNameWidgetID ,				 kLNGPrefix + 101)
DECLARE_PMID(kWidgetIDSpace, kLoginSelectaHostStaticTextWidgetID ,				 kLNGPrefix + 89)

DECLARE_PMID(kWidgetIDSpace, kFirstLoginButtonWidgetID ,				 kLNGPrefix + 90)
DECLARE_PMID(kWidgetIDSpace, kFirstCancelButton_WidgetID ,				 kLNGPrefix + 91)
DECLARE_PMID(kWidgetIDSpace, kLoginButtonWidgetID ,						 kLNGPrefix + 92)
DECLARE_PMID(kWidgetIDSpace, kSecondCancelButtonWidgetID ,				 kLNGPrefix + 93)
DECLARE_PMID(kWidgetIDSpace, kThirdCancelButtonWidgetID ,				 kLNGPrefix + 94)
DECLARE_PMID(kWidgetIDSpace, kNewOKButtonWidgetID ,						 kLNGPrefix + 95)
DECLARE_PMID(kWidgetIDSpace, kLNGMultilineTextExpander2WidgetID,		 kLNGPrefix + 96)

DECLARE_PMID(kWidgetIDSpace, kLNGVersionUpdaterDialogWidgetID,		 kLNGPrefix + 97) //Versioning
DECLARE_PMID(kWidgetIDSpace, kersionUpdaterStaticTextWidgetID,		 kLNGPrefix + 98) //Versioning
DECLARE_PMID(kWidgetIDSpace, kversionUpdaterStaticTextWidgetID,		 kLNGPrefix + 99) //Versioning
DECLARE_PMID(kWidgetIDSpace, kVersionUpdaterURLTextEditWidgetID,		 kLNGPrefix + 100) //Versioning

DECLARE_PMID(kWidgetIDSpace, kLaunchpadTreeViewWidgetID,		 kLNGPrefix + 102)



// "About Plug-ins" sub-menu:
#define kLNGAboutMenuKey			kLNGStringPrefix "kLNGAboutMenuKey"
#define kLNGAboutMenuPath		kSDKDefStandardAboutMenuPath kLNGCompanyKey

// "Plug-ins" sub-menu:
#define kLNGPluginsMenuKey 		kLNGStringPrefix "kLNGPluginsMenuKey"
#define kLNGPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kLNGCompanyKey kSDKDefDelimitMenuPath kLNGPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kLNGAboutBoxStringKey	kLNGStringPrefix "kLNGAboutBoxStringKey"
#define kLNGTargetMenuPath kLNGPluginsMenuPath

// added by avinash
#define kLNGSelectAHostTextKey		kLNGStringPrefix "kLNGSelectAHostTextKey"
#define kLNGEmailIdTextKey	kLNGStringPrefix	"kLNGEmailIdTextKey"
#define kLNGNoEnVSetTextKey	kLNGStringPrefix	"kLNGNoEnVSetTextKey"
#define kLNGDisplayNameTextKey	kLNGStringPrefix	"kLNGDisplayNameTextKey"
#define kLNGApsivaHostTextKey kLNGStringPrefix	"kLNGApsivaHostTextKey"
#define kLNGPasswordTextKey	kLNGStringPrefix	"kLNGPasswordTextKey"
#define kLNGLoginSuccessfulTextKey	kLNGStringPrefix "kLNGLoginSuccessfulTextKey"
#define kLNGApsivaHostnameURLTextKey	kLNGStringPrefix	"kLNGApsivaHostnameURLTextKey"
#define kPropDbVendTextKey	kLNGStringPrefix	"kPropDbVendTextKey"
#define kPropDbVerTextKey	kLNGStringPrefix "kPropDbVerTextKey"
#define kPropDbSrvTextKey	kLNGStringPrefix	"kPropDbSrvTextKey"
#define kPropDbPortTextKey	kLNGStringPrefix	"kPropDbPortTextKey"
#define kPropDbUnameTextKey		kLNGStringPrefix	"kPropDbUnameTextKey"
#define kPropDbPwordTextKey	kLNGStringPrefix	"kPropDbPwordTextKey"
#define kEnvPropTextKey	kLNGStringPrefix "kEnvPropTextKey"
#define kProp2EnvNameTextKey kLNGStringPrefix	"kProp2EnvNameTextKey"
#define kProp2ConnNameTextKey	kLNGStringPrefix "kProp2ConnNameTextKey"
#define kPropDlgNoProxyTextKey	 kLNGStringPrefix "kPropDlgNoProxyTextKey"

#define kBlankStringTextKey	kLNGStringPrefix	"kBlankStringTextKey"
#define kEnvironmentsTextkey	kLNGStringPrefix	"kEnvironmentsTextkey"
#define kCarStringTextKey	kLNGStringPrefix	"kCarStringTextKey"
#define kProxyStaticKey	kLNGStringPrefix	"kProxyStaticKey"
#define kProtocolStaticKey kLNGStringPrefix "kProtocolStaticKey"
#define kHostServerStaticKey	kLNGStringPrefix "kHostServerStaticKey"
#define kPortStaticKey	kLNGStringPrefix "kPortStaticKey"
#define kUseridStaticKey kLNGStringPrefix "kUseridStaticKey"
#define kPasswordStaticKey kLNGStringPrefix "kPasswordStaticKey"
#define kUseProxyStaticKey kLNGStringPrefix "kUseProxyStaticKey"
#define kAttemptingConnectionStaticTextKey kLNGStringPrefix "kAttemptingConnectionStaticTextKey"
#define kEnviornmentNameTextKey		kLNGStringPrefix	"kEnviornmentNameTextKey"
#define kModeTextKey	kLNGStringPrefix "kModeTextKey"
#define kHTTPTextKey	kLNGStringPrefix	"kHTTPTextKey"
#define kJDBCTextKey	kLNGStringPrefix	"kJDBCTextKey"
#define kServerUrlTextKey	kLNGStringPrefix "kServerUrlTextKey"
#define kLNGLogoutStringKey		kLNGStringPrefix "kLNGLogoutStringKey"
#define kPlsSelectAClientStringKey		kLNGStringPrefix "kPlsSelectAClientStringKey"
#define kSaveLogStringKey kLNGStringPrefix "kSaveLogStringKey"
#define kClearLogStringKey kLNGStringPrefix "kClearLogStringKey"

// till here


// Menu item positions:

#define kLNGDialogTitleKey kLNGStringPrefix "kLNGDialogTitleKey"
// "Plug-ins" sub-menu item key for dialog:
#define kLNGDialogMenuItemKey kLNGStringPrefix "kLNGDialogMenuItemKey"
#define kLNGResetMenuItemKey kLNGStringPrefix "kLNGResetMenuItemKey"
// "Plug-ins" sub-menu item position for dialog:
#define kLNGDialogMenuItemPosition	1.0
#define kLNGResetMenuItemPosition 8//15.0

#define kLaunchpadDialog1TitleKey	kLNGStringPrefix "kLaunchpadDialog1TitleKey"
#define kLoginDialogTitleKey		kLNGStringPrefix "kLoginDialogTitleKey"
#define kPropertiesDialogTitleKey	kLNGStringPrefix "kPropertiesDialogTitleKey"
#define kProperties2DialogTitleKey	kLNGStringPrefix "kProperties2DialogTitleKey"
#define kLNGAboutBoxStringKey		kLNGStringPrefix "kLNGAboutBoxStringKey"

#define kLoginStringKey			kLNGStringPrefix "kLoginStringKey"
#define kPropStringKey			kLNGStringPrefix "kPropStringKey"
#define kNewStringKey			kLNGStringPrefix "kNewStringKey"
#define kDeleteStringKey		kLNGStringPrefix "kDeleteStringKey"

#define kLNGPRINTsourceKey		kLNGStringPrefix	"kLNGPRINTsourceKey"
#define kLNGLoginStringKey		kLNGStringPrefix	"kLNGLoginStringKey"


#define kLNGTreePanelNodeRsrcID 3200
#define kLNGVesionUpdaterDialogResourceID 3201  // Versioning
// Initial data format version numbers  
#define kLNGFirstMajorFormatNumber  RezLong(1)
#define kLNGFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kLNGCurrentMajorFormatNumber kLNGFirstMajorFormatNumber
#define kLNGCurrentMinorFormatNumber kLNGFirstMinorFormatNumber

//PNGR ID
#define kPNGFirstLoginIconRsrcID      					1000
#define kPNGFirstLoginIconRollRsrcID  					1000
#define kPNGFirstCancelIconRsrcID     					1001
#define kPNGFirstCancelIconRollRsrcID 					1001
		
#define kPNGPropertiesIconRsrcID      					1002
#define kPNGPropertiesIconRollRsrcID  					1002

#define kPNGNewIconRsrcID             					1003
#define kPNGNewIconRollRsrcID         					1003

#define kPNGDeleteIconRsrcID          					1004
#define kPNGDeleteIconRollRsrcID      					1004

#define kPNGSecondCancelIconRsrcID    					1005
#define kPNGSecondCancelIconRollRsrcID					1005

#define kPNGSecondLoginIconRsrcID     					1006
#define kPNGSecondLoginIconRollRsrcID 					1006

#define kPNGNewOKIconRsrcID           					1007
#define kPNGNewOKIconRollRsrcID       					1007

#define kPNGThirdCancelIconRsrcID     					1008
#define kPNGThirdCancelIconRollRsrcID 					1008

#define kPNGOptionsIconRsrcID     						1009
#define kPNGOptionsIconRollRsrcID 						1009

#define kPNGContinueIconRsrcID     						1010
#define kPNGContinueIconRollRsrcID 						1010

#define kPNGUpdatesDownLoadRollIconRsrcID               1011
#define kPNGUpdatesDownLoadIconRsrcID                   1011

#endif // __LNGID_h__
