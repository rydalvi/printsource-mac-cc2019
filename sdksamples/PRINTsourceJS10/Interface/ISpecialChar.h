#ifndef _ISPECIALCHAR__H_
#define _ISPECIALCHAR__H_
#include "AFWJSID.h"

#include "IPMUnknown.h"
#include "vector"
#include "TagStruct.h"
#include "AdvanceTableScreenValue.h"

using namespace std;
class HtmlTracker
{
public:
	int32 startIndex;
	int32 EndIndex;
	int32 style; // if 1 = bold, 2 = Italic, by default =0 
	bool16 flag;

	HtmlTracker()
	{
		startIndex  = 0;
		EndIndex = 0;
		style= 0; 
		flag= kFalse;
	}
};
typedef vector<HtmlTracker> VectorHtmlTrackerValue, *VectorHtmlTrackerPtr;

class ISpecialChar : public IPMUnknown
{
	public:
		enum { kDefaultIID = IID_ISPECIALCHAR };
		virtual PMString translateString(const PMString&)=0;
		virtual PMString translateToDatabaseCode(const PMString&)=0;
		virtual PMString handleAmpersandCase(PMString &)=0;
		virtual void ChangeQutationMarkONOFFState(bool16)=0;
		virtual PMString translateStringNew(const PMString& code, VectorHtmlTrackerPtr vectHtmlValue)=0;
		//virtual TagList translateStringForAdvanceTableCell(const CellData &,PMString &)=0;
};

#endif	