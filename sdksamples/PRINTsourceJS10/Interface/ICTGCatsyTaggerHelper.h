#ifndef __CTGCATSYTAGGERHELPER_H__
#define __CTGCATSYTAGGERHELPER_H__

//#include "VCPlugInHeaders.h"
#include "IPMUnknown.h"
#include "CTGID.h"


class ICTGCatsyTaggerHelper : public IPMUnknown
{

 public:

	enum	{kDefaultIID = IID_ICATSYTAGGERIFACE};
    
    virtual bool16 populateTreeForItemAttributes(double curSelSubecId, double itemId, double itemTypeId, PMString ItemDisplayName) = 0;

};

#endif
