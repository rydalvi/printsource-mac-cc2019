//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __DSFID_h__
#define __DSFID_h__

#include "SDKDef.h"

// Company:
#define kDSFCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kDSFCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kDSFPluginName	"APJS9_DataSprayer"			// Name of this plug-in.
#define kDSFPrefixNumber	0xDB1800 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kDSFVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kDSFAuthor		"Apsiva Inc"					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kDSFPrefixNumber above to modify the prefix.)
#define kDSFPrefix		RezLong(kDSFPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kDSFStringPrefix	SDK_DEF_STRINGIZE(kDSFPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kDSFMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kDSFMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kDSFPluginID, kDSFPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kDataSprayerBoss,	kDSFPrefix + 3)
DECLARE_PMID(kClassIDSpace, kBscTAAttrBoss,		kDSFPrefix + 4)
DECLARE_PMID(kClassIDSpace, kTableUtilityBoss,	kDSFPrefix + 5)
//DECLARE_PMID(kClassIDSpace, kDSFBoss, kDSFPrefix + 6)
//DECLARE_PMID(kClassIDSpace, kDSFBoss, kDSFPrefix + 7)
//DECLARE_PMID(kClassIDSpace, kDSFBoss, kDSFPrefix + 8)
//DECLARE_PMID(kClassIDSpace, kDSFBoss, kDSFPrefix + 9)
//DECLARE_PMID(kClassIDSpace, kDSFBoss, kDSFPrefix + 10)
//DECLARE_PMID(kClassIDSpace, kDSFBoss, kDSFPrefix + 11)
//DECLARE_PMID(kClassIDSpace, kDSFBoss, kDSFPrefix + 12)
//DECLARE_PMID(kClassIDSpace, kDSFBoss, kDSFPrefix + 13)
//DECLARE_PMID(kClassIDSpace, kDSFBoss, kDSFPrefix + 14)
//DECLARE_PMID(kClassIDSpace, kDSFBoss, kDSFPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kDSFBoss, kDSFPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kDSFBoss, kDSFPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kDSFBoss, kDSFPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kDSFBoss, kDSFPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kDSFBoss, kDSFPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kDSFBoss, kDSFPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kDSFBoss, kDSFPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kDSFBoss, kDSFPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kDSFBoss, kDSFPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kDSFBoss, kDSFPrefix + 25)


// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_IDataSprayer,	kDSFPrefix + 0)
DECLARE_PMID(kInterfaceIDSpace, IID_ITEXTMISCELLANYSUITEE1,		kDSFPrefix + 1 )
//DECLARE_PMID(kInterfaceIDSpace, IID_ISLUGDATA,		kDSFPrefix + 2 )
DECLARE_PMID(kInterfaceIDSpace, IID_ITABLEUTILITY,	kDSFPrefix + 3 )
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_IDSFINTERFACE, kDSFPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kDataSprayerImpl,						kDSFPrefix + 3 )
DECLARE_PMID(kImplementationIDSpace, kTextMiscellanySuiteASBImpl1,			kDSFPrefix + 4)
DECLARE_PMID(kImplementationIDSpace, kTextMiscellanySuiteLayoutCSBImpl1,	kDSFPrefix + 5)
DECLARE_PMID(kImplementationIDSpace, kBscTAAttrReportImpl,					kDSFPrefix + 6)
//DECLARE_PMID(kImplementationIDSpace, kSlugImpl,								kDSFPrefix + 7)
DECLARE_PMID(kImplementationIDSpace, kTableUtilityImpl,						kDSFPrefix + 8)
//DECLARE_PMID(kImplementationIDSpace, kDSFImpl, kDSFPrefix + 6)
//DECLARE_PMID(kImplementationIDSpace, kDSFImpl, kDSFPrefix + 7)
//DECLARE_PMID(kImplementationIDSpace, kDSFImpl, kDSFPrefix + 8)
//DECLARE_PMID(kImplementationIDSpace, kDSFImpl, kDSFPrefix + 9)
//DECLARE_PMID(kImplementationIDSpace, kDSFImpl, kDSFPrefix + 10)
//DECLARE_PMID(kImplementationIDSpace, kDSFImpl, kDSFPrefix + 11)
//DECLARE_PMID(kImplementationIDSpace, kDSFImpl, kDSFPrefix + 12)
//DECLARE_PMID(kImplementationIDSpace, kDSFImpl, kDSFPrefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kDSFImpl, kDSFPrefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kDSFImpl, kDSFPrefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kDSFImpl, kDSFPrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kDSFImpl, kDSFPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kDSFImpl, kDSFPrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kDSFImpl, kDSFPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kDSFImpl, kDSFPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kDSFImpl, kDSFPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kDSFImpl, kDSFPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kDSFImpl, kDSFPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kDSFImpl, kDSFPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kDSFImpl, kDSFPrefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kDSFAboutActionID, kDSFPrefix + 0)
//DECLARE_PMID(kActionIDSpace, kDSFActionID, kDSFPrefix + 5)
//DECLARE_PMID(kActionIDSpace, kDSFActionID, kDSFPrefix + 6)
//DECLARE_PMID(kActionIDSpace, kDSFActionID, kDSFPrefix + 7)
//DECLARE_PMID(kActionIDSpace, kDSFActionID, kDSFPrefix + 8)
//DECLARE_PMID(kActionIDSpace, kDSFActionID, kDSFPrefix + 9)
//DECLARE_PMID(kActionIDSpace, kDSFActionID, kDSFPrefix + 10)
//DECLARE_PMID(kActionIDSpace, kDSFActionID, kDSFPrefix + 11)
//DECLARE_PMID(kActionIDSpace, kDSFActionID, kDSFPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kDSFActionID, kDSFPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kDSFActionID, kDSFPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kDSFActionID, kDSFPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kDSFActionID, kDSFPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kDSFActionID, kDSFPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kDSFActionID, kDSFPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kDSFActionID, kDSFPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kDSFActionID, kDSFPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kDSFActionID, kDSFPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kDSFActionID, kDSFPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kDSFActionID, kDSFPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kDSFActionID, kDSFPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kDSFActionID, kDSFPrefix + 25)


// WidgetIDs:
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 2)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 3)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 4)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 5)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 6)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 7)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 8)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 9)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 10)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 11)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 12)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 13)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 14)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 15)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 16)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 17)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 18)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 19)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 20)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 21)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 22)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 23)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 24)
//DECLARE_PMID(kWidgetIDSpace, kDSFWidgetID, kDSFPrefix + 25)


// "About Plug-ins" sub-menu:
#define kDSFAboutMenuKey			kDSFStringPrefix "kDSFAboutMenuKey"
#define kDSFAboutMenuPath		kSDKDefStandardAboutMenuPath kDSFCompanyKey

// "Plug-ins" sub-menu:
#define kDSFPluginsMenuKey 		kDSFStringPrefix "kDSFPluginsMenuKey"
#define kDSFPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kDSFCompanyKey kSDKDefDelimitMenuPath kDSFPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kDSFAboutBoxStringKey	kDSFStringPrefix "kDSFAboutBoxStringKey"
#define kDSFTargetMenuPath kDSFPluginsMenuPath

// Menu item positions:


// Initial data format version numbers
#define kDSFFirstMajorFormatNumber  RezLong(1)
#define kDSFFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kDSFCurrentMajorFormatNumber kDSFFirstMajorFormatNumber
#define kDSFCurrentMinorFormatNumber kDSFFirstMinorFormatNumber

#endif // __DSFID_h__
