#ifndef __TableSourceInfoValue_h__
#define __TableSourceInfoValue_h__

//#include "ObjectTableValue.h"
#include "AdvanceTableScreenValue.h"
#include "ItemTableValue.h"

class TableSourceInfoValue
{
	vector<CItemTableValue>  *VectorScreenTableInfoPtr;
	vector<CItemTableValue>	*VectorScreenItemTableInfoPtr;
	vector<CAdvanceTableScreenValue>  *VectorAdvanceTableScreenValuePtr;
	bool16 isProduct;
	bool16 isTable;
	bool16 isCallFromTABLEsource;
	vector<double>  vec_TableType_ID;
	vector<double> vec_Table_ID;

public:
	TableSourceInfoValue()
	{
		VectorScreenTableInfoPtr = NULL; 
		VectorAdvanceTableScreenValuePtr = NULL;
		VectorScreenItemTableInfoPtr = NULL;
		isProduct = kFalse; 
		isTable = kFalse;
		isCallFromTABLEsource = kFalse;
		vec_TableType_ID.clear();
		vec_Table_ID.clear();
	}
	inline void setAll(vector<CItemTableValue>*VtItemTableInfoPtr,vector<CItemTableValue>* ScreenTableInfoPtr, vector<CAdvanceTableScreenValue>* AdvanceTableScreenValuePtr, bool16 isPrd , bool16 isTble , bool16 isCallFromTBLEsrc,vector<double> vec_TblType_ID,vector<double> vec_Tbl_ID)
	{
		VectorScreenItemTableInfoPtr=VtItemTableInfoPtr;
		VectorScreenTableInfoPtr = ScreenTableInfoPtr; 
		VectorAdvanceTableScreenValuePtr = AdvanceTableScreenValuePtr;
		isProduct = isPrd; 
		isTable = isTble;
		isCallFromTABLEsource = isCallFromTBLEsrc;
		vec_TableType_ID=vec_TblType_ID;
		vec_Table_ID=vec_Tbl_ID;
	}
	
	inline bool16 getIsProduct()
	{
		return 	isProduct;	
	}
	inline bool16 getIsTable()
	{
		return 	isTable;	
	}
	inline bool16 getIsCallFromTABLEsource()
	{
		return 	isCallFromTABLEsource;	
	}
	inline vector<CItemTableValue> *getVectorScreenTableInfoPtr()
	{
		return VectorScreenTableInfoPtr;
	}
	inline vector<CAdvanceTableScreenValue> *getVectorAdvanceTableScreenValuePtr()
	{
		return VectorAdvanceTableScreenValuePtr;
	}
	
	inline vector<double> getVec_TableType_ID()
	{
		return vec_TableType_ID;
	}
	inline vector<double> getVec_Table_ID()
	{
		return vec_Table_ID;
	}

	inline vector<CItemTableValue>*getVectorScreenItemTableInfoPtr()
	{
		return VectorScreenItemTableInfoPtr;
	}
	
};


#endif 