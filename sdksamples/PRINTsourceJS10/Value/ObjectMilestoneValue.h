#ifndef __CObjectMilestoneValue_h__
#define __CObjectMilestoneValue_h__

#include "VCPluginHeaders.h"
#include "PMString.h"

class CObjectMilestoneValue
{
private:

	double object_id;
	double milestone_id;
	PMString due_date;
	double due_userid;
	bool16 completed;
	PMString complete_date;
	double complete_userid;
	double locale_id;
	bool16 deleted;
	double createUser_id;
	PMString createdate;
	double updateUser_id;
	PMString updatedate;

public:
	CObjectMilestoneValue()
	{
	}
	
	double getobject_id()
	{
		return object_id;
	}

	double getmilestone_id()
	{
		return milestone_id;
	}
	double getdue_userid()
	{
		return due_userid;
	}

	double getcomplete_userid()
	{
		return complete_userid;
	}

	double getlocale_id()
	{
		return locale_id;
	}

	double getcreateUser_id()
	{
		return createUser_id;
	}

	double getupdateUser_id()
	{
		return updateUser_id;
	}

	PMString getdue_date()
	{
		return due_date;
	}

	PMString getcomplete_date()
	{
		return complete_date;
	}

	PMString getcreatedate()
	{
		return createdate;
	}

	PMString getupdatedate()
	{
		return updatedate;
	}

	void setobject_id(double newobject_id)
	{
		object_id = newobject_id;
	}
	
	void setmilestone_id(double newmilestone_id)
	{
		milestone_id = newmilestone_id;
	}

	void setdue_userid(double newdue_userid)
	{
		due_userid = newdue_userid;
	}

	void setcomplete_userid(double newcomplete_userid)
	{
		complete_userid = newcomplete_userid;
	}

	void setlocale_id(double newlocale_id)
	{
		locale_id = newlocale_id;
	}

	void setcreateUser_id(double newcreateUser_id)
	{
		createUser_id = newcreateUser_id;
	}

	void setupdateUser_id(double newupdateUser_id)
	{
		updateUser_id = newupdateUser_id;
	}

	void setdue_date(PMString newdue_date)
	{
		due_date = newdue_date;
	}

	void setcomplete_date(PMString newcomplete_date)
	{
		complete_date = newcomplete_date;
	}

	void setcreatedate(PMString newcreatedate)
	{
		createdate = newcreatedate;
	}

	void setupdatedate(PMString newupdatedate)
	{
		updatedate = newupdatedate;
	}
//added by Yogesh on 24.2.05 at 4.37 pm
	bool16 getcompleted()
	{
		return completed;
	}

	void setcompleted(bool16 status)
	{
		completed = status;
	}

	bool16 getdeleted()
	{
		return deleted;
	}

	void setdeleted(bool16 status)
	{
		deleted = status;
	}
//ended by Yogesh on 24.2.05 at 4.37 pm
	
	
};

#endif