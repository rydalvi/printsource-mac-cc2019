#ifndef __PRICINGMODULE__
#define __PRICINGMODULE__

#include "VCPluginHeaders.h"
#include "PMString.h"

using namespace std;

class PricingModule
{
private:
    double pricingModuleId;
    PMString module;
    PMString description;

public:
	PricingModule()
	{
        pricingModuleId = -1;
        module = "";
        description = "";
	}
	
	~PricingModule()
	{
	}
	
    double getPricingModuleId()
    {
        return pricingModuleId;
    }
    
    void setPricingModuleId(double id)
    {
        pricingModuleId = id;
    }
    
    PMString getModule()
    {
        return module;
    }
    
    void setModule(PMString module)
    {
        this->module = module;
    }
    
    PMString getDescription()
    {
        return description;
    }
    
    void setDescription(PMString description)
    {
        this->description = description;
    }
};

#endif