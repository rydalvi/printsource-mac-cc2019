#ifndef __CSprayStencilInfo_h__
#define __CSprayStencilInfo_h__

#include "VCPluginHeaders.h"
#include "PMString.h"

using namespace std;

class CSprayStencilInfo
{
public:
	bool16 isCopy;
	bool16 isProductCopy;
	bool16 isSectionCopy;
	bool16 isEventField;

	bool16 isAsset;
	bool16 isProductAsset;
	bool16 isSectionLevelAsset;

	bool16 isDBTable;
	bool16 isProductDBTable;
	bool16 isCustomTablePresent;

	bool16 isHyTable;
	bool16 isProductHyTable;
	bool16 isSectionLevelHyTable;
	
	bool16 isChildTag;
	bool16 isProductChildTag;

	bool16 isBMSAssets;
	bool16 isProductBMSAssets;
	bool16 isSectionLevelBMSAssets;

	bool16 isItemPVMPVAssets;
	bool16 isProductPVMPVAssets;
	bool16 isSectionPVMPVAssets;
	bool16 isPublicationPVMPVAssets;
	bool16 isCatagoryPVMPVAssets;

	bool16 isEventSectionImages;
	bool16 isCategoryImages;
	bool16 isSprayItemPerFrame;

	double langId;

	vector<double > AttributeIds; //All Attribute Ids
	vector<double> ProductAttributeIds;
	vector<double> itemAttributeIds;
	vector<double> childItemAttributeIds;
	vector<double > AssetIds; //All Asset Ids
	vector<double> ProductAssetIds;
	vector<double> itemAssetIds;
	vector<double > dBTypeIds;
	vector<double > HyTypeIds;

	vector<double > itemPVAssetIdList;
	vector<double > productPVAssetIdList;
	vector<double > sectionPVAssetIdList;
	vector<double > publicationPVAssetIdList;
	vector<double > catagoryPVAssetIdList;
	vector<double > categoryAssetIdList;
	vector<double > eventSectionAssetIdList;

	vector<double > itemGroupIds;
	vector<double > itemIds;
    vector<double> itemAttributeGroupIds;

	CSprayStencilInfo()
	{
		isCopy = kFalse;
		isProductCopy = kFalse;
		isSectionCopy = kFalse;

		isAsset = kFalse;
		isProductAsset = kFalse;
		isSectionLevelAsset = kFalse;

		isDBTable = kFalse;
		isProductDBTable = kFalse;
		isCustomTablePresent = kFalse;

		isHyTable = kFalse;
		isProductHyTable = kFalse;
		isSectionLevelHyTable = kFalse;
	
		isChildTag = kFalse;
		isProductChildTag = kFalse;

		isBMSAssets = kFalse;
		isProductBMSAssets = kFalse;
		isSectionLevelBMSAssets = kFalse;

		isItemPVMPVAssets = kFalse;
		isProductPVMPVAssets = kFalse;
		isSectionPVMPVAssets = kFalse;
		isPublicationPVMPVAssets = kFalse;
		isCatagoryPVMPVAssets = kFalse;
		isEventSectionImages = kFalse;
		isCategoryImages = kFalse;
		langId = 91; // default lang
		isSprayItemPerFrame = kFalse;
		
	}	
};
typedef vector<CSprayStencilInfo> vectorCSprayStencilInfo;

#endif
