#ifndef __CLIENTINFOVALUE__
#define __CLIENTINFOVALUE__

#include "VCPluginHeaders.h"
#include "PMString.h"

using namespace std;

class ClientInfoValue
{
private:	
	PMString clientno;
	PMString assetserverpath;

	
	

	PMString documentPath;
	PMString language;
	PMString project;
	bool16   isActive;
	int32    isItemTablesasTabbedText;
	int32	 isAddTableHeaders;
	int32    assetStatus;
	int32    logLevel;
	int32    missingFlag;
	PMString sectionID;
	PMString isONESource ;
	int32    defaultFlow ;	// 1 = Horizontal Flow , 0 = Vertical Flow.
	int32    displayPartnerImages;
	int32    displayPickListImages;
	int32    isShowObjectCount;
	PMString dbAppenderForPassword;	

	int32 byPassForSingleSelectionSpray;
	int32 horizontalSpacing;
	int32 verticalSpacing;



public:
	ClientInfoValue()
	{
		clientno = "";
		assetserverpath = "";

		documentPath= "";	
		language= "";	
		project= "";	
		isActive= kFalse;
		isItemTablesasTabbedText = 0;
		isAddTableHeaders = 0;
		assetStatus = 0;
		logLevel = 0;
		missingFlag=0;
		
		sectionID = "";
	    isONESource = "" ;
		defaultFlow = 1; 
		displayPartnerImages = 1;
		displayPickListImages = 1;
		isShowObjectCount = 0;
		dbAppenderForPassword = "";	

		byPassForSingleSelectionSpray = 1;
		horizontalSpacing = 0;
		verticalSpacing = 0;
	}
	
	~ClientInfoValue()
	{
	}

	
	PMString getClientno()
	{
		return clientno;
	}

	void setClientno(PMString str)
	{
		clientno = str;
	}

	
	PMString getAssetserverpath()
	{
		return assetserverpath;
	}

	void setAssetserverpath(PMString str)
	{
		assetserverpath = str;
	}
	
	void setDocumentPath(PMString docPath)
	{
		documentPath = docPath;
	}

	void setLanguage(PMString lang)
	{
		language = lang;
	}

	void setProject(PMString proj)
	{
		project = proj;
	}

	void setIsActive(bool16 flag)
	{
		isActive = flag;
	}

	/*PMString getImagePath()
	{
		return imagePath;
	}*/

	PMString getDocumentPath()
	{
		return documentPath;
	}

	PMString getLanguage()
	{
		return language;
	}

	PMString getProject()
	{
		return project;
	}

	bool16 getIsActive()
	{
		return isActive;
	}

	int32 getisItemTablesasTabbedText(void)
	{
		return isItemTablesasTabbedText;
	}
	
	int32 getisAddTableHeaders(void)
	{
		return isAddTableHeaders;
	}

	void setisAddTableHeaders(int32 state)
	{
		isAddTableHeaders = state;
	}

	void setisItemTablesasTabbedText(int32 state)
	{
		isItemTablesasTabbedText = state;
	}

//------------ 20-feb chetan
	int32 getAssetStatus(void)
	{
		return assetStatus;
	}

	int32 getLogLevel(void)
	{
		return logLevel;
	}

	void setAssetStatus(int32 aStatus)
	{
		assetStatus = aStatus ;
	}

	void setLogLevel(int32 LogLevel)
	{
		logLevel = LogLevel;
	}
	void setMissingFlag(int32 MissingFlag)
	{
		missingFlag=MissingFlag;

	}
	int32 getMissingFlag(void)
	{
		return missingFlag;
	}


	PMString getSectionID(void)	//--------
	{
		return sectionID;
	}

	void setSectionID(PMString sectID)
	{
		sectionID = sectID;

	}
	
	PMString getIsONESource(void)
	{
		return isONESource;
	}

	void setIsONESource(PMString isOneSource)
	{
		isONESource = isOneSource;

	}


	void setdefaultFlow(int32 defFlow)
	{
		defaultFlow=defFlow;

	}
	int32 getdefaultFlow(void)
	{
		return defaultFlow;
	}
	
	void setDisplayPartnerImages(int32 partnerImg)
	{
		displayPartnerImages=partnerImg;

	}
	int32 getDisplayPartnerImages(void)
	{
		return displayPartnerImages;
	}

	void setDisplayPickListImages(int32 pickListImg)
	{
		displayPickListImages=pickListImg;

	}
	int32 getDisplayPickListImages(void)
	{
		return displayPickListImages;
	}
	
	void setShowObjectCountFlag(int32 isShowObjCnt)
	{
		isShowObjectCount=isShowObjCnt;

	}
	int32 getShowObjectCountFlag(void)
	{
		return isShowObjectCount;
	}

	void setdbAppenderForPassword(PMString userPassword)
	{
		dbAppenderForPassword = userPassword;
	}

	int32 getByPassForSingleSelectionSprayFlag(void)
	{
		return byPassForSingleSelectionSpray;
	}

	void setByPassForSingleSelectionSprayFlag(int32 inbyPassForSingleSelectionSpray)
	{
		byPassForSingleSelectionSpray = inbyPassForSingleSelectionSpray;
	}

	int32 getHorizontalSpacing(void)
	{
		return horizontalSpacing;
	}

	void setHorizontalSpacing(int32 inhorizontalSpacing)
	{
		horizontalSpacing = inhorizontalSpacing;
	}

	int32 getVerticalSpacing(void)
	{
		return verticalSpacing;
	}

	void setVerticalSpacing(int32 inverticalSpacing)
	{
		verticalSpacing = inverticalSpacing;
	}


	

};

#endif
