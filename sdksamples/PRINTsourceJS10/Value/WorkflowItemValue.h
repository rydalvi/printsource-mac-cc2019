#ifndef __WORKFLOWITEMVALUE__
#define __WORKFLOWITEMVALUE__

#include "VCPluginHeaders.h"
#include "PMString.h"

using namespace std;

class WorkflowItemValue
{
private:
    double languageId;
    double fieldId;
    PMString fieldKey;
    PMString fieldName;
    PMString value;
    
public:
	WorkflowItemValue()
	{
        languageId=-1;
        fieldId =-1;
        fieldKey ="";
        fieldName = "";
        value = "";
	}
	
	~WorkflowItemValue()
	{
	}
    
    double getLanguageId()
    {
        return languageId;
    }
    
    void setLanguageId(double id)
    {
        languageId = id;
    }
    
    double getFieldId()
    {
        return fieldId;
    }
    
    void setFieldId(double id)
    {
        fieldId = id;
    }
    
    PMString getFieldKey()
    {
        return fieldKey;
    }
    
    void setFieldKey(PMString stringObj)
    {
        fieldKey = stringObj;
    }
    
    PMString getFieldName()
    {
        return fieldName;
    }
    
    void setFieldName(PMString stringObj)
    {
        fieldName = stringObj;
    }
    
    PMString getValue()
    {
        return value;
    }
    
    void setValue(PMString stringObj)
    {
        value = stringObj;
    }
	
};

#endif