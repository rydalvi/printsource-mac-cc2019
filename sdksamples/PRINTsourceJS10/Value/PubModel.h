#ifndef __PUBMODEL_H__
#define __PUBMODEL_H__

#include "VCPlugInHeaders.h"
#include "PMString.h"

class CPubModel{
private:

	double eventId;
	PMString name;
	double typeId;
	int32 level_No;
	int32 seq_Count;
	int32 child_Count;
	double parentId;
	PMString eventType;
	double rootId;
	double pubTypeId;

	/*
	
	PMString number;
	PMString book_Name;
	int32 page_Width;
	int32 page_Height;
	PMString doc_Name;
	PMString doc_Image_Name;
	PMString master_Template_Filename;
	PMString product_Stencil;
	PMString item_Stencil;
	PMString proof1_Name;
	PMString proof2_Name;
	PMString proof3_Name;
	PMString proof4_Name;
	PMString proof5_Name;
	PMString proof6_Name;
	bool16 whiteboard_Create;
	int32 vertical_Box_Spacing;
	int32 horizontal_Box_Spacing;
	bool16 alternate;
	bool16 flow_Direction;
	bool16 flow_Preference;
	int32 whiteboard_Stage;
	int32 spreadCount;
	PMString Comments;
	bool16 isAutoSpray;
	bool16 Ispage_based;
	int32 doc_page_no;
	int32 wb_page_no;
	int32 spread_no;
	int32 stencil_id;
	bool16 left_Hand_Odd_pages;
	bool16 locked;
	int32 pub_type_id;
	*/


public:

	CPubModel()
	{
		eventId = -1;
		name ="";
		typeId = -1;		
		level_No = -1;
		seq_Count = -1;
		child_Count = -1;
		parentId = -1;
		eventType = "";
		rootId = -1;
		pubTypeId = -1;

		/*
		
		number = "-1";
		book_Name = "";
		page_Width = -1;
		page_Height = -1;
		doc_Name = "";
		doc_Image_Name = "";
		master_Template_Filename = "";
		product_Stencil = "";
		item_Stencil = "";
		proof1_Name = "";
		proof2_Name = "";
		proof3_Name = "";
		proof4_Name = "";
		proof5_Name = "";
		proof6_Name = "";
		whiteboard_Create = kTrue;
		vertical_Box_Spacing = -1;
		horizontal_Box_Spacing = -1;
		alternate = kTrue;
		flow_Direction = kTrue;
		flow_Preference = kTrue;
		whiteboard_Stage = -1;
		int32 spreadCount = -1;
		Comments = "";
		isAutoSpray = kTrue;		
		Ispage_based = kFalse;
		doc_page_no = -1;
		wb_page_no = -1;
		spread_no = -1;
		stencil_id = -1;
		left_Hand_Odd_pages = kFalse;
		locked = kFalse;
		pub_type_id = -1;
		*/
	}

	double getEventId()
	{
		return eventId;
	}

	PMString getName()
	{
		return name;
	}

	double getTypeID()
	{
		return typeId;
	}

	double getRootID()
	{
		return rootId;
	}

	int32 getchild_Count()
	{
		return child_Count;
	}

	int32 getlevel_No()
	{
		return level_No;
	}

	int32 getseq_Count()
	{
		return seq_Count;
	}
	void setEventId(double pubID)
	{
		eventId = pubID;
	}

	void setName(PMString name1)
	{
		name = name1;
	}

	void setTypeID(double typeID)
	{
		typeId = typeID;
	}

	void setRootID(double rootID)
	{
		rootId = rootID;
	}

	void setlevel_No(int32 levNo)
	{
		level_No = levNo;
	}

	void setseq_Count(int32 seqNo)
	{
		seq_Count = seqNo;
	}

	void setchild_Count(int32 childNO)
	{
		child_Count = childNO;
	}

	double getParentId()
	{
		return parentId;
	}

	void setParentId(double parentID)
	{
		parentId = parentID;
	}

	PMString getEventType() {
		return eventType;
	}

	void setEventType(PMString pubTypeString) {
		this->eventType = pubTypeString;
	}

	void setPubTypeId(double intId) {
		pubTypeId = intId;
	}
	double getPubTypeId() {
		return pubTypeId;
	}

	////30Oct..
	//PMString getNumber()
	//{
	//	return number;
	//}
	//void setNumber(PMString num)
	//{
	//	number = num;
	//}

	//end 30Oct..

//14-april book_Name
	/*PMString getBookName()
	{
		return book_Name;
	}
	void setBookName(PMString bookName)
	{
		book_Name = bookName;
	}

	int32 getPageWidth() 
	{
		return page_Width;
	}
	
	void setPageWidth(int32 pageWidth) 
	{
		page_Width = pageWidth;
	}

	int32 getPageHeight() 
	{
		return page_Height;
	}
	
	void setPageHeight(int32 pageHeight) 
	{
		page_Height = pageHeight;
	}

	PMString getDocName()
	{
		return doc_Name;
	}
	void setDocName(PMString docName)
	{
		doc_Name = docName;
	}

	PMString getDocImageName()
	{
		return doc_Image_Name;
	}
	void setDocImageName(PMString docImageName)
	{
		doc_Image_Name = docImageName;
	}

	PMString getMasterTemplateFilename()
	{
		return master_Template_Filename;
	}
	void setMasterTemplateFilename(PMString masterTemplateFilename)
	{
		master_Template_Filename = masterTemplateFilename;
	}

	PMString getProductStencil()
	{
		return product_Stencil;
	}
	void setProductStencil(PMString productStencil)
	{
		product_Stencil = productStencil;
	}

	PMString getItemStencil()
	{
		return item_Stencil;
	}
	void setItemStencil(PMString itemStencil)
	{
		item_Stencil = itemStencil;
	}

	PMString getProof1Name()
	{
		return proof1_Name;
	}
	void setProof1Name(PMString proof1Name)
	{
		proof1_Name = proof1Name;
	}

	PMString getProof2Name()
	{
		return proof2_Name;
	}
	void setProof2Name(PMString proof2Name)
	{
		proof2_Name = proof2Name;
	}

	PMString getProof3Name()
	{
		return proof3_Name;
	}
	void setProof3Name(PMString proof3Name)
	{
		proof3_Name = proof3Name;
	}

	PMString getProof4Name()
	{
		return proof4_Name;
	}
	void setProof4Name(PMString proof4Name)
	{
		proof4_Name = proof4Name;
	}

	PMString getProof5Name()
	{
		return proof5_Name;
	}
	void setProof5Name(PMString proof5Name)
	{
		proof5_Name = proof5Name;
	}

	PMString getProof6Name()
	{
		return proof6_Name;
	}
	void setProof6Name(PMString proof6Name)
	{
		proof6_Name = proof6Name;
	}

	bool16 getWhiteboardCreate()
	{
		return whiteboard_Create;
	}
	void setWhiteboardCreate(bool16 whiteboardCreate)
	{
		whiteboard_Create = whiteboardCreate;
	}

	int32 getVerticalBoxSpacing() 
	{
		return vertical_Box_Spacing;
	}
	
	void setVerticalBoxSpacing(int32 verticalBoxSpacing) 
	{
		vertical_Box_Spacing = verticalBoxSpacing;
	}

	int32 getHorizontalBoxSpacing() 
	{
		return horizontal_Box_Spacing;
	}
	
	void setHorizontalBoxSpacing(int32 horizontalBoxSpacing) 
	{
		horizontal_Box_Spacing = horizontalBoxSpacing;
	}

	bool16 getalternate()
	{
		return alternate;
	}
	void setalternate(bool16 alternating)
	{
		alternate = alternating;
	}

	bool16 getFlowDirection()
	{
		return flow_Direction;
	}
	void setFlowDirection(bool16 flowDirection)
	{
		flow_Direction = flowDirection;
	}

	bool16 getFlowPreference()
	{
		return flow_Preference;
	}
	void setFlowPreference(bool16 flowPreference)
	{
		flow_Preference = flowPreference;
	}

	int32 getWhiteboardStage() 
	{
		return whiteboard_Stage;
	}
	
	void setWhiteboardStage(int32 whiteboardStage) 
	{
		whiteboard_Stage = whiteboardStage;
	}*/
//14-april
//2-may
//	int32 getSpreadCount() 
//	{
//		return spreadCount;
//	}
//	
//	void setSpreadCount(int32 spread_Count) 
//	{
//		spreadCount = spread_Count;
//	}
////2-may
//
//// Added By Rahul
//
//	PMString getComments()
//	{
//		return Comments;
//	}
//	void setComments(PMString comment)
//	{
//		Comments = comment;
//	}
//// //
////// Chetan -- 28-09
//	bool16 getIsAutoSpray()
//	{
//		return isAutoSpray;
//	}
//	void setIsAutoSpray(bool16 sprayType)
//	{
//		isAutoSpray = sprayType;
//	}
////// Chetan -- 28-09
//
//	bool16 getIspage_based()
//	{
//		return Ispage_based;
//	}
//	int32 getdoc_page_no()
//	{
//		return doc_page_no;
//	}
//	int32 getwb_page_no()
//	{
//		return wb_page_no;
//	}
//	int32 getspread_no()
//	{
//		return spread_no;
//	}
//	int32 getstencil_id()
//	{
//		return stencil_id;
//	}
//
//	void setIspage_based(bool16 page_based)
//	{
//		Ispage_based = page_based;
//	}
//	void setdoc_page_no(int32 Doc_page_no)
//	{
//		doc_page_no = Doc_page_no;
//	}
//	void setwb_page_no(int32 Wb_page_no)
//	{
//		wb_page_no = Wb_page_no;
//	}
//	void setspread_no(int32 Spread_no)
//	{
//		spread_no = Spread_no;
//	}
//	void setstencil_id(int32 Stencil_id)
//	{
//		stencil_id = Stencil_id;
//	}
//
//	bool16 getLeftHandOddpages()
//	{
//		return left_Hand_Odd_pages;
//	}
//	void setLeftHandOddpages(bool16 IsLeftHandOddpages)
//	{
//		left_Hand_Odd_pages = IsLeftHandOddpages;
//	}
//
//	bool16 getlocked()
//	{
//		return locked;
//	}
//
//	void setLocked(bool16 isLocked)
//	{
//		locked = isLocked;
//	}
//
//	int32 getpub_type_id()
//	{
//		return pub_type_id;
//	}
//
//	void setpub_type_id(int32 Pub_type_id)
//	{
//		pub_type_id = Pub_type_id;
//	}
//


};
#endif