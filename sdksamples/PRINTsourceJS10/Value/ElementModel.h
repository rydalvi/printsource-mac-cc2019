#ifndef __CElementModel_h__
#define __CElementModel_h__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "PicklistField.h"
#include "Details.h"

class CElementModel : public PicklistField
{
private:
	
	double		elementId;
	//int32		element_type_id;	
	//PMString	code;
	//PMString	info;	
	//PMString	attribute_no;
	PMString	displayName;
	bool16		cdActive;
	bool16		printActive;
	bool16		webActive;
	//int			seq_order;
	//bool16		tabular_data;
	//PMString	separator;
	int32		editLines;	

	double		typeId;

	//int32		pv_type_id;
	//int32		min_length;
	//int32		max_length;
	double		languageId;
	bool16		required;
	double		parentElementId;
	//int32		classId;
	//bool16		pub_item;
	double		dataTypeId;
	PMString	emptyValueDisplay;
	int32		uniqueness;
	PMString	dataTypeString;
	PMString	sysName;
	
	vector<Details>	nameList;

public:

	CElementModel()
	{
		elementId = 0;		
		displayName = "";
		cdActive = kFalse;
		printActive = kTrue;
		webActive = kFalse;		
		editLines = -1;	
		typeId = -1;		
		languageId= -1;		
		parentElementId= -1;		
		dataTypeId= -1;
		emptyValueDisplay = "";
		uniqueness= -1;
		dataTypeString = "";
		sysName = "";
	}

	/**
	 * @return Returns the elementId.
	 */
	double getElementId() {
		return elementId;
	}
	/**
	 * @param classId The elementId to set.
	 */
	void setElementId(double elementId) {
		this->elementId = elementId;
	}

	/**
	 * @return Returns the cdActive.
	 */
	bool16 getCdActive() {
		return cdActive;
	}
	/**
	 * @param cdActive The cdActive to set.
	 */
	void setCdActive(bool16 cdActive) {
		this->cdActive = cdActive;
	}
	
	/**
	 * @return Returns the displayName.
	 */
	PMString getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName The displayName to set.
	 */
	void setDisplayName(PMString displayName) {
		this->displayName = displayName;
	}
	
	/**
	 * @return Returns the editLines.
	 */
	int32 getEditLines() {
		return editLines;
	}
	/**
	 * @param editLines The editLines to set.
	 */
	void setEditLines(int32 editLines) {
		this->editLines = editLines;
	}
	
	/**
	 * @return Returns the emptyValueDisplay.
	 */
	PMString getEmptyValueDisplay() {
		return emptyValueDisplay;
	}
	/**
	 * @param emptyValueDisplay The emptyValueDisplay to set.
	 */
	void setEmptyValueDisplay(PMString emptyValueDisplay) {
		this->emptyValueDisplay = emptyValueDisplay;
	}
	
	/**
	 * @return Returns the languageId.
	 */
	double getLanguageId() {
		return languageId;
	}
	/**
	 * @param languageId The languageId to set.
	 */
	void setLanguageId(double languageId) {
		this->languageId = languageId;
	}
	/**
	 * @return Returns the dataTypeId.
	 */
	double getDataTypeId() {
		return dataTypeId;
	}
	/**
	 * @param dataTypeId The dataTypeId to set.
	 */
	void setDataTypeId(double dataTypeId) {
		this->dataTypeId = dataTypeId;
	}
	/**
	 * @return Returns the uniqueness.
	 */
	int32 getUniqueness() {
		return uniqueness;
	}
	/**
	 * @param uniqueness The uniqueness to set.
	 */
	void setUniqueness(int32 uniqueness) {
		this->uniqueness = uniqueness;
	}
	/**
	 * @return Returns the parentElementId.
	 */
	double getParentElementId() {
		return parentElementId;
	}
	/**
	 * @param parentElementId The parentElementId to set.
	 */
	void setParentElementId(double parentElementId) {
		this->parentElementId = parentElementId;
	}
	/**
	 * @return Returns the printActive.
	 */
	bool16 getPrintActive() {
		return printActive;
	}
	/**
	 * @param printActive The printActive to set.
	 */
	void setPrintActive(bool16 printActive) {
		this->printActive = printActive;
	}
	
	/**
	 * @return Returns the dataTypeString.
	 */
	PMString getDataTypeString() {
		return dataTypeString;
	}
	/**
	 * @param dataTypeString The dataTypeString to set.
	 */
	void setDataTypeString(PMString dataTypeString) {
		this->dataTypeString = dataTypeString;
	}
		
	/**
	 * @return Returns the typeId.
	 */
	double getTypeId() {
		return typeId;
	}
	/**
	 * @param typeId The typeId to set.
	 */
	void setTypeId(double typeId) {
		this->typeId = typeId;
	}
	/**
	 * @return Returns the webActive.
	 */
	bool16 getWebActive() {
		return webActive;
	}
	/**
	 * @param webActive The webActive to set.
	 */
	void setWebActive(bool16 webActive) {
		this->webActive = webActive;
	}

	PMString getSysName() {
		return sysName;
	}
	
	void setSysName(PMString str) {
		this->sysName = str;
	}
	
	void setNameList(vector<Details> vectorObj)
	{
		nameList = vectorObj;
	}

	vector<Details> getNameList()
	{
		return nameList;
	}

	PMString getDisplayNameByLanguageId(double languageId) {

		PMString result("");
		for(int32 i =0; i < nameList.size(); i++)
		{
			Details detailObj = nameList.at(i);
			if(detailObj.getLanguageId() == languageId)
			{
				return detailObj.getValue();
			}
		}
		return displayName;
	}

};

#endif
