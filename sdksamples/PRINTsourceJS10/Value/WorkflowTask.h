#ifndef __WORKFLOWTASK__
#define __WORKFLOWTASK__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "WorkflowItem.h"

using namespace std;

class WorkflowTask
{
private:
    double clientId;
    double taskId;
    double objectId;
    double currentStepId;
    PMString taskName;
    double dueDate;
    PMString taskStatus;
    bool16 emailNotification;
    double attributeGroupId;
    double userId;
    double roleId;
    double languageId;
    double assignorId;
    PMString assignorName;
    double workflowId;
    PMString workflowName;
    PMString objectType;
    
    WorkflowItem workflowItem;
    
    
public:
	WorkflowTask()
	{
        clientId=-1;
        taskId=-1;
        objectId=-1;
        currentStepId=-1;
        taskName="";
        dueDate=-1;
        taskStatus="";
        emailNotification= kFalse;
        attributeGroupId=-1;
        userId=-1;
        roleId=-1;
        languageId=-1;
        assignorId=-1;
        assignorName="";
        workflowId=-1;
        workflowName="";
        objectType="";
	
	}
	
	~WorkflowTask()
	{
	}
    
    double getClientId()
    {
        return clientId;
    }
    
    void setClientId(double id)
    {
        clientId = id;
    }
    
    double getTaskId()
    {
        return taskId;
    }
    
    void setTaskId(double id)
    {
        taskId = id;
    }
    
    double getObjectId()
    {
        return objectId;
    }
    
    void setObjectId(double id)
    {
        objectId = id;
    }
    
    double getCurrentStepId()
    {
        return currentStepId;
    }
    
    void setCurrentStepId(double id)
    {
        currentStepId = id;
    }
    
    PMString getTaskName()
    {
        return taskName;
    }
    
    void setTaskName(PMString stringObj)
    {
        taskName = stringObj;
    }
    
    double getDueDate()
    {
        return dueDate;
    }
    
    void setDueDate(double id)
    {
        dueDate = id;
    }
    
    PMString getTaskStatus()
    {
        return taskStatus;
    }
    
    void setTaskStatus(PMString stringObj)
    {
        taskStatus = stringObj;
    }
    
    bool16 getEmailNotification()
    {
        return emailNotification;
    }
    
    void setEmailNotification(bool16 boolObj)
    {
        emailNotification = boolObj;
    }
    
    double getAttributeGroupId()
    {
        return attributeGroupId;
    }
    
    void setAttributeGroupId(double id)
    {
        attributeGroupId = id;
    }
    
    double getUserId()
    {
        return userId;
    }
    
    void setUserId(double id)
    {
        userId = id;
    }
    
    double getRoleId()
    {
        return roleId;
    }
    
    void setRoleId(double id)
    {
        roleId = id;
    }
    
    double getLanguageId()
    {
        return languageId;
    }
    
    void setLanguageId(double id)
    {
        languageId = id;
    }
    
    double getAssignorId()
    {
        return assignorId;
    }
    
    void setAssignorId(double id)
    {
        assignorId = id;
    }
	
    PMString getAssignorName()
    {
        return assignorName;
    }
    
    void setAssignorName(PMString stringObj)
    {
        assignorName = stringObj;
    }
    
    double getWorkflowId()
    {
        return workflowId;
    }
    
    void setWorkflowId(double id)
    {
        workflowId = id;
    }
    
    PMString getWorkflowName()
    {
        return workflowName;
    }
    
    void setWorkflowName(PMString stringObj)
    {
        workflowName = stringObj;
    }
    
    PMString getObjectType()
    {
        return objectType;
    }
    
    void setObjectType(PMString stringObj)
    {
        objectType = stringObj;
    }
    
    WorkflowItem getWorkflowItem()
    {
        return workflowItem;
    }
    
    void setWorkflowItem(WorkflowItem workflowItemObj)
    {
        this->workflowItem = workflowItemObj;
    }
    
};

#endif