#ifndef __COLUMN__
#define __COLUMN__

#include "VCPluginHeaders.h"
#include "PMString.h"

using namespace std;

class Column
{
private:

	PMString	fieldName;
	double		fieldId;
	int32		index;
	bool8		changed;
	bool8		red;

public:
	Column()
	{
		fieldName = "";
		fieldId = -1;
		index = -1;
		changed = kFalse;
		red = kFalse;
	}
	
	~Column()
	{
	}
	

	double getFieldId()
	{
		return fieldId;
	}
	void setFieldId(double id)
	{
		fieldId=id;
	}

	PMString getFieldName()
	{
		return fieldName;
	}
	void setFieldName(PMString tempName)
	{
		fieldName = tempName;
	}

	int32 getIndex()
	{
		return index;
	}
	void setIndex(int32 id)
	{
		index=id;
	}

	bool8 getChanged()
	{
		return changed;
	}
	void setChanged(bool8 changeFlag)
	{
		changed=changeFlag;
	}

	bool8 getRed()
	{
		return red;
	}
	void setRed(bool8 redFlag)
	{
		red=redFlag;
	}

};

#endif