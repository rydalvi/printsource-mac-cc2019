//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ISession.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"

// General includes:
#include "CActionComponent.h"
#include "RLActionComponent.h" 
#include "CAlert.h"

// Project includes:
#include "RLID.h"

#include "AFWJSID.h"
#include "IAppFramework.h"

#include "IActionStateList.h"
#include "IBookManager.h"
#include "IBookContentMgr.h"
#include "IBook.h"
#include "IWindow.h"

#define CA(x) CAlert::InformationAlert(x);
IDialog* firstDialog = 0;
/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

	
	@ingroup apjs10_refreshlanguage

*/
//class RLActionComponent : public CActionComponent
//{
//public:
///**
// Constructor.
// @param boss interface ptr from boss object on which this interface is aggregated.
// */
//		RLActionComponent(IPMUnknown* boss);
//
//		/** The action component should perform the requested action.
//			This is where the menu item's action is taken.
//			When a menu item is selected, the Menu Manager determines
//			which plug-in is responsible for it, and calls its DoAction
//			with the ID for the menu item chosen.
//
//			@param actionID identifies the menu item that was selected.
//			@param ac active context
//			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
//			@param widget contains the widget that invoked this action. May be nil. 
//			*/
//		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);
//
//	private:
//		/** Encapsulates functionality for the about menu item. */
//		void DoAbout();
//		
//		/** Opens this plug-in's dialog. */
//		void DoDialog();
//		
//
//
//};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(RLActionComponent, kRLActionComponentImpl)

/* RLActionComponent Constructor
*/
RLActionComponent::RLActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{
}

/* DoAction
*/
void RLActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{
	switch (actionID.Get())
	{

		case kRLAboutActionID:
		{
			this->DoAbout();
			break;
		}
					

		case kRLDialogActionID:
		{
			this->DoDialog();
			break;
		}

		case kRefreshByLaanguageActionID:
		{
			this->DoDialog();
			break;
		}

		//case ktempActionID:
		//{
		//	CA("Close this ");
		//	break;
		//}

		default:
		{
			break;
		}
	}
}

/* DoAbout
*/
void RLActionComponent::DoAbout()
{
	CAlert::ModalAlert
	(
		kRLAboutBoxStringKey,				// Alert string
		kOKString, 						// OK button
		kNullString, 						// No second button
		kNullString, 						// No third button
		1,							// Set OK button to default
		CAlert::eInformationIcon				// Information icon.
	);
}



/* DoDialog
*/
void RLActionComponent::DoDialog()
{
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		ASSERT(application);
		if (application == nil) {	
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		ASSERT(dialogMgr);
		if (dialogMgr == nil) {
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kRLPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			kSDKDefDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		ASSERT(dialog);
		if (dialog == nil) {
			break;
		}

		InterfacePtr<IWindow> window(dialog, UseDefaultIID());
		ASSERT(window != nil);
		if (window == nil) {
			break;
		}

		PMString windowTitle(kRLDialog1TitleKey);
		window->SetTitle(windowTitle);

		firstDialog = dialog;
		// Open the dialog.
		dialog->Open(); 
	
	} while (false);			
}

//  Code generated by DollyXs code generator


bool16 IsTheBookBlank();
void RLActionComponent::UpdateActionStates(IActionStateList* iListPtr)
{	
	//CA("RLActionComponent::UpdateActionStates");
	do
	{
		IAppFramework* ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;
	
		bool16 result=ptrIAppFramework->getLoginStatus();

		for(int32 iter = 0; iter < iListPtr->Length(); iter++) 
		{
			ActionID actionID = iListPtr->GetNthAction(iter);		
			
			if(actionID == kRefreshByLaanguageActionID)
			{
				if(result && !IsTheBookBlank()){
					//CA("going to enable");
					iListPtr->SetNthActionState(iter,kEnabledAction);
				}
				else{//CA("going to desable");
					iListPtr->SetNthActionState(iter,kDisabled_Unselected);
				}
			}
		}
	}while(0);
}
void RLActionComponent::UpdateActionStates(IActiveContext* ac, IActionStateList *listToUpdate, GSysPoint mousePoint, IPMUnknown* widget)
{
	do
	{
		IAppFramework* ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;
	
		bool16 result=ptrIAppFramework->getLoginStatus();

		for(int32 iter = 0; iter < listToUpdate->Length(); iter++) 
		{
			ActionID actionID = listToUpdate->GetNthAction(iter);		
			
			if(actionID == kRefreshByLaanguageActionID)
			{
				if(result && !IsTheBookBlank())
					listToUpdate->SetNthActionState(iter,kEnabledAction);
				else
					listToUpdate->SetNthActionState(iter,kDisabled_Unselected);
			}
		}
	}while(0);
	
}
bool16 IsTheBookBlank()
{
//CA("IsTheBookBlank");
	bool16 status = kFalse;
	do
	{
		IAppFramework* ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;
		
		InterfacePtr<IBookManager> bookManager(/*gSession*/GetExecutionContextSession(), UseDefaultIID());
		if (bookManager == nil) 
		{ 
			ptrIAppFramework->LogDebug("AP46_UWBDialogObserver::UWBDialogObserver::IsTheBookBlank::bookManager == nil");
			break; 
		}
		IBook * currentActiveBook = bookManager->GetCurrentActiveBook();
		if(currentActiveBook == nil)
		{
			ptrIAppFramework->LogDebug("AP46_UWBDialogObserver::UWBDialogObserver::IsTheBookBlank::currentActiveBook == nil");
			break;			
		}
	
		InterfacePtr<IBookContentMgr> iBookContentMgr(currentActiveBook,UseDefaultIID());
		if(iBookContentMgr == nil)
		{
			ptrIAppFramework->LogDebug("AP46_UWBDialogObserver::UWBDialogObserver::IsTheBookBlank::iBookContentMgr == nil");
			break;
		}

		int32 contentCount = iBookContentMgr->GetContentCount();
		if(contentCount <= 0)
		{
			status = kTrue;
		}
	}while(kFalse);

	return status;
 }

void RLActionComponent::DoSummaryDialog()
{
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(/*gSession*/GetExecutionContextSession()->QueryApplication());
		ASSERT(application);
		if (application == nil) {	
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		ASSERT(dialogMgr);
		if (dialogMgr == nil) {
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kRLPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			kSDKDefDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		ASSERT(dialog);
		if (dialog == nil) {
			break;
		}


		// Set appropriate window title based on mode
		InterfacePtr<IWindow> window(dialog, UseDefaultIID());
		ASSERT(window != nil);
		if (window == nil) {
			break;
		}

		PMString windowTitle(kRLDialog4TitleKey);
		window->SetTitle(windowTitle);
		
		// Open the dialog.
		dialog->Open(); 
	
	} while (false);			
}
