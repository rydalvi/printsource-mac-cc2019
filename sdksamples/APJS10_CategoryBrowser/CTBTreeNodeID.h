#ifndef __CTBTreeNodeID__
#define __CTBTreeNodeID__

#include "NodeID.h"
#include "IPMStream.h"
#include "CTBID.h"

const int kCTBIntNodeId = 0; // This value is not used


class CTBTreeNodeID : public NodeIDClass
{
	public:
		enum { kNodeType = kCTBIntNodeId }; 


		/**	Factory method to create 
		new object of this class
			@return   new instance of this class 
		 */
		//static	NodeID_rv Create() { return new CTBTreeNodeID(); }

		/**	Factory method to create new object of this class
			@param path [IN] specifies initial path
			@return  new instance of this class
		 */
		static	NodeID_rv Create(const double& nodeId)  
			{
				return new CTBTreeNodeID(nodeId); 
			}

		/**	Destructor
		 */
		virtual ~CTBTreeNodeID() {}
		

		/**	@return type of this node
		 */
		virtual	NodeType GetNodeType() const { return kNodeType; } 

		/**	Comparator function
			@param NodeID [IN] specifies the node that we should compare against
			@return  Comparison results
		 */
		virtual int32 Compare(const NodeIDClass* NodeID) const;

		/**	Create a copy of this
			@return  
		 */
		virtual NodeIDClass* Clone() const;

		/**	Read this from the given stream
			@param stream [IN] specified stream
			@return  
		 */
		virtual void Read(IPMStream* stream);

		/**	Write this to the given stream
			@param [OUT] stream 
			@return  
		 */
		virtual void Write(IPMStream* stream) const;

		/**	Accessor for the path associated with this node
			@return const PMString& 
		 */
		const double& GetNodeId() const { return fnodeId; }

	private:
		// Note we're keeping the destructor private to force use of the factory methods
		//CTBTreeNodeID() {}

		CTBTreeNodeID(double nodeId):fnodeId(nodeId) {};

		//mutable int64	fnodeId;
        mutable double	fnodeId;
};

#endif // __PnlTrvFileNodeID__


