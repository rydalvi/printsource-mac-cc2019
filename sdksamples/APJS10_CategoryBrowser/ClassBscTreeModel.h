#ifndef __CLASSBSCTREEMODEL_H
#define __CLASSBSCTREEMODEL_H

class IHierarchy;

/** Helper class to mediate between the tree-view adapter and the document 
	object model. The root of tree is the spreadlist. 

	It's probably better coding practice to factor the model to be independent 
	of the adapter and provide an API like this one here rather than place the 
	burden on the adapter class itself of mapping into the tree-model of 
	interest. It is certainly easier to test.
*/

class ClassBscTreeModel 
{
public:
	ClassBscTreeModel();
	virtual ~ClassBscTreeModel();
	/**
		Accessor for the UID of the root of the tree model. In this
		case we've set the spread-list (on kDocBoss) to be the root of the tree.
	*/
	UID	GetRootUID() const;
	/** Determine how many children the root (spreadlist) has, equal
		to the count of spreads.
		@return number of children that the root node has
	*/
	int32 GetRootCount() const;
	/** Determine the index of the given UID in the root's kids, i.e. what's the
		index of a spread with given UID?
		@param uid
		@return index of child in list of children of root, (-1) if not child of root
	*/
	int32 GetIndexForRootChild(const UID& uid) const;
	/** Determine the UID of the child of the root node (spreadlist) by index, i.e.
		answer the question "what's the UID of the spread at given index?".
		@param index
		@return UID of the child at given index, kInvalidUID if not child at specified index
		
	*/
	UID GetNthRootChild(const int32& index) const;
	/**
		Return count of children on node with given UID.
		@param uid data on node of interest
		@return child count for given node
	*/
	int32 GetChildCount(const UID& uid) const;
	/**
		Determine the UID of the parent of given UID.
		@param uid specifies the item of interest
		@return UID of parent if it can be found or kInvalidUID otherwise (for instance, if uid refers to the root)
	*/
	UID GetParentUID(const UID& uid) const;
	/**
		Determine the index of a child with given UID
		@param parentUID gives UID of the parent
		@param childUID givesn the UID of child that we're seeking index in parent list of kids for
		@return index in the parent's children of the given child, (-1) if failed to find child
	*/
	int32 GetChildIndexFor(const UID& parentUID, const UID& childUID) const;
	/** Determine the UID of child of a given parent by index
		@param parentUID
		@param index
		@return UID of child at given index
	*/
	UID GetNthChildUID(const UID& parentUID, const int32& index) const;
	/** Convert a UID into something that can be displayed in a tree-view.
		The debug build shows the class of the boss object associated with this UID,
		and the release build just displays the UID since the class info is not available.

		@param uid unique identifier for document object
		@return string based representation of the data given	
	*/
	PMString ToString(const UID& uid) const;
	/** Discover how far from the root the node with this UID as data is. 
		For instance, the child of the root will return 1, the grandchildren 2 and so on.

		@param uid specifies the data at the node of interest
		@return how many paths to hop in tree to get to the root
	*/
	int32 GetNodePathLengthFromRoot(const UID& uid);
	void setRoot(int32 val, PMString& pubName, int32 cid) { root=val; classificationName=pubName; classId=cid; }

private:
	static int32 root;
	static int32 classId;//Actually the sub-section id
	static PMString classificationName;
};

#endif

