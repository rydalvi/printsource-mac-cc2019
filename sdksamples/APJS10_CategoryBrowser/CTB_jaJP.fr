//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifdef __ODFRC__


// Japanese string table is defined here

resource StringTable (kSDKDefStringsResourceID + index_jaJP)
{
        k_jaJP,	// Locale Id
        0,		// Character encoding converter

        {
        	// ----- Menu strings
                kCTBCompanyKey,					kCTBCompanyValue,
                kCTBAboutMenuKey,					kCTBPluginName "[JP]...",
                kCTBPluginsMenuKey,				kCTBPluginName "[JP]",

                kSDKDefAboutThisPlugInMenuKey,			kSDKDefAboutThisPlugInMenuValue_jaJP,

                // ----- Command strings

                // ----- Window strings

                // ----- Panel/dialog strings
                kCTBPanelTitleKey,				kCTBPluginName "[JP]",
                kCTBStaticTextKey,				kCTBPluginName "[JP]",

              // ----- Error strings

                // ----- Misc strings
                kCTBAboutBoxStringKey,			kCTBPluginName " [JP], version " kCTBVersion " by " kCTBAuthor "\n\n" kSDKDefCopyrightStandardValue "\n\n" kSDKDefPartnersStandardValue_jaJP,
				 kCTBAboutBoxStringKey,			kCTBPluginName " [JP], version " kCTBVersion " by " kCTBAuthor "\n\n" kSDKDefCopyrightStandardValue "\n\n" kSDKDefPartnersStandardValue_jaJP,
                kCTBPanelTitleKey,		"Select Category or Event",
				kCTBSelectLangaugeandEventCategoryStringKey, "Please Select Langauge and Event/Category To Continue.",
				kCTBSelectLanguageStringKey, " Select Language :",

       
        }

};

#endif // __ODFRC__
