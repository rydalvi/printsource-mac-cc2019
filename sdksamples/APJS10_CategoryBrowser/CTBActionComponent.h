#ifndef __CTBActionComponent_h__
#define __CTBActionComponent_h__

#include "VCPlugInHeaders.h"
#include "CActionComponent.h"
#include "CAlert.h"
//Commented by nitin 21-06-07
//#include "IPaletteMgr.h"
#include "PaletteRefUtils.h"
// Project includes:
#include "CTBID.h"
#include "IActionStateList.h"

/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.	
	@ingroup ap50_categorybrowser

*/
class CTBActionComponent : public CActionComponent
{
public:
/**
 Constructor.
 @param boss interface ptr from boss object on which this interface is aggregated.
 */
		CTBActionComponent(IPMUnknown* boss);

		~CTBActionComponent();
		/** The action component should perform the requested action.
			This is where the menu item's action is taken.
			When a menu item is selected, the Menu Manager determines
			which plug-in is responsible for it, and calls its DoAction
			with the ID for the menu item chosen.

			@param actionID identifies the menu item that was selected.
			@param ac active context
			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
			@param widget contains the widget that invoked this action. May be nil. 
			*/

		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);
		void ClosePallete();
		void DoPallete(/*int32 TopBound=200, int32 LeftBound=200, int32 BottomBound=0, int32 RightBound=0*/);
		void DoPallete(int32 TopBound,int32 LeftBound,int32 BottomBound,int32 RightBound);
		void UpdateActionStates(IActiveContext* ac, IActionStateList* listToUpdate, GSysPoint mousePoint = kInvalidMousePoint, IPMUnknown* widget = nil);
	private:
		/** Encapsulates functionality for the about menu item. */
		void DoAbout();
		//static IPaletteMgr* palettePanelPtr;

};

#endif